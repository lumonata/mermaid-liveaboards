<?php

/*
| -------------------------------------------------------------------------------------
| FUNCTION UNTUK MANAGE DIVE SPOT COLUMNS
| -------------------------------------------------------------------------------------
*/
add_filter( 'manage_dive_spots_posts_columns', 'dive_spot_columns' );

function dive_spot_columns( $columns ) 
{
    $columns = array(
        'cb'            => $columns['cb'],
        'title'         => __( 'Title' ),
        'destination'   => __( 'Destination' ),
        'latitude'      => __( 'Latitude' ),
        'longitude'     => __( 'Longitude' ),
        'date'         => __( 'Date' ),
    );
    return $columns;
}

add_action( 'manage_dive_spots_posts_custom_column', 'smashing_dive_spots_column', 10, 2);
function smashing_dive_spots_column( $column, $post_id )
{
    if ( 'destination' === $column ):
        echo get_the_title(get_post_meta( $post_id, '_dive_spot_destination', true ));
    endif;

    if ( 'latitude' === $column ):
        echo get_post_meta( $post_id, '_dive_spot_latitude', true );
    endif;

    if ( 'longitude' === $column ):
        echo get_post_meta( $post_id, '_dive_spot_longitude', true );
    endif;
}


/*
| -------------------------------------------------------------------------------------
| FUNCTION UNTUK MANAGE WEATHER COLUMNS
| -------------------------------------------------------------------------------------
*/
add_filter( 'manage_weather_posts_columns', 'weather_columns' );
function weather_columns( $columns ) 
{
    $columns = array(
        'cb'            => $columns['cb'],
        'title'         => __( 'Title' ),
        'year'          => __( 'Year' ),
        'temperature'   => __( 'Temperature' ),
        'icon'          => __( 'Icon' ),
        'date'          => __( 'Date' ),
    );
    return $columns;
}

add_action( 'manage_weather_posts_custom_column', 'smashing_weather_column', 10, 2);
function smashing_weather_column( $column, $post_id )
{
    if ( 'year' === $column ):
        echo get_post_meta( $post_id, '_weather_year', true );
    endif;

    if ( 'temperature' === $column ):
        echo get_post_meta( $post_id, '_weather_high_temperature', true ) .'°c / '.get_post_meta( $post_id, '_weather_low_temperature', true ).'°c';
    endif;

    if ( 'icon' === $column ):
        echo wp_get_attachment_image(get_post_meta( $post_id, '_weather_icon_id', true ), 'thumbnail');
    endif;
}

/*
| -------------------------------------------------------------------------------------
| FUNCTION UNTUK MANAGE SCHEDULES AND RATES COLUMNS
| -------------------------------------------------------------------------------------
*/
add_filter( 'manage_schedule_rates_posts_columns', 'schedule_rates_columns' );

function schedule_rates_columns( $columns ) {
	 $columns = array(
        'cb'                    => $columns['cb'],
        'title'                 => __( 'Title' ),
		'action_link'           => __( 'Action Link' ),
        'date'                  => __( 'Date' ),
       
    );

    return $columns;
}

add_action( 'manage_schedule_rates_posts_custom_column', 'smashing_schedule_rates_column', 10, 2);

function smashing_schedule_rates_column( $column, $post_id ) {
	
	if ( 'action_link' === $column )
    {
        $xml  = site_url( 'wp-content/themes/mermaid-theme-v2/schedule-rates-xml.php?id=' . $post_id );
        $csv  = site_url( 'wp-content/themes/mermaid-theme-v2/schedule-rates-csv.php?id=' . $post_id );
        $dsv  = site_url( 'wp-content/themes/mermaid-theme-v2/schedule-rates-dsv.php?id=' . $post_id );
        $link = get_the_permalink( $post_id );
        
        echo '
        <a class="dashicons-before dashicons-pdf tooltip" target="_blank" href="' . $xml . '">
            <span class="tooltiptext" id="xml_link_' . $post_id . '">Download XML</span>
        </a>
        <a class="dashicons-before dashicons-media-spreadsheet tooltip" target="_blank" href="' . $csv . '">
            <span class="tooltiptext" id="csv_link_' . $post_id . '">Download CSV</span>
        </a>
        <a class="dashicons-before dashicons-media-text tooltip" target="_blank" href="' . $dsv . '">
            <span class="tooltiptext" id="csv_link_' . $post_id . '">Download DSV</span>
        </a>';
    }
}


/*
| -------------------------------------------------------------------------------------
| FUNCTION UNTUK MANAGE CHECKOUT COLUMNS
| -------------------------------------------------------------------------------------
*/
add_filter( 'manage_checkout_posts_columns', 'checkout_columns' );

function checkout_columns( $columns ) 
{
    $columns = array(
        'cb'                    => $columns['cb'],
        'title'                 => __( 'Invoice No' ),
        'boat'                  => __( 'Boat' ),
        'date_trip'             => __( 'Trip Date' ),
        'trip_code'             => __( 'Trip Code' ),
        'destination'           => __( 'Destination' ),
        'status'                => __( 'Status Payment' ),
        'next_payment_date'     => __( 'Next Payment Date' ),
        'action_link'           => __( 'Action Link' ),
    );

    return $columns;
}

add_action( 'manage_checkout_posts_custom_column', 'smashing_checkout_column', 10, 2);

function smashing_checkout_column( $column, $post_id )
{
    $prefix = "_checkout_";

    if ( 'boat' === $column )
    {
        $boat_id = get_post_meta( $post_id, $prefix . 'boat', true );

        echo get_the_title( $boat_id );
    }

    if ( 'date_trip' === $column )
    {
        $departure_date = get_post_meta( $post_id, 'departure_date', true );
        $arrival_date   = get_post_meta( $post_id, 'arrival_date', true );

        if( empty( $departure_date ) && empty( $arrival_date ) )
        {
            echo '-';
        }
        else
        {
            if( $departure_date != '' && $arrival_date != '' )
            {
                echo date( 'd M Y', strtotime( $departure_date ) ) . ' - ' . date( 'd M Y', strtotime( $arrival_date ) );
            }
            elseif( empty( $arrival_date ) && $departure_date != '' )
            {
                echo date( 'd M Y', strtotime( $departure_date ) );
            }
            elseif( empty( $departure_date ) && $arrival_date != '' )
            {
                echo date( 'd M Y', strtotime( $arrival_date ) );
            }
        }
    }

    if ( 'trip_code' === $column )
    {
        $trip_code  = get_post_meta( $post_id, $prefix . 'trip_code', true );

        if( empty( $trip_code ) )
        {
            echo '-';
        }
        else
        {
            $ntrip_code = json_decode( base64_decode( $trip_code ) );

            if( $ntrip_code === null && json_last_error() !== JSON_ERROR_NONE )
            {
                echo $trip_code;
            }
            else
            {
                echo $ntrip_code->post_title;
            }
        }
    }

    if ( 'destination' === $column )
    {
        $iteneraries_id = get_post_meta( $post_id, $prefix . 'iteneraries', true );

        if( empty( $iteneraries_id ) )
        {
            echo '-';
        }
        else
        {
            echo get_the_title( $iteneraries_id );
        }
    }

    if ( 'status' === $column )
    {
        $status_payment = get_post_meta( $post_id, $prefix . 'status_payment', true );

        echo ucwords( $status_payment );
    }

    if ( 'next_payment_date' === $column )
    {
        $reminder_date = get_post_meta( $post_id, $prefix . 'reminder_date', true );

        if( empty( $reminder_date ) )
        {
            echo '-';
        }
        else
        {
            echo date( 'd M Y', strtotime( $reminder_date ) );
        }
    }

    if ( 'action_link' === $column )
    {
        $pdf  = site_url( 'wp-content/themes/mermaid-theme-v2/invoice-pdf.php?id=' . $post_id );
        $xls  = site_url( 'wp-content/themes/mermaid-theme-v2/invoice-xlsx.php?id=' . $post_id );
        $link = get_the_permalink( $post_id );
        
        echo '
        <a class="dashicons-before dashicons-external tooltip" target="_blank" href="' . $link . '">
            <span class="tooltiptext" id="payment_link_' . $post_id . '">Go To Link Payment</span>
        </a>
        <!--
        <a class="dashicons-before dashicons-pdf tooltip" target="_blank" href="' . $pdf . '">
            <span class="tooltiptext" id="pdf_link_' . $post_id . '">Download PDF</span>
        </a>
        -->
        <a class="dashicons-before dashicons-media-spreadsheet tooltip" target="_blank" href="' . $xls . '">
            <span class="tooltiptext" id="xlsx_link_' . $post_id . '">Download Excel</span>
        </a>
        <a class="dashicons-before dashicons-admin-page tooltip copy-link" data-id="'.$post_id.'" onmouseout="outFunc(' . $post_id . ')">
            <span class="tooltiptext" id="copy_link_' . $post_id . '">Copy To Clipboard</span>
            <input type="text" class="hidden_input" name="link" id="link_' . $post_id . '" value="' . $link . '" />
        </a>';
    }
}

/*
| -------------------------------------------------------------------------------------
| FUNCTION UNTUK MANAGE CHECKOUT 2 COLUMNS
| -------------------------------------------------------------------------------------
*/
add_filter( 'manage_checkout-2_posts_columns', 'checkout_2_columns' );

function checkout_2_columns( $columns ) 
{
    $columns = array(
        'cb'                => $columns['cb'],
        'title'             => __( 'Invoice No' ),
        'boat'              => __( 'Boat' ),
        'date_trip'         => __( 'Date' ),
        'trip_code'         => __( 'Trip Code' ),
        'destination'       => __( 'Destination' ),
        'status'            => __( 'Status Payment' ),
        'next_payment_date' => __( 'Next Payment Date' ),
        'action_link'       => __( 'Action Link' ),
    );

    return $columns;
}

add_action( 'manage_checkout-2_posts_custom_column', 'smashing_checkout_2_column', 10, 2 );

function smashing_checkout_2_column( $column, $post_id )
{
    $prefix = "_checkout2_";

    if ( 'boat' === $column )
    {
        $boat_id = get_post_meta( $post_id, $prefix . 'boat', true );

        echo get_the_title( $boat_id );
    }

    if ( 'date_trip' === $column )
    {
        $departure_date = get_post_meta( $post_id, 'departure_date', true );
        $arrival_date   = get_post_meta( $post_id, 'arrival_date', true );

        if( empty( $departure_date ) && empty( $arrival_date ) )
        {
            echo '-';
        }
        else
        {
            if( $departure_date != '' && $arrival_date != '' )
            {
                echo date( 'd M Y', strtotime( $departure_date ) ) . ' - ' . date( 'd M Y', strtotime( $arrival_date ) );
            }
            elseif( empty( $arrival_date ) && $departure_date != '' )
            {
                echo date( 'd M Y', strtotime( $departure_date ) );
            }
            elseif( empty( $departure_date ) && $arrival_date != '' )
            {
                echo date( 'd M Y', strtotime( $arrival_date ) );
            }
        }
    }

    if ( 'trip_code' === $column )
    {
        $trip_code  = get_post_meta( $post_id, $prefix . 'trip_code', true );

        if( empty( $trip_code ) )
        {
            echo '-';
        }
        else
        {
            $ntrip_code = json_decode( base64_decode( $trip_code ) );

            if( $ntrip_code === null && json_last_error() !== JSON_ERROR_NONE )
            {
                echo $trip_code;
            }
            else
            {
                echo $ntrip_code->post_title;
            }
        }
    }

    if ( 'destination' === $column )
    {
        $iteneraries_id = get_post_meta( $post_id, $prefix . 'iteneraries', true );

        if( empty( $iteneraries_id ) )
        {
            echo '-';
        }
        else
        {
            echo get_the_title( $iteneraries_id );
        }
    }

    if ( 'status' === $column )
    {
        $status_payment = get_post_meta( $post_id, $prefix . 'status_payment', true );

        echo ucwords( $status_payment );
    }

    if ( 'next_payment_date' === $column )
    {
        $reminder_date = get_post_meta( $post_id, $prefix . 'reminder_date', true );

        if( empty( $reminder_date ) )
        {
            echo '-';
        }
        else
        {
            echo date( 'd M Y', strtotime( $reminder_date ) );
        }
    }

    if ( 'action_link' === $column )
    {
        $pdf  = site_url( 'wp-content/themes/mermaid-theme-v2/invoice-pdf.php?id=' . $post_id );
        $xls  = site_url( 'wp-content/themes/mermaid-theme-v2/invoice-xlsx.php?id=' . $post_id );
        $link = get_the_permalink( $post_id );
        
        echo '
        <a class="dashicons-before dashicons-external tooltip" target="_blank" href="' . $link . '">
            <span class="tooltiptext" id="payment_link_' . $post_id . '">Go To Link Payment</span>
        </a>
        <!--
        <a class="dashicons-before dashicons-pdf tooltip" target="_blank" href="' . $pdf . '">
            <span class="tooltiptext" id="pdf_link_' . $post_id . '">Download PDF</span>
        </a>
        -->
        <a class="dashicons-before dashicons-media-spreadsheet tooltip" target="_blank" href="' . $xls . '">
            <span class="tooltiptext" id="xlsx_link_' . $post_id . '">Download Excel</span>
        </a>
        <a class="dashicons-before dashicons-admin-page tooltip copy-link" data-id="'.$post_id.'" onmouseout="outFunc(' . $post_id . ')">
            <span class="tooltiptext" id="copy_link_' . $post_id . '">Copy To Clipboard</span>
            <input type="text" class="hidden_input" name="link" id="link_' . $post_id . '" value="' . $link . '" />
        </a>';
    }
}

/*
| -------------------------------------------------------------------------------------
| FUNCTION UNTUK MANAGE ITINERARIES COLUMNS
| -------------------------------------------------------------------------------------
*/
add_filter( 'manage_iteneraries_posts_columns', 'iteneraries_columns' );
add_action( 'manage_iteneraries_posts_custom_column', 'iteneraries_return_columns', 10, 2 );

function iteneraries_columns( $columns ) 
{
    $routes  = array( '_iteneraries_route_id' => __( 'Mapping ID' ) );
    $columns = array_slice( $columns, 0, 2, true ) + $routes + array_slice( $columns, 2, NULL, true );

    return $columns;
}

function iteneraries_return_columns( $columns, $post_id )
{
    if ( '_iteneraries_route_id' === $columns )
    {
        $route_id = get_post_meta( $post_id, '_iteneraries_route_id', true );

        echo $route_id;
    }
}

/*
| -------------------------------------------------------------------------------------
| FUNCTION UNTUK MANAGE CABINS COLUMNS
| -------------------------------------------------------------------------------------
*/
add_filter( 'manage_cabins_posts_columns', 'cabins_columns' );
add_action( 'manage_cabins_posts_custom_column', 'cabins_return_columns', 10, 2 );

function cabins_columns( $columns ) 
{
    $routes  = array( '_cabins_id' => __( 'Mapping ID' ) );
    $columns = array_slice( $columns, 0, 2, true ) + $routes + array_slice( $columns, 2, NULL, true );

    return $columns;
}

function cabins_return_columns( $columns, $post_id )
{
    if ( '_cabins_id' === $columns )
    {
        $route_id = get_post_meta( $post_id, '_cabins_id', true );

        echo $route_id;
    }
}

/*
| -------------------------------------------------------------------------------------
| FUNCTION UNTUK MANAGE BOATS COLUMNS
| -------------------------------------------------------------------------------------
*/
add_filter( 'manage_boat_posts_columns', 'boat_columns' );
add_action( 'manage_boat_posts_custom_column', 'boat_return_columns', 10, 2 );

function boat_columns( $columns ) 
{
    $routes  = array( '_boat_id_' => __( 'Mapping ID' ) );
    $columns = array_slice( $columns, 0, 2, true ) + $routes + array_slice( $columns, 2, NULL, true );

    return $columns;
}

function boat_return_columns( $columns, $post_id )
{
    if ( '_boat_id_' === $columns )
    {
        $route_id = get_post_meta( $post_id, '_boat_id_', true );

        echo $route_id;
    }
}
