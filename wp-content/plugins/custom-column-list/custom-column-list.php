<?php
/*
Plugin Name: Custom Column List
Description: This plugin for manage column list admin post type
Author: Tantri Mindrawan
*/

define( 'CCL_VERSION', '1.0.0' );

define( 'CCL_PLUGIN', __FILE__ );

define( 'CCL_PLUGIN_BASENAME', plugin_basename( CCL_PLUGIN ) );

define( 'CCL_PLUGIN_NAME', trim( dirname( CCL_PLUGIN_BASENAME ), '/' ) );

define( 'CCL_PLUGIN_DIR', untrailingslashit( dirname( CCL_PLUGIN ) ) );


// Include ls-functions.php, user require_once to stop the script if ls-functions.php is not found
require_once plugin_dir_path(__FILE__) . 'includes/ccl-functions.php';