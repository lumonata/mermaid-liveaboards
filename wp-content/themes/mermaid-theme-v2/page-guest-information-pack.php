<?php
/*
Template Name: Guest - Agent Information Pack Page
*/
?>

<?php
if(!is_agent_logged_in()):
    if(is_page('agent-information-pack')):
        global $wp_query;
        $wp_query->set_404();
        status_header( 404 );
        get_template_part( 404 ); exit();
    endif;
endif;
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>

    <?php include_once "layout/hero.php"; ?>

    <?php
        // INISIALISASI VARIBLE PAGE
        $title   = get_the_title();
        $title   = empty($title) ? '': '<h1 class="heading-default">'.$title.'</h1>';
        $content = get_the_content();
        $content = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $content);
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = empty($content) ? '': '<div class="desc-about">'.$content.'</div>';
    ?>

    <!-- SECTION WELCOME -->
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        echo $title;
                        echo $content;
                    ?>
                </div>
            </div>
            <div class="row file-guest-information">
                <?php
                $title_mermaid_i  = get_post_meta(get_the_ID(), '_page_mermaid_i_title', true);
                $file_mermaid_i   = get_post_meta(get_the_ID(), '_page_file_mermaid_i', true);
                $title_mermaid_ii = get_post_meta(get_the_ID(), '_page_mermaid_ii_title', true);
                $file_mermaid_ii  = get_post_meta(get_the_ID(), '_page_file_mermaid_ii', true);

                if(!empty($file_mermaid_i) && filter_var( $file_mermaid_i, FILTER_VALIDATE_URL ) !== false):
                    echo '
                    <div class="col-lg-6 col-md-12">
                        <a href="'.$file_mermaid_i.'" target="_blank">
                            <h2>'.$title_mermaid_i.'<br/> '.get_the_title().'</h2>
                        </a>
                    </div>
                    ';
                endif;

                if(!empty($file_mermaid_ii) && filter_var( $file_mermaid_ii, FILTER_VALIDATE_URL ) !== false):
                    echo '
                    <div class="col-lg-6 col-md-12">
                        <a href="'.$file_mermaid_ii.'" target="_blank">
                            <h2>'.$title_mermaid_ii.'<br/> '.get_the_title().'</h2>
                        </a>
                    </div>
                    ';
                endif;
                ?>
                
            </div>
        </div>
    </section>


    <?php endif; ?>

<?php get_footer(); ?>