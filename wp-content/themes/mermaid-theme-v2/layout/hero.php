<?php

if( is_post_type_archive( 'destination' ) )
{
    $bg_header        = get_the_post_thumbnail_url( '240' );
    $bg_header_medium = get_the_post_thumbnail_url( '240', 'medium_large' );
}
elseif( is_search() )
{
    $slug         = $_SESSION['boat_name'];
    $queried_post = get_page_by_path( $slug, '', 'boat' );

    $post_id = $queried_post->ID;

    $bg_header        = wp_get_attachment_image_src( get_post_meta( $post_id, '_boat_image_search_id', true ), 'large' );
    $bg_header_medium = wp_get_attachment_image_src( get_post_meta( $post_id, '_boat_image_search_id', true ), 'medium_large' );
    $bg_header        = $bg_header[0];
    $bg_header_medium = $bg_header_medium[0];
}
elseif( is_category_parent( 'trip-reports' ) )
{
    $queried_object   = get_queried_object();
    $term_id          = $queried_object->term_id;
    $bg_header        = get_option( '_category_image'.$term_id );
    $bg_header_medium = $bg_header;
}
elseif( is_post_type_archive( 'review' ) )
{
    $page             = get_page_by_path( 'reviews' );
    $ID               = $page->ID;
    $bg_header        = get_the_post_thumbnail_url( $ID );
    $bg_header_medium = get_the_post_thumbnail_url( $ID, 'medium_large' );
}
elseif( is_tag() )
{
    $page             = get_page_by_path( 'news-events' );
    $ID               = $page->ID;
    $bg_header        = get_the_post_thumbnail_url( $ID );
    $bg_header_medium = get_the_post_thumbnail_url( $ID, 'medium_large' );
}
elseif( is_singular( 'gallery' ) )
{
    $id               = get_the_ID();
    $bg_header        = get_the_post_thumbnail_url( $id );
    $bg_header_medium = get_the_post_thumbnail_url( $id, 'medium_large' );
}
else
{
    if( $post->post_parent > 0 )
    {
        $posts = get_post( $post->post_parent );

        if( $posts->post_name == "schedule-rates" )
        {
            $end_url          = end( cek_url() );
            $boat             = get_page_by_path( $end_url, '', 'boat' );
            $bg_header        = get_the_post_thumbnail_url( $boat->ID );
            $bg_header_medium = get_the_post_thumbnail_url( $boat->ID, 'medium_large' );
        }
        else
        {
            $bg_header        = get_the_post_thumbnail_url();
            $bg_header_medium = get_the_post_thumbnail_url( $boat->ID, 'medium_large' );
        }
    }
    else
    {
        $bg_header        = get_the_post_thumbnail_url();
        $bg_header_medium = get_the_post_thumbnail_url( '', 'medium_large' );
    }
}

$header_slide_image     = "";
$bg_header_image        = empty( $bg_header ) ? THEME_URL_ASSETS.'/images/mermaid-header-main.jpg' : $bg_header;
$bg_header_image_medium = empty( $bg_header_medium ) ? THEME_URL_ASSETS.'/images/mermaid-header-main.jpg' : $bg_header_medium;

?>
<section class="top-header b-lazy" data-src="<?= $bg_header_image ?>'" data-src-small="<?= $bg_header_image_medium ?>">

    <?php if( is_singular( 'boat' ) ): ?>
    <div class="arrow prev" data-post="<?= get_the_ID() ?>"></div>
    <div class="arrow next" data-post="<?= get_the_ID() ?>"></div>
    <?php endif; ?>

    <?php if( is_home() || is_front_page() || is_page( 'contact-us' ) || is_page( 'about-us' ) || is_singular( 'gallery' ) ): ?>
    <div class="logo">
        <a href="<?= get_site_url() ?>">
            <img data-src-static="<?= get_template_directory_uri() ?>/assets/images/mermaid-logo.png" alt="" />
        </a>
    </div>
    <?php endif; ?>

    <div class="line-gradient"></div>
</section>

<section class="wrap-form-search">
    <div class="container">
        <div class="row">
            <div class="col-md-12" >

                <?php if( !is_singular( 'post' ) && !is_page_template( 'page-about.php' ) ): ?>
                <div class="form-search">
                    <form role="search" action="<?= esc_url( home_url( '/' ) ); ?>" method="GET">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 box-form-search">

                                <?php if( ( isset( $_SESSION['boat_name'] ) && !empty( $_SESSION['boat_name'] ) ) && isset( $_GET['s'] ) ) : ?>
                                <div class="select-mermaid-form select-default">
                                    <?php $boat = isset( $_GET['s'] ) && !empty( $_GET['s'] ) ? $_GET['s'] : get_label_string( 'Select Your Mermaid', true ); ?>
                                    <?php $bval = $_SESSION['boat_name']; ?>
                                    <input type="hidden" name="s" class="form-hidden required" value="<?= $bval; ?>">

                                    <?php if ( $_SESSION['boat_name'] == 'mermaid-ii' ): ?>
                                    <span>Mermaid II</span>
                                    <?php else: ?>
                                    <span>Mermaid I</span>
                                    <?php endif; ?>

                                    <ul class="shadow-default">
                                        <?php $i = 0; ?>
                                        <?php while( $boat_data->have_posts() ): $boat_data->the_post(); ?>

                                        <?php if( $bval == $post->post_name ): ?>
                                        <li class="active" data-value="<?= $post->post_name ?>" data-title="<?= get_the_title() ?>"><?= get_the_title() ?></li>
                                        <?php else: ?>
                                        <li data-value="<?= $post->post_name ?>" data-title="<?= get_the_title() ?>"><?= get_the_title() ?></li>
                                        <?php endif; ?>
                                        
                                        <?php $i++; ?>
                                        <?php endwhile; ?>
                                        <?php wp_reset_postdata(); ?>
                                    </ul>
                                </div>
                                <?php else: ?>

                                <div class="select-mermaid-form select-default">
                                    <?php $boat = isset( $_GET['s'] ) && !empty( $_GET['s'] ) ? $_GET['s'] : get_label_string( 'Select Your Mermaid', true ); ?>
                                    <?php $bval = isset( $_GET['s'] ) ? $_GET['s'] : ''; ?>
                                    <input type="hidden" name="s" class="form-hidden required" value="<?php echo $bval; ?>">

                                    <?php global $boat_data_search; ?>
                                    <?php if( isset( $_GET['s'] ) && !empty( $_GET['s'] ) ): ?>
                                    <span><?= $boat_data_search->post_title ?></span>
                                    <?php else: ?>
                                    <span><?= $boat ?></span>
                                    <?php endif; ?>

                                    <ul class="shadow-default">
                                        <?php $i = 0; ?>
                                        <?php while( $boat_data->have_posts() ): $boat_data->the_post(); ?>

                                        <?php if( $bval == $post->post_name ): ?>
                                        <li class="active" data-value="<?= $post->post_name ?>" data-title="<?= get_the_title() ?>"><?= get_the_title() ?></li>
                                        <?php else: ?>
                                        <li data-value="<?= $post->post_name ?>" data-title="<?= get_the_title() ?>"><?= get_the_title() ?></li>
                                        <?php endif; ?>

                                        <?php $i++; ?>
                                        <?php endwhile; ?>
                                        <?php wp_reset_postdata(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            </div>
                            <div class="col-lg-3 col-md-6 box-form-search">
                                <?php if( !empty( $_SESSION['destinasi'] ) && isset( $_GET['s'] ) ) : ?>
                                 <div class="select-destination-form select-default">
                                    <?php $dest = isset( $_GET['dest'] ) ? $_GET['dest'] : get_label_string( 'Select Your Destination', true ); ?>
                                    <?php $dval = $_SESSION['destinasi']; ?>
                                    <input type="hidden" name="dest" class="form-hidden required" value="<?= $dval; ?>">

                                    <?php $i = 0; ?>
                                    <?php if( $dval == 'all' ): ?>
                                    <span>All Destinations</span>
                                    <?php else: ?>
                                        <?php foreach( $iteneraries_data as $dt ): ?>
                                            <?php if( $dval == $dt['post_id'] ): ?>
                                            <span><?= $dt['post_title'] ?></span>
                                            <?php break; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                    <ul class="shadow-default">
                                        <li data-value="all" data-title="All Destinations" <?php if( strtolower( $dval ) == "all" ): echo 'class="active"'; endif; ?>>All Destinations</li>
                                        <?php foreach( $iteneraries_data as $dt ): ?>
                                            <?php if( $dval == $dt['post_id'] ): ?>
                                            <li class="active" data-value="<?= $dt['post_id'] ?>" data-title="<?= $dt['post_title'] ?>"><?= $dt['post_title'] ?></li>
                                            <?php else: ?>
                                            <li data-value="<?= $dt['post_id'] ?>" data-title="<?= $dt['post_title'] ?>"><?= $dt['post_title'] ?></li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <?php else: ?>
                                <div class="select-destination-form select-default">
                                    <?php $dest = isset( $_GET['dest'] ) ? $_GET['dest'] : get_label_string( 'Select Your Destination', true ); ?>
                                    <?php $dval = isset( $_GET['dest'] ) ? $_GET['dest'] : ''; ?>
                                    <input type="hidden" name="dest" class="form-hidden required" value="<?php echo $dval; ?>">
                                    
                                    <?php global $dest_data_search; ?>
                                    <?php if( isset( $_GET['s'] ) ): ?>
                                        <?php if( isset( $_GET['dest'] ) && $_GET['dest'] == "all" ): ?>
                                        <span>All Destinations</span>
                                        <?php else: ?>
                                        <span><?= $dest_data_search->post_title; ?></span>
                                        <?php endif; ?>
                                    <?php else: ?>
                                    <span><?= $dest; ?></span>
                                    <?php endif; ?>

                                    <ul class="shadow-default">
                                        <li data-value="all" data-title="All Destinations" <?php if( strtolower( $dval ) == "all" ): echo 'class="active"'; endif; ?>>All Destinations</li>
                                        <?php foreach( $iteneraries_data as $dt ): ?>
                                            <?php if( $dval == $dt['post_id'] ): ?>
                                            <li class="active" data-value="<?= $dt['post_id'] ?>" data-title="<?= $dt['post_title'] ?>"><?= $dt['post_title'] ?></li>
                                            <?php else: ?>
                                            <li data-value="<?= $dt['post_id'] ?>" data-title="<?= $dt['post_title'] ?>"><?= $dt['post_title'] ?></li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-2 col-md-6 col-sm-6 box-form-search">
                                <div class="select-month-form select-default">
                                    <?php $month = isset( $_GET['month'] ) ? $_GET['month'] : date( "F" ); ?>
                                    <input type="hidden" name="month" class="form-hidden" value="<?php echo strtolower( $month ); ?>">
                                    <span><?php echo $months = ( $month == "all" ) ? 'All Months' : ucwords( $month ); ?></span>
                                    <ul class="shadow-default">
                                        <li data-value="all" data-title="All Months" <?php if( strtolower( $month ) == "all" ): echo 'class="active"'; endif; ?>>All Months</li> 
                                        <?php $months_data = get_month_data(); ?>
                                        <?php foreach( $months_data as $m ): ?>
                                        <?php if( strtolower( $m ) == strtolower( $month ) ): ?>
                                        <li data-value="<?= strtolower( $m ) ?>" class="active" data-title="<?= $m ?>"><?= $m ?></li>
                                        <?php else: ?>
                                        <li data-value="<?= strtolower( $m ) ?>" data-title="<?= $m ?>"><?= $m ?></li>
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-6 col-sm-6 box-form-search">
                                <div class="select-year-form select-default">
                                    <?php $year = isset( $_GET['year'] ) ? $_GET['year'] : date( "Y" ); ?>
                                    <input type="hidden" name="year" class="form-hidden" value="<?php echo $year; ?>">
                                    <span><?php echo $years = ( $year == "all" ) ? 'All Years' : $year; ?></span>
                                    <ul class="shadow-default">
                                        <li data-value="all" data-title="All Years" <?php if( strtolower( $year ) == "all" ): echo 'class="active"'; endif; ?>>All Years</li>
                                        <?php $year_now = date( "Y" ); ?>
                                        <?php $year_max = $year_now + 3; ?>

                                        <?php for( $i = $year_now; $i <= $year_max; $i++ ): ?>
                                            <?php if( $year == $i ): ?>
                                            <li data-value="<?= $i ?>" data-title="<?= $i ?>" class="active"><?= $i ?></li>
                                            <?php else: ?>
                                            <li data-value="<?= $i ?>" data-title="<?= $i ?>"><?= $i ?></li>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-12 box-form-search">
                                <button type="submit"><?php get_label_string( 'Search' ); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>