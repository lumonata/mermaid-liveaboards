<div class="wrap-popup">
    <div class="container">
        <div class="spinner">
            <img src="<?php echo THEME_URL_ASSETS.'/images/loading.svg'; ?>" />
        </div>
        <div class="row header-form-inquiry">
            <div class="col-md-12 clearfix">
                <h2>Inquiry Form</h2>
                <span class="close-popup"></span>
            </div>
        </div>
        
        <?php echo do_shortcode('[contact-form-7 id="5" title="Inquiry Form"]'); ?>
		
        <input type="hidden" name="thankyou_link" value="<?php echo get_site_url().'/thank-you/'; ?>" />
    </div>
</div>