<?php get_header(); ?>

<section class="wrap-404 b-lazy" data-src="<?php echo THEME_URL_ASSETS .'/images/bg_404.jpg'; ?>" data-src-small="<?php echo THEME_URL_ASSETS .'/images/bg_404_medium.jpg'; ?>">
    <div class="line-gradient"></div>

    <div class="container-404">
        <div class="logo">
            <a href="<?php echo get_site_url(); ?>"><img data-src-static="<?php echo get_template_directory_uri().'/assets/images/mermaid-logo.png'; ?>" /></a>
        </div>

        <h1>404</h1>
        <h3>Page Not Found</h3>

        <a href="<?php echo get_site_url(); ?>" class="btn button-default">Back To Home</a>
    </div>

    <?php get_copyright(); ?>
</section>

<?php get_footer(); ?>