<?php

if( isset( $_GET[ 'id' ] ) && !empty( $_GET[ 'id' ] ) )
{
    $path = preg_replace( '/wp-content.*$/', '', __DIR__ );

    require_once $path . 'wp-load.php';
    require_once 'export-dsv.php';

    $mod = new batch_download_csv_file();

    $mod->render();
}