<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <?php if( is_home() || is_front_page() ): ?>

            <!-- SECTION TOP HOME PAGE -->
            <?php include_once "layout/hero.php"; ?>

            <!-- SECTION ABOUT -->
            <?php section_about_homepage(); ?>

            <section class="row homepage-content">
                <div class="bg-grey"></div>
                
                <!-- DISCOVER INDONESIA -->
                <?php section_discover_indonesia() ?>

                <!-- SELECT YOUR MERMAID -->
                <?php section_select_mermaid(); ?>

                <!-- MERMAID FACILITIES -->
                <?php section_facilities(); ?>

            </section>

            <section class="row homepage-content homepage-content-2">
                <div class="bg-grey"></div>
                
                <!-- DISCOVER OUR DESTINATION -->
                <?php section_destinations_iteneraries('destination'); ?>
                
            </section>

            <section class="row homepage-content homepage-content-3">
                <div class="bg-grey"></div>
                
                <!-- DIVING IN INDONESIA -->
                <?php section_diving_indonesia(); ?>

                <!-- NEWS & EVENTS -->
                <?php section_news_events(); ?>

                <!-- Recent Reviews -->
                <?php section_reviews(); ?>

            </section>

        <?php endif; ?>

    <?php endwhile;  ?>

<?php get_footer(); ?>