<?php get_header(); ?>

    <?php include_once "layout/hero.php"; ?>

    <section class="row homepage-content homepage-content-2">
        
        <!-- DISCOVER OUR DESTINATION -->
        <?php section_destinations_iteneraries('iteneraries'); ?>
        
    </section>


<?php get_footer(); ?>