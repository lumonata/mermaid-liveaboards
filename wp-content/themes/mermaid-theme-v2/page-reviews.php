<?php
/*
Template Name: Reviews Page
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>

    <?php include_once "layout/hero.php"; ?>

    <?php
        // INISIALISASI VARIBLE PAGE
        $title   = get_the_title();
        $title   = empty($title) ? '': '<h1 class="heading-default">'.$title.'</h1>';
        $content = get_the_content();
        $content = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $content);
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = empty($content) ? '': '<div class="desc-about">'.$content.'</div>';
    ?>

    <!-- SECTION WELCOME -->
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        echo $title;
                        echo $content;
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="wrap-list-reviews">
        <div class="container-mermaid">
            <div class="row container-list-reviews">
            <?php
            $args = array(
                'post_status'   => 'publish',
                'post_type'     => 'review',
            );
            $query = new WP_Query($args);

            $total_posts = count($query->posts);
            $j = 1;
            while ($query->have_posts()):
                $query->the_post();
                $post_title     = get_the_title();
                $post_date      = get_post_meta( get_the_ID(), '_review_date', true );
                $post_date      = empty($post_date) ? '' : '<h5>'.date("d M Y", strtotime($post_date)).'</h5>';
                $post_country   = get_post_meta( get_the_ID(), '_review_country', true );
                $post_country   = empty($post_country) ? '' : '<h5 class="country">'.$post_country.'</h5>';
                $post_content   = wp_strip_all_tags(get_the_content());
                $post_content   = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
                $post_content   = apply_filters('the_content', $post_content);
                $post_content   = str_replace(']]>', ']]&gt;', $post_content);
                // $post_content   = strlen($post_content) > 180 ? rtrim(substr($post_content, 0, 200)).'...' : $post_content;
                $post_rate      = get_post_meta( get_the_ID(), '_review_rate', true );
                
                ?>

                <div class="col-md-6">
                    <h3><?php echo $post_title; ?></h3>
                    <?php 
                        echo $post_country;
                        echo $post_date; 
                    ?>
                    <div class="star-review">
                        <?php
                        if(!empty($post_rate)):
                            for($i=1; $i <= $post_rate; $i++):
                                echo '<span></span>';
                            endfor;
                        endif;
                        ?>
                    </div>
                    <div class="desc"><?php echo $post_content; ?></div>
                </div>

            <?php
                if($j % 2 == 0):
                    echo '</div>';

                    if($j < $total_posts):
                        echo '<div class="row container-list-reviews">';
                    endif;

                endif;
                $j++;
            endwhile;
            wp_reset_postdata();
            ?>
            </div>
        </div>
    </section>

<?php endif; ?>

<?php get_footer(); ?>