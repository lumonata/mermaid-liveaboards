<?php
/*
Template Name: Page Mermaid
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php
        // SECTION TOP HOME PAGE
        include_once "layout/hero.php";

        // INISIALISASI VARIBLE PAGE
        $title   = get_post_meta( get_the_ID(), '_page_title_page', true );
        $title   = empty($title) ? '':   '<h1 class="heading-default">'.$title.'</h1>';
        $content = get_post_meta( get_the_ID(), '_page_description_page', true );
        $content = empty($content) ? '': '<div class="desc-about">'.$content.'</div>';
    ?>

    <!-- SECTION WELCOME -->
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        echo $title;
                        echo $content;
                    ?>
                </div>
            </div>
        </div>
    </section>

    <!-- SECTION MERMAID CABINS -->
    <section class="mermaid-cabins">
        <div class="bg-grey"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="container-mermaid">
                    <h2 class="title-section">MERMAID I CABINS</h2>
                    <div class="line"></div>

                    <div class="row">
                        <?php list_mermaid_cabins(); ?>
                    </div>
                </div>
            </div>
        </div>

        
    </section>

<?php endwhile;  ?>

<?php get_footer(); ?>