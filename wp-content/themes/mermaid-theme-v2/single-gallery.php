<?php get_header(); ?>

    <?php if ( have_posts() ) : the_post(); ?>

        <?php 
            include_once "layout/hero.php";

            $post_id            = get_the_ID();
            $photographer_foto  = wp_get_attachment_image_src( get_post_meta( $post_id, '_gallery_photographer_foto_id', true ), 'thumbnail' );
            $photographer_name  = get_post_meta( $post_id, '_gallery_photographer_name', true );
            $photographer_bio   = get_post_meta( $post_id, '_gallery_photographer_bio', true );
            $photographer_bio   = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $photographer_bio);
            $photographer_bio   = apply_filters('the_content', $photographer_bio);
            $photographer_bio   = str_replace(']]>', ']]&gt;', $photographer_bio);
            $galery_images      = get_post_meta($post_id, '_gallery_foto_images', true);
        ?>

        <section class="photographer-and-title-gallery">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 container-gallery-title clearfix">
                        <img data-src="<?php echo $photographer_foto[0]; ?>" data-src-small="<?php echo $photographer_foto[0]; ?>" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="b-lazy photographer-foto" />
                        <div class="title-gallery">
                            <?php 
                                global $post;
                                
                                if( strpos( $post->post_name ) === FALSE  ):
                                    ?>
                                    <h4>Photographer</h4>
                                    <?php
                                endif;
                            ?>
                            <h2><?php echo $photographer_name; ?></h2>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 photographer-bio">
                        <?php echo $photographer_bio; ?>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        $all_images = array();
                        $filter_images = array();
                        if(is_array($galery_images)):
                            foreach($galery_images as $gi):
                                $album_name             = $gi['album_name'];
                                $album_name_sanitize    = sanitize_title($album_name);
                                $album_images           = $gi['album_images'];

                                if(is_array($album_images)):
                                    foreach($album_images as $key => $d):
                                        $gallery_images_url         = wp_get_attachment_image_src( $key, 'large' );
                                        $gallery_images_url_thumb   = wp_get_attachment_image_src( $key, 'post-thumbnail' );
                                        $gallery_caption            = wp_get_attachment_caption( $key );

                                        $all_images[] = array(
                                            'album_name_sanitize'   => $album_name_sanitize,
                                            'album_name'            => $album_name,
                                            'img_url'               => $gallery_images_url[0],
                                            'img_url_thumb'         => $gallery_images_url_thumb[0],
                                            'img_caption'           => $gallery_caption
                                        );
                                    endforeach;
                                endif;

                                $filter_images[] = array(
                                    'album_name_sanitize'   => $album_name_sanitize,
                                    'album_name'            => $album_name,
                                );

                            endforeach;
                        endif;
                        ?>

                        <div class="wrap-photo-gallery">
                            <?php if(count($galery_images) > 1): ?>
                            <div class="container-filter-gallery">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <h3>Filter Gallery</h3>
                                    </div>
                                    <div class="col-lg-10">
                                    
                                        <?php
                                        $filter     = array();
                                        $type_list  = array();
                                        foreach($filter_images as $d):
                                            $album_name             = $d['album_name'];
                                            $album_name_sanitize    = $d['album_name_sanitize'];

                                            if( !in_array($album_name, $type_list) )
                                            {
                                                $type_list[] = $album_name;
                                            }

                                            $filter[]       = '
                                                <li data-filter=".'.$album_name_sanitize.'" data-value="'.$album_name.'">'.$album_name.'</li>
                                            ';
            
                                        endforeach;
                                        ?>

                                        <ul>
                                            <li class="active" data-filter="*" data-value="All">All Photos</li>
                                            <?php echo implode( '', $filter ); ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>

                            <div class="grid">
                                <div class="grid-sizer"></div>
                                <div class="gutter-sizer"></div>
                                <?php
                                if(!empty($all_images)):
                                    foreach($all_images as $d):
                                ?>
                                        <div class="grid-item <?php echo $d['album_name_sanitize']; ?>">
                                            <a href="<?php echo $d['img_url']; ?>" data-fancybox="images" data-caption="<?php echo $d['img_caption'] ?>" class="fancy-group">
                                                <img class="lazy-gallery" data-src="<?php echo $d['img_url_thumb']; ?>">
                                            </a>
                                        </div>
                                <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <?php endif; ?>

<?php get_footer(); ?>