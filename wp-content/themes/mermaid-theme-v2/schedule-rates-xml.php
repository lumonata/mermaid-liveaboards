<?php 

require_once( '../../../wp-load.php' ); 

$post_id = absint($_GET['id']);

if ( $post_id > 0 ) {
	
	$post = get_post( $post_id );
	
	// Start building CSV content with headers
	
	$prefix = "_schedule_data_";
	
	$args = array(
		'post_status'   => 'publish',
		'post_type'     => 'schedule_data',
		'numberposts'   => -1,
		'post_parent'   => $post_id,
		'order'         => 'ASC',
		'orderby'       => 'meta_value',
		'meta_key'      => $prefix.'departure_date'
	);
	
	 $posts = get_posts($args);
	 
	 header( 'Content-Disposition: attachment;filename="schedule-and-rates-' . sanitize_title( $post->post_title ) . '.xml' );
	 header( 'Content-Type: text/xml' );
	 
	 $iteneraries_list = the_post_list('iteneraries');
	 $iteneraries_text = '';
	 $html_content = '<?xml version="1.0" encoding="UTF-8"?>';
	 // $html_content = "<title>". $post->post_title ."</title>";
	 $html_content .= "<data>";
	 
	 foreach($posts as $d) :
		$post_title         = $d->post_title;
		$post_id            = $d->ID;
		$depart_date        = get_post_meta( $post_id, $prefix.'departure_date', true );
		$arrival_date       = get_post_meta( $post_id, $prefix.'arrival_date', true );
		$depart_time        = get_post_meta( $post_id, $prefix.'depart_time', true );
		$arrival_time       = get_post_meta( $post_id, $prefix.'arrival_time', true );
		$depart_point       = get_post_meta( $post_id, $prefix.'depart_point', true );
		$arrival_point      = get_post_meta( $post_id, $prefix.'arrival_point', true );
		$iteneraries        = get_post_meta( $post_id, $prefix.'iteneraries', true );
		$no_dives           = get_post_meta( $post_id, $prefix.'no_dives', true );
		$allotment_master   = get_post_meta( $post_id, $prefix.'allotment_master', true );
		$allotment_single   = get_post_meta( $post_id, $prefix.'allotment_single', true );
		$allotment_deluxe   = get_post_meta( $post_id, $prefix.'allotment_deluxe', true );
		$allotment_lower    = get_post_meta( $post_id, $prefix.'allotment_lower', true );
		
		if(!empty($iteneraries_list)):
			foreach($iteneraries_list as $key => $d):
			   if($key == $iteneraries):
				 $iteneraries_text = $d;
			   endif;
			endforeach;
		 endif; 
		$html_content .= "<schedule_data>";
		$html_content .= "<trip_code>". $post_title ."</trip_code>";
		$html_content .= "<departure>". $depart_date . ':' . $depart_time . ':' . $depart_point ."</departure>";
		$html_content .= "<arrival>". $arrival_date . ':' . $arrival_time . ':' . $arrival_point ."</arrival>";
		$html_content .= "<destination>". $iteneraries_text ."</destination>";
		$html_content .= "<master>". $allotment_master ."</master>";
		$html_content .= "<single>". $allotment_single ."</single>";
		$html_content .= "<deluxe>". $allotment_deluxe ."</deluxe>";
		$html_content .= "<lower>". $allotment_lower ."</lower>";
		$html_content .= "</schedule_data>";
				
	 endforeach;
	 
	 $html_content .= '</data>';
	 
	 echo $html_content;
	 
	 exit;
	 wp_reset_postdata();
}		
	
	

?>