<?php

require_once( '../../../wp-load.php' ); 
require_once 'includes/dompdf/autoload.inc.php';
	
use Dompdf\Dompdf;

$dompdf = new Dompdf();
$query  = new WP_Query( array( 
    'p'              => $_GET['id'],
    'post_type'      => 'checkout',
    'post_status'    => 'publish', 
    'posts_per_page' => 1 
));

if( $query->have_posts() )
{
    while( $query->have_posts() )
    {
    	$query->the_post();

        $trip_code = get_post_meta( $post->ID, '_checkout_trip_code', true );

        //-- Trip Data            
        $trip = json_decode( base64_decode( $trip_code ) );

        if( $trip === null && json_last_error() !== JSON_ERROR_NONE )
        {
            $sdata = get_posts( array( 'post_type' => 'schedule_data', 's' => $trip_code ) );

            if( !empty( $sdata ) )
            {
                foreach( $sdata as $trip )
                {
                    setup_postdata( $trip );

                    if( $trip->post_title != '' )
                    {
                        $trip->post_meta = get_post_meta( $trip->ID );
                    }
                }
            }
        }
        else
        {
            $trip_code = $trip->post_title;
        }

        if( empty( $trip ) === false )
        {
            $dep = strtotime( $trip->post_meta->_schedule_data_departure_date );
            $arv = strtotime( $trip->post_meta->_schedule_data_arrival_date );

            if( date( 'Y', $dep ) == date( 'Y', $arv ) )
            {
                $depart  = date( 'M d', $dep );
            }
            else
            {
                $depart  = date( 'M d Y', $dep );
            }

            $arrival = date( 'M d Y', $arv );

            $dppoint = get_post_meta( $post->ID, '_checkout_departure_point', true );
            $avpoint = get_post_meta( $post->ID, '_checkout_arrival_point', true );

            if( !empty( $dppoint ) )
            {
                $dobj = json_decode( base64_decode( $dppoint ), true );

                $dpoint = $dobj[ 'point' ];

                $dtime = get_post_meta( $post->ID, '_checkout_departure_time', true );

                if( empty( $dtime ) )
                {
                    $dtime = $dobj[ 'time' ];
                }
            }
            else
            {
                $dpoint = '';
                $dtime  = '';
            }

            if( !empty( $avpoint ) )
            {
                $aobj = json_decode( base64_decode( $avpoint ), true );

                $apoint = $aobj[ 'point' ];

                $atime = get_post_meta( $post->ID, '_checkout_arrival_time', true );

                if( empty( $atime ) )
                {
                    $atime = $aobj[ 'time' ];
                }
            }
            else
            {
                $apoint = '';
                $atime  = '';
            }

            $btype     = get_post_meta( $post->ID, '_checkout_booking_type', true );
            $itenerary = get_post_meta( $post->ID, '_checkout_iteneraries', true );
            $remarks   = get_post_meta( $post->ID, '_checkout_remarks', true );
            $boat      = get_post_meta( $post->ID, '_checkout_boat', true );            
            $banks     = get_post_meta( $post->ID, '_checkout_bank_options', true );
            
            $html = '
            <style type="text/css">
                @font-face {
                    font-family: "Work Sans";
                    font-style: normal;
                    font-weight: 400;
                    font-display: swap;
                    src: url(https://fonts.gstatic.com/s/worksans/v11/QGY_z_wNahGAdqQ43RhVcIgYT2Xz5u32K0nXBi8Jpg.woff2) format("woff2");
                    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                }
                @font-face {
                    font-family: "Work Sans";
                    font-style: normal;
                    font-weight: 600;
                    font-display: swap;
                    src: url(https://fonts.gstatic.com/s/worksans/v11/QGY_z_wNahGAdqQ43RhVcIgYT2Xz5u32K5fQBi8Jpg.woff2) format("woff2");
                    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                }
                .page_break{page-break-before: always;}
                .tg{border-collapse:collapse;border-spacing:0;width:100%;font-family:"Work Sans";font-size:10px;font-weight:400;}
                .tg td{border-color:#e6e9f7;border-style:solid;border-width:1px;overflow:hidden;padding:5px;word-break:normal;color:#555;}
                .tg th{border-color:#e6e9f7;border-style:solid;border-width:1px;overflow:hidden;padding:5px;word-break:normal;color:#555;}
                .tg .tg-0lax{text-align:left;vertical-align:top}
                .tg b{font-weight:600;}
            </style>
            <table class="tg">
                <thead>
                    <tr>
                        <th style="text-align:center; padding:10px 0; border:0;" colspan="9">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMMAAABRCAYAAABv0RLiAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NjU5ODdERjQxRTBGMTFFOUE2RjBCMTY0MTgyNUFBNzUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NjU5ODdERjUxRTBGMTFFOUE2RjBCMTY0MTgyNUFBNzUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo2NTk4N0RGMjFFMEYxMUU5QTZGMEIxNjQxODI1QUE3NSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo2NTk4N0RGMzFFMEYxMUU5QTZGMEIxNjQxODI1QUE3NSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PrS32pMAAF2aSURBVHja7H0HmFXV1fY6/fY6d3ovdIYZem+iYm/Yu8Qau5GoUWPUaBI1Krbop1GDvXcURQEZOgMMUxim97m9n17+fS5YkBmKgN/z/WE/z7HMvfe0vd613nftvdfGNE2Do+1oO9oAyH1/rIGm8bSqhlyaxjkAMCr1R9ARpJnQf9P6/+M4BpIggsA7/TZnVtNAZ5LE6nGisPwqDLPEMYyS0BkE9FMBQw2dF/QDJymI+mQs6nMrZWNPWQIY1nO0i462/3UwKEqPKxl/7FlV6avQtHiWpskMMlj8B5CgRqAD1/9CEhh8uaQHZG124PwbF49Cf/f+8nwiv/RMjnsPgcG0B9h2AUFLnZkkMVj3RQQ6G53gKZrCOlyexUe76Gj7rRo+6AeYWdDUvmEIDEM1TbQheCAwyCgyKLsPFccwDWhKharPI1D9bRRI3DQouFQtUYphduTsLT87rLv+jdvAQFtg3ZcabFytAQsOFCUY/FAeTJW5DAQyw//Wi9U0wY3+SR3x66girUpx5//ac6oCpam89Te5lsKakFMmf3MwYLgjSdOVX2CYuDuAED8Eg13/Rq6cRl299os4dG8UId1MgyMtvxl96N/7bDKoajgdS/0e+8WFAAFBhc3fJqC3RoIMBLt4ggNMUw9BzGgQanzzNj6ZqDiCfYMN9v40LWSOdrzxoCqD+UgbiMLtKA/sfP/GI2yGg9pJov/j86K99Wf9FmBI9L59G+vfMeQ3B4PeKObk1wmM4TFkzD99dZcxUwgINd+z0FYtgdtGgKAQkJZT1Kk75b28tBpDqO43AvYDGHYDAskFCtdgx7okNG6UYdooGkwMBok4j2iafAiPFTTyvtcviHa3zDli5iHHzGJi02XoPy2//IxPvH5TomPVfBUw/oiDQds0Ldb86aU8r9FH6hpiYtUZstg1a2+X40+Pt/3rkUQwbjnyUFAtcvKLq7j+xon/K2AgiKxmwKwt2o/2je2OGjjIIQGCLSIUZtIgyRo4bQaw2qyRQZwoiYDA7OFUERAwBAQtyoKvTYUxxSQS4OjvKoZCr6LHxAMxSWYg3SPI1eNN9taceNfaOQN7bsW0v2ffb1ggbQk5/tb0ePDpFeh+S35MFMgNY8TkknspFSc0DQ6TgWrIiyimARMT2oZ5RqqxKNLdOHYAA6LQsxoP+fKy0M/2LfyMT264+eehnRc+vpcQ2zNUWT2MQFQMu573F7egtEzFjE25Its2d/A+1fAjBgYMdwpAFH+LrPOnrxM44LgKJMdDkRMDTtDQoQInKuiGNGwwoY44j+HH94hjyIIVYGIJwAQFKssUQNoZgqwGDEmAgYBdCav9vTapM5cPv/KiLHpPRf/7I9gkYeVJlkx0GXb1RI7TCn80DfAOTfS88I9A69pFA0Wwg20GU+UbePhv4xLx2zaqav+CFBD5Zx6hIchohErAYQMDpnDh9/4gxL94VFWVoT8aiNyRpWo7JzqyWWB71h7/sxygSdW2nRNsePrfXDSUdcge0zS9iuClrVL4qsc59sUP0J8cstI8XRGXXEPRBKgKzxwuKMiCt4gLLnlJFntPhpTN7I5O4oZzaAsHUmzjLDahOH7qU26ImPj40WDj+1dogB1Snx6AGCHsPzpzhAU8kgTRywMpIYpj0sAbUBBvlYFXSaCMtuhgkRb9PKHtBgLFCyDVxyGG1IXdpYE9F4OmDg0ZEgBN4pDjNOwlLQa8M6qgJRlr6FWCT33Mu2bV0+bpr1NM8edyaNOxySYrpLnr7HHv8rPwXH6rwK+6ifCtOLFza3k075gLpx6WwG2cvZqUcuqTNUtH+G3NS6wFpTd5GzZOsSRooJ2iUUulnvX0tEhrGG9QuZYcSciQjI6c5oO9FkFPfCPR+bt1jOPVmzHb+K8ZZuazqlaTl2wJppvQ9SzUl8cmEqM/wPDV52jKtgv42s3FcXnRPe7hntZDTjnSJogQp31g9T05PdT25OlC7oaRYGLFYG2MyDCooGHcbqOVMOShDYAFrVLMl02aRjfhJJk8qGsx2Q2sr9urhk/6lHfOaiTNI9+k6clVqrD8+Hg9DVaqMT/p//p0klFiPL/hclLYeEJwPd9rq3hpMnaoLmdfg26aGsej4auR1wuMRZYHeJSH0OYERAIYmKwYePJV+PY7DWIBCViBgrPuXHJ3duHQv+59nghEwgvXaWpiEo5CgFQThRgiX2abBogpgbNQhY4ECQ1VAEl0DQ7S4LRF/7nZ5cl6cn8PgEQyk2w+5xs7VjO9L+QEzGERe1qA6K8niPFTBbAOp0WF4AmzGCG61uQCNf61y/JL8l49LGBAR3zDbX9tWbvsrpa4ATw5CsT9RnAaJSg/0SZr6ZWrNSWE43jUjft6C/pqyxrTp/3tMrsnq/bXXC/Q8c2F5sjvXhM5GkKiVSNstFxfhVEGEaDiWEnV0nAZwwXa0BeAlpZLvio55aETDdShR8DUtbsbSqS1l1bXNIFNNMpgthKQ9NIwYSYLzJAxzbjJuVNVgh4KC3v42v70mHjxP4vm3Phn7FdEYIFL0vGmC752YZtm+sJ2EAwWjeUJbMcqAkZXipAxlpQlTCAYjMO4rQLEnS+eVjRxzieHDPr9J0wILcXvJeT9mxKQDBAwvFABUVGB53AoQWx5GwKHiji+IgtZA+d2dtM6FFloloe2HRKIAg7ZeRryBCqokgoeJwGdCGD9/ejchAIHmkoymC1C2Hrnzd76G1bWbKHNEqnRDiMBhR4Voq0kGKIi7cwTwd9NQ9h9+5vjDxMQdF6rQm8uZDCMJxuF9z4aRTwNst0qCBIJyYYYaVO+mk3oSemuMHS0nbAuc9Y/zrZ7HN2/9oqO3HmvR4PzTwjVfH/hll4aM5sxyo0UgREdoU0EnpUn0EYXC7U1I32uubfceLiAoEHSyLgiOWqOJVoQi9liKBAYFfSs6SrEO4xgoreV4plqqZEUIFqroihy5x2Fsy//+6/11IzRLIYdd98crL96ZUstYe3lcMxuxqEAvVupD2nLNRLpQI5H4TnoSlzx7KhjDx0I+wcDRqsYZpB0cOO8AnJMhR1dCgSiKuRlkWDN1CAjHclrTEPGLQIbj2YPciYjejFOfayZYWX4ZmMUttRxcN3lLpg+mwKeYsCEwGa300iUoQcmVTiYF5leOGNzR/epTw0p+PAOmbdAmkNG2kMFmlKAYlJhHDqEU9lhx5388KG8LInzpytK/TQFq5+uqdum0cL20cZY1IQja3TkCsAgyqAo2K4xeqT/jRENwjt4aBevfGfIiXcttFjIxCF1lq6l0q++x15UfeJIUJ0mGgOHVUll5PTxHsoqQziMg1pw42PZeZ6dv37sQMYFtm0MYDsnKNqmqbhSPc2Y7CihCGQNThLy3SKiThpyfpiezgIsjIEBE6BlpzkmFT10xdBpx79/qJQlLXfClq7uM58pKn3zDmufHdLsKBqh90ujfqUYpDFNHLR1l3gzJl79Nwo/PO5tn2DQtDipqkGDLhYIUgJZxeD91V5k2QxccYkD0nLIVF6RZkQkohWQRNE6WNYH2Qfy+8hropeXjCvgzi2RC0dboklD2C1YbGDgkmAyyLtiEdIVBz5lijPIyns3Zg7ZeLmGqBYpicBYkCXSyClaJVDQDUpGE5Tm7TRRwn+eUamz/4jTxrW/5mVF/Z0ZzWvevMDfsfaUnh4fk+8xwqh8N2LKBKTbkVEakFGaEGcxKqAyOMiiCC3yJWuGn3TPxRYjiIeeRu0YY3K99DCkS05TggADAjqJrgUGdCAgSDYCSNIK+dSHN4kxR5K2THsBcJAOPhJo0NOwcWT3tneu7e2srUhEkzBuiAscZisyRA086FlpswyADr1nMXT4+ozAFz955cip098/dLMUjIr6wfWZQ9ZcpkkU2DURTKhPsR/6FNFrwUBD2nQpgySf+x+FveIOwpS39VCvStx33337AAOPCdwnVyFzysQ1FWJtKrTt5GDo1LmBOfMsKGD5XAzqAB/i/129ApSNn9uXVTDk33ufRzAI/CfX4hhvE/tFqN3MQ8UxF3RPmG75jMc7xqmYEQhEOoR+pB1aRUjKDFTOWfClyWJdv68uk+TvTgr773sv2f7eBUR73BLwmiAq4sBJBPqMQHaAAU4BUCSDPIoAsra2QBPXXkbizhwgC6tR6IsfzMsy2bN92SNOftedd/yLZmNxTc32GL6zwZ/BK4SxNURAnKOAxpGRoo5TPQRwbis487AsCzmuG6cd1b++m2LZLPvyw+Hevz4PzduGip1G8MdIYKVdz6ooeoYPQ5STQM9pQP/dZ5WUb07EpLaTCbKwEwjXQQl2DCM0V87ompyRZ7/g9Ez6KBoiI+vW+z2xIOv28RTWjaKPKNGIHiGam6GA4DKClsGA223KRGL3U3QD7K9NXGnazgXh4L1vxVuXnE+1Ry2xXjOEOAw9K4HoJ4F6FANCvy5lABo5aQ2vK1XE5QtxRXThdFk1Qgx7RCLDD6asGx6hIC4vqogKAIwYMqzD5AlvYxJbSlRVg8xcBuS1IvS173BUzjhZT7MJA50Dx1RIhBRIIIMtKB2xhaDX9eGcACqJzq97dBT2CfSdCKcATgwe+2QxTnRv/9fNgdavb2rZ3u9o2yEkUdAxF2UhHuEwQkzUUJTBIcNlhsp0BbxtLGxvC0FtWJFlYx9XNGT7hZVTzisdPfma6wwWZ+NB9Ra6LU9hcT86low7beGSbe9eszhaW3XDhm4SOn04dIcZGIe8dnYPB1S6BERhLalIt79ASI/hQOW9cNCjrsH60q5tixeHuhsmN2wMRPt7ZNptjVmzs2xYGAFA0UOumYaSTANkSgI0NAagNSwpLUmZtaS/VjqyYstzoyde9VDZmNNfgIPkLhRDaPkVU7bqx6zuba82vXvVxp4+yVLTp0Knn4JAwgSTEuikNOq7AhT3M9+eoXDK+4TlztMQm4gcVDTSVMrX8uGivh2vXd20qdneUi+wyP+aclwYWNPNEJA05NQwyEwzwngEvEAHBy09UdgeEIUQeMXsorYrx06uGzZh7p+uN1pdLUcADPrgh0ZhiADjMRFCYRVYxImNrvwYKKamPi8BzlILuNMksCDjC/v7dAGt54B/OVGPSFFe9HQBHzIQkwtyS8dtVOl4jUJ8tXtMQUNAQ3QM6Q9ZRuAgiEGJEk4wkF582UuZpde8UDzJa5D5uJ2TNFNX1eN3Jdprzu3wA7Ao0nBRGjAvijYhZ7LgjMcuOdlibdAwgnV70uPIiykUYzwk6iLzfeZgX+NxsRi673AcGCsDYR8GfegdNcad0YBga/axPUVM9jpXyZBLnp849yGzI3PK4wclJs25PfkVfz2veDyjDp/ZbRREkeltrxnPVj/5ZqJfNvijEiKKCKFxGtpjHPhzz399wjkn3z9KlCSL1R5Pz8rkTUYNOVR9DAj71VNcEt6qecEwa0lGMKAQbcIJKwR7NdjmJaEunFNHaElDCPPlOQuemzm6su+T8un3nkUZ0v0HHI2QEdjTZz9ldc97InekzzRLTjhEFHF3Vr3wINXz/Snd6L3GkUOW4hSQAYDugNWXf/ojF8wx27qQP+Y8GdlJp8ukUQYTf4Qig2bVMMWNiUi5J1UIRRBvQ2HYlV3STdnK30p6X73HaJNN1jQ9i4LCMx9xIlaXh/8CDLJUV6pqkXRMUqF1JxKalhxwuNy9OF2xCSgLjyS4QcNJFOF0vYCDgSb1cMgMDgZaMTk9Kc9jsFt1qpN66b6GHM63YzMoEQmM6W5flolzflefoGwOK3XerCk7TWZLw2Edv5e/PYlOdg31h3BQsqatrRimck11m+d+tSEBlfPO23Dadf84ztfZndnZ3F7Y01lfUbVy1bBZxxeWWBxZB+y5KIONQ4yASwHDNiwlwI3GcNr3a0WSQLQ9EZb4oSPcQiQesde3RGH+/KHiuKkTdh7W54QYxWCfXKogh+hjjfG0ijM+HMZUn7Fxe6+1SzHBqX9+4gpnet6Wnta2svamlmE1O7bMVqllCybMuui5A+dmhGywuWMpOuq0J3/o00ing29ukoHkVJ0+h3OySNv3DWHkXFX6xIqRjelZOd2H6yn3R5P0makELoigCRr4ERgIxJNsTlcDZc1tJ40ZjcmIr9KTQ4DDgUOET2CSKJgYek87Fvgvz8FxGWcDEnR1CJA7dmTMZCK/BdWoEBjOqxpm+CEdrYtnuyk1cGs/uEdJgs20vUTlkdgqnLjzpFv+fqy95YnrTfT7t7/yrY+OhKNFCAy1h89AFAjVvXdtlzcNCk+94aHpp134Z2OspiSLumJj1dZ+qzOE95nR2y0qzu3XD4Dp61JkUVMPOfdhdicK3Q6R9LaqMOqUWx6ed9KMb+Mrbvhs+44e+/oN23Nmn3Z4oaCKn57Ss6l+fNQ0a9UJt//hthFjxmwStjyOmMATl7y+hklc6nL5HFZacowZWj8SHQAnfqCoqakR2E+Z9V83kuPO7Mzv0mRQGHftCbf867Qy6bPb7cYXr3n07R5HV0dXzuEEw/46RtNDK6HKyLsjlMYksKXlgM3hqtZxpBgmfhGPCUAgEZ2RSYPKBoCPBUbt8TiK1yhLNSdTBOKYXQr44xrklgxfiT7qUHSpAKSsZ5kISgaMQpqBSCFQV3EH+RIVs5mIFMYMI70Lbvvn6Tl5eZ2W6Q8smrPg2OdnlnMQjbL5h9NA4v7Vc+obc+mpN7435bgFF/7JRIGMucsbC+ac9NToEkQFLRn9ewtTXXPgh5z7J4idWaqQAMeEK/59+uXX3G/KHLk64/iHzjn3FLcc6m/Xn5M5fE8q4rXL15zJu++79fS7X5qjAyEVpUZe8NfhFZms1WWL0kZLeK97xFNTIw5xGaVgN5v8Raxs7Zlz7ZNnjxlb3moce/P1004/dskx40To6uo7rH26HzDo6NZwXdTqw2D6wJo7PVM0WWypFWgZpUW1NkYAGRl4eiEJpBiEUG/LtD2FUcKFZLOHVBXo70QIp42QVZgCg05jY1SMi+IyAhviSDiJwiESzhSFaBJ2cA5UTu7MqW9Ig7m/f/rMnJzMXXSINAGMeuy6iSfO+pSP91ceVoak5nVMOPPhEwtLctft8UHxwsfzh+cE0jMzuuEItf7anrEJ2wVLT75q0bU/5tidk5eNOXPRhXlp/ZZIlE8/XNcSON6cVXHXn6eedcXjFhP9E5DpjJ2GIec9VVCYxhoNVOLIPGlPXstGThx1zuLTR46bsCNlM5RJgZGPXnXMWccsi/dvLz+cV9snTZLlrlJVTXpIUgMR+W8JuXJENUTEZFLzTSw5ZTWE0cnLSMg5szEkomXorV85uah8pj7ekEpbitKmKYBJLjUmQ+MOEWhEoRzujM5d3D8nSQTcbbSxsUTOdyM9ouyaCzjAsof9jgFErbbS4++/trAkb82en9jUgskPXdnb3XdY59w7M4oHmfOTH8gbf+nfFENmx5EwD03lsRA3bu30i07/yEBjeyQAqNyr35lzhkMW2ZAD7Nldh+N6jNEW96BjoM8shac+M6xSsWBweEa6f9ki3qiJKV70u6GTpm7a8xMrX3bs05dxa76box28qfxKMEiby5FYAMxAApdQgRU1JOgsulpP7AqFFfUYU/EF4OvPZKwIDE4S+lrrikVJHEdT9IoUGPhvL6QJFBU6RKjdEYNhY2cGnZ7cDbt4Aw60LasZ+G3zFE1N0Qh198OpB1mowJU1tNqdPXBYtrlyvRZH9r8OncMeWBsy/nfPajhJHJGTYwwMn3LuKwQxMN0aMensD9Crw+A3aGZncdesUy67X5+tjGHYYX+vtrSKTfZ0fMDzkkx2X/n0BW+mMpHY4XlcfN9eKI7jmL62ABl5j4oihQIWq1P3eMHdvgjRmlFf4kIylQlKy6VAiPVCzNc+P8XilTYnOsYzBA0NdSKICEylY6ZtpinyR6+Jm4v79UFSDeFSQ/ajobBDU8SuxdUHNVAE++Sou7n6b1IKxGT3cGar84hQB93oBgPCrs/1ATj8Nyp5goHDmeZD93REIgNOEOq+QEZSjIZhhw/3+wYDKC7dhpKI1GxvFoHEKcjKKW2Hn4VFjCjzyW4L6JjJG4aDAULQtPHT03ZFlvpRmhbPlTgM+jo08GSnw5Cxs77ecwBC6sQ0fVCPT9XeIGkcBCmBIkoCjraj7bds+17cg8kZSlKGhF9OrWazWCzgzi7u2BO9WZsVi8urIuFrs0tgsFPQWr9qmMzzYyTp+xMI5O31Kd493SJYHHZwpWVt3yMHZK/o4dxOoOJJdIjAKvrwtT4JTDnaO0fbEW4qrWmC/YDAoFMLJamBkMRA1sWzxYxokqN3j1BFjugm8ZKtAvLoekQrziNgRFoC2MTLt4ni5osZ2gT1NRy0NkXBZsrgjRbbHlkW1ZDVwpoyEjJhBIxTQEkBDE8tLT3ajrYjkoTQknk89/HfYrE7t3O9F+2U+quO2a+ARligCQmATarQ5eMgP98AtMG8V2EvHHPViiRxvEKRkDvRkFr7IML7F4M+CwDphA1VcTAhgVxWPqOKIom6PcBEFIQozBLDiIBFn/yhTzhTVb2axtHIcLQdbhDEC3jukz8I/NJLFLXNZkBSp+ubAAiOdaPKT5u2fN80SZMYBhlmDHnskJ8DmnAAZTC2/fJ7BsMJn+qVLxT9bE6kAnLMqWlNNENBbzMLLQ08OD1uyB81Yd0AGkwiMZHTSDw1qiGq+oQ+/caP1r082g5byxTFVQ8mIldu5hKLr1eVHhuD4oC/EYfPV2LQz5HKfiODpHAkgZGQ5PSFMjhklgzvNzDMXmDAcEcA/VMSDVaKFmMp8qXqC3aQKF76cQRkXgZbcRZ4cosGmkqMSSqNK7gFKDyqF8cARZFAkQTyaB8ebYfW5DJV/u4WjvvkAi5RbSeQDROUDRROgu4mBdaukqGtj4WZGQXx/YBBRSI2WMSkFjOpYEVYsrkd+o+ie9MpexuO2RoU4MolGhm1xINBUGDFNyx8v5aFDEoDpyNTM9sc2/e+DhXHwOhXMaJIMJoBiZRUzSRJ4p1HO/O/icPIBkX1ngxqOF+XqprmacLIrOXYXssBDqSxWbL07eOJvv+cFe/fToqJ0p3dXW6JsATTSisB6V6ArnYNOhpZAF5PDDl8+wSDqvptmOzLJdJo8LM8MlIF0R6zPudFr4Qg7JlRymEpaux2QVheruIG0Iw0dGyMwRvvJkDnZQgLUDhq8g6aIuv2zliZgCBLemS5G1QzDbKcQAfSHALrPmoh/yU5HdVflIj9/Q1F3jaZ1DiI9kqwaiUOBcNveGLasRfcctDxIPLNgqaqh87VYMZ3lrzrluSNmvUykfnN9bVr7nqqawcJoydiwMYU4BMC0FaH4MwqaN8nGDQ1nAl4KIM3WiEYSwAfFkBTCH18YWBlixm8ejRJ1VxE/D+Q1EAWZKBBQ5rCCFlDyvWllgPONSfI/FWSpJxB0LSuuUGV9agkM0fN5L+jJePPPKMoqyZrpA0oZF3xVgz4lhAkDWuOA7hAn8J8UOtOMPPxr2RMmLzU7va0EsSuAcH8IfYe3GiApB8DWSNBRJYoiBIYbc6Y1eEOpZz64KckkaowkoqsAZeUwUSSYLal6XOKBhwNY5gZb2PAgLa7BCmvmz0Swwakg7My8yCzaNiKwa5E4Ok9ul7GaQwQHlIj3ZqqqkfN5L8hw5PMwbD6mZF+I0C/ClyLCk1bZOhpTwBuyt96sEBI2RPFxF3p6c0/ACEVLZTQNJyQwWrXQBVVcJgR4CRRny3QwxjNwX1GBlnemaNpHCUJOEKuCjazGWxp6Z2D30BpD4oOLK7FTZjGAC9gqYXlFmTkzuyihCMtY83gUMZD+tpWfXyBYvT1tSrSDAJ91FT+CyiSLMpbvw6IDRvC5phPhJouDpK+OEwcUSKMmn3OE4frOorSW56M6U4d0XhJA0VUIBLlYNKQ8lqCIOR9RgZF6R4NmARcAkUGxK/02aaMyRYY7Ps47uwhyIKl6m6qJMvIuFFksFIU5I2bVU0T+KCruwiyrAvAyOp1iWmk2GUEBlHgs+CA1mgfbf+XG0E5vX7vuE+31URhybJWWFPdC72iXZhz7YO3Z+YWbjxsoNOSlJDcVW5HQaLZF5BBQXGjZOTYHyulDB4ZpIaRBE6nyrrwPAJDmgnxK3t48MtRQNGV1by05SwNQyFI1CkSgBlJ9+zhlev2daM45ujCMHMXYOxQfVsGfa6Sqoj63gbYUXP5/7+dcuVDvyufe/HikxrqPQaTRRlZMb4lPSOz9TBeIkuWu4YzJAYmM0BnnQoNLSwYjEbILRq6db9gQCQnTR/94pEQVvXKO8htkxTj3adwwUxNOhCI1A9QVMA1cGXloiN/xz7BQHg4HHcj3hZDkQFPDbpJQpJJIQwOvu7P0fZ/q5EUJZUMK9+MjiNyflnqmM0lazIojELXAmjaLoC/LwGe9GxvenbBj2Nfg9IkDZk0jmkQi8pAyCqkebLBYLa17euiNDWxihbpiNyLLtbBA8aJ4CgYydudrnX7kVGwq3oDuiFKj2MKRAPdOk3KOmoqR9sh6wW5N1uOR8CAvGwiqEB3i4CcLQdl5RM3mK023z7BgBQ+oin9TgwhKRZFsEC0xeL2KIyBCe7Tw+P5PqXB3N29WQBvhwhJ5NM95dOq0UUOoCqFKujxiDHoG5joESmkF+vLONqVR9uhNonfOQ4EvSoLBhGvBuGwDPruCeWTj1m5h/0OCAY17lLVSIm+7RSXVIHG9Z04Sdgfh8cIRsatY95Hch04QQR7WjoUlU/46gBkFBI2rg4CeCAZMnVbbMQLkigdjQxH2yE3IVlfqnAaUPquUF4FBE7SixtDTsmwqj3o2sCZpPZcDRIZqmqEKAorJkRdrO4MFhGY8P4ubMgY0yQ1fAwyJ0NGXqkOiG8PSO33Z0YgGgabyaGLCPQAYeDZeB5Fuw7TK9EwVQlUqlrShSJfGnpKK4JvRN8miKQyoxiWoWcuYvu9TzVRpGneKQRm7QU8fcXg34uWq1poBA6EhOPZGwAju/Z/7tBYTQ2XEURuNWBM0wE8E40owFwMlxgcz1uFnMp++0dTE7mKGpiKYxiHE7mrD+Q3B9BSe5NpGu/U1JhesSINw4kQhrm2oz8f4DiBxihq/1QQvQU4WfYVRpr7Dkuva/xo3BAspY36jlAqJPr1fUA4cGUWtmbkFNTtFwygKWZ9qp0+izqZUMFA4WC2u3Qhu/95Iuaha/wJc5hLBp1DKyb1MBRVf0B3TQ+pSvThN3lMApAGEoRoEBLh/mKrw3WIL4MrEqUVl6tq1XGqXF+hKgKjb8ull8XUQ6UR5yDQagAm/cH/uHLHXzr4edgMnv3s95K89Bpgmzz9LUOiheOfq6ANTPvPvmaSpNozBH75xaqyaTpJes3hjiRI2iXbC0ddPRl9zg4MgmAlz354q6wuWyCE2g2KcvP6zJIL9A1V1EGeKVvkV14uit+fpWrVlXivHxTm7qccJacPttEhqSh9kwXu88tEcc18gO4crCkMiuH6Z1xjLr3+IBKUTkXtm6aqGy/EoC8dMEnRNBFTVN6mKSolCh0ZmNKTpecH5U4eVOMtH6cNO+V89ENuX7RAllvnc+zb96jKqklSQy+Ych+7xZQ/72DHGMyqxhcjf52uKvUnq2r7cE2L2ARh5xANa3Ga040gRHDwe0XoD8ZgfMWQaoPRHN8vGCS5aYI+ZKbKOoq01G46JE3rY8r7Ld2HGzJ7YnHaLymC01E0VF/7cECeh7G6fYpx1/RtBoFPi+g7+/QMyyoa8atAoCr+QkH49D4MW3kWG2ixdNdGkY5RwKhHuUwLeEoMYDABfP+9BB3rOqFk4ufT51w8/seqHj8ZaixH4Jfeio6LAFrT2SQBLV+z0NHVR2WNZhkEht0Zi7oTOPb1R2V58wicYIFlGfBtY6G9Ogxh7dvii4ZflUUSWMueEbh3HM9+co8oLj8Jw72kEAVoW8MCh7fZM0tStY+4X3i5MoH/5E6B/+I0VWl24QQCtIhD3QoWKE9j0biSvT22orQtELivbhSFr6arqhc0XC/CDNC5k4M27zfHnzT6YjOG4/vbXYcSxapFGHx0hRzbVtzX2gt9PQKISREoTK/1SgDhsIMn1wikzQBGQoDunVGorn/6tLMWzRljNFvWDcxAfNN47p17Rf7z41QsCnRCg45tImQwMcJ0YBWRsiVpy0WK2jiDwLuLSHndyGQ8CYFAENF7Th/GhTSHCTk8FXAHCb3tGrT18ZDgFRhSPmmvQtADgEFFXL1qChIJICMg8EkFbHqFZ9rQfyCGTVgzRY0pqZo8Pc1ZNmHGfXCAi/AJgvAqGMGGfbSJolSwiSpobORXbR0rChvPF8UnnlLFNve3H7OwZWlYM9A53cWlpV15uQavNVw3Zctn0UxFI2DtugS0NcZgxGkTvtsTCKpL1dbclEy8cq2q1ns0MIIiG2HDMg76VvEQYRSBoAxRfeGGJL3xkMC/dqssxkmZY5BAY2DnJgmav4nDu8ub4ezfn7oZAeFnmbhEmSi8+2dR/OhcWQqQGhjA20uBF5138yoWyk50+PcEguIBWHs3m3zjMpHbZpM1ChSJhoReh6pZgFUreBh7uukXA6K9p8ny0tsSsQ9nyLIPORkGZJEGBpNBRgSktZGEIBd38QLnNBrNycEjYnKYIPzPM7j4zty1S/vh849ELpTIaDfZijpcTnfAYrao+Vk20UVsm2/gfLmOAhwoKwltNRr0h2gZcHyAqBA5Fd3bQpZ9f74qd9MamIGmbeBtjMJ7X3BwZomq5u7b1dlEfvW9vPTBQgNR6xB9YahdG4aarQLUtRuDrOQMiaJRM+ICPbFUys0aSpEzp8gQ6cDBG+HAbLHB0DETN+8fDJqMDs6lrzhLhqTU7D6LqpfmMOk0ab/Lz3TyOP+aJ69TFPUWo9UVPVADJhylvarT7u1sEIv0ra50fsAloibYVbT4AJe9IfByn5wnCk/9m4sKhv88GYV33uyEy2+45d9X3LzoeoPJovNXNdL+3fjgq7es6etQqeqtnVBcMTs+dvrc53edI+QSuHWLdmz89GLOvzbbbDOrIkZCaZkE2zaJEK4TIJnkwZmVFjIaE0ZRfHhZV+NXc1s3I+8Tk1CnStCF3EZdbQy+WN4CacMmSpfc8Mc7IVX9pnNsJLBhUf2G9+ZLwSa7LY2C9By91DqKTtUaML0CtPh5mODJDP3gmAA2nx3o++KerVXvjBb6UV+YHZCWhUGUxcDiV6C2nofmfh5muDIad72B/kJF/P6PfQ2vXVq9bpvRoNjA6bCAxaWgs2mQi7x2Qy0GO1sEsLhFTFVVbHAN4x/Ns4+9n+xZWvbqUwFYX1fQfN2dD1w6ZeaMtTRNavjupbmqytvZ4LVzuO7+1M5MTVtw6OlSIH9EQbfRaPqJRvI1Z0tC9W11m98ez3qbCFeaHSgzA548Eak3Cbo3SpBppsBhM4UHv6dACZt4YQmiq1NoVoWl74fhk0+SQKdP+nDOKec+d8MVldsyMj0JkqQUgU3Ydn63qEpWt5VpMpXahDMYYsHhTot6MnMa9g+GVOkNHCdwGbiQCCxS4axeeBgjDtiwaZPjgCjVns0TM6QXtjrtdUUOOwmhkAa+jroi9IHuJA6oIBfPvXdKMvn3N0nVBq89HoFP32qF0y65duN1d91/68/vx1EI9nFTDeTL6/qQF7PDNX+6/w6DwbCRj3WktW5cuLphzc6h/tio2KRTnrjd6kg0rnz7ro/srAkP9yGDR++iLMMM7jFFGMc99Eb1N8snf/rvKFhItzZ3/vhmk7G2gK0T6dWbu0F15MCT//P8n1wu1xqFXTm75qubly393E9h5smdF93w7iW1qxYfH27/7rq8oSbo79AAC0hA0Axk55WsAegvYOPPP7N+2QcnfLCkE3e4Jjefce75b3l3vn5FZ0MoO6xS6I0p0BOWYWhpJhQMK0edu/zccPvTD2/5eEvRhx8LUDrt3OWVowq71q545bKyYUZIIJ1kN+JQ2y5DZzAOZdlW3mC0cAPTTF9FPLloqdS7PfPxe4JQ01voffnDj0/zpKftpQElacV8QWkoofRJlhwO2xAs+6I8ZFpTMxZSEztlMeGpevPWpzZu3JluLrn88dmTb6iObnz4yW5Zcg8tVkEOaxAKY5DnNIPF4ekZTBck4v9YIivLp1gTNnjloR548PWQ8s9XXr7urLNO2avcvwnxtdETYwyWFIAxERBKyhAMxKFy2JQmuyutZ79gUNWIBfHkQpzS93eWQRBl5IUkUNQjvQoTQ14iXbKaa4FARNTEkMBE2+08mxxmMJn3CwYkEF0s+9r9JoMNVryTgDXf9EFBSSlct+jOu36eJZJh3fFc7yOvrP3MjwmaSb538ZP3jhk77tkUmHhZ2dE4tipv8k2vnzj1mH8bzaYeMbly6qmXUjjDi1DbQ4JE45CVYYWO4JbiN//ZU7zluwDkj5zWftZ1992TV+iq2vLRxZtZtouOJjW4/9l/vDJi1KhH9HNzMVOIZ+e/dMrvTvy+dMzEz4xGJmaQjZU0K0B/3KRP8AU7g8OMyQVgotbMaNz42TWfv7GlaONmAxx/7t0vnXvFtbcjLxv+LrQxx66uulxOkkCg6DAs1wAF43IUG/PuX9a+9/6opW/0Ej388ODFd//lmtlzZ78n8qwj7l95THpaV164m4ZIBDmZiAjdCAwFjL2XwLHw3tRIcCcSf30T5+oyqz4XYdkaAZ5595G7BwJCKnUpLLuAECRgNBV6/RQ0drIo2mngyC7r+CGqk7Q5lDf5sVNzplmCJUNKUtopbH3vd0zXttlqzACdfuR8ORlI0iianBkDZZIwjv3gRYVfMcUsGWDV61H428st8JfFz903EBB25YGSaZocc6oaliqc3dXBQRDRpPwhY7YNlJzYCwy6ANRwXwmDPE88KCH9oIKVIIEi9IHsI9sIjGcT+g5yqs7EkShLhoGLB8chMOx3rELgl12J4/6KSBsNyz4OgtcXg3PPvabKleZZsYs+cLSsvLEo0fHyn794povsDQ8J3Pz4YwtLhpf/uDmeI70kfOZ1/1i4x/vAGqYTHgwMQRW6u0RoauWgvQsx8mALkmdmOPni2789/rxrLjIajX2y+vYdeZkB52MbeuDkcy+qOm3B2bf9cB5L5oSaKRdOuHYPz5WHD6MUE0SrMCAkpJP0mYoSC8ue++fJG5pFyC6Z13bP07f9cUR5xbs//KZyAsZQyEl5NxBAyxI0hWTo+r6VWL90w5gNrXa5/PhbXr/1muvvctitqRnGNEPiM082YlxUg2icgESvBH3+JCTQi84qGtEwEAXluDceUISqYdBFwAcf9cGxCy7aOmnq1CWDvXsT15sbRpQl3mOE9h0yJBMiuMwWyB06purnWr54+JgNP8uI2Q1Fcn4+hahthwIba5BWRb8jLWn9tMWxVxpalftni+xz59HIhhPorl9+pwvGzZiz4cIrLntkUNIsdRXjSsBKGnVnhHRSJwc4bYaR46ZuGDDlthcYgoE0sTOGSYIRwp0KSMgyKZJAyGaO+AJ9vkMV9eni+kbrPOKjEhsFzts+yZmRv7+oMFQQPr/FgBlh+VdBaGuNg8WTBadecOnL+iMpat2ZEvf4A33r1474+N8xUDKObbhx8WNnOhzOHfsdNxFitlhbBHr708Dfn0BCNw4bvTEYUpobv/qef94/afaJiyE1517Bof+zMz98rR/cQ6bVL3rw7/om6aHBhWnQJsst43HNBJ3oPUcRJV223Qt1XVGYOKG8+5Kb//Ts5NknPkcQP9sBR5MIU3Z0uIA8qb7UvN3Lw4tfdUAcaZVps4/59k//evSO4iFD9pjpKas9E7W0WK7CG8GFDK8tKkI/8o6oV6F46Oi99rZTVF+5wH1yuYlV4LuVArR5cbjxwvPfGCytrm91RogRKoEiVdhHQEs7C5oogj0zG5yerNWDT5HoyBX49lxGQ/SIR2pGwSAeZyFr6PAWo3nv/cSF0PvXElwrYLwLtqwTYUdvAu65Y+GbFEUPmu5XxMZxosqDhp49gs7oD7KQnp6ulQwdsfWAwIAphFHwqiCwNAQiairViToEiCNUOvTnLea3eX1IJEZ5fddMDd2DAPHuxlIon8nsa4xD4D44H6Ang/OSsH0bD3kmDCqOPbk9vyS9U1ZffI3tf/O86nc6iG3VVq1k3h9eOPb8a+8yMHTowIbq/GnIBlF41Xf73HVPmDlHve+FT88qLCn7sTpgtOfr4zb+Z82EoFbe/pdnXz3ZZrf37+u8cqLJo3bVZwqUEYJeDNiEAGt3+mHCtLlN/1jyznS73e7bm8cLDN/itcdRFOGRp9/ZlwR/IAYXXnHdR3998ukzBuT+idosMtYJkZAd5LgCbT4BCH1LMZNTyS0sXr13hP3yHEzsMHAdBHy5Mgh5pUPEEaPLv9yHyC4UrHIu4UO0llAhGJfBgCNtkpbZaXWmDToNR1F7KzA5SUssBf0+vaSQCoE4D5Vlo9cP8BBlKvflfJqgwNtNwXebfEAwVhhVWbnPffI0nM+J6vu1hxnw+Tno8bNQOnJ4t9OdtvPAwODpHe8oJoDdoUCS37VuDUOUBdfHso9wo7JnLlXgy5v0RUF6yRgR4Y/zd+rzk9LQ0TNwZ8QZQVx/OoXT0NvKQlLEYcKYbMivNNu52I1vdldXu7d9xkEYxrWfdOefbi8dXv7egY/YyZSqtU2mGRoSHGITogaRBAfl08r9CAg1P/9q85aanN5occcV9z5/RnpGZtt+x0E4zBxqIBjChQKuoKXWfY8qMMF555/12UBA2C1VPQQhpUUDGAjIsHvDHJTlWeC4E48fdB9kPhA2gV+AGBLaybACTV4WecgEDB83qSYnv6j+F6PmhYLwxXUmxPfrUMxs6krA6Gml2xEFrBv8FdVNUOWwU0VMIoE4fwBFKQeOaKEzrY4kyUGTLrK0eZoqKCDGaejuESGJxG1PVEXUbfj3ewFHah+tMT1WXCAhEQCoaw7A+GnzNpQMGbZ6346MdYqSvlkFBqGABD4UFecOHVNDUlRkv2DQKxXxLXUVvB8HVt9hEXloiti1iZ5ea/JIg8GeObTb7TSqXQkZ18t6JBDzkHrr3QKXrGCM5p6BRVLzSBJvHyEGNGhBXcZLCtQiLxtc8Ymze50MPq9NK5l81osXX7boVqPJdFAFXFUpkh5r9BfEgiYUETSkn2RgkQcrr6zUBd4eBls+7/evDp+58C2TzZ08sLMn0iizSqX2ZUE4643wKTrqSR80k4KsonGeIStsi7QaQEqKEE7yUFCYD0NGVQw6dYMyIw+M+lDtIyAc5aEvKqQ2nZkwdfb3qXzCz8dnxKrjQWlzSkES6pv1CiUCjByRMs7Bl+BqAbuKvhcJmSEYQrwf2Uo8KYApLXfzvqeedBcISRRxw4hdhlXo6IlCDBzBnJ+tL/gJOOsW4AoPrM8EzUhL9SEPP698dCvsuxQ+gwltlSZkthabBv4QAp6swZDhIwcF9p5gkAIY293uTkQNiI+i7oopQCGEhJERaDjVc6TBIKmKTBtS92zQQ1I/B1AS6IJEX+sMpnj05wN6vq6NU9UeL1Xb4oLWZglCfbFUOXsXPYTLnHrRB8fPPunRtMzsbb+mZDqusXQ8rJj6/SRoIQHRRmRM6J1kF5ZU/3IwkTKYZf040HPLifVj09JZ6EEhPBmRobYjDgphhvSC4YPqGLZncxmWSCDvaIAu5OF7QiwUj84Pp2VkDxyJNN6MWbbO5jEbSBEcGntZ8EUEoA0OmDpn3sa9Zx6sPFZFWqGtgYKalijwCPiZeYX72DYX6Ttx3dkca0EdoUCfV0ZGx4MP8fkFReO37GOsYJgmtcwiUaTlkdoKxCVYt90H4+eettnh9vT+YuiIlKSNYzj0nrpaaPh6nRexBgpFhpn7XBagKYFCTF5T5NbHVxIyNOwUwG7SC2fntA/a33tQJGnHeE9W/xjagCIDr29Qoqa8oUSSgFFM4EiDQcXIqMlCxvIy9RIZOIQRKkKROPgaNkwb7DfeCG74ai0DzegRcRWhH73dJOaB8+99+YF55191kScrZ+uv3TtA0+rLLZ447UAMsT8kQ4cvAWaHE0qHDq865GRBuGGiJKEAjuJINCyAN8RBWmZ+JK+wcPugzoLtGSaLWGpsugtx4GBUgqySynoUUQaOmtyOcWS4vjQRISGAtNj2riSEgjy4M7Lk7PyCPa6jiW3pdHzVtHjQAP09CgIaB7jBBuk5xYNqH1VjbZraO0yIIS0VR5G5l4f23jj4ObOQXlDWuI/kQYEkxMwBHwPRCNKIEQmSgqonAb7bGzjeAlnuzYuEGAQ0DTqRTrJn5Khlw0euHNTRyG0n8dxfP1ZMCRORZkYRdFdatSTXCbl5BZ0HBAZva2NBwzaCDkcJiKKbI1AMFxHtYAxGMJrMR1wzGMzpobCQ5p8+k4KJ5QTwCAz9URl6mxuG6/NQBmQOhGRBDg9YxLtR1IWpJW6YNtQBaS77Ia+Qi7dtONbfozsEAH9EBD/i6KVlQ5PFpaWrDwn0QsAgJr2jcKBB8iOKhLxpnBOhaNjwHQbGMGBnKVLcHfXXj8c1BjjE/3vCPJAYDqXDRg0KzGh/w5gAipZGxMeTMRE6AwKifhJMnXvsVqvVtn3P1CVrJ6JxWzKkL5RX9Y0YoSjLrWRm5wymf8yK+MkLtNycp+fSWBRRdE4ej3OABHd/XkHR4B4Y6xsjJpMQCxHQ61VSjoCgaCgsLd2+t4fvrlREn5WLESBwGigc0myVY7vdnvSB70tdcznOX/NmvGfZ0FgHCcleFBGR/k0gepiWnsF6MnN+GemI3ceeYAiFko62Lhz6epEBIDaW5qB2T85NVcV2HGkwkLRZYRi7aLJqUFioF6vBUuMNor/bLQhC5d6TLwCsZL2rpACHkkI8JbjNLiOcVJCEcNum+Qd21ejo5ppXH9xRvfQdfTjg55/s7JBtdU0UxJBeCLIiEogiFBYXB41Gk/dQnjMR9xe1dieK+ASF+K+CODOfeseVEycNugBe5OOWHl/CxvsZ6OnmEN3R59jYERhGDPqbUIS3t3YyYAwBdHuTEOcloBAPnXnMsW/+gm9nyfi2K1iP2ZRMkCnxaKAJyHQZNAIfiJcrBYr41Ads10PnBrdREO2lUWTmIYy0mizJkJOb3UTsMfkviXqzb96PtDxUPYXikqnlXHUtLDR1RpCztUFufuHPF4/pGUQnhvdNVJJcam9wC4NDbjoF2Tke/Xu/FOcGgHX39bXc/NK//r7DWvW+AbxbcViFyPX3VRJwyKMRJlvUbLH+LIvIjlWl16o6tz36koyc/h5gEOM7jzMbCdDQhTMdGLhsOAiIKgVivM7Dj3hkQOTMWlkacq1YIcGX32FAGjCIoL8q/c0Q6dwxY6B5UEiB2mfOJWHSXBw8dhq6Udd1CAp0r1tyDJcM3zOIEk1H+vcEVV3ySrjpwk0fLf7DnzZ8X5X/S0Fmo0JpBhrXiyBDX0hMFUbOzsnUR2yThxRxon5rn1diQjES/Mhr+5GwZRgKCoqKBq0gooq+cQ4za9VpUnO/ACwSz3lFpUJhSemg6Uu3sWNIWaGY2n6s0yeBhCikOzNHHFleuewnj9JxfrL94sZE0yN3yCEMHPrOq+jF2q0GSDdipJiMFux51vBMSNy6dM07jx7XsMqqaAicDBLzW1t5iMQFCMdkyMjO3f0cmhtg842ddQs3ffjsZW/xLG/T/+rriLqNEWRvrAR9QREwVYbi4sJud3rG7pQnN0qRXvw60LnQH21/9Q+RfgMwtIo4P8DpMzIhzyXp2cWfze1XhwF8+XrDpj/8+eorGrBm/7x3Rk2Y+m1eNo9AhsGQEhr1mx0cJnBIIr/7d22XxkN/WPbUn2+Y9NZrGzGSIn4S0Bri20I8MELbvX2UGeEsjkJlnJMhjutFwgx9RxoMIPd5NOhO70Yv2GREIthEpoboA7Eo9DbVTM4oG7PXT/paVMmWhUFaCYb4IAOdXgkaBBLsbTXg+ea6+/Nn/r6cdA5FXp/Qa6hhsrJ5nsh9syDRVpvVUd0CrdvN8qT59z4//pSrFsHP1hvwCZ/T1988yoxEFx9RU1NSjAwNo8aMrfllFuagR9pxJd3MaBhJAwSReNRryzptVsjOyhnUsJOBdTNd5jiwXgP4kMbQf1NQUtptMpnbBx5wimXE+9afnI5gj1hIKsoaKQ0qx1b4XB5PeyoKKqvv3/7ZLbc0t1h6xk8d0aIFmiucjAQ2MwnjRzqh0MgC4V91KxQPX4vOaAJt9UK+48E//evx9dj21jEbbvnjOcj3vn4VEdBAUtRUiZ9xw1wwdUL5DmTq0/m+Rc9+9O4Ho7/+mgycddnD1xhMBt0rm6LhaAYuUsjNYTCyxAqziwrBXVSI0TRVjGLYeZH+Bx9Y8uz7LoNh7ooTpmnFWWYlv9ePg4RouyPHDMUFW/LEyFdP0I7ZrwK0TNakl+9c/umn5sX/DLLHn3HP36++6fYHo42LlhJhEfKzjDBuPAMOPAeSOGfk4ysfM1mSpi1rnpiz+KFacGae9cx9/3z6lj2ySQpfNS7b01woRSgQkxhCoj7xbdf6gjSnBYVXJnTkwcARPHr/I3MwGDGSgFUf09CHhFkACay2HdtKKk+8WKdqe+SIScLT568VIaPIAOPnqBDxkhDmUIzBTFDd0giG/EULKDFzgSYhDy8JiHL1Q+uaGHS3msCSe8KbU6+85oH8oeV7GWEo5Ctu9kZzZ82goX4Zh7STBlajFYpKh3x7qI/JRRqn2RAVBBlDzgYDWZT00WBvXlHRtsF+E/G2F8phCuIJDDhBTsWwoaNG7xgsSmG4rIkOXPN5LZBEscyIwDBnXCYsXGDxYFD7T67z46krPnpp5JffmTovXbT47IwJX18cbW2pSNYjW8C01Ob0ZSNQdLC+NlMRw7XJeJtpx9rV5pf/1QeicfbHf37qfy7Kzqi+tX+bqBc/0vdfAwsC0fEVmVCQ3nRTzZpzs19/fi0tUrPfvvOxB24rHVLWs0sDKCRBxRmMIZBxI2QYcLAZkIFndeYk2n9f1RHYSb/4eB248i79x6JFf34gXn/dZgnpV5bHUHRTID+NAHsWCXTygYtZ4YWLO70BeP/VHbB+Q1r1H/7yxhWIAm7TQDNJsVAhg+DWiqjCCx9FQUrIMLLMDBs+u/uk2lY/bFpv9J1+/iM3nXflNW/tlVoVlTeuTsvnzP3tFBIqiIChsKRTptJMK5Sk62sbEhl7RKYjkU2iV85vlwi6sxt5sMlIXqIXRaNOJE0GkDtrc7l4ZIzR6tgji5A14rilLY0f3dm5UiDKSkU451gCOjswaOtCFItl4JsPZcQ1u1OzTWNBFh0K2Eune2dc/4erCkqHfYIPskNQItw+Yvx0GXOj8Kmh9yGzLDLYMVLZ8BFbDuUZUbAFkaudWVDOQHgzEv0sAjBy3SPGVOw0GIwDahExGfCoWt0EW5oRutokiMUEMBgNMGHKtMGLbOE85S4FjG8k9MWtwCOFFYwp8NmXq5kvln11ZdOOKBSPu2T5PS88cHaaxx1OJlaeC2YcLE4VMJMKS1cEYUsbBXm5CUhyz3k2bQ6BN2CD39+5eNGCC85/hEDGz3G8y4yodAhJWRZRJAHR0w9Wd8Inq+8r9OSM6Lvh7veunzpj+gd7ohST0kYYeawbT41Wr9ocBJ4XwLEOg9hTG2lFdrC/v+vZW+afseAF9LJIudTIi50kGCwqfLM+CrE4CwXfGcBiU6GxrQFad3Iwa/7Fr7/60UO/d7rd0V2UMpgrsn05ooWBzY0cbG+OgYqo3KaN3YgC0jD/3IWfPfXmH2/0ZGS07T3OwFVnqvWrzmpqtAJmIWD0JAE05E1K8hGy2i0gR30QaN0+05Odf0S3jlWaambaCAkqJ9FAGSBV48ZiIEBAWkoOdoN355YFhePm7AEGe+GUNbmV815uXv/p74iwFQzI42iKBukmHFwodDf3qiAGBOjzInNw5CdnXv77xWNmn7yYpul9TpfIzW2owKMsrH+Dg+ZmCUpzXVB+/KxNDM00H9JYiuQd7srtqBTqJFi3NglNTX1QkO+GOcedsHzQH9HhjNzypCe0RoGvvu2HeDwKQ8pGsQVFpcsGTy/2lily3EkKOGzdzqPnT4C3JwztbTgMGz4qcuZ1jz9+3ClnPkZTZCqyUOSQLVYhAgJrhc4+vaKhBI2NSfj6mxjEkQ4YO21mz9NvPHpjeUXFBz/ZNceQyBy+qYpCQ2MQAoEEWG1O9bzfLXrp6ptvvdPtdu1dTQXHJYLC9XoRsG5zBNq7o6kF+nUhFirGT2y9/+knrygfN3HlD+GNkl11DMaWr25i0HdjqTJC/T0JCHjj4EnPEW6//593n37BpY/+fNNPVY6bnNkJY021Cltrw8iWEaAjHAwpH9947V0P/GXyrGPeGsiOU2Dorf3yrEA34/JUGCHTLQDpV6ADiTvMiIPTg8P2rQkobagZNXz6SaXo601HJixwlBL3FeSVMgBWClRBr9WEbi+sQX0UCcYEMpxt645FYNBL4u+xVsI1+e6bmD7R9MnnS8/XJBkzIwAlEX810hTiyRRotrzE5IWXfDhq2vyHbXZHw4HcjiHeamtdwUi8fdZXc6+d9UlO2ZiNrsz85kMVzwSxZSzRt9O4DFFAv5btO2PhRV+Pn3PK88VDRn8/6I/E9WfEN3RTn39ESZprWN+tf7n9k7knLHg6LSOjcXBdgrPW/pD82ecc+dJnAUgIhDps7DGNp59/8btzj5v/otlq32NmKEVO+wwChVs2VLdVLv8+AO098dQ6r5LSUT1X3X7PYzPmHfe6wWjcY9Sd4dXA6qX98P5nYaCAhvMuv/LDa277490FRcWDrntHNisbE472Lz/3VlStDwKBHJfbkh697pZ7X7xg4dUPW222PQBkMJzxQm/LsgVfLOum+EAEOTq9ALZbPO/yC5dfcv2t9+YXFm3ay6gN7jYpYm5/+63txf1tHDidaeEL7/jTI+dfc9PjDMMMus4G0/dOC3YufJ4Wt18lodAVbtdQ2NJnqaJeZzF0IORv9AGRXg5/WvzemSRJfnhEooJcPSXWe9WaUMQCGVky6HuKb1+qQminCN9tRS+N4yGvfJp63SNvT0Jf37R3Hp6nepvrinuaa0cwhFwkAW00We39roycZmd2SbvR4ug4KGyKficX50xGZ14fjh++fY61+KYK7/YvF3LOU9/OyC9qNJmt/v39Ruh7f4G/qXWonHnSR5l5RV3IKGMHcCWI1b502brq/uFJMrNlePn4LaXDRtaSJMUN7o8Crra6dbPWbNhaCbhZKRk6cvvw8srVTrdn4LlSYsi57IMXr0+q6bHysROriocO24ohb77fO+O9Oa//62+3NbcG08dMnLF5wszj3s/NH3wwrGfnumM/ePuN34msCCPHz1xaMWXm9xlZOe0Yhg26ArKj9rt5by35zyWjx81ZPnHWsUvTMrJ8+7svTE8bSh2337v9gy/ubWk3KqKqUKoiYpi+txoiuLI+1yTBAZ1eAhc88OGFyKO8cSTAwAeeuCcUWHK/QDjB45GA1BRoW6fA+k8l2NoVBoIXIa+gFK5Z/OmNBpPlKfg/3DT4bYrI/lbX+f+lpWgS4bnjH8ULLv9Pgfb/uLsO6Ciuc/1P2dletJJWvXchVECAEEKmmm4wxoYYcC/EOY5bYjtxHs+ExI6DsbGDi4xtOsaADaaDaAKEAEmAGiqrrpVW2l2tttcpb2YlsISEX95578U4cw7o7Jw79/5z5/73/t9fETHpdYkseo2I9noUNEkqWcFQ7CVJoTI0ulkgkh75//lqFDg7z08SuRkQ2O1gcxAgC8MgMtwJ5UIGAqR86GR3BZ7dAIaOxqnhSZm/aGZA/s3G+bdiBlTk71KI/G/rq4Pisv7FqwMDXmysGnM1z4I6J1iusidSnBDCk1AYNdYLhlMsjsAdgHgdYKopnsYyQzL7VN3POG+cCopT83JesJ6B3xyWcd4BzKQDNolbogmX4MA78Ncy0BYb+H0rMwdXl4I3gE34A234AzjpThcTrh1X+854N9sHQ1NxCIpq2Ul2jPAO0kF0cLwjG6DFDHfJ8TTo2egBusgB2rv/iV2Pc8cXs0ONlGFbNECHc9C7CaE/juXOWBZiYGzrIHrEA+/ghJ9OVs0Z7AIH+uwY3Ba5ZyrMMjac7Fh+ArXfnObtk4KxFQcsAANlvBtO7GGgXcPyg5OESJEcsl/9Yq9/TOojPxepbmvJ/cb2zQd4slAtLhZ2NVf2StrUmP/8x/88k0fw6vs1Gh3+2pp3LrjBXykLETcbu71ws6QrZkz+os8IOLXCzkgMEgWu62ywx+hbKb8pS9+ewReKGiyaHX8zdJY8C5LQis7anlh5QMalIL+6FJsTkeBKidbRR6Nes00oF1M8F0VL3WZXUETyirV+CZPfHU5pn9J44/fXeQGr3pWGZ38+5B1MV+bobn7xHSYLvakIEnT0NlnCDT2GxMAQPiGPDuoWKPK34sTUd5BhlXM0Ofb2He/VllXme0kKgsN5oIwNtYoDpuzC8FmrEcDuKpuTlk2fWnq0sYq41bPv1Ghb2jbs9jJtM8XBIeWMh/bUXtOmSoVOpSom0CSShZXz8MXrAA/2earaNFvXutzXXhMGhZQKBUivvtMRXV/dFSMQ4RCbFGT0E0eWYeLZXwMeeke48M3ZbXWbNpcVtwarAgSQOiH7pEKqVAMzeRMmia/A3n777XtEdiBowFNKof3UgzyeSypSsl+hhwtbxCA2kwZdKwYOLwaouQ8YhowKGzP92D+3G/0/kIryzOBoQ7zWw9NLDpTGH9lXGRw/Zu6RpPQJ2xFkYIdGeW7U2U4h3iP5raXXU3Z8fj6CL0upHTft0beEeB3q1u5eWXywKvnbnVeC4rPm7k8dk7eD0wOgCKojXAeTy08dv3/fd+2K3PlP/yVQhFdi3sNzNDWlqds2lUX6+UW1K0Sy64xRS186eTSpxRHTnJ498dAwNS51flFryT+e0rYKFGFpUzcPAcs0ZrFrmwUe3eEFZYUlo3bsblQlZc37Ru7RGCz1B8eRxlNTKCpAIVCOOvbjU9ceaL749JHP1x2PsGGTvgiJm/6F1OtWW6p/yPbYTuTyRcY5mDDvEAuiLcMlYafS1vLnj9WXbowSh83dLZTIhhhxaa/bbaw+MtPWcTZrwwfnEls1MWUhyuiTdNuRyWbtuQy0u3wB7n//HowQmhm319RXW5htbi2cdOJoWcrnn9QgUr/J2wxdZI/x5ul0Z9/JsULk5AqcDhagkoQBlXVfWPXFpws3rKtmIhKWv+q0S9o87fuWXDm1LwfBs4pVUaMqfX4399I/uutYNlmY1kVdTGOo0kzGdDCbcVwdy7SdymK+fGkss/2FTKb6jxmMsfr0BbY99nPS6jH9Yc/Xv49iVq9a3s7+Vo7UhqK3rby8dzSzdGIC09rSknf7WftLW/etTWCeeyDfbXc404Y+V5Gx+69xzNf/+Ojgj/0cXFm2L5VZtWSa2+ZwJ966X/bNiuNHtr+3Y2T6XtxT8mUKs/c/Zjotpt6kEdtY1xxZ+2wg88aLqyrZ3yhFUWBrenXblYJkpuC1yQ6T0TAwVkdKd+VU01OzVMzxg0fnDe7D2vLdAzWbR5ON2xMZa/XGwyON43VdeKjxNPv9fp/CXD+78+WR2jh15XMvfJHF3JcRxlRVVM7n7lmaDi0r3JDBfPHbWKbxwtbf3u7P3hFRd3CqcWaGjPndiy9uu91Hb0t499kl58t2JjLVW7KYjrqilb72pqLla58PZvbt2f/c7TEtF59Z/UwYU7DhPR896D0HLkNml0HyXx5ymfBW2kSCPIQCqg8glJXy8qYx4CZwFkyTYLtYkOewm1f9nLRiQpWaQUmQyJUOuFueKEbo4dJ3yfzkpFzhd3s3RIUzdsRmSCFdaSNsho6Zgx+xd2pDbWYp5M2cs/NHzZCI72UhHp9PeG4ZyrgrKe/ZDzKyxwxTdzPutgTLjauz+V4CmlraBF2N5dNHBI1EUKdQQoPCz5/bzWnOIi+OXbIVUYngSJFaqGlvi/ZpUS2b39q+qVIeEPfAgVkL5gxRpEiiFx+Myr1/W6uegGN7ts/r7VIPi8fGTceW4jYbVHS4ofpq4UMj2nakcrsyRAxiCQ8IgvBhFlHEtGMp6YkdNVoHFF2uHHe7P1GAVRga5BSLfc6Tt9XTAmW0JmjCn5+MCPUzVLV5QHel4Pc++s3GsfnJMshOFP5Y5kA6aWv+9FlqfXdD6i3gcc9dWPjCEv7ErTlOOrjI1W0HoYjdwfQohIUCjEpD4IaDB5frK6D95LqPKIB5PxvMQUIcLpYAvL/Qyl1qXQAiIBj24+IIe93OqoAheedDRkXdkAjM0H6j6OHBz+jqD83kKzPtMQkJZwZ1RKAY59KOCNk+RT45u+mzAsqL5oQlz/humHzee3JlRxshCEkM7Q4PpaG9rvJXI4t8qImDrSQNt0sNU04Bn0/RwKAoKRAr21gRI9jceHpBRRUK0+fOHTE5gChx6ZbQdDmU3NDAteITU4YagfSx5w9fmt7ZG9c+9z4RNKkbch2W3nHDwT6DcrWP+QTCeUn75hPjiRyKiACnx+OGykbLkHRFOEIjPBwBdl6HGkKFKU208P5duJB9b3XdaKu2aQKN8724BAOk+x9vMc4rawZAtDciZdmf0sdMvHDPMoNvEpTpPaKcTxe6xOnfm7ScJokBzIhCegoFE8Zi0GAXQHHR91jF6S1cAqmUn8legIgEGGA4+pO6fqGw/+9QZYXC7ReWdSA6E4Pq4pPpFE3H9j9gF7Wrbz4SPnrO92y3+kH9OPk8DLJUCEa6e5cDc/XFSyf/8dylS1UjxJlQgq6aH55Q22dsUt33yMYEdozWuoqJNAPZw5iGBo9KLoJElZNz1X6A/ZeJeM48ceVqD6RPXLQxLj663qu/EaNT62V2Dxc2GXmXiMfE8tCEcLWEpeb6tbLEITaknqKHT1zslQRmv/LrzNEKHdB6tLNN/cgISkXg8uxKWSCMY6h//139FJOhNUbdaIWMrKxTg1AGiHAP8AifAlk+7JQJyj8erBLAZ0eb4fyVyjRJYMgRRTAOHRoNn9Q8vxqcv6ujyOsfJ2bMPLvg4ae339PM4GMIWYJZPnnrw0jY/NW9BqfXZeUCZjFIDkdgWg4BBlYEKPxufei1c7s48Jj1L6ePFZH8A3wJE5CfPkGwAWYYep+PTfwhMk1KGjS1Yk1jzRyflsd484EeExqWmHnfHYCXMSqCRRAk7Ibq75b8p+7Mrz+uuG4FE0UMr3/gvj61srI5IiZz/jcoNuOriNEhVn3nNayloWaYeMIyiCQ5xR8YTWFg3YHFP+hL51/XVm95mLT5w2PL81W+RSrq87dz6WVQPtzNYo6A3M6XhumDQvhQW98WzjL+rV0cr7m8/0lFWO7ehNEzjvrFjymOjbFD5ZXTnCg1JJiKYbmfUOIwLlEAdN/2T12O7RUe/Rv7P/+qmpeau/yT5U88tWXwLoMj/cpU9kQZZokWyYI0CrnYY3aS0GeyCICXeT4xIm9DU5MHPi3wwLVjh5SuxqdfNLZ+eIzykv73PDP4JhkX0crcdWv9Jq2f6iRibxh0NuiqBPBHUJg2TsDKuhgc3fVO3Onv/n6c9Lgn/ytpo+huCcbH2AXlK8gxop4fA68vQyCX7WMYyyBjK4LCQ0pVwQ4oP3d4OXdLe/P7Z6RhuTUhoUF3xPgiPG6IomZ22SW/9rgqbU1eTnaoUYCT0jvHdGgO/AolZRCqcmWQTkOeVOZvk8sscL7wwOI7FyCOIwKdyQqHKsR6UrnwQbli4Qo/SWLpgwtR8FS+/2h7zdVncLGQkAX215p0Omx3zYyO8girUMCBU+AyzvWLhHR1Xl9PRdKkvCScFQBn0+JIKj5BAjevFsZZjIbpQ09RFq9IeVDVaoODP9Q3OBu3ysvOXxK3d0iZV19/fTvB5/84xwgrMopx4MLb2fm1DsMoQgLhSyiEqy0iEhE+b2B81Huv5D/w21WEKFJ/6CgKm78m4cz2jWPbS7e9/ItghluXOGZOcfCsLblYwhO/AwLT6xrtwO+lYcF4AWSlCqD09Neq3RufOdHVUs6lG4z7n/AbTRvTvJ7K+x22gi0u97qL6tq/Fl39YU2Jsbt9zk89SNNeDgiwR7rPoDVikjOKIXy1raVCLpTlDm7A/BhR4NjjGbk80Fw7m+Uy1S+w6S5nxWXO34zcEXXH/pZ5XG4QSYM9MWPmF0HQ/OJR2TN2RQZQfkPEHoc2uK/5/CwBagfduZc36s48t5fRqkPSM8RQfvZgot1mnnxHv0yP3g7pE6ao0/IfP0AkPLuTn/rZnIBxCepybTfs2fXtCgA/k1+oGHCeCzTt7XctmIEilNfmdAFf4KdDEaRfoWA7ury90wIn9nyydOt7049dP7ZjSYxKDL3dTVB1ufDRIc+zq5HLl2W182HJc58855e8fkFUtNSbGOFGbl7a/+yw8QQoS1N/ddjhVkFzpNns4tEM35OUnFrN9pwIuCAgOmdVwWNvfJux7IkXC7LjxfT+Ijv8+f0t+SRF/XKYwfeChNwZdt/r61Me2zk6bMKiNR1dtK6u2AxZQQgsuk8BuKtKePirZ393Zu8fbvR01b9BUWTyXZaxkKbNWW73pRds1nVnzKZnym22V0+4nVse76j+btKN4/vyvy/YmNNws3LcfwMaGBmfKwLPjGT6De0XQxhGIqGAj/rUecMaUvyZx8PSJJAQbBM0X/n4s8YO1KsMiv92BJ6lENxXjAunadp3GihSXlmdlrv0Jfau34AFF8i+s/PKK40qQcaaN1IW7kxRjtudjEaufSpjspIimA6ovFq8aCgz0IDiFCur47djWzCeqpeRTroQmSQGg1HPyv9RWqlS2BEbwQNTZ/WIWimGIjGquytM3WSDxLSsgZxJpoCyIz8s6aZmn1v06+1jUvI25siSCsb7RU8oiUlGoOTc0also+AfiWG4bEccqKcRxIMBkVEVFDu+OCkdhdKiw0stRm3C0JPoFtZAhztSum7MqbvZA+GRsc3xyekNhvrPvzI0nXzdt7GK5dqkvBdW5Tz69q8Xz1OA00Oj9C+NGW6Do4C4npgFa9+e9MqOVEHKkj+UlyF97g4z5McikBHDfrCWo5Izu5742+Gdz924Xrb7pF5fx06CeRxJqpe4XMc+tFjeOm8xP3nNbvvjJ27n/im0q5cAswfQXgT4OhS0TSTU62ngCQQ/Gd2HYUFmHCh2gSKSAfeAAQNSQ071hb+UUSSEsCeHLy+hh+F4Bxm2h6Ho2DKhKqEsJoeEA98eCWvrG3UqIFCpGWE4E6BcAS4EZRifmwIrCgT08SUipOrky1e7Wyq5gHswth5e1mVL7syesvhTfkBCnUAVWy8KeWCzOCznTFomDZcKDy9gm/kPIsDJkUVxBfwG59Fy0BKvg5P+GC9AiFooSyucNJGAuvJzU90Oa9bw47UhV6upzWpqFzP3z1uwx3fTemnmmaJWxZR5T23IGjfx+vhJ911JSB9fyo9eXjB2kgJqa0oDO1oacgYzPHAOwginSWL6/eb4s3ZnZfHA7dFJ6koPvzjkTMMQLmEBVy/jDgBtT9A0nFp2usgG8xYu/YpPILTZI1ary3yi6I9iomTW7qTc8XqVklMHIr9MZritjwlP7s19cu3f8l7dNVo45o8reu0JJwmzg4rgURAjsIPcUcbvKXt3ZmPx8+9ZDU9edZh/s9drW/My4jib7TXpwaNjF5cWB2c9Bb3XvNBWTEPhSQdUVPRAZHCIIywq4fJ/gxkizG4UeMAEDvi8sAveOsapW7dJ11AtoBHoA8ausFgxcPTZMMplix+2iBC2GX/SgfBEErq0LkjPnX1o5J3XHYi4WPxBcq409EDIIRnnNn667fSRw/FGK68NoCG1ofrKDHn41MtcRsxBywaAl3ohJV0GTTWXQvoM3Xm338FiiGJIBCxmk+oWkzBgGcVzFM+8dtkEEkVwsU/pRCz9bNJMpUOKNwrqL+9aD/1+TLcuOWPe9c7ObxogZ+qKT0dnZvrS1/Q0HHihzxsFKWlpQ+MbkPhrcWnhXonYxjLX6SU/TqgjCHVYgc/zgMvWl9avZZjyfWRifEdULAZVF/evZDyW8f07jluMOixCuUwAUlQ/eF6DvKYvNhcUnFYS/jknHlqx6iPfzei4s4z7cqirs+S1QUyToWvtDAyLSLzOI3hw77hj/C8uoUxp9Y/JqPJPW7ADlScVMpS8G0ih3qlzClvUFpnLYEN7W/Tg0jh8IYrlFxno40KUemgwNtNQccUNFy5YoKTMCHW1WhAIZZ5HX3vnjZSs3LvGbnisF17ymL5+Czc6wG1wiIR+zGIeeuZpr23Dm2X7i0J6kcXbRmeH6yyd738lBrNYaHCBm3TkC5RRmEiiLIFBDn0YT9xr7Dyw6voNleXhZ996UyDgDwGElKdttlP7/ueoRSukuj0gk3mnoXDtMarnw9XXT+5Lru0ac3rRyud3M86/ft9+rTrInxcYERGXwi4kRaUPyzDuNHffN+8KXc3KjnobBAvMc5VROe0IUzkTzJ+9rG1ygLXHIpk4MTaRZnoyKOPf1189cyFw32Gi54XX331GFRysR5GILoHQpQ0Xls9VXy2PC49iFvIUoXZgdLlWzUdbt360KcPgmrX3zXc2vMAuLJJxXvqgrqxgqVpNw9gx8RECWUQViuGcJoogrSV/t/ccziopdgDiaE2PjU+RC2T+Vo/hwy816mrZtasOSAj15KuixzbhfGUZCLj86udnVZX3Cuy9pc8SimSzlDj7WFf9sQlnitwQJjYljB6nzKMQzf1NVevWf/L3guTOvvQTaz7c/Cs///50nwTfJq4p3/Z0zdWLU+JHRY/GBMIkTcPH72//5Ahv9sN/+k1EbHz3veOo939tA/C5I/RI9B1NcSZNQ2ptbXVGR1tzenNzfbKE51GOjZULSC9J2G1uRNfnhW4r2OUBQS3JYyadzF/85NcR8ak1PwGdwdr52729N9QTezX8Zj8lY+D7OaP5hDPUoceouu60svHLP1gmgc3PW+r2/MnWpdAJCXAbzYZAjTe7cdrjH85lJRPHYPDRU7bsaFVjVsuMZW/+Zpim1PTVG67mbS9ZNEq1x4EYZEqLii91xbXctBJFtaEN0x97f/modDxYd/2lHd3VwlaMdvJaTH6ReY9tXKDw86+i3Mee0ZR/sKaxVNSI8TBzl7Y7PTBz+ZkpUxuVVYUlEzSa4EYU8QoCZLpYiZTi95jE3WXNkadnLXlpfea48UMzVluPj1ef+mJNVW39NHmYlOCK2aib8ZvKyCXvP/TYC9tEYhHFzo/Y3Pzqob27yuXNbaidQK2jpz745tr7Zj/yAYuoI3S1Lxzct7ONammju10eW1SAKtb76upFx9RlXz7+w0Gkx24lDaTHmDp22rJTK1etfpIzjJvr/7LuP97Y9LiHn3huzfo/buuq/utHRw+4yD4T0gU8WqCQmWJp9pD2elV1KVkPfrnw0ae3iaXSQWpnN1p5fsuivbu/XykRaaeKxCBvayPbcme89MriFc/v74cs/6bMMKJYwwrxuq4OGeIxK1m8KHI57VKrvkvBlwY4BYqgLj9VWLtAJP7n6hZTLp7HxfBIACcPRxgehmAep50F5gxCSP3sKAsC2S0dpb2kkEZYQIgCiaEMj2FQL4Lzh7kYU47OAMDlDoyQjOA67UVpt5dgoa4bx322PlaSd4mNum6B0D/cIhKLvVzNEdLt4bHDe3k4g7pZwYYghK7+hAde1Ovy4hSNeHyJ2YDhc6gZIW2YxeTkixX+VhaDYmZDp8TrJTGhPMwuEgtdLKi+y17gQXo0TTE2uyOEZNepKiSmwU+pGDJvFOnCHQ6KJQJhvCQtJHi4i+ALuDI0QHqduNsFNI+HsNPFEOx/mFCIu5wOUsi+nJfgoSwZtICrpSYQCMn+b0ch2ja1XKkKtYskMm+vrlvM4wkZoYhgjxcE1XVpJZzQGRQWbsV5+F0jEzmgrNW0+Zl69YrgsCi9f1DwbXHyvwQYAMdQt5ZdBxJCAAAAAElFTkSuQmCC" width="150" />
                            <p style="text-align:center;margin:0;">INVOICE NUMBER</p>
                            <h1 style="text-align:center;font-size:14px;margin:10px 0;">' . get_the_title() . '</h1>
                        </th>
                    </tr>
                </thead>
                <tbody>';

                    if( $btype == '1' )
                    {
                        $agent_id = get_post_meta( $post->ID, '_checkout_agent_name', true );

                        if( !empty( $agent_id ) )
                        {
                            $agent_name = the_agent_list( $agent_id );
                            $agent_addr = get_post_meta( $post->ID, '_checkout_agent_address', true );

                            if( !empty( $agent_addr ) )
                            {
                                $agent_addr = ' <em>( ' . $agent_addr . ' )</em>';
                            }

                            $html .= '
                            <tr>
                                <td style="text-align:left;background: #001b48;color: #FFF;" colspan="9">Agent</td>
                            </tr>
                            <tr>
                                <td style="text-align:left;" colspan="9">' . $agent_name . $agent_addr . '</td>
                            </tr>';
                        }
                    }
                    else if( $btype == '2' )
                    {
                        $country_code = get_post_meta( $post->ID, '_checkout_country', true );

                        if( !empty( $country_code ) )
                        {
                            $html .= '
                            <tr>
                                <td style="text-align:left;background: #001b48;color: #FFF;" colspan="9">Country</td>
                            </tr>
                            <tr>
                                <td style="text-align:left;" colspan="9">' . the_country_list( $country_code ) . '</td>
                            </tr>';
                        }
                    }

                    $html .= '
                    <tr>
                        <td style="background: #001b48;color: #FFF;" colspan="2">Vessel</td>
                        <td style="background: #001b48;color: #FFF;">Trip Code</td>
                        <td style="background: #001b48;color: #FFF;" colspan="3">Date</td>
                        <td style="background: #001b48;color: #FFF;" colspan="3">Destination</td>
                    </tr>
                    <tr>
                        <td colspan="2">' . get_the_title( $boat ) . '</td>
                        <td>' . $trip_code . '</td>
                        <td colspan="3">' . $depart . ' - ' . $arrival . '</td>
                        <td colspan="3">' . get_the_title( $itenerary ) . '</td>
                    </tr>
                    <tr>
                        <td style="text-align:left;background: #001b48;color: #FFF;" colspan="9">Cruise List</td>
                    </tr>
                    <tr>
                        <td style="background: #f4f8fb;" colspan="2">Cruise Details</td>
                        <td style="background: #f4f8fb;">Cabin Type</td>
                        <td style="background: #f4f8fb; text-align:right;">No. Guest</td>
                        <td style="background: #f4f8fb; text-align:right;">Gross (€)</td>
                        <td style="background: #f4f8fb; text-align:right;">Discount (%)</td>
                        <td style="background: #f4f8fb; text-align:right;">Comm. (%)</td>
                        <td style="background: #f4f8fb; text-align:right;">Nett (€)</td>
                        <td style="background: #f4f8fb; text-align:right;">Total Nett (€)</td>
                    </tr>';

                    $cruise = get_metadata( 'post', $post->ID, '_checkout_cruise_list' );
                    $ctotal = 0;

                    if( empty( $cruise ) === false )
                    {
                        foreach( $cruise as $list )
                        {
                            foreach( $list as $c )
                            {
                                $discone    = isset( $c[ 'repeat_guest_disc' ] ) ? floatval( $c[ 'repeat_guest_disc' ] ) : 0;
                                $total_nett = isset( $c[ 'total_nett' ] ) ? floatval( $c[ 'total_nett' ] ) : 0;
                                $gross      = isset( $c[ 'gross' ] ) ? floatval( $c[ 'gross' ] ) : 0;
                                $nett       = isset( $c[ 'nett' ] ) ? floatval( $c[ 'nett' ] ) : 0;
                                $disctwo    = 0;
                                $commission = 0;

                                if( $btype == '1' || $btype == '2' )
                                {
                                    $disctwo = isset( $c[ 'other_disc_value' ] ) ? floatval( $c[ 'other_disc_value' ] ) : 0;
                                }

                                if( $btype == '1' )
                                {
                                    $commission = isset( $c[ 'commission_value' ] ) ? floatval( $c[ 'commission_value' ] ) : 0;
                                }

                                if( $discone > 0 && $disctwo > 0 )
                                {
                                    $discount = number_format( $discone, 0 ) . ' + ' . number_format( $disctwo, 0 );
                                }
                                else
                                {
                                    $discount = number_format( $discone, 0 );
                                }

                                $ctotal += floatval( $c[ 'total_nett' ] );

                                $html .= '
                                <tr>
                                    <td colspan="2">' . $c[ 'cruise_details' ] . '</td>
                                    <td>' . $c[ 'cabin_type' ] . '</td>
                                    <td style="text-align:right;">' . $c[ 'no_guest' ] . '</td>
                                    <td style="text-align:right;">' . number_format( $gross, 0 )  . '</td>
                                    <td style="text-align:right;">' . $discount . '</td>
                                    <td style="text-align:right;">' . number_format( $commission, 0 ) . '</td>
                                    <td style="text-align:right;">' . number_format( $nett, 0 ) . '</td>
                                    <td style="text-align:right;">' . number_format( $total_nett, 0 ) . '</td>
                                </tr>';
                            }
                        }
                    }

                    $html .= '
                    <tr>
                        <td style="background: #f4f8fb; text-align:right;" colspan="8">Total</td>
                        <td style="background: #f4f8fb; text-align:right;">' . number_format( $ctotal, 0 ) . '</td>
                    </tr>';

                    $travel = get_metadata( 'post', $post->ID, '_checkout_travel_service' );
                    $ttotal  = 0;

                    if( empty( $travel ) === false )
                    {
                        $html .= '
                        <tr>
                            <td style="background: #001b48;color: #FFF;" colspan="9">Travel Service</td>
                        </tr>
                        <tr>
                            <td style="background: #f4f8fb;">Date</td>
                            <td style="background: #f4f8fb;" colspan="2">Service/Route</td>
                            <td style="background: #f4f8fb;" colspan="2">Hotel/Flight</td>
                            <td style="background: #f4f8fb; text-align:right;">Guest</td>
                            <td style="background: #f4f8fb; text-align:right;">Nett (IDR)</td>
                            <td style="background: #f4f8fb; text-align:right;">Nett (€)</td>
                            <td style="background: #f4f8fb; text-align:right;">Total Nett (€)</td>
                        </tr>';

                        foreach( $travel as $list )
                        {
                            foreach( $list as $t )
                            {
                                $date    = empty( $t[ 'date' ] ) ? '-' : date( 'F d Y', strtotime( $t[ 'date' ] ) );
                                $netteur = empty( $t[ 'nett_euro' ] ) ? '-' : number_format( $t[ 'nett_euro' ], 0 );
                                $nettidr = empty( $t[ 'nett_idr' ] ) ? '0' : number_format( $t[ 'nett_idr' ], 0 );
                                $sroute  = empty( $t[ 'service_route' ] ) ? '-' : $t[ 'service_route' ];
                                $hflight = empty( $t[ 'hotel_flight' ] ) ? '-' : $t[ 'hotel_flight' ];
                                $tnett   = empty( $t[ 'total_nett' ] ) ? '0' : $t[ 'total_nett' ];
                                $guests  = empty( $t[ 'guests' ] ) ? '0' : $t[ 'guests' ];

                                $ttotal += floatval( $t[ 'total_nett' ] );

                                $html .= '
                                <tr>
                                    <td>' . $date . '</td>
                                    <td colspan="2">' . $sroute . '</td>
                                    <td colspan="2">' . $hflight . '</td>
                                    <td style="text-align:right;">' . $guests . '</td>
                                    <td style="text-align:right;">' . $nettidr . '</td>
                                    <td style="text-align:right;">' . $netteur . '</td>
                                    <td style="text-align:right;">' . $tnett . '</td>
                                </tr>';
                            }
                        }
                    }

                    $payment = get_metadata( 'post', $post->ID, '_checkout_payment_terms' );
                    $gtotal  = $ctotal + $ttotal;

                    if( empty( $payment ) === false )
                    {
                        $html .= '
                        <tr>
                            <td style="background: #f4f8fb;" colspan="3">Remarks</td>
                            <td style="background: #f4f8fb;" colspan="2">IDR xe</td>
                            <td style="background: #f4f8fb; text-align:center;">% Due</td>
                            <td style="background: #f4f8fb; text-align:center;" colspan="2">Due Date</td>
                            <td style="background: #f4f8fb; text-align:right;">Grandtotal</td>
                        </tr>';

                        foreach( $payment as $list )
                        {
                            foreach( $list as $p )
                            {
                                $html .= '
                                <tr>';

                                    if( reset( $list ) == $p )
                                    {
                                        $html .= '
                                        <td colspan="3" rowspan="' . ( count( $list ) + 2 ) . '">' . $remarks . '</td>';
                                    }

                                    $html .= '
                                    <td colspan="2">' . $p[ 'label' ] . '</td>
                                    <td style="text-align:center;">' . $p[ 'percentage' ] . '%</td>
                                    <td style="text-align:center;" colspan="2">' . $p[ 'due_date' ] . '</td>
                                    <td style="text-align:right;">' . number_format( $p[ 'total_nett' ], 0 ) . '</td>
                                </tr>';
                            }
                        }

                        $html .= '
                        <tr>
                            <td colspan="2">Total Cruise</td>
                            <td style="text-align:center;">100%</td>
                            <td style="text-align:center;" colspan="2"></td>
                            <td style="text-align:right;">' . number_format( $ctotal, 0 ) . '</td>
                        </tr>
                        <tr>
                            <td colspan="2">Total Service</td>
                            <td style="text-align:center;">100%</td>
                            <td style="text-align:center;" colspan="2"></td>
                            <td style="text-align:right;">' . number_format( $ttotal, 0 ) . '</td>
                        </tr>';
                    }
                    else
                    {
                        $html .= '
                        <tr>
                            <td style="background: #f4f8fb;" colspan="3">Remarks</td>
                            <td style="background: #f4f8fb;" colspan="2">IDR xe</td>
                            <td style="background: #f4f8fb; text-align:center;">% Due</td>
                            <td style="background: #f4f8fb; text-align:center;" colspan="2">Due Date</td>
                            <td style="background: #f4f8fb; text-align:right;">Grandtotal</td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="2">' . $remarks . '</td>
                            <td colspan="2">Total Cruise</td>
                            <td style="text-align:center;">100%</td>
                            <td style="text-align:center;" colspan="2"></td>
                            <td style="text-align:right;">' . number_format( $ctotal, 0 ) . '</td>
                        </tr>
                        <tr>
                            <td colspan="2">Total Service</td>
                            <td style="text-align:center;">100%</td>
                            <td style="text-align:center;" colspan="2"></td>
                            <td style="text-align:right;">' . number_format( $ttotal, 0 ) . '</td>
                        </tr>';
                    }
                    
                    $html .= '
                    <tr>
                        <td style="background: #f4f8fb;" colspan="3"></td>
                        <td style="background: #f4f8fb; text-align:right;" colspan="5">Grandtotal</td>
                        <td style="background: #f4f8fb; text-align:right;">' . number_format( $gtotal, 0 ) . '</td>
                    </tr>
                    <tr>
                        <td style="background: #001b48;color: #FFF;" colspan="3">Departure</td>
                        <td style="background: #001b48;color: #FFF;" colspan="6">Arrival</td>
                    </tr>
                    <tr>
                        <td colspan="3">Point : ' . $dpoint . '</td>
                        <td colspan="6">Point : ' . $apoint . '</td>
                    </tr>
                    <tr>
                        <td colspan="3">Time : ' . $dtime . '</td>
                        <td colspan="6">Time : ' . $atime . '</td>
                    </tr>';

                    if( $banks == '0' )
                    {
                        $btransfer = get_post_meta( $post->ID, '_checkout_bank_transfer_detail', true );

                        if( $btransfer != '' )
                        {
                            $bank_list = bank_get_option( 'bank_list' );

                            if( isset( $bank_list[ $btransfer ] ) )
                            {
                                $html .= '
                                <tr>
                                    <td style="text-align:left;background: #001b48;color: #FFF;" colspan="9">Bank Transfer Details</td>
                                </tr>
                                <tr>
                                    <td colspan="1">Receiver</td>
                                    <td colspan="8">' . $bank_list[ $btransfer ][ 'receiver' ] . '</td>
                                </tr>
                                <tr>
                                    <td colspan="1">Account No</td>
                                    <td colspan="8">' . $bank_list[ $btransfer ][ 'account_number' ] . '</td>
                                </tr>
                                <tr>
                                    <td colspan="1">Swift Code</td>
                                    <td colspan="8">' . $bank_list[ $btransfer ][ 'swift_code' ] . '</td>
                                </tr>
                                <tr>
                                    <td colspan="1">Bank Name</td>
                                    <td colspan="8">' . $bank_list[ $btransfer ][ 'bank_name' ] . '</td>
                                </tr>
                                <tr>
                                    <td colspan="1">Bank Address</td>
                                    <td colspan="8">' . $bank_list[ $btransfer ][ 'bank_address' ] . '</td>
                                </tr>';
                            }
                        }
                    }
                    else
                    {
                        $receiver       = get_post_meta( $post->ID, '_checkout_receiver', true );
                        $bank_name      = get_post_meta( $post->ID, '_checkout_bank_name', true );
                        $swift_code     = get_post_meta( $post->ID, '_checkout_swift_code', true );
                        $bank_address   = get_post_meta( $post->ID, '_checkout_bank_address', true );
                        $account_number = get_post_meta( $post->ID, '_checkout_account_number', true );

                        $html .= '
                        <tr>
                            <td style="text-align:left;background: #001b48;color: #FFF;" colspan="9">Bank Transfer Details</td>
                        </tr>
                        <tr>
                            <td colspan="1">Receiver</td>
                            <td colspan="8">' . $receiver . '</td>
                        </tr>
                        <tr>
                            <td colspan="1">Account No</td>
                            <td colspan="8">' . $account_number . '</td>
                        </tr>
                        <tr>
                            <td colspan="1">Swift Code</td>
                            <td colspan="8">' . $swift_code . '</td>
                        </tr>
                        <tr>
                            <td colspan="1">Bank Name</td>
                            <td colspan="8">' . $bank_name . '</td>
                        </tr>
                        <tr>
                            <td colspan="1">Bank Address</td>
                            <td colspan="8">' . $bank_address . '</td>
                        </tr>';
                    }

                    $html .= '
                </tbody>
            </table>
            <div class="page_break"></div>
            <table class="tg">
                <tbody>
                    <tr>
                        <td colspan="2" style="text-align:left;background: #001b48;color: #FFF;">General Booking, Cancellation, Payment and other policies - effective date October 1 2021</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background: #f4f8fb;">
                            <b>Acceptance of the above booking means that you/your company agree to the terms and conditions as outlined below in their<br>
                            entirety and to be read in conjunction with any published addendum applicable for the booking period :-</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>Cruise Payments :</b> Payments are preferred by telegraphic transfer in Euros, sender pays all fees, to the account on your invoice only
                            Payments in USD are available on request and the exchange rate will be based on xe.com exchange rate on date of transfer
                            Short payments will be invoiced. Please send copy of your transfer advice by scan email as soon as you process payment.
                            Payments via credit card via Stripe orPaypal are also accepted and are fee free for direct bookings only.
                            Agent payment via Stripe or PayPal will be subject to a 5% admin fee on top of the invoiced amount.
                            Sorry we do not accept personal cheques or travellers cheques<br/><br/>
                            When you accept this booking , you accept that it represents you/your company not that of the individual guest and therefore you or
                            your company remain responsible for any/all payments no later than on or by the due date. Failure to make payments by the due
                            date may result in the guest\'s space being released and any monies paid being forfeited. Group/charter payments are required to be
                            paid as invoiced and will not be accepted by individual participants of that cruise.<br/><br/>
                            <b>On Board Payments:</b> On board sales are accepted in Euros, US dollars , Indonesian rupiah and by credit card (Visa and Mastercard).<br/>
                            <b>Cruise price includes:</b> transfer to/from the vessel on departure/arrival days as per each cruise destination allowances , all meals and
                            snacks, fruits, coffee, tea, hot chocolate, soft drinks and drinking water, tanks, weights, weight belts and diving with experienced
                            and certified  Divemasters &amp; Instructors<br/><br/>
                            <b>Cruise price does not include:</b> Any applicable fuel surcharge, transfers to/from anywhere outside the cruise free transfer zone as per
                            our information packs, equipment rental, any on board courses, Nitrox and national park/port fees. Snorkellers will be charged
                            the same rate as divers<br/><br/>
                            <b>National Park/Port Fees :</b> are payable on board only. National Park and Port fees are subject to increase and/or amendment by
                            government policy and are out of Mermaid Liveaboards\' control.<br/>
                            <b>Dive Insurance:</b>  Dive insurance is mandatory for all guests in Indonesia by government regulation. If guests board without proof
                            of insurance they are required to purchase on board.<br/><br/>
                            Travel and Cancellation Insurances:</b> Guests are strongly advised to have valid travel and cancellation insurances.
                            <b>Guest Information:</b> Hotel/airport transfer information is required a minimum of 1 week prior to the cruise.
                            Guest Information forms must be submitted to Mermaid Liveaboards by email no later than 1 month prior to the cruise
                            When checking in, all divers are required to complete a liability release waiver and to show proof of diving certification and dive
                            insurance such as PADI, Dan or equivalent. Please have these available in your hand luggage.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background: #001b48;color: #FFF;">Payment and cancellations - the fine print :</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background: #f4f8fb;">FIT Bookings :</td>
                    </tr>
                    <tr>
                        <td>
                            <b>Deposit<br>
                            Final Payment<br>
                            Last Minute Bookings ie less than 60 days</b>
                        </td>
                        <td>
                            <em>30% within 10 days of booking<br>
                            60 days prior to departure<br>
                            100% payment within 3 days of booking or prior to<br>
                            boarding if booked less than 48 hours prior to departure</em>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><span style="text-decoration:underline">Cancellations:</span><br>
                            More than 90 days prior departure:<br>
                            90 to 60 days prior departure: <br>
                            Less than 60 days prior departure:</b>
                        </td>
                        <td>
                            <em><br>
                            Refund of deposit less €100.00 per person administration charge <br>
                            Cruise non-transferable and deposit lost<br>
                            Non-cancellable, non-refundable and non-transferable</em>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Transfers are only accepted if requested more than 90 days prior to the cruise date. Transferred cruises are only transferable within
                            18 months of the original cruise date and will be adjusted to the rates applicable for the new cruise date. Any discounts valid for the 
                            original booking do not transfer to the new date unless the same offer is valid for the same destination at time of transfer request.<br><br/>
                            <b>Important Note:</b> Refunds will be made by bank transfer only - beneficiary to pay any/all bank fees
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background: #f4f8fb;">Full Boat Charter or Group Bookings : identified as 8 pax or more Mermaid I; 9 pax or more Mermaid II</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <em>10% non-refundable and non-transferable deposit is required within 10 business days of booking. The next 25% of the total cost
                            to be paid no less than 120 days before the cruise date. The balance 65% is payable a minimum of 90 days prior to the cruise date.
                            If any payments are not made on or by the due date the cruise will be released, any monies paid will be forfeited and the space
                            made available for resale.<br><br/>
                            <b style="text-decoration:underline; font-style:normal;">Cancellations:</b><br>
                            More than 120 days before the departure date, all payments will be refunded, less the 10% initial deposit.
                            Less than 120 days but more than 90 days before the departure date all payments will be refunded,  less 35% of the total cost.
                            If a cancellation is made for part of the charter/ group then the refund will be pro-rated and any free of charge concessions will be
                            forfeited. Any discount will reduce to FIT rate if cancellation results in less than minimum numbers for a group booking.
                            Less than 90 days before the departure date, no refunds will be made and date transfers are not accepted for any reason.</em>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background: #001b48;color: #FFF;">Other Services : Terms and Conditions</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background: #f4f8fb;">Hotels, Tours and Transfers:-</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Full payment is required a minimum of 60 days prior to your check in/tour day. Bookings made less than 60 days prior to the
                            activity - payment is due within 3 days of invoicing. Failure to comply with prepayment terms may result in the cancellation of any 
                            reservations made on your behalf. Hotels require complete rooming lists a minimum of 1 month prior for group bookings
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background: #f4f8fb;">Airline Tickets:-</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Tickets must be confirmed within 48 hours of request to ensure the same rate and are subject to availability at time of confirmation
                            Tickets confirmed more than 48 hours after our quote are subject to fare and class change on rebooking with the airline
                            Ticket rates include a nominal service fee of €30.00/per person/per ticket 
                            Airport taxes are not included and are required to be paid prior to boarding at the airports
                            Tickets are reroutable during the ticket\'s validity dates subject to acceptance by the booked airline
                            Airlines will apply an administration fee for any changes to date, class, route and the guest will be required to pay such charges as
                            required by the airline plus €20.00/per person/per ticket service fee.<br><br/>
                            <b>Airline Tickets and other Services Cancellation Policy:</b> Refunds will be paid as per each service\'s policy and for flights as per each <br>
                            airline\'s policy as shown on the tickets less the initial  €30.00/per person/per ticket plus an €20.00 per service admin fees
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="page_break"></div>
            <table class="tg">
                <tbody>
                    <tr>
                        <td colspan="2" style="text-align:left;background: #001b48;color: #FFF;">Important Notes:</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>Minimum Numbers:</b> Mermaid Liveaboards  reserves the right to cancel any trip if there are less than the equivalent
                            of 4 deluxe cabin guests. Mermaid Liveaboards will do their best to transfer the guest to a vessel the company deems to be of similar
                            or better standard . Alternatively we may ask the guest to transfer to the date before/after the cruise. In the event  none of these
                            options are possible ,we will offer a full refund (please allow maximum 2 weeks processing time)<br/><br/>
                            <b>Policies :</b> Mermaid Liveaboards and PT Indonesia Liveaboards and its booking company CMC Marine Ltd reserve the rights
                            to alter published rates without prior notice. Invoiced bookings will remain at the invoiced rates for that cruise date/destination.
                            If the invoiced booking is transferred to another cruise/destination, the applicable rates for that cruise will apply.
                            The company reserves the right to introduce any other fees as deemed necessary eg fuel surcharges.
                            Mermaid Liveaboards/PT Indonesia Liveaboards and their booking company CMC Marine Ltd are  held accountable solely by the laws
                            of Indonesia. Any contractual laws from countries other than Indonesia are outside of our concern.
                            Any booking and/or on board cruise disputes will be discussed/reviewed only with the guest/s booking for each individual invoice.
                            Mermaid Liveaboards does not accept any legal liability for loss or damage to any luggage/dive equipment while being transferred
                            to or from the vessel nor while on board. Mermaid Liveaboards will offer any assistance possible to help with any insurance claims
                            for lost or damaged property.<br/><br/>                                
                            Lastly, Mermaid Liveaboards, PT Indonesia Liveaboards and its booking agency CMC Marine Limited , do not offer refunds in the
                            event of personal injury, airplane delay, breakdowns, weather, sickness, strikes, war, criminal acts, pandemics, quarantine, acts of god,
                            or vessel relocation due to guest evacuation for any reason or any other event whatsoever beyond the company\'s actual control.<br/><br/>                                                            
                            Regardless of which cruise you choose ... Be prepared for some of the world\'s best diving plus a maximum of comfort, service  and
                            safety on M/V Mermaid I & II - Welcome on board !
                        </td>
                    </tr>
                </tbody>
            </table>';

            //echo $html;
            $dompdf->loadHtml( $html );

            $dompdf->setPaper( 'A4', 'potrait' );

            $dompdf->render();
            
            $dompdf->stream( 'invoice-' . sanitize_title( $post->post_title ) . '.pdf' );
        }
    }

    wp_reset_postdata();
}