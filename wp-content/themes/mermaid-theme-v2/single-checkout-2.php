<?php

$form_type = 'mmc_marine';

//-- GET payment method option status
$is_cc_active     = payment_get_option( $form_type, $form_type . '_show_cc_method' );
$is_paypal_active = payment_get_option( $form_type, $form_type . '_show_paypal_method' );
$is_tf_active     = payment_get_option( $form_type, $form_type . '_show_bank_transfer_method' );

//-- GET additonal charge setting
$astatus = payment_get_option( $form_type, $form_type . '_use_additional_charge' );
$avalue  = payment_get_option( $form_type, $form_type . '_additional_charge_value' );

//-- GET currency
$paypal_currency        = payment_get_option( $form_type, $form_type . '_paypal_currency' );
$bank_transfer_currency = payment_get_option( $form_type, $form_type . '_bank_transfer_currency' );

$post_id = get_the_ID();
$post    = get_post( $post_id );

$status_payment = get_post_meta( $post_id, '_checkout_status_payment', true );
$btype          = get_post_meta( $post_id, '_checkout_booking_type', true );
$expired_date   = get_post_meta( $post_id, '_checkout_expired_date', true );
$iteneraries    = get_post_meta( $post_id, '_checkout_iteneraries', true );
$trip_code      = get_post_meta( $post_id, '_checkout_trip_code', true );
$remarks        = get_post_meta( $post_id, '_checkout_remarks', true );
$boat_id        = get_post_meta( $post_id, '_checkout_boat', true );

$travel  = get_post_meta( $post_id, '_checkout_travel_service' );
$payment = get_post_meta( $post_id, '_checkout_payment_terms' );
$cruise  = get_post_meta( $post_id, '_checkout_cruise_list' );
$scharge = get_post_meta( $post_id, '_checkout_surcharge' );

$showas   = get_post_meta( $post_id, '_checkout_surcharge_show_option', true );
$tduedate = get_post_meta( $post_id, '_checkout_travel_service_payment_date', true );
$sduedate = get_post_meta( $post_id, '_checkout_surcharge_payment_date', true );

$inv_no   = get_post_meta( $post_id, '_checkout_invoice_no', true);
$guest_nm = get_post_meta( $post_id, '_checkout_guest_name', true );

$departure_date = get_post_meta( $post_id, 'departure_date', true );
$arrival_date   = get_post_meta( $post_id, 'arrival_date', true );

//-- Count all subtotal
$ctotal_nett = 0;
$ttotal_nett = 0;
$stotal_nett = 0;

if( isset( $cruise ) && empty( $cruise ) === false )
{
    $ctotal_nett = array_sum( array_column( $cruise[0], 'total_nett' ) );
}

if( isset( $travel ) && empty( $travel ) === false )
{
    $ttotal_nett = array_sum( array_column( $travel[0], 'total_nett' ) );
}

if( isset( $scharge ) && empty( $scharge ) === false )
{
    $stotal_nett = array_sum( array_column( $scharge[0], 'total_nett' ) );
}

//-- GET total payment in EUR
$amount_eur = 0;

if( !empty( $payment ) )
{
    foreach( $payment as $pay )
    {
        foreach( $pay as $p )
        {
            if( $p['status'] == 'Awaiting Payment' )
            {
                $amount_eur += $p['total_nett'];
            }
        }
    }
}

//-- GET total payment in USD
$amount_usd = 0;

if( $amount_eur > 0 )
{
    $amount_usd = convertCurrency( 'EUR', 'USD', $amount_eur ) ;
}

//-- GET total with additional charge
$amount_eur_with_charge = $amount_eur;
$amount_usd_with_charge = $amount_usd;

if( $astatus == 1 )
{
    if( $amount_eur > 0 )
    {
        $amount_eur_with_charge = $amount_eur + ( ( $amount_eur * $avalue ) / 100 );
    }

    if( $amount_usd > 0 )
    {
        $amount_usd_with_charge = $amount_usd + ( ( $amount_usd * $avalue ) / 100 );
    }
}

get_header();

if( have_posts() )
{
    the_post();

    ?>

    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo">
                        <a href="<?php echo get_site_url(); ?>"><img data-src-static="<?php echo get_template_directory_uri()."/assets/images/mermaid-logo.png" ?>" /></a>
                    </div>
                    <?php

                    $status = 'invalid';

                    if( $inv_no == '' || $guest_nm == '' )
                    {
                        ?>
                        <h1 class="title-payment heading-default"><?= get_option( 'title_page_payment_link_invalid' ) ?></h1>
                        <p><?= nl2br( get_option( 'description_page_payment_link_invalid' ) ) ?></p>
                        <?php
                    }
                    else
                    {
                        if( strtotime( $expired_date ) <= strtotime( date( 'Y-m-d' ) ) )
                        {
                            ?>
                            <h1 class="title-payment heading-default"><?= get_option( 'title_page_payment_link_expired' ) ?></h1>
                            <p><?= nl2br( get_option( 'description_page_payment_link_expired' ) ) ?></p>
                            <?php
                        }
                        else
                        {
                            if( strtolower( $status_payment ) == 'waiting payment' )
                            {                                
                                $status = 'valid';
                                
                                if( !empty( $inv_no ) && !empty( $guest_nm ) )
                                {
                                    ?>
                                    <div class="row">
                                        <div class="col-md-6"><h4>Invoice Number</h4></div>
                                        <div class="col-md-6"><h4>Guest Name</h4></div>
                                       
                                         <div class="col-md-6"><h1><?= $inv_no ?></h1></div>
                                         <div class="col-md-6"><h1><?= $guest_nm ?></h1></div>
                                    </div>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <h4>Invoice Number</h4>
                                    <h1><?= get_the_title() ?></h1>
                                    <?php
                                }
                            }
                            else if( strtolower( $status_payment ) == 'waiting verification' )
                            {
                                ?>
                                <h1 class="title-payment heading-default"><?= get_option( 'title_page_payment_sent' ) ?></h1>
                                <p><?= nl2br( get_option( 'description_page_payment_sent' ) ) ?></p>
                                <?php
                            }
                            else
                            {
                                ?>
                                <h1 class="title-payment heading-default"><?= get_option( 'title_page_payment_link_invalid' ) ?></h1>
                                <p><?= nl2br( get_option( 'description_page_payment_link_invalid' ) ) ?></p>
                                <?php
                            }
                        }
                    }

                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php

    if( $status == 'valid' )
    {
        ?>
        <section class="wrap-content-checkout">
            <div class="container">
                <!-- ====== MOBILE ====== -->
                <div class="row">
                    <div class="col-md-12 container-table-checkout-mobile">
                        <?php

                        if( $ctotal_nett > 0 )
                        {
                            $trip_c = json_decode( base64_decode( $trip_code ) );

                            if( $trip_c !== null && json_last_error() === JSON_ERROR_NONE )
                            {
                                $trip_code = $trip_c->post_title;
                            }
                            
                            ?>
                            <div class="box-table-checkout">
                                <div class="row row-m-0">
                                    <div class="col-md-4 col-mb-0">
                                        <p><b>Trip Code :</b><br><?= $trip_code ?></p>
                                    </div>
                                    <div class="col-md-4 col-mb-0">
                                        <p><b>Trip Date :</b><br><?= sprintf( '%s - %s', $departure_date, $arrival_date) ?></p>
                                    </div>
                                    <div class="col-md-4 col-mb-0">
                                        <p><b>Destination :</b> <br> <?= html_entity_decode( get_the_title( $iteneraries ), ENT_QUOTES, 'UTF-8' ) ?></p>
                                    </div>
                                </div>
                                <?php

                                foreach( $cruise as $list )
                                {
                                    foreach( $list as $c )
                                    {
                                        $total_nett         = isset( $c[ 'total_nett' ] ) ? number_format( $c[ 'total_nett' ], 0 ) : '';
                                        $nett               = isset( $c[ 'nett' ] ) ? number_format( $c[ 'nett' ], 0 ) : '';
                                        $gross              = isset( $c[ 'gross' ] ) ? number_format( $c[ 'gross'], 0 ) : '';

                                        $cruise_details     = isset( $c[ 'cruise_details' ] ) ? $c[ 'cruise_details' ] : '';
                                        $cabin_type         = isset( $c[ 'cabin_type' ] ) ? $c[ 'cabin_type' ] : '';
                                        $cabin_type_options = isset( $c[ 'cabin_type_options' ] ) ? $c[ 'cabin_type_options' ] : '';

                                        $no_guest           = isset( $c[ 'no_guest' ] ) ? $c[ 'no_guest' ] : '';
                                        
                                        if( $cabin_type_options == 0 )
                                        {
                                            $cabin_options = 'Double';
                                        }   
                                        elseif( $cabin_type_options == 1 )
                                        {
                                             $cabin_options = 'Twin Share';
                                        }   
                                        elseif( $cabin_type_options == 2 )
                                        {
                                             $cabin_options = 'Single Surcharge';
                                        }
                                        else
                                        {
                                             $cabin_options = '';
                                        }

                                        ?>
                                        <div class="wrap-list-order">
                                            <div class="head-list-order clearfix">
                                                <div class="col-order">
                                                    <span>Cruise Length</span>
                                                    <p><?php echo $cruise_details; ?></p>
                                                </div>
                                                <div class="col-order">
                                                    <div class="show-when-expand">
                                                        <span>Cabin Type</span>
                                                        <p><?php echo $cabin_type; ?> <?= $cabin_options ?></p>
                                                    </div>
                                                    <div class="hide-when-expand active">
                                                        <span>.</span>
                                                        <a href="" class="see-details">See Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body-list-order clearfix">
                                                <div class="col-order">
                                                    <span>No. Guest</span>
                                                    <p><?php echo $no_guest; ?></p>
                                                </div>
                                                <div class="col-order">
                                                    <span>Gross (€)</span>
                                                    <p><?php echo $gross; ?></p>
                                                </div>
                                                <?php 

                                                if( isset( $c[ 'repeat_guest_disc' ] ) && !empty( $c[ 'repeat_guest_disc' ] ) )
                                                {
                                                    $discount_guest     = $c[ 'disc_type' ];
                                                    $discount_guest_val = floatval( $c[ 'repeat_guest_disc' ] );

                                                    if( !empty( $discount_guest ) && $discount_guest_val > 0 )
                                                    {
                                                        ?>
                                                        <div class="col-order">
                                                            <span><?= $discount_guest ?></span>
                                                            <p><?php echo $discount_guest_val; ?>%</p>
                                                        </div>
                                                        <?php
                                                    }
                                                }

                                                if( $btype == '1' || $btype == '2' )
                                                {
                                                    if( isset( $c[ 'other_disc_value' ] ) && !empty( $c[ 'other_disc_value' ] ) )
                                                    {
                                                        $discount_other     = $c[ 'other_disc_type' ];
                                                        $discount_other_val = floatval( $c[ 'other_disc_value' ] );

                                                        if( !empty( $discount_other ) && $discount_other_val > 0 )
                                                        {
                                                            ?>
                                                            <div class="col-order">
                                                                <span><?= $discount_other ?></span>
                                                                <p><?php echo $discount_other_val; ?>%</p>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>
                                                <div class="col-order">
                                                    <span>Nett (€)</span>
                                                    <p><?php echo $nett; ?></p>
                                                </div>

                                                <div class="foot-list-order clearfix">
                                                    <div class="col-order">
                                                        <p>Total Nett (€)</p>
                                                    </div>
                                                    <div class="col-order clearfix">
                                                        <p><?php echo $total_nett; ?></p>
                                                        <img src="<?php echo THEME_URL_ASSETS .'/images/icon-arrow-top.svg'; ?>" class="colapse-up" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }

                                ?>
                            </div>
                            <?php
                        }

                        if( $ttotal_nett > 0 )
                        {
                            ?>
                            <h2 class="top-box">Travel Service</h2>
                            <div class="box-table-checkout">
                                <?php

                                foreach( $travel as $list )
                                {
                                    foreach( $list as $t )
                                    {
                                        $tnett   = empty( $t[ 'total_nett' ] ) ? '0' : number_format( $t[ 'total_nett' ], 0 );
                                        $netteur = empty( $t[ 'nett_euro' ] ) ? '-' : number_format( $t[ 'nett_euro' ], 0 );
                                        $nettidr = empty( $t[ 'nett_idr' ] ) ? '0' : number_format( $t[ 'nett_idr' ], 0 );

                                        $date    = empty( $t[ 'date' ] ) ? '-' : date( 'F d Y', strtotime( $t[ 'date' ] ) );
                                        $guests  = empty( $t[ 'guests' ] ) ? '0' : $t[ 'guests' ];

                                        if( empty( $t[ 'service_route' ] ) && empty( $t[ 'hotel_flight' ] ) )
                                        {
                                            $sroute = '-';
                                        }
                                        elseif( empty( $t[ 'service_route' ] ) && !empty( $t[ 'hotel_flight' ] ) )
                                        {
                                            $sroute = $t[ 'hotel_flight' ];
                                        }
                                        elseif( !empty( $t[ 'service_route' ] ) && empty( $t[ 'hotel_flight' ] ) )
                                        {
                                            $sroute = $t[ 'service_route' ];
                                        }
                                        else
                                        {
                                            $sroute = $t[ 'service_route' ] . '/' . $t[ 'hotel_flight' ];
                                        }

                                        ?>
                                        <div class="wrap-list-order">
                                            <div class="head-list-order clearfix">
                                                <div class="col-order">
                                                    <span>Service</span>
                                                    <p><?php echo $sroute; ?></p>
                                                </div>
                                                <div class="col-order">
                                                    <div class="show-when-expand">
                                                        <span>Date</span>
                                                        <p><?php echo $date; ?></p>
                                                    </div>
                                                    <div class="hide-when-expand active">
                                                        <span>.</span>
                                                        <a href="" class="see-details">See Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body-list-order clearfix">
                                                <div class="col-order">
                                                    <span>No. Unit</span>
                                                    <p><?php echo $guests; ?></p>
                                                </div>
                                                <div class="col-order">
                                                    <span>Nett. (IDR)</span>
                                                    <p><?php echo $nettidr; ?></p>
                                                </div>
                                                <div class="col-order">
                                                    <span>Nett (€)</span>
                                                    <p><?php echo $netteur; ?></p>
                                                </div>

                                                <div class="foot-list-order clearfix">
                                                    <div class="col-order">
                                                        <p>Total Nett (€)</p>
                                                    </div>
                                                    <div class="col-order clearfix">
                                                        <p><?php echo $tnett; ?></p>
                                                        <img src="<?php echo THEME_URL_ASSETS .'/images/icon-arrow-top.svg'; ?>" class="colapse-up" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }

                                ?>
                            </div>
                            <?php
                        }

                        if( $stotal_nett > 0 )
                        {
                            ?>
                            <h2 class="top-box">Surcharge</h2>
                            <div class="box-table-checkout">
                                <?php

                                foreach( $scharge as $list )
                                {
                                    foreach( $list as $t )
                                    {
                                        $tnett   = empty( $t[ 'total_nett' ] ) ? '0' : number_format( $t[ 'total_nett' ], 0 );
                                        $netteur = empty( $t[ 'nett_euro' ] ) ? '-' : number_format( $t[ 'nett_euro' ], 0 );
                                        $nettidr = empty( $t[ 'nett_idr' ] ) ? '0' : number_format( $t[ 'nett_idr' ], 0 );

                                        $date    = empty( $t[ 'date' ] ) ? '-' : date( 'F d Y', strtotime( $t[ 'date' ] ) );
                                        $service = empty( $t[ 'service' ] ) ? '-' : $t[ 'service' ];
                                        $units   = empty( $t[ 'units' ] ) ? '0' : $t[ 'units' ];

                                        ?>
                                        <div class="wrap-list-order">
                                            <div class="head-list-order clearfix">
                                                <div class="col-order">
                                                    <span>Service</span>
                                                    <p><?php echo $service; ?></p>
                                                </div>
                                                <div class="col-order">
                                                    <div class="show-when-expand">
                                                        <span>Date</span>
                                                        <p><?php echo $date; ?></p>
                                                    </div>
                                                    <div class="hide-when-expand active">
                                                        <span>.</span>
                                                        <a href="" class="see-details">See Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body-list-order clearfix">
                                                <div class="col-order">
                                                    <span>No. Unit</span>
                                                    <p><?php echo $units; ?></p>
                                                </div>
                                                <div class="col-order">
                                                    <span>Nett. (IDR)</span>
                                                    <p><?php echo $nettidr; ?></p>
                                                </div>
                                                <div class="col-order">
                                                    <span>Nett (€)</span>
                                                    <p><?php echo $netteur; ?></p>
                                                </div>

                                                <div class="foot-list-order clearfix">
                                                    <div class="col-order">
                                                        <p>Total Nett (€)</p>
                                                    </div>
                                                    <div class="col-order clearfix">
                                                        <p><?php echo $tnett; ?></p>
                                                        <img src="<?php echo THEME_URL_ASSETS .'/images/icon-arrow-top.svg'; ?>" class="colapse-up" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }

                                ?>
                            </div>
                            <?php
                        }

                        if( empty( $payment ) === false )
                        {
                            ?>
                            <div class="box-table-checkout">
                                <?php

                                if( empty( $remarks ) === false )
                                {
                                    ?>
                                    <div class="wrap-list-order">
                                        <p>Remarks</p>
                                        <p><?php echo $remarks; ?></p>
                                    </div>
                                    <?php
                                }

                                foreach( $payment as $list )
                                {
                                    foreach( $list as $p )
                                    {
                                        if( $p[ 'total_nett' ] > 0 )
                                        {
                                            ?>
                                            <div class="wrap-list-order">
                                                <div class="payment-terms clearfix">
                                                    <div class="col-order">
                                                        <p><?= $p[ 'label' ]  ?></p>
                                                    </div>
                                                    <div class="col-order">
                                                        <span>Due Date</span>
                                                        <p><?= $p[ 'due_date' ] ?></p>
                                                    </div>
                                                    <div class="col-order"></div>
                                                    <div class="foot-list-order clearfix">
                                                        <div class="col-order">
                                                            <p>Total Nett (€)</p>
                                                        </div>
                                                        <div class="col-order clearfix">
                                                            <p><?= number_format( $p[ 'total_nett'], 0 ) ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                }

                                ?>
                            </div>
                            <?php
                        }
                        else if( empty( $remarks ) === false )
                        {
                            ?>
                            <div class="box-table-checkout">
                                <div class="wrap-list-order">
                                    <p>Remarks</p>
                                    <p><?php echo $remarks; ?></p>
                                </div>
                            </div>
                            <?php
                        }

                        ?>
                    </div>
                </div>

                <!-- ====== DESKTOP ====== -->
                <div class="row">
                    <div class="col-lg-8 col-md-12 container-table-checkout">
                        <div class="booking-detail border-blue">
                            <?php

                            if( $ctotal_nett > 0 )
                            {
                                $trip_c = json_decode( base64_decode( $trip_code ) );

                                if( $trip_c !== null && json_last_error() === JSON_ERROR_NONE )
                                {
                                    $trip_code = $trip_c->post_title;
                                }
                                
                                ?>
                                <div class="row row-m-0">
                                    <div class="col-md-4 col-mb-0">
                                        <p><b>Trip Code :</b><br><?= $trip_code ?></p>
                                    </div>
                                    <div class="col-md-4 col-mb-0">
                                        <p><b>Trip Date :</b><br><?= sprintf( '%s - %s', $departure_date, $arrival_date) ?></p>
                                    </div>
                                    <div class="col-md-4 col-mb-0">
                                        <p><b>Destination :</b> <br> <?= html_entity_decode( get_the_title( $iteneraries ), ENT_QUOTES, 'UTF-8' ) ?></p>
                                    </div>
                                </div>
                                <?php
                            }

                            ?>
                            <div class="con-book-detail wrap-table-default">
                                <table>
                                    <?php

                                    if( $ctotal_nett > 0 )
                                    {
                                        ?>
                                        <tr>
                                            <td style="background-color: #FFF; padding: 0;">
                                                <table>
                                                    <?php

                                                    $disc_txt = array();

                                                    foreach( $cruise as $list )
                                                    {
                                                        foreach( $list as $ckey => $c )
                                                        {
                                                            if( isset( $c[ 'repeat_guest_disc' ] ) && !empty( $c[ 'repeat_guest_disc' ] ) )
                                                            {
                                                                array_push( $disc_txt, $c[ 'disc_type' ] );
                                                            }

                                                            if( $btype == '1' || $btype == '2' )
                                                            {
                                                                if( isset( $c[ 'other_disc_value' ] ) && !empty( $c[ 'other_disc_value' ] ) )
                                                                {
                                                                    array_push( $disc_txt, $c[ 'other_disc_type' ] );
                                                                }
                                                            }
                                                        }
                                                    }

                                                    $disc_txt = array_unique( $disc_txt );

                                                    ?>                                        
                                                    <thead>
                                                        <tr>
                                                            <th class="align-left" width="180">Cruise Length</th>
                                                            <th class="align-left" width="180">Cabin Type</th>
                                                            <th class="align-right">No. Guest</th>
                                                            <th class="align-right">Gross (€)</th>
                                                            <?php 

                                                            foreach( $disc_txt as $dic_lbl )
                                                            {
                                                                ?>
                                                                <th><?= $dic_lbl ?></th>
                                                                <?php
                                                            }

                                                            ?>
                                                            <th class="align-right">Nett (€)</th>
                                                            <th class="align-right" width="130">Total Nett (€)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php

                                                        foreach( $cruise as $list )
                                                        {
                                                            foreach( $list as $c )
                                                            {
                                                                $tnett = isset( $c[ 'total_nett' ] ) ? number_format( $c[ 'total_nett' ], 0 ) : '';
                                                                $nett  = isset( $c[ 'nett' ] ) ? number_format( $c[ 'nett' ], 0 ) : '';
                                                                $gross = isset( $c[ 'gross' ] ) ? number_format( $c[ 'gross'], 0 ) : '';

                                                                $cruise_details     = isset( $c[ 'cruise_details' ] ) ? $c[ 'cruise_details' ] : '';
                                                                $cabin_type         = isset( $c[ 'cabin_type' ] ) ? $c[ 'cabin_type' ] : '';
                                                                $cabin_type_options = isset( $c[ 'cabin_type_options' ] ) ? $c[ 'cabin_type_options' ] : '';
                                                               
                                                                $no_guest = isset( $c[ 'no_guest' ] ) ? $c[ 'no_guest' ] : '';
                                                                
                                                                if( $cabin_type_options == 0 )
                                                                {
                                                                    $cabin_options = 'Double';
                                                                }   
                                                                elseif( $cabin_type_options == 1)
                                                                {
                                                                     $cabin_options = 'Twin Share';
                                                                }   
                                                                elseif( $cabin_type_options == 2 )
                                                                {
                                                                     $cabin_options = 'Single Surcharge';
                                                                }
                                                                else
                                                                {
                                                                     $cabin_options = '';
                                                                }
                                                                
                                                                ?>
                                                                <tr>
                                                                    <td valign="top" class="align-left"><?= $cruise_details ?></td>
                                                                    <td valign="top" class="align-left"><?= $cabin_type ?> <?= $cabin_options ?></td>
                                                                    <td valign="top" class="align-right"><?= $no_guest ?></td>
                                                                    <td valign="top" class="align-right"><?= $gross ?></td>
                                                                    <?php 

                                                                    foreach( $disc_txt as $disc_lbl )
                                                                    {
                                                                        $disc_val = array();

                                                                        if( $disc_lbl == $c[ 'disc_type' ] && ( isset( $c[ 'repeat_guest_disc' ] ) && !empty( $c[ 'repeat_guest_disc' ] ) ) )
                                                                        {
                                                                            $disc_val[] = floatval( $c[ 'repeat_guest_disc' ] ) . '%';
                                                                        }

                                                                        if( $disc_lbl == $c[ 'other_disc_type' ] && ( isset( $c[ 'other_disc_value' ] ) && !empty( $c[ 'other_disc_value' ] ) ) )
                                                                        {
                                                                            $disc_val[] = floatval( $c[ 'other_disc_value' ] ) . '%';
                                                                        }
                                                                        
                                                                        ?>
                                                                        <td valign="top" class="align-right"><?= implode(' + ', $disc_val); ?></td>
                                                                        <?php
                                                                    }

                                                                    ?>                                                 
                                                                    <td valign="top" class="align-right"><?= $nett ?></td>
                                                                    <td valign="top" class="align-right"><?= $tnett ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }

                                                        ?>
                                                    </tbody> 
                                                    <?php

                                                    if( empty( $payment ) === false )
                                                    {     
                                                        ?>                            
                                                        <tfoot>
                                                            <?php

                                                            $col = 5 + count( $disc_txt );

                                                            foreach( $payment as $list )
                                                            {
                                                                foreach( $list as $p )
                                                                {
                                                                    if( isset( $p[ 'total_deposit' ] ) )
                                                                    {
                                                                        $total_deposit = $p[ 'total_deposit' ];
                                                                    }
                                                                    else
                                                                    {
                                                                        $total_deposit = ( $ctotal_nett * $p['percentage'] ) / 100;
                                                                    }

                                                                    ?>
                                                                    <tr style="background-color: #f9f9f9;">
                                                                        <td colspan="<?= $col ?>" class="align-right"><?= $p[ 'label' ] ?> :</td>
                                                                        <td class="align-right" style="color: red;"><?= number_format( $total_deposit, 0 ) ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }

                                                            ?>
                                                        </tfoot>
                                                        <?php
                                                    }

                                                    ?>
                                                </table>
                                            </td>
                                        </tr>                                 
                                        <?php
                                    }
                                    
                                    if( $ttotal_nett > 0 )
                                    {
                                        ?>
                                        <tr>
                                            <td style="background-color: #FFF; padding: 0;">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th class="align-left heading" colspan="6">Travel Services :</th>
                                                        </tr>
                                                        <tr>
                                                            <th class="align-left" width="180">Date</th>
                                                            <th class="align-left" width="180">Service</th>
                                                            <th class="align-right">No. Unit</th>
                                                            <th class="align-right">Nett (IDR)</th>
                                                            <th class="align-right">Nett (€)</th>
                                                            <th class="align-right" width="130">Total Nett (€)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php

                                                        foreach( $travel as $list )
                                                        {
                                                            foreach( $list as $t )
                                                            {
                                                                $tnett   = empty( $t[ 'total_nett' ] ) ? '0' : number_format( $t[ 'total_nett' ], 0 );
                                                                $netteur = empty( $t[ 'nett_euro' ] ) ? '-' : number_format( $t[ 'nett_euro' ], 0 );
                                                                $nettidr = empty( $t[ 'nett_idr' ] ) ? '0' : number_format( $t[ 'nett_idr' ], 0 );

                                                                $date    = empty( $t[ 'date' ] ) ? '-' : date( 'F d Y', strtotime( $t[ 'date' ] ) );
                                                                $guests  = empty( $t[ 'guests' ] ) ? '0' : $t[ 'guests' ];

                                                                if( empty( $t[ 'service_route' ] ) && empty( $t[ 'hotel_flight' ] ) )
                                                                {
                                                                    $sroute = '-';
                                                                }
                                                                elseif( empty( $t[ 'service_route' ] ) && !empty( $t[ 'hotel_flight' ] ) )
                                                                {
                                                                    $sroute = $t[ 'hotel_flight' ];
                                                                }
                                                                elseif( !empty( $t[ 'service_route' ] ) && empty( $t[ 'hotel_flight' ] ) )
                                                                {
                                                                    $sroute = $t[ 'service_route' ];
                                                                }
                                                                else
                                                                {
                                                                    $sroute = $t[ 'service_route' ] . '/' . $t[ 'hotel_flight' ];
                                                                }

                                                                ?>
                                                                <tr>
                                                                    <td valign="top" class="align-left"><?= $date ?></td>
                                                                    <td valign="top" class="align-left"><?= $sroute ?></td>
                                                                    <td valign="top" class="align-right"><?= $guests ?></td>
                                                                    <td valign="top" class="align-right"><?= $nettidr ?></td>
                                                                    <td valign="top" class="align-right"><?= $netteur ?></td>
                                                                    <td valign="top" class="align-right"><?= $tnett ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }

                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr style="background-color: #f9f9f9;">
                                                            <?php

                                                            if( $ctotal_nett <= 0 && $stotal_nett <= 0 && empty( $payment ) === false )
                                                            { 
                                                                foreach( $payment as $list )
                                                                {
                                                                    foreach( $list as $p )
                                                                    {
                                                                        ?>
                                                                        <td colspan="5" class="align-right"><?= $p[ 'label' ] ?> :</td>
                                                                        <td class="align-right" style="color: red;"><?= number_format( $p[ 'total_nett' ], 0 ) ?></td>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                <td colspan="5" class="align-right">Subtotal :</td>
                                                                <td class="align-right"><?= number_format( $ttotal_nett, 0 ) ?></td>
                                                                <?php
                                                            }

                                                            ?>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    
                                    if( $stotal_nett > 0 )
                                    {
                                        ?>
                                        <tr>
                                            <td style="background-color: #FFF; padding: 0;">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th class="align-left heading" colspan="6">Surcharge :</th>
                                                        </tr>
                                                        <tr>
                                                            <th class="align-left" width="180">Date</th>
                                                            <th class="align-left" width="180">Service</th>
                                                            <th class="align-right">No. Unit</th>
                                                            <th class="align-right">Nett (IDR)</th>
                                                            <th class="align-right">Nett (€)</th>
                                                            <th class="align-right" width="130">Total Nett (€)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php

                                                        foreach( $scharge as $list )
                                                        {
                                                            foreach( $list as $t )
                                                            {
                                                                $tnett   = empty( $t[ 'total_nett' ] ) ? '0' : number_format( $t[ 'total_nett' ], 0 );
                                                                $netteur = empty( $t[ 'nett_euro' ] ) ? '-' : number_format( $t[ 'nett_euro' ], 0 );
                                                                $nettidr = empty( $t[ 'nett_idr' ] ) ? '0' : number_format( $t[ 'nett_idr' ], 0 );

                                                                $date    = empty( $t[ 'date' ] ) ? '-' : date( 'F d Y', strtotime( $t[ 'date' ] ) );
                                                                $service = empty( $t[ 'service' ] ) ? '-' : $t[ 'service' ];
                                                                $units   = empty( $t[ 'units' ] ) ? '0' : $t[ 'units' ];

                                                                ?>
                                                                <tr>
                                                                    <td valign="top" class="align-left"><?= $date ?></td>
                                                                    <td valign="top" class="align-left"><?= $service ?></td>
                                                                    <td valign="top" class="align-right"><?= $units ?></td>
                                                                    <td valign="top" class="align-right"><?= $nettidr ?></td>
                                                                    <td valign="top" class="align-right"><?= $netteur ?></td>
                                                                    <td valign="top" class="align-right"><?= $tnett ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }

                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr style="background-color: #f9f9f9;">
                                                            <?php

                                                            if( $ctotal_nett <= 0 && $ttotal_nett <= 0 && empty( $payment ) === false )
                                                            { 
                                                                foreach( $payment as $list )
                                                                {
                                                                    foreach( $list as $p )
                                                                    {
                                                                        ?>
                                                                        <td colspan="5" class="align-right"><?= $p[ 'label' ] ?> :</td>
                                                                        <td class="align-right" style="color: red;"><?= number_format( $p[ 'total_nett' ], 0 ) ?></td>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                <td colspan="5" class="align-right">Subtotal :</td>
                                                                <td class="align-right"><?= number_format( $stotal_nett, 0 ) ?></td>
                                                                <?php
                                                            }

                                                            ?>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                    }

                                    //-- Check if surcharge include as invoice
                                    if( $showas == 1 )
                                    {
                                        $grand_total = $ctotal_nett + $ttotal_nett + $stotal_nett;
                                    }
                                    else
                                    {
                                        $grand_total = $ctotal_nett + $ttotal_nett;
                                    }

                                    ?>
                                    <tr style="border-bottom: 1px solid #e6e9f7;">
                                        <td style="background-color: #f4f8fb; padding: 0;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: #FFF; padding: 0;">
                                            <table>
                                                <tbody>
                                                    <tr style="background-color: #f9f9f9;">
                                                        <td colspan="3" class="align-right" style="font-weight: 500;"></td>
                                                    </tr>
                                                    
                                                    <?php

                                                    if( $ctotal_nett <= 0 && $ttotal_nett > 0 && $stotal_nett > 0 && empty( $payment ) === false )
                                                    { 
                                                        foreach( $payment as $list )
                                                        {
                                                            foreach( $list as $p )
                                                            {
                                                                ?>
                                                                <tr style="background-color: #f9f9f9;">
                                                                    <td  colspan="2" class="align-right" style="font-weight: 500;"><?= $p[ 'label' ] ?> :</td>
                                                                    <td class="align-right" style="color: red;"><?= number_format( $p[ 'total_nett' ], 0 ) ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                    }

                                                    if( $ctotal_nett > 0 )
                                                    {
                                                        ?>
                                                        <tr style="background-color: #f9f9f9;">
                                                            <td colspan="2" class="align-right" style="font-weight: 500;">Cruise :</td>
                                                            <td class="align-right" width="130"><?= number_format( $ctotal_nett, 0 ) ?></td>
                                                        </tr>
                                                        <?php
                                                    }

                                                    if( $ttotal_nett > 0 )
                                                    {
                                                        ?>
                                                        <tr style="background-color: #ffffff; <?= $ctotal_nett > 0 ? 'border-top: 1px solid #e6e9f7;' : '' ?>">
                                                            <td colspan="2" class="align-right" style="font-weight: 500;">Service :</td>
                                                            <td class="align-right" width="130"><?= number_format( $ttotal_nett, 0 ) ?></td>
                                                        </tr>
                                                        <?php
                                                    }

                                                    if( $stotal_nett > 0 && $showas == 1 )
                                                    {
                                                        ?>
                                                        <tr style="background-color: #ffffff; <?= $ctotal_nett > 0 || $ttotal_nett > 0 ? 'border-top: 1px solid #e6e9f7;' : '' ?>">
                                                            <td colspan="2" class="align-right" style="font-weight: 500;">Surcharge :</td>
                                                            <td class="align-right" width="130"><?= number_format( $stotal_nett, 0 ) ?></td>
                                                        </tr>
                                                        <?php
                                                    }

                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="background-color: #f9f9f9;">
                                                        <td colspan="2" class="align-right">Grand Total:</td>
                                                        <td class="align-right" width="130"><?= number_format( $grand_total, 0 ) ?></td>
                                                    </tr>
                                                    <?php

                                                    if( empty( $remarks ) === false )
                                                    {
                                                        ?>
                                                        <tr style="background-color: #ffffff; border-top: 1px solid #e6e9f7;">
                                                            <td colspan="3" class="align-left" style="font-weight: normal; font-size: 14px; font-style: italic;"><b style="font-weight: 500;">Remarks:</b><br/><?= $remarks ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    
                                                    ?>
                                                </tfoot>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 container-payment-option">
                        <div class="form-checkout border-blue">
                            <h2>Payment Option for Reservation</h2>
                            <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
                                <div class="wrap-choose-payment-type">
                                    <?php

                                    if( $is_cc_active == 1 )
                                    {
                                        ?>
                                        <div class="row list-choose-payment">
                                            <div class="col-md-12">
                                                <div class="group">
                                                    <label class="container-radio">
                                                        Credit Card
                                                        <input type="radio" name="payment" class="cc-pay" value="cc">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <img src="<?php echo THEME_URL_ASSETS.'/images/icon payment_stripe.svg'; ?>" class="icon-cc" />
                                            </div>
                                            <div class="col-md-12 intructions cc-inst">
                                                <h4>Instructions</h4>
                                                <?php

                                                if( $astatus == 1 )
                                                {
                                                    ?>
                                                    <p>Select this option, choose your currency, and click continue to pay with your credit card. Note a small fee of <?= $avalue ?>% applies.</p>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <p>Select this option, choose your currency, and click continue to pay with your credit card.</p>
                                                    <?php
                                                }

                                                ?>
                                                <select name="cc_currency" class="selectize" autocomplete="off">
                                                    <option value="eur">EUR</option>
                                                    <option value="usd">USD</option>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    if( $is_paypal_active == 1 )
                                    {
                                        ?>
                                        <div class="row list-choose-payment">
                                            <div class="col-md-12">
                                                <div class="group">
                                                    <label class="container-radio">
                                                        Paypal
                                                        <input type="radio" name="payment" value="paypal">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <img src="<?php echo THEME_URL_ASSETS.'/images/icon_payment_paypal.svg'; ?>" class="icon-paypal" />
                                            </div>
                                            <div class="col-md-12 intructions paypal-inst">
                                                <h4>Instructions</h4>
                                                <?php

                                                if( $astatus == 1 )
                                                {
                                                    ?>
                                                    <p>Select this option and click continue to pay with your Paypal account. Note a small fee of <?= $avalue ?>% applies.</p>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <p>Select this option and click continue to pay with your Paypal account</p>
                                                    <?php
                                                }

                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    if( $is_tf_active == 1 )
                                    {
                                        ?>
                                        <div class="row list-choose-payment">
                                            <div class="col-md-12">
                                                <div class="group">
                                                    <label class="container-radio">
                                                        Bank Transfer
                                                        <input type="radio" name="payment" value="bank-trf">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <img src="<?php echo THEME_URL_ASSETS.'/images/icon_payment_bank_transfer.svg'; ?>" class="icon-bank-trf" />
                                                <input type="hidden" name="key" value="<?php echo $post->post_name; ?>" />
                                            </div>
                                            <div class="col-md-12 intructions bank-trf-inst">
                                                <h4>Instructions</h4>
                                                <p>Select this option and click Continue to be directed to our Bank Transfer details page.</p>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    ?>
                                </div>

                                <div class="row wrap-button-continue">
                                    <div class="sr-only">
                                        <input type="text" name="bank_transfer_currency" value="<?php echo $bank_transfer_currency ?>" />
                                        <input type="text" name="amount_eur" value="<?php echo $amount_eur_with_charge; ?>" />
                                        <input type="text" name="amount_usd" value="<?php echo $amount_usd_with_charge; ?>" />
                                        <input type="text" name="paypal_currency" value="<?php echo $paypal_currency; ?>" />
                                        <input type="text" name="post_type" value="<?php echo $post->post_type; ?>">
                                        <input type="text" name="form_type" value="<?php echo $form_type; ?>">
                                        <input type="text" name="post_id" value="<?php echo $post_id; ?>">
                                        <input type="text" name="action" value="checkout_payment">
                                    </div>
                                    <div class="col-md-12">
                                        <p>Amount due now</p>
                                        <h4 class="all-total-amount"
                                            data-amount-eur-no-charge="<?php echo '€ '.number_format( $amount_eur, 2 ); ?>"
                                            data-amount-usd-no-charge="<?php echo '$ '.number_format( $amount_usd, 2 ); ?>"
                                            data-amount-eur-with-charge="<?php echo '€ '.number_format( $amount_eur_with_charge, 2 ); ?>"
                                            data-amount-usd-with-charge="<?php echo '$ '.number_format( $amount_usd_with_charge, 2 ); ?>">
                                            <?php echo '€ '.number_format( $amount_eur, 2 ); ?>
                                        </h4>
                                        <button type="submit" class="button-default">Continue</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }
}

?>

<?php get_footer(); ?>