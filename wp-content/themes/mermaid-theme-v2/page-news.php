<?php
/*
Template Name: News Page
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>

    <?php include_once "layout/hero.php"; ?>

    <?php
        // INISIALISASI VARIBLE PAGE
        $title   = get_the_title();
        $title   = empty($title) ? '': '<h1 class="heading-default">'.$title.'</h1>';
        $content = get_the_content();
        $content = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $content);
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = empty($content) ? '': '<div class="desc-about">'.$content.'</div>';
    ?>

    <!-- SECTION WELCOME -->
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        echo $title;
                        echo $content;
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="wrap-list-news">
        <div class="container-mermaid">
            <div class="row">
                <?php
                // 2 news top
                $args_top = array(
                    'post_status'       => 'publish',
                    'post_type'         => 'post',
                    'posts_per_page'    => 2,
                    'offset'            => 0,
                    'orderby'           => 'publish_date',
                    'order'             => 'DESC',
                    'category_name'     => 'news-events'
                );
                $query_top = new WP_Query($args_top);

                while ($query_top->have_posts()):
                    $query_top->the_post();

                    $post_id            = get_the_ID();
                    $post_link          = get_permalink();
                    $post_date          = date("d M Y", strtotime(get_the_date()));
                    $post_title         = get_the_title();
                    $post_content       = wp_strip_all_tags(get_the_content());
                    $post_content       = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
                    $post_content       = apply_filters('the_content', $post_content);
                    $post_content       = str_replace(']]>', ']]&gt;', $post_content);
                    $post_content       = strlen($post_content) > 150 ? rtrim(substr($post_content, 0, 180)).'...': $post_content;
                    $post_image         = get_the_post_thumbnail_url($post_id, 'large');
                    $post_image_medium  = get_the_post_thumbnail_url($post_id, 'medium_large');
                    $youtube_video      = get_post_meta( get_the_ID(), '_post_youtube_video', 1 );
                ?>
                    <div class="col-md-6 list-news">
                        <?php if(!empty($youtube_video)): ?>
                            <div class="container-youtube-video">
                                <a href="<?php echo $youtube_video; ?>" class="fancy-group">
                                    <div class="thumb-img">
                                        <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $post_image ?>" data-src-small="<?php echo $post_image_medium ?>" alt="">
                                        <div class="icon-play" data-play="0" data-youtube-code="<?php echo $youtube_video_code; ?>"></div>
                                    </div>
                                </a>
                                <div class="video-background" data-play="0" data-youtube-code="<?php echo $youtube_video_code; ?>">
                                    <div class="video-foreground"></div>
                                </div>
                            </div>
                        <?php else: ?>
                            <a href="<?php echo $post_link; ?>"><img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $post_image ?>" data-src-small="<?php echo $post_image_medium ?>" alt=""></a>
                        <?php endif; ?>

                        <div class="detail-list">
                            <!--h5><?php echo $post_date; ?></h5-->
                            <a href="<?php echo $post_link; ?>"><h3><?php echo $post_title; ?></h3></a>
                            <?php echo $post_content; ?>
                            <!--h6>by <?php //echo ucwords(get_the_author()); ?></h6-->
                        </div>
                    </div>
                <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div>


            <div class="row list-blue-default infinite-scrolling"> 
                <?php
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                
                $args = array(
                    'post_status'       => 'publish',
                    'post_type'         => 'post',
                    'posts_per_page'    => 12,
                    'orderby'           => 'publish_date',
                    'order'             => 'DESC',
                    'paged'             => $paged,
                    'category_name'     => 'news-events'
                );
                $query = new WP_Query($args);

                $i = 1;
                while ($query->have_posts()): 
                    $query->the_post();

                    $post_id            = get_the_ID();
                    $post_link          = get_permalink();
                    $post_date          = date("d M Y", strtotime(get_the_date()));
                    $post_title         = get_the_title();
                    $post_content       = wp_strip_all_tags(get_the_content());
                    $post_content       = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
                    $post_content       = apply_filters('the_content', $post_content);
                    $post_content       = str_replace(']]>', ']]&gt;', $post_content);
                    $post_content       = strlen($post_content) > 150 ? rtrim(substr($post_content, 0, 180)).'...': $post_content;
                    $post_image         = get_the_post_thumbnail_url($post_id, 'large');
                    $post_image_medium  = get_the_post_thumbnail_url($post_id, 'medium_large');
                    $youtube_video      = get_post_meta( get_the_ID(), '_post_youtube_video', 1 );

                        
                    $post_content   = strlen($post_content) > 100 ? rtrim(substr($post_content, 0, 100)).'...' : $post_content;

                    if($i>2):
                        
                ?>
                    <div class="col-md-3 list-news-page">

                        <?php if(!empty($youtube_video)): ?>
                            <div class="container-youtube-video">
                                <a href="<?php echo $youtube_video; ?>" class="fancy-group">
                                    <div class="thumb-img">
                                        <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $post_image ?>" data-src-small="<?php echo $post_image_medium ?>" alt="">
                                        <div class="icon-play" data-play="0" data-youtube-code="<?php echo $youtube_video_code; ?>"></div>
                                    </div>
                                </a>
                                <div class="video-background" data-play="0" data-youtube-code="<?php echo $youtube_video_code; ?>">
                                    <div class="video-foreground"></div>
                                </div>
                            </div>
                        <?php else: ?>
                            <a href="<?php echo $post_link; ?>"><img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $post_image ?>" data-src-small="<?php echo $post_image_medium ?>" alt=""></a>
                        <?php endif; ?>

                        <div class="container-desc">
                            <!--h5><?php echo $post_date; ?></h5-->
                            <a href="<?php echo $post_link; ?>"><h3><?php echo $post_title; ?></h3></a>
                            <p><?php echo $post_content; ?></p>
                            <!--h6>by <?php //echo ucwords(get_the_author()); ?></h6-->
                        </div>
                    </div>
                <?php
                    endif;
                $i++;
                endwhile;

                echo '<div class="paging paging-desktop">';
                    $big = 999999999;
                    echo paginate_links( array(
                        'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format'    => '?paged=%#%',
                        'current'   => max( 1, get_query_var('paged') ),
                        'total'     => $query->max_num_pages,
                        'prev_text' => 'PREV',
                        'next_text' => 'NEXT',
                        )
                    );

                echo '</div>';
                wp_reset_postdata();
                ?>
            </div> <!-- TUTUP DIV LIST BLUE -->
        </div>
    </section>

<?php endif; ?>

<?php get_footer(); ?>