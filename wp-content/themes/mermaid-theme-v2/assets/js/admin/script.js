jQuery(document).ready(function () {
    autocomplete_off_and_size_box();
    button_copy_link();
    add_schedule_data_row();
    form_schedule_data_table();
    delete_schedule_data();
    schedule_rate_validation();
    change_boat();
    submit_post();
    add_text_on_category_edit_page();
    init_checkout_form_func();
});

function init_checkout_form_func()
{
    if( jQuery('#post_type').val() == 'checkout' || jQuery('#post_type').val() == 'checkout-2' )
    {
        jQuery('.cmb2-id--checkout-boat .pw_select').on('change.select2', function(){
            init_filter_trip_code();
        });

        jQuery('.cmb2-id--checkout-trip-year .pw_select').on('change.select2', function(){
            init_filter_trip_code();
        });

        jQuery('.cmb2-id--checkout-trip-code .pw_select').on('change.select2', function(){
            var trip = jQuery(this).val();

            if( trip != '' )
            {
                var d = JSON.parse( window.atob( trip ) );

                if( typeof d.post_meta._schedule_data_iteneraries != 'undefined' )
                {
                    jQuery('.cmb2-id--checkout-iteneraries .pw_select').val( d.post_meta._schedule_data_iteneraries ).trigger('change.select2');
                }
                else
                {
                    jQuery('.cmb2-id--checkout-iteneraries .pw_select').val('').trigger('change.select2');
                }

                if( typeof d.post_meta._schedule_data_departure_date != 'undefined' )
                {
                    jQuery('.cmb2-id-departure-date [name=departure_date]').datepicker( 'setDate', new Date( d.post_meta._schedule_data_departure_date ) );
                }
                else
                {
                    jQuery('.cmb2-id-departure-date [name=departure_date]').datepicker( 'setDate', null );
                }

                if( typeof d.post_meta._schedule_data_arrival_date != 'undefined' )
                {
                    jQuery('.cmb2-id-arrival-date [name=arrival_date]').datepicker( 'setDate', new Date( d.post_meta._schedule_data_arrival_date ) );
                }
                else
                {
                    jQuery('.cmb2-id-arrival-date [name=arrival_date]').datepicker( 'setDate', null );
                }

                if( typeof d.post_meta._schedule_data_depart_point != 'undefined' )
                {
                    jQuery('.cmb2-id--checkout-departure-point .pw_select').val( d.post_meta._schedule_data_depart_point ).trigger('change.select2');
                }
                else
                {
                    jQuery('.cmb2-id--checkout-departure-point .pw_select').val('').trigger('change.select2');
                }

                if( typeof d.post_meta._schedule_data_arrival_point != 'undefined' )
                {
                    jQuery('.cmb2-id--checkout-arrival-point .pw_select').val( d.post_meta._schedule_data_arrival_point ).trigger('change.select2');
                }
                else
                {
                    jQuery('.cmb2-id--checkout-arrival-point .pw_select').val('').trigger('change.select2');
                }

                if( typeof d.post_meta._schedule_data_depart_time != 'undefined' )
                {
                    jQuery('.cmb2-id--checkout-departure-time .pw_select').val( d.post_meta._schedule_data_depart_time ).trigger('change.select2');
                }
                else
                {
                    jQuery('.cmb2-id--checkout-departure-time .pw_select').val('').trigger('change.select2');
                }

                if( typeof d.post_meta._schedule_data_arrival_time != 'undefined' )
                {
                    jQuery('.cmb2-id--checkout-arrival-time .pw_select').val( d.post_meta._schedule_data_arrival_time ).trigger('change.select2');
                }
                else
                {
                    jQuery('.cmb2-id--checkout-arrival-time .pw_select').val('').trigger('change.select2');
                }
            }
            else
            {
                jQuery('.cmb2-id--checkout-iteneraries .pw_select').val('').trigger('change.select2');
            }

            jQuery('[data-groupid="_checkout_cruise_list"] .cmb-repeatable-grouping').each(function(i, e){
                var idx = jQuery(e).attr('data-iterator');

                calculate_total_nett( idx );
            });
        });

        jQuery('.cmb2-id--checkout-booking-type .pw_select').on('change.select2', function(){
            init_conditional_field();

            jQuery('[data-groupid="_checkout_cruise_list"] .cmb-repeatable-grouping').each(function(i, e){
                var idx = jQuery(e).attr('data-iterator');
                
                calculate_total_nett( idx );
            });
        });

        jQuery('.cmb2-id--checkout-agent-name .pw_select').on('change.select2', function(){
            var agent = jQuery(this).val();

            if( agent != '' && agent != 0 )
            {
                var d = JSON.parse( window.atob( agent ) );

                if( typeof d.addr != 'undefined' )
                {
                    jQuery('[name=_checkout_agent_address]').val( d.addr );
                }
                else
                {
                    jQuery('[name=_checkout_agent_address]').val('');
                }
            }
            else
            {
                jQuery('[name=_checkout_agent_address]').val('');
            }
        });

        jQuery('[data-groupid="_checkout_cruise_list"] .cmb-repeatable-grouping').each(function(i, e){
            var idx = jQuery(e).attr('data-iterator');

            jQuery('#cmb-group-_checkout_cruise_list-' + idx + ' .pw_select').on('change.select2', function(){
                calculate_total_nett( idx );
            });

            jQuery('#_checkout_cruise_list_' + idx + '_no_guest').on('blur', function(){
                calculate_total_nett( idx );
            });

            jQuery('#_checkout_cruise_list_' + idx + '_gross').on('blur', function(){
                calculate_total_nett( idx );
            });

            jQuery('#_checkout_cruise_list_' + idx + '_repeat_guest_disc').on('blur', function(){
                calculate_total_nett( idx );
            });

            jQuery('#_checkout_cruise_list_' + idx + '_other_disc_value').on('blur', function(){
                calculate_total_nett( idx );
            });

            jQuery('#_checkout_cruise_list_' + idx + '_commission_value').on('blur', function(){
                calculate_total_nett( idx );
            });
        });

        jQuery('[data-groupid="_checkout_surcharge"] .cmb-repeatable-grouping').each(function(i, e){
            var idx = jQuery(e).attr('data-iterator');

            jQuery('#_checkout_surcharge_' + idx + '_units').on('blur', function(){
                calculate_total_surcharge( idx );
            });

            jQuery('#_checkout_surcharge_' + idx + '_nett_idr').on('blur', function(){
                calculate_nett_euro_2( idx );
            });

            jQuery('#_checkout_surcharge_' + idx + '_nett_euro').on('blur', function(){
                calculate_total_surcharge( idx );
            });
        });

        jQuery('[data-groupid="_checkout_travel_service"] .cmb-repeatable-grouping').each(function(i, e){
            var idx = jQuery(e).attr('data-iterator');

            jQuery('#_checkout_travel_service_' + idx + '_guests').on('blur', function(){
                calculate_total_service( idx );
            });

            jQuery('#_checkout_travel_service_' + idx + '_nett_idr').on('blur', function(){
                calculate_nett_euro( idx );
            });

            jQuery('#_checkout_travel_service_' + idx + '_nett_euro').on('blur', function(){
                calculate_total_service( idx );
            });
        });

        jQuery('[data-groupid="_checkout_payment_terms"] .cmb-repeatable-grouping').each(function(i, e){
            var idx = jQuery(e).attr('data-iterator');

            jQuery('#_checkout_payment_terms_' + idx + '_percentage').on('blur', function(){
                calculate_total_payment( idx );
            });

            jQuery('#_checkout_payment_terms_' + idx + '_status').on('change', function(){
                calculate_total_payment( idx );
            });
        });

        jQuery('#_checkout_surcharge_show_option').on('change', function(){
            calculate_all_total_payment();
        });

        jQuery('.cmb2-id--checkout-bank-options [type=radio]').on('change', function(){
            init_conditional_field();
        });

        init_conditional_field();
        init_group_field_action();
    }
}

function init_filter_trip_code()
{
    var year = jQuery('.cmb2-id--checkout-trip-year .pw_select').val();
    var boat = jQuery('.cmb2-id--checkout-boat .pw_select').val();

    jQuery('.cmb2-id--checkout-trip-code .pw_select option').each(function(){
        var trip = jQuery(this).val();

        if( trip != '' && trip != 0 )
        {
            var d = JSON.parse( window.atob( trip ) );

            if( typeof d.post_meta._schedule_rates_boat != 'undefined' && d.post_meta._schedule_rates_boat != boat && boat != '' )
            {
                jQuery(this).prop('selected', false);
                jQuery(this).prop('disabled', true);
            }
            else
            {
                if( typeof d.post_meta._schedule_rates_year != 'undefined' && d.post_meta._schedule_rates_year != year && year != '' )
                {
                    jQuery(this).prop('selected', false);
                    jQuery(this).prop('disabled', true);
                }
                else
                {
                    jQuery(this).prop('disabled', false);
                }
            }
        }
    });

    jQuery('.cmb2-id--checkout-trip-code .pw_select').trigger('change.select2');
}

function init_group_field_action()
{
    jQuery('#checkout_metaboxes_cruise_list, #checkout2_metaboxes_cruise_list').on('cmb2_add_row', function(){
        var idx = jQuery('.cmb-repeatable-grouping', this).last().attr('data-iterator');

        jQuery('#cmb-group-_checkout_cruise_list-' + idx + ' .pw_select').on('change.select2', function(){
            calculate_total_nett( idx );
        });

        jQuery('#_checkout_cruise_list_' + idx + '_no_guest').on('blur', function(){
            calculate_total_nett( idx );
        });

        jQuery('#_checkout_cruise_list_' + idx + '_gross').on('blur', function(){
            calculate_total_nett( idx );
        });

        jQuery('#_checkout_cruise_list_' + idx + '_repeat_guest_disc').on('blur', function(){
            calculate_total_nett( idx );
        });

        jQuery('#_checkout_cruise_list_' + idx + '_other_disc_value').on('blur', function(){
            calculate_total_nett( idx );
        });

        jQuery('#_checkout_cruise_list_' + idx + '_commission_value').on('blur', function(){
            calculate_total_nett( idx );
        });
    });

    jQuery('#checkout_metaboxes_travel_services, #checkout2_metaboxes_travel_services').on('cmb2_add_row', function(e){
        var idx = jQuery('.cmb-repeatable-grouping', this).last().attr('data-iterator');

        jQuery('#_checkout_travel_service_' + idx + '_guests').on('blur', function(){
            calculate_total_service( idx );
        });

        jQuery('#_checkout_travel_service_' + idx + '_nett_idr').on('blur', function(){
            calculate_nett_euro( idx );
        });

        jQuery('#_checkout_travel_service_' + idx + '_nett_euro').on('blur', function(){
            calculate_total_service( idx );
        });
    });

    jQuery('#checkout_metaboxes_payment_terms, #checkout2_metaboxes_payment_terms').on('cmb2_add_row', function(e){
        var idx = jQuery('.cmb-repeatable-grouping', this).last().attr('data-iterator');

        jQuery('#_checkout_payment_terms_' + idx + '_percentage').on('blur', function(){
            calculate_total_payment( idx );
        });

        jQuery('#_checkout_payment_terms_' + idx + '_status').on('change', function(){
            calculate_total_payment( idx );
        });
    });

    jQuery('#checkout_metaboxes_cruise_list, #checkout_metaboxes_travel_services, #checkout_metaboxes_payment_terms, #checkout2_metaboxes_cruise_list, #checkout2_metaboxes_travel_services, #checkout2_metaboxes_payment_terms').on( 'cmb2_remove_row', function(e){
        jQuery('[data-groupid="_checkout_travel_service"] .cmb-repeatable-grouping').each(function(i, e){
            var idx = jQuery(e).attr('data-iterator');

            calculate_total_service( idx );
        });

        jQuery('[data-groupid="_checkout_payment_terms"] .cmb-repeatable-grouping').each(function(i, e){
            var idx = jQuery(e).attr('data-iterator');
            
            calculate_total_payment( idx );
        });
    });
}

function init_conditional_field()
{
    var btype = jQuery('.cmb2-id--checkout-booking-type .pw_select').val();

    if( btype == '1' || btype == '3' )
    {
        jQuery('.cmb2-id--checkout-agent-name, .cmb2-id--checkout-agent-address').show();
        jQuery('.cmb2-id--checkout-country').hide();
    }
    else if( btype == '2' || btype == '4' )
    {
        jQuery('.cmb2-id--checkout-agent-name, .cmb2-id--checkout-agent-address').hide();
        jQuery('.cmb2-id--checkout-country').show();
    }
    else
    {
        jQuery('.cmb2-id--checkout-agent-name, .cmb2-id--checkout-agent-address, .cmb2-id--checkout-country').hide();
    }

    jQuery('#_checkout_cruise_list_repeat .cmb-repeatable-grouping').each( function(i, e){
        var idx = jQuery(this).attr('data-iterator');

        if( btype == '1' || btype == '2' )
        {
            jQuery('.cmb2-id--checkout-cruise-list-' + idx + '-other-disc-type, .cmb2-id--checkout-cruise-list-' + idx + '-other-disc-value').show();
        }
        else
        {
            jQuery('.cmb2-id--checkout-cruise-list-' + idx + '-other-disc-type, .cmb2-id--checkout-cruise-list-' + idx + '-other-disc-value').hide();
        }

        if( btype == '1' || btype == '3' )
        {
            jQuery('.cmb2-id--checkout-cruise-list-' + idx + '-commission-value').show();
        }
        else
        {
            jQuery('.cmb2-id--checkout-cruise-list-' + idx + '-commission-value').hide();
        }
    });

    var banks = jQuery('[name=_checkout_bank_options]:checked').val();

    if( banks == '0' )
    {
        jQuery('.cmb2-id--checkout-bank-transfer-detail').show();
        jQuery('.cmb2-id--checkout-bank-name, .cmb2-id--checkout-bank-address, .cmb2-id--checkout-receiver, .cmb2-id--checkout-account-number, .cmb2-id--checkout-swift-code').hide();
    }
    else
    {
        jQuery('.cmb2-id--checkout-bank-transfer-detail').hide();
        jQuery('.cmb2-id--checkout-bank-name, .cmb2-id--checkout-bank-address, .cmb2-id--checkout-receiver, .cmb2-id--checkout-account-number, .cmb2-id--checkout-swift-code').show();
    }
}

function get_total_cruise()
{
    var total = 0;

    jQuery('#_checkout_cruise_list_repeat .cmb-repeatable-grouping').each( function(i, e){
        var idx = jQuery(this).attr('data-iterator');
        var ttl = jQuery('#_checkout_cruise_list_' + idx + '_total_nett');

        total += parseFloat( ttl.val() ) || 0;
    });

    return total;
}

function get_total_service()
{
    var total = 0;

    jQuery('#_checkout_travel_service_repeat .cmb-repeatable-grouping').each( function(i, e){
        var idx = jQuery(this).attr('data-iterator');
        var ttl = jQuery('#_checkout_travel_service_' + idx + '_total_nett');

        total += parseFloat( ttl.val() ) || 0;
    });

    return total;
}

function get_total_surcharge()
{
    var total = 0;

    jQuery('#_checkout_surcharge_repeat .cmb-repeatable-grouping').each( function(i, e){
        var idx = jQuery(this).attr('data-iterator');
        var ttl = jQuery('#_checkout_surcharge_' + idx + '_total_nett');

        total += parseFloat( ttl.val() ) || 0;
    });

    return total;
}

function calculate_total_nett( idx )
{
    var tcode = jQuery('.cmb2-id--checkout-trip-code .pw_select').val();

    if( tcode != '' )
    {
        var disc1 = jQuery('#_checkout_cruise_list_' + idx + '_repeat_guest_disc');
        var disc2 = jQuery('#_checkout_cruise_list_' + idx + '_other_disc_value');
        var comms = jQuery('#_checkout_cruise_list_' + idx + '_commission_value');
        var tnett = jQuery('#_checkout_cruise_list_' + idx + '_total_nett');
        var cabin = jQuery('#_checkout_cruise_list_' + idx + '_cabin_type');
        var guest = jQuery('#_checkout_cruise_list_' + idx + '_no_guest');
        var gros  = jQuery('#_checkout_cruise_list_' + idx + '_gross');
        var nett  = jQuery('#_checkout_cruise_list_' + idx + '_nett');
        var btype = jQuery('#_checkout_booking_type');

        var mprice = 0;
        var sprice = 0;
        var dprice = 0;
        var lprice = 0;

        if( jQuery('.cmb2-id--checkout-trip-code .pw_select').val() != '' )
        {
            var d = JSON.parse( window.atob( tcode ) );

            if( typeof d.post_meta._schedule_data_price_master != 'undefined' )
            {
                var mprice = d.post_meta._schedule_data_price_master;
            }

            if( typeof d.post_meta._schedule_data_price_single != 'undefined' )
            {
                var sprice = d.post_meta._schedule_data_price_single;
            }

            if( typeof d.post_meta._schedule_data_price_deluxe != 'undefined' )
            {
                var dprice = d.post_meta._schedule_data_price_deluxe;
            }

            if( typeof d.post_meta._schedule_data_price_lower != 'undefined' )
            {
                var lprice = d.post_meta._schedule_data_price_lower;
            }
        }

        //-- Overide to 1 if guest number <= 0
        var guestnum = parseInt( guest.val() ) || 1;

        //-- Set Total, Subtotal, & Grandtotal
        var total      = 0;
        var subtotal   = 0;
        var grandtotal = 0;

        if( cabin.val() == 'Master' )
        {
            total = parseFloat( mprice ) || 0;
        }
        else if( cabin.val() == 'Single' )
        {
            total = parseFloat( sprice ) || 0;
        }
        else if( cabin.val() == 'Deluxe' || cabin.val() == 'Deluxe Twin' )
        {
            total = parseFloat( dprice ) || 0;
        }
        else if( cabin.val() == 'Lower Deck' || cabin.val() == 'Lower Deck Twin' )
        {
            total = parseFloat( lprice ) || 0;
        }

        if( total >= 0 )
        {
            //-- Set Discount
            var discone    = parseFloat( disc1.val() ) || 0;
            var disctwo    = parseFloat( disc2.val() ) || 0;
            var commission = parseFloat( comms.val() ) || 0;

            //-- Apply Discount 1
            subtotal = total - ( ( total * discone ) / 100 );

            if( btype.val() == 1 || btype.val() == 2 )
            {
                //-- Apply Discount 2
                subtotal = subtotal - ( ( subtotal * disctwo ) / 100 );
            }

            //-- Apply Commission
            if( btype.val() == 1 || btype.val() == 3 )
            {
                subtotal = subtotal - ( ( subtotal * commission ) / 100 );
            }

            grandtotal = subtotal * guestnum;
        }

        gros.val( total );
        nett.val( subtotal );
        guest.val( guestnum );
        tnett.val( grandtotal );
    }

    calculate_all_total_surcharge();
    calculate_all_total_service();
    calculate_all_total_payment();
    calculate_total_due_amount();
}

function calculate_nett_euro( idx )
{
    var sel = jQuery('#_checkout_travel_service_' + idx + '_nett_idr');
    var idr = parseInt( sel.val() ) || 0;

    jQuery.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajaxurl,
        data: {
            action: 'converter_curency',
            get_value: idr,
        }, 
        success: function( neur ){
             jQuery('#_checkout_travel_service_' + idx + '_nett_euro').val( neur ).trigger('blur');
        }
    });
}

function calculate_nett_euro_2( idx )
{
    var sel = jQuery('#_checkout_surcharge_' + idx + '_nett_idr');
    var idr = parseInt( sel.val() ) || 0;

    jQuery.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajaxurl,
        data: {
            action: 'converter_curency',
            get_value: idr,
        }, 
        success: function( neur ){
             jQuery('#_checkout_surcharge_' + idx + '_nett_euro').val( neur ).trigger('blur');
        }
    });
}

function calculate_total_service( idx )
{
    var tnett = jQuery('#_checkout_travel_service_' + idx + '_total_nett');
    var nett  = jQuery('#_checkout_travel_service_' + idx + '_nett_euro');
    var guest = jQuery('#_checkout_travel_service_' + idx + '_guests');

    //-- Overide to 1 if guest number <= 0
    var guestnum = parseInt( guest.val() ) || 1;

    //-- Set Subtotal & Grandtotal
    var subtotal   = nett.val();
    var grandtotal = parseFloat( subtotal * guestnum ).toFixed(2);

    nett.val( subtotal );
    guest.val( guestnum );
    tnett.val( grandtotal );

    calculate_all_total_payment();
    calculate_total_due_amount();
}

function calculate_total_surcharge( idx )
{
    var tnett = jQuery('#_checkout_surcharge_' + idx + '_total_nett');
    var nett  = jQuery('#_checkout_surcharge_' + idx + '_nett_euro');
    var units = jQuery('#_checkout_surcharge_' + idx + '_units');

    //-- Overide to 1 if units number <= 0
    var unitsnum = parseInt( units.val() ) || 1;

    //-- Set Subtotal & Grandtotal
    var subtotal   = nett.val();
    var grandtotal = parseFloat( subtotal * unitsnum ).toFixed(2);

    nett.val( subtotal );
    units.val( unitsnum );
    tnett.val( grandtotal );

    calculate_all_total_payment();
    calculate_total_due_amount();
}

function calculate_total_payment( idx )
{
    var tdepo = jQuery('#_checkout_payment_terms_' + idx + '_total_deposit');
    var tnett = jQuery('#_checkout_payment_terms_' + idx + '_total_nett');
    var depo  = jQuery('#_checkout_payment_terms_' + idx + '_percentage');
    var show  = jQuery('#_checkout_surcharge_show_option').val();

    //-- Get Total
    var total_scharge = parseFloat( get_total_surcharge() ) || 0;
    var total_service = parseFloat( get_total_service() ) || 0;
    var total_cruise  = parseFloat( get_total_cruise() ) || 0;
    var deposit       = parseFloat( depo.val() ) || 0;
    var total_deposit = Math.round( ( total_cruise * deposit ) / 100 ) || 0;

    if( show == 1 )
    {
        var due_amount = total_service + total_scharge;
    }
    else
    {
        var due_amount = total_service;
    }

    tdepo.val( total_deposit );
    tnett.val( total_deposit + due_amount );

    calculate_total_due_amount();
}

function calculate_all_total_service()
{
    jQuery('[data-groupid="_checkout_travel_service"] .cmb-repeatable-grouping').each(function(i, e){
        var idx = jQuery(e).attr('data-iterator');

        calculate_total_service( idx );
    });
}

function calculate_all_total_surcharge()
{
    jQuery('[data-groupid="_checkout_surcharge"] .cmb-repeatable-grouping').each(function(i, e){
        var idx = jQuery(e).attr('data-iterator');

        calculate_total_surcharge( idx );
    });
}

function calculate_all_total_payment()
{
    jQuery('[data-groupid="_checkout_payment_terms"] .cmb-repeatable-grouping').each(function(i, e){
        var idx = jQuery(e).attr('data-iterator');
        
        calculate_total_payment( idx );
    });
}

function calculate_total_due_amount()
{
    var total_scharge = parseFloat( get_total_surcharge() ) || 0;
    var total_service = parseFloat( get_total_service() ) || 0;
    var total_cruise  = parseFloat( get_total_cruise() ) || 0;
    var due_amount    = total_cruise + total_service;
    var waiting_depo  = 0;

    jQuery('[data-groupid="_checkout_payment_terms"] .cmb-repeatable-grouping').each(function(i, e){
        var idx  = jQuery(e).attr('data-iterator');
        var stat = jQuery('#_checkout_payment_terms_' + idx + '_status');
        var dep  = jQuery('#_checkout_payment_terms_' + idx + '_percentage');

        if( stat.val() == 'Awaiting Payment' )
        {
            dp = parseFloat( dep.val() ) || 0;

            waiting_depo += Math.round( ( due_amount * dp ) / 100 );
        }
    });

    // if( waiting_depo > 0 )
    // {
    //     jQuery('#_checkout_total_payment').val( waiting_depo );
    // }
    // else
    // {
    //     jQuery('#_checkout_total_payment').val( due_amount );
    // }
}

/*
| -------------------------------------------------------------------------------------
| Function untuk config auto complte off and add text size box
| -------------------------------------------------------------------------------------
*/
function autocomplete_off_and_size_box(){
    jQuery('#postimagediv .inside').append('<div class="recommended-size-box">Recommended image size: 1920px (width) x 960px (height)</div>');
    jQuery('#_checkout_reminder_date').attr('autocomplete','off');
    jQuery('#_checkout_expired_date').attr('autocomplete','off');
}


/*
| -------------------------------------------------------------------------------------
| Function untuk copy link payment
| -------------------------------------------------------------------------------------
*/
function button_copy_link(){
    jQuery('.copy-link').click(function(){
        var id = jQuery(this).data('id');
        var copyText = document.getElementById("link_"+id);
        copyText.select();
        document.execCommand("copy");
        var tooltip = document.getElementById("copy_link_"+id);
        tooltip.innerHTML = "Copied: " + copyText.value;
    });
}


/*
| -------------------------------------------------------------------------------------
| Function jika button on mouse out copy link
| -------------------------------------------------------------------------------------
*/
function outFunc(id){
    var tooltip = document.getElementById("copy_link_"+id);
    tooltip.innerHTML = "Copy to clipboard";
}


/*
| -------------------------------------------------------------------------------------
| Function untuk on change schedule form
| -------------------------------------------------------------------------------------
*/
function form_schedule_data_table() {
    jQuery('.field_schedule').on('blur', function () {
        jQuery(this).removeClass('error');
        const boat = jQuery('#_schedule_rates_boat').val();
        const name = jQuery(this).data("name");
        const field = jQuery(this).data("field");
        const table = jQuery(this).data("table");
        const val = jQuery(this).val();
        const key = jQuery(this).data("key");
        const action = "save-form-schedule";
        const action_type = jQuery(this).data("action-type");
        const datas = "boat=" + boat + "&action=" + action + "&val=" + val + "&key=" + key + "&name=" + name + "&field=" + field + "&key=" + key + "&table=" + table + "&action_type=" + action_type;

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajaxurl,
            data: datas,
            success: function (response) {

                // console.log(response);
                if (response.status == "success_update") {

                } else if (response.status == "success_insert") {
                    jQuery('.container-schedule-rate table tr.row_' + response.key_old + ' td input.field_schedule, .container-schedule-rate table tr.row_' + response.key_old + ' td select.field_schedule').attr("data-action-type", response.action_type);

                    jQuery('.container-schedule-rate table tr.row_' + response.key_old + ' td input.field_schedule, .container-schedule-rate table tr.row_' + response.key_old + ' td select.field_schedule').attr("data-key", response.key_new);

                    setInterval(function () {
                        jQuery('.container-schedule-rate table tr.row_' + response.key_old).addClass('row_' + response.key_new);
                        jQuery('.container-schedule-rate table tr.row_' + response.key_old).removeClass('row_' + response.key_old);
                    }, 300);

                } else {

                }

                schedule_rate_validation();
            }
        });
    });
}


/*
| -------------------------------------------------------------------------------------
| Function Add Schedule Data Row
| -------------------------------------------------------------------------------------
*/
function add_schedule_data_row() {
    jQuery('.button-add-row').click(function () {
        const action = "add-row-schedule";
        const post_id = jQuery('[name=post_ID]').val();
        const datas = "action=" + action + "&post_id=" + post_id;

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajaxurl,
            data: datas,
            success: function (response) {
                if (response.status == "success") {
                    // jQuery('.wrap-schedule-rate table-schedule tbody').fadeOut(300, function () {
                    jQuery('.wrap-schedule-rate .table-schedule tbody').append(response.html);
                    form_schedule_data_table();
                    delete_schedule_data();
                    schedule_rate_validation();
                    // });
                }
            }
        });
    });
}


/*
| -------------------------------------------------------------------------------------
| Function Delete Schedule Data Row
| -------------------------------------------------------------------------------------
*/
function delete_schedule_data() {
    jQuery('.delete_schedule_data').click(function () {
        const key = jQuery(this).data("key");
        const action = "delete-row-schedule";
        const datas = "action=" + action + "&key=" + key;

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {

                jQuery.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: ajaxurl,
                    data: datas,
                    success: function (response) {
                        if (response.status == "success") {
                            jQuery('.container-schedule-rate table tr.row_' + key).fadeOut(300, function () {
                                jQuery(this).remove();
                            });
                        }
                    }
                });
            }
        });
    });
}


/*
| -------------------------------------------------------------------------------------
| Function Add Itenerary
| -------------------------------------------------------------------------------------
*/
function itenerary_add() {
    jQuery('[name=save_itenerary]').click(function () {
        const boat = jQuery('#_schedule_rates_boat').val();
        const key = jQuery(this).data("key");
        const day = jQuery('.' + key + '-form').find('[name=_iteneraries_day]').val();
        const sub_day = jQuery('.' + key + '-form').find('[name=_iteneraries_sub_day]').val();
        const detail = jQuery('.' + key + '-form').find('[name=_iteneraries_detail]').val();
        const dives = jQuery('.' + key + '-form').find('[name=_iteneraries_dives]').val();
        const dest_id = jQuery('.' + key + '-form').find('[name=dest_id]').val();
        const post_id = jQuery('[name=post_ID]').val();
        const action = jQuery('.' + key + '-form').find('[name=action_itenerary]').val();
        const datas = "boat=" + boat + "&day=" + day + "&sub_day=" + sub_day + "&detail=" + detail + "&dives=" + dives + "&dest_id=" + dest_id + "&post_id=" + post_id + "&action=" + action;

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajaxurl,
            data: datas,
            success: function (response) {
                // console.log(response);
            }
        });

        return false;
    });
}


/*
| -------------------------------------------------------------------------------------
| Function Config Button Action Itenerary
| -------------------------------------------------------------------------------------
*/
function button_action_itenerary() {
    jQuery('.btn-edit-metabox').click(function () {
        const boat = jQuery(this).data("boat");
        const position_array = jQuery(this).data("position_array");
        // const post_id = jQuery(this).data("post_id");
        const day = jQuery(this).data("day");
        const sub_day = jQuery(this).data("sub_day");
        const detail = jQuery(this).data("detail");
        const dives = jQuery(this).data("dives");

        jQuery('.' + boat + '-form').find('[name=postition_array]').val(position_array);
        jQuery('.' + boat + '-form').find('[name=_iteneraries_day]').val(day);
        jQuery('.' + boat + '-form').find('[name=_iteneraries_sub_day]').val(sub_day);
        jQuery('.' + boat + '-form').find('[name=_iteneraries_detail]').val(detail);
        jQuery('.' + boat + '-form').find('[name=_iteneraries_dives]').val(dives);
        jQuery('.' + boat + '-form').find('[name=action_itenerary]').val("update-itenerary");

        jQuery('html, body').animate({
            scrollTop: jQuery('#wrap-' + boat + '-form').offset().top - 50,
        }, 1000);
        return false;
    });
}


/*
| -------------------------------------------------------------------------------------
| Function Config Schedule Rate Validation
| -------------------------------------------------------------------------------------
*/
function schedule_rate_validation(){
    var boat = jQuery('#_schedule_rates_boat').children("option:selected").val();
    // var action = "get-slug-boat";
    // const datas = "boat=" + boat + "&action=" + action;

    // var boat_slug = function() {
    //     var result = null;
    //     jQuery.ajax({
    //         async: "false",
    //         type: "POST",
    //         dataType: 'html',
    //         url: ajaxurl,
    //         data: datas,
    //         success: function (response) {
    //             // console.log(response);
    //             result = response.msg;
    //             // if(response.status=="success"){
    //             //     result = response.msg;
    //             // }else{
    //             //     result = '';
    //             // }
    //         }
    //     });
    //     return result;
    // }();

    jQuery('table.table-schedule tbody tr').each(function(){
        var key = jQuery(this).data("key");
        var e = 0;
        
        jQuery(this).find('.field_schedule').each(function(){
            var field = jQuery(this).data("field");
            
            if(boat == 213){
                if(field != "allotment_master" && field!= "price_master" && field != "allotment_single" && field != "price_single"){
                    var val = jQuery(this).val();
                    if (val == '') {
                        e++;
                    }
                }
            }else{
                var val = jQuery(this).val();
                if (val == '') {
                    e++;
                }
            }
        });
        
        jQuery(this).find('.icon-validation').hide();
        jQuery(this).find('.icon-validation').removeClass('complete');
        jQuery(this).find('.icon-validation').removeClass('not-complete');

        if(e == 0){
            jQuery('.icon-row-'+key).show();
            jQuery('.icon-row-'+key).addClass('complete');
            jQuery('.wrap-schedule-rate table tr.row_'+key).removeClass('error');
        }else{
            jQuery('.icon-row-'+key).show();
            jQuery('.icon-row-'+key).addClass('not-complete');
        }
    })
}


/*
| -------------------------------------------------------------------------------------
| Function Config Ketika Pilihan Boat Berubah
| -------------------------------------------------------------------------------------
*/
function change_boat(){
    jQuery('#_schedule_rates_boat').on('change', function(){
        schedule_rate_validation();
    });
}


/*
| -------------------------------------------------------------------------------------
| Function untuk config form submit post
| -------------------------------------------------------------------------------------
*/
function submit_post(){
    jQuery('.schedule_rates_body form[name=post]').submit(function(){
        jQuery('.wrap-schedule-rate tr').removeClass('error');
        jQuery('.field_schedule').removeClass('error');
        var boat = jQuery('#_schedule_rates_boat').children("option:selected").val();
        var length, count = 0;
        var all_key_validate = [];

        jQuery('table.table-schedule tbody tr').each(function(){
            var key = jQuery(this).data("key");
            var e = 0;

            length = jQuery(this).find('.field_schedule').length;
            
            jQuery(this).find('.field_schedule').each(function(){
                var field = jQuery(this).data("field");
                if(boat == 213){
                    if(field != "allotment_master" && field!= "price_master" && field != "allotment_single" && field != "price_single"){
                        var val = jQuery(this).val();
                        if (val == '') {
                            e++;
                        }
                    }
                }else{
                    var val = jQuery(this).val();
                    if (val == '') {
                        e++;
                    }
                }
            });

            if(boat == 213){
                length = 9;
                if(e != length && e != 0){
                    all_key_validate.push(key);
                    count++;
                }
            }else{
                if(e != length && e != 0){
                    all_key_validate.push(key);
                    count++;
                }
            }
        });

        if(count != 0){
            swal({
                title: "You have "+count+" row is not complete yet.",
                text: "Please complete the form until the validation success before you click update.",
                icon: "warning",
                dangerMode: true,
                showCloseButton: true,
                showCancelButton: false,
            }).then((ok) => {
                if(ok){
                    var first_key_validate = all_key_validate[0];
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.wrap-schedule-rate table tr.row_'+first_key_validate).offset().top,
                    }, 1000, function(){
                        for (i = 0; i < all_key_validate.length; i++) {
                            jQuery('.wrap-schedule-rate table tr.row_'+all_key_validate[i]).addClass('error');
                            
                            jQuery('.wrap-schedule-rate table tr.row_'+all_key_validate[i]).find('.field_schedule').each(function(){
                                var field = jQuery(this).data("field");
                                if(boat == 213){
                                    if(field != "allotment_master" && field!= "price_master" && field != "allotment_single" && field != "price_single"){
                                        var val = jQuery(this).val();
                                        if (val == '') {
                                            jQuery(this).addClass('error');
                                        }
                                    }
                                }else{
                                    var val = jQuery(this).val();
                                    if (val == '') {
                                        jQuery(this).addClass('error');
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }else{
            return true;
        }

        return false;
    });
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menambahkan text pada form edit di category
| -------------------------------------------------------------------------------------
*/
function add_text_on_category_edit_page(){
    if(jQuery('[name=tag-image]').length > 0){
        var parent = jQuery('[name=tag-image]').parent();
        parent.append('<p><i>Recommended image size: 1920px (width) x 960px (height)</i></p>')
    }
}