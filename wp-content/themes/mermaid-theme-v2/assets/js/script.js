/*! selectize.js - v0.12.6 | https://github.com/selectize/selectize.js | Apache License (v2) */
!function(a,b){"function"==typeof define&&define.amd?define("sifter",b):"object"==typeof exports?module.exports=b():a.Sifter=b()}(this,function(){var a=function(a,b){this.items=a,this.settings=b||{diacritics:!0}};a.prototype.tokenize=function(a){if(!(a=e(String(a||"").toLowerCase()))||!a.length)return[];var b,c,d,g,i=[],j=a.split(/ +/);for(b=0,c=j.length;b<c;b++){if(d=f(j[b]),this.settings.diacritics)for(g in h)h.hasOwnProperty(g)&&(d=d.replace(new RegExp(g,"g"),h[g]));i.push({string:j[b],regex:new RegExp(d,"i")})}return i},a.prototype.iterator=function(a,b){var c;c=g(a)?Array.prototype.forEach||function(a){for(var b=0,c=this.length;b<c;b++)a(this[b],b,this)}:function(a){for(var b in this)this.hasOwnProperty(b)&&a(this[b],b,this)},c.apply(a,[b])},a.prototype.getScoreFunction=function(a,b){var c,e,f,g,h;c=this,a=c.prepareSearch(a,b),f=a.tokens,e=a.options.fields,g=f.length,h=a.options.nesting;var i=function(a,b){var c,d;return a?(a=String(a||""),-1===(d=a.search(b.regex))?0:(c=b.string.length/a.length,0===d&&(c+=.5),c)):0},j=function(){var a=e.length;return a?1===a?function(a,b){return i(d(b,e[0],h),a)}:function(b,c){for(var f=0,g=0;f<a;f++)g+=i(d(c,e[f],h),b);return g/a}:function(){return 0}}();return g?1===g?function(a){return j(f[0],a)}:"and"===a.options.conjunction?function(a){for(var b,c=0,d=0;c<g;c++){if((b=j(f[c],a))<=0)return 0;d+=b}return d/g}:function(a){for(var b=0,c=0;b<g;b++)c+=j(f[b],a);return c/g}:function(){return 0}},a.prototype.getSortFunction=function(a,c){var e,f,g,h,i,j,k,l,m,n,o;if(g=this,a=g.prepareSearch(a,c),o=!a.query&&c.sort_empty||c.sort,m=function(a,b){return"$score"===a?b.score:d(g.items[b.id],a,c.nesting)},i=[],o)for(e=0,f=o.length;e<f;e++)(a.query||"$score"!==o[e].field)&&i.push(o[e]);if(a.query){for(n=!0,e=0,f=i.length;e<f;e++)if("$score"===i[e].field){n=!1;break}n&&i.unshift({field:"$score",direction:"desc"})}else for(e=0,f=i.length;e<f;e++)if("$score"===i[e].field){i.splice(e,1);break}for(l=[],e=0,f=i.length;e<f;e++)l.push("desc"===i[e].direction?-1:1);return j=i.length,j?1===j?(h=i[0].field,k=l[0],function(a,c){return k*b(m(h,a),m(h,c))}):function(a,c){var d,e,f;for(d=0;d<j;d++)if(f=i[d].field,e=l[d]*b(m(f,a),m(f,c)))return e;return 0}:null},a.prototype.prepareSearch=function(a,b){if("object"==typeof a)return a;b=c({},b);var d=b.fields,e=b.sort,f=b.sort_empty;return d&&!g(d)&&(b.fields=[d]),e&&!g(e)&&(b.sort=[e]),f&&!g(f)&&(b.sort_empty=[f]),{options:b,query:String(a||"").toLowerCase(),tokens:this.tokenize(a),total:0,items:[]}},a.prototype.search=function(a,b){var c,d,e,f,g=this;return d=this.prepareSearch(a,b),b=d.options,a=d.query,f=b.score||g.getScoreFunction(d),a.length?g.iterator(g.items,function(a,e){c=f(a),(!1===b.filter||c>0)&&d.items.push({score:c,id:e})}):g.iterator(g.items,function(a,b){d.items.push({score:1,id:b})}),e=g.getSortFunction(d,b),e&&d.items.sort(e),d.total=d.items.length,"number"==typeof b.limit&&(d.items=d.items.slice(0,b.limit)),d};var b=function(a,b){return"number"==typeof a&&"number"==typeof b?a>b?1:a<b?-1:0:(a=i(String(a||"")),b=i(String(b||"")),a>b?1:b>a?-1:0)},c=function(a,b){var c,d,e,f;for(c=1,d=arguments.length;c<d;c++)if(f=arguments[c])for(e in f)f.hasOwnProperty(e)&&(a[e]=f[e]);return a},d=function(a,b,c){if(a&&b){if(!c)return a[b];for(var d=b.split(".");d.length&&(a=a[d.shift()]););return a}},e=function(a){return(a+"").replace(/^\s+|\s+$|/g,"")},f=function(a){return(a+"").replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")},g=Array.isArray||"undefined"!=typeof $&&$.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)},h={a:"[aá¸€á¸Ä‚ÄƒÃ‚Ã¢ÇÇŽÈºâ±¥È¦È§áº áº¡Ã„Ã¤Ã€Ã ÃÃ¡Ä€ÄÃƒÃ£Ã…Ã¥Ä…Ä„ÃƒÄ…Ä„]",b:"[bâ¢Î²Î’Bà¸¿ðŒá›’]",c:"[cÄ†Ä‡ÄˆÄ‰ÄŒÄÄŠÄ‹CÌ„cÌ„Ã‡Ã§á¸ˆá¸‰È»È¼Æ‡ÆˆÉ•á´„ï¼£ï½ƒ]",d:"[dÄŽÄá¸Šá¸‹á¸á¸‘á¸Œá¸á¸’á¸“á¸Žá¸ÄÄ‘DÌ¦dÌ¦Æ‰É–ÆŠÉ—Æ‹ÆŒáµ­á¶á¶‘È¡á´…ï¼¤ï½„Ã°]",e:"[eÃ‰Ã©ÃˆÃ¨ÃŠÃªá¸˜á¸™ÄšÄ›Ä”Ä•áº¼áº½á¸šá¸›áººáº»Ä–Ä—Ã‹Ã«Ä’Ä“È¨È©Ä˜Ä™á¶’É†É‡È„È…áº¾áº¿á»€á»á»„á»…á»‚á»ƒá¸œá¸á¸–á¸—á¸”á¸•È†È‡áº¸áº¹á»†á»‡â±¸á´‡ï¼¥ï½…É˜ÇÆÆÎµ]",f:"[fÆ‘Æ’á¸žá¸Ÿ]",g:"[gÉ¢â‚²Ç¤Ç¥ÄœÄÄžÄŸÄ¢Ä£Æ“É Ä Ä¡]",h:"[hÄ¤Ä¥Ä¦Ä§á¸¨á¸©áº–áº–á¸¤á¸¥á¸¢á¸£É¦Ê°Ç¶Æ•]",i:"[iÃÃ­ÃŒÃ¬Ä¬Ä­ÃŽÃ®ÇÇÃÃ¯á¸®á¸¯Ä¨Ä©Ä®Ä¯ÄªÄ«á»ˆá»‰ÈˆÈ‰ÈŠÈ‹á»Šá»‹á¸¬á¸­Æ—É¨É¨Ì†áµ»á¶–Ä°iIÄ±Éªï¼©ï½‰]",j:"[jÈ·Ä´ÄµÉˆÉ‰ÊÉŸÊ²]",k:"[kÆ˜Æ™ê€êá¸°á¸±Ç¨Ç©á¸²á¸³á¸´á¸µÎºÏ°â‚­]",l:"[lÅÅ‚Ä½Ä¾Ä»Ä¼Ä¹Äºá¸¶á¸·á¸¸á¸¹á¸¼á¸½á¸ºá¸»Ä¿Å€È½Æšâ± â±¡â±¢É«É¬á¶…É­È´ÊŸï¼¬ï½Œ]",n:"[nÅƒÅ„Ç¸Ç¹Å‡ÅˆÃ‘Ã±á¹„á¹…Å…Å†á¹†á¹‡á¹Šá¹‹á¹ˆá¹‰NÌˆnÌˆÆÉ²È Æžáµ°á¶‡É³ÈµÉ´ï¼®ï½ŽÅŠÅ‹]",o:"[oÃ˜Ã¸Ã–Ã¶Ã“Ã³Ã’Ã²Ã”Ã´Ç‘Ç’ÅÅ‘ÅŽÅÈ®È¯á»Œá»ÆŸÉµÆ Æ¡á»Žá»ÅŒÅÃ•ÃµÇªÇ«ÈŒÈÕ•Ö…]",p:"[pá¹”á¹•á¹–á¹—â±£áµ½Æ¤Æ¥áµ±]",q:"[qê–ê—Ê ÉŠÉ‹ê˜ê™qÌƒ]",r:"[rÅ”Å•ÉŒÉÅ˜Å™Å–Å—á¹˜á¹™ÈÈ‘È’È“á¹šá¹›â±¤É½]",s:"[sÅšÅ›á¹ á¹¡á¹¢á¹£êž¨êž©ÅœÅÅ Å¡ÅžÅŸÈ˜È™SÌˆsÌˆ]",t:"[tÅ¤Å¥á¹ªá¹«Å¢Å£á¹¬á¹­Æ®ÊˆÈšÈ›á¹°á¹±á¹®á¹¯Æ¬Æ­]",u:"[uÅ¬Å­É„Ê‰á»¤á»¥ÃœÃ¼ÃšÃºÃ™Ã¹Ã›Ã»Ç“Ç”Å°Å±Å¬Å­Æ¯Æ°á»¦á»§ÅªÅ«Å¨Å©Å²Å³È”È•âˆª]",v:"[vá¹¼á¹½á¹¾á¹¿Æ²Ê‹êžêŸâ±±Ê‹]",w:"[wáº‚áºƒáº€áºÅ´Åµáº„áº…áº†áº‡áºˆáº‰]",x:"[xáºŒáºáºŠáº‹Ï‡]",y:"[yÃÃ½á»²á»³Å¶Å·Å¸Ã¿á»¸á»¹áºŽáºá»´á»µÉŽÉÆ³Æ´]",z:"[zÅ¹Åºáºáº‘Å½Å¾Å»Å¼áº’áº“áº”áº•ÆµÆ¶]"},i=function(){var a,b,c,d,e="",f={};for(c in h)if(h.hasOwnProperty(c))for(d=h[c].substring(2,h[c].length-1),e+=d,a=0,b=d.length;a<b;a++)f[d.charAt(a)]=c;var g=new RegExp("["+e+"]","g");return function(a){return a.replace(g,function(a){return f[a]}).toLowerCase()}}();return a}),function(a,b){"function"==typeof define&&define.amd?define("microplugin",b):"object"==typeof exports?module.exports=b():a.MicroPlugin=b()}(this,function(){var a={};a.mixin=function(a){a.plugins={},a.prototype.initializePlugins=function(a){var c,d,e,f=this,g=[];if(f.plugins={names:[],settings:{},requested:{},loaded:{}},b.isArray(a))for(c=0,d=a.length;c<d;c++)"string"==typeof a[c]?g.push(a[c]):(f.plugins.settings[a[c].name]=a[c].options,g.push(a[c].name));else if(a)for(e in a)a.hasOwnProperty(e)&&(f.plugins.settings[e]=a[e],g.push(e));for(;g.length;)f.require(g.shift())},a.prototype.loadPlugin=function(b){var c=this,d=c.plugins,e=a.plugins[b];if(!a.plugins.hasOwnProperty(b))throw new Error('Unable to find "'+b+'" plugin');d.requested[b]=!0,d.loaded[b]=e.fn.apply(c,[c.plugins.settings[b]||{}]),d.names.push(b)},a.prototype.require=function(a){var b=this,c=b.plugins;if(!b.plugins.loaded.hasOwnProperty(a)){if(c.requested[a])throw new Error('Plugin has circular dependency ("'+a+'")');b.loadPlugin(a)}return c.loaded[a]},a.define=function(b,c){a.plugins[b]={name:b,fn:c}}};var b={isArray:Array.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)}};return a}),function(a,b){"function"==typeof define&&define.amd?define("selectize",["jquery","sifter","microplugin"],b):"object"==typeof exports?module.exports=b(require("jquery"),require("sifter"),require("microplugin")):a.Selectize=b(a.jQuery,a.Sifter,a.MicroPlugin)}(this,function(a,b,c){"use strict";var d=function(a,b){if("string"!=typeof b||b.length){var c="string"==typeof b?new RegExp(b,"i"):b,d=function(a){var b=0;if(3===a.nodeType){var e=a.data.search(c);if(e>=0&&a.data.length>0){var f=a.data.match(c),g=document.createElement("span");g.className="highlight";var h=a.splitText(e),i=(h.splitText(f[0].length),h.cloneNode(!0));g.appendChild(i),h.parentNode.replaceChild(g,h),b=1}}else if(1===a.nodeType&&a.childNodes&&!/(script|style)/i.test(a.tagName)&&("highlight"!==a.className||"SPAN"!==a.tagName))for(var j=0;j<a.childNodes.length;++j)j+=d(a.childNodes[j]);return b};return a.each(function(){d(this)})}};a.fn.removeHighlight=function(){return this.find("span.highlight").each(function(){this.parentNode.firstChild.nodeName;var a=this.parentNode;a.replaceChild(this.firstChild,this),a.normalize()}).end()};var e=function(){};e.prototype={on:function(a,b){this._events=this._events||{},this._events[a]=this._events[a]||[],this._events[a].push(b)},off:function(a,b){var c=arguments.length;return 0===c?delete this._events:1===c?delete this._events[a]:(this._events=this._events||{},void(a in this._events!=!1&&this._events[a].splice(this._events[a].indexOf(b),1)))},trigger:function(a){if(this._events=this._events||{},a in this._events!=!1)for(var b=0;b<this._events[a].length;b++)this._events[a][b].apply(this,Array.prototype.slice.call(arguments,1))}},e.mixin=function(a){for(var b=["on","off","trigger"],c=0;c<b.length;c++)a.prototype[b[c]]=e.prototype[b[c]]};var f=/Mac/.test(navigator.userAgent),g=f?91:17,h=f?18:17,i=!/android/i.test(window.navigator.userAgent)&&!!document.createElement("input").validity,j=function(a){return void 0!==a},k=function(a){return void 0===a||null===a?null:"boolean"==typeof a?a?"1":"0":a+""},l=function(a){return(a+"").replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g,"&quot;")},m={};m.before=function(a,b,c){var d=a[b];a[b]=function(){return c.apply(a,arguments),d.apply(a,arguments)}},m.after=function(a,b,c){var d=a[b];a[b]=function(){var b=d.apply(a,arguments);return c.apply(a,arguments),b}};var n=function(a){var b=!1;return function(){b||(b=!0,a.apply(this,arguments))}},o=function(a,b){var c;return function(){var d=this,e=arguments;window.clearTimeout(c),c=window.setTimeout(function(){a.apply(d,e)},b)}},p=function(a,b,c){var d,e=a.trigger,f={};a.trigger=function(){var c=arguments[0];if(-1===b.indexOf(c))return e.apply(a,arguments);f[c]=arguments},c.apply(a,[]),a.trigger=e;for(d in f)f.hasOwnProperty(d)&&e.apply(a,f[d])},q=function(a,b,c,d){a.on(b,c,function(b){for(var c=b.target;c&&c.parentNode!==a[0];)c=c.parentNode;return b.currentTarget=c,d.apply(this,[b])})},r=function(a){var b={};if("selectionStart"in a)b.start=a.selectionStart,b.length=a.selectionEnd-b.start;else if(document.selection){a.focus();var c=document.selection.createRange(),d=document.selection.createRange().text.length;c.moveStart("character",-a.value.length),b.start=c.text.length-d,b.length=d}return b},s=function(a,b,c){var d,e,f={};if(c)for(d=0,e=c.length;d<e;d++)f[c[d]]=a.css(c[d]);else f=a.css();b.css(f)},t=function(b,c){return b?(w.$testInput||(w.$testInput=a("<span />").css({position:"absolute",top:-99999,left:-99999,width:"auto",padding:0,whiteSpace:"pre"}).appendTo("body")),w.$testInput.text(b),s(c,w.$testInput,["letterSpacing","fontSize","fontFamily","fontWeight","textTransform"]),w.$testInput.width()):0},u=function(a){var b=null,c=function(c,d){var e,f,g,h,i,j,k,l;c=c||window.event||{},d=d||{},c.metaKey||c.altKey||(d.force||!1!==a.data("grow"))&&(e=a.val(),c.type&&"keydown"===c.type.toLowerCase()&&(f=c.keyCode,g=f>=48&&f<=57||f>=65&&f<=90||f>=96&&f<=111||f>=186&&f<=222||32===f,46===f||8===f?(l=r(a[0]),l.length?e=e.substring(0,l.start)+e.substring(l.start+l.length):8===f&&l.start?e=e.substring(0,l.start-1)+e.substring(l.start+1):46===f&&void 0!==l.start&&(e=e.substring(0,l.start)+e.substring(l.start+1))):g&&(j=c.shiftKey,k=String.fromCharCode(c.keyCode),k=j?k.toUpperCase():k.toLowerCase(),e+=k)),h=a.attr("placeholder"),!e&&h&&(e=h),(i=t(e,a)+4)!==b&&(b=i,a.width(i),a.triggerHandler("resize")))};a.on("keydown keyup update blur",c),c()},v=function(a){var b=document.createElement("div");return b.appendChild(a.cloneNode(!0)),b.innerHTML},w=function(c,d){var e,f,g,h,i=this;h=c[0],h.selectize=i;var j=window.getComputedStyle&&window.getComputedStyle(h,null);if(g=j?j.getPropertyValue("direction"):h.currentStyle&&h.currentStyle.direction,g=g||c.parents("[dir]:first").attr("dir")||"",a.extend(i,{order:0,settings:d,$input:c,tabIndex:c.attr("tabindex")||"",tagType:"select"===h.tagName.toLowerCase()?1:2,rtl:/rtl/i.test(g),eventNS:".selectize"+ ++w.count,highlightedValue:null,isBlurring:!1,isOpen:!1,isDisabled:!1,isRequired:c.is("[required]"),isInvalid:!1,isLocked:!1,isFocused:!1,isInputHidden:!1,isSetup:!1,isShiftDown:!1,isCmdDown:!1,isCtrlDown:!1,ignoreFocus:!1,ignoreBlur:!1,ignoreHover:!1,hasOptions:!1,currentResults:null,lastValue:"",caretPos:0,loading:0,loadedSearches:{},$activeOption:null,$activeItems:[],optgroups:{},options:{},userOptions:{},items:[],renderCache:{},onSearchChange:null===d.loadThrottle?i.onSearchChange:o(i.onSearchChange,d.loadThrottle)}),i.sifter=new b(this.options,{diacritics:d.diacritics}),i.settings.options){for(e=0,f=i.settings.options.length;e<f;e++)i.registerOption(i.settings.options[e]);delete i.settings.options}if(i.settings.optgroups){for(e=0,f=i.settings.optgroups.length;e<f;e++)i.registerOptionGroup(i.settings.optgroups[e]);delete i.settings.optgroups}i.settings.mode=i.settings.mode||(1===i.settings.maxItems?"single":"multi"),"boolean"!=typeof i.settings.hideSelected&&(i.settings.hideSelected="multi"===i.settings.mode),i.initializePlugins(i.settings.plugins),i.setupCallbacks(),i.setupTemplates(),i.setup()};return e.mixin(w),void 0!==c?c.mixin(w):function(a,b){b||(b={});console.error("Selectize: "+a),b.explanation&&(console.group&&console.group(),console.error(b.explanation),console.group&&console.groupEnd())}("Dependency MicroPlugin is missing",{explanation:'Make sure you either: (1) are using the "standalone" version of Selectize, or (2) require MicroPlugin before you load Selectize.'}),a.extend(w.prototype,{setup:function(){var b,c,d,e,j,k,l,m,n,o,p=this,r=p.settings,s=p.eventNS,t=a(window),v=a(document),w=p.$input;if(l=p.settings.mode,m=w.attr("class")||"",b=a("<div>").addClass(r.wrapperClass).addClass(m).addClass(l),c=a("<div>").addClass(r.inputClass).addClass("items").appendTo(b),d=a('<input type="text" autocomplete="off" />').appendTo(c).attr("tabindex",w.is(":disabled")?"-1":p.tabIndex),k=a(r.dropdownParent||b),e=a("<div>").addClass(r.dropdownClass).addClass(l).hide().appendTo(k),j=a("<div>").addClass(r.dropdownContentClass).appendTo(e),(o=w.attr("id"))&&(d.attr("id",o+"-selectized"),a("label[for='"+o+"']").attr("for",o+"-selectized")),p.settings.copyClassesToDropdown&&e.addClass(m),b.css({width:w[0].style.width}),p.plugins.names.length&&(n="plugin-"+p.plugins.names.join(" plugin-"),b.addClass(n),e.addClass(n)),(null===r.maxItems||r.maxItems>1)&&1===p.tagType&&w.attr("multiple","multiple"),p.settings.placeholder&&d.attr("placeholder",r.placeholder),!p.settings.splitOn&&p.settings.delimiter){var x=p.settings.delimiter.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&");p.settings.splitOn=new RegExp("\\s*"+x+"+\\s*")}w.attr("autocorrect")&&d.attr("autocorrect",w.attr("autocorrect")),w.attr("autocapitalize")&&d.attr("autocapitalize",w.attr("autocapitalize")),d[0].type=w[0].type,p.$wrapper=b,p.$control=c,p.$control_input=d,p.$dropdown=e,p.$dropdown_content=j,e.on("mouseenter mousedown click","[data-disabled]>[data-selectable]",function(a){a.stopImmediatePropagation()}),e.on("mouseenter","[data-selectable]",function(){return p.onOptionHover.apply(p,arguments)}),e.on("mousedown click","[data-selectable]",function(){return p.onOptionSelect.apply(p,arguments)}),q(c,"mousedown","*:not(input)",function(){return p.onItemSelect.apply(p,arguments)}),u(d),c.on({mousedown:function(){return p.onMouseDown.apply(p,arguments)},click:function(){return p.onClick.apply(p,arguments)}}),d.on({mousedown:function(a){a.stopPropagation()},keydown:function(){return p.onKeyDown.apply(p,arguments)},keyup:function(){return p.onKeyUp.apply(p,arguments)},keypress:function(){return p.onKeyPress.apply(p,arguments)},resize:function(){p.positionDropdown.apply(p,[])},blur:function(){return p.onBlur.apply(p,arguments)},focus:function(){return p.ignoreBlur=!1,p.onFocus.apply(p,arguments)},paste:function(){return p.onPaste.apply(p,arguments)}}),v.on("keydown"+s,function(a){p.isCmdDown=a[f?"metaKey":"ctrlKey"],p.isCtrlDown=a[f?"altKey":"ctrlKey"],p.isShiftDown=a.shiftKey}),v.on("keyup"+s,function(a){a.keyCode===h&&(p.isCtrlDown=!1),16===a.keyCode&&(p.isShiftDown=!1),a.keyCode===g&&(p.isCmdDown=!1)}),v.on("mousedown"+s,function(a){if(p.isFocused){if(a.target===p.$dropdown[0]||a.target.parentNode===p.$dropdown[0])return!1;p.$control.has(a.target).length||a.target===p.$control[0]||p.blur(a.target)}}),t.on(["scroll"+s,"resize"+s].join(" "),function(){p.isOpen&&p.positionDropdown.apply(p,arguments)}),t.on("mousemove"+s,function(){p.ignoreHover=!1}),this.revertSettings={$children:w.children().detach(),tabindex:w.attr("tabindex")},w.attr("tabindex",-1).hide().after(p.$wrapper),a.isArray(r.items)&&(p.setValue(r.items),delete r.items),i&&w.on("invalid"+s,function(a){a.preventDefault(),p.isInvalid=!0,p.refreshState()}),p.updateOriginalInput(),p.refreshItems(),p.refreshState(),p.updatePlaceholder(),p.isSetup=!0,w.is(":disabled")&&p.disable(),p.on("change",this.onChange),w.data("selectize",p),w.addClass("selectized"),p.trigger("initialize"),!0===r.preload&&p.onSearchChange("")},setupTemplates:function(){var b=this,c=b.settings.labelField,d=b.settings.optgroupLabelField,e={optgroup:function(a){return'<div class="optgroup">'+a.html+"</div>"},optgroup_header:function(a,b){return'<div class="optgroup-header">'+b(a[d])+"</div>"},option:function(a,b){return'<div class="option">'+b(a[c])+"</div>"},item:function(a,b){return'<div class="item">'+b(a[c])+"</div>"},option_create:function(a,b){return'<div class="create">Add <strong>'+b(a.input)+"</strong>&hellip;</div>"}};b.settings.render=a.extend({},e,b.settings.render)},setupCallbacks:function(){var a,b,c={initialize:"onInitialize",change:"onChange",item_add:"onItemAdd",item_remove:"onItemRemove",clear:"onClear",option_add:"onOptionAdd",option_remove:"onOptionRemove",option_clear:"onOptionClear",optgroup_add:"onOptionGroupAdd",optgroup_remove:"onOptionGroupRemove",optgroup_clear:"onOptionGroupClear",dropdown_open:"onDropdownOpen",dropdown_close:"onDropdownClose",type:"onType",load:"onLoad",focus:"onFocus",blur:"onBlur"};for(a in c)c.hasOwnProperty(a)&&(b=this.settings[c[a]])&&this.on(a,b)},onClick:function(a){var b=this;b.isFocused&&b.isOpen||(b.focus(),a.preventDefault())},onMouseDown:function(b){var c=this,d=b.isDefaultPrevented();a(b.target);if(c.isFocused){if(b.target!==c.$control_input[0])return"single"===c.settings.mode?c.isOpen?c.close():c.open():d||c.setActiveItem(null),!1}else d||window.setTimeout(function(){c.focus()},0)},onChange:function(){this.$input.trigger("change")},onPaste:function(b){var c=this;if(c.isFull()||c.isInputHidden||c.isLocked)return void b.preventDefault();c.settings.splitOn&&setTimeout(function(){var b=c.$control_input.val();if(b.match(c.settings.splitOn))for(var d=a.trim(b).split(c.settings.splitOn),e=0,f=d.length;e<f;e++)c.createItem(d[e])},0)},onKeyPress:function(a){if(this.isLocked)return a&&a.preventDefault();var b=String.fromCharCode(a.keyCode||a.which);return this.settings.create&&"multi"===this.settings.mode&&b===this.settings.delimiter?(this.createItem(),a.preventDefault(),!1):void 0},onKeyDown:function(a){var b=(a.target,this.$control_input[0],this);if(b.isLocked)return void(9!==a.keyCode&&a.preventDefault());switch(a.keyCode){case 65:if(b.isCmdDown)return void b.selectAll();break;case 27:return void(b.isOpen&&(a.preventDefault(),a.stopPropagation(),b.close()));case 78:if(!a.ctrlKey||a.altKey)break;case 40:if(!b.isOpen&&b.hasOptions)b.open();else if(b.$activeOption){b.ignoreHover=!0;var c=b.getAdjacentOption(b.$activeOption,1);c.length&&b.setActiveOption(c,!0,!0)}return void a.preventDefault();case 80:if(!a.ctrlKey||a.altKey)break;case 38:if(b.$activeOption){b.ignoreHover=!0;var d=b.getAdjacentOption(b.$activeOption,-1);d.length&&b.setActiveOption(d,!0,!0)}return void a.preventDefault();case 13:return void(b.isOpen&&b.$activeOption&&(b.onOptionSelect({currentTarget:b.$activeOption}),a.preventDefault()));case 37:return void b.advanceSelection(-1,a);case 39:return void b.advanceSelection(1,a);case 9:return b.settings.selectOnTab&&b.isOpen&&b.$activeOption&&(b.onOptionSelect({currentTarget:b.$activeOption}),b.isFull()||a.preventDefault()),void(b.settings.create&&b.createItem()&&a.preventDefault());case 8:case 46:return void b.deleteSelection(a)}return!b.isFull()&&!b.isInputHidden||(f?a.metaKey:a.ctrlKey)?void 0:void a.preventDefault()},onKeyUp:function(a){var b=this;if(b.isLocked)return a&&a.preventDefault();var c=b.$control_input.val()||"";b.lastValue!==c&&(b.lastValue=c,b.onSearchChange(c),b.refreshOptions(),b.trigger("type",c))},onSearchChange:function(a){var b=this,c=b.settings.load;c&&(b.loadedSearches.hasOwnProperty(a)||(b.loadedSearches[a]=!0,b.load(function(d){c.apply(b,[a,d])})))},onFocus:function(a){var b=this,c=b.isFocused;if(b.isDisabled)return b.blur(),a&&a.preventDefault(),!1;b.ignoreFocus||(b.isFocused=!0,"focus"===b.settings.preload&&b.onSearchChange(""),c||b.trigger("focus"),b.$activeItems.length||(b.showInput(),b.setActiveItem(null),b.refreshOptions(!!b.settings.openOnFocus)),b.refreshState())},onBlur:function(a,b){var c=this;if(c.isFocused&&(c.isFocused=!1,!c.ignoreFocus)){if(!c.ignoreBlur&&document.activeElement===c.$dropdown_content[0])return c.ignoreBlur=!0,void c.onFocus(a);var d=function(){c.close(),c.setTextboxValue(""),c.setActiveItem(null),c.setActiveOption(null),c.setCaret(c.items.length),c.refreshState(),b&&b.focus&&b.focus(),c.isBlurring=!1,c.ignoreFocus=!1,c.trigger("blur")};c.isBlurring=!0,c.ignoreFocus=!0,c.settings.create&&c.settings.createOnBlur?c.createItem(null,!1,d):d()}},onOptionHover:function(a){this.ignoreHover||this.setActiveOption(a.currentTarget,!1)},onOptionSelect:function(b){var c,d,e=this;b.preventDefault&&(b.preventDefault(),b.stopPropagation()),d=a(b.currentTarget),d.hasClass("create")?e.createItem(null,function(){e.settings.closeAfterSelect&&e.close()}):void 0!==(c=d.attr("data-value"))&&(e.lastQuery=null,e.setTextboxValue(""),e.addItem(c),e.settings.closeAfterSelect?e.close():!e.settings.hideSelected&&b.type&&/mouse/.test(b.type)&&e.setActiveOption(e.getOption(c)))},onItemSelect:function(a){var b=this;b.isLocked||"multi"===b.settings.mode&&(a.preventDefault(),b.setActiveItem(a.currentTarget,a))},load:function(a){var b=this,c=b.$wrapper.addClass(b.settings.loadingClass);b.loading++,a.apply(b,[function(a){b.loading=Math.max(b.loading-1,0),a&&a.length&&(b.addOption(a),b.refreshOptions(b.isFocused&&!b.isInputHidden)),b.loading||c.removeClass(b.settings.loadingClass),b.trigger("load",a)}])},setTextboxValue:function(a){var b=this.$control_input;b.val()!==a&&(b.val(a).triggerHandler("update"),this.lastValue=a)},getValue:function(){return 1===this.tagType&&this.$input.attr("multiple")?this.items:this.items.join(this.settings.delimiter)},setValue:function(a,b){p(this,b?[]:["change"],function(){this.clear(b),this.addItems(a,b)})},setActiveItem:function(b,c){var d,e,f,g,h,i,j,k,l=this;if("single"!==l.settings.mode){if(b=a(b),!b.length)return a(l.$activeItems).removeClass("active"),l.$activeItems=[],void(l.isFocused&&l.showInput());if("mousedown"===(d=c&&c.type.toLowerCase())&&l.isShiftDown&&l.$activeItems.length){for(k=l.$control.children(".active:last"),g=Array.prototype.indexOf.apply(l.$control[0].childNodes,[k[0]]),h=Array.prototype.indexOf.apply(l.$control[0].childNodes,[b[0]]),g>h&&(j=g,g=h,h=j),e=g;e<=h;e++)i=l.$control[0].childNodes[e],-1===l.$activeItems.indexOf(i)&&(a(i).addClass("active"),l.$activeItems.push(i));c.preventDefault()}else"mousedown"===d&&l.isCtrlDown||"keydown"===d&&this.isShiftDown?b.hasClass("active")?(f=l.$activeItems.indexOf(b[0]),l.$activeItems.splice(f,1),b.removeClass("active")):l.$activeItems.push(b.addClass("active")[0]):(a(l.$activeItems).removeClass("active"),l.$activeItems=[b.addClass("active")[0]]);l.hideInput(),this.isFocused||l.focus()}},setActiveOption:function(b,c,d){var e,f,g,h,i,k=this;k.$activeOption&&k.$activeOption.removeClass("active"),k.$activeOption=null,b=a(b),b.length&&(k.$activeOption=b.addClass("active"),!c&&j(c)||(e=k.$dropdown_content.height(),f=k.$activeOption.outerHeight(!0),c=k.$dropdown_content.scrollTop()||0,g=k.$activeOption.offset().top-k.$dropdown_content.offset().top+c,h=g,i=g-e+f,g+f>e+c?k.$dropdown_content.stop().animate({scrollTop:i},d?k.settings.scrollDuration:0):g<c&&k.$dropdown_content.stop().animate({scrollTop:h},d?k.settings.scrollDuration:0)))},selectAll:function(){var a=this;"single"!==a.settings.mode&&(a.$activeItems=Array.prototype.slice.apply(a.$control.children(":not(input)").addClass("active")),a.$activeItems.length&&(a.hideInput(),a.close()),a.focus())},hideInput:function(){var a=this;a.setTextboxValue(""),a.$control_input.css({opacity:0,position:"absolute",left:a.rtl?1e4:-1e4}),a.isInputHidden=!0},showInput:function(){this.$control_input.css({opacity:1,position:"relative",left:0}),this.isInputHidden=!1},focus:function(){var a=this;a.isDisabled||(a.ignoreFocus=!0,a.$control_input[0].focus(),window.setTimeout(function(){a.ignoreFocus=!1,a.onFocus()},0))},blur:function(a){this.$control_input[0].blur(),this.onBlur(null,a)},getScoreFunction:function(a){return this.sifter.getScoreFunction(a,this.getSearchOptions())},getSearchOptions:function(){var a=this.settings,b=a.sortField;return"string"==typeof b&&(b=[{field:b}]),{fields:a.searchField,conjunction:a.searchConjunction,sort:b,nesting:a.nesting}},search:function(b){var c,d,e,f=this,g=f.settings,h=this.getSearchOptions();if(g.score&&"function"!=typeof(e=f.settings.score.apply(this,[b])))throw new Error('Selectize "score" setting must be a function that returns a function');if(b!==f.lastQuery?(f.lastQuery=b,d=f.sifter.search(b,a.extend(h,{score:e})),f.currentResults=d):d=a.extend(!0,{},f.currentResults),g.hideSelected)for(c=d.items.length-1;c>=0;c--)-1!==f.items.indexOf(k(d.items[c].id))&&d.items.splice(c,1);return d},refreshOptions:function(b){var c,e,f,g,h,i,j,l,m,n,o,p,q,r,s,t;void 0===b&&(b=!0);var u=this,w=a.trim(u.$control_input.val()),x=u.search(w),y=u.$dropdown_content,z=u.$activeOption&&k(u.$activeOption.attr("data-value"));for(g=x.items.length,"number"==typeof u.settings.maxOptions&&(g=Math.min(g,u.settings.maxOptions)),h={},i=[],c=0;c<g;c++)for(j=u.options[x.items[c].id],l=u.render("option",j),m=j[u.settings.optgroupField]||"",n=a.isArray(m)?m:[m],e=0,f=n&&n.length;e<f;e++)m=n[e],u.optgroups.hasOwnProperty(m)||(m=""),h.hasOwnProperty(m)||(h[m]=document.createDocumentFragment(),i.push(m)),h[m].appendChild(l);for(this.settings.lockOptgroupOrder&&i.sort(function(a,b){return(u.optgroups[a].$order||0)-(u.optgroups[b].$order||0)}),o=document.createDocumentFragment(),c=0,g=i.length;c<g;c++)m=i[c],u.optgroups.hasOwnProperty(m)&&h[m].childNodes.length?(p=document.createDocumentFragment(),p.appendChild(u.render("optgroup_header",u.optgroups[m])),p.appendChild(h[m]),o.appendChild(u.render("optgroup",a.extend({},u.optgroups[m],{html:v(p),dom:p})))):o.appendChild(h[m]);if(y.html(o),u.settings.highlight&&(y.removeHighlight(),x.query.length&&x.tokens.length))for(c=0,g=x.tokens.length;c<g;c++)d(y,x.tokens[c].regex);if(!u.settings.hideSelected)for(c=0,g=u.items.length;c<g;c++)u.getOption(u.items[c]).addClass("selected");q=u.canCreate(w),q&&(y.prepend(u.render("option_create",{input:w})),t=a(y[0].childNodes[0])),u.hasOptions=x.items.length>0||q,u.hasOptions?(x.items.length>0?(s=z&&u.getOption(z),s&&s.length?r=s:"single"===u.settings.mode&&u.items.length&&(r=u.getOption(u.items[0])),r&&r.length||(r=t&&!u.settings.addPrecedence?u.getAdjacentOption(t,1):y.find("[data-selectable]:first"))):r=t,u.setActiveOption(r),b&&!u.isOpen&&u.open()):(u.setActiveOption(null),b&&u.isOpen&&u.close())},addOption:function(b){var c,d,e,f=this;if(a.isArray(b))for(c=0,d=b.length;c<d;c++)f.addOption(b[c]);else(e=f.registerOption(b))&&(f.userOptions[e]=!0,f.lastQuery=null,f.trigger("option_add",e,b))},registerOption:function(a){var b=k(a[this.settings.valueField]);return void 0!==b&&null!==b&&!this.options.hasOwnProperty(b)&&(a.$order=a.$order||++this.order,this.options[b]=a,b)},registerOptionGroup:function(a){var b=k(a[this.settings.optgroupValueField]);return!!b&&(a.$order=a.$order||++this.order,this.optgroups[b]=a,b)},addOptionGroup:function(a,b){b[this.settings.optgroupValueField]=a,(a=this.registerOptionGroup(b))&&this.trigger("optgroup_add",a,b)},removeOptionGroup:function(a){this.optgroups.hasOwnProperty(a)&&(delete this.optgroups[a],this.renderCache={},this.trigger("optgroup_remove",a))},clearOptionGroups:function(){this.optgroups={},this.renderCache={},this.trigger("optgroup_clear")},updateOption:function(b,c){var d,e,f,g,h,i,j,l=this;if(b=k(b),f=k(c[l.settings.valueField]),null!==b&&l.options.hasOwnProperty(b)){if("string"!=typeof f)throw new Error("Value must be set in option data");j=l.options[b].$order,f!==b&&(delete l.options[b],-1!==(g=l.items.indexOf(b))&&l.items.splice(g,1,f)),c.$order=c.$order||j,l.options[f]=c,h=l.renderCache.item,i=l.renderCache.option,h&&(delete h[b],delete h[f]),i&&(delete i[b],delete i[f]),-1!==l.items.indexOf(f)&&(d=l.getItem(b),e=a(l.render("item",c)),d.hasClass("active")&&e.addClass("active"),d.replaceWith(e)),l.lastQuery=null,l.isOpen&&l.refreshOptions(!1)}},removeOption:function(a,b){var c=this;a=k(a);var d=c.renderCache.item,e=c.renderCache.option;d&&delete d[a],e&&delete e[a],delete c.userOptions[a],delete c.options[a],c.lastQuery=null,c.trigger("option_remove",a),c.removeItem(a,b)},clearOptions:function(){var b=this;b.loadedSearches={},b.userOptions={},b.renderCache={};var c=b.options;a.each(b.options,function(a,d){-1==b.items.indexOf(a)&&delete c[a]}),b.options=b.sifter.items=c,b.lastQuery=null,b.trigger("option_clear")},getOption:function(a){return this.getElementWithValue(a,this.$dropdown_content.find("[data-selectable]"))},getAdjacentOption:function(b,c){var d=this.$dropdown.find("[data-selectable]"),e=d.index(b)+c;return e>=0&&e<d.length?d.eq(e):a()},getElementWithValue:function(b,c){if(void 0!==(b=k(b))&&null!==b)for(var d=0,e=c.length;d<e;d++)if(c[d].getAttribute("data-value")===b)return a(c[d]);return a()},getItem:function(a){return this.getElementWithValue(a,this.$control.children())},addItems:function(b,c){this.buffer=document.createDocumentFragment();for(var d=this.$control[0].childNodes,e=0;e<d.length;e++)this.buffer.appendChild(d[e]);for(var f=a.isArray(b)?b:[b],e=0,g=f.length;e<g;e++)this.isPending=e<g-1,this.addItem(f[e],c);var h=this.$control[0];h.insertBefore(this.buffer,h.firstChild),this.buffer=null},addItem:function(b,c){p(this,c?[]:["change"],function(){var d,e,f,g,h,i=this,j=i.settings.mode;if(b=k(b),-1!==i.items.indexOf(b))return void("single"===j&&i.close());i.options.hasOwnProperty(b)&&("single"===j&&i.clear(c),"multi"===j&&i.isFull()||(d=a(i.render("item",i.options[b])),h=i.isFull(),i.items.splice(i.caretPos,0,b),i.insertAtCaret(d),(!i.isPending||!h&&i.isFull())&&i.refreshState(),i.isSetup&&(f=i.$dropdown_content.find("[data-selectable]"),i.isPending||(e=i.getOption(b),g=i.getAdjacentOption(e,1).attr("data-value"),i.refreshOptions(i.isFocused&&"single"!==j),g&&i.setActiveOption(i.getOption(g))),!f.length||i.isFull()?i.close():i.isPending||i.positionDropdown(),i.updatePlaceholder(),i.trigger("item_add",b,d),i.isPending||i.updateOriginalInput({silent:c}))))})},removeItem:function(b,c){var d,e,f,g=this;d=b instanceof a?b:g.getItem(b),b=k(d.attr("data-value")),-1!==(e=g.items.indexOf(b))&&(d.remove(),d.hasClass("active")&&(f=g.$activeItems.indexOf(d[0]),g.$activeItems.splice(f,1)),g.items.splice(e,1),g.lastQuery=null,!g.settings.persist&&g.userOptions.hasOwnProperty(b)&&g.removeOption(b,c),e<g.caretPos&&g.setCaret(g.caretPos-1),g.refreshState(),g.updatePlaceholder(),g.updateOriginalInput({silent:c}),g.positionDropdown(),g.trigger("item_remove",b,d))},createItem:function(b,c){var d=this,e=d.caretPos;b=b||a.trim(d.$control_input.val()||"");var f=arguments[arguments.length-1];if("function"!=typeof f&&(f=function(){}),"boolean"!=typeof c&&(c=!0),!d.canCreate(b))return f(),!1;d.lock();var g="function"==typeof d.settings.create?this.settings.create:function(a){var b={};return b[d.settings.labelField]=a,b[d.settings.valueField]=a,b},h=n(function(a){if(d.unlock(),!a||"object"!=typeof a)return f();var b=k(a[d.settings.valueField]);if("string"!=typeof b)return f();d.setTextboxValue(""),d.addOption(a),d.setCaret(e),d.addItem(b),d.refreshOptions(c&&"single"!==d.settings.mode),f(a)}),i=g.apply(this,[b,h]);return void 0!==i&&h(i),!0},refreshItems:function(){this.lastQuery=null,this.isSetup&&this.addItem(this.items),this.refreshState(),this.updateOriginalInput()},refreshState:function(){this.refreshValidityState(),this.refreshClasses()},refreshValidityState:function(){if(!this.isRequired)return!1;var a=!this.items.length;this.isInvalid=a,this.$control_input.prop("required",a),this.$input.prop("required",!a)},refreshClasses:function(){var b=this,c=b.isFull(),d=b.isLocked;b.$wrapper.toggleClass("rtl",b.rtl),b.$control.toggleClass("focus",b.isFocused).toggleClass("disabled",b.isDisabled).toggleClass("required",b.isRequired).toggleClass("invalid",b.isInvalid).toggleClass("locked",d).toggleClass("full",c).toggleClass("not-full",!c).toggleClass("input-active",b.isFocused&&!b.isInputHidden).toggleClass("dropdown-active",b.isOpen).toggleClass("has-options",!a.isEmptyObject(b.options)).toggleClass("has-items",b.items.length>0),b.$control_input.data("grow",!c&&!d)},isFull:function(){
return null!==this.settings.maxItems&&this.items.length>=this.settings.maxItems},updateOriginalInput:function(a){var b,c,d,e,f=this;if(a=a||{},1===f.tagType){for(d=[],b=0,c=f.items.length;b<c;b++)e=f.options[f.items[b]][f.settings.labelField]||"",d.push('<option value="'+l(f.items[b])+'" selected="selected">'+l(e)+"</option>");d.length||this.$input.attr("multiple")||d.push('<option value="" selected="selected"></option>'),f.$input.html(d.join(""))}else f.$input.val(f.getValue()),f.$input.attr("value",f.$input.val());f.isSetup&&(a.silent||f.trigger("change",f.$input.val()))},updatePlaceholder:function(){if(this.settings.placeholder){var a=this.$control_input;this.items.length?a.removeAttr("placeholder"):a.attr("placeholder",this.settings.placeholder),a.triggerHandler("update",{force:!0})}},open:function(){var a=this;a.isLocked||a.isOpen||"multi"===a.settings.mode&&a.isFull()||(a.focus(),a.isOpen=!0,a.refreshState(),a.$dropdown.css({visibility:"hidden",display:"block"}),a.positionDropdown(),a.$dropdown.css({visibility:"visible"}),a.trigger("dropdown_open",a.$dropdown))},close:function(){var a=this,b=a.isOpen;"single"===a.settings.mode&&a.items.length&&(a.hideInput(),a.isBlurring||a.$control_input.blur()),a.isOpen=!1,a.$dropdown.hide(),a.setActiveOption(null),a.refreshState(),b&&a.trigger("dropdown_close",a.$dropdown)},positionDropdown:function(){var a=this.$control,b="body"===this.settings.dropdownParent?a.offset():a.position();b.top+=a.outerHeight(!0),this.$dropdown.css({width:a[0].getBoundingClientRect().width,top:b.top,left:b.left})},clear:function(a){var b=this;b.items.length&&(b.$control.children(":not(input)").remove(),b.items=[],b.lastQuery=null,b.setCaret(0),b.setActiveItem(null),b.updatePlaceholder(),b.updateOriginalInput({silent:a}),b.refreshState(),b.showInput(),b.trigger("clear"))},insertAtCaret:function(a){var b=Math.min(this.caretPos,this.items.length),c=a[0],d=this.buffer||this.$control[0];0===b?d.insertBefore(c,d.firstChild):d.insertBefore(c,d.childNodes[b]),this.setCaret(b+1)},deleteSelection:function(b){var c,d,e,f,g,h,i,j,k,l=this;if(e=b&&8===b.keyCode?-1:1,f=r(l.$control_input[0]),l.$activeOption&&!l.settings.hideSelected&&(i=l.getAdjacentOption(l.$activeOption,-1).attr("data-value")),g=[],l.$activeItems.length){for(k=l.$control.children(".active:"+(e>0?"last":"first")),h=l.$control.children(":not(input)").index(k),e>0&&h++,c=0,d=l.$activeItems.length;c<d;c++)g.push(a(l.$activeItems[c]).attr("data-value"));b&&(b.preventDefault(),b.stopPropagation())}else(l.isFocused||"single"===l.settings.mode)&&l.items.length&&(e<0&&0===f.start&&0===f.length?g.push(l.items[l.caretPos-1]):e>0&&f.start===l.$control_input.val().length&&g.push(l.items[l.caretPos]));if(!g.length||"function"==typeof l.settings.onDelete&&!1===l.settings.onDelete.apply(l,[g]))return!1;for(void 0!==h&&l.setCaret(h);g.length;)l.removeItem(g.pop());return l.showInput(),l.positionDropdown(),l.refreshOptions(!0),i&&(j=l.getOption(i),j.length&&l.setActiveOption(j)),!0},advanceSelection:function(a,b){var c,d,e,f,g,h=this;0!==a&&(h.rtl&&(a*=-1),c=a>0?"last":"first",d=r(h.$control_input[0]),h.isFocused&&!h.isInputHidden?(f=h.$control_input.val().length,(a<0?0===d.start&&0===d.length:d.start===f)&&!f&&h.advanceCaret(a,b)):(g=h.$control.children(".active:"+c),g.length&&(e=h.$control.children(":not(input)").index(g),h.setActiveItem(null),h.setCaret(a>0?e+1:e))))},advanceCaret:function(a,b){var c,d,e=this;0!==a&&(c=a>0?"next":"prev",e.isShiftDown?(d=e.$control_input[c](),d.length&&(e.hideInput(),e.setActiveItem(d),b&&b.preventDefault())):e.setCaret(e.caretPos+a))},setCaret:function(b){var c=this;if(b="single"===c.settings.mode?c.items.length:Math.max(0,Math.min(c.items.length,b)),!c.isPending){var d,e,f,g;for(f=c.$control.children(":not(input)"),d=0,e=f.length;d<e;d++)g=a(f[d]).detach(),d<b?c.$control_input.before(g):c.$control.append(g)}c.caretPos=b},lock:function(){this.close(),this.isLocked=!0,this.refreshState()},unlock:function(){this.isLocked=!1,this.refreshState()},disable:function(){var a=this;a.$input.prop("disabled",!0),a.$control_input.prop("disabled",!0).prop("tabindex",-1),a.isDisabled=!0,a.lock()},enable:function(){var a=this;a.$input.prop("disabled",!1),a.$control_input.prop("disabled",!1).prop("tabindex",a.tabIndex),a.isDisabled=!1,a.unlock()},destroy:function(){var b=this,c=b.eventNS,d=b.revertSettings;b.trigger("destroy"),b.off(),b.$wrapper.remove(),b.$dropdown.remove(),b.$input.html("").append(d.$children).removeAttr("tabindex").removeClass("selectized").attr({tabindex:d.tabindex}).show(),b.$control_input.removeData("grow"),b.$input.removeData("selectize"),0==--w.count&&w.$testInput&&(w.$testInput.remove(),w.$testInput=void 0),a(window).off(c),a(document).off(c),a(document.body).off(c),delete b.$input[0].selectize},render:function(b,c){var d,e,f="",g=!1,h=this;return"option"!==b&&"item"!==b||(d=k(c[h.settings.valueField]),g=!!d),g&&(j(h.renderCache[b])||(h.renderCache[b]={}),h.renderCache[b].hasOwnProperty(d))?h.renderCache[b][d]:(f=a(h.settings.render[b].apply(this,[c,l])),"option"===b||"option_create"===b?c[h.settings.disabledField]||f.attr("data-selectable",""):"optgroup"===b&&(e=c[h.settings.optgroupValueField]||"",f.attr("data-group",e),c[h.settings.disabledField]&&f.attr("data-disabled","")),"option"!==b&&"item"!==b||f.attr("data-value",d||""),g&&(h.renderCache[b][d]=f[0]),f[0])},clearCache:function(a){var b=this;void 0===a?b.renderCache={}:delete b.renderCache[a]},canCreate:function(a){var b=this;if(!b.settings.create)return!1;var c=b.settings.createFilter;return a.length&&("function"!=typeof c||c.apply(b,[a]))&&("string"!=typeof c||new RegExp(c).test(a))&&(!(c instanceof RegExp)||c.test(a))}}),w.count=0,w.defaults={options:[],optgroups:[],plugins:[],delimiter:",",splitOn:null,persist:!0,diacritics:!0,create:!1,createOnBlur:!1,createFilter:null,highlight:!0,openOnFocus:!0,maxOptions:1e3,maxItems:null,hideSelected:null,addPrecedence:!1,selectOnTab:!1,preload:!1,allowEmptyOption:!1,closeAfterSelect:!1,scrollDuration:60,loadThrottle:300,loadingClass:"loading",dataAttr:"data-data",optgroupField:"optgroup",valueField:"value",labelField:"text",disabledField:"disabled",optgroupLabelField:"label",optgroupValueField:"value",lockOptgroupOrder:!1,sortField:"$order",searchField:["text"],searchConjunction:"and",mode:null,wrapperClass:"selectize-control",inputClass:"selectize-input",dropdownClass:"selectize-dropdown",dropdownContentClass:"selectize-dropdown-content",dropdownParent:null,copyClassesToDropdown:!0,render:{}},a.fn.selectize=function(b){var c=a.fn.selectize.defaults,d=a.extend({},c,b),e=d.dataAttr,f=d.labelField,g=d.valueField,h=d.disabledField,i=d.optgroupField,j=d.optgroupLabelField,l=d.optgroupValueField,m=function(b,c){var h,i,j,k,l=b.attr(e);if(l)for(c.options=JSON.parse(l),h=0,i=c.options.length;h<i;h++)c.items.push(c.options[h][g]);else{var m=a.trim(b.val()||"");if(!d.allowEmptyOption&&!m.length)return;for(j=m.split(d.delimiter),h=0,i=j.length;h<i;h++)k={},k[f]=j[h],k[g]=j[h],c.options.push(k);c.items=j}},n=function(b,c){var m,n,o,p,q=c.options,r={},s=function(a){var b=e&&a.attr(e);return"string"==typeof b&&b.length?JSON.parse(b):null},t=function(b,e){b=a(b);var j=k(b.val());if(j||d.allowEmptyOption)if(r.hasOwnProperty(j)){if(e){var l=r[j][i];l?a.isArray(l)?l.push(e):r[j][i]=[l,e]:r[j][i]=e}}else{var m=s(b)||{};m[f]=m[f]||b.text(),m[g]=m[g]||j,m[h]=m[h]||b.prop("disabled"),m[i]=m[i]||e,r[j]=m,q.push(m),b.is(":selected")&&c.items.push(j)}};for(c.maxItems=b.attr("multiple")?null:1,p=b.children(),m=0,n=p.length;m<n;m++)o=p[m].tagName.toLowerCase(),"optgroup"===o?function(b){var d,e,f,g,i;for(b=a(b),f=b.attr("label"),f&&(g=s(b)||{},g[j]=f,g[l]=f,g[h]=b.prop("disabled"),c.optgroups.push(g)),i=a("option",b),d=0,e=i.length;d<e;d++)t(i[d],f)}(p[m]):"option"===o&&t(p[m])};return this.each(function(){if(!this.selectize){var e=a(this),f=this.tagName.toLowerCase(),g=e.attr("placeholder")||e.attr("data-placeholder");g||d.allowEmptyOption||(g=e.children('option[value=""]').text());var h={placeholder:g,options:[],optgroups:[],items:[]};"select"===f?n(e,h):m(e,h),new w(e,a.extend(!0,{},c,h,b))}})},a.fn.selectize.defaults=w.defaults,a.fn.selectize.support={validity:i},w.define("drag_drop",function(b){if(!a.fn.sortable)throw new Error('The "drag_drop" plugin requires jQuery UI "sortable".');if("multi"===this.settings.mode){var c=this;c.lock=function(){var a=c.lock;return function(){var b=c.$control.data("sortable");return b&&b.disable(),a.apply(c,arguments)}}(),c.unlock=function(){var a=c.unlock;return function(){var b=c.$control.data("sortable");return b&&b.enable(),a.apply(c,arguments)}}(),c.setup=function(){var b=c.setup;return function(){b.apply(this,arguments);var d=c.$control.sortable({items:"[data-value]",forcePlaceholderSize:!0,disabled:c.isLocked,start:function(a,b){b.placeholder.css("width",b.helper.css("width")),d.css({overflow:"visible"})},stop:function(){d.css({overflow:"hidden"});var b=c.$activeItems?c.$activeItems.slice():null,e=[];d.children("[data-value]").each(function(){e.push(a(this).attr("data-value"))}),c.setValue(e),c.setActiveItem(b)}})}}()}}),w.define("dropdown_header",function(b){var c=this;b=a.extend({title:"Untitled",headerClass:"selectize-dropdown-header",titleRowClass:"selectize-dropdown-header-title",labelClass:"selectize-dropdown-header-label",closeClass:"selectize-dropdown-header-close",html:function(a){return'<div class="'+a.headerClass+'"><div class="'+a.titleRowClass+'"><span class="'+a.labelClass+'">'+a.title+'</span><a href="javascript:void(0)" class="'+a.closeClass+'">&times;</a></div></div>'}},b),c.setup=function(){var d=c.setup;return function(){d.apply(c,arguments),c.$dropdown_header=a(b.html(b)),c.$dropdown.prepend(c.$dropdown_header)}}()}),w.define("optgroup_columns",function(b){var c=this;b=a.extend({equalizeWidth:!0,equalizeHeight:!0},b),this.getAdjacentOption=function(b,c){var d=b.closest("[data-group]").find("[data-selectable]"),e=d.index(b)+c;return e>=0&&e<d.length?d.eq(e):a()},this.onKeyDown=function(){var a=c.onKeyDown;return function(b){var d,e,f,g;return!this.isOpen||37!==b.keyCode&&39!==b.keyCode?a.apply(this,arguments):(c.ignoreHover=!0,g=this.$activeOption.closest("[data-group]"),d=g.find("[data-selectable]").index(this.$activeOption),g=37===b.keyCode?g.prev("[data-group]"):g.next("[data-group]"),f=g.find("[data-selectable]"),e=f.eq(Math.min(f.length-1,d)),void(e.length&&this.setActiveOption(e)))}}();var d=function(){var a,b=d.width,c=document;return void 0===b&&(a=c.createElement("div"),a.innerHTML='<div style="width:50px;height:50px;position:absolute;left:-50px;top:-50px;overflow:auto;"><div style="width:1px;height:100px;"></div></div>',a=a.firstChild,c.body.appendChild(a),b=d.width=a.offsetWidth-a.clientWidth,c.body.removeChild(a)),b},e=function(){var e,f,g,h,i,j,k;if(k=a("[data-group]",c.$dropdown_content),(f=k.length)&&c.$dropdown_content.width()){if(b.equalizeHeight){for(g=0,e=0;e<f;e++)g=Math.max(g,k.eq(e).height());k.css({height:g})}b.equalizeWidth&&(j=c.$dropdown_content.innerWidth()-d(),h=Math.round(j/f),k.css({width:h}),f>1&&(i=j-h*(f-1),k.eq(f-1).css({width:i})))}};(b.equalizeHeight||b.equalizeWidth)&&(m.after(this,"positionDropdown",e),m.after(this,"refreshOptions",e))}),w.define("remove_button",function(b){b=a.extend({label:"&times;",title:"Remove",className:"remove",append:!0},b);if("single"===this.settings.mode)return void function(b,c){c.className="remove-single";var d=b,e='<a href="javascript:void(0)" class="'+c.className+'" tabindex="-1" title="'+l(c.title)+'">'+c.label+"</a>",f=function(b,c){return a("<span>").append(b).append(c)};b.setup=function(){var g=d.setup;return function(){if(c.append){var h=a(d.$input.context).attr("id"),i=(a("#"+h),d.settings.render.item);d.settings.render.item=function(a){return f(i.apply(b,arguments),e)}}g.apply(b,arguments),b.$control.on("click","."+c.className,function(a){a.preventDefault(),d.isLocked||d.clear()})}}()}(this,b);!function(b,c){var d=b,e='<a href="javascript:void(0)" class="'+c.className+'" tabindex="-1" title="'+l(c.title)+'">'+c.label+"</a>",f=function(a,b){var c=a.search(/(<\/[^>]+>\s*)$/);return a.substring(0,c)+b+a.substring(c)};b.setup=function(){var g=d.setup;return function(){if(c.append){var h=d.settings.render.item;d.settings.render.item=function(a){return f(h.apply(b,arguments),e)}}g.apply(b,arguments),b.$control.on("click","."+c.className,function(b){if(b.preventDefault(),!d.isLocked){var c=a(b.currentTarget).parent();d.setActiveItem(c),d.deleteSelection()&&d.setCaret(d.items.length)}})}}()}(this,b)}),w.define("restore_on_backspace",function(a){var b=this;a.text=a.text||function(a){return a[this.settings.labelField]},this.onKeyDown=function(){var c=b.onKeyDown;return function(b){var d,e;return 8===b.keyCode&&""===this.$control_input.val()&&!this.$activeItems.length&&(d=this.caretPos-1)>=0&&d<this.items.length?(e=this.options[this.items[d]],this.deleteSelection(b)&&(this.setTextboxValue(a.text.apply(this,[e])),this.refreshOptions(!0)),void b.preventDefault()):c.apply(this,arguments)}}()}),w});

// Fancybox
!function(t,e,n,o){"use strict";function s(t){var e=t.currentTarget,o=t.data?t.data.options:{},s=t.data?t.data.items:[],i="",a=0;t.preventDefault(),t.stopPropagation(),n(e).attr("data-fancybox")&&(i=n(e).data("fancybox")),i?(s=s.length?s.filter('[data-fancybox="'+i+'"]'):n("[data-fancybox="+i+"]"),a=s.index(e)):s=[e],n.fancybox.open(s,o,a)}if(!n)return o;var i={speed:330,loop:!0,opacity:"auto",margin:[44,0],gutter:30,infobar:!0,buttons:!0,slideShow:!0,fullScreen:!0,thumbs:!0,closeBtn:!0,smallBtn:"auto",image:{preload:"auto",protect:!1},ajax:{settings:{data:{fancybox:!0}}},iframe:{tpl:'<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',preload:!0,scrolling:"no",css:{}},baseClass:"",slideClass:"",baseTpl:'<div class="fancybox-container" role="dialog" tabindex="-1"><div class="fancybox-bg"></div><div class="fancybox-controls"><div class="fancybox-infobar"><button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button><div class="fancybox-infobar__body"><span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span></div><button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button></div><div class="fancybox-buttons"><button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button></div></div><div class="fancybox-slider-wrap"><div class="fancybox-slider"></div></div><div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div></div>',spinnerTpl:'<div class="fancybox-loading"></div>',errorTpl:'<div class="fancybox-error"><p>The requested content cannot be loaded. <br /> Please try again later.<p></div>',closeTpl:'<button data-fancybox-close class="fancybox-close-small">×</button>',parentEl:"body",touch:!0,keyboard:!0,focus:!0,closeClickOutside:!0,beforeLoad:n.noop,afterLoad:n.noop,beforeMove:n.noop,afterMove:n.noop,onComplete:n.noop,onInit:n.noop,beforeClose:n.noop,afterClose:n.noop,onActivate:n.noop,onDeactivate:n.noop},a=n(t),r=n(e),c=0,l=function(t){return t&&t.hasOwnProperty&&t instanceof n},u=function(){return t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||function(e){t.setTimeout(e,1e3/60)}}(),d=function(o){var s;return"function"==typeof n&&o instanceof n&&(o=o[0]),s=o.getBoundingClientRect(),s.bottom>0&&s.right>0&&s.left<(t.innerWidth||e.documentElement.clientWidth)&&s.top<(t.innerHeight||e.documentElement.clientHeight)},h=function(t,o,s){var a=this;a.opts=n.extend(!0,{index:s},i,o||{}),a.id=a.opts.id||++c,a.group=[],a.currIndex=parseInt(a.opts.index,10)||0,a.prevIndex=null,a.createGroup(t),a.group.length&&(a.$lastFocus=n(e.activeElement),a.elems={},a.slides={},a.init(t))};n.extend(h.prototype,{init:function(){var t,e,o=this;o.scrollTop=a.scrollTop(),o.scrollLeft=a.scrollLeft(),n.fancybox.isTouch||n("body").hasClass("fancybox-enabled")||(t=n("body").width(),t=n("body").addClass("fancybox-enabled").width()-t,t>1&&n('<style id="fancybox-noscroll" type="text/css">').html(".compensate-for-scrollbar, .fancybox-enabled { margin-right: "+t+"px; }").appendTo("head")),e=n(o.opts.baseTpl).attr("id","fancybox-container-"+o.id).data("FancyBox",o).addClass(o.opts.baseClass).hide().prependTo(o.opts.parentEl),o.$refs={container:e,bg:e.find(".fancybox-bg"),controls:e.find(".fancybox-controls"),buttons:e.find(".fancybox-buttons"),slider_wrap:e.find(".fancybox-slider-wrap"),slider:e.find(".fancybox-slider"),caption:e.find(".fancybox-caption")},o.prevPos=null,o.currPos=0,o.allowZoomIn=!0,o.trigger("onInit"),o.activate(),o.current||o.jumpTo(o.currIndex)},createGroup:function(t){var e=this,s=n.makeArray(t);n.each(s,function(t,s){var i,a,r,c,l,u={},d={};n.isPlainObject(s)?(u=s,d=s.opts||{}):"object"===n.type(s)&&n(s).length?(i=n(s),a=i.data(),d="options"in a?a.options:{},d="object"===n.type(d)?d:{},u.type="type"in a?a.type:d.type,u.src="src"in a?a.src:d.src||i.attr("href"),d.width="width"in a?a.width:d.width,d.height="height"in a?a.height:d.height,d.thumb="thumb"in a?a.thumb:d.thumb,d.caption="caption"in a?a.caption:d.caption||i.attr("title"),d.selector="selector"in a?a.selector:d.selector,d.$orig=i):u={type:"html",content:s+""},u.opts=n.extend(!0,{},e.opts,d),r=u.type,c=u.src||"",r||(u.content?r="html":c.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i)?r="image":c.match(/\.(pdf)((\?|#).*)?$/i)?r="pdf":"#"===c.charAt(0)&&(r="inline"),u.type=r),u.index=e.group.length,u.opts.$orig&&!u.opts.$orig.length&&delete u.opts.$orig,!u.opts.$thumb&&u.opts.$orig&&(u.opts.$thumb=u.opts.$orig.find("img:first")),u.opts.$thumb&&!u.opts.$thumb.length&&delete u.opts.$thumb,"function"===n.type(e.opts.caption)?u.opts.caption=e.opts.caption.apply(s,[e,u]):u.opts.caption=u.opts.caption===o?"":u.opts.caption+"","ajax"===r&&(l=c.split(/\s+/,2),l.length>1&&(u.src=l.shift(),u.opts.selector=l.shift())),"auto"==u.opts.smallBtn&&(n.inArray(r,["html","inline","ajax"])>-1?(u.opts.buttons=!1,u.opts.smallBtn=!0):u.opts.smallBtn=!1),"pdf"===r&&(u.type="iframe",u.opts.closeBtn=!0,u.opts.smallBtn=!1,u.opts.iframe.preload=!1),u.opts.modal&&n.extend(!0,u.opts,{infobar:0,buttons:0,keyboard:0,slideShow:0,fullScreen:0,closeClickOutside:0}),e.group.push(u)})},addEvents:function(){var o=this,s=function(){a.scrollTop(o.scrollTop).scrollLeft(o.scrollLeft),o.$refs.slider_wrap.show(),o.update()};o.removeEvents(),o.$refs.container.on("click.fb-close","[data-fancybox-close]",function(t){t.stopPropagation(),t.preventDefault(),o.close(t)}).on("click.fb-previous","[data-fancybox-previous]",function(t){t.stopPropagation(),t.preventDefault(),o.previous()}).on("click.fb-next","[data-fancybox-next]",function(t){t.stopPropagation(),t.preventDefault(),o.next()}),n(t).on("orientationchange.fb resize.fb",function(t){u(function(){"orientationchange"==t.type?(o.$refs.slider_wrap.hide(),u(s)):s()})}),r.on("focusin.fb",function(t){var e;n.fancybox&&(e=n.fancybox.getInstance(),!e||n(t.target).hasClass("fancybox-container")||n.contains(e.$refs.container[0],t.target)||(t.stopPropagation(),e.focus()))}),n(e).on("keydown.fb",function(t){var e=o.current,s=t.keyCode||t.which;if(e&&e.opts.keyboard&&!n(t.target).is("input")&&!n(t.target).is("textarea")){if(8===s||27===s)return t.preventDefault(),void o.close();switch(s){case 37:case 38:t.preventDefault(),o.previous();break;case 39:case 40:t.preventDefault(),o.next();break;case 80:case 32:t.preventDefault(),o.SlideShow&&(t.preventDefault(),o.SlideShow.toggle());break;case 70:o.FullScreen&&(t.preventDefault(),o.FullScreen.toggle());break;case 71:o.Thumbs&&(t.preventDefault(),o.Thumbs.toggle())}}})},removeEvents:function(){a.off("scroll.fb resize.fb orientationchange.fb"),r.off("keydown.fb focusin.fb click.fb-close"),this.$refs.container.off("click.fb-close click.fb-previous click.fb-next")},previous:function(t){this.current.opts.loop||this.currIndex>0?this.jumpTo(this.currIndex-1,t):this.update(!1,!1,t)},next:function(t){this.current.opts.loop||this.currIndex<this.group.length-1?this.jumpTo(this.currIndex+1,t):this.update(!1,!1,t)},jumpTo:function(t,e){var n,s,i=this,a=null===i.prevIndex;t=parseInt(t,10),n=t,s=t,n%=i.group.length,n=n<0?i.group.length+n:n,i.isAnimating||n==i.currIndex&&!a||(i.group.length>1&&!a&&(2==i.group.length?s=t-i.currIndex+i.currPos:(s=n-i.currIndex+i.currPos,Math.abs(i.currPos-(s+i.group.length))<Math.abs(i.currPos-s)?s+=i.group.length:Math.abs(i.currPos-(s-i.group.length))<Math.abs(i.currPos-s)&&(s-=i.group.length))),i.prevIndex=i.currIndex,i.prevPos=i.currPos,i.currIndex=n,i.currPos=s,i.createSlide(s),i.group.length>1&&((i.opts.loop||s-1>=0)&&i.createSlide(s-1),(i.opts.loop||s+1<i.group.length)&&i.createSlide(s+1)),i.current=i.slides[s],i.current.isMoved=!1,i.current.isComplete=!1,e=parseInt(e===o?i.current.opts.speed:e,10),i.trigger("beforeMove"),i.updateControls(),a?(i.current.$slide.addClass("fancybox-slide--current"),i.$refs.container.show(),i.$refs.bg.css("transition-duration",e+90+"ms").hide().show(0),i.$refs.container.addClass("fancybox-container--ready")):i.$refs.slider.children().removeClass("fancybox-slide--current fancybox-slide--complete"),i.update(!0,!1,a?0:e),i.loadSlide(i.current))},createSlide:function(t){var e,o,s=this;o=t%s.group.length,o=o<0?s.group.length+o:o,!s.slides[t]&&s.group[o]&&(e=n('<div class="fancybox-slide"></div>').appendTo(s.$refs.slider),s.slides[t]=n.extend(!0,{},s.group[o],{pos:t,$slide:e,isMoved:!1,isLoaded:!1}))},zoomInOut:function(t,e,o){var s,i,a,r=this,c=r.current,l=c.$placeholder,u=c.opts.$thumb,h=u?u.offset():0,p=c.$slide.offset(),f=c.opts.opacity;return!!(l&&h&&d(u))&&(n.fancybox.stop(l),r.isAnimating=!0,s={top:h.top-p.top+parseFloat(u.css("border-top-width")||0),left:h.left-p.left+parseFloat(u.css("border-left-width")||0),width:u.width(),height:u.height(),scaleX:1,scaleY:1},"auto"==f&&(f=Math.abs(c.width/c.height-s.width/s.height)>.1),"in"===t?(i=s,a=r.getFitPos(c),a.scaleX=a.width/i.width,a.scaleY=a.height/i.height,f&&(i.opacity=.1,a.opacity=1)):(i=n.fancybox.getTranslate(l),a=s,c.$ghost&&(c.$ghost.show(),c.$image&&c.$image.remove()),i.scaleX=i.width/a.width,i.scaleY=i.height/a.height,i.width=a.width,i.height=a.height,f&&(a.opacity=0)),r.updateCursor(a.width,a.height),delete a.width,delete a.height,n.fancybox.setTranslate(l,i),l.show(),setTimeout(function(){l.css("transition","all "+e+"ms"),n.fancybox.setTranslate(l,a),setTimeout(function(){l.css("transition","none"),o(),r.isAnimating=!1},e+20)},90),!0)},zoomIn:function(){var t=this,e=t.current,o=e.$placeholder;return t.allowZoomIn=!1,t.isOpening=!0,t.zoomInOut("in",e.opts.speed,function(){var s=n.fancybox.getTranslate(o);s.scaleX=1,s.scaleY=1,n.fancybox.setTranslate(o,s),t.isOpening=!1,t.update(!1,!0,0),t.updateCursor(),e.$ghost&&t.setBigImage(e)})},zoomOut:function(t){var e=this,n=e.current;return!!e.zoomInOut("out",n.opts.speed,t)&&(e.$refs.bg.css("transition-duration",n.opts.speed+"ms"),this.$refs.container.removeClass("fancybox-container--ready"),!0)},canPan:function(){var t=this,e=t.current,n=e.$placeholder,o=!1;return n&&(o=t.getFitPos(e),o=Math.abs(n.width()-o.width)>1||Math.abs(n.height()-o.height)>1),o},isScaledDown:function(){var t=this,e=t.current,o=e.$placeholder,s=!1;return o&&(s=n.fancybox.getTranslate(o),s=s.width<e.width||s.height<e.height),s},scaleToActual:function(t,e,s){var i,a,r,c,l,u=this,d=u.current,h=d.$placeholder,p=parseInt(d.$slide.width(),10),f=parseInt(d.$slide.height(),10),g=d.width,b=d.height;h&&(u.isAnimating=!0,t=t===o?.5*p:t,e=e===o?.5*f:e,i=n.fancybox.getTranslate(h),c=g/i.width,l=b/i.height,a=.5*p-.5*g,r=.5*f-.5*b,g>p&&(a=i.left*c-(t*c-t),a>0&&(a=0),a<p-g&&(a=p-g)),b>f&&(r=i.top*l-(e*l-e),r>0&&(r=0),r<f-b&&(r=f-b)),u.updateCursor(g,b),n.fancybox.animate(h,{top:i.top,left:i.left,width:g,height:b,scaleX:i.width/g,scaleY:i.height/b},{top:r,left:a,scaleX:1,scaleY:1},s||d.opts.speed,function(){u.isAnimating=!1}))},scaleToFit:function(t){var e,o=this,s=o.current,i=s.$placeholder;i&&(o.isAnimating=!0,e=o.getFitPos(s),o.updateCursor(e.width,e.height),n.fancybox.animate(i,null,{top:e.top,left:e.left,scaleX:e.width/i.width(),scaleY:e.height/i.height()},t||s.opts.speed,function(){o.isAnimating=!1}))},getFitPos:function(t){var e,o,s,i,r,c,l,u=t.$placeholder||t.$content,d=t.width,h=t.height,p=t.opts.margin;return!(!u||!u.length||!d&&!h)&&("number"===n.type(p)&&(p=[p,p]),2==p.length&&(p=[p[0],p[1],p[0],p[1]]),a.width()<800&&(p=[0,0,0,0]),e=parseInt(t.$slide.width(),10)-(p[1]+p[3]),o=parseInt(t.$slide.height(),10)-(p[0]+p[2]),s=Math.min(1,e/d,o/h),c=Math.floor(s*d),l=Math.floor(s*h),i=Math.floor(.5*(o-l))+p[0],r=Math.floor(.5*(e-c))+p[3],{top:i,left:r,width:c,height:l})},update:function(t,e,o){var s=this,i=s.current.pos*Math.floor(s.current.$slide.width())*-1-s.current.pos*s.current.opts.gutter;s.isOpening!==!0&&(o=parseInt(o,10)||0,n.fancybox.stop(s.$refs.slider),t===!1?s.updateSlide(s.current,e):n.each(s.slides,function(t,n){s.updateSlide(n,e)}),o?n.fancybox.animate(s.$refs.slider,null,{top:0,left:i},o,function(){s.current.isMoved=!0,s.afterMove()}):(n.fancybox.setTranslate(s.$refs.slider,{left:i}),s.afterMove()),s.updateCursor(),s.trigger("onUpdate"))},updateSlide:function(t,e){var o=this,s=t.$placeholder;t=t||o.current,t&&!o.isClosing&&(n.fancybox.setTranslate(t.$slide,{left:t.pos*Math.floor(t.$slide.width())+t.pos*t.opts.gutter}),e!==!1&&s&&n.fancybox.setTranslate(s,o.getFitPos(t)),t.$slide.trigger("refresh"))},updateCursor:function(t,e){var n,s=this,i=s.$refs.container.removeClass("fancybox-controls--canzoomIn fancybox-controls--canzoomOut fancybox-controls--canGrab");!s.isClosing&&s.opts.touch&&(n=t!==o&&e!==o?t<s.current.width&&e<s.current.height:s.isScaledDown(),n?i.addClass("fancybox-controls--canzoomIn"):s.group.length<2?i.addClass("fancybox-controls--canzoomOut"):i.addClass("fancybox-controls--canGrab"))},loadSlide:function(t){var e,o,s,i=this;if(!t)return!1;if(!t.isLoading){switch(i.trigger("beforeLoad",t),e=t.type,o=t.$slide,o.unbind("refresh").trigger("onReset").addClass("fancybox-slide--"+(e||"unknown")).addClass(t.opts.slideClass),t.isLoading=!0,e){case"image":i.setImage(t);break;case"iframe":i.setIframe(t);break;case"html":i.setContent(t,t.content);break;case"inline":n(t.src).length?i.setContent(t,n(t.src)):i.setError(t);break;case"ajax":i.showLoading(t),s=n.ajax(n.extend({},t.opts.ajax.settings,{url:t.src,success:function(e,n){"success"===n&&i.setContent(t,e)},error:function(e,n){e&&"abort"!==n&&i.setError(t)}})),o.one("onReset",function(){s.abort()});break;default:i.setError(t)}return!0}},setImage:function(t){var e=this;return t.isLoaded&&!t.hasError?void e.afterLoad(t):(t.$placeholder=n('<div class="fancybox-placeholder"></div>').hide().prependTo(t.$slide),void(t.opts.preload!==!1&&t.opts.width&&t.opts.height&&(t.opts.thumb||t.opts.$thumb)?(t.width=t.opts.width,t.height=t.opts.height,t.$ghost=n("<img />").addClass("fancybox-image").one("load error",function(){e.isClosing||(n(this).appendTo(t.$placeholder),n("<img/>")[0].src=t.src,e.allowZoomIn&&t.index===e.currIndex&&e.zoomIn()||(e.isOpening=!1,e.updateSlide(t,!0),t.$placeholder.show(),e.setBigImage(t)))}).attr("src",t.opts.thumb||t.opts.$thumb.attr("src"))):e.setBigImage(t)))},setBigImage:function(t){var e=this,o=n("<img />");t.opts.image.protect&&n('<div class="fancybox-spaceball"></div>').appendTo(t.$placeholder),o.one("error",function(){e.setError(t)}).one("load",function(){e.isClosing||(t.$image=o.addClass("fancybox-image").appendTo(t.$placeholder),t.width=this.naturalWidth,t.height=this.naturalHeight,e.afterLoad(t),t.$ghost&&(t.timouts=setTimeout(function(){t.$ghost.hide()},300)))}).attr("src",t.src),o[0].complete?o.trigger("load"):o[0].error?o.trigger("error"):t.timouts=setTimeout(function(){o[0].complete||e.showLoading(t)},150)},revealImage:function(t){t.$placeholder&&(t.$placeholder.show(),t.index===this.currIndex&&this.updateCursor())},setIframe:function(t){var e,s=this,i=t.opts.iframe,a=t.$slide;t.$content=n('<div class="fancybox-content"></div>').css(i.css).appendTo(a),e=n(i.tpl.replace(/\{rnd\}/g,(new Date).getTime())).attr("scrolling",n.fancybox.isTouch?"auto":i.scrolling).appendTo(t.$content),i.preload?(t.$content.addClass("fancybox-tmp"),s.showLoading(t),e.on("load.fb error.fb",function(e){this.isReady=1,t.$slide.trigger("refresh"),s.afterLoad(t)}),a.on("refresh.fb",function(){var n,s,a,r,c,l=t.$content;if(1===e[0].isReady){try{n=e.contents(),s=n.find("body")}catch(t){}s&&s.length&&(i.css.width===o||i.css.height===o)&&(a=e[0].contentWindow.document.documentElement.scrollWidth,r=Math.ceil(s.outerWidth(!0)+(l.width()-a)),c=Math.ceil(s.outerHeight(!0)),l.css({width:i.css.width===o?r+(l.outerWidth()-l.innerWidth()):i.css.width,height:i.css.height===o?c+(l.outerHeight()-l.innerHeight()):i.css.height})),l.removeClass("fancybox-tmp")}})):this.afterLoad(t),e.attr("src",t.src),t.opts.smallBtn&&t.$content.prepend(t.opts.closeTpl),a.one("onReset",function(){try{n(this).find("iframe").hide().attr("src","//about:blank")}catch(t){}n(this).empty()})},setContent:function(t,e){var o=this;o.isClosing||(o.hideLoading(t),t.$slide.empty(),l(e)&&e.parent().length?(e.data("placeholder")&&e.parents(".fancybox-slide").trigger("onReset"),e.data({placeholder:n("<div></div>").hide().insertAfter(e)}).css("display","inline-block")):("string"===n.type(e)&&(e=n("<div>").append(e).contents(),3===e[0].nodeType&&(e=n("<div>").html(e))),t.opts.selector&&(e=n("<div>").html(e).find(t.opts.selector))),t.$slide.one("onReset",function(){var o=l(e)?e.data("placeholder"):0;o&&(e.hide().replaceAll(o),e.data("placeholder",null)),n(this).empty(),t.isLoaded=!1}),t.$content=n(e).appendTo(t.$slide),t.opts.smallBtn===!0&&t.$content.find(".fancybox-close-small").remove().end().eq(0).append(t.opts.closeTpl),this.afterLoad(t))},setError:function(t){t.hasError=!0,this.setContent(t,t.opts.errorTpl)},showLoading:function(t){var e=this;t=t||e.current,t&&!t.$spinner&&(t.$spinner=n(e.opts.spinnerTpl).appendTo(t.$slide))},hideLoading:function(t){var e=this;t=t||e.current,t&&t.$spinner&&(t.$spinner.remove(),delete t.$spinner)},afterMove:function(){var t=this,e=t.current;e&&(e.isMoved=!0,e.$slide.siblings().trigger("onReset"),n.each(t.slides,function(e,n){(n.pos<t.currPos-1||n.pos>t.currPos+1)&&(n.$slide.remove(),delete t.slides[e])}),t.trigger("afterMove"),e.isLoaded&&t.complete())},afterLoad:function(t){var e=this;e.isClosing||(t.isLoading=!1,t.isLoaded=!0,e.trigger("afterLoad",t),e.hideLoading(t),t.$ghost||e.updateSlide(t,!0),t.index===e.currIndex?(t.isMoved?e.complete():e.revealImage(t),e.slides[e.currPos+1]&&"image"===e.slides[e.currPos+1].type&&e.loadSlide(e.slides[e.currPos+1]),e.slides[e.currPos-1]&&"image"===e.slides[e.currPos-1].type&&e.loadSlide(e.slides[e.currPos-1])):e.revealImage(t))},complete:function(){var t=this,e=t.current;e.isComplete=!0,t.allowZoomIn&&t.zoomIn()||(t.isOpening=!1,t.revealImage(e)),e.$slide.addClass("fancybox-slide--complete"),t.opts.focus&&t.focus(),t.trigger("onComplete")},focus:function(){var t=this.current&&this.current.isComplete?this.current.$slide.find('button,:input,[tabindex],a:not(".disabled")').filter(":visible:first"):null;t&&t.length||(t=this.$refs.container),t.focus(),this.$refs.slider_wrap.scrollLeft(0),this.current&&this.current.$slide.scrollTop(0)},activate:function(){var t=this;n(".fancybox-container").each(function(){var e=n(this).data("FancyBox");e&&e.uid!==t.uid&&!e.isClosing&&e.trigger("onDeactivate")}),t.current&&(t.$refs.container.index()>0&&t.$refs.container.prependTo(e.body),t.updateControls()),t.trigger("onActivate"),t.addEvents()},close:function(t){var e=this,o=e.current,s=n.proxy(function(){e.cleanUp(t)},this);return!e.isAnimating&&!e.isClosing&&(e.isClosing=!0,o.timouts&&clearTimeout(o.timouts),t!==!0&&n.fancybox.stop(e.$refs.slider),e.$refs.container.removeClass("fancybox-container--active").addClass("fancybox-container--closing"),o.$slide.removeClass("fancybox-slide--complete").siblings().remove(),o.isMoved||o.$slide.css("overflow","visible"),e.removeEvents(),e.hideLoading(o),e.hideControls(),e.updateCursor(),e.trigger("beforeClose",o,t),void(t===!0?(setTimeout(s,o.opts.speed),this.$refs.container.removeClass("fancybox-container--ready")):e.zoomOut(s)||n.fancybox.animate(e.$refs.container,null,{opacity:0},o.opts.speed,"easeInSine",s)))},cleanUp:function(t){var e,o=this;o.$refs.slider.children().trigger("onReset"),o.$refs.container.empty().remove(),o.current=null,o.trigger("afterClose",t),e=n.fancybox.getInstance(),e?e.activate():(n("body").removeClass("fancybox-enabled"),n("#fancybox-noscroll").remove()),o.$lastFocus&&o.$lastFocus.focus(),a.scrollTop(o.scrollTop).scrollLeft(o.scrollLeft)},trigger:function(t,e){var o=Array.prototype.slice.call(arguments,1),s=this,i=e&&e.opts?e:s.current;i?o.unshift(i):i=s,o.unshift(s),n.isFunction(i.opts[t])&&i.opts[t].apply(i,o),s.$refs.container.trigger(t+".fb",o)},toggleControls:function(t){this.isHiddenControls?this.updateControls(t):this.hideControls()},hideControls:function(){this.isHiddenControls=!0,this.$refs.container.removeClass("fancybox-show-controls"),this.$refs.container.removeClass("fancybox-show-caption")},updateControls:function(t){var e=this,o=e.$refs.container,s=e.$refs.caption,i=e.current,a=i.index,r=i.opts,c=r.caption;this.isHiddenControls&&t!==!0||(this.isHiddenControls=!1,e.$refs.container.addClass("fancybox-show-controls"),o.toggleClass("fancybox-show-infobar",!!r.infobar&&e.group.length>1).toggleClass("fancybox-show-buttons",!!r.buttons).toggleClass("fancybox-is-modal",!!r.modal),n(".fancybox-button--left",o).toggleClass("fancybox-button--disabled",!r.loop&&a<=0),n(".fancybox-button--right",o).toggleClass("fancybox-button--disabled",!r.loop&&a>=e.group.length-1),n(".fancybox-button--play",o).toggle(!!(r.slideShow&&e.group.length>1)),n(".fancybox-button--close",o).toggle(!!r.closeBtn),n(".js-fancybox-count",o).html(e.group.length),n(".js-fancybox-index",o).html(a+1),i.$slide.trigger("refresh"),s&&s.empty(),c&&c.length?(s.html(c),this.$refs.container.addClass("fancybox-show-caption "),e.$caption=s):this.$refs.container.removeClass("fancybox-show-caption"))}}),n.fancybox={version:"3.0.17",defaults:i,getInstance:function(t){var e=n('.fancybox-container:not(".fancybox-container--closing"):first').data("FancyBox"),o=Array.prototype.slice.call(arguments,1);return e instanceof h&&("string"===n.type(t)?e[t].apply(e,o):"function"===n.type(t)&&t.apply(e,o),e)},open:function(t,e,n){return new h(t,e,n)},close:function(t){var e=this.getInstance();e&&(e.close(),t===!0&&this.close())},isTouch:e.createTouch!==o,use3d:function(){var n=e.createElement("div");return t.getComputedStyle(n).getPropertyValue("transform")&&!(e.documentMode&&e.documentMode<=11)}(),getTranslate:function(t){var e,n;return!(!t||!t.length)&&(e=t.get(0).getBoundingClientRect(),n=t.eq(0).css("transform"),n&&n.indexOf("matrix")!==-1?(n=n.split("(")[1],n=n.split(")")[0],n=n.split(",")):n=[],n.length?(n=n.length>10?[n[13],n[12],n[0],n[5]]:[n[5],n[4],n[0],n[3]],n=n.map(parseFloat)):n=[0,0,1,1],{top:n[0],left:n[1],scaleX:n[2],scaleY:n[3],opacity:parseFloat(t.css("opacity")),width:e.width,height:e.height})},setTranslate:function(t,e){var n="",s={};if(t&&e)return e.left===o&&e.top===o||(n=(e.left===o?t.position().top:e.left)+"px, "+(e.top===o?t.position().top:e.top)+"px",n=this.use3d?"translate3d("+n+", 0px)":"translate("+n+")"),e.scaleX!==o&&e.scaleY!==o&&(n=(n.length?n+" ":"")+"scale("+e.scaleX+", "+e.scaleY+")"),n.length&&(s.transform=n),e.opacity!==o&&(s.opacity=e.opacity),e.width!==o&&(s.width=e.width),e.height!==o&&(s.height=e.height),t.css(s)},easing:{easeOutCubic:function(t,e,n,o){return n*((t=t/o-1)*t*t+1)+e},easeInCubic:function(t,e,n,o){return n*(t/=o)*t*t+e},easeOutSine:function(t,e,n,o){return n*Math.sin(t/o*(Math.PI/2))+e},easeInSine:function(t,e,n,o){return-n*Math.cos(t/o*(Math.PI/2))+n+e}},stop:function(t){t.removeData("animateID")},animate:function(t,e,s,i,a,r){var c,l,d,h=this,p=null,f=0,g=function(n){if(c=[],l=0,t.length&&t.data("animateID")===d){if(n=n||Date.now(),p&&(l=n-p),p=n,f+=l,f>=i)return s.scaleX!==o&&s.scaleY!==o&&e.width!==o&&e.height!==o&&(s.width=e.width*s.scaleX,s.height=e.height*s.scaleY,s.scaleX=1,s.scaleY=1),h.setTranslate(t,s),void r();for(var b in s)s.hasOwnProperty(b)&&e[b]!==o&&(e[b]==s[b]?c[b]=s[b]:c[b]=h.easing[a](f,e[b],s[b]-e[b],i));h.setTranslate(t,c),u(g)}};return h.animateID=d=h.animateID===o?1:h.animateID+1,t.data("animateID",d),r===o&&"function"==n.type(a)&&(r=a,a=o),a||(a="easeOutCubic"),r=r||n.noop,i?(e?this.setTranslate(t,e):e=this.getTranslate(t),t.show(),void u(g)):(this.setTranslate(t,s),void r())}},n.fn.fancybox=function(t){return this.off("click.fb-start").on("click.fb-start",{items:this,options:t||{}},s),this},n(e).on("click.fb-start","[data-fancybox]",s)}(window,document,window.jQuery),function(t){"use strict";var e=function(e,n,o){if(e)return o=o||"","object"===t.type(o)&&(o=t.param(o,!0)),t.each(n,function(t,n){e=e.replace("$"+t,n||"")}),o.length&&(e+=(e.indexOf("?")>0?"&":"?")+o),e},n={youtube:{matcher:/(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,params:{autoplay:1,autohide:1,fs:1,rel:0,hd:1,wmode:"transparent",enablejsapi:1,html5:1},paramPlace:8,type:"iframe",url:"//www.youtube.com/embed/$4",thumb:"//img.youtube.com/vi/$4/hqdefault.jpg"},vimeo:{matcher:/((player\.)?vimeo(pro)?\.com)\/(video\/)?([\d]+)?(\?(.*))?/,params:{autoplay:1,hd:1,show_title:1,show_byline:1,show_portrait:0,fullscreen:1,api:1},paramPlace:7,type:"iframe",url:"//player.vimeo.com/video/$5"},metacafe:{matcher:/metacafe.com\/watch\/(\d+)\/(.*)?/,type:"iframe",url:"//www.metacafe.com/embed/$1/?ap=1"},dailymotion:{matcher:/dailymotion.com\/video\/(.*)\/?(.*)/,params:{additionalInfos:0,autoStart:1},type:"iframe",url:"//www.dailymotion.com/embed/video/$1"},vine:{matcher:/vine.co\/v\/([a-zA-Z0-9\?\=\-]+)/,type:"iframe",url:"//vine.co/v/$1/embed/simple"},instagram:{matcher:/(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,type:"image",url:"//$1/p/$2/media/?size=l"},google_maps:{matcher:/(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,type:"iframe",url:function(t){return"//maps.google."+t[2]+"/?ll="+(t[9]?t[9]+"&z="+Math.floor(t[10])+(t[12]?t[12].replace(/^\//,"&"):""):t[12])+"&output="+(t[12]&&t[12].indexOf("layer=c")>0?"svembed":"embed")}}};t(document).on("onInit.fb",function(o,s){t.each(s.group,function(o,s){var i,a,r,c,l,u,d,h=s.src||"",p=!1;s.type||(t.each(n,function(n,o){if(a=h.match(o.matcher),l={},d=n,a){if(p=o.type,o.paramPlace&&a[o.paramPlace]){c=a[o.paramPlace],"?"==c[0]&&(c=c.substring(1)),c=c.split("&");for(var f=0;f<c.length;++f){var g=c[f].split("=",2);2==g.length&&(l[g[0]]=decodeURIComponent(g[1].replace(/\+/g," ")))}}return o.idPlace&&(u=a[o.idPlace]),r=t.extend(!0,{},o.params,s.opts[n],l),h="function"===t.type(o.url)?o.url.call(this,a,r,s):e(o.url,a,r),i="function"===t.type(o.thumb)?o.thumb.call(this,a,r,s):e(o.thumb,a),!1}}),p?(s.src=h,s.type=p,s.opts.thumb||s.opts.$thumb&&s.opts.$thumb.length||(s.opts.thumb=i),u&&(s.opts.id=d+"-"+u),"iframe"===p&&(t.extend(!0,s.opts,{iframe:{preload:!1,scrolling:"no"},smallBtn:!1,closeBtn:!0,fullScreen:!1,slideShow:!1}),s.opts.slideClass+=" fancybox-slide--video")):s.type="iframe")})})}(window.jQuery),function(t,e,n){"use strict";var o=function(){return t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||function(e){t.setTimeout(e,1e3/60)}}(),s=function(e){var n=[];e=e.originalEvent||e||t.e,e=e.touches&&e.touches.length?e.touches:e.changedTouches&&e.changedTouches.length?e.changedTouches:[e];for(var o in e)e[o].pageX?n.push({x:e[o].pageX,y:e[o].pageY}):e[o].clientX&&n.push({x:e[o].clientX,y:e[o].clientY});return n},i=function(t,e,n){return"x"===n?t.x-e.x:"y"===n?t.y-e.y:Math.sqrt(Math.pow(t.x-e.x,2)+Math.pow(t.y-e.y,2))},a=function(t){return t.is("a")||t.is("button")||t.is("input")||t.is("select")||t.is("textarea")||n.isFunction(t.get(0).onclick)},r=function(e){var n=t.getComputedStyle(e)["overflow-y"],o=t.getComputedStyle(e)["overflow-x"],s=("scroll"===n||"auto"===n)&&e.scrollHeight>e.clientHeight,i=("scroll"===o||"auto"===o)&&e.scrollWidth>e.clientWidth;return s||i},c=function(t){for(var e=!1;!(e=r(t.get(0)))&&(t=t.parent(),t.length&&!t.hasClass("fancybox-slider")&&!t.is("body")););return e},l=function(t){var e=this;e.instance=t,e.$wrap=t.$refs.slider_wrap,e.$slider=t.$refs.slider,e.$container=t.$refs.container,e.destroy(),e.$wrap.on("touchstart.fb mousedown.fb",n.proxy(e,"ontouchstart"))};l.prototype.destroy=function(){this.$wrap.off("touchstart.fb mousedown.fb touchmove.fb mousemove.fb touchend.fb touchcancel.fb mouseup.fb mouseleave.fb")},l.prototype.ontouchstart=function(e){var r=this,l=n(e.target),u=r.instance,d=u.current,h=d.$content||d.$placeholder,p=function(){r.sliderNewPos&&(n.fancybox.setTranslate(r.$slider,r.sliderNewPos),r.sliderNewPos=null),r.contentNewPos&&(n.fancybox.setTranslate(r.$content,r.contentNewPos),r.contentNewPos=null),(r.isSwiping||r.isPanning||r.isZooming)&&o(p)};a(l)||a(l.parent())||c(l)&&!l.hasClass("fancybox-slide")||(e.stopPropagation(),e.preventDefault(),!d||r.instance.isAnimating||r.instance.isClosing||(r.startPoints=s(e),r.startPoints.length>1&&!d.isMoved||(r.$wrap.off("touchmove.fb mousemove.fb",n.proxy(r,"ontouchmove")),r.$wrap.off("touchend.fb touchcancel.fb mouseup.fb mouseleave.fb",n.proxy(r,"ontouchend")),r.$wrap.on("touchmove.fb mousemove.fb",n.proxy(r,"ontouchmove")),r.$wrap.on("touchend.fb touchcancel.fb mouseup.fb mouseleave.fb",n.proxy(r,"ontouchend")),r.$target=l,r.$content=h,r.startTime=(new Date).getTime(),r.distanceX=r.distanceY=r.distance=0,r.canvasWidth=Math.round(d.$slide.width()),r.canvasHeight=Math.round(d.$slide.height()),r.canTap=d.isMoved,r.isPanning=!1,r.isSwiping=!1,r.isZooming=!1,r.sliderStartPos=n.fancybox.getTranslate(r.$slider),r.sliderNewPos=null,r.contentStartPos=n.fancybox.getTranslate(r.$content),r.contentNewPos=null,1==r.startPoints.length&&("image"===d.type&&(r.contentStartPos.width>r.canvasWidth+1||r.contentStartPos.height>r.canvasHeight+1)?(n.fancybox.stop(r.$content),r.isPanning=!0):(n.fancybox.stop(r.$slider),r.isSwiping=!0),r.$container.addClass("fancybox-controls--isGrabbing")),"image"!==d.type||d.hasError||2!=r.startPoints.length||!d.isLoaded&&!d.$ghost||(r.isZooming=!0,r.canTap=!1,n.fancybox.stop(r.$content),r.centerPointStartX=(r.startPoints[0].x+r.startPoints[1].x)/2-n(t).scrollLeft(),r.centerPointStartY=(r.startPoints[0].y+r.startPoints[1].y)/2-n(t).scrollTop(),r.percentageOfImageAtPinchPointX=(r.centerPointStartX-r.contentStartPos.left)/r.contentStartPos.width,r.percentageOfImageAtPinchPointY=(r.centerPointStartY-r.contentStartPos.top)/r.contentStartPos.height,r.startDistanceBetweenFingers=i(r.startPoints[0],r.startPoints[1])),p())))},l.prototype.ontouchmove=function(t){var e=this;t.preventDefault(),e.instance.isAnimating||(e.newPoints=s(t),e.newPoints.length&&(e.distanceX=i(e.newPoints[0],e.startPoints[0],"x"),e.distanceY=i(e.newPoints[0],e.startPoints[0],"y"),e.distance=i(e.newPoints[0],e.startPoints[0]),e.distance>0&&(e.isSwiping?e.onSwipe():e.isPanning?e.onPan():e.isZooming&&e.onZoom())))},l.prototype.onSwipe=function(){var e,o=this,s=o.isSwiping;s===!0?Math.abs(o.distance)>10&&(e=Math.abs(180*Math.atan2(o.distanceY,o.distanceX)/Math.PI),s=e>45&&e<135||o.instance.group.length<=1?"y":"x",(o.instance.opts.touch.vertical===!1||"auto"===o.instance.opts.touch.vertical&&n(t).width()>800)&&(s="x"),o.isSwiping=s,o.canTap=!1,o.instance.current.isMoved=!1,o.instance.allowZoomIn=!1,o.startPoints=o.newPoints):o.sliderNewPos={top:"x"==s?0:o.sliderStartPos.top+o.distanceY,left:"y"==s?o.sliderStartPos.left:o.sliderStartPos.left+o.distanceX}},l.prototype.onPan=function(){var t,e,n,o=this;o.canTap=!1,t=o.contentStartPos.width>o.canvasWidth?o.contentStartPos.left+o.distanceX:o.contentStartPos.left,e=o.contentStartPos.top+o.distanceY,n=o.limitMovement(t,e,o.contentStartPos.width,o.contentStartPos.height),n.scaleX=o.contentStartPos.scaleX,n.scaleY=o.contentStartPos.scaleY,o.contentNewPos=n,o.contentLastPos=n},l.prototype.limitMovement=function(t,e,n,o){var s,i,a,r,c=this,l=c.canvasWidth,u=c.canvasHeight,d=c.contentStartPos.left,h=c.contentStartPos.top,p=c.distanceX,f=c.distanceY;return s=Math.max(0,.5*l-.5*n),i=Math.max(0,.5*u-.5*o),a=Math.min(l-n,.5*l-.5*n),r=Math.min(u-o,.5*u-.5*o),n>l&&(p>0&&t>s&&(t=s-1+Math.pow(-s+d+p,.8)||0),p<0&&t<a&&(t=a+1-Math.pow(a-d-p,.8)||0)),o>u&&(f>0&&e>i&&(e=i-1+Math.pow(-i+h+f,.8)||0),f<0&&e<r&&(e=r+1-Math.pow(r-h-f,.8)||0)),{top:e,left:t}},l.prototype.limitPosition=function(t,e,n,o){var s=this,i=s.canvasWidth,a=s.canvasHeight;return n>i?(t=t>0?0:t,t=t<i-n?i-n:t):t=Math.max(0,i/2-n/2),o>a?(e=e>0?0:e,e=e<a-o?a-o:e):e=Math.max(0,a/2-o/2),{top:e,left:t}},l.prototype.onZoom=function(){var e=this,o=e.contentStartPos.width,s=e.contentStartPos.height,a=e.contentStartPos.left,r=e.contentStartPos.top,c=i(e.newPoints[0],e.newPoints[1]),l=c/e.startDistanceBetweenFingers,u=Math.floor(o*l),d=Math.floor(s*l),h=(o-u)*e.percentageOfImageAtPinchPointX,p=(s-d)*e.percentageOfImageAtPinchPointY,f=(e.newPoints[0].x+e.newPoints[1].x)/2-n(t).scrollLeft(),g=(e.newPoints[0].y+e.newPoints[1].y)/2-n(t).scrollTop(),b=f-e.centerPointStartX,m=g-e.centerPointStartY,y=a+(h+b),v=r+(p+m),x={
top:v,left:y,scaleX:e.contentStartPos.scaleX*l,scaleY:e.contentStartPos.scaleY*l};e.canTap=!1,e.newWidth=u,e.newHeight=d,e.contentNewPos=x,e.contentLastPos=x},l.prototype.ontouchend=function(t){var e=this,o=e.instance.current,i=Math.max((new Date).getTime()-e.startTime,1),a=e.isSwiping,r=e.isPanning,c=e.isZooming;if(e.endPoints=s(t),!(e.endPoints.length>1)||"x"!==e.isSwiping&&"y"!==e.isSwiping){if(e.$container.removeClass("fancybox-controls--isGrabbing"),e.$wrap.off("touchmove.fb mousemove.fb",n.proxy(this,"ontouchmove")),e.$wrap.off("touchend.fb touchcancel.fb mouseup.fb mouseleave.fb",n.proxy(this,"ontouchend")),e.isSwiping=!1,e.isPanning=!1,e.isZooming=!1,e.canTap)return e.ontap();e.velocityX=e.distanceX/i*.5,e.velocityY=e.distanceY/i*.5,e.speed=o.opts.speed,e.speedX=Math.max(.75*e.speed,Math.min(1.5*e.speed,1/Math.abs(e.velocityX)*e.speed)),e.speedY=Math.max(.75*e.speed,Math.min(1.5*e.speed,1/Math.abs(e.velocityY)*e.speed)),a?e.endSwiping(a):r?e.endPanning():c&&e.endZooming()}},l.prototype.endSwiping=function(t){var e=this;"y"==t&&Math.abs(e.distanceY)>50?(n.fancybox.animate(e.$slider,null,{top:e.sliderStartPos.top+e.distanceY+150*e.velocityY,left:e.sliderStartPos.left,opacity:0},e.speedY),e.instance.close(!0)):"x"==t&&e.distanceX>50?e.instance.previous(e.speedX):"x"==t&&e.distanceX<-50?e.instance.next(e.speedX):e.instance.update(!1,!0,e.speedX)},l.prototype.endPanning=function(){var t=this,e=t.contentLastPos.left+t.velocityX*t.speed*2,o=t.contentLastPos.top+t.velocityY*t.speed*2,s=t.limitPosition(e,o,t.contentStartPos.width,t.contentStartPos.height);s.width=t.contentStartPos.width,s.height=t.contentStartPos.height,n.fancybox.animate(t.$content,null,s,t.speed,"easeOutSine")},l.prototype.endZooming=function(){var t,e=this,o=e.instance.current,s=e.contentLastPos.left,i=e.contentLastPos.top,a=e.newWidth,r=e.newHeight,c={top:i,left:s,width:a,height:r,scaleX:1,scaleY:1};n.fancybox.setTranslate(e.$content,c),a<e.canvasWidth&&r<e.canvasHeight?e.instance.scaleToFit(150):a>o.width||r>o.height?e.instance.scaleToActual(e.centerPointStartX,e.centerPointStartY,150):(t=e.limitPosition(s,i,a,r),n.fancybox.animate(e.$content,null,t,e.speed,"easeOutSine"))},l.prototype.ontap=function(){var t=this,e=t.endPoints[0].x,o=t.endPoints[0].y;if(e-=t.$wrap.offset().left,o-=t.$wrap.offset().top,!n.fancybox.isTouch)return t.instance.opts.closeClickOutside&&t.$target.is(".fancybox-slide")?void t.instance.close():void("image"==t.instance.current.type&&t.instance.current.isMoved&&(t.instance.canPan()?t.instance.scaleToFit():t.instance.isScaledDown()?t.instance.scaleToActual(e,o):t.instance.group.length<2&&t.instance.close()));if(t.tapped){if(t.tapped=!1,clearTimeout(t.id),Math.abs(e-t.x)>50||Math.abs(o-t.y)>50||!t.instance.current.isMoved)return this;if(!t.instance.current.isLoaded&&!t.instance.current.$ghost)return;"image"==t.instance.current.type&&(t.instance.canPan()?t.instance.scaleToFit():t.instance.isScaledDown()&&t.instance.scaleToActual(e,o))}else t.tapped=!0,t.x=e,t.y=o,t.id=setTimeout(function(){t.tapped=!1,t.instance.toggleControls(!0)},300);return this},n(e).on("onActivate.fb",function(t,e){e.opts.touch&&!e.Guestures&&(e.Guestures=new l(e))}),n(e).on("beforeClose.fb",function(t,e){e.Guestures&&e.Guestures.destroy()})}(window,document,window.jQuery),function(t,e){"use strict";var n=function(t){this.instance=t,this.init()};e.extend(n.prototype,{timer:null,speed:3e3,$button:null,init:function(){var t=this;t.$button=e('<button data-fancybox-play class="fancybox-button fancybox-button--play" title="Slideshow (P)"></button>').appendTo(t.instance.$refs.buttons),t.instance.$refs.container.on("click","[data-fancybox-play]",function(){t.toggle()})},set:function(){var t=this;t.instance&&t.instance.current&&t.instance.currIndex<t.instance.group.length-1?t.timer=setTimeout(function(){t.instance.next()},t.speed):t.stop()},clear:function(){var t=this;clearTimeout(t.timer),t.timer=null},start:function(){var t=this;t.stop(),t.instance&&t.instance.current&&t.instance.currIndex<t.instance.group.length-1&&(t.instance.$refs.container.on({"beforeLoad.fb.player":e.proxy(t,"clear"),"onComplete.fb.player":e.proxy(t,"set")}),t.instance.current.isComplete?t.set():t.timer=!0,t.instance.$refs.container.trigger("onPlayStart"),t.$button.addClass("fancybox-button--pause"))},stop:function(){var t=this;t.clear(),t.instance.$refs.container.trigger("onPlayEnd").off(".player"),t.$button.removeClass("fancybox-button--pause")},toggle:function(){var t=this;t.timer?t.stop():t.start()}}),e(t).on("onInit.fb",function(t,e){e.opts.slideShow&&!e.SlideShow&&e.group.length>1&&(e.SlideShow=new n(e))}),e(t).on("beforeClose.fb onDeactivate.fb",function(t,e){e.SlideShow&&e.SlideShow.stop()})}(document,window.jQuery),function(t,e){"use strict";var n=function(t){this.instance=t,this.init()};e.extend(n.prototype,{$button:null,init:function(){var n=this;n.isAvailable()&&(n.$button=e('<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fullscreen" title="Full screen (F)"></button>').appendTo(n.instance.$refs.buttons),n.instance.$refs.container.on("click.fb-fullscreen","[data-fancybox-fullscreen]",function(t){t.stopPropagation(),t.preventDefault(),n.toggle()}),e(t).on("onUpdate.fb",function(t,e){n.$button.toggle(!!e.current.opts.fullScreen),n.$button.toggleClass("fancybox-button-shrink",n.isActivated())}),e(t).on("afterClose.fb",function(){n.exit()}))},isAvailable:function(){var t=this.instance.$refs.container.get(0);return!!(t.requestFullscreen||t.msRequestFullscreen||t.mozRequestFullScreen||t.webkitRequestFullscreen)},isActivated:function(){return!!(t.fullscreenElement||t.mozFullScreenElement||t.webkitFullscreenElement||t.msFullscreenElement)},launch:function(){var t=this.instance.$refs.container.get(0);t&&!this.instance.isClosing&&(t.requestFullscreen?t.requestFullscreen():t.msRequestFullscreen?t.msRequestFullscreen():t.mozRequestFullScreen?t.mozRequestFullScreen():t.webkitRequestFullscreen&&t.webkitRequestFullscreen(t.ALLOW_KEYBOARD_INPUT))},exit:function(){t.exitFullscreen?t.exitFullscreen():t.msExitFullscreen?t.msExitFullscreen():t.mozCancelFullScreen?t.mozCancelFullScreen():t.webkitExitFullscreen&&t.webkitExitFullscreen()},toggle:function(){this.isActivated()?this.exit():this.isAvailable()&&this.launch()}}),e(t).on("onInit.fb",function(t,e){e.opts.fullScreen&&!e.FullScreen&&(e.FullScreen=new n(e))})}(document,window.jQuery),function(t,e){"use strict";var n=function(t){this.instance=t,this.init()};e.extend(n.prototype,{$button:null,$grid:null,$list:null,isVisible:!1,init:function(){var t=this;t.$button=e('<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="Thumbnails (G)"></button>').appendTo(this.instance.$refs.buttons).on("touchend click",function(e){e.stopPropagation(),e.preventDefault(),t.toggle()})},create:function(){var t,n,o=this.instance;this.$grid=e('<div class="fancybox-thumbs"></div>').appendTo(o.$refs.container),t="<ul>",e.each(o.group,function(e,o){n=o.opts.thumb||(o.opts.$thumb?o.opts.$thumb.attr("src"):null),n||"image"!==o.type||(n=o.src),n&&n.length&&(t+='<li data-index="'+e+'"  tabindex="0" class="fancybox-thumbs-loading"><img data-src="'+n+'" /></li>')}),t+="</ul>",this.$list=e(t).appendTo(this.$grid).on("click touchstart","li",function(){o.jumpTo(e(this).data("index"))}),this.$list.find("img").hide().one("load",function(){var t,n,o,s,i=e(this).parent().removeClass("fancybox-thumbs-loading"),a=i.outerWidth(),r=i.outerHeight();t=this.naturalWidth||this.width,n=this.naturalHeight||this.height,o=t/a,s=n/r,o>=1&&s>=1&&(o>s?(t/=s,n=r):(t=a,n/=o)),e(this).css({width:Math.floor(t),height:Math.floor(n),"margin-top":Math.min(0,Math.floor(.3*r-.3*n)),"margin-left":Math.min(0,Math.floor(.5*a-.5*t))}).show()}).each(function(){this.src=e(this).data("src")})},focus:function(){this.instance.current&&this.$list.children().removeClass("fancybox-thumbs-active").filter('[data-index="'+this.instance.current.index+'"]').addClass("fancybox-thumbs-active").focus()},close:function(){this.$grid.hide()},update:function(){this.instance.$refs.container.toggleClass("fancybox-container--thumbs",this.isVisible),this.isVisible?(this.$grid||this.create(),this.$grid.show(),this.focus()):this.$grid&&this.$grid.hide(),this.instance.allowZoomIn||this.instance.update()},hide:function(){this.isVisible=!1,this.update()},show:function(){this.isVisible=!0,this.update()},toggle:function(){this.isVisible?this.hide():this.show()}}),e(t).on("onInit.fb",function(t,e){e.opts.thumbs&&!e.Thumbs&&e.group.length>1&&("image"==e.group[0].type||e.group[0].opts.thumb)&&("image"==e.group[1].type||e.group[1].opts.thumb)&&(e.Thumbs=new n(e))}),e(t).on("beforeMove.fb",function(t,e,n){var o=e.Thumbs;o&&(n.modal?(o.$button.hide(),o.hide()):(o.$button.show(),e.opts.thumbs.showOnStart===!0&&e.allowZoomIn?o.show():o.isVisible&&o.focus()))}),e(t).on("beforeClose.fb",function(t,e){e.Thumbs&&e.Thumbs.isVisible&&e.opts.thumbs.hideOnClosing!==!1&&e.Thumbs.close(),e.Thumbs=null})}(document,window.jQuery);

// SLICK
(function(i){"use strict";"function"==typeof define&&define.amd?define(["jquery"],i):"undefined"!=typeof exports?module.exports=i(require("jquery")):i(jQuery)})(function(i){"use strict";var e=window.Slick||{};e=function(){function e(e,o){var s,n=this;n.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:i(e),appendDots:i(e),arrows:!0,asNavFor:null,prevArrow:'<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',nextArrow:'<button class="slick-next" aria-label="Next" type="button">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(e,t){return i('<button type="button" />').text(t+1)},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,focusOnChange:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnFocus:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,useTransform:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1e3},n.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,scrolling:!1,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,swiping:!1,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},i.extend(n,n.initials),n.activeBreakpoint=null,n.animType=null,n.animProp=null,n.breakpoints=[],n.breakpointSettings=[],n.cssTransitions=!1,n.focussed=!1,n.interrupted=!1,n.hidden="hidden",n.paused=!0,n.positionProp=null,n.respondTo=null,n.rowCount=1,n.shouldClick=!0,n.$slider=i(e),n.$slidesCache=null,n.transformType=null,n.transitionType=null,n.visibilityChange="visibilitychange",n.windowWidth=0,n.windowTimer=null,s=i(e).data("slick")||{},n.options=i.extend({},n.defaults,o,s),n.currentSlide=n.options.initialSlide,n.originalSettings=n.options,"undefined"!=typeof document.mozHidden?(n.hidden="mozHidden",n.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(n.hidden="webkitHidden",n.visibilityChange="webkitvisibilitychange"),n.autoPlay=i.proxy(n.autoPlay,n),n.autoPlayClear=i.proxy(n.autoPlayClear,n),n.autoPlayIterator=i.proxy(n.autoPlayIterator,n),n.changeSlide=i.proxy(n.changeSlide,n),n.clickHandler=i.proxy(n.clickHandler,n),n.selectHandler=i.proxy(n.selectHandler,n),n.setPosition=i.proxy(n.setPosition,n),n.swipeHandler=i.proxy(n.swipeHandler,n),n.dragHandler=i.proxy(n.dragHandler,n),n.keyHandler=i.proxy(n.keyHandler,n),n.instanceUid=t++,n.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,n.registerBreakpoints(),n.init(!0)}var t=0;return e}(),e.prototype.activateADA=function(){var i=this;i.$slideTrack.find(".slick-active").attr({"aria-hidden":"false"}).find("a, input, button, select").attr({tabindex:"0"})},e.prototype.addSlide=e.prototype.slickAdd=function(e,t,o){var s=this;if("boolean"==typeof t)o=t,t=null;else if(t<0||t>=s.slideCount)return!1;s.unload(),"number"==typeof t?0===t&&0===s.$slides.length?i(e).appendTo(s.$slideTrack):o?i(e).insertBefore(s.$slides.eq(t)):i(e).insertAfter(s.$slides.eq(t)):o===!0?i(e).prependTo(s.$slideTrack):i(e).appendTo(s.$slideTrack),s.$slides=s.$slideTrack.children(this.options.slide),s.$slideTrack.children(this.options.slide).detach(),s.$slideTrack.append(s.$slides),s.$slides.each(function(e,t){i(t).attr("data-slick-index",e)}),s.$slidesCache=s.$slides,s.reinit()},e.prototype.animateHeight=function(){var i=this;if(1===i.options.slidesToShow&&i.options.adaptiveHeight===!0&&i.options.vertical===!1){var e=i.$slides.eq(i.currentSlide).outerHeight(!0);i.$list.animate({height:e},i.options.speed)}},e.prototype.animateSlide=function(e,t){var o={},s=this;s.animateHeight(),s.options.rtl===!0&&s.options.vertical===!1&&(e=-e),s.transformsEnabled===!1?s.options.vertical===!1?s.$slideTrack.animate({left:e},s.options.speed,s.options.easing,t):s.$slideTrack.animate({top:e},s.options.speed,s.options.easing,t):s.cssTransitions===!1?(s.options.rtl===!0&&(s.currentLeft=-s.currentLeft),i({animStart:s.currentLeft}).animate({animStart:e},{duration:s.options.speed,easing:s.options.easing,step:function(i){i=Math.ceil(i),s.options.vertical===!1?(o[s.animType]="translate("+i+"px, 0px)",s.$slideTrack.css(o)):(o[s.animType]="translate(0px,"+i+"px)",s.$slideTrack.css(o))},complete:function(){t&&t.call()}})):(s.applyTransition(),e=Math.ceil(e),s.options.vertical===!1?o[s.animType]="translate3d("+e+"px, 0px, 0px)":o[s.animType]="translate3d(0px,"+e+"px, 0px)",s.$slideTrack.css(o),t&&setTimeout(function(){s.disableTransition(),t.call()},s.options.speed))},e.prototype.getNavTarget=function(){var e=this,t=e.options.asNavFor;return t&&null!==t&&(t=i(t).not(e.$slider)),t},e.prototype.asNavFor=function(e){var t=this,o=t.getNavTarget();null!==o&&"object"==typeof o&&o.each(function(){var t=i(this).slick("getSlick");t.unslicked||t.slideHandler(e,!0)})},e.prototype.applyTransition=function(i){var e=this,t={};e.options.fade===!1?t[e.transitionType]=e.transformType+" "+e.options.speed+"ms "+e.options.cssEase:t[e.transitionType]="opacity "+e.options.speed+"ms "+e.options.cssEase,e.options.fade===!1?e.$slideTrack.css(t):e.$slides.eq(i).css(t)},e.prototype.autoPlay=function(){var i=this;i.autoPlayClear(),i.slideCount>i.options.slidesToShow&&(i.autoPlayTimer=setInterval(i.autoPlayIterator,i.options.autoplaySpeed))},e.prototype.autoPlayClear=function(){var i=this;i.autoPlayTimer&&clearInterval(i.autoPlayTimer)},e.prototype.autoPlayIterator=function(){var i=this,e=i.currentSlide+i.options.slidesToScroll;i.paused||i.interrupted||i.focussed||(i.options.infinite===!1&&(1===i.direction&&i.currentSlide+1===i.slideCount-1?i.direction=0:0===i.direction&&(e=i.currentSlide-i.options.slidesToScroll,i.currentSlide-1===0&&(i.direction=1))),i.slideHandler(e))},e.prototype.buildArrows=function(){var e=this;e.options.arrows===!0&&(e.$prevArrow=i(e.options.prevArrow).addClass("slick-arrow"),e.$nextArrow=i(e.options.nextArrow).addClass("slick-arrow"),e.slideCount>e.options.slidesToShow?(e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.prependTo(e.options.appendArrows),e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.appendTo(e.options.appendArrows),e.options.infinite!==!0&&e.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))},e.prototype.buildDots=function(){var e,t,o=this;if(o.options.dots===!0&&o.slideCount>o.options.slidesToShow){for(o.$slider.addClass("slick-dotted"),t=i("<ul />").addClass(o.options.dotsClass),e=0;e<=o.getDotCount();e+=1)t.append(i("<li />").append(o.options.customPaging.call(this,o,e)));o.$dots=t.appendTo(o.options.appendDots),o.$dots.find("li").first().addClass("slick-active")}},e.prototype.buildOut=function(){var e=this;e.$slides=e.$slider.children(e.options.slide+":not(.slick-cloned)").addClass("slick-slide"),e.slideCount=e.$slides.length,e.$slides.each(function(e,t){i(t).attr("data-slick-index",e).data("originalStyling",i(t).attr("style")||"")}),e.$slider.addClass("slick-slider"),e.$slideTrack=0===e.slideCount?i('<div class="slick-track"/>').appendTo(e.$slider):e.$slides.wrapAll('<div class="slick-track"/>').parent(),e.$list=e.$slideTrack.wrap('<div class="slick-list"/>').parent(),e.$slideTrack.css("opacity",0),e.options.centerMode!==!0&&e.options.swipeToSlide!==!0||(e.options.slidesToScroll=1),i("img[data-lazy]",e.$slider).not("[src]").addClass("slick-loading"),e.setupInfinite(),e.buildArrows(),e.buildDots(),e.updateDots(),e.setSlideClasses("number"==typeof e.currentSlide?e.currentSlide:0),e.options.draggable===!0&&e.$list.addClass("draggable")},e.prototype.buildRows=function(){var i,e,t,o,s,n,r,l=this;if(o=document.createDocumentFragment(),n=l.$slider.children(),l.options.rows>0){for(r=l.options.slidesPerRow*l.options.rows,s=Math.ceil(n.length/r),i=0;i<s;i++){var d=document.createElement("div");for(e=0;e<l.options.rows;e++){var a=document.createElement("div");for(t=0;t<l.options.slidesPerRow;t++){var c=i*r+(e*l.options.slidesPerRow+t);n.get(c)&&a.appendChild(n.get(c))}d.appendChild(a)}o.appendChild(d)}l.$slider.empty().append(o),l.$slider.children().children().children().css({width:100/l.options.slidesPerRow+"%",display:"inline-block"})}},e.prototype.checkResponsive=function(e,t){var o,s,n,r=this,l=!1,d=r.$slider.width(),a=window.innerWidth||i(window).width();if("window"===r.respondTo?n=a:"slider"===r.respondTo?n=d:"min"===r.respondTo&&(n=Math.min(a,d)),r.options.responsive&&r.options.responsive.length&&null!==r.options.responsive){s=null;for(o in r.breakpoints)r.breakpoints.hasOwnProperty(o)&&(r.originalSettings.mobileFirst===!1?n<r.breakpoints[o]&&(s=r.breakpoints[o]):n>r.breakpoints[o]&&(s=r.breakpoints[o]));null!==s?null!==r.activeBreakpoint?(s!==r.activeBreakpoint||t)&&(r.activeBreakpoint=s,"unslick"===r.breakpointSettings[s]?r.unslick(s):(r.options=i.extend({},r.originalSettings,r.breakpointSettings[s]),e===!0&&(r.currentSlide=r.options.initialSlide),r.refresh(e)),l=s):(r.activeBreakpoint=s,"unslick"===r.breakpointSettings[s]?r.unslick(s):(r.options=i.extend({},r.originalSettings,r.breakpointSettings[s]),e===!0&&(r.currentSlide=r.options.initialSlide),r.refresh(e)),l=s):null!==r.activeBreakpoint&&(r.activeBreakpoint=null,r.options=r.originalSettings,e===!0&&(r.currentSlide=r.options.initialSlide),r.refresh(e),l=s),e||l===!1||r.$slider.trigger("breakpoint",[r,l])}},e.prototype.changeSlide=function(e,t){var o,s,n,r=this,l=i(e.currentTarget);switch(l.is("a")&&e.preventDefault(),l.is("li")||(l=l.closest("li")),n=r.slideCount%r.options.slidesToScroll!==0,o=n?0:(r.slideCount-r.currentSlide)%r.options.slidesToScroll,e.data.message){case"previous":s=0===o?r.options.slidesToScroll:r.options.slidesToShow-o,r.slideCount>r.options.slidesToShow&&r.slideHandler(r.currentSlide-s,!1,t);break;case"next":s=0===o?r.options.slidesToScroll:o,r.slideCount>r.options.slidesToShow&&r.slideHandler(r.currentSlide+s,!1,t);break;case"index":var d=0===e.data.index?0:e.data.index||l.index()*r.options.slidesToScroll;r.slideHandler(r.checkNavigable(d),!1,t),l.children().trigger("focus");break;default:return}},e.prototype.checkNavigable=function(i){var e,t,o=this;if(e=o.getNavigableIndexes(),t=0,i>e[e.length-1])i=e[e.length-1];else for(var s in e){if(i<e[s]){i=t;break}t=e[s]}return i},e.prototype.cleanUpEvents=function(){var e=this;e.options.dots&&null!==e.$dots&&(i("li",e.$dots).off("click.slick",e.changeSlide).off("mouseenter.slick",i.proxy(e.interrupt,e,!0)).off("mouseleave.slick",i.proxy(e.interrupt,e,!1)),e.options.accessibility===!0&&e.$dots.off("keydown.slick",e.keyHandler)),e.$slider.off("focus.slick blur.slick"),e.options.arrows===!0&&e.slideCount>e.options.slidesToShow&&(e.$prevArrow&&e.$prevArrow.off("click.slick",e.changeSlide),e.$nextArrow&&e.$nextArrow.off("click.slick",e.changeSlide),e.options.accessibility===!0&&(e.$prevArrow&&e.$prevArrow.off("keydown.slick",e.keyHandler),e.$nextArrow&&e.$nextArrow.off("keydown.slick",e.keyHandler))),e.$list.off("touchstart.slick mousedown.slick",e.swipeHandler),e.$list.off("touchmove.slick mousemove.slick",e.swipeHandler),e.$list.off("touchend.slick mouseup.slick",e.swipeHandler),e.$list.off("touchcancel.slick mouseleave.slick",e.swipeHandler),e.$list.off("click.slick",e.clickHandler),i(document).off(e.visibilityChange,e.visibility),e.cleanUpSlideEvents(),e.options.accessibility===!0&&e.$list.off("keydown.slick",e.keyHandler),e.options.focusOnSelect===!0&&i(e.$slideTrack).children().off("click.slick",e.selectHandler),i(window).off("orientationchange.slick.slick-"+e.instanceUid,e.orientationChange),i(window).off("resize.slick.slick-"+e.instanceUid,e.resize),i("[draggable!=true]",e.$slideTrack).off("dragstart",e.preventDefault),i(window).off("load.slick.slick-"+e.instanceUid,e.setPosition)},e.prototype.cleanUpSlideEvents=function(){var e=this;e.$list.off("mouseenter.slick",i.proxy(e.interrupt,e,!0)),e.$list.off("mouseleave.slick",i.proxy(e.interrupt,e,!1))},e.prototype.cleanUpRows=function(){var i,e=this;e.options.rows>0&&(i=e.$slides.children().children(),i.removeAttr("style"),e.$slider.empty().append(i))},e.prototype.clickHandler=function(i){var e=this;e.shouldClick===!1&&(i.stopImmediatePropagation(),i.stopPropagation(),i.preventDefault())},e.prototype.destroy=function(e){var t=this;t.autoPlayClear(),t.touchObject={},t.cleanUpEvents(),i(".slick-cloned",t.$slider).detach(),t.$dots&&t.$dots.remove(),t.$prevArrow&&t.$prevArrow.length&&(t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),t.htmlExpr.test(t.options.prevArrow)&&t.$prevArrow.remove()),t.$nextArrow&&t.$nextArrow.length&&(t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),t.htmlExpr.test(t.options.nextArrow)&&t.$nextArrow.remove()),t.$slides&&(t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){i(this).attr("style",i(this).data("originalStyling"))}),t.$slideTrack.children(this.options.slide).detach(),t.$slideTrack.detach(),t.$list.detach(),t.$slider.append(t.$slides)),t.cleanUpRows(),t.$slider.removeClass("slick-slider"),t.$slider.removeClass("slick-initialized"),t.$slider.removeClass("slick-dotted"),t.unslicked=!0,e||t.$slider.trigger("destroy",[t])},e.prototype.disableTransition=function(i){var e=this,t={};t[e.transitionType]="",e.options.fade===!1?e.$slideTrack.css(t):e.$slides.eq(i).css(t)},e.prototype.fadeSlide=function(i,e){var t=this;t.cssTransitions===!1?(t.$slides.eq(i).css({zIndex:t.options.zIndex}),t.$slides.eq(i).animate({opacity:1},t.options.speed,t.options.easing,e)):(t.applyTransition(i),t.$slides.eq(i).css({opacity:1,zIndex:t.options.zIndex}),e&&setTimeout(function(){t.disableTransition(i),e.call()},t.options.speed))},e.prototype.fadeSlideOut=function(i){var e=this;e.cssTransitions===!1?e.$slides.eq(i).animate({opacity:0,zIndex:e.options.zIndex-2},e.options.speed,e.options.easing):(e.applyTransition(i),e.$slides.eq(i).css({opacity:0,zIndex:e.options.zIndex-2}))},e.prototype.filterSlides=e.prototype.slickFilter=function(i){var e=this;null!==i&&(e.$slidesCache=e.$slides,e.unload(),e.$slideTrack.children(this.options.slide).detach(),e.$slidesCache.filter(i).appendTo(e.$slideTrack),e.reinit())},e.prototype.focusHandler=function(){var e=this;e.$slider.off("focus.slick blur.slick").on("focus.slick","*",function(t){var o=i(this);setTimeout(function(){e.options.pauseOnFocus&&o.is(":focus")&&(e.focussed=!0,e.autoPlay())},0)}).on("blur.slick","*",function(t){i(this);e.options.pauseOnFocus&&(e.focussed=!1,e.autoPlay())})},e.prototype.getCurrent=e.prototype.slickCurrentSlide=function(){var i=this;return i.currentSlide},e.prototype.getDotCount=function(){var i=this,e=0,t=0,o=0;if(i.options.infinite===!0)if(i.slideCount<=i.options.slidesToShow)++o;else for(;e<i.slideCount;)++o,e=t+i.options.slidesToScroll,t+=i.options.slidesToScroll<=i.options.slidesToShow?i.options.slidesToScroll:i.options.slidesToShow;else if(i.options.centerMode===!0)o=i.slideCount;else if(i.options.asNavFor)for(;e<i.slideCount;)++o,e=t+i.options.slidesToScroll,t+=i.options.slidesToScroll<=i.options.slidesToShow?i.options.slidesToScroll:i.options.slidesToShow;else o=1+Math.ceil((i.slideCount-i.options.slidesToShow)/i.options.slidesToScroll);return o-1},e.prototype.getLeft=function(i){var e,t,o,s,n=this,r=0;return n.slideOffset=0,t=n.$slides.first().outerHeight(!0),n.options.infinite===!0?(n.slideCount>n.options.slidesToShow&&(n.slideOffset=n.slideWidth*n.options.slidesToShow*-1,s=-1,n.options.vertical===!0&&n.options.centerMode===!0&&(2===n.options.slidesToShow?s=-1.5:1===n.options.slidesToShow&&(s=-2)),r=t*n.options.slidesToShow*s),n.slideCount%n.options.slidesToScroll!==0&&i+n.options.slidesToScroll>n.slideCount&&n.slideCount>n.options.slidesToShow&&(i>n.slideCount?(n.slideOffset=(n.options.slidesToShow-(i-n.slideCount))*n.slideWidth*-1,r=(n.options.slidesToShow-(i-n.slideCount))*t*-1):(n.slideOffset=n.slideCount%n.options.slidesToScroll*n.slideWidth*-1,r=n.slideCount%n.options.slidesToScroll*t*-1))):i+n.options.slidesToShow>n.slideCount&&(n.slideOffset=(i+n.options.slidesToShow-n.slideCount)*n.slideWidth,r=(i+n.options.slidesToShow-n.slideCount)*t),n.slideCount<=n.options.slidesToShow&&(n.slideOffset=0,r=0),n.options.centerMode===!0&&n.slideCount<=n.options.slidesToShow?n.slideOffset=n.slideWidth*Math.floor(n.options.slidesToShow)/2-n.slideWidth*n.slideCount/2:n.options.centerMode===!0&&n.options.infinite===!0?n.slideOffset+=n.slideWidth*Math.floor(n.options.slidesToShow/2)-n.slideWidth:n.options.centerMode===!0&&(n.slideOffset=0,n.slideOffset+=n.slideWidth*Math.floor(n.options.slidesToShow/2)),e=n.options.vertical===!1?i*n.slideWidth*-1+n.slideOffset:i*t*-1+r,n.options.variableWidth===!0&&(o=n.slideCount<=n.options.slidesToShow||n.options.infinite===!1?n.$slideTrack.children(".slick-slide").eq(i):n.$slideTrack.children(".slick-slide").eq(i+n.options.slidesToShow),e=n.options.rtl===!0?o[0]?(n.$slideTrack.width()-o[0].offsetLeft-o.width())*-1:0:o[0]?o[0].offsetLeft*-1:0,n.options.centerMode===!0&&(o=n.slideCount<=n.options.slidesToShow||n.options.infinite===!1?n.$slideTrack.children(".slick-slide").eq(i):n.$slideTrack.children(".slick-slide").eq(i+n.options.slidesToShow+1),e=n.options.rtl===!0?o[0]?(n.$slideTrack.width()-o[0].offsetLeft-o.width())*-1:0:o[0]?o[0].offsetLeft*-1:0,e+=(n.$list.width()-o.outerWidth())/2)),e},e.prototype.getOption=e.prototype.slickGetOption=function(i){var e=this;return e.options[i]},e.prototype.getNavigableIndexes=function(){var i,e=this,t=0,o=0,s=[];for(e.options.infinite===!1?i=e.slideCount:(t=e.options.slidesToScroll*-1,o=e.options.slidesToScroll*-1,i=2*e.slideCount);t<i;)s.push(t),t=o+e.options.slidesToScroll,o+=e.options.slidesToScroll<=e.options.slidesToShow?e.options.slidesToScroll:e.options.slidesToShow;return s},e.prototype.getSlick=function(){return this},e.prototype.getSlideCount=function(){var e,t,o,s,n=this;return s=n.options.centerMode===!0?Math.floor(n.$list.width()/2):0,o=n.swipeLeft*-1+s,n.options.swipeToSlide===!0?(n.$slideTrack.find(".slick-slide").each(function(e,s){var r,l,d;if(r=i(s).outerWidth(),l=s.offsetLeft,n.options.centerMode!==!0&&(l+=r/2),d=l+r,o<d)return t=s,!1}),e=Math.abs(i(t).attr("data-slick-index")-n.currentSlide)||1):n.options.slidesToScroll},e.prototype.goTo=e.prototype.slickGoTo=function(i,e){var t=this;t.changeSlide({data:{message:"index",index:parseInt(i)}},e)},e.prototype.init=function(e){var t=this;i(t.$slider).hasClass("slick-initialized")||(i(t.$slider).addClass("slick-initialized"),t.buildRows(),t.buildOut(),t.setProps(),t.startLoad(),t.loadSlider(),t.initializeEvents(),t.updateArrows(),t.updateDots(),t.checkResponsive(!0),t.focusHandler()),e&&t.$slider.trigger("init",[t]),t.options.accessibility===!0&&t.initADA(),t.options.autoplay&&(t.paused=!1,t.autoPlay())},e.prototype.initADA=function(){var e=this,t=Math.ceil(e.slideCount/e.options.slidesToShow),o=e.getNavigableIndexes().filter(function(i){return i>=0&&i<e.slideCount});e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),null!==e.$dots&&(e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function(t){var s=o.indexOf(t);if(i(this).attr({role:"tabpanel",id:"slick-slide"+e.instanceUid+t,tabindex:-1}),s!==-1){var n="slick-slide-control"+e.instanceUid+s;i("#"+n).length&&i(this).attr({"aria-describedby":n})}}),e.$dots.attr("role","tablist").find("li").each(function(s){var n=o[s];i(this).attr({role:"presentation"}),i(this).find("button").first().attr({role:"tab",id:"slick-slide-control"+e.instanceUid+s,"aria-controls":"slick-slide"+e.instanceUid+n,"aria-label":s+1+" of "+t,"aria-selected":null,tabindex:"-1"})}).eq(e.currentSlide).find("button").attr({"aria-selected":"true",tabindex:"0"}).end());for(var s=e.currentSlide,n=s+e.options.slidesToShow;s<n;s++)e.options.focusOnChange?e.$slides.eq(s).attr({tabindex:"0"}):e.$slides.eq(s).removeAttr("tabindex");e.activateADA()},e.prototype.initArrowEvents=function(){var i=this;i.options.arrows===!0&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow.off("click.slick").on("click.slick",{message:"previous"},i.changeSlide),i.$nextArrow.off("click.slick").on("click.slick",{message:"next"},i.changeSlide),i.options.accessibility===!0&&(i.$prevArrow.on("keydown.slick",i.keyHandler),i.$nextArrow.on("keydown.slick",i.keyHandler)))},e.prototype.initDotEvents=function(){var e=this;e.options.dots===!0&&e.slideCount>e.options.slidesToShow&&(i("li",e.$dots).on("click.slick",{message:"index"},e.changeSlide),e.options.accessibility===!0&&e.$dots.on("keydown.slick",e.keyHandler)),e.options.dots===!0&&e.options.pauseOnDotsHover===!0&&e.slideCount>e.options.slidesToShow&&i("li",e.$dots).on("mouseenter.slick",i.proxy(e.interrupt,e,!0)).on("mouseleave.slick",i.proxy(e.interrupt,e,!1))},e.prototype.initSlideEvents=function(){var e=this;e.options.pauseOnHover&&(e.$list.on("mouseenter.slick",i.proxy(e.interrupt,e,!0)),e.$list.on("mouseleave.slick",i.proxy(e.interrupt,e,!1)))},e.prototype.initializeEvents=function(){var e=this;e.initArrowEvents(),e.initDotEvents(),e.initSlideEvents(),e.$list.on("touchstart.slick mousedown.slick",{action:"start"},e.swipeHandler),e.$list.on("touchmove.slick mousemove.slick",{action:"move"},e.swipeHandler),e.$list.on("touchend.slick mouseup.slick",{action:"end"},e.swipeHandler),e.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},e.swipeHandler),e.$list.on("click.slick",e.clickHandler),i(document).on(e.visibilityChange,i.proxy(e.visibility,e)),e.options.accessibility===!0&&e.$list.on("keydown.slick",e.keyHandler),e.options.focusOnSelect===!0&&i(e.$slideTrack).children().on("click.slick",e.selectHandler),i(window).on("orientationchange.slick.slick-"+e.instanceUid,i.proxy(e.orientationChange,e)),i(window).on("resize.slick.slick-"+e.instanceUid,i.proxy(e.resize,e)),i("[draggable!=true]",e.$slideTrack).on("dragstart",e.preventDefault),i(window).on("load.slick.slick-"+e.instanceUid,e.setPosition),i(e.setPosition)},e.prototype.initUI=function(){var i=this;i.options.arrows===!0&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow.show(),i.$nextArrow.show()),i.options.dots===!0&&i.slideCount>i.options.slidesToShow&&i.$dots.show()},e.prototype.keyHandler=function(i){var e=this;i.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===i.keyCode&&e.options.accessibility===!0?e.changeSlide({data:{message:e.options.rtl===!0?"next":"previous"}}):39===i.keyCode&&e.options.accessibility===!0&&e.changeSlide({data:{message:e.options.rtl===!0?"previous":"next"}}))},e.prototype.lazyLoad=function(){function e(e){i("img[data-lazy]",e).each(function(){var e=i(this),t=i(this).attr("data-lazy"),o=i(this).attr("data-srcset"),s=i(this).attr("data-sizes")||r.$slider.attr("data-sizes"),n=document.createElement("img");n.onload=function(){e.animate({opacity:0},100,function(){o&&(e.attr("srcset",o),s&&e.attr("sizes",s)),e.attr("src",t).animate({opacity:1},200,function(){e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")}),r.$slider.trigger("lazyLoaded",[r,e,t])})},n.onerror=function(){e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),r.$slider.trigger("lazyLoadError",[r,e,t])},n.src=t})}var t,o,s,n,r=this;if(r.options.centerMode===!0?r.options.infinite===!0?(s=r.currentSlide+(r.options.slidesToShow/2+1),n=s+r.options.slidesToShow+2):(s=Math.max(0,r.currentSlide-(r.options.slidesToShow/2+1)),n=2+(r.options.slidesToShow/2+1)+r.currentSlide):(s=r.options.infinite?r.options.slidesToShow+r.currentSlide:r.currentSlide,n=Math.ceil(s+r.options.slidesToShow),r.options.fade===!0&&(s>0&&s--,n<=r.slideCount&&n++)),t=r.$slider.find(".slick-slide").slice(s,n),"anticipated"===r.options.lazyLoad)for(var l=s-1,d=n,a=r.$slider.find(".slick-slide"),c=0;c<r.options.slidesToScroll;c++)l<0&&(l=r.slideCount-1),t=t.add(a.eq(l)),t=t.add(a.eq(d)),l--,d++;e(t),r.slideCount<=r.options.slidesToShow?(o=r.$slider.find(".slick-slide"),e(o)):r.currentSlide>=r.slideCount-r.options.slidesToShow?(o=r.$slider.find(".slick-cloned").slice(0,r.options.slidesToShow),e(o)):0===r.currentSlide&&(o=r.$slider.find(".slick-cloned").slice(r.options.slidesToShow*-1),e(o))},e.prototype.loadSlider=function(){var i=this;i.setPosition(),i.$slideTrack.css({opacity:1}),i.$slider.removeClass("slick-loading"),i.initUI(),"progressive"===i.options.lazyLoad&&i.progressiveLazyLoad()},e.prototype.next=e.prototype.slickNext=function(){var i=this;i.changeSlide({data:{message:"next"}})},e.prototype.orientationChange=function(){var i=this;i.checkResponsive(),i.setPosition()},e.prototype.pause=e.prototype.slickPause=function(){var i=this;i.autoPlayClear(),i.paused=!0},e.prototype.play=e.prototype.slickPlay=function(){var i=this;i.autoPlay(),i.options.autoplay=!0,i.paused=!1,i.focussed=!1,i.interrupted=!1},e.prototype.postSlide=function(e){var t=this;if(!t.unslicked&&(t.$slider.trigger("afterChange",[t,e]),t.animating=!1,t.slideCount>t.options.slidesToShow&&t.setPosition(),t.swipeLeft=null,t.options.autoplay&&t.autoPlay(),t.options.accessibility===!0&&(t.initADA(),t.options.focusOnChange))){var o=i(t.$slides.get(t.currentSlide));o.attr("tabindex",0).focus()}},e.prototype.prev=e.prototype.slickPrev=function(){var i=this;i.changeSlide({data:{message:"previous"}})},e.prototype.preventDefault=function(i){i.preventDefault()},e.prototype.progressiveLazyLoad=function(e){e=e||1;var t,o,s,n,r,l=this,d=i("img[data-lazy]",l.$slider);d.length?(t=d.first(),o=t.attr("data-lazy"),s=t.attr("data-srcset"),n=t.attr("data-sizes")||l.$slider.attr("data-sizes"),r=document.createElement("img"),r.onload=function(){s&&(t.attr("srcset",s),n&&t.attr("sizes",n)),t.attr("src",o).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"),l.options.adaptiveHeight===!0&&l.setPosition(),l.$slider.trigger("lazyLoaded",[l,t,o]),l.progressiveLazyLoad()},r.onerror=function(){e<3?setTimeout(function(){l.progressiveLazyLoad(e+1)},500):(t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),l.$slider.trigger("lazyLoadError",[l,t,o]),l.progressiveLazyLoad())},r.src=o):l.$slider.trigger("allImagesLoaded",[l])},e.prototype.refresh=function(e){var t,o,s=this;o=s.slideCount-s.options.slidesToShow,!s.options.infinite&&s.currentSlide>o&&(s.currentSlide=o),s.slideCount<=s.options.slidesToShow&&(s.currentSlide=0),t=s.currentSlide,s.destroy(!0),i.extend(s,s.initials,{currentSlide:t}),s.init(),e||s.changeSlide({data:{message:"index",index:t}},!1)},e.prototype.registerBreakpoints=function(){var e,t,o,s=this,n=s.options.responsive||null;if("array"===i.type(n)&&n.length){s.respondTo=s.options.respondTo||"window";for(e in n)if(o=s.breakpoints.length-1,n.hasOwnProperty(e)){for(t=n[e].breakpoint;o>=0;)s.breakpoints[o]&&s.breakpoints[o]===t&&s.breakpoints.splice(o,1),o--;s.breakpoints.push(t),s.breakpointSettings[t]=n[e].settings}s.breakpoints.sort(function(i,e){return s.options.mobileFirst?i-e:e-i})}},e.prototype.reinit=function(){var e=this;e.$slides=e.$slideTrack.children(e.options.slide).addClass("slick-slide"),e.slideCount=e.$slides.length,e.currentSlide>=e.slideCount&&0!==e.currentSlide&&(e.currentSlide=e.currentSlide-e.options.slidesToScroll),e.slideCount<=e.options.slidesToShow&&(e.currentSlide=0),e.registerBreakpoints(),e.setProps(),e.setupInfinite(),e.buildArrows(),e.updateArrows(),e.initArrowEvents(),e.buildDots(),e.updateDots(),e.initDotEvents(),e.cleanUpSlideEvents(),e.initSlideEvents(),e.checkResponsive(!1,!0),e.options.focusOnSelect===!0&&i(e.$slideTrack).children().on("click.slick",e.selectHandler),e.setSlideClasses("number"==typeof e.currentSlide?e.currentSlide:0),e.setPosition(),e.focusHandler(),e.paused=!e.options.autoplay,e.autoPlay(),e.$slider.trigger("reInit",[e])},e.prototype.resize=function(){var e=this;i(window).width()!==e.windowWidth&&(clearTimeout(e.windowDelay),e.windowDelay=window.setTimeout(function(){e.windowWidth=i(window).width(),e.checkResponsive(),e.unslicked||e.setPosition()},50))},e.prototype.removeSlide=e.prototype.slickRemove=function(i,e,t){var o=this;return"boolean"==typeof i?(e=i,i=e===!0?0:o.slideCount-1):i=e===!0?--i:i,!(o.slideCount<1||i<0||i>o.slideCount-1)&&(o.unload(),t===!0?o.$slideTrack.children().remove():o.$slideTrack.children(this.options.slide).eq(i).remove(),o.$slides=o.$slideTrack.children(this.options.slide),o.$slideTrack.children(this.options.slide).detach(),o.$slideTrack.append(o.$slides),o.$slidesCache=o.$slides,void o.reinit())},e.prototype.setCSS=function(i){var e,t,o=this,s={};o.options.rtl===!0&&(i=-i),e="left"==o.positionProp?Math.ceil(i)+"px":"0px",t="top"==o.positionProp?Math.ceil(i)+"px":"0px",s[o.positionProp]=i,o.transformsEnabled===!1?o.$slideTrack.css(s):(s={},o.cssTransitions===!1?(s[o.animType]="translate("+e+", "+t+")",o.$slideTrack.css(s)):(s[o.animType]="translate3d("+e+", "+t+", 0px)",o.$slideTrack.css(s)))},e.prototype.setDimensions=function(){var i=this;i.options.vertical===!1?i.options.centerMode===!0&&i.$list.css({padding:"0px "+i.options.centerPadding}):(i.$list.height(i.$slides.first().outerHeight(!0)*i.options.slidesToShow),i.options.centerMode===!0&&i.$list.css({padding:i.options.centerPadding+" 0px"})),i.listWidth=i.$list.width(),i.listHeight=i.$list.height(),i.options.vertical===!1&&i.options.variableWidth===!1?(i.slideWidth=Math.ceil(i.listWidth/i.options.slidesToShow),i.$slideTrack.width(Math.ceil(i.slideWidth*i.$slideTrack.children(".slick-slide").length))):i.options.variableWidth===!0?i.$slideTrack.width(5e3*i.slideCount):(i.slideWidth=Math.ceil(i.listWidth),i.$slideTrack.height(Math.ceil(i.$slides.first().outerHeight(!0)*i.$slideTrack.children(".slick-slide").length)));var e=i.$slides.first().outerWidth(!0)-i.$slides.first().width();i.options.variableWidth===!1&&i.$slideTrack.children(".slick-slide").width(i.slideWidth-e)},e.prototype.setFade=function(){var e,t=this;t.$slides.each(function(o,s){e=t.slideWidth*o*-1,t.options.rtl===!0?i(s).css({position:"relative",right:e,top:0,zIndex:t.options.zIndex-2,opacity:0}):i(s).css({position:"relative",left:e,top:0,zIndex:t.options.zIndex-2,opacity:0})}),t.$slides.eq(t.currentSlide).css({zIndex:t.options.zIndex-1,opacity:1})},e.prototype.setHeight=function(){var i=this;if(1===i.options.slidesToShow&&i.options.adaptiveHeight===!0&&i.options.vertical===!1){var e=i.$slides.eq(i.currentSlide).outerHeight(!0);i.$list.css("height",e)}},e.prototype.setOption=e.prototype.slickSetOption=function(){var e,t,o,s,n,r=this,l=!1;if("object"===i.type(arguments[0])?(o=arguments[0],l=arguments[1],n="multiple"):"string"===i.type(arguments[0])&&(o=arguments[0],s=arguments[1],l=arguments[2],"responsive"===arguments[0]&&"array"===i.type(arguments[1])?n="responsive":"undefined"!=typeof arguments[1]&&(n="single")),"single"===n)r.options[o]=s;else if("multiple"===n)i.each(o,function(i,e){r.options[i]=e});else if("responsive"===n)for(t in s)if("array"!==i.type(r.options.responsive))r.options.responsive=[s[t]];else{for(e=r.options.responsive.length-1;e>=0;)r.options.responsive[e].breakpoint===s[t].breakpoint&&r.options.responsive.splice(e,1),e--;r.options.responsive.push(s[t])}l&&(r.unload(),r.reinit())},e.prototype.setPosition=function(){var i=this;i.setDimensions(),i.setHeight(),i.options.fade===!1?i.setCSS(i.getLeft(i.currentSlide)):i.setFade(),i.$slider.trigger("setPosition",[i])},e.prototype.setProps=function(){var i=this,e=document.body.style;i.positionProp=i.options.vertical===!0?"top":"left",
"top"===i.positionProp?i.$slider.addClass("slick-vertical"):i.$slider.removeClass("slick-vertical"),void 0===e.WebkitTransition&&void 0===e.MozTransition&&void 0===e.msTransition||i.options.useCSS===!0&&(i.cssTransitions=!0),i.options.fade&&("number"==typeof i.options.zIndex?i.options.zIndex<3&&(i.options.zIndex=3):i.options.zIndex=i.defaults.zIndex),void 0!==e.OTransform&&(i.animType="OTransform",i.transformType="-o-transform",i.transitionType="OTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(i.animType=!1)),void 0!==e.MozTransform&&(i.animType="MozTransform",i.transformType="-moz-transform",i.transitionType="MozTransition",void 0===e.perspectiveProperty&&void 0===e.MozPerspective&&(i.animType=!1)),void 0!==e.webkitTransform&&(i.animType="webkitTransform",i.transformType="-webkit-transform",i.transitionType="webkitTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(i.animType=!1)),void 0!==e.msTransform&&(i.animType="msTransform",i.transformType="-ms-transform",i.transitionType="msTransition",void 0===e.msTransform&&(i.animType=!1)),void 0!==e.transform&&i.animType!==!1&&(i.animType="transform",i.transformType="transform",i.transitionType="transition"),i.transformsEnabled=i.options.useTransform&&null!==i.animType&&i.animType!==!1},e.prototype.setSlideClasses=function(i){var e,t,o,s,n=this;if(t=n.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),n.$slides.eq(i).addClass("slick-current"),n.options.centerMode===!0){var r=n.options.slidesToShow%2===0?1:0;e=Math.floor(n.options.slidesToShow/2),n.options.infinite===!0&&(i>=e&&i<=n.slideCount-1-e?n.$slides.slice(i-e+r,i+e+1).addClass("slick-active").attr("aria-hidden","false"):(o=n.options.slidesToShow+i,t.slice(o-e+1+r,o+e+2).addClass("slick-active").attr("aria-hidden","false")),0===i?t.eq(t.length-1-n.options.slidesToShow).addClass("slick-center"):i===n.slideCount-1&&t.eq(n.options.slidesToShow).addClass("slick-center")),n.$slides.eq(i).addClass("slick-center")}else i>=0&&i<=n.slideCount-n.options.slidesToShow?n.$slides.slice(i,i+n.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):t.length<=n.options.slidesToShow?t.addClass("slick-active").attr("aria-hidden","false"):(s=n.slideCount%n.options.slidesToShow,o=n.options.infinite===!0?n.options.slidesToShow+i:i,n.options.slidesToShow==n.options.slidesToScroll&&n.slideCount-i<n.options.slidesToShow?t.slice(o-(n.options.slidesToShow-s),o+s).addClass("slick-active").attr("aria-hidden","false"):t.slice(o,o+n.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"));"ondemand"!==n.options.lazyLoad&&"anticipated"!==n.options.lazyLoad||n.lazyLoad()},e.prototype.setupInfinite=function(){var e,t,o,s=this;if(s.options.fade===!0&&(s.options.centerMode=!1),s.options.infinite===!0&&s.options.fade===!1&&(t=null,s.slideCount>s.options.slidesToShow)){for(o=s.options.centerMode===!0?s.options.slidesToShow+1:s.options.slidesToShow,e=s.slideCount;e>s.slideCount-o;e-=1)t=e-1,i(s.$slides[t]).clone(!0).attr("id","").attr("data-slick-index",t-s.slideCount).prependTo(s.$slideTrack).addClass("slick-cloned");for(e=0;e<o+s.slideCount;e+=1)t=e,i(s.$slides[t]).clone(!0).attr("id","").attr("data-slick-index",t+s.slideCount).appendTo(s.$slideTrack).addClass("slick-cloned");s.$slideTrack.find(".slick-cloned").find("[id]").each(function(){i(this).attr("id","")})}},e.prototype.interrupt=function(i){var e=this;i||e.autoPlay(),e.interrupted=i},e.prototype.selectHandler=function(e){var t=this,o=i(e.target).is(".slick-slide")?i(e.target):i(e.target).parents(".slick-slide"),s=parseInt(o.attr("data-slick-index"));return s||(s=0),t.slideCount<=t.options.slidesToShow?void t.slideHandler(s,!1,!0):void t.slideHandler(s)},e.prototype.slideHandler=function(i,e,t){var o,s,n,r,l,d=null,a=this;if(e=e||!1,!(a.animating===!0&&a.options.waitForAnimate===!0||a.options.fade===!0&&a.currentSlide===i))return e===!1&&a.asNavFor(i),o=i,d=a.getLeft(o),r=a.getLeft(a.currentSlide),a.currentLeft=null===a.swipeLeft?r:a.swipeLeft,a.options.infinite===!1&&a.options.centerMode===!1&&(i<0||i>a.getDotCount()*a.options.slidesToScroll)?void(a.options.fade===!1&&(o=a.currentSlide,t!==!0&&a.slideCount>a.options.slidesToShow?a.animateSlide(r,function(){a.postSlide(o)}):a.postSlide(o))):a.options.infinite===!1&&a.options.centerMode===!0&&(i<0||i>a.slideCount-a.options.slidesToScroll)?void(a.options.fade===!1&&(o=a.currentSlide,t!==!0&&a.slideCount>a.options.slidesToShow?a.animateSlide(r,function(){a.postSlide(o)}):a.postSlide(o))):(a.options.autoplay&&clearInterval(a.autoPlayTimer),s=o<0?a.slideCount%a.options.slidesToScroll!==0?a.slideCount-a.slideCount%a.options.slidesToScroll:a.slideCount+o:o>=a.slideCount?a.slideCount%a.options.slidesToScroll!==0?0:o-a.slideCount:o,a.animating=!0,a.$slider.trigger("beforeChange",[a,a.currentSlide,s]),n=a.currentSlide,a.currentSlide=s,a.setSlideClasses(a.currentSlide),a.options.asNavFor&&(l=a.getNavTarget(),l=l.slick("getSlick"),l.slideCount<=l.options.slidesToShow&&l.setSlideClasses(a.currentSlide)),a.updateDots(),a.updateArrows(),a.options.fade===!0?(t!==!0?(a.fadeSlideOut(n),a.fadeSlide(s,function(){a.postSlide(s)})):a.postSlide(s),void a.animateHeight()):void(t!==!0&&a.slideCount>a.options.slidesToShow?a.animateSlide(d,function(){a.postSlide(s)}):a.postSlide(s)))},e.prototype.startLoad=function(){var i=this;i.options.arrows===!0&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow.hide(),i.$nextArrow.hide()),i.options.dots===!0&&i.slideCount>i.options.slidesToShow&&i.$dots.hide(),i.$slider.addClass("slick-loading")},e.prototype.swipeDirection=function(){var i,e,t,o,s=this;return i=s.touchObject.startX-s.touchObject.curX,e=s.touchObject.startY-s.touchObject.curY,t=Math.atan2(e,i),o=Math.round(180*t/Math.PI),o<0&&(o=360-Math.abs(o)),o<=45&&o>=0?s.options.rtl===!1?"left":"right":o<=360&&o>=315?s.options.rtl===!1?"left":"right":o>=135&&o<=225?s.options.rtl===!1?"right":"left":s.options.verticalSwiping===!0?o>=35&&o<=135?"down":"up":"vertical"},e.prototype.swipeEnd=function(i){var e,t,o=this;if(o.dragging=!1,o.swiping=!1,o.scrolling)return o.scrolling=!1,!1;if(o.interrupted=!1,o.shouldClick=!(o.touchObject.swipeLength>10),void 0===o.touchObject.curX)return!1;if(o.touchObject.edgeHit===!0&&o.$slider.trigger("edge",[o,o.swipeDirection()]),o.touchObject.swipeLength>=o.touchObject.minSwipe){switch(t=o.swipeDirection()){case"left":case"down":e=o.options.swipeToSlide?o.checkNavigable(o.currentSlide+o.getSlideCount()):o.currentSlide+o.getSlideCount(),o.currentDirection=0;break;case"right":case"up":e=o.options.swipeToSlide?o.checkNavigable(o.currentSlide-o.getSlideCount()):o.currentSlide-o.getSlideCount(),o.currentDirection=1}"vertical"!=t&&(o.slideHandler(e),o.touchObject={},o.$slider.trigger("swipe",[o,t]))}else o.touchObject.startX!==o.touchObject.curX&&(o.slideHandler(o.currentSlide),o.touchObject={})},e.prototype.swipeHandler=function(i){var e=this;if(!(e.options.swipe===!1||"ontouchend"in document&&e.options.swipe===!1||e.options.draggable===!1&&i.type.indexOf("mouse")!==-1))switch(e.touchObject.fingerCount=i.originalEvent&&void 0!==i.originalEvent.touches?i.originalEvent.touches.length:1,e.touchObject.minSwipe=e.listWidth/e.options.touchThreshold,e.options.verticalSwiping===!0&&(e.touchObject.minSwipe=e.listHeight/e.options.touchThreshold),i.data.action){case"start":e.swipeStart(i);break;case"move":e.swipeMove(i);break;case"end":e.swipeEnd(i)}},e.prototype.swipeMove=function(i){var e,t,o,s,n,r,l=this;return n=void 0!==i.originalEvent?i.originalEvent.touches:null,!(!l.dragging||l.scrolling||n&&1!==n.length)&&(e=l.getLeft(l.currentSlide),l.touchObject.curX=void 0!==n?n[0].pageX:i.clientX,l.touchObject.curY=void 0!==n?n[0].pageY:i.clientY,l.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(l.touchObject.curX-l.touchObject.startX,2))),r=Math.round(Math.sqrt(Math.pow(l.touchObject.curY-l.touchObject.startY,2))),!l.options.verticalSwiping&&!l.swiping&&r>4?(l.scrolling=!0,!1):(l.options.verticalSwiping===!0&&(l.touchObject.swipeLength=r),t=l.swipeDirection(),void 0!==i.originalEvent&&l.touchObject.swipeLength>4&&(l.swiping=!0,i.preventDefault()),s=(l.options.rtl===!1?1:-1)*(l.touchObject.curX>l.touchObject.startX?1:-1),l.options.verticalSwiping===!0&&(s=l.touchObject.curY>l.touchObject.startY?1:-1),o=l.touchObject.swipeLength,l.touchObject.edgeHit=!1,l.options.infinite===!1&&(0===l.currentSlide&&"right"===t||l.currentSlide>=l.getDotCount()&&"left"===t)&&(o=l.touchObject.swipeLength*l.options.edgeFriction,l.touchObject.edgeHit=!0),l.options.vertical===!1?l.swipeLeft=e+o*s:l.swipeLeft=e+o*(l.$list.height()/l.listWidth)*s,l.options.verticalSwiping===!0&&(l.swipeLeft=e+o*s),l.options.fade!==!0&&l.options.touchMove!==!1&&(l.animating===!0?(l.swipeLeft=null,!1):void l.setCSS(l.swipeLeft))))},e.prototype.swipeStart=function(i){var e,t=this;return t.interrupted=!0,1!==t.touchObject.fingerCount||t.slideCount<=t.options.slidesToShow?(t.touchObject={},!1):(void 0!==i.originalEvent&&void 0!==i.originalEvent.touches&&(e=i.originalEvent.touches[0]),t.touchObject.startX=t.touchObject.curX=void 0!==e?e.pageX:i.clientX,t.touchObject.startY=t.touchObject.curY=void 0!==e?e.pageY:i.clientY,void(t.dragging=!0))},e.prototype.unfilterSlides=e.prototype.slickUnfilter=function(){var i=this;null!==i.$slidesCache&&(i.unload(),i.$slideTrack.children(this.options.slide).detach(),i.$slidesCache.appendTo(i.$slideTrack),i.reinit())},e.prototype.unload=function(){var e=this;i(".slick-cloned",e.$slider).remove(),e.$dots&&e.$dots.remove(),e.$prevArrow&&e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.remove(),e.$nextArrow&&e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.remove(),e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")},e.prototype.unslick=function(i){var e=this;e.$slider.trigger("unslick",[e,i]),e.destroy()},e.prototype.updateArrows=function(){var i,e=this;i=Math.floor(e.options.slidesToShow/2),e.options.arrows===!0&&e.slideCount>e.options.slidesToShow&&!e.options.infinite&&(e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===e.currentSlide?(e.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):e.currentSlide>=e.slideCount-e.options.slidesToShow&&e.options.centerMode===!1?(e.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):e.currentSlide>=e.slideCount-1&&e.options.centerMode===!0&&(e.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))},e.prototype.updateDots=function(){var i=this;null!==i.$dots&&(i.$dots.find("li").removeClass("slick-active").end(),i.$dots.find("li").eq(Math.floor(i.currentSlide/i.options.slidesToScroll)).addClass("slick-active"))},e.prototype.visibility=function(){var i=this;i.options.autoplay&&(document[i.hidden]?i.interrupted=!0:i.interrupted=!1)},i.fn.slick=function(){var i,t,o=this,s=arguments[0],n=Array.prototype.slice.call(arguments,1),r=o.length;for(i=0;i<r;i++)if("object"==typeof s||"undefined"==typeof s?o[i].slick=new e(o[i],s):t=o[i].slick[s].apply(o[i].slick,n),"undefined"!=typeof t)return t;return o}});

// BLAZY
(function(q,m){"function"===typeof define&&define.amd?define(m):"object"===typeof exports?module.exports=m():q.Blazy=m()})(this,function(){function q(b){var c=b._util;c.elements=E(b.options);c.count=c.elements.length;c.destroyed&&(c.destroyed=!1,b.options.container&&l(b.options.container,function(a){n(a,"scroll",c.validateT)}),n(window,"resize",c.saveViewportOffsetT),n(window,"resize",c.validateT),n(window,"scroll",c.validateT));m(b)}function m(b){for(var c=b._util,a=0;a<c.count;a++){var d=c.elements[a],e;a:{var g=d;e=b.options;var p=g.getBoundingClientRect();if(e.container&&y&&(g=g.closest(e.containerClass))){g=g.getBoundingClientRect();e=r(g,f)?r(p,{top:g.top-e.offset,right:g.right+e.offset,bottom:g.bottom+e.offset,left:g.left-e.offset}):!1;break a}e=r(p,f)}if(e||t(d,b.options.successClass))b.load(d),c.elements.splice(a,1),c.count--,a--}0===c.count&&b.destroy()}function r(b,c){return b.right>=c.left&&b.bottom>=c.top&&b.left<=c.right&&b.top<=c.bottom}function z(b,c,a){if(!t(b,a.successClass)&&(c||a.loadInvisible||0<b.offsetWidth&&0<b.offsetHeight))if(c=b.getAttribute(u)||b.getAttribute(a.src)){c=c.split(a.separator);var d=c[A&&1<c.length?1:0],e=b.getAttribute(a.srcset),g="img"===b.nodeName.toLowerCase(),p=(c=b.parentNode)&&"picture"===c.nodeName.toLowerCase();if(g||void 0===b.src){var h=new Image,w=function(){a.error&&a.error(b,"invalid");v(b,a.errorClass);k(h,"error",w);k(h,"load",f)},f=function(){g?p||B(b,d,e):b.style.backgroundImage='url("'+d+'")';x(b,a);k(h,"load",f);k(h,"error",w)};p&&(h=b,l(c.getElementsByTagName("source"),function(b){var c=a.srcset,e=b.getAttribute(c);e&&(b.setAttribute("srcset",e),b.removeAttribute(c))}));n(h,"error",w);n(h,"load",f);B(h,d,e)}else b.src=d,x(b,a)}else"video"===b.nodeName.toLowerCase()?(l(b.getElementsByTagName("source"),function(b){var c=a.src,e=b.getAttribute(c);e&&(b.setAttribute("src",e),b.removeAttribute(c))}),b.load(),x(b,a)):(a.error&&a.error(b,"missing"),v(b,a.errorClass))}function x(b,c){v(b,c.successClass);c.success&&c.success(b);b.removeAttribute(c.src);b.removeAttribute(c.srcset);l(c.breakpoints,function(a){b.removeAttribute(a.src)})}function B(b,c,a){a&&b.setAttribute("srcset",a);b.src=c}function t(b,c){return-1!==(" "+b.className+" ").indexOf(" "+c+" ")}function v(b,c){t(b,c)||(b.className+=" "+c)}function E(b){var c=[];b=b.root.querySelectorAll(b.selector);for(var a=b.length;a--;c.unshift(b[a]));return c}function C(b){f.bottom=(window.innerHeight||document.documentElement.clientHeight)+b;f.right=(window.innerWidth||document.documentElement.clientWidth)+b}function n(b,c,a){b.attachEvent?b.attachEvent&&b.attachEvent("on"+c,a):b.addEventListener(c,a,{capture:!1,passive:!0})}function k(b,c,a){b.detachEvent?b.detachEvent&&b.detachEvent("on"+c,a):b.removeEventListener(c,a,{capture:!1,passive:!0})}function l(b,c){if(b&&c)for(var a=b.length,d=0;d<a&&!1!==c(b[d],d);d++);}function D(b,c,a){var d=0;return function(){var e=+new Date;e-d<c||(d=e,b.apply(a,arguments))}}var u,f,A,y;return function(b){if(!document.querySelectorAll){var c=document.createStyleSheet();document.querySelectorAll=function(a,b,d,h,f){f=document.all;b=[];a=a.replace(/\[for\b/gi,"[htmlFor").split(",");for(d=a.length;d--;){c.addRule(a[d],"k:v");for(h=f.length;h--;)f[h].currentStyle.k&&b.push(f[h]);c.removeRule(0)}return b}}var a=this,d=a._util={};d.elements=[];d.destroyed=!0;a.options=b||{};a.options.error=a.options.error||!1;a.options.offset=a.options.offset||100;a.options.root=a.options.root||document;a.options.success=a.options.success||!1;a.options.selector=a.options.selector||".b-lazy";a.options.separator=a.options.separator||"|";a.options.containerClass=a.options.container;a.options.container=a.options.containerClass?document.querySelectorAll(a.options.containerClass):!1;a.options.errorClass=a.options.errorClass||"b-error";a.options.breakpoints=a.options.breakpoints||!1;a.options.loadInvisible=a.options.loadInvisible||!1;a.options.successClass=a.options.successClass||"b-loaded";a.options.validateDelay=a.options.validateDelay||25;a.options.saveViewportOffsetDelay=a.options.saveViewportOffsetDelay||50;a.options.srcset=a.options.srcset||"data-srcset";a.options.src=u=a.options.src||"data-src";y=Element.prototype.closest;A=1<window.devicePixelRatio;f={};f.top=0-a.options.offset;f.left=0-a.options.offset;a.revalidate=function(){q(a)};a.load=function(a,b){var c=this.options;void 0===a.length?z(a,b,c):l(a,function(a){z(a,b,c)})};a.destroy=function(){var a=this._util;this.options.container&&l(this.options.container,function(b){k(b,"scroll",a.validateT)});k(window,"scroll",a.validateT);k(window,"resize",a.validateT);k(window,"resize",a.saveViewportOffsetT);a.count=0;a.elements.length=0;a.destroyed=!0};d.validateT=D(function(){m(a)},a.options.validateDelay,a);d.saveViewportOffsetT=D(function(){C(a.options.offset)},a.options.saveViewportOffsetDelay,a);C(a.options.offset);l(a.options.breakpoints,function(a){if(a.width>=window.screen.width)return u=a.src,!1});setTimeout(function(){q(a)})}});

//PACE
/*! pace 1.0.2 */
(function(){var a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X=[].slice,Y={}.hasOwnProperty,Z=function(a,b){function c(){this.constructor=a}for(var d in b)Y.call(b,d)&&(a[d]=b[d]);return c.prototype=b.prototype,a.prototype=new c,a.__super__=b.prototype,a},$=[].indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(b in this&&this[b]===a)return b;return-1};for(u={catchupTime:100,initialRate:.03,minTime:250,ghostTime:100,maxProgressPerFrame:20,easeFactor:1.25,startOnPageLoad:!0,restartOnPushState:!0,restartOnRequestAfter:500,target:"body",elements:{checkInterval:100,selectors:["body"]},eventLag:{minSamples:10,sampleCount:3,lagThreshold:3},ajax:{trackMethods:["GET"],trackWebSockets:!0,ignoreURLs:[]}},C=function(){var a;return null!=(a="undefined"!=typeof performance&&null!==performance&&"function"==typeof performance.now?performance.now():void 0)?a:+new Date},E=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||window.msRequestAnimationFrame,t=window.cancelAnimationFrame||window.mozCancelAnimationFrame,null==E&&(E=function(a){return setTimeout(a,50)},t=function(a){return clearTimeout(a)}),G=function(a){var b,c;return b=C(),(c=function(){var d;return d=C()-b,d>=33?(b=C(),a(d,function(){return E(c)})):setTimeout(c,33-d)})()},F=function(){var a,b,c;return c=arguments[0],b=arguments[1],a=3<=arguments.length?X.call(arguments,2):[],"function"==typeof c[b]?c[b].apply(c,a):c[b]},v=function(){var a,b,c,d,e,f,g;for(b=arguments[0],d=2<=arguments.length?X.call(arguments,1):[],f=0,g=d.length;g>f;f++)if(c=d[f])for(a in c)Y.call(c,a)&&(e=c[a],null!=b[a]&&"object"==typeof b[a]&&null!=e&&"object"==typeof e?v(b[a],e):b[a]=e);return b},q=function(a){var b,c,d,e,f;for(c=b=0,e=0,f=a.length;f>e;e++)d=a[e],c+=Math.abs(d),b++;return c/b},x=function(a,b){var c,d,e;if(null==a&&(a="options"),null==b&&(b=!0),e=document.querySelector("[data-pace-"+a+"]")){if(c=e.getAttribute("data-pace-"+a),!b)return c;try{return JSON.parse(c)}catch(f){return d=f,"undefined"!=typeof console&&null!==console?console.error("Error parsing inline pace options",d):void 0}}},g=function(){function a(){}return a.prototype.on=function(a,b,c,d){var e;return null==d&&(d=!1),null==this.bindings&&(this.bindings={}),null==(e=this.bindings)[a]&&(e[a]=[]),this.bindings[a].push({handler:b,ctx:c,once:d})},a.prototype.once=function(a,b,c){return this.on(a,b,c,!0)},a.prototype.off=function(a,b){var c,d,e;if(null!=(null!=(d=this.bindings)?d[a]:void 0)){if(null==b)return delete this.bindings[a];for(c=0,e=[];c<this.bindings[a].length;)e.push(this.bindings[a][c].handler===b?this.bindings[a].splice(c,1):c++);return e}},a.prototype.trigger=function(){var a,b,c,d,e,f,g,h,i;if(c=arguments[0],a=2<=arguments.length?X.call(arguments,1):[],null!=(g=this.bindings)?g[c]:void 0){for(e=0,i=[];e<this.bindings[c].length;)h=this.bindings[c][e],d=h.handler,b=h.ctx,f=h.once,d.apply(null!=b?b:this,a),i.push(f?this.bindings[c].splice(e,1):e++);return i}},a}(),j=window.Pace||{},window.Pace=j,v(j,g.prototype),D=j.options=v({},u,window.paceOptions,x()),U=["ajax","document","eventLag","elements"],Q=0,S=U.length;S>Q;Q++)K=U[Q],D[K]===!0&&(D[K]=u[K]);i=function(a){function b(){return V=b.__super__.constructor.apply(this,arguments)}return Z(b,a),b}(Error),b=function(){function a(){this.progress=0}return a.prototype.getElement=function(){var a;if(null==this.el){if(a=document.querySelector(D.target),!a)throw new i;this.el=document.createElement("div"),this.el.className="pace pace-active",document.body.className=document.body.className.replace(/pace-done/g,""),document.body.className+=" pace-running",this.el.innerHTML='<div class="pace-progress">\n  <div class="pace-progress-inner"></div>\n</div>\n<div class="pace-activity"></div>',null!=a.firstChild?a.insertBefore(this.el,a.firstChild):a.appendChild(this.el)}return this.el},a.prototype.finish=function(){var a;return a=this.getElement(),a.className=a.className.replace("pace-active",""),a.className+=" pace-inactive",document.body.className=document.body.className.replace("pace-running",""),document.body.className+=" pace-done"},a.prototype.update=function(a){return this.progress=a,this.render()},a.prototype.destroy=function(){try{this.getElement().parentNode.removeChild(this.getElement())}catch(a){i=a}return this.el=void 0},a.prototype.render=function(){var a,b,c,d,e,f,g;if(null==document.querySelector(D.target))return!1;for(a=this.getElement(),d="translate3d("+this.progress+"%, 0, 0)",g=["webkitTransform","msTransform","transform"],e=0,f=g.length;f>e;e++)b=g[e],a.children[0].style[b]=d;return(!this.lastRenderedProgress||this.lastRenderedProgress|0!==this.progress|0)&&(a.children[0].setAttribute("data-progress-text",""+(0|this.progress)+"%"),this.progress>=100?c="99":(c=this.progress<10?"0":"",c+=0|this.progress),a.children[0].setAttribute("data-progress",""+c)),this.lastRenderedProgress=this.progress},a.prototype.done=function(){return this.progress>=100},a}(),h=function(){function a(){this.bindings={}}return a.prototype.trigger=function(a,b){var c,d,e,f,g;if(null!=this.bindings[a]){for(f=this.bindings[a],g=[],d=0,e=f.length;e>d;d++)c=f[d],g.push(c.call(this,b));return g}},a.prototype.on=function(a,b){var c;return null==(c=this.bindings)[a]&&(c[a]=[]),this.bindings[a].push(b)},a}(),P=window.XMLHttpRequest,O=window.XDomainRequest,N=window.WebSocket,w=function(a,b){var c,d,e;e=[];for(d in b.prototype)try{e.push(null==a[d]&&"function"!=typeof b[d]?"function"==typeof Object.defineProperty?Object.defineProperty(a,d,{get:function(){return b.prototype[d]},configurable:!0,enumerable:!0}):a[d]=b.prototype[d]:void 0)}catch(f){c=f}return e},A=[],j.ignore=function(){var a,b,c;return b=arguments[0],a=2<=arguments.length?X.call(arguments,1):[],A.unshift("ignore"),c=b.apply(null,a),A.shift(),c},j.track=function(){var a,b,c;return b=arguments[0],a=2<=arguments.length?X.call(arguments,1):[],A.unshift("track"),c=b.apply(null,a),A.shift(),c},J=function(a){var b;if(null==a&&(a="GET"),"track"===A[0])return"force";if(!A.length&&D.ajax){if("socket"===a&&D.ajax.trackWebSockets)return!0;if(b=a.toUpperCase(),$.call(D.ajax.trackMethods,b)>=0)return!0}return!1},k=function(a){function b(){var a,c=this;b.__super__.constructor.apply(this,arguments),a=function(a){var b;return b=a.open,a.open=function(d,e){return J(d)&&c.trigger("request",{type:d,url:e,request:a}),b.apply(a,arguments)}},window.XMLHttpRequest=function(b){var c;return c=new P(b),a(c),c};try{w(window.XMLHttpRequest,P)}catch(d){}if(null!=O){window.XDomainRequest=function(){var b;return b=new O,a(b),b};try{w(window.XDomainRequest,O)}catch(d){}}if(null!=N&&D.ajax.trackWebSockets){window.WebSocket=function(a,b){var d;return d=null!=b?new N(a,b):new N(a),J("socket")&&c.trigger("request",{type:"socket",url:a,protocols:b,request:d}),d};try{w(window.WebSocket,N)}catch(d){}}}return Z(b,a),b}(h),R=null,y=function(){return null==R&&(R=new k),R},I=function(a){var b,c,d,e;for(e=D.ajax.ignoreURLs,c=0,d=e.length;d>c;c++)if(b=e[c],"string"==typeof b){if(-1!==a.indexOf(b))return!0}else if(b.test(a))return!0;return!1},y().on("request",function(b){var c,d,e,f,g;return f=b.type,e=b.request,g=b.url,I(g)?void 0:j.running||D.restartOnRequestAfter===!1&&"force"!==J(f)?void 0:(d=arguments,c=D.restartOnRequestAfter||0,"boolean"==typeof c&&(c=0),setTimeout(function(){var b,c,g,h,i,k;if(b="socket"===f?e.readyState<2:0<(h=e.readyState)&&4>h){for(j.restart(),i=j.sources,k=[],c=0,g=i.length;g>c;c++){if(K=i[c],K instanceof a){K.watch.apply(K,d);break}k.push(void 0)}return k}},c))}),a=function(){function a(){var a=this;this.elements=[],y().on("request",function(){return a.watch.apply(a,arguments)})}return a.prototype.watch=function(a){var b,c,d,e;return d=a.type,b=a.request,e=a.url,I(e)?void 0:(c="socket"===d?new n(b):new o(b),this.elements.push(c))},a}(),o=function(){function a(a){var b,c,d,e,f,g,h=this;if(this.progress=0,null!=window.ProgressEvent)for(c=null,a.addEventListener("progress",function(a){return h.progress=a.lengthComputable?100*a.loaded/a.total:h.progress+(100-h.progress)/2},!1),g=["load","abort","timeout","error"],d=0,e=g.length;e>d;d++)b=g[d],a.addEventListener(b,function(){return h.progress=100},!1);else f=a.onreadystatechange,a.onreadystatechange=function(){var b;return 0===(b=a.readyState)||4===b?h.progress=100:3===a.readyState&&(h.progress=50),"function"==typeof f?f.apply(null,arguments):void 0}}return a}(),n=function(){function a(a){var b,c,d,e,f=this;for(this.progress=0,e=["error","open"],c=0,d=e.length;d>c;c++)b=e[c],a.addEventListener(b,function(){return f.progress=100},!1)}return a}(),d=function(){function a(a){var b,c,d,f;for(null==a&&(a={}),this.elements=[],null==a.selectors&&(a.selectors=[]),f=a.selectors,c=0,d=f.length;d>c;c++)b=f[c],this.elements.push(new e(b))}return a}(),e=function(){function a(a){this.selector=a,this.progress=0,this.check()}return a.prototype.check=function(){var a=this;return document.querySelector(this.selector)?this.done():setTimeout(function(){return a.check()},D.elements.checkInterval)},a.prototype.done=function(){return this.progress=100},a}(),c=function(){function a(){var a,b,c=this;this.progress=null!=(b=this.states[document.readyState])?b:100,a=document.onreadystatechange,document.onreadystatechange=function(){return null!=c.states[document.readyState]&&(c.progress=c.states[document.readyState]),"function"==typeof a?a.apply(null,arguments):void 0}}return a.prototype.states={loading:0,interactive:50,complete:100},a}(),f=function(){function a(){var a,b,c,d,e,f=this;this.progress=0,a=0,e=[],d=0,c=C(),b=setInterval(function(){var g;return g=C()-c-50,c=C(),e.push(g),e.length>D.eventLag.sampleCount&&e.shift(),a=q(e),++d>=D.eventLag.minSamples&&a<D.eventLag.lagThreshold?(f.progress=100,clearInterval(b)):f.progress=100*(3/(a+3))},50)}return a}(),m=function(){function a(a){this.source=a,this.last=this.sinceLastUpdate=0,this.rate=D.initialRate,this.catchup=0,this.progress=this.lastProgress=0,null!=this.source&&(this.progress=F(this.source,"progress"))}return a.prototype.tick=function(a,b){var c;return null==b&&(b=F(this.source,"progress")),b>=100&&(this.done=!0),b===this.last?this.sinceLastUpdate+=a:(this.sinceLastUpdate&&(this.rate=(b-this.last)/this.sinceLastUpdate),this.catchup=(b-this.progress)/D.catchupTime,this.sinceLastUpdate=0,this.last=b),b>this.progress&&(this.progress+=this.catchup*a),c=1-Math.pow(this.progress/100,D.easeFactor),this.progress+=c*this.rate*a,this.progress=Math.min(this.lastProgress+D.maxProgressPerFrame,this.progress),this.progress=Math.max(0,this.progress),this.progress=Math.min(100,this.progress),this.lastProgress=this.progress,this.progress},a}(),L=null,H=null,r=null,M=null,p=null,s=null,j.running=!1,z=function(){return D.restartOnPushState?j.restart():void 0},null!=window.history.pushState&&(T=window.history.pushState,window.history.pushState=function(){return z(),T.apply(window.history,arguments)}),null!=window.history.replaceState&&(W=window.history.replaceState,window.history.replaceState=function(){return z(),W.apply(window.history,arguments)}),l={ajax:a,elements:d,document:c,eventLag:f},(B=function(){var a,c,d,e,f,g,h,i;for(j.sources=L=[],g=["ajax","elements","document","eventLag"],c=0,e=g.length;e>c;c++)a=g[c],D[a]!==!1&&L.push(new l[a](D[a]));for(i=null!=(h=D.extraSources)?h:[],d=0,f=i.length;f>d;d++)K=i[d],L.push(new K(D));return j.bar=r=new b,H=[],M=new m})(),j.stop=function(){return j.trigger("stop"),j.running=!1,r.destroy(),s=!0,null!=p&&("function"==typeof t&&t(p),p=null),B()},j.restart=function(){return j.trigger("restart"),j.stop(),j.start()},j.go=function(){var a;return j.running=!0,r.render(),a=C(),s=!1,p=G(function(b,c){var d,e,f,g,h,i,k,l,n,o,p,q,t,u,v,w;for(l=100-r.progress,e=p=0,f=!0,i=q=0,u=L.length;u>q;i=++q)for(K=L[i],o=null!=H[i]?H[i]:H[i]=[],h=null!=(w=K.elements)?w:[K],k=t=0,v=h.length;v>t;k=++t)g=h[k],n=null!=o[k]?o[k]:o[k]=new m(g),f&=n.done,n.done||(e++,p+=n.tick(b));return d=p/e,r.update(M.tick(b,d)),r.done()||f||s?(r.update(100),j.trigger("done"),setTimeout(function(){return r.finish(),j.running=!1,j.trigger("hide")},Math.max(D.ghostTime,Math.max(D.minTime-(C()-a),0)))):c()})},j.start=function(a){v(D,a),j.running=!0;try{r.render()}catch(b){i=b}return document.querySelector(".pace")?(j.trigger("start"),j.go()):setTimeout(j.start,50)},"function"==typeof define&&define.amd?define(["pace"],function(){return j}):"object"==typeof exports?module.exports=j:D.startOnPageLoad&&j.start()}).call(this);

/*
| -------------------------------------------------------------------------------------
| Inisialisasi Google Maps
| -------------------------------------------------------------------------------------
*/
function initialize() {
    if (jQuery('.map-google').length > 0) {
        var mapDiv = document.getElementById('map');
        var latitude = '-3.282563647725947';
        var longitude = '123.29728820439762';

        var latLng = new google.maps.LatLng(latitude, longitude);

        map = new google.maps.Map(mapDiv, {
            center: latLng,
            zoom: 6,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            navigationControl: true,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.SMALL,
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            },
            scaleControl: true,
            scaleControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            scrollwheel: false,
            draggable: true,
            styles: [{
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#aab5bb"
                    }]
                },
                {
                    "featureType": "administrative.country",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#555555"
                    }]
                },
                {
                    "featureType": "administrative.locality",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#555555"
                    }]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#f6fafd"
                    }]
                }
            ]
        });

        var locations = jQuery('[name=json_location]').val();
        locations = JSON.parse(locations);
        var marker, i;

        var infowindow = new google.maps.InfoWindow({
            maxWidth: 200
        });

        var currentMark;
        var currenti;
        var gmark = [];
        var zindex = 15;
        for (i = 0; i < locations.length; i++) {

            var icon = {
                url: locations[i]['marker'],
            }
            var icon_default = {
                url: locations[i]['marker_default'],
            }

            if (locations[i]['marker']) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']),
                    map: map,
                    // title: locations[i]['title'],
                    icon: icon,
                    zIndex: zindex,
                });
                gmark.push(marker);
            } else {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']),
                    map: map,
                    // title: locations[i]['title'],
                    icon: icon_default,
                    zIndex: zindex,
                });
                gmark.push(marker);
            }

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {

                    // CHANGE MARKER OTHER
                    for (j = 0; j < gmark.length; j++) {
                        var icon = {
                            url: locations[j]['marker'],
                        }

                        var icon_active = {
                            url: locations[j]['marker_active'],
                        }

                        var icon_default = {
                            url: locations[j]['marker_default'],
                        }
                        if (locations[j]['marker']) {
                            if (gmark[j] != marker) {
                                gmark[j].setIcon(icon);
                            } else {
                                gmark[i].setIcon(icon_active);
                            }
                        } else {
                            if (gmark[j] != marker) {
                                gmark[j].setIcon(icon_default);
                            } else {
                                gmark[i].setIcon(icon_active);
                            }
                        }
                    }

                    // marker.setIcon(locations[i]['icon_letter_x']);

                    var contentString = '<div id="gmaps-content-landing">';

                    if (locations[i]['image'] != "") {
                        contentString += '<div class="image-map">' +
                            '<img src="' + locations[i]['image'] + '" />' +
                            '</div>';
                    }

                    contentString += '<div class="container-desc">' +
                        '<div class="title">' + locations[i]['title'] + '</div>' +
                        '<div class="content-maps">' + locations[i]['content'] + '</div>' +
                        '<div class="link-read-more"><a class="button-arrow" href="' + locations[i]['link'] + '">Read More</a></div>' +
                        '</div>' +
                        '</div>';

                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                    closeMarker = infowindow;
                    globalClose.push(closeMarker);

                    currentMark = this;
                    currenti = i;
                }
            })(marker, i));
            zindex = zindex - 1;
        }

        google.maps.event.addListener(infowindow, 'closeclick', function () {
            if (locations[currenti]['marker']) {
                var map_icon = locations[currenti]['marker'];
                var icon = {
                    url: map_icon,
                }
                currentMark.setIcon(icon); //removes the marker
            } else {
                var map_icon = locations[currenti]['marker_default'];
                var icon = {
                    url: map_icon,
                }
                currentMark.setIcon(map_icon);
            }
        });

    }
}

var gmarkers = [];
var imarkers = [];
var globalClose = [];

/*
| -------------------------------------------------------------------------------------
| Function Set Info Window Google Maps
| -------------------------------------------------------------------------------------
*/
function set_info_window(map, locations) {

    var marker, i;
    var closeMarker;

    for (i = 0; i < locations.length; i++) {
        // var map_icon = jQuery('[name=map_icon_' + locations[i][3] + ']').val();

        var marker = new google.maps.Marker({
            position: {
                lat: parseFloat(locations[i][1]),
                lng: parseFloat(locations[i][2])
            },
            map: map,
            zIndex: 1,
            icon: {
                url: map_icon,
            },
        });

        gmarkers.push(marker);
        imarkers.push(i);
    }
}

var width_screen = window.innerWidth;
Pace.on('done', function() {
// 	 $(window).scroll(function (){
                // var flag = true;
        // var st = $(this).scrollTop();
        // if (st >= 500) {
                //  if (flag) {
                $.getScript( '//maps.googleapis.com/maps/api/js?key=AIzaSyDFlVTwZqnOJhc3LsnE9pKHdgajhbB9S2U&&libraries=places').done(function(){
                initialize();
                });
            //     flag = false;
            //   }
            
            // }
    // });
});
jQuery(document).ready(function () {
    zeus_control();
    discover_destination_button_schedule();
    anchor_table_rate();
    navbar_fixed_on_scroll();
    initialize_scrollbar();
    click_select_default();
    header_menu_config();
    config_owl_slider();
    set_gallery_boat_page();
    config_slick_slider();
    trip_details_button();
    filter_currency_year();
    choose_itenerary_by_boat();
    get_data_for_form();
    config_selectize('selectize');
    international_telphone_js('phone');
    international_telphone_js('phone-emergency');
    international_telphone_js('company-phone-emergency');
    close_popup_button();
    button_book_inq_table_schedule();
    config_form_booking_popup();
    plus_minus_button_booking_popup();
    config_faq_list();
    gallery_packery();
    config_lazy();
    fancybox_initialize();
    contact_form_7_callback();
	//mailchimp_callback();
    configure_form_search();
    thankyou_page();
    select_currency();
    setting_height_bg_grey();
    currency_navbar_button();
    subscribe_newsletter();
    menu_mobile();
    table_config_responsive();
    checkout_page_order_list();
    infinite_scroll_config();
    datepicker_config();
    special_meal_change();
	remove_url_textarea();
	remove_autocomplete_contactForm();
	enrollment_form_rental_equipment();
    checkout_payment_option();
});


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK LOAD IMAGE AFTER BROWSER LOAD
| -----------------------------------------------------------------------------
*/
function zeus_control() {
    var imgDefer = document.getElementsByTagName('img');
    for (var i = 0; i < imgDefer.length; i++) {
        if (imgDefer[i].getAttribute('data-src-static')) {
            imgDefer[i].setAttribute('src', imgDefer[i].getAttribute('data-src-static'));
        }
    }
}

/*
| -----------------------------------------------------------------------------
| FUNCTION KONFIGURASI BUTTON SCHEDULE PADA SECTION DISCOVER INDONESIA
| -----------------------------------------------------------------------------
*/
function discover_destination_button_schedule() {
    jQuery('.discover-destination .wrap-button .button-default').click(function () {

        jQuery(this).toggleClass('clicked');
        jQuery(this).parent().find('.wrap-btn-sch-mermaid').toggleClass('active');
    });
}

/*
| -----------------------------------------------------------------------------
| FUNCTION anchor to table schedule
| -----------------------------------------------------------------------------
*/
function anchor_table_rate() {
    if (jQuery('.page-template-page-schedule-rates.logged-in .page-anchor').length > 0) {
        jQuery('html, body').animate({
            scrollTop: jQuery('.welcome-about').position().top,
        }, 1000);
    }
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK SHOW NAVBAR TOP WHEN SCROLL
| -----------------------------------------------------------------------------
*/
function navbar_fixed_on_scroll() {
    jQuery(window).on('scroll', function () {
        var y = jQuery(this).scrollTop();

        if (jQuery('.page-template-page-bank-trf').length <= 0) {
            if (y > 70) {
                jQuery('nav.fixed-top-menu').addClass("active");
            } else {
                jQuery('nav.fixed-top-menu').removeClass("active");
            }
        }
    });
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE SCROLLBAR
| -----------------------------------------------------------------------------
*/
function initialize_scrollbar() {
    if (jQuery('.scrollbar-macosx').length > 0) {
        jQuery('.scrollbar-macosx').scrollbar();
    }
}


/*
| -------------------------------------------------------------------------------------
| Inisialisasi Form Select
| -------------------------------------------------------------------------------------
*/
function click_select_default() {
    jQuery('.select-default span').click(function () {
        jQuery('.select-default ul').fadeOut(300);
        if (jQuery(this).parent().find("ul").is(':visible')) {
            jQuery(this).parent().find("ul").fadeOut(300);
        } else {
            jQuery(this).parent().find("ul").fadeIn(300, function () {
                const this_ul = jQuery(this);
                jQuery(this).find("li").click(function () {
                    jQuery(this_ul).find("li").removeClass("active");
                    jQuery(this).addClass("active");
                    const value = jQuery(this).data("value");
                    const title = jQuery(this).data("title");
                    jQuery(this).parent().parent().find('span').text(title);
                    jQuery(this).parent().parent().find('.form-hidden').val(value);
                    jQuery(this).parent().fadeOut(300);
                })
            });
        }
    });
}


/*
| -------------------------------------------------------------------------------------
| Inisialisasi Header Menu Config
| -------------------------------------------------------------------------------------
*/
function header_menu_config() {
    jQuery('ul li.dropdown > a').click(function () {
        return false;
    });
}

/*
| -------------------------------------------------------------------------------------
| SET GALLERY PAGE BOAT
| -------------------------------------------------------------------------------------
*/
function set_gallery_boat_page() {
    jQuery('.arrow.prev, .arrow.next').click(function () {
        const ajax_url = url_for_ajax.ajaxUrl,
            key = jQuery(this).data("post"),
            count = jQuery(this).data("count"),
            action = "set-gallery-boat",
            datas = "key=" + key + "&action=" + action;

        if (count != 1) {
            jQuery('.loader-spinner').fadeIn(300);
            jQuery.ajax({
                type: "POST",
                dataType: 'json',
                url: ajax_url,
                data: datas,
                success: function (response) {
                    if (response.status == "success") {
                        jQuery('.arrow.prev, .arrow.next').attr("data-count", 1);
                        jQuery('.list-popup-boat').html(response.msg);
                        setTimeout(function () {
                            jQuery('.loader-spinner').fadeOut(300);
                            jQuery('.spinner-button').fadeOut(300);
                            jQuery('.single-boat .gallery-popup').fadeIn(300, function () {
                                config_owl_slider();
                                jQuery('body').addClass('full');
                            });
                        }, 500);
                    }
                }
            });
        } else {
            jQuery('.single-boat .gallery-popup').fadeIn(300, function () {
                jQuery('.loader-spinner').fadeOut(300);
                jQuery('body').addClass('full');
            });
        }
    });
}


/*
| -------------------------------------------------------------------------------------
| Inisialisasi Slide OWL
| -------------------------------------------------------------------------------------
*/
function config_owl_slider() {
    jQuery('h4.full-screen, .slide-image-page-detail .m-slide img').click(function () {
        jQuery('.slide-image-page-detail').addClass('gallery-popup');
        jQuery('.m-slide').trigger('refresh.owl.carousel');
        jQuery('body').addClass('full');
    });
    jQuery('.slide-image-page-detail span.close').click(function () {
        jQuery('.slide-image-page-detail').removeClass('gallery-popup');
        jQuery('.m-slide').trigger('refresh.owl.carousel');
        jQuery('body').removeClass('full');
    });

    // PAGE SINGLE BOAT
    jQuery('.single-boat span.close').click(function () {

        jQuery('.single-boat .gallery-popup').fadeOut(300, function () {
            jQuery('body').removeClass('full');
        });
    });
    if (jQuery('.single-boat .m-slide .item').length > 0) {

        jQuery('.single-boat .m-slide').owlCarousel({
            margin: 10,
            loop: true,
            autoWidth: true,
            items: 1,
            center: true
        });

        jQuery('.m-slide').trigger('refresh.owl.carousel');

        jQuery('.single-boat .m-prev').on('click', function () {
            jQuery('.single-boat .m-slide').trigger('prev.owl.carousel');
        });

        jQuery('.single-boat .m-next').on('click', function () {
            jQuery('.single-boat .m-slide').trigger('next.owl.carousel');
        });
    }

    // PAGE SINGLE ITENERARIES AND SINGLE DESTINATION
    if (jQuery('.slide-image-page-detail .m-slide .item').length > 0) {
        jQuery('.slide-image-page-detail .m-slide').owlCarousel({
            margin: 10,
            loop: true,
            autoWidth: true,
            items: 1,
            center: true
        });

        jQuery('.m-slide').trigger('refresh.owl.carousel');

        jQuery('.slide-image-page-detail .m-prev').on('click', function () {
            jQuery('.slide-image-page-detail .m-slide').trigger('prev.owl.carousel');
            zeus_control();
        });

        jQuery('.slide-image-page-detail .m-next').on('click', function () {
            jQuery('.slide-image-page-detail .m-slide').trigger('next.owl.carousel');
            zeus_control();
        });
    }

    if (jQuery('.container-gallery-popup .m-slide .item').length > 0) {
        jQuery('.container-gallery-popup .m-slide').owlCarousel({
            margin: 10,
            loop: true,
            autoWidth: true,
            items: 1,
            center: true
        });

        jQuery('.container-gallery-popup .m-prev').on('click', function () {
            jQuery('.container-gallery-popup .m-slide').trigger('prev.owl.carousel');
        });

        jQuery('.container-gallery-popup .m-next').on('click', function () {
            jQuery('.container-gallery-popup .m-slide').trigger('next.owl.carousel');
        });
    }
}


/*
| -------------------------------------------------------------------------------------
| Inisialisasi Slide Slick
| -------------------------------------------------------------------------------------
*/
function config_slick_slider() {
    if (jQuery('.container-slide-image .inner img').length > 0) {
        jQuery('.container-slide-image .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 1,
            infinite: true,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            arrow: true
        });
    }

    if (jQuery('.container-other-post-list .inner .list-other-post').length > 0) {
        jQuery('.container-other-post-list .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 2,
            infinite: false,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            arrow: true,
            responsive: [{
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            }, ]
        });
    }

    // SLICK REVIEW HOMEPAGE
    if (jQuery('.recent-reviews-list .inner .item-list').length > 0) {
        jQuery('.recent-reviews-list .inner ').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 4,
            infinite: true,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            arrow: false,
            prevArrow: jQuery(".button-reviews-button .prev"),
            nextArrow: jQuery(".button-reviews-button .next"),
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 481,
                    settings: {
                        slidesToShow: 1,
                    }
                },
            ]
        });
    }

    // SLICK SLIDER DESTINATION
    if (jQuery('.container-discover-list .inner .list-dest').length > 0) {
        jQuery('.container-discover-list .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 4,
            infinite: false,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 651,
                    settings: {
                        slidesToShow: 2,
                    }
                },
            ]
        });
    }

    // SLICK SLIDER DESTINATION HOMEPAGE
    if (jQuery('.con-image-mermaid .inner .slide-list').length > 0) {
        jQuery('.con-image-mermaid .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 1,
            infinite: true,
            speed: 500,
            autoplay: false,
            dots: true,
            // appendDots: jQuery(this).find('.dots-slider'),
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            prevArrow: false,
            nextArrow: false,
            responsive: [{
                breakpoint: 481,
                settings: "unslick"
            }]
        });
    }

    // SLICK ITENERARIES LIST
    if (jQuery('.itenerary-list .inner .list-item-iteneraries').length > 4) {
        jQuery('.itenerary-list .list-blue-default').removeClass('add-mrg');
        jQuery('.itenerary-list .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 4,
            infinite: false,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 481,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: true,
                        prevArrow: false,
                        nextArrow: false
                    }
                }
            ]
        });
    }

    // SLICK LIST NEWS
    if (jQuery('.news-event .list-blue-default .con-list-news').length > 0) {
        jQuery('.news-event .list-blue-default').slick({
            lazyLoad: 'ondemand',
            variableWidth: true,
            centerMode: false,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            responsive: [{
                    breakpoint: 9999,
                    settings: "unslick"
                },
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        prevArrow: false,
                        nextArrow: false
                    }
                }
            ]
        });
    }


    if (jQuery('.container-list-slide-star .list-default').length > 0) {
        jQuery('.container-list-slide-star').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            centerMode: false,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            responsive: [{
                    breakpoint: 9999,
                    settings: "unslick"
                },
                {
                    breakpoint: 721,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: true,
                        prevArrow: false,
                        nextArrow: false
                    }
                }
            ]
        });
    }

    if (jQuery('.wrap-list-dest-mobile .container-gradient').length > 0) {
        jQuery('.wrap-list-dest-mobile').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            centerMode: false,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            responsive: [{
                    breakpoint: 9999,
                    settings: "unslick"
                },
                {
                    breakpoint: 481,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: true,
                        prevArrow: false,
                        nextArrow: false
                    }
                }
            ]
        });
    }


    jQuery('.container-slide-image .inner, .container-discover-list .inner, .slide-image-page-detail .inner, .slide-image-page-detail .inner-nav, .container-other-post-list .inner, .con-image-mermaid .inner, .top-header .inner, .itenerary-list .inner, .wrap-list-dest-mobile, .news-event .list-blue-default').on('afterChange', function (event, slick, direction) {
        config_lazy();
    });

    slick_boat_specifications();
}


/*
| -------------------------------------------------------------------------------------
| Function untuk configurasi slick boat specifications
| -------------------------------------------------------------------------------------
*/
function slick_boat_specifications() {
    // SLICK BOAT SPECIFICATIONS
    if (jQuery('.wrap-slide-boat-specifications .inner .list-boat-specifications').length > 0) {
        jQuery('.wrap-slide-boat-specifications .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 1,
            infinite: true,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            arrow: false,
            prevArrow: jQuery(".con-button-spec .prev"),
            nextArrow: jQuery(".con-button-spec .next"),
        });
    }

    setTimeout(function () {
        const title = jQuery('.slick-current .list-boat-specifications').data("title");
        jQuery('.title-boat-spec h2').text(title);
    }, 300);

    jQuery('.wrap-slide-boat-specifications .inner').on('afterChange', function (event, slick, direction) {
        const title = jQuery('.slick-current .list-boat-specifications').data("title");
        const filter = jQuery('.slick-current .list-boat-specifications').data("filter");

        jQuery('.title-boat-spec h2').fadeOut(300, function () {
            jQuery('.title-boat-spec h2').text(title);
            jQuery('.title-boat-spec h2').fadeIn(300);
        });

        jQuery('.wrap-desc-boat-specifications .list-desc-boat-spec').fadeOut(300, function () {
            setTimeout(function () {
                jQuery('.wrap-desc-boat-specifications .list-desc-boat-spec.' + filter).fadeIn(300);
            }, 400);
        });
    });

    jQuery('.other-spec').click(function () {
        jQuery('.wrap-slide-boat-specifications .inner').slick('slickNext');
        return false;
    })
}


/*
| -------------------------------------------------------------------------------------
| Function untuk trip detail button popup
| -------------------------------------------------------------------------------------
*/
function trip_details_button() {
    jQuery('.trip-details').click(function () {
        const ajax_url = url_for_ajax.ajaxUrl;
        const trip = jQuery(this).data("trip");
        const boat = jQuery(this).data("boat-id");
        const action = "get-detail-trip";
        const datas = "trip=" + trip + "&action=" + action + "&boat=" + boat;

        jQuery('.loading-page').fadeIn(300);
        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajax_url,
            data: datas,
            success: function (response) {
                if (response.status == "success") {
                    jQuery('.wrap-popup-trip-details .wrap-trip-details').html(response.msg);
                    jQuery('body').addClass('full');
                    jQuery('.wrap-popup-trip-details').addClass('active');
                    setTimeout(function(){
                        config_owl_slider();
                        config_lazy();
                    }, 300);
                }
                setTimeout(function(){
                    jQuery('.loading-page').fadeOut(1000);
                }, 300);
            }
        });

        // jQuery('body').addClass('full');
        // jQuery('.wrap-popup-trip-details').addClass('active');
        // const url = jQuery(this).attr("href") + '?popup=yes&boat_id=' + boat_id;

        // jQuery.fancybox.open({
        //     src: url,
        //     type: 'iframe',
        // });
        return false;
    });
}


/*
| -------------------------------------------------------------------------------------
| Function untuk filter currency and year pada schedule and rates
| -------------------------------------------------------------------------------------
*/
function filter_currency_year() {

    jQuery('.radio-desktop').prop('checked', false);
    jQuery('.radio-mobile').prop('checked', false);
    // IF FILTER YEAR CLICK
    jQuery('ul.choose-filter li').click(function () {
        jQuery('.wrap-schedule-rate-table .wrap-table-default .loading-global').fadeIn(300);
        const ajax_url = url_for_ajax.ajaxUrl;
        const page_now = jQuery('[name=page_now]').val();
        const filter = jQuery(this).data("filter");
        const iteneraries = jQuery('[name=filter-iteneraries]').val();
        const action = "choose-filter-schedule";
        const mermaid_type = jQuery('[name=mermaid-type]').val();
        const code_currency = jQuery('.select-currency').data('currency-code');
        const symbol_currency = jQuery('.select-currency').data('currency-symbol');
        const datas = "year=" + filter + "&action=" + action + "&mermaid_type=" + mermaid_type + "&iteneraries=" + iteneraries + "&code_currency=" + code_currency + "&symbol_currency=" + symbol_currency + "&page_now=" + page_now;

        jQuery("[name=filter-year]").val(filter);

        if (!jQuery(this).hasClass("active")) {
            jQuery('ul.choose-year li').removeClass("active");
            jQuery(this).addClass("active");
            jQuery.ajax({
                type: "POST",
                dataType: 'json',
                url: ajax_url,
                data: datas,
                success: function (response) {
                    if (response.status == "success") {
                        jQuery('.wrap-table-schedule table tbody tr').fadeOut(300, function () {
                            jQuery(this).remove();
                            jQuery('.wrap-table-schedule table tbody').html(response.html);
                            jQuery('.wrap-schedule-data-mobile .container-schedule-data-mobile').html(response.html_mobile);
                            jQuery('.wrap-schedule-rate-table .button-more-sch').html(response.html_button_more);
                            price_click_filter();
                            trip_details_button();
                            button_book_inq_table_schedule();
                            jQuery('.wrap-schedule-rate-table .wrap-table-default .loading-global').fadeOut(300);
                        });
                    }
                }
            });
        } else {
            jQuery('.wrap-schedule-rate-table .wrap-table-default .loading-global').fadeOut(300);
        }
    });

    // IF FILTER Mermaid Change
    jQuery('select.choose-filter').change(function () {
        jQuery('.wrap-schedule-rate-table .wrap-table-default .loading-global').fadeIn(300);
        const ajax_url = url_for_ajax.ajaxUrl;
        const page_now = jQuery('[name=page_now]').val();
        const year = jQuery("[name=filter-year]").val();
        const iteneraries = jQuery('[name=filter-iteneraries]').val();
        const action = "choose-filter-schedule";
        const mermaid_type = jQuery('[name=mermaid-type]').val();
        const code_currency = jQuery('.select-currency').data('currency-code');
        const symbol_currency = jQuery('.select-currency').data('currency-symbol');
        const datas = "year=" + year + "&action=" + action + "&mermaid_type=" + mermaid_type + "&iteneraries=" + iteneraries + "&code_currency=" + code_currency + "&symbol_currency=" + symbol_currency + "&page_now=" + page_now;

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajax_url,
            data: datas,
            success: function (response) {
                if (response.status == "success") {
                    jQuery('.wrap-table-schedule table tbody').html(response.html);
                    jQuery('.wrap-schedule-data-mobile .container-schedule-data-mobile').html(response.html_mobile);
                    jQuery('.wrap-schedule-rate-table .button-more-sch').html(response.html_button_more);
                    //price_click_filter();
                    trip_details_button();
                    button_book_inq_table_schedule();
                }
                jQuery('.wrap-schedule-rate-table .wrap-table-default .loading-global').fadeOut(300);
            }
        });
    });

    price_click_filter();
}


function price_click_filter() {
    jQuery('.price-desktop').click(function () {
		
        jQuery('.radio-desktop').prop('checked', false);
		
        ajax_price_choose(this, 'desktop');
    });

    jQuery('.price-mobile').click(function () {
        jQuery('.radio-mobile').prop('checked', false);
        ajax_price_choose(this, 'mobile');
    });
}


function ajax_price_choose(data, screen) {
    const parent = jQuery(data).parent();
    const ajax_url = url_for_ajax.ajaxUrl;
    const radio = jQuery(parent).find('input');
    const type = jQuery(radio).data("type");
    const key = jQuery(radio).data("key");
    const boat = jQuery(radio).data("boat");
    const itenerary = jQuery(radio).data("itenerary");
    const sanitize = jQuery(radio).data("sanitize");
    const action = "choose-price";
    const datas = "type=" + type + "&key=" + key + "&action=" + action + "&screen=" + screen + "&boat=" + boat + "&itenerary=" + itenerary;

    jQuery(radio).prop('checked', true);
	
	if (screen == "desktop") {
		jQuery('.wrap-schedule-rate-table .wrap-table-default .loading').fadeIn(300);
	} else {
		jQuery('.wrap-schedule-data-mobile #mobile-' + sanitize).find('.loading').fadeIn(300);
	}
	
    jQuery.ajax({
        type: "POST",
        dataType: 'json',
        url: ajax_url,
        data: datas,
        success: function (response) {
           
            if (response.status == "success") {
                if (screen == "desktop") {
                    var html_status = '.wrap-table-schedule table tbody tr#' + sanitize + ' td.status';
                    var html_status_all = '.wrap-table-schedule table tbody tr td.status';
                } else {
                    var html_status = '.wrap-schedule-data-mobile #mobile-' + sanitize + ' .status';
                    var html_status_all = '.wrap-schedule-data-mobile .status';
                }

                jQuery(html_status_all).find('a').removeClass('inquire').addClass('book_now').text('Book Now');
                jQuery(html_status).html(response.button_html);

                config_form_booking_popup();
                button_book_inq_table_schedule();
				
            }
			 
            jQuery('.wrap-schedule-rate-table .wrap-table-default .loading').fadeOut(300);
            jQuery('.wrap-schedule-data-mobile .list-schedule-data#' + sanitize).find('.loading').fadeIn(300);
        }
    });
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK KONFIGURASI SELECT CURRENCY
| -------------------------------------------------------------------------------------------------------------------------
*/
function select_currency() {
    jQuery('.select-currency').click(function () {
        if (jQuery('.wrap-list-currency').is(':visible')) {
            jQuery('.wrap-list-currency').fadeOut(300);
        } else {
            jQuery('.wrap-list-currency').fadeIn(300);
        }
    });

    jQuery('.container-list-currency-featured .list-currency-featured, .container-list-currency-all .list-currency-all').click(function () {
        const template = jQuery(this).parents('.wrap-list-currency');
        jQuery(template).find('.list-curr').removeClass('active');
        const ajax_url = url_for_ajax.ajaxUrl;
        const page_now = jQuery('[name=page_now]').val();
        const code_currency = jQuery(this).data('currency-code');
        const symbol_currency = jQuery(this).data('currency-symbol');
        const year = jQuery("[name=filter-year]").val();
        const month = jQuery('[name=filter-month]').val();
        const iteneraries = jQuery('[name=filter-iteneraries]').val();
        const dest = jQuery('[name=filter-dest]').val();
        const year_month = jQuery('[name=year-month]').val();
        const action = "choose-filter-schedule";
        const mermaid_type = jQuery('[name=mermaid-type]').val();
        const datas = "year=" + year + "&month=" + month + "&action=" + action + "&mermaid_type=" + mermaid_type + "&iteneraries=" + iteneraries + "&dest=" + dest + "&code_currency=" + code_currency + "&symbol_currency=" + symbol_currency + "&year_month=" + year_month + "&page_now=" + page_now;

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajax_url,
            data: datas,
            success: function (response) {
                if (response.status == "success") {
                    jQuery('.wrap-list-currency').fadeOut(300, function () {
                        jQuery('.select-currency').text(code_currency);
                        jQuery('.select-currency').attr("data-currency-symbol", symbol_currency);
                        jQuery('.select-currency').attr("data-currency-code", code_currency);
                        jQuery('.wrap-table-schedule table tbody').html(response.html);
                        jQuery('.wrap-schedule-data-mobile .container-schedule-data-mobile').html(response.html_mobile);
                        jQuery('.wrap-schedule-rate-table .button-more-sch').html(response.html_button_more);
                        jQuery(template).find('.' + code_currency).addClass('active');
                        price_click_filter();
                        trip_details_button();
                        button_book_inq_table_schedule();
                    });
                }
            }
        });
    });
}


/*
| -------------------------------------------------------------------------------------
| Function untuk filter itenerary by boat
| -------------------------------------------------------------------------------------
*/
function choose_itenerary_by_boat() {
    jQuery('.wrap-iteneraries-table .row .wrap-list-boat-iteneraries').click(function () {
        const filter = jQuery(this).data("filter");

        jQuery('.wrap-iteneraries-table .row .wrap-list-boat-iteneraries').removeClass('active');
        jQuery(this).addClass('active');
        jQuery('.wrap-iteneraries-table .container-itenerary-table').removeClass('active');
        jQuery('#' + filter).addClass('active');
    });

    jQuery('.button-itenerary-table').click(function () {

        const filter = jQuery(this).data("filter");
        jQuery('html, body').animate({
            scrollTop: jQuery('.wrap-iteneraries-table').position().top,
        }, 500, function () {


            jQuery('.wrap-iteneraries-table .row .wrap-list-boat-iteneraries').removeClass('active');
            jQuery('.wrap-iteneraries-table .row .' + filter).addClass('active');

            jQuery('.wrap-iteneraries-table .container-itenerary-table').removeClass('active');
            jQuery('#' + filter).addClass('active');
        });

        return false;
    });

    // if( jQuery('.wrap-iteneraries-table .row .wrap-list-boat-iteneraries.active.mermaid-ii-itenerary-table').length > 0 ){
    //     console.log('hahah');
        
    //     const filter = jQuery('.wrap-iteneraries-table .row .wrap-list-boat-iteneraries.active.mermaid-ii-itenerary-table').data("filter");

    //     jQuery('.wrap-iteneraries-table .container-itenerary-table').removeClass('active');
    //     jQuery('#' + filter).addClass('active');

    // }
}


/*
| -------------------------------------------------------------------------------------
| Function untuk configuration selectize
| -------------------------------------------------------------------------------------
*/
function config_selectize(classs) {
    jQuery('.' + classs).selectize();
}


/*
| -------------------------------------------------------------------------------------
| Function untuk mendapatkan value data for form
| -------------------------------------------------------------------------------------
*/
function get_data_for_form() {

    // NATIONALITY DATA
    if (jQuery('.selectize-nationality').length > 0) {
        const ajax_url = url_for_ajax.ajaxUrl;
        const action = "nationality-data";
        const datas = "action=" + action;

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajax_url,
            data: datas,
            success: function (response) {
                if (response.status == "success") {
                    jQuery('.selectize-nationality').append(response.html).ready(function () {
                        config_selectize('selectize-nationality');
                    });
                }
            }
        });
    }


    // ITINERARY DATA
    if (jQuery('.selectize-itinerary').length > 0) {
        const ajax_url = url_for_ajax.ajaxUrl;
        const action = "itinerary-data";
        const datas = "action=" + action;

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajax_url,
            data: datas,
            success: function (response) {
                if (response.status == "success") {
                    jQuery('.selectize-itinerary').append(response.html).ready(function () {
                        config_selectize('selectize-itinerary');
                    });
                }
            }
        });
    }


    // DESTINATION DATA
    if (jQuery('#type_of_inquiry').length > 0) {
        const ajax_url = url_for_ajax.ajaxUrl;
        const action = "destination-data";
        const datas = "action=" + action;

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajax_url,
            data: datas,
            success: function (response) {
                if (response.status == "success") {
                    jQuery('#type_of_inquiry').append(response.html).ready(function () {
                        config_selectize('selectize-destination');
                    });
                }
            }
        });
    }

    // SCHEDULE DATA
    if (jQuery('#trip_code').length > 0) {
        const ajax_url = url_for_ajax.ajaxUrl;
        const action = "schedule-data";
        const datas = "action=" + action;

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ajax_url,
            data: datas,
            success: function (response) {
                if (response.status == "success") {
                    jQuery('#trip_code').append(response.html).ready(function () {
                        config_selectize('selectize-trip-code');
                    });
                }
            }
        });
    }
}


/*
| -------------------------------------------------------------------------------------
| Function untuk inisialisasi international telphone js
| -------------------------------------------------------------------------------------
*/
function international_telphone_js(id) {
    if (jQuery('#'+id).length > 0) {
        const utils_js = jQuery('[name=theme_url_asset]').val() + '/js/utils.js';
        const input = document.querySelector("#"+id);
        window.intlTelInput(input, {
            utilsScript: utils_js,
        });
    }
}

/*
| -------------------------------------------------------------------------------------
| Function untuk konfigurasi button close popup
| -------------------------------------------------------------------------------------
*/
function close_popup_button() {
    jQuery('span.close-popup, .wrap-popup-notification .overlay').click(function () {
        jQuery('.wrap-popup').fadeOut(300);
        jQuery('header').fadeOut(300, function () {
            jQuery(this).find('.close-popup').hide();
        });
        jQuery('.wrap-popup-notification').removeClass('active');
        jQuery('.wrap-alert-inport').removeClass('active');
        jQuery('.wrap-popup-trip-details').removeClass('active');
        setTimeout(function () {
            jQuery('body').removeClass('full');
            jQuery('.wrap-popup-trip-details .wrap-trip-details').html("");
        }, 500);
    });
}


/*
| -------------------------------------------------------------------------------------
| Function untuk konfigurasi button book now dan inquiry pada table schedule and rates
| -------------------------------------------------------------------------------------
*/
function button_book_inq_table_schedule() {
    jQuery('.wrap-table-schedule a.disabled').click(function () {
        const message = jQuery('[name=alert_cabin_first]').val();
        jQuery('.wrap-popup-notification .container-popup-notification').find('.message').remove();
        jQuery('.wrap-popup-notification .container-popup-notification').append('<div class="message">' + message + '</div>');
        jQuery('.wrap-popup-notification').addClass('active');
        jQuery('body').addClass('full');
        return false;
    });

    jQuery('.wrap-table-schedule a.disabled-inport').click(function () {
        jQuery('.wrap-alert-inport').addClass('active');
        return false;
    });
}


/*
| -------------------------------------------------------------------------------------
| Function untuk konfigurasi form booking popup
| -------------------------------------------------------------------------------------
*/
function config_form_booking_popup() {
    jQuery('.wpcf7-checkbox .wpcf7-list-item').append('<span class="checkmark"></span>');
    jQuery('[name=person_count_master]').val(0);
    jQuery('[name=person_count_single]').val(0);
    jQuery('[name=person_count_deluxe]').val(0);
    jQuery('[name=person_count_lower]').val(0);
    jQuery('.wpcf7-checkbox .wpcf7-list-item:nth-child(1) input').addClass('checkbox-master');
    jQuery('.wpcf7-checkbox .wpcf7-list-item:nth-child(2) input').addClass('checkbox-single');
    jQuery('.wpcf7-checkbox .wpcf7-list-item:nth-child(3) input').addClass('checkbox-deluxe');
    jQuery('.wpcf7-checkbox .wpcf7-list-item:nth-child(4) input').addClass('checkbox-lower');

    jQuery('.cabin input').prop("checked", false);

    jQuery('.wrap-schedule-rate-table a.inquire, .wrap-schedule-rate-table a.book_now').click(function (e) {
        const screen = jQuery(this).data("screen");
        const boat = jQuery(this).data("boat");
        var port_in = jQuery(this).data("port-in");
        var port_out = jQuery(this).data("port-out");

        if(boat == "mermaid-ii"){
            jQuery('.checkbox-master').parent().hide();
            jQuery('.no-of-guest .master').hide();
            jQuery('.checkbox-single').parent().hide();
            jQuery('.no-of-guest .single').hide();
            jQuery('.checkbox-lower').val("Budget");
            jQuery('.checkbox-lower').parent().find('.wpcf7-list-item-label').text("Budget");
        }else{
            jQuery('.checkbox-master').parent().show();
            jQuery('.no-of-guest .master').show();
            jQuery('.checkbox-single').parent().show();
            jQuery('.no-of-guest .single').show();
            jQuery('.checkbox-lower').val("Lower Deck");
            jQuery('.checkbox-lower').parent().find('.wpcf7-list-item-label').text("Lower Deck");
        }

        var trip_code, depart_date, itenerary, return_date, boat_name, cabin_type, cabin_price, cabin_typea = '';
		
        if (screen == "desktop") {
            var parent = jQuery(this).parent().parent();
            trip_code = parent.find('td.trip_code').data("value");
            depart_date = parent.find('td.depart_date').data("value");
            itenerary = parent.find('td.itenerary h3').data("value");
            return_date = parent.find('td.itenerary h4.return').data("value");
            boat_name = parent.find('td.itenerary h4.mermaid-type').data("value");
            cabin_type = parent.find('td input:checked').data("type");
            cabin_typea = cabin_type.toLowerCase().replace(/\b[a-z]/g, function (cabin_type) {
                return cabin_type.toUpperCase();
				
            });
        } else {
            var parent = jQuery(this).parents('.list-schedule-data');
            trip_code = parent.find('.trip-code').text();
            depart_date = parent.find('h5.depart-date').text();
            itenerary = parent.find('h5.itenerary-title').text()
            return_date = parent.find('.itenerary h4.return').text();
            boat_name = parent.find('.itenerary h4.mermaid-type').text();
            cabin_type = parent.find('input:checked').data("type");
            cabin_typea = cabin_type.toLowerCase().replace(/\b[a-z]/g, function (cabin_type) {
                return cabin_type.toUpperCase();
            });
        }
		 
		// if(boat == "mermaid-i"){
			// if(cabin_type == 'Lower Deck' ){
				// cabin_type = 'Lower';
			// }
		// }
	
        jQuery('.person').text("0");

        jQuery('.wrap-popup .spinner').fadeIn(300);
        jQuery('.wrap-popup .spinner img').fadeIn(300);

        jQuery('.wrap-popup').fadeIn(300, function () {
            jQuery('body').addClass('full');
            const lower_cabin_type = cabin_type.toLowerCase();

            jQuery('.wrap-popup').animate({
                scrollTop: 0
            }, 1000);

            if(port_in == ""){
                port_in = "-";
            }
            if(port_out == ""){
                port_out = "-";
            }

            jQuery(this).find('[type=checkbox]').each(function(){
            	if( jQuery(this).hasClass('checkbox-' + cabin_type) )
            	{
            		jQuery(this).prop('checked', true);
            	}
            	else
            	{
            		jQuery(this).prop('checked', false);
            	}

            	if( jQuery(this).hasClass('checkbox-master') && parent.find('[data-type=master]').length > 0 )
            	{
            		var dmaster = parent.find('[data-type=master]').data();

	            	jQuery(this).attr('data-type', dmaster.type );
	            	jQuery(this).attr('data-price', dmaster.price );
	            	jQuery(this).attr('data-name', trip_code + ' Cabin Master' );
	            	jQuery(this).attr('data-currency-code', dmaster.currencyCode );
	            	jQuery(this).attr('data-id', dmaster.itenerary + '-' + dmaster.sanitize );
            	}

            	if( jQuery(this).hasClass('checkbox-single') && parent.find('[data-type=single]').length > 0 )
            	{
            		var dsingle = parent.find('[data-type=single]').data();
            		
	            	jQuery(this).attr('data-type', dsingle.type );
	            	jQuery(this).attr('data-price', dsingle.price );
	            	jQuery(this).attr('data-name', trip_code + ' Cabin Single' );
	            	jQuery(this).attr('data-currency-code', dsingle.currencyCode );
	            	jQuery(this).attr('data-id', dsingle.itenerary + '-' + dsingle.sanitize );
            	}

            	if( jQuery(this).hasClass('checkbox-deluxe') && parent.find('[data-type=deluxe]').length > 0 )
            	{
            		var ddeluxe = parent.find('[data-type=deluxe]').data();
            		
	            	jQuery(this).attr('data-type', ddeluxe.type );
	            	jQuery(this).attr('data-price', ddeluxe.price );
	            	jQuery(this).attr('data-name', trip_code + ' Cabin Deluxe' );
	            	jQuery(this).attr('data-currency-code', ddeluxe.currencyCode );
	            	jQuery(this).attr('data-id', ddeluxe.itenerary + '-' + ddeluxe.sanitize );
            	}

            	if( jQuery(this).hasClass('checkbox-lower') && parent.find('[data-type=lower]').length > 0 )
            	{
            		var dlower = parent.find('[data-type=lower]').data();
            		
	            	jQuery(this).attr('data-type', dlower.type );
	            	jQuery(this).attr('data-price', dlower.price );
	            	jQuery(this).attr('data-name', trip_code + ' Cabin Lower' );
	            	jQuery(this).attr('data-currency-code', dlower.currencyCode );
	            	jQuery(this).attr('data-id', dlower.itenerary + '-' + dlower.sanitize );
            	}
            });

            jQuery(this).find('[name=person_count_' + cabin_type + ']').val(1);
            jQuery(this).find('.person-' + cabin_type).text(1);
            jQuery(this).find('#boat_name').text(boat_name);
            jQuery(this).find('[name=boat_name]').val(boat_name);
            jQuery(this).find('#port_in').text(port_in);
            jQuery(this).find('[name=port_in]').val(port_in);
            jQuery(this).find('#port_out').text(port_out);
            jQuery(this).find('[name=port_out]').val(port_out);
            jQuery(this).find('#trip_start').text(depart_date);
            jQuery(this).find('[name=trip_start]').val(depart_date);
            jQuery(this).find('#trip_finish').text(return_date);
            jQuery(this).find('[name=trip_finish]').val(return_date);
            jQuery(this).find('#trip_code').text(trip_code);
            jQuery(this).find('[name=trip_code]').val(trip_code);
            jQuery(this).find('#itenerary').text(itenerary);
            jQuery(this).find('[name=itenerary]').val(itenerary);
            jQuery(this).find('#popup-' + lower_cabin_type).attr('checked', true);
            jQuery(this).find('[name=cabin_type_value]').val(jsUcfirst(cabin_type));
            jQuery(this).find('[name=trip_code]').attr('data-id', parent.find('[data-type=master]').data('sanitize') );

            jQuery('.wrap-popup .spinner').fadeOut(300);
            jQuery('.wrap-popup .spinner img').fadeOut(300);
        });

        return false;
    });

    // RADIO BUTTON CHECKED CABIN TYPE
    jQuery('.cabin input').click(function () {
        var value = jQuery('input[name=cabin_type]:checked').val();
        var dtype = jQuery(this).data('type');
        var type  = jQuery(this).attr('value');
        	type  = type.toLowerCase();

        jQuery('.wrap-popup .spinner').fadeIn(300);
        jQuery('.wrap-popup .spinner img').fadeIn(300);

        var value_ucfirst = '';

        if( type != '' && typeof (type) != 'undefined')
        {
            value_ucfirst = jsUcfirst(type);
        }

        if( jQuery(this).prop('checked') == true )
        {
            jQuery('[name=person_count_' + dtype + ']').val(1);
            jQuery('.person-' + dtype).text(1);
        }

        if( jQuery(this).is(':not(:checked)') )
        {
            jQuery('[name=person_count_' + dtype + ']').val(0);
            jQuery('.person-' + dtype).text(0);
        }

        jQuery('.wrap-popup [name=cabin_type_value]').val( jsUcfirst( value_ucfirst ) );

        setTimeout(function () {
            jQuery('.wrap-popup .spinner').fadeOut(300);
            jQuery('.wrap-popup .spinner img').fadeOut(300);
        }, 800);
    });


}

function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


/*
| -------------------------------------------------------------------------------------
| Function untuk plus minus button booking popup
| -------------------------------------------------------------------------------------
*/
function plus_minus_button_booking_popup() {
    jQuery('.wrap-popup .plus').click(function () {
        const data_type = jQuery(this).data("type");
        jQuery('.checkbox-' + data_type).prop("checked", true);

        if (data_type == "master") {
            var max_person = 2;
        } else if (data_type == "single") {
            var max_person = 1;
        } else if (data_type == "deluxe") {
            var max_person = 8;
        } else if (data_type == "lower") {
            var max_person = 4;
        } else {
            var max_person = 0;
        }

        const parent = jQuery(this).parent().parent();
        const person = parseInt(parent.find('.' + data_type + ' [name=person_count_' + data_type + ']').val());
        const person_new = person + 1;

        if (person_new <= max_person) {
            const text_person = person_new;
            parent.find('.' + data_type + ' [name=person_count_' + data_type + ']').val(person_new);
            parent.find('.' + data_type + ' .person-' + data_type).text(text_person);
        }
    });

    jQuery('.wrap-popup .minus').click(function () {
        const data_type = jQuery(this).data("type");
        const parent = jQuery(this).parent().parent();
        const person = parseInt(parent.find('.' + data_type + ' [name=person_count_' + data_type + ']').val());
        const person_new = person - 1;

        if (person > 0) {
            const text_person = person_new;
            parent.find('.' + data_type + ' [name=person_count_' + data_type + ']').val(person_new);
            parent.find('.' + data_type + ' .person-' + data_type).text(text_person);
        }

        if (person_new == 0) {
            jQuery('.checkbox-' + data_type).prop("checked", false);
        }
    });
}

function add_some_text(text, value) {
    var result;
    if (value > 1) {
        result = value + ' ' + text + 's';
    } else {
        result = value + ' ' + text;
    }

    return result;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk konfigurasi faq list
| -------------------------------------------------------------------------------------
*/
function config_faq_list() {
    // console.log("asdasd");
    jQuery('.wrap-list-faq .container-list-faq').click(function () {
        jQuery('.wrap-list-faq .container-list-faq').removeClass('blur');
        jQuery('.wrap-list-faq .container-list-faq').removeClass('active');
        jQuery('.wrap-list-faq .container-list-faq').addClass('blur');
        if (jQuery(this).find('.desc-faq').is(":visible")) {
            jQuery('.wrap-list-faq .container-list-faq').removeClass('blur');
            jQuery('.wrap-list-faq .container-list-faq').removeClass('active');
            jQuery(this).find('.desc-faq').slideUp(300);
        } else {
            jQuery('.wrap-list-faq .container-list-faq .desc-faq').slideUp(300);
            jQuery(this).find('.desc-faq').slideDown(300);
            jQuery(this).addClass('active');
        }
    });
}


/*
| -------------------------------------------------------------------------------------
| Function untuk konfigurasi gallery packery
| -------------------------------------------------------------------------------------
*/
function gallery_packery() {
    if (jQuery('.wrap-photo-gallery .grid').length > 0) {
        var $grid = jQuery('.wrap-photo-gallery .grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                columnWidth: '.grid-sizer',
                gutter: '.gutter-sizer'
            }
        });

        // console.log(bLazy_gallery);
        


        jQuery('.lazy-gallery').Lazy({
            afterLoad: function (element) {
                $grid.isotope();
            },
            placeholder: "data:image/gif;base64,R0lGODlhEALAPQAPzl5uLr9Nrl8e7..."
        });

        jQuery(window).on('scroll', function () {
            $grid.isotope();
        });
    }

    jQuery('.container-filter-gallery li').click(function () {
        var current = $(this);

        current.siblings('li').removeClass('active');
        current.addClass('active');

        var filterValue = current.attr('data-filter');

        var container = current.closest('.wrap-photo-gallery').find('.grid');
        container.isotope({
            filter: filterValue
        });
    });
}


/*
| -------------------------------------------------------------------------------------
| Function untuk konfigurasi lazy load
| -------------------------------------------------------------------------------------
*/
function config_lazy() {
    // jQuery('.lazy').lazy({
    //     placeholder: "data:image/gif;base64,R0lGODlhEALAPQAPzl5uLr9Nrl8e7...",
    //     afterLoad: function (element) {
    //         jQuery(element).addClass('stop-lazy');
    //     },
    // });

    var bLazy = new Blazy({
        breakpoints: [{
            width: 576,
            src: 'data-src-small'
        }],
        success: function(e){
            jQuery(e).addClass('stop-lazy');
        },
        error: function(e, msg){
            const img_default = jQuery('[name=image_default_src]').val();
            if(msg === 'missing'){
                if(jQuery(e).is('img')){
                    jQuery(e).attr("src", img_default);
                }
            }
            else if(msg === 'invalid'){
                if(jQuery(e).is('img')){
                    jQuery(e).attr("src", img_default);
                }
            }  
        }
    });
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE FANCYBOX
| -------------------------------------------------------------------------------------------------------------------------
*/
function fancybox_initialize() {
    if (jQuery("a.fancy-group").length > 0) {
        jQuery("a.fancy-group").fancybox({
            'opacity': true,
            'overlayShow': false,
            'transitionIn': 'elastic',
            'transitionOut': 'none',
            'showNavArrows': true,
            'youtube': {
                'controls': 0,
                'showinfo': 0
            },
            'transitionEffect': 'fade',
            'helpers': {
                'title': {
                    'type': 'inside',
                    'position': 'top'
                }
            },
            'afterLoad': function (instance, current) {
                var caption = current.opts.caption;
                jQuery(current.$slide).find('.fancybox-placeholder .caption-img').remove();
                if (caption != "") {
                    jQuery(current.$slide).find('.fancybox-placeholder img').after("<div class='caption-img'><p>" + caption + "</p></div>");
                    jQuery(current.$slide).find('.fancybox-placeholder .caption-img').fadeIn(300);
                }
            }
        });
    }
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE CONTACT FORM 7 CALLBACK
| -------------------------------------------------------------------------------------------------------------------------
*/
function contact_form_7_callback() {
    jQuery('[name=site-url]').val(jQuery('[name=web_url]').val());
    jQuery('[name=asset-url]').val(jQuery('[name=theme_url_asset]').val());
	
    const agent_email = jQuery('[name=email_agent]').val();
    // jQuery('#email, #your-email').val(agent_email);

    // CONTACT PAGE CALLBACK
    if (jQuery('.page-template-page-contact').length > 0) {

        jQuery('.btn-send-inq').click(function () {
            jQuery('.loading-page').fadeIn(300);
        });

        document.addEventListener('wpcf7mailsent', function (event) {
            //var message = jQuery('[name=message_popup_success]').val();
            //jQuery('.loading-page').fadeOut(300);
            // jQuery('.page-template-page-contact').find('.wrap-popup-notification').addClass("active");
            // jQuery('.wrap-popup-notification .container-popup-notification').find('.message').remove();
            // jQuery('.page-template-page-contact').find('.wrap-popup-notification .container-popup-notification').append('<div class="message">'+message+'</div>');
            const thankyou_link = jQuery('[name=thankyou_link]').val();

            window.location.href = thankyou_link;
        }, false);

        document.addEventListener('wpcf7spam', function (event) {
            jQuery('.loading-page').fadeOut(300);
            message_error_contact_form();
        }, false);

        document.addEventListener('wpcf7mailfailed', function (event) {
            jQuery('.loading-page').fadeOut(300);
            message_error_contact_form();
        }, false);

        document.addEventListener('wpcf7invalid', function (event) {
            jQuery('.loading-page').fadeOut(300);
            jQuery('html, body').animate({
                scrollTop: jQuery('.body-form-contact').position().top - 50,
            }, 1000);
        }, false);
    }

    // INQUIRY FORM CALLBACK
    if (jQuery('.header-form-inquiry').length > 0) {

        jQuery('.btn-send-inq').click(function () {
            const port_in     = jQuery('.wrap-section-form-inquiry').find('[name=port_in]').val();
            const port_out    = jQuery('.wrap-section-form-inquiry').find('[name=port_out]').val();
            const boat_name   = jQuery('.wrap-section-form-inquiry').find('[name=boat_name]').val();
            const trip_code   = jQuery('.wrap-section-form-inquiry').find('[name=trip_code]').val();
            const itenerary   = jQuery('.wrap-section-form-inquiry').find('[name=itenerary]').val();
            const trip_start  = jQuery('.wrap-section-form-inquiry').find('[name=trip_start]').val();
            const trip_finish = jQuery('.wrap-section-form-inquiry').find('[name=trip_finish]').val();
            
            jQuery('.wrap-popup .spinner').fadeIn(300);
            jQuery('.wrap-popup .spinner-button').fadeIn(300);
        });

        document.addEventListener('wpcf7mailsent', function (event) {
            const boat_name = jQuery('.wrap-section-form-inquiry').find('[name=boat_name]').val();
            const departure_date = jQuery('.wrap-section-form-inquiry').find('[name=trip_start]').val();
            const itenerary = jQuery('.wrap-section-form-inquiry').find('[name=itenerary]').val();
            var cabin_type_value = jQuery('.wrap-section-form-inquiry').find('[name=cabin_type_value]').val();
            const thankyou_link = jQuery('.wrap-popup').find('[name=thankyou_link]').val();
			
            localStorage.setItem("boat_name", boat_name);
            localStorage.setItem("departure_date", departure_date);
            localStorage.setItem("itenerary", itenerary);
            localStorage.setItem("cabin_type_value", cabin_type_value);
            localStorage.setItem("thankyou_page", 1);

            window.location.href = thankyou_link;
			
        }, false);

        document.addEventListener('wpcf7invalid', function (event) {
            // console.log(jQuery('.wrap-popup').find('#fullname').offset().top);
            jQuery('.wrap-popup .spinner').fadeOut(300);
            jQuery('.wrap-popup .spinner-button').fadeOut(300);
            jQuery('.wrap-popup').animate({
                scrollTop: jQuery('.wrap-popup').find('#fullname').offset().top,
            }, 1000);
        }, false);

        document.addEventListener('wpcf7spam', function (event) {
            jQuery('.wrap-popup .spinner').fadeOut(300);
            jQuery('.wrap-popup .spinner-button').fadeOut(300);

            message_error_contact_form();
        }, false);

        document.addEventListener('wpcf7mailfailed', function (event) {
            jQuery('.wrap-popup .spinner').fadeOut(300);
            jQuery('.wrap-popup .spinner-button').fadeOut(300);
            message_error_contact_form();
        }, false);
    }

    // ENROLLMENT FORM CALLBACK
    if (jQuery('.page-template-page-enrollment-form').length > 0) {

        document.addEventListener('wpcf7mailsent', function (event) {
            // var message = jQuery('[name=message_popup_success]').val();
            // jQuery('.loading-page').fadeOut(300);
            // jQuery('.page-template-page-enrollment-form').find('.wrap-popup-notification').addClass("active");
            // jQuery('.wrap-popup-notification .container-popup-notification').find('.message').remove();
            // jQuery('.page-template-page-enrollment-form').find('.wrap-popup-notification .container-popup-notification').append('<div class="message">'+message+'</div>');
            const thankyou_link = jQuery('[name=thankyou_link]').val();

            window.location.href = thankyou_link;
        }, false);

        jQuery('.btn-send-inq').click(function () {
            jQuery('.loading-page').fadeIn(300);
        });

        document.addEventListener('wpcf7spam', function (event) {
            jQuery('.loading-page').fadeOut(300);
            message_error_contact_form();
        }, false);

        document.addEventListener('wpcf7mailfailed', function (event) {
            jQuery('.loading-page').fadeOut(300);
            message_error_contact_form();
        }, false);

        document.addEventListener('wpcf7invalid', function (event) {
            jQuery('.loading-page').fadeOut(300);
            jQuery('html, body').animate({
                scrollTop: jQuery('.body-form-contact').offset().top - 100,
            }, 1000);
        }, false);
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MESSAGE ERROR CONTACT FORM
| -------------------------------------------------------------------------------------------------------------------------
*/
function message_error_contact_form() {
    jQuery('.page-template-page-contact').find('.wrap-popup-notification').addClass("active");
    jQuery('.wrap-popup-notification .container-popup-notification').find('.message').remove();
    jQuery('.page-template-page-contact').find('.wrap-popup-notification .container-popup-notification').append('<div class="message"><h3>Error!</h3><p>Something went wrong, please try again later.</p></div>');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK KONFIGURASI FORM SEARCH
| -------------------------------------------------------------------------------------------------------------------------
*/
function configure_form_search() {
    jQuery('.form-search button[type=submit]').click(function () {
        const form = jQuery(this).parents('form');
        const s = form.find('[name=s]').val();
        var e = 0;
        form.find('.required').removeClass('error');
        form.find('.required').each(function () {
            const val = jQuery(this).val();
            if (val == '') {
                const parent_form_input = jQuery(this).parent();
                parent_form_input.find("span").addClass("error");
                e++;
            }
        });
        if (e == 0) {
            return true;
        }
        return false;
    })
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK KONFIGURASI THANK YOU PAGE
| -------------------------------------------------------------------------------------------------------------------------
*/
function thankyou_page() {
    if (jQuery('.thankyou-page').length <= 0) {
        localStorage.boat_name = "";
        localStorage.departure_date = "";
        localStorage.itenerary = "";
        localStorage.cabin_type_value = "";
        localStorage.thankyou_page = "";
    }

    if (localStorage.boat_name != "" && localStorage.departure_date != "" && localStorage.itenerary != "" && localStorage.cabin_type_value != "") {
         if(localStorage.boat_name == "Mermaid I"){
			if(localStorage.cabin_type_value == 'Lower'){
				localStorage.cabin_type_value = 'Lower Deck';
			}
		 }else{
			 if(localStorage.cabin_type_value == 'Lower'){
				localStorage.cabin_type_value = 'Budget';
			}
		 }
		var message = ' ' + localStorage.boat_name + ' on ' + localStorage.departure_date + ' for ' + localStorage.itenerary + ' in our ' + localStorage.cabin_type_value + ' Cabin';
        jQuery('.thankyou-page p.text-thank-you').append(message);
    }

    if(jQuery('.thankyou-page').length > 0){
        // setTimeout(function(){
            jQuery('html, body').animate({
                scrollTop: jQuery('.wrap-static-content').position().top - 100,
            }, 1000);
        // }, 300);
    }

    
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK SETTING HEIGHT BG GREY
| -------------------------------------------------------------------------------------------------------------------------
*/
function setting_height_bg_grey() {
    if (width_screen <= 992) {
        jQuery('.mermaid-cabins').find('.bg-grey').css("height", '275px');
    } else {
        var height = jQuery('.mermaid-cabins').height() + 60;
        jQuery('.mermaid-cabins').find('.bg-grey').css("height", height);
    }
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK KONFIGURASI BUTTON CURRENCY NAVBAR
| -------------------------------------------------------------------------------------------------------------------------
*/
function currency_navbar_button() {
    jQuery('.currency-navbar').click(function () {
        if (jQuery('.wrap-list-currency').is(':visible')) {
            jQuery('.wrap-list-currency').fadeOut(300);
        } else {
            jQuery('.wrap-list-currency').fadeIn(300, function () {
                jQuery(this).find('ul li.list-currency-all, .list-currency-featured').click(function () {
                    const symbol_currency = jQuery(this).data("currency-symbol");
                });
            });
        }
    });
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK KONFIGURASI SUBSCRIBE NEWSLETTER
| -------------------------------------------------------------------------------------------------------------------------
*/
function subscribe_newsletter() {

	console.log(mc4wp);
	mc4wp.forms.on('3362.success', function(form, data) {
		
		var message = jQuery('.mc4wp-alert p').text();
		
		jQuery('.loading-page').fadeOut(300);
		jQuery('.wrap-popup-notification .container-popup-notification').find('.message').remove();
		jQuery('.wrap-popup-notification .container-popup-notification').append('<div class="message">'+message+'</div>');
		jQuery('[name=email]').val("");
		jQuery('.wrap-popup-notification').addClass('active');
	});
	
	mc4wp.forms.on('3362.error', function(form, data) {
		
		var message = jQuery('.mc4wp-alert p').text();
		
		jQuery('.loading-page').fadeOut(300);
		jQuery('.wrap-popup-notification .container-popup-notification').find('.message').remove();
		jQuery('.wrap-popup-notification .container-popup-notification').append('<div class="message">'+message+'</div>');
		jQuery('[name=email]').val("");
		jQuery('.wrap-popup-notification').addClass('active');
	});
	
	mc4wp.forms.on('3362.subscribed', function(form, data) {
		
		var message = jQuery('.mc4wp-alert p').text();
		
		jQuery('.loading-page').fadeOut(300);
		jQuery('.wrap-popup-notification .container-popup-notification').find('.message').remove();
		jQuery('.wrap-popup-notification .container-popup-notification').append('<div class="message">'+message+'</div>');
		jQuery('[name=email]').val("");
		jQuery('.wrap-popup-notification').addClass('active');
	});
	
	mc4wp.forms.on('3362.updated_subscriber', function(form, data) {
		
		var message = jQuery('.mc4wp-alert p').text();
		
		jQuery('.loading-page').fadeOut(300);
		jQuery('.wrap-popup-notification .container-popup-notification').find('.message').remove();
		jQuery('.wrap-popup-notification .container-popup-notification').append('<div class="message">'+message+'</div>');
		jQuery('[name=email]').val("");
		jQuery('.wrap-popup-notification').addClass('active');
	});
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENGECEK EMAIL VALID ATAU TIDAK
| -------------------------------------------------------------------------------------------------------------------------
*/
function valid_email(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENU MOBILE
| -------------------------------------------------------------------------------------------------------------------------
*/
function menu_mobile() {
    if (width_screen <= 1366) {
        jQuery('.burger-menu').click(function () {
            jQuery('body').addClass('full');
            jQuery('header').fadeIn(300, function () {
                jQuery(this).find('.close-popup').show();
            });
        });

        jQuery('header ul li.dropdown a').click(function () {
            jQuery('header ul li ul').slideUp(300);
            jQuery('header ul li').removeClass('active');
            const parent = jQuery(this).parent();
            setTimeout(function () {

                if (jQuery(parent).find('ul').is(':visible')) {
                    jQuery(parent).removeClass('active');
                    jQuery(parent).find('ul').slideUp();
                } else {
                    jQuery(parent).addClass('active');
                    jQuery(parent).find('ul').slideDown();
                }
            }, 300);
        });
    }
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK TABLE RESPONSIVE
| -------------------------------------------------------------------------------------------------------------------------
*/
function table_config_responsive() {
    jQuery('table.table-default tr').each(function () {
        var j = 1;
        jQuery(this).find('td').each(function () {
            var caption = jQuery("table.table-default tr th:nth-child(" + j + ")").text();
            jQuery(this).attr("data-heading", caption);
            j++;
        });
    });
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK CHECKOUT PAGE ORDER LIST
| -------------------------------------------------------------------------------------------------------------------------
*/
function checkout_page_order_list(){
    jQuery('.wrap-list-order').click(function(){
        // var parent = jQuery(this).parents('.wrap-list-order');
        var parent = jQuery(this);
        if(jQuery(parent).find('.body-list-order').is(':visible')){
            jQuery(parent).find('.body-list-order').slideUp(300);

            setTimeout(function(){
                jQuery(parent).find('.hide-when-expand').addClass('active');
                jQuery(parent).find('.show-when-expand').removeClass('active');
            }, 150);
        }else{
            jQuery(parent).find('.body-list-order').slideDown(300);

            setTimeout(function(){
                jQuery(parent).find('.hide-when-expand').removeClass('active');
                jQuery(parent).find('.show-when-expand').addClass('active');
            }, 150)
        }
        return false;
    });  
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INFINITE SCROLL
| -------------------------------------------------------------------------------------------------------------------------
*/
function infinite_scroll_config(){
    if(jQuery('.infinite-scrolling').length > 0){
        $list_news = jQuery('.infinite-scrolling');
        $list_news.infiniteScroll({
            // options
            path: '.paging-desktop .next',
            hideNav: '.paging-desktop',
            history: false,
        });

        $list_news.on('load.infiniteScroll', function (event, response, path) {
            var $items = jQuery(response).find('.list-news-page');
            $list_news.append($items);
            fancybox_initialize();
            config_lazy();
        });
    }
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK DATEPICKER
| -------------------------------------------------------------------------------------------------------------------------
*/
function datepicker_config(){
    if(jQuery( ".datepicker" ).length > 0){
        jQuery( ".datepicker" ).datepicker({
            dateFormat: 'dd MM yy',
            changeMonth: true,
            changeYear: true,
        });
    }
	
	if(jQuery( "#date-of-birth" ).length > 0){
		 jQuery( "#date-of-birth" ).datepicker({
            dateFormat: 'dd MM yy',
            changeMonth: true,
            yearRange: '-100:+0',
            changeYear: true,
			defaultDate: new Date(2000, 00, 01)
        });
	}
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION JIKA ADA PERUBAHAN PADA SPECIAL MEAL
| -------------------------------------------------------------------------------------------------------------------------
*/
function special_meal_change(){
    jQuery('#special-meal').change(function(){
        const val = jQuery(this).val();
        if(val == "Other Remarks"){
            jQuery('.other-remarks').slideDown(300);
        }else{
            jQuery('.other-remarks').slideUp(300);
            jQuery('#remarks-meal').val("");
        }
    });
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION JIKA ADA LINK di semua textarea
| -------------------------------------------------------------------------------------------------------------------------
*/

function remove_url_textarea(){
	var url = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
	
	 jQuery("#your_message, #additional_note").on("keyup", function( e ) {
		var urls, output = "";
        if ( e.keyCode !== 8 && e.keyCode !== 9 && e.keyCode !== 13 && e.keyCode !== 32 && e.keyCode !== 46  ) {
            // Return is backspace, tab, enter, space or delete was not pressed.
            return;
        }

        while ((urls = url.exec(this.value)) !== null) {
           $('#modal_textarea').modal('show');
		   jQuery(this).val('');
		   
        }
        console.log("URLS: " + output.substring(0, output.length - 2));
	 })
	 
	 jQuery("#your_message, #additional_note").on("mouseout", function() {
		var urls, output = "";


        while ((urls = url.exec(this.value)) !== null) {
           $('#modal_textarea').modal('show');
		   jQuery(this).val('');
		   
        }
        console.log("URLS: " + output.substring(0, output.length - 2));
	 })
}

function remove_autocomplete_contactForm(){
	jQuery(".wpcf7-text").attr('autocomplete','off');
}

function enrollment_form_rental_equipment(){
	
	var bcd 	= jQuery(".rental_equipment .box-checkbox .bcd").find('.wpcf7-checkbox');
	var wetsuit = jQuery(".rental_equipment .box-checkbox .wetsuit").find('.wpcf7-checkbox');
	var fins 	= jQuery(".rental_equipment .box-checkbox .fins").find('.wpcf7-checkbox');
	
	 jQuery('#rental_check .checkbox-master').click(function(e){
		
		if(jQuery(this).prop("checked") == true){
			// jQuery( ".rental_equipment" ).show( "slow" );
			// jQuery( ".rental_equipment" ).removeClass( "wpcf7cf-hidden" );
			 jQuery("#rental_check .checkbox-single").prop('checked', false); 
			
			// bcd.addClass('wpcf7-validates-as-required');
			// wetsuit.addClass('wpcf7-validates-as-required');
			// fins.addClass('wpcf7-validates-as-required');
			
		 }
		 // else if(jQuery(this).prop("checked") == false){
			 // jQuery( ".rental_equipment" ).slideUp( "slow" );
			// jQuery( ".rental_equipment" ).addClass( "wpcf7cf-hidden" );
			// bcd.removeClass('wpcf7-validates-as-required');
			// bcd.removeClass('wpcf7-not-valid');
			// wetsuit.removeClass('wpcf7-validates-as-required');
			// fins.removeClass('wpcf7-validates-as-required');
		 // }
		
		
	 });
	
	jQuery('#rental_check .checkbox-single').click(function(){
		if(jQuery(this).prop("checked") == true){
		   // jQuery( ".rental_equipment" ).slideUp( "slow" );
			// jQuery( ".rental_equipment" ).addClass( "wpcf7cf-hidden" );
			jQuery("#rental_check .checkbox-master").prop('checked', false); 
			
			// bcd.removeClass('wpcf7-validates-as-required');
			// bcd.removeClass('wpcf7-not-valid');
			// wetsuit.removeClass('wpcf7-validates-as-required');
			// fins.removeClass('wpcf7-validates-as-required');
		}
	});
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Payment option behaviour on change
| -------------------------------------------------------------------------------------------------------------------------
*/
function checkout_payment_option()
{
    jQuery('[name=payment]').prop('checked', false);
    jQuery('[name=payment]').on('change', function() {
        var payment  = jQuery(this).val();
        var selector = jQuery('.wrap-button-continue h4');

        if( payment == 'cc' )
        {
            var currency = jQuery('[name=cc_currency]').val();
            var values   = selector.data('amount-' + currency + '-with-charge');
        } 
        else if( payment == 'paypal' )
        {
            var currency = jQuery('[name=paypal_currency]').val();
            var values   = selector.data('amount-' + currency + '-with-charge');
        }
        else
        {
            var currency = jQuery('[name=bank_transfer_currency]').val();
            var values   = selector.data('amount-' + currency + '-no-charge');
        }

        jQuery('.wrap-button-continue h4').text(values);

        if( jQuery('.wrap-choose-payment-type .list-choose-payment').length > 0 )
        {
            jQuery('.wrap-choose-payment-type .list-choose-payment .intructions').slideUp(300);
            jQuery('.wrap-choose-payment-type .list-choose-payment .' + payment + '-inst').slideDown(300);
        }
    });

	jQuery('[name=cc_currency]').on('change', function(){
		var currency = jQuery(this).val();
		var selector = jQuery('.wrap-button-continue h4');
        var values   = selector.data('amount-' + currency + '-with-charge');

		selector.text( values );
	});
	
    jQuery('.wrap-button-continue button').on('click', function(){
    	var payment = jQuery('.container-payment-option [name=payment]:checked').val();

	    if( payment == '' || typeof payment == 'undefined' )
	    {
	    	var content = '<div class="message"><p>Please select your payment option.</p></div>';
	    	var popup   = jQuery('.wrap-popup-notification');

	        popup.find('.container-popup-notification .message').remove();
	        popup.find('.container-popup-notification').append( content );
	        popup.addClass('active');

	        jQuery('body').addClass('full');

	    	return false;
	    }
    });
}