var locations = jQuery('[name=json_dive_spot]').val();
locations = JSON.parse(locations);

const gmarkerss = [];
var imarkers = [];
var globalClose = [];
var width_screen = window.innerWidth;

/*
| -------------------------------------------------------------------------------------
| Inisialisasi Google Maps
| -------------------------------------------------------------------------------------
*/
function initialize() {
    // if (jQuery('#map').length > 0) {
    var mapDiv = document.getElementById('map');
    var latitude = jQuery('[name=destination_latitude]').val();
    var longitude = jQuery('[name=destination_longitude]').val();
    var zoom_level = jQuery('[name=destination_zoom_level]').val();

    if (zoom_level == "") {
        zoom_level = 9;
    }

    var zoom = parseInt(zoom_level);
    var latLng = new google.maps.LatLng(latitude, longitude);

    map = new google.maps.Map(mapDiv, {
        center: latLng,
        zoom: zoom,
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        navigationControl: true,
        navigationControlOptions: {
            style: google.maps.NavigationControlStyle.SMALL,
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },
        scaleControl: true,
        scaleControlOptions: {
            position: google.maps.ControlPosition.BOTTOM_LEFT
        },
        scrollwheel: false,
        draggable: true,
        styles: [{
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#aab5bb"
                }]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#555555"
                }]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#555555"
                }]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#f6fafd"
                }]
            }
        ]
    });


    var infowindow = new google.maps.InfoWindow({
        maxWidth: 200
    });

    var marker, i;
    var closeMarker;

    var currentMark;
    var currenti;
    var gmark = [];

    for (i = 0; i < locations.length; i++) {
        var icon = {
            url: locations[i]['icon_marker'],
        }

        var marker = new google.maps.Marker({
            position: {
                lat: parseFloat(locations[i]['latitude']),
                lng: parseFloat(locations[i]['longitude'])
            },
            map: map,
            zIndex: 0,
            icon: icon
        });

        gmarkerss.push(marker);
        gmark.push(marker);
        imarkers.push(i);

        google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {

                for (j = 0; j < gmark.length; j++) {
                    var icon = {
                        url: locations[j]['icon_marker'],
                    }

                    var icon_active = {
                        url: locations[j]['icon_marker_active'],
                    }

                    if (locations[j]['marker']) {
                        if (gmark[j] != marker) {
                            gmark[j].setIcon(icon);
                        } else {
                            gmark[i].setIcon(icon_active);
                        }
                    } else {
                        if (gmark[j] != marker) {
                            gmark[j].setIcon(icon);
                        } else {
                            gmark[i].setIcon(icon_active);
                        }
                    }
                }


                jQuery('.location-list').removeClass('active');
                jQuery('#' + i).addClass('active');
                value = jQuery('.location-list.active').attr('id');

                var contentString = '<div id="gmaps-content-landing">';

                if (locations[i]['image'] != "") {
                    contentString += '<div class="image-map">' +
                        '<img src="' + locations[i]['image'] + '" />' +
                        '</div>';
                }

                contentString += '<div class="container-desc">' +
                    '<div class="title">' + locations[i]['title'] + '</div>' +
                    '<div class="content-maps">' + locations[i]['content'] + '</div>' +
                    '<div class="link-read-more"><a class="button-arrow" href="' + locations[i]['link'] + '">Schedule & Rates</a></div>' +
                    '</div>' +
                    '</div>';


                infowindow.setContent(contentString);
                infowindow.open(map, marker);
                closeMarker = infowindow;
                globalClose.push(closeMarker);
                currentMark = this;
                currenti = i;

            }
        })(marker, i));
    }

    google.maps.event.addListener(infowindow, 'closeclick', function () {
        if (locations[currenti]['icon_marker']) {
            var map_icon = locations[currenti]['icon_marker'];
            var icon = {
                url: map_icon,
            }
            currentMark.setIcon(icon);
            marker.setZIndex(0);
            jQuery('#' + currenti).removeClass('active');
        } else {
            currentMark.setIcon();
            marker.setZIndex(0);
        }
    });
    // }
}



jQuery(document).ready(function () {
    append_data();
    button_choose_map_location();
});



/*
| -------------------------------------------------------------------------------------
| Function Untuk Set Data Location List
| -------------------------------------------------------------------------------------
*/
function append_data() {
    jQuery('.container-location-list .scrollbar-macosx').empty();
    var number = 1;
    for (var i = 0; i < locations.length; i++) {
        jQuery('.container-location-list .scrollbar-macosx').append("<div id='" + i + "' class='location-list " + i + "'>" + number + ". " + locations[i]['title'] + "</div></div>");
        number++;
    }
}

/*
| -------------------------------------------------------------------------------------
| Function jika list location side di klik
| -------------------------------------------------------------------------------------
*/
function button_choose_map_location() {
    jQuery(document).on('click', '.location-list', function (event) {
        event.preventDefault();
        jQuery('.location-list').removeClass('active');
        jQuery(this).addClass('active');

        closeWindow();

        var value = jQuery(this).attr('id');
        var closedSelectetd = [];

        var infowindow = new google.maps.InfoWindow({
            maxWidth: 200
        });


        var currentMark;
        var currenti;
        for (i = 0; i < locations.length; i++) {
            marker = gmarkerss[i];

            if (i == value) {
                var icon_active = {
                    url: locations[i]['icon_marker_active'],
                }

                marker.setIcon(icon_active);
                marker.setZIndex(3);

                var contentString = '<div id="gmaps-content-landing">';

                if (locations[i]['image'] != "") {
                    contentString += '<div class="image-map">' +
                        '<img src="' + locations[i]['image'] + '" />' +
                        '</div>';
                }

                contentString += '<div class="container-desc">' +
                    '<div class="title">' + locations[i]['title'] + '</div>' +
                    '<div class="content-maps">' + locations[i]['content'] + '</div>' +
                    '<div class="link-read-more"><a class="button-arrow" href="' + locations[i]['link'] + '">Schedule & Rates</a></div>' +
                    '</div>' +
                    '</div>';

                infowindow.setContent(contentString);
                infowindow.open(map, marker);
                closeMarker = infowindow;
                globalClose.push(closeMarker);

                currentMark = marker;
                currenti = i;
            } else {
                var icon = {
                    url: locations[i]['icon_marker'],
                }
                marker.setIcon(icon);
                marker.setZIndex(1);
            }
        }

        if (width_screen <= 480) {
            jQuery('html, body').animate({
                scrollTop: jQuery('.dive-spot-wrap').position().top,
            }, 500);
        }

        google.maps.event.addListener(infowindow, 'closeclick', function () {
            // console.log(currentMark);
            var icon = {
                url: locations[currenti]['icon_marker']
            }
            currentMark.setIcon(icon);
            currentMark.setZIndex(1);
        });

    });
}

function closeWindow() {
    var closeTheWindows;

    if (globalClose.length != 0) {
        for (var i = 0; i < globalClose.length; i++) {
            closeTheWindows = globalClose[i];
            closeTheWindows.close();
        }
    }
}