jQuery(document).ready(function(){
    jQuery('.field_date').datepicker({
		dateFormat : 'dd-mm-yy',
		onClose: function() {        
			jQuery(this).trigger('blur');
		}
	});
});