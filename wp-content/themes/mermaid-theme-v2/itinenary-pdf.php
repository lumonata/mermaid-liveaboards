<?php

require_once( '../../../wp-load.php' );
require_once 'includes/dompdf/autoload.inc.php';

use Dompdf\Dompdf;

$dompdf = new Dompdf();

$args  = array( 'post_status' => 'publish', 'post_type' => 'iteneraries', 'p' => $_GET['id'], 'posts_per_page' => 1 );
$query = new WP_Query( $args );

if( $query->have_posts() )
{
    while ( $query->have_posts() )
    {
        $query->the_post();

        $int_id    = get_the_ID();
        $int_title = get_the_title();
        $int_slug  = sanitize_title( $int_title );

        $args2  = array( 'post_status' => 'publish', 'post_type' => 'boat', 'p' => $_GET['p'], 'posts_per_page' => 1 );
        $query2 = new WP_Query( $args2 );

        if( $query2->have_posts() )
        {
            $i = 0;

            while ( $query2->have_posts() )
            {
                $query2->the_post();

                $boat_id    = get_the_ID();
                $boat_title = get_the_title();
                $boat_slug  = sanitize_title( $boat_title );
                $table_int  = get_itenerary_data_table( $int_id, $boat_id, $int_title );

                $class = $i == 0 ? 'active' : '';

                $itenerary_data[] = array(
                    'html'     => $table_int['html'],
                    'data'     => $table_int['data'],
                    'class'    => $class,
                    'sanitize' => $boat_slug,
                    'title'    => $boat_title
                );

                $total_dives_boat = $table_int['total_dives'];

                $i++;
            }
        }
    }

    wp_reset_postdata();
}

if( !empty( $itenerary_data ) )
{
    $mermaid_other_key = $key == 0 ? $key + 1 : $key - 1;

    $html        = '';
    $items       = '';
    $total_dives = 0;

    $classes = array( 'tg-8vkm','tg-6sgx' );

    foreach( $itenerary_data as $key => $value )
    {
        foreach( $value['data'] as $itenerary )
        {
            $count_dive = $itenerary['dives'];
            $classest   = $classes[ $i++ % 2 ];

            if( $count_dive == 0 )
            {
                $dives = '-';
            }
            else
            {
                $dives = $itenerary['dives'];
            }

            $total_dives += $count_dive;

            $items .= '
			<tr>
				<td class="' . $classest . '" style="text-align:center;text-transform:uppercase;color: #001946;font-size: 25px;font-weight:500;line-height:25px">' . $itenerary['day'] . '</td>
				<td class="' . $classest . '" style="text-align:left;color: #545454;font-size: 13px;line-height:27px">' . $itenerary['description'] . '</td>
				<td class="' . $classest . '" style="text-align:center;color: #001946;font-size: 14px;line-height:25px"> dives <br><br><span style="font-size: 30px;">' . $dives . '</span></td>
			</tr>';
        }
    }

    $dives_count = $total_dives > 0 ? $total_dives . ' ' : $total_dives . ' ';

    $html .= '
	<html>
		<head>
			<meta http-equiv="Content-Type" content="charset=utf-8" />
			<style type="text/css">
				@page {
					margin:0px;
					padding:0px;
				}
				* { padding: 0; margin: 0; }
				 html{margin:40px 50px}
				.tg  {border-collapse:collapse;border-spacing:0;border-color:#fff;}
				.tg td{font-family:Arial, sans-serif;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-top-width:1px;border-bottom-width:1px;border-color:#fff;color:#669;background-color:#e8edff;}
				.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-top-width:1px;border-bottom-width:1px;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
				.tg .tg-hmp3{background-color:#D2E4FC;text-align:left;vertical-align:top}
				.tg .tg-baqh{text-align:center;vertical-align:top}
				.tg .tg-rlkd{background-color:#001b48;color:#ffffff;text-align:center;vertical-align:top;font-size: 16px;padding-top:20px;padding-bottom:20px;font-weight:bold}
				.tg .tg-j0tj{background-color:#D2E4FC;text-align:center;vertical-align:top}
				.tg .tg-0lax{text-align:left;vertical-align:top}
				.tg .tg-8vkm{background-color:#f4f8fb;text-align:center;vertical-align:top;font-size: 12px}
				.tg .tg-6sgx{background-color:#ffffff;text-align:left;vertical-align:top;font-size: 12px}
			</style>
		</head>
		<body>
			<table class="tg">
				<tr>
					<th class="tg-rlkd" width="15%" style="text-align:center;text-transform:uppercase">Day</th>
					<th class="tg-rlkd" width="75%" style="text-align:center;text-transform:uppercase">Itinerary with MV ' . $boat_title . ' ' . $int_title . '</th>
					<th class="tg-rlkd" width="10%" style="text-align:center;text-transform:uppercase">Dives</th>
				</tr>
				' . $items . '
				<tr>
					<th class="tg-rlkd" colspan="3" style="text-align:center;font-size:14px">TOTAL DIVES : ' . $dives_count . '</th>
				</tr>
			</table>
		</body>
	</html>';

    $dompdf->loadHtml( $html );

    $dompdf->setPaper( 'A4', 'potrait' );

    $dompdf->render();

    $dompdf->stream( $int_slug . '-itinerary-boat-' . $boat_slug . '-table.pdf' );
}
