<?php get_header(); ?>

<?php include_once "layout/hero.php"; ?>

 <!-- SECTION WELCOME -->
<section class="welcome-about welcome-mermaid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php $title = single_cat_title( '', false ); ?>
                <h1 class="heading-default"><?= $title == 'Trip Reports' ? $title : sprintf( '%s - Trip Reports', $title ) ?></h1>                
            </div>
        </div>
    </div>
</section>

<section class="wrap-list-news">
    <div class="container-mermaid">
        <div class="row">
            <?php

            $q = new WP_Query( array(
                'orderby'           => 'publish_date',
                'post_status'       => 'publish',
                'post_type'         => 'post',
                'order'             => 'DESC',
                'category__in'      => $term_id,
                'posts_per_page'    => 2,
            ));

            ?>

            <?php while ( $q->have_posts() ): $q->the_post(); ?>
            <div class="col-md-6 list-news">
                <?php

                $youtube_video = get_post_meta( $post->ID, '_post_youtube_video', 1 );
                $medium_image  = get_the_post_thumbnail_url( $post->ID, 'medium_large');
                $large_image   = get_the_post_thumbnail_url( $post->ID, 'large');

                ?>

                <?php if( !empty($youtube_video ) ): ?>
                <div class="container-youtube-video">
                    <a href="<?php echo $youtube_video; ?>" class="fancy-group">
                        <div class="thumb-img">
                            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $large_image ?>" data-src-small="<?php echo $medium_image ?>" alt="">
                            <div class="icon-play" data-play="0" data-youtube-code="<?php echo $youtube_video; ?>"></div>
                        </div>
                    </a>
                    <div class="video-background" data-play="0" data-youtube-code="<?php echo $youtube_video; ?>">
                        <div class="video-foreground"></div>
                    </div>
                </div>
                <?php else: ?>
                <a href="<?php the_permalink() ?>">
                    <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $large_image ?>" data-src-small="<?php echo $medium_image ?>" alt="">
                </a>
                <?php endif; ?>

                <div class="detail-list">
                    <!-- <h5><?php the_date() ?></h5> -->
                    <a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><h3><?php the_title() ?></h3></a>
                    <?php the_excerpt() ?>
                    <!-- <h6>by <?php echo ucwords(get_the_author()); ?></h6> -->
                </div>
            </div>
            <?php endwhile; ?>

        </div>

        <div class="row list-blue-default infinite-scrolling">
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-md-3 list-news-page">
                <?php

                $youtube_video = get_post_meta( $post->ID, '_post_youtube_video', 1 );
                $medium_image  = get_the_post_thumbnail_url( $post->ID, 'medium_large');
                $large_image   = get_the_post_thumbnail_url( $post->ID, 'large');

                ?>

                <?php if( !empty($youtube_video ) ): ?>
                <div class="container-youtube-video">
                    <a href="<?php echo $youtube_video; ?>" class="fancy-group">
                        <div class="thumb-img">
                            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $large_image ?>" data-src-small="<?php echo $medium_image ?>" alt="">
                            <div class="icon-play" data-play="0" data-youtube-code="<?php echo $youtube_video; ?>"></div>
                        </div>
                    </a>
                    <div class="video-background" data-play="0" data-youtube-code="<?php echo $youtube_video; ?>">
                        <div class="video-foreground"></div>
                    </div>
                </div>
                <?php else: ?>
                <a href="<?php the_permalink() ?>">
                    <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $large_image ?>" data-src-small="<?php echo $medium_image ?>" alt="">
                </a>
                <?php endif; ?>

                <div class="container-desc">
                    <a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><h3><?php the_title() ?></h3></a>
                    <?php the_excerpt() ?>
                </div>
            </div>
            <?php endwhile; ?>

            <div class="paging paging-desktop col-12">
                <?php 

                the_posts_pagination( array(
                    'screen_reader_text' => __( ' ', 'mermaid' ),
                    'prev_text' => __( 'PREV', 'mermaid' ),
                    'next_text' => __( 'NEXT', 'mermaid' ),
                    'mid_size'  => 2,
                ));

                ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>