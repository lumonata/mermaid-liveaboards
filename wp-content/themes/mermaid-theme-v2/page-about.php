<?php
/*
Template Name: About Page
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>

    <?php include_once "layout/hero.php"; ?>

    <?php
        // INISIALISASI VARIBLE PAGE
        $title         = get_the_title();
        $title         = empty($title) ? '':   '<h1 class="heading-default">'.$title.'</h1>';
        $content       = get_the_content();
        $content       = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $content);
        $content       = apply_filters('the_content', $content);
        $content       = str_replace(']]>', ']]&gt;', $content);
        $content       = empty($content) ? '': '<div class="desc-about">'.$content.'</div>';
        $section_image = wp_get_attachment_image_url(get_post_meta(get_the_ID(), '_page_image_section_id', true), 'large');
        $ceo_name      = get_post_meta(get_the_ID(), '_page_ceo_name', true);
        $director_name = get_post_meta(get_the_ID(), '_page_director_name', true);
        $title_dives   = get_post_meta(get_the_ID(), '_page_title_dive_areas', true);
        $desc_dives    = get_post_meta(get_the_ID(), '_page_description_dive_areas', true);
    ?>

    <!-- SECTION WELCOME -->
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        echo $title;
                        echo $content;
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="wrap-ceo-director">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 con-img-cd">
                <img src="<?php echo $section_image; ?>" />
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 con-text-cd">
                <div class="text-ceo-director">
                    <h2>CEO</h2>
                    <h4><?php echo $ceo_name; ?></h4>
                    <h2>Director</h2>
                    <h4><?php echo $director_name; ?></h4>
                </div>
            </div>
        </div>
    </section>

    <!-- SECTION WELCOME -->
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        echo '<h1 class="heading-default">'.$title_dives.'</h1>';
                        echo $desc_dives;
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>

<?php get_footer(); ?>