<?php get_header(); ?>

<?php

session_start();

$_SESSION['boat_name'] = $_GET['s'] ;
$_SESSION['destinasi'] = $_GET['dest'] ;

?>

<?php include_once "layout/hero.php"; ?>
<?php include_once "layout/inquiry-form-popup.php"; ?>
	
<!-- SECTION SCHDULE ITENERARIES -->
<section class="wrap-schedule-rate-table">
    <div class="container-mermaid">
        <?php table_schedule_and_rate( 'search' ); ?>
    </div>
</section>

<?php get_footer(); ?>