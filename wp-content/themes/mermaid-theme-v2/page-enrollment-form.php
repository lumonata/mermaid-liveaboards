<?php
/*
Template Name: Enrollment Form Page
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php include_once "layout/hero.php"; ?>

    <section class="page-default page-static page-enrollment">
        <div class="container">

            <?php
            $shortcode_contact  = get_post_meta( get_the_ID(), '_page_shortcode_contact', 1 );
            $post_content       = get_the_content();
            $post_content       = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
            $post_content       = apply_filters('the_content', $post_content);
            $post_content       = str_replace(']]>', ']]&gt;', $post_content);
            ?>

            <h1><?php the_title(); ?></h1>
            <!-- <div class="description-page"><?php echo $post_content; ?></div> -->

            <div class="container-form-contact">
                <div class="top-form-contact">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Enrolment Form</h2>
                            <?php
                                $enable_download_file   = get_post_meta( get_the_ID(), '_page_enable_download_file', 1 );
                                $file_pdf               = get_post_meta( get_the_ID(), '_page_pdf_file', 1 );

                                if($enable_download_file && !empty($file_pdf)):
                                    echo '<a href="'.$file_pdf.'" target="_blank" class="btn-download-file">Download File PDF</a>';
                                endif;
                            ?>
                        </div>
                    </div>
                </div>

                <div class="body-form-contact">
                    <?php echo do_shortcode($shortcode_contact); ?>

                    <input type="hidden" name="thankyou_link" value="<?php echo get_site_url().'/your-information-has-been-sent/'; ?>" />
                </div>
            </div>

        </div>
    </section>
    
    <input type="hidden" name="message_popup_success" value="<?php echo get_option('message_enrollment_form_popup'); ?>"  />

<?php endwhile;  ?>

<?php get_footer(); ?>