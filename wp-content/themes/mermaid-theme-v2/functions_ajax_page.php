<?php

// /*
// | -------------------------------------------------------------------------------------
// | Function ajax untuk admin page menambahkan itenerary
// | -------------------------------------------------------------------------------------
// */
// add_action( 'wp_ajax_add-itenerary', 'add_itenerary_admin' );
// function add_itenerary_admin()
// {
//     $result['status']   = "failed";
//     $result['msg']      = "<h3>Something went wrong, please try again later</h3>";

//     if(isset($_POST['action']) && $_POST['action'] == "add-itenerary"):
//         $day        = $_POST['day'];
//         $sub_day    = $_POST['sub_day'];
//         $detail     = $_POST['detail'];
//         $dives      = $_POST['dives'];
//         $dest_id    = $_POST['dest_id'];
//         $post_id    = $_POST['post_id'];

//         if ( !metadata_exists( 'post', $post_id, '_itenerary_data_'.$dest_id ) ):
//             $data[] = array(
//                 'day'       => $day,
//                 'sub_day'   => $sub_day,
//                 'detail'    => $detail,
//                 'dives'     => $dives,
//             );
//             $json_data = json_encode($data);
//             add_post_meta($post_id, '_itenerary_data_'.$dest_id, $json_data);
//         else:
//             $itenerary_data     = get_post_meta($post_id, '_itenerary_data_'.$dest_id, true);
//             $itenerary_data_db  = !empty($itenerary_data) ? json_decode($itenerary_data, true) : '';
//             $count_itenerary    = !empty($itenerary_data_db) ? count($itenerary_data_db) : 0;

//             if($count_itenerary > 0):
//                 $data[$count_itenerary] = array(
//                     'day'       => $day,
//                     'sub_day'   => $sub_day,
//                     'detail'    => $detail,
//                     'dives'     => $dives,
//                 );
//                 $itenerary_data_new = $itenerary_data_db;
//                 $itenerary_data_new += $data;
//                 $json_data          = json_encode($itenerary_data_new);
//                 update_post_meta($post_id, '_itenerary_data_'.$dest_id, $json_data);
//             endif;
//         endif;
//     endif;

//     wp_send_json($result);
// }


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk filter year pada schedule & currency
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_choose-filter-schedule', 'filter_schedule_data' );
add_action( 'wp_ajax_nopriv_choose-filter-schedule', 'filter_schedule_data' );

function filter_schedule_data()
{
    if( isset( $_POST['action'] ) && $_POST['action'] == 'choose-filter-schedule' )
    {
        $dest            = isset( $_POST['dest'] ) ? $_POST['dest'] : '';
        $year            = isset( $_POST['year'] ) ? $_POST['year'] : '';
        $month           = isset( $_POST['month'] ) ? $_POST['month'] : '';
        $page_now        = isset( $_POST['page_now'] ) ? $_POST['page_now'] : '';
        $year_month      = isset( $_POST['year_month'] ) ? $_POST['year_month'] : '';
        $iteneraries     = isset( $_POST['iteneraries'] ) ? $_POST['iteneraries'] : '';
        $mermaid_type    = isset( $_POST['mermaid_type'] ) ? $_POST['mermaid_type'] : '';
        $code_currency   = isset( $_POST['code_currency'] ) ? $_POST['code_currency'] : '';
        $symbol_currency = isset( $_POST['symbol_currency'] ) ? $_POST['symbol_currency'] : '€';
        $prefix          = '_schedule_data_';

        $meta_query_boat               = array();
        $meta_query_year               = array();
        $meta_query_iteneraries        = array();
        $meta_query_year_month         = array();
        $meta_query_iteneraries_search = array();

        if( !empty( $code_currency ) && $code_currency != 'EUR' )
        {
            $price_currency = convertCurrency( 'EUR', $code_currency );
        }
        else
        {
            $price_currency = 1;
        }

        $meta_query_general = array(
            array(
                'key'     => '_schedule_data_departure_date',
                'value'   => date( 'Y-m-d' ),
                'type'    => 'DATE',
                'compare' => '>='
            )
        );

        if( !empty( $iteneraries ) && $iteneraries != 'all' )
        {
            $meta_query_iteneraries = array(
                array(
                    'key'     => '_schedule_data_iteneraries',
                    'value'   => $iteneraries,
                    'compare' => '=',
                )
            );
        }

        if( !empty( $year ) && !empty( $month ) )
        {
            $meta_query_year = meta_query_year_month_search( '_schedule_data_', $_POST );
        }
        elseif( !empty( $year ) && $year != 'all' )
        {
            $meta_query_year = array(
                array(
                    'key'   => '_schedule_data_departure_date',
                    'value' => array(
                        $year . '-01-01',
                        $year . '-12-31',
                    ),
                    'compare' => 'BETWEEN'
                )
            );
        }

        if( !empty( $mermaid_type ) )
        {
            $meta_query_boat = array(
                array(
                    'key'     => '_schedule_data_boat',
                    'value'   => $mermaid_type,
                    'compare' => '=',
                )
            );
        }

        if( !empty( $year_month ) )
        {
            $meta_query_year_month = array(
                array(
                    'key'     => '_schedule_data_departure_date',
                    'value'   => $year_month,
                    'compare' => '>=',
                )
            );
        }

        if( !empty( $dest ) )
        {
            $iteneraries_list = get_iteneraries_list( $dest );

            $query_arr = array();

            if( !empty( $iteneraries_list ) )
            {
                foreach( $iteneraries_list as $d )
                {
                    $query_arr[] = array(
                        'key'     => '_schedule_data_iteneraries',
                        'value'   => $d['id'],
                        'compare' => '=',
                    );
                }
            }

            $meta_query_iteneraries_search[] = array_merge( array( 'relation' => 'or' ), $query_arr );
        }

        $roles_agent = 0;

        if( is_user_logged_in() )
        {
            $current_user = wp_get_current_user();
            $user_login   = $current_user->user_login;
            $user_roles   = $current_user->roles;
            $roles        = $user_roles[0];

            if( $roles == 'agent' )
            {
                $roles_agent = 1;
            }
        }

        $args = array(
            'meta_key'    => '_schedule_data_departure_date',
            'post_type'   => 'schedule_data',
            'orderby'     => 'meta_value',
            'post_status' => 'publish',
            'order'       => 'ASC',
            'numberposts' => -1,
            'meta_query'  => array_merge(
                $meta_query_general,
                $meta_query_iteneraries,
                $meta_query_iteneraries_search,
                $meta_query_year,
                $meta_query_boat,
                $meta_query_year_month
            )
        );

        $query = get_posts( $args );

        if( count( $query ) == 0 )
        {
            $meta_query_year = meta_query_year_month_search( '_schedule_data_', $_GET, 1 );

            $args = array(
                'meta_key'    => '_schedule_data_departure_date',
                'post_type'   => 'schedule_data',
                'orderby'     => 'meta_value',
                'post_status' => 'publish',
                'order'       => 'ASC',
                'numberposts' => -1,
                'meta_query'  => array_merge(
                    $meta_query_general,
                    $meta_query_boat,
                    $meta_query_iteneraries,
                    $meta_query_iteneraries_search,
                    $meta_query_year
                )
            );

            $query = get_posts( $args );
        }

        $all_boat    = array();
        $html        = '';
        $html_mobile = '';
        $i           = 0;

        // =========== LABEL STRING ===========
        $trip_details_text = get_label_string( 'Trip Details', true );
        $trip_code_text    = get_label_string( 'Trip Code', true );
        $itenerary_text    = get_label_string( 'Itenerary', true );
        $book_now_text     = get_label_string( 'Book Now', true );
        $duration_text     = get_label_string( 'Duration', true );
        $master_text       = get_label_string( 'Master', true );
        $single_text       = get_label_string( 'Single', true );
        $deluxe_text       = get_label_string( 'Deluxe', true );
        $budget_text       = get_label_string( 'Budget', true );
        $status_text       = get_label_string( 'Status', true );
        $return_text       = get_label_string( 'Return', true );
        $lower_deck_text   = get_label_string( 'Lower', true );
        $date_text         = get_label_string( 'Date', true );

        if( !empty( $query ) )
        {
            foreach( $query as $d )
            {
                $post_id        = $d->ID;
                $post_title     = $d->post_title;
                $sanitize_title = sanitize_title( $post_title );

                //-- GET DATE
                $depart_date           = get_post_meta( $post_id, $prefix . 'departure_date', true );
                $return_date           = get_post_meta( $post_id, $prefix . 'arrival_date', true );
                $strtotime_depart_date = strtotime( $depart_date );
                $strtotime_return_date = strtotime( $return_date );
                $start                 = new DateTime( $depart_date );
                $end                   = new DateTime( $return_date );
                $result_date           = $end->diff( $start );
                $night                 = $result_date->d;
                $day                   = $night + 1;

                //-- PRICE DATA
                $price_master = get_post_meta( $post_id, $prefix . 'price_master', true );
                $price_single = get_post_meta( $post_id, $prefix . 'price_single', true );
                $price_deluxe = get_post_meta( $post_id, $prefix . 'price_deluxe', true );
                $price_lower  = get_post_meta( $post_id, $prefix . 'price_lower', true );

                //-- DISCOUNT DATA
                $price_master_dsc = get_post_meta( $post_id, $prefix . 'price_master_dsc', true );
                $price_single_dsc = get_post_meta( $post_id, $prefix . 'price_single_dsc', true );
                $price_deluxe_dsc = get_post_meta( $post_id, $prefix . 'price_deluxe_dsc', true );
                $price_lower_dsc  = get_post_meta( $post_id, $prefix . 'price_lower_dsc', true );

                //-- ALLOTMENT DATA
                if( is_user_logged_in() )
                {
                    $allotment_master = get_post_meta( $post_id, $prefix . 'allotment_master', true );
                    $allotment_single = get_post_meta( $post_id, $prefix . 'allotment_single', true );
                    $allotment_deluxe = get_post_meta( $post_id, $prefix . 'allotment_deluxe', true );
                    $allotment_lower  = get_post_meta( $post_id, $prefix . 'allotment_lower', true );
                }

                //-- Iteneraries
                $iteneraries_id   = get_post_meta( $post_id, $prefix . 'iteneraries', true );
                $iteneraries      = get_the_title( $iteneraries_id );
                $iteneraries_link = get_the_permalink( $iteneraries_id );

                //-- Departure Port
                $port_in = get_post_meta( $iteneraries_id, '_iteneraries_departure_port', true );
                $port_in = !empty( $port_in ) ? $port_in : '-';

                //-- Arrival port
                $port_out = get_post_meta( $iteneraries_id, '_iteneraries_arrival_port', true );
                $port_out = !empty( $port_out ) ? $port_out : '-';

                $boat_id   = get_post_meta( $post_id, $prefix . 'boat', true );
                $boat_name = get_the_title( $boat_id );
                $boat_slug = sanitize_title( $boat_name );

                if( array_search( $boat_name, array_column( $all_boat, 'title' ) ) === false )
                {
                    if( strtolower( $boat_name ) == 'mermaid i' )
                    {
                        $link_sch = get_site_url().'/schedule-rates/mermaid-i/';
                    }
                    else
                    {
                        $link_sch = get_site_url().'/schedule-rates/mermaid-ii/';
                    }

                    $all_boat[] = array(
                        'title' => $boat_name,
                        'link'  => $link_sch
                    );
                }

                // ================= HTML DESKTOP =================
                $html .= '
                <tr id="' . $sanitize_title . '">
                    <td class="date depart_date" data-value="' . date( 'd M Y', $strtotime_depart_date ) . '">
                        <span>' . date( 'd', $strtotime_depart_date ) . '</span>
                        <h5>' . date( 'M', $strtotime_depart_date ) . '</h5>
                        <h5>' . date( 'Y', $strtotime_depart_date ). '</h5>
                    </td>
                    <td class="trip_code" data-value="'.$post_title.'">' . $post_title . '</td>
                    <td class="itenerary">
                        <h3 data-value="' . $iteneraries . '">' . $iteneraries . '</h3>
                        <h4 class="duration">' . $duration_text . ': ' . $day . 'D ' . $night . 'N</h4>
                        <h4 class="return" data-value="' . date( 'd M Y', $strtotime_return_date ). '">' . $return_text . ': ' . date( 'd M Y', $strtotime_return_date ) . '</h4>
                        <h4 class="mermaid-type" data-value="' . $boat_name . '">' . $boat_name . '</h4>';

                if( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) )
                {
                    $html .= '<a href="' . $iteneraries_link . '" class="disabled-inport" data-boat-id="' . $boat_id . '" data-trip="' . $iteneraries_id . '">' . $trip_details_text . '</a>';
                }
                else
                {
                    $html .= '<a href="' . $iteneraries_link . '" class="trip-details" data-boat-id="' . $boat_id . '" data-trip="' . $iteneraries_id . '">' . $trip_details_text . '</a>';
                }

                $html .= '
                    </td>';

                if( $boat_slug == 'mermaid-i' || $page_now != 'schedule_rate' )
                {
                    if( $price_master == 0 || empty( $price_master ) )
                    {
                        $html .= '<td>-</td>';
                    }
                    else
                    {
                        $html .= '<td class="price_type">';

                        $price_master_calc = $price_currency * $price_master;
                        $price_master_frmt = number_format( $price_master_calc, 2 );

                        if ( $price_master_dsc != 0 )
                        {
                            $price_discount_calc = $price_master_calc - ( $price_master_calc * ( $price_master_dsc / 100 ) );
                            $price_discount_frmt = number_format( $price_discount_calc, 2 );

                            $html .= '<p class="discount_price_real"> <strike>' . $symbol_currency . ' ' . $price_master_frmt . '</strike></p>';
                            $html .= '<input type="radio" class="radio-desktop radio-master" name="type-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="master" id="master-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '" data-price="' . $price_discount_calc . '" data-currency-code="' . $code_currency . '">';
                            $html .= '<label for="master-' . $i . '" class="price-desktop">' . $symbol_currency . ' ' . $price_discount_frmt . '</label>';
                        }
                        else
                        {
                            $html .= '<input type="radio" class="radio-desktop radio-master" name="type-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="master" id="master-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '" data-price="' . $price_master_calc . '" data-currency-code="' . $code_currency . '">';
                            $html .= '<label for="master-' . $i . '" class="price-desktop">' . $symbol_currency . ' ' . $price_master_frmt . '</label>';
                        }

                        $html .= ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_master . '</p>' : '';
                        $html .= !empty( $price_master_dsc ) ? ' <p class="discount_price"><span class="triangle-right"></span><strong>' . $price_master_dsc . '%</strong> Discount</p>' : '';
                        $html .= '</td>';
                    }

                    if( $price_single == 0 || empty( $price_single ) )
                    {
                        $html .= '<td>-</td>';
                    }
                    else
                    {
                        $html .= '<td class="price_type">';

                        $price_single_calc = $price_currency * $price_single;
                        $price_single_frmt = number_format( $price_single_calc, 2 );

                        if ( $price_single_dsc != 0 )
                        {
                            $price_discount_calc = $price_single_calc - ( $price_single_calc * ( $price_single_dsc / 100 ) );
                            $price_discount_frmt = number_format( $price_discount_calc, 2 );

                            $html .= '<p class="discount_price_real"> <strike>' . $symbol_currency . ' ' . $price_single_frmt . '</strike></p>';
                            $html .= '<input type="radio" class="radio-desktop radio-single" name="type-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="single" id="single-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '" data-price="' . $price_discount_calc . '" data-currency-code="' . $code_currency . '">';
                            $html .= '<label for="single-' . $i . '" class="price-desktop">' . $symbol_currency . ' ' . $price_discount_frmt . '</label>';
                        }
                        else
                        {
                            $html .= '<input type="radio" class="radio-desktop radio-single" name="type-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="single" id="single-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '" data-price="' . $price_single_calc . '" data-currency-code="' . $code_currency . '">';
                            $html .= '<label for="single-' . $i . '" class="price-desktop">' . $symbol_currency . ' ' . $price_single_frmt . '</label>';
                        }

                        $html .= ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_single . '</p>' : '';
                        $html .= !empty( $price_single_dsc ) ? ' <p class="discount_price"><span class="triangle-right"></span><strong>' . $price_single_dsc . '%</strong> Discount</p>' : '';
                        $html .= '</td>';
                    }
                }

                if( $price_deluxe == 0 || empty( $price_deluxe ) )
                {
                    $html .= '<td>-</td>';
                }
                else
                {
                    $html .= '<td class="price_type">';

                    $price_deluxe_calc = $price_currency * $price_deluxe;
                    $price_deluxe_frmt = number_format( $price_deluxe_calc, 2 );

                    if ( $price_deluxe_dsc != 0 )
                    {
                        $price_discount_calc = $price_deluxe_calc - ( $price_deluxe_calc * ( $price_deluxe_dsc / 100 ) );
                        $price_discount_frmt = number_format( $price_discount_calc, 2 );

                        $html .= '<p class="discount_price_real"> <strike>' . $symbol_currency . ' ' . $price_deluxe_frmt . '</strike></p>';
                        $html .= '<input type="radio" class="radio-desktop radio-deluxe" name="type-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="deluxe" id="deluxe-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '" data-price="' . $price_discount_calc . '" data-currency-code="' . $code_currency . '">';
                        $html .= '<label for="deluxe-' . $i . '" class="price-desktop">' . $symbol_currency . ' ' . $price_discount_frmt . '</label>';
                    }
                    else
                    {
                        $html .= '<input type="radio" class="radio-desktop radio-deluxe" name="type-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="deluxe" id="deluxe-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '" data-price="' . $price_deluxe_calc . '" data-currency-code="' . $code_currency . '">';
                        $html .= '<label for="deluxe-' . $i . '" class="price-desktop">' . $symbol_currency . ' ' . $price_deluxe_frmt . '</label>';
                    }

                    $html .= ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_deluxe . '</p>' : '';
                    $html .= !empty( $price_deluxe_dsc ) ? ' <p class="discount_price"><span class="triangle-right"></span><strong>' . $price_deluxe_dsc . '%</strong> Discount</p>' : '';
                    $html .= '</td>';
                }

                if( $price_lower == 0 || empty( $price_lower ) )
                {
                    $html .= '<td>-</td>';
                }
                else
                {
                    $html .= '<td class="price_type">';

                    $price_lower_calc = $price_currency * $price_lower;
                    $price_lower_frmt = number_format( $price_lower_calc, 2 );

                    if ( $price_lower_dsc != 0 )
                    {
                        $price_discount_calc = $price_lower_calc - ( $price_lower_calc * ( $price_lower_dsc / 100 ) );
                        $price_discount_frmt = number_format( $price_discount_calc, 2 );

                        $html .= '<p class="discount_price_real"> <strike>' . $symbol_currency . ' ' . $price_lower_frmt . '</strike></p>';
                        $html .= '<input type="radio" class="radio-desktop radio-lower" name="type-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="lower" id="lower-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '" data-price="' . $price_discount_calc . '" data-currency-code="' . $code_currency . '">';
                        $html .= '<label for="lower-' . $i . '" class="price-desktop">' . $symbol_currency . ' ' . $price_discount_frmt . '</label>';
                    }
                    else
                    {
                        $html .= '<input type="radio" class="radio-desktop radio-lower" name="type-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="lower" id="lower-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '" data-price="' . $price_lower_calc . '" data-currency-code="' . $code_currency . '">';
                        $html .= '<label for="lower-' . $i . '" class="price-desktop">' . $symbol_currency . ' ' . $price_lower_frmt . '</label>';
                    }

                    $html .= ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_lower . '</p>' : '';
                    $html .= !empty( $price_lower_dsc ) ? ' <p class="discount_price"><span class="triangle-right"></span><strong>' . $price_lower_dsc . '%</strong> Discount</p>' : '';
                    $html .= '</td>';
                }

                $html .= '
                    <td class="status">';

                if( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) )
                {
                    $html .= '<a href="javascript:;" class="disabled-inport" data-boat="' . $boat_slug . '" data-port-in="' . $port_in . '" data-port-out="' . $port_out . '">' . $book_now_text . '</a>';
                }
                else
                {
                    $html .= '<a href="javascript:;" class="disabled" data-boat="' . $boat_slug . '" data-port-in="' . $port_in . '" data-port-out="' . $port_out . '">' . $book_now_text . '</a>';
                }

                $html .= '
                    </td>
                </tr>';

                // ================= HTML MOBILE =================
                $html_mobile .= '
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="list-schedule-data" id="mobile-' . $sanitize_title . '">
                        <div class="loading"></div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <h3>' . $date_text . '</h3>
                                <h5 class="depart-date">' . date( 'd M Y', $strtotime_depart_date ) . '</h5>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <h3>' . $trip_code_text . '</h3>
                                <h5 class="trip-code">' . $post_title . '</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <h3>' . $itenerary_text . '</h3>
                                <h5 class="itenerary-title">' . $iteneraries . '</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 itenerary">
                                <h4 class="duration">' . $day . 'D ' . $night . 'N</h4>
                                <h4 class="return" data-value="' . date( 'd M Y', $strtotime_return_date ) . '">' . date( 'd M Y', $strtotime_return_date ) . '</h4>
                            </div>
                            <div class="col-lg-6 col-md-6 itenerary">
                                <h4 class="mermaid-type" data-value="' . $boat_name . '">' . $boat_name . '</h4>';

                if( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) )
                {
                    $html_mobile .= '<a href="' . $iteneraries_link . '" class="disabled-inport" data-boat-id="' . $boat_id . '" data-trip="' . $iteneraries_id . '">' . $trip_details_text . '</a>';
                }
                else
                {
                    $html_mobile .= '<a href="' . $iteneraries_link . '" class="trip-details" data-boat-id="' . $boat_id . '" data-trip="' . $iteneraries_id . '">' . $trip_details_text . '</a>';
                }

                $html_mobile .= '  
                            </div>
                        </div>
                        <div class="row">';

                if( $boat_slug != 'mermaid-ii' || $page_now != 'schedule_rate' )
                {
                    if( $price_master == 0 || empty( $price_master ) )
                    {
                        $html_mobile .= '<div class="col-lg-6 col-md-6">-</div>';
                    }
                    else
                    {
                        $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_master . '</p>' : '';

                        $price_master_calc = $price_currency * $price_master;
                        $price_master_frmt = number_format( $price_master_calc, 0 );

                        if ( $price_master_dsc != 0 )
                        {
                            $price_discount_calc = $price_master_calc - ( $price_master_calc * ( $price_master_dsc / 100 ) );
                            $price_discount_frmt = number_format( $price_discount_calc, 2 );

                            $html_mobile .= '
                            <div class="col-lg-6 col-md-6 cabins-type">
                                <label>Master</label>
                                <p class="discount_price_real_mobile"><strike>' . $symbol_currency . ' ' . $price_master_frmt . ' </strike></p>
                                <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="master" id="master-m-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '"  data-price="' . $price_discount_calc . '" data-currency-code="' . $code_currency . '" class="m-price-choose radio-mobile radio-master">
                                <label for="master-' . $i . '" class="price-mobile">' . $symbol_currency . ' ' . $price_discount_frmt . '</label>
                                ' . $roles_agent_html . '
                                <div class="label_discount"><span class="triangle-right"></span><p class="discount_price"><strong>' . $price_master_dsc . '%</strong> Discount</p></div>
                            </div>';
                        }
                        else
                        {
                            $html_mobile .= '
                            <div class="col-lg-6 col-md-6 cabins-type">
                                <label>Master</label>
                                <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="master" id="master-m-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '"  data-price="' . $price_master_calc . '" data-currency-code="' . $code_currency . '" class="m-price-choose radio-mobile radio-master">
                                <label for="master-' . $i . '" class="price-mobile">' . $symbol_currency . ' ' . $price_master_frmt . '</label>
                                ' . $roles_agent_html . '
                            </div>';
                        }
                    }

                    if( $price_single == 0 || empty( $price_single ) )
                    {
                        $html .= '<div class="col-lg-6 col-md-6">-</div>';
                    }
                    else
                    {
                        $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_single . '</p>' : '';

                        $price_single_calc = $price_currency * $price_single;
                        $price_single_frmt = number_format( $price_single_calc, 0 );

                        if ( $price_single_dsc != 0 )
                        {
                            $price_discount_calc = $price_single_calc - ( $price_single_calc * ( $price_single_dsc / 100 ) );
                            $price_discount_frmt = number_format( $price_discount_calc, 2 );

                            $html_mobile .= '
                            <div class="col-lg-6 col-md-6 cabins-type">
                                <label>Master</label>
                                <p class="discount_price_real_mobile"><strike>' . $symbol_currency . ' ' . $price_single_frmt . ' </strike></p>
                                <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="single" id="single-m-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '"  data-price="' . $price_discount_calc . '" data-currency-code="' . $code_currency . '" class="m-price-choose radio-mobile radio-single">
                                <label for="single-' . $i . '" class="price-mobile">' . $symbol_currency . ' ' . $price_discount_frmt . '</label>
                                ' . $roles_agent_html . '
                                <div class="label_discount"><span class="triangle-right"></span><p class="discount_price"><strong>' . $price_single_dsc . '%</strong> Discount</p></div>
                            </div>';
                        }
                        else
                        {
                            $html_mobile .= '
                            <div class="col-lg-6 col-md-6 cabins-type">
                                <label>Master</label>
                                <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="single" id="single-m-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '"  data-price="' . $price_single_calc . '" data-currency-code="' . $code_currency . '" class="m-price-choose radio-mobile radio-single">
                                <label for="single-' . $i . '" class="price-mobile">' . $symbol_currency . ' ' . $price_single_frmt . '</label>
                                ' . $roles_agent_html . '
                            </div>';
                        }
                    }
                }

                $html_mobile .= '  
                        </div>
                        <div class="row">';

                if( $price_deluxe == 0 || empty( $price_deluxe ) )
                {
                    $html_mobile .= '<div class="col-lg-6 col-md-6">-</div>';
                }
                else
                {
                    $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_deluxe . '</p>' : '';
                    $boat_cabin_name  = $boat_slug != "mermaid-ii" ? 'Deluxe' : 'Deluxe Cabin';

                    $price_deluxe_calc = $price_currency * $price_deluxe;
                    $price_deluxe_frmt = number_format( $price_deluxe_calc, 0 );

                    if ( $price_deluxe_dsc != 0 )
                    {
                        $price_discount_calc = $price_deluxe_calc - ( $price_deluxe_calc * ( $price_deluxe_dsc / 100 ) );
                        $price_discount_frmt = number_format( $price_discount_calc, 2 );

                        $html_mobile .= '
                        <div class="col-lg-6 col-md-6 cabins-type">
                            <label>' . $boat_cabin_name . '</label>
                            <p class="discount_price_real_mobile"><strike>' . $symbol_currency . ' ' . $price_deluxe_frmt . ' </strike></p>
                            <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="deluxe" id="deluxe-m-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '"  data-price="' . $price_discount_calc . '" data-currency-code="' . $code_currency . '" class="m-price-choose radio-mobile radio-deluxe">
                            <label for="deluxe-' . $i . '" class="price-mobile">' . $symbol_currency . ' ' . $price_discount_frmt . '</label>
                            ' . $roles_agent_html . '
                            <div class="label_discount"><span class="triangle-right"></span><p class="discount_price"><strong>' . $price_deluxe_dsc . '%</strong> Discount</p></div>
                        </div>';
                    }
                    else
                    {
                        $html_mobile .= '
                        <div class="col-lg-6 col-md-6 cabins-type">
                            <label>' . $boat_cabin_name . '</label>
                            <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="deluxe" id="deluxe-m-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '"  data-price="' . $price_deluxe_calc . '" data-currency-code="' . $code_currency . '" class="m-price-choose radio-mobile radio-deluxe">
                            <label for="deluxe-' . $i . '" class="price-mobile">' . $symbol_currency . ' ' . $price_deluxe_frmt . '</label>
                            ' . $roles_agent_html . '
                        </div>';
                    }
                }

                if( $price_lower == 0 || empty( $price_lower ) )
                {
                    $html_mobile .= '<div class="col-lg-6 col-md-6">-</div>';
                }
                else
                {
                    $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_lower . '</p>' : '';
                    $boat_cabin_name  = $boat_slug != "mermaid-ii" ? 'Lower Deck' : 'Budget Cabin';

                    $price_lower_calc = $price_currency * $price_lower;
                    $price_lower_frmt = number_format( $price_lower_calc, 0 );

                    if ( $price_lower_dsc != 0 )
                    {
                        $price_discount_calc = $price_lower_calc - ( $price_lower_calc * ( $price_lower_dsc / 100 ) );
                        $price_discount_frmt = number_format( $price_discount_calc, 2 );

                        $html_mobile .= '
                        <div class="col-lg-6 col-md-6 cabins-type">
                            <label>' . $boat_cabin_name . '</label>
                            <p class="discount_price_real_mobile"><strike>' . $symbol_currency . ' ' . $price_lower_frmt . ' </strike></p>
                            <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="lower" id="lower-m-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '"  data-price="' . $price_discount_calc . '" data-currency-code="' . $code_currency . '" class="m-price-choose radio-mobile radio-lower">
                            <label for="lower-' . $i . '" class="price-mobile">' . $symbol_currency . ' ' . $price_discount_frmt . '</label>
                            ' . $roles_agent_html . '
                            <div class="label_discount"><span class="triangle-right"></span><p class="discount_price"><strong>' . $price_lower_dsc . '%</strong> Discount</p></div>
                        </div>';
                    }
                    else
                    {
                        $html_mobile .= '
                        <div class="col-lg-6 col-md-6 cabins-type">
                            <label>' . $boat_cabin_name . '</label>
                            <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $iteneraries_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="lower" id="lower-m-' . $i . '" data-boat="' . sanitize_title( $boat_name ) . '"  data-price="' . $price_lower_calc . '" data-currency-code="' . $code_currency . '" class="m-price-choose radio-mobile radio-lower">
                            <label for="lower-' . $i . '" class="price-mobile">' . $symbol_currency . ' ' . $price_lower_frmt . '</label>
                            ' . $roles_agent_html . '
                        </div>';
                    }
                }

                $html_mobile .= '  
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 status">';

                if( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) )
                {
                    $html_mobile .= '<a href="javascript:;" class="disabled-inport" data-screen="mobile" data-boat="' . $boat_slug . '" data-port-in="' . $port_in . '" data-port-out="' . $port_out . '">' . $book_now_text . '</a>';
                }
                else
                {
                    $html_mobile .= '<a href="javascript:;" class="disabled" data-screen="mobile" data-boat="' . $boat_slug . '" data-port-in="' . $port_in . '" data-port-out="' . $port_out . '">' . $book_now_text . '</a>';
                }

                $html_mobile .= '
                            </div>
                        </div>
                    </div>
                </div>';

                if( $i % 2 == 1 )
                {
                    $html_mobile .= '
                    </div>
                    <div class="row">';
                }

                $i++;
            }
        }

        $html_all_mobile = '
        <div class="row">
            ' . $html_mobile . '
        </div>';

        $html_button_more = '';

        if( !empty( $all_boat ) )
        {
            sort( $all_boat );

            foreach( $all_boat as $ab )
            {
                $html_button_more .= '<a href="' . $ab['link'] . '" class="btn-more-schedule">More Schedules & Rates for ' . $ab['title'] . '</a>';
            }
        }

        if( !empty( $query ) )
        {
            wp_send_json( array(
                'html_button_more' => $html_button_more,
                'html_mobile'      => $html_all_mobile,
                'html'             => $html,
                'status'           => 'success'
            ) );
        }
        else
        {
            wp_send_json( array(
                'msg'    => '<h3>Something went wrong, please try again later</h3>',
                'status' => 'failed'
            ) );
        }
    }
    else
    {
        wp_send_json( array(
            'msg'    => '<h3>Something went wrong, please try again later</h3>',
            'status' => 'failed'
        ) );
    }
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk merubah tombol status jika memilih price
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_choose-price', 'choose_price_schedule' );
add_action( 'wp_ajax_nopriv_choose-price', 'choose_price_schedule' );
function choose_price_schedule()
{
    $result['status'] = "failed";
    $result['msg']    = "<h3>Something went wrong, please try again later</h3>";

    if( isset( $_POST['action'] ) && $_POST['action'] == "choose-price" ):
        $prefix         = "_schedule_data_";
        $type           = $_POST['type'];
        $post_id        = $_POST['key'];
        $boat           = $_POST['boat'];
        $post_title     = get_the_title( $post_id );
        $sanitize_title = sanitize_title( $post_title );
        $screen         = $_POST['screen'];
        $itenerary      = $_POST['itenerary'];

        // Departure Port & Arrival port
        $port_in  = get_post_meta( $itenerary, '_iteneraries_departure_port', true );
        $port_in  = !empty( $port_in ) ? $port_in : '-';
        $port_out = get_post_meta( $itenerary, '_iteneraries_arrival_port', true );
        $port_out = !empty( $port_out ) ? $port_out : '-';

        $allotment = get_post_meta( $post_id, $prefix.'allotment_'.$type, true );

        if( $allotment > 0 ):
            $button = '<a href="" class="book_now" data-screen="'.$screen.'" data-boat="'.$boat.'" data-port-in="'.$port_in.'" data-port-out="'.$port_out.'">'.get_label_string( 'Book Now', true ).'</a>';
        else:
            $button = '<a href="" class="inquire" data-screen="'.$screen.'" data-boat="'.$boat.'" data-port-in="'.$port_in.'" data-port-out="'.$port_out.'">'.get_label_string( 'Inquire', true ).'</a>';
        endif;

    $button_default           = '<a href="" class="book_now" data-screen="'.$screen.'" data-boat="'.$boat.'" data-port-in="'.$port_in.'" data-port-out="'.$port_out.'">'.get_label_string( 'Book Now', true ).'</a>';
    $result['status']         = "success";
    $result['slug']           = $sanitize_title;
    $result['button_html']    = $button;
    $result['button_default'] = $button_default;

    endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk merubah tombol status jika memilih price
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_view-dive-spots', 'view_dive_spots' );
add_action( 'wp_ajax_nopriv_view-dive-spots', 'view_dive_spots' );
function view_dive_spots()
{
    $result['status'] = "failed";
    $result['msg']    = "<h3>Something went wrong, please try again later</h3>";

    // if(isset($_POST['action']) && $_POST['action'] == "view-dive-spots"):
    //     $type           = $_POST['type'];
    //     $post_id        = $_POST['key'];
    //     $post_title     = get_the_title($post_id);
    //     $sanitize_title = sanitize_title($post_title);

    //     $allotment = get_post_meta( $post_id, '_schedule_rates_allotment_'.$type, true );

    //     if($allotment > 0):
    //         $button = '<a href="" class="book_now">'.get_label_string('Book Now', true).'</a>';
    //     else:
    //         $button = '<a href="" class="inquire">'.get_label_string('Inquire', true).'</a>';
    //     endif;

    //     $result['status']       = "success";
    //     $result['slug']         = $sanitize_title;
    //     $result['button_html']  = $button;

    // endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk mendapatkan data type
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_nationality-data', 'get_data_type' );
add_action( 'wp_ajax_nopriv_nationality-data', 'get_data_type' );
add_action( 'wp_ajax_itinerary-data', 'get_data_type' );
add_action( 'wp_ajax_nopriv_itinerary-data', 'get_data_type' );
add_action( 'wp_ajax_destination-data', 'get_data_type' );
add_action( 'wp_ajax_nopriv_destination-data', 'get_data_type' );
add_action( 'wp_ajax_schedule-data', 'get_data_type' );
add_action( 'wp_ajax_nopriv_schedule-data', 'get_data_type' );
function get_data_type()
{
    global $wpdb;

    $result['status'] = "failed";
    $result['msg']    = "<h3>Something went wrong, please try again later</h3>";

    // NATIONALITY DATA
    if( isset( $_POST['action'] ) && $_POST['action'] == "nationality-data" ):
        $nationality_result = the_country_list();

        $option = '';

        foreach( $nationality_result as $code => $name ):
            $option .= '<option value="' . $code . '">' . $name . '</option>';
        endforeach;

        $result['status'] = "success";
        $result['html']   = $option;
    endif;

    // ITINERARY DATA
    if( isset( $_POST['action'] ) && $_POST['action'] == "itinerary-data" ):
        $itenerary_result = $wpdb->get_results( "SELECT ID, post_title FROM {$wpdb->prefix}posts WHERE post_status='publish' AND post_type='iteneraries' ORDER BY menu_order ASC" );

        $option     = "";
        $reverse_id = array();
        foreach( $itenerary_result as $d ):
            // echo $name;
            $code    = $d->ID;
            $name    = $d->post_title;
            $reverse = get_post_meta( $code, '_iteneraries_reverse', true );

            if ( !in_array( $code, $reverse_id ) ):
                if( strtolower( $name ) != "in port" ):
                    $or_reverse_text = !empty( $reverse ) ? ' or Reverse' : '';
                    $option .= "<option value='$name".$or_reverse_text."'>$name".$or_reverse_text."</option>";
                endif;

                $reverse_id[] = $reverse;
            endif;
        endforeach;

        $result['status'] = "success";
        $result['html']   = $option;
    endif;

    // DESTINATION DATA
    if( isset( $_POST['action'] ) && $_POST['action'] == "destination-data" ):
        $nationality_result = $wpdb->get_results( "SELECT ID, post_title FROM {$wpdb->prefix}posts WHERE post_status='publish' AND post_type='destination' ORDER BY post_title ASC" );

        $option = "";
        foreach( $nationality_result as $d ):
            $code = $d->ID;
            $name = $d->post_title;
            $option .= "<option value='$code'>$name</option>";
        endforeach;

        $result['status'] = "success";
        $result['html']   = $option;
    endif;

    // SCHEDULE DATA
    if( isset( $_POST['action'] ) && $_POST['action'] == "schedule-data" ):
        $args = array(
            'post_status'    => 'publish',
            'post_type'      => 'boat',
            'order'          => 'ASC',
            'posts_per_page' => -1,
        );

        $query = new WP_Query( $args );

        $option = "";

        while ( $query->have_posts() ):
            $query->the_post();

            $code  = get_the_ID();
            $title = get_the_title();

            $option .= "<option value='$code'>$title</option>";
        endwhile;
        wp_reset_postdata();

        $result['status'] = "success";
        $result['html']   = $option;
    endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk filter year pada schedule & currency
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_payment-checkout', 'payment_checkout' );
add_action( 'wp_ajax_nopriv_payment-checkout', 'payment_checkout' );
function payment_checkout()
{
    global $wpdb;
    $result['status'] = "failed";
    $result['msg']    = "<h3>Error</h3><p>Something went wrong, please try again later</p>";

    if( isset( $_POST['action'] ) && $_POST['action'] == "payment-checkout" ):
        $key = $_POST['key'];

        $data = $wpdb->get_results(
            $wpdb->prepare( "SELECT ID, post_title, post_type FROM {$wpdb->prefix}posts WHERE post_name=%s LIMIT 0,1", $key )
        );
        $count = count( $data );

        if( $count > 0 ):
            $invoice_id  = $data[0]->ID;
            $post_type   = $data[0]->post_type;
            $url_payment = get_url_payment( $invoice_id, $post_type, $_POST );

            if( !empty( $url_payment ) ):
                $result['status'] = "success";
                $result['msg']    = "success";
                $result['url']    = $url_payment;
            endif;
        endif;

    endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk filter year pada schedule & currency
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_newsletter_subscribe', 'newsletter_subscribe' );
add_action( 'wp_ajax_nopriv_newsletter_subscribe', 'newsletter_subscribe' );
function newsletter_subscribe()
{
    $result['status'] = "failed";
    $result['msg']    = '<div class="message"><h3>Error</h3><p>Something went wrong, please try again later</p></div>';

    if( isset( $_POST['action'] ) && $_POST['action'] == "newsletter_subscribe" ):
        $email = $_POST['email'];

        if ( !filter_var( $email, FILTER_VALIDATE_EMAIL ) ):
            $result['status'] = "failed";
            $result['msg']    = '<div class="message"><h3>Email invalid</h3><p>Please check your email</p></div>';
        else:

            // GET MAILCHIMP SETTING ON DATABASE
            $mailchimp_setting             = get_option( 'mailchimp_setting' );
            $mailchimp_api_key             = "";
            $mailchimp_list_id             = "";
            $mailchimp_success_title       = "";
            $mailchimp_success_description = "";
            if( !empty( $mailchimp_setting ) ):
                $mailchimp_setting             = json_decode( $mailchimp_setting );
                $mailchimp_api_key             = $mailchimp_setting->mailchimp_api_key;
                $mailchimp_list_id             = $mailchimp_setting->mailchimp_list_id;
                $mailchimp_success_title       = $mailchimp_setting->mailchimp_success_title;
                $mailchimp_success_description = $mailchimp_setting->mailchimp_success_description;
            endif;

        $MailChimp             = new MailChimp( $mailchimp_api_key );
        $MailChimp->verify_ssl = false;

        $post_mailchimp = $MailChimp->post( "lists/$mailchimp_list_id/members", array(
            'email_address' => $email,
            'status'        => 'pending',
        ) );

        if( $post_mailchimp ):
            $result['status'] = "success";
            $result['msg']    = '<div class="message"><h3>'.$mailchimp_success_title.'</h3><p>'.$mailchimp_success_description.'</p></div>';
        endif;
    endif;
    endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk mendapatkan gallery boat page
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_set-gallery-boat', 'set_gallery_boat' );
add_action( 'wp_ajax_nopriv_set-gallery-boat', 'set_gallery_boat' );
function set_gallery_boat()
{
    $result['status'] = "failed";
    $result['msg']    = '<div class="message"><h3>Error</h3><p>Something went wrong, please try again later</p></div>';

    if( isset( $_POST['action'] ) && $_POST['action'] == "set-gallery-boat" ):
        $key                = $_POST['key'];
        $header_slide_image = get_post_meta( $key, '_boat_gallery', true );
        $html               = "";

        if( !empty( $header_slide_image ) ):
            foreach( $header_slide_image as $img_id => $d ):
                $image   = wp_get_attachment_image_src( $img_id, 'large' );
                $caption = wp_get_attachment_caption( $img_id );
                $caption = !empty( $caption ) ? '<h3>'.$caption.'</h3>' : '';

                $html .= '
                    <div class="item">
                        <img class="b-lazy" src="'.$image[0].'" />
                        '.$caption.'
                    </div>
                ';

            endforeach;
        endif;

        if( !empty( $html ) ):
            $result['status'] = "success";
            $result['msg']    = $html;
        endif;
    endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk mendapatkan detail iteneraries popup
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_get-detail-trip', 'get_detail_trip' );
add_action( 'wp_ajax_nopriv_get-detail-trip', 'get_detail_trip' );

function get_detail_trip()
{
    $result['status'] = 'failed';
    $result['msg']    = '
    <div class="message">
        <h3>Error</h3>
        <p>Something went wrong, please try again later</p>
    </div>';

    if( isset( $_POST['action'] ) && $_POST['action'] == 'get-detail-trip' )
    {
        $trip = $_POST['trip'];
        $boat = $_POST['boat'];
        $post = new WP_Query( array( 'p' => $trip, 'post_type' => 'any' ) );
        $html = '';

        if( $post->have_posts() )
        {
            while( $post->have_posts() )
            {
                $post->the_post();

                $title       = get_the_title();
                $itable      = get_itenerary_data_table( $trip, $boat );
                $content     = apply_filters( 'the_content', get_the_content() );
                $subtitle    = get_post_meta( $trip, '_iteneraries_subtitle', true );
                $route_image = get_post_meta( $trip, '_iteneraries_route_image', true );

                $html .= '
                <section class="welcome-about welcome-mermaid">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 ' . ( empty( $title ) ? 'style="display:none;"' : '' ) . '  class="heading-default">' . $title . '</h1>
                                <h4 ' . ( empty( $subtitle ) ? 'style="display:none;"' : '' ) . '  >' . $subtitle . '</h4>
                                <div class="desc-about">' . $content . '</div>
                            </div>
                        </div>
                    </div>';

                if( !empty( $route_image ) && file_exists( $route_image ) )
                {
                    $html .= '
                        <div class="container-mermaid route-image">
                            <img src="'. $route_image . '" alt="' . $title . '" />
                        </div>';
                }

                $html .= '
                </section>
                <section class="wrap-iteneraries-table">
                    <div class="container-mermaid">
                        <div class="container-itenerary-table show">
                            ' . $itable['html'] . '
                        </div>
                    </div>
                </section>
                <section class="gallery-itenerary">
                    <div class="bg-grey"></div>
                    <div class="box-title-gallery">
                        <div class="container clearfix">
                            <div class="row">
                                <div class="col-lg-8 col-md-6">
                                    <h2>' . $title . ' Gallery</h2>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <h4 class="full-screen">
                                        ' . get_label_string( 'Full Screen', true ) . '
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-mermaid slide-image-page-detail">
                        <div class="container-gallery">
                            <h2 class="gallery-title"> ' . $title . ' Gallery</h2>
                            <span class="close">Close</span>                            
                            <div class="wrap-owl-slide">
                                <div class="m-arrow m-prev"></div>
                                <div class="owl-carousel owl-theme m-slide">';

                $dest_iteneraries    = get_post_meta( $trip, '_iteneraries_destination', true );
                $gallery_iteneraries = array();

                if( !empty( $dest_iteneraries ) )
                {
                    foreach( $dest_iteneraries as $dest )
                    {
                        $gallery = get_post_meta( $dest, '_destination_image', true );

                        if( !empty( $gallery ) )
                        {
                            $ig = 1;

                            foreach( $gallery as $img_id => $d )
                            {
                                $image   = wp_get_attachment_image_src( $img_id, 'large' );
                                $caption = wp_get_attachment_caption( $img_id );
                                $caption = !empty( $caption ) ? '<h3>' . $caption . '</h3>' : '';

                                $gallery_iteneraries[] = array(
                                    'image'   => $image[0],
                                    'caption' => $caption
                                );

                                $ig++;
                            }
                        }
                    }
                }

                if( !empty( $gallery_iteneraries ) )
                {
                    foreach( $gallery_iteneraries as $d )
                    {
                        $html .= '
                                            <div class="item">
                                                <img src="'. $d['image'] . '" alt="" />
                                                '. $d['caption'] . '
                                            </div>';
                    }
                }

                $html .= '
                                </div>
                                <div class="m-arrow m-next"></div>
                            </div>
                        </div>
                    </div>
                </section>';

                $group_field_iteneraries = get_group_field( $trip, 'iteneraries', true );

                $departure_port  = get_post_meta( get_the_ID(), '_iteneraries_departure_port', true );
                $arrival_port    = get_post_meta( get_the_ID(), '_iteneraries_arrival_port', true );
                $park_fee_title  = get_post_meta( $trip, '_iteneraries_title_park_fee', true );
                $park_fee_status = get_post_meta( $trip, '_iteneraries_show_park_fee', true );
                $park_fee_list   = get_post_meta( $trip, '_iteneraries_park_fee_list', true );

                $park_fee_title = empty( $park_fee_title ) ? 'Park Fee' : $park_fee_title;

                $html .= '
                <section class="wrap-custom-group-field">
                    <div class="container">
                        <div class="row custom-field-group">
                            <div class="col-lg-6 col-md-6 container-list-custom-field grid-list">
                                <div class="description-group-field">
                                    <h2 class="title-cf">' . get_label_string( 'Departure Port', true ) . '</h2>
                                    '. empty( $departure_port ) ? '-' : $departure_port . '
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 container-list-custom-field grid-list">
                                <div class="description-group-field">
                                    <h2 class="title-cf">' . get_label_string( 'Arrival Port', true ) . '</h2>
                                    '. empty( $arrival_port ) ? '-' : $arrival_port .'
                                </div>
                            </div>
                        </div>';

                $html .= $group_field_iteneraries;

                if( $park_fee_status == 'on' && !empty( $park_fee_list ) )
                {
                    $html .= '
                            <div class="row custom-field-group park-fee">
                                <div class="col-md-12 container-list-custom-field grid-list">
                                    <h2 class="title-cf">' . $park_fee_title . '</h2>
                                    <div class="description-group-field">
                                        <ul>';

                    $pi = 1;

                    foreach( $park_fee_list as $d )
                    {

                        if( $pi % 2 == 1 && $pi > 1 )
                        {
                            $html .= '</ul><ul>';
                        }

                        $html .= '
                                                <li>
                                                    <h3>' . $d['title_park_fee'] . '</h3>
                                                    <p>' . $d['value_park_fee'] . '</p>
                                                </li>';

                        $pi++;
                    }

                    $html .= '
                                        </ul>
                                    </div>
                                </div>
                            </div>';
                }

                $flight_list = get_post_meta( $trip, '_iteneraries_flight_recommendation_list', true );

                if( !empty( $flight_list ) )
                {
                    $html .= '
                            <div class="row custom-field-group park-fee">
                                <div class="col-md-12 container-list-custom-field grid-list">
                                    <h2 class="title-cf">' . get_post_meta( $trip, '_iteneraries_title_flight_recommendation', true ) . '</h2>
                                    <p>' . get_post_meta( $trip, '_iteneraries_brief_flight_recommendation', true ) . '</p>
                                    <div class="description-group-field flight-list">
                                        <ul>';

                    $i = 1;

                    foreach( $flight_list as $d )
                    {
                        $content_post   = get_post( $d );
                        $content_flight = $content_post->post_content;
                        $content_flight = apply_filters( 'the_content', $content_flight );
                        $content_flight = str_replace( ']]>', ']]&gt;', $content_flight );

                        if( $i % 2 == 1 && $i > 1 )
                        {
                            $html .= '</ul><ul>';
                        }

                        $html .= '
                                                <li>
                                                    <img src="' . get_the_post_thumbnail_url( $d, 'post-thumbnail' ) . '" alt="' . $content_post->post_title . '" />
                                                    '. $content_flight . '
                                                </li>';

                        $i++;
                    }

                    $html .= '
                                        </ul>
                                    </div>
                                </div>
                            </div>';
                }

                $html .= '
                    </div>
                </section>';

                $result['status'] = 'success';
                $result['msg']    = $html;
            }
        }

        wp_reset_postdata();
    }

    wp_send_json( $result );
}
