<?php
/*
Template Name: Contact Page
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php include_once "layout/hero.php"; ?>

    <section class="page-default page-static">
        <div class="container">

            <?php
            $post_content   = get_the_content();
            $post_content   = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
            $post_content   = apply_filters('the_content', $post_content);
            $post_content   = str_replace(']]>', ']]&gt;', $post_content);

            $contact_email = get_option('c_email');
            $contact_skype = get_option('c_skype');
            ?>

            <h1><?php the_title(); ?></h1>
            <div class="description-page"><?php echo $post_content; ?></div>

            
            <div class="container-form-contact">
                <div class="top-form-contact">
                    <div class="row">
                        <div class="col-md-4">
                            <h2>Contact Form</h2>
                        </div>
                        <div class="col-md-8">
                            <p><?php get_label_string('Note Contact Form'); ?></p>
                        </div>
                    </div>
                </div>

                <div class="body-form-contact">
                    <h2>Your Information</h2>

                    <?php echo do_shortcode('[contact-form-7 id="328" title="Contact Form Page"]'); ?>

                    <input type="hidden" name="thankyou_link" value="<?php echo get_site_url().'/thank-you-for-contacting-us/'; ?>" />

                </div>
            </div>

            <div class="row official-contact">
                <div class="col-md-6">
                    <h3>Mermaid Liveaboards Skype</h3>
                    <h4><a href="skype:<?php echo $contact_skype; ?>?call"><?php echo $contact_skype; ?></a></h4>
                </div>
                <div class="col-md-6">
                    <!--<p><?php get_label_string('Note Response Email'); ?></p>-->
                    <p>
                        We promise a response to emails in less than 24 hours. If you do not receive a response or have an urgent question, please contact us to 
                        <a href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?></a>. Please also check your junk box just in case our reply email went there.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <input type="hidden" name="message_popup_success" value="<?php echo get_option('message_contact_form_popup'); ?>"  />

<?php endwhile;  ?>

<?php get_footer(); ?>