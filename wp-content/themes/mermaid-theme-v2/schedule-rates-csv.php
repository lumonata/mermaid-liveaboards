<?php 

require_once( '../../../wp-load.php' ); 
require_once 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

$post_id = absint($_GET['id']);

if ( $post_id > 0 ) {
	
	$post = get_post( $post_id );
	
	// Start building CSV content with headers
	
	$prefix = "_schedule_data_";
	
	$args = array(
		'post_status'   => 'publish',
		'post_type'     => 'schedule_data',
		'numberposts'   => -1,
		'post_parent'   => $post_id,
		'order'         => 'ASC',
		'orderby'       => 'meta_value',
		'meta_key'      => $prefix.'departure_date'
	);
	
	 $posts = get_posts($args);
	 
	 header( 'Content-Disposition: attachment;filename="schedule-and-rates-' . sanitize_title( $post->post_title ) . '.csv' );
	 header( 'Content-Type: text/csv' );
	
	 $spread = new Spreadsheet();
     $sheet  = $spread->getActiveSheet();
			
	 //-- SETTING global style
     $spread->getDefaultStyle()->applyFromArray( array( 'font'  => array( 'size' => 11, 'name' => 'Arial' ) ) );
	 
	 //-- SETTING page
	 $sheet->getPageMargins()->setTop( 0.2 );
	 $sheet->getPageMargins()->setLeft( 0.2 );
	 $sheet->getPageMargins()->setRight( 0.2 );
	 $sheet->getPageMargins()->setBottom( 0.2 );

	 $sheet->getPageSetup()->setFitToWidth( 1 );
	 $sheet->getPageSetup()->setFitToHeight( 0 );
	 $sheet->getPageSetup()->setHorizontalCentered( true );
	 
	 $sheet->getColumnDimension( 'A' )->setWidth( 15 );
	 $sheet->getColumnDimension( 'B' )->setWidth( 11.57 );
	 $sheet->getColumnDimension( 'C' )->setWidth( 15.57 );
	 $sheet->getColumnDimension( 'D' )->setWidth( 9.86 );
	 $sheet->getColumnDimension( 'E' )->setWidth( 12 );
	 $sheet->getColumnDimension( 'F' )->setWidth( 12 );
	 $sheet->getColumnDimension( 'G' )->setWidth( 14 );
	 $sheet->getColumnDimension( 'H' )->setWidth( 14 );
	 $sheet->getColumnDimension( 'I' )->setWidth( 17 );
	 $sheet->getColumnDimension( 'J' )->setWidth( 17 );
			
	 //-- ADD logo
	 $sheet->mergeCells( 'A1:I1' );
	 $sheet->getRowDimension( 1 )->setRowHeight( 72 );

	 $drawing = new Drawing();
	 $drawing->setName( 'Mermaid Liveaboards Logo' );
	 $drawing->setDescription( 'Mermaid Liveaboards Logo' );
	 $drawing->setPath( 'assets/images/logo.png');
	 $drawing->setHeight( 85 );
	 $drawing->setOffsetX( -50 );
	 $drawing->setCoordinates( 'E1' );
	 $drawing->setWorksheet( $sheet );
	 
	 //-- SET value
	 $sheet->mergeCells( 'A2:I2' );
	 $sheet->setCellValue( 'A2', 'Schedule & Rate' );
	 
	 //-- SET style
	 $sheet->getStyle( 'A2:I2' )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FF001B48' );
	 $sheet->getStyle( 'A2' )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER );
	 $sheet->getStyle ('A2' )->getFont()->getColor()->setARGB( 'FFFFFFFF' );
	 $sheet->getStyle( 'A2' )->getFont()->setSize( 16 )->setBold( true );
			
	 $sheet->setCellValue( 'A3', 'Trip Code' );
	 $sheet->setCellValue( 'B3', 'Departure' );
	 $sheet->setCellValue( 'C3', 'Arrival' );
	 $sheet->setCellValue( 'D3', 'Destination' );
	 $sheet->setCellValue( 'E3', 'No. Dives' );
	 $sheet->setCellValue( 'F3', 'Master' );
	 $sheet->setCellValue( 'G3', 'Single' );
	 $sheet->setCellValue( 'H3', 'Deluxe' );
	 $sheet->setCellValue( 'I3', 'Lower' );
		
	 $row = 4;
	 
	 $iteneraries_list = the_post_list('iteneraries');
	 $iteneraries_text = '';
	 
	 foreach($posts as $d) :
		$post_title         = $d->post_title;
		$post_id            = $d->ID;
		$depart_date        = get_post_meta( $post_id, $prefix.'departure_date', true );
		$arrival_date       = get_post_meta( $post_id, $prefix.'arrival_date', true );
		$depart_time        = get_post_meta( $post_id, $prefix.'depart_time', true );
		$arrival_time       = get_post_meta( $post_id, $prefix.'arrival_time', true );
		$depart_point       = get_post_meta( $post_id, $prefix.'depart_point', true );
		$arrival_point      = get_post_meta( $post_id, $prefix.'arrival_point', true );
		$iteneraries        = get_post_meta( $post_id, $prefix.'iteneraries', true );
		$no_dives           = get_post_meta( $post_id, $prefix.'no_dives', true );
		$allotment_master   = get_post_meta( $post_id, $prefix.'allotment_master', true );
		$allotment_single   = get_post_meta( $post_id, $prefix.'allotment_single', true );
		$allotment_deluxe   = get_post_meta( $post_id, $prefix.'allotment_deluxe', true );
		$allotment_lower    = get_post_meta( $post_id, $prefix.'allotment_lower', true );
		
		if(!empty($iteneraries_list)):
			foreach($iteneraries_list as $key => $d):
			   if($key == $iteneraries):
				 $iteneraries_text = $d;
			   endif;
			endforeach;
		 endif; 
	 
		$sheet->setCellValue( 'A' . $row, $post_title );
		$sheet->setCellValue( 'B' . $row, $depart_date . ' : ' . $depart_time . ' : ' . $depart_point);
		$sheet->setCellValue( 'C' . $row, $arrival_date . ' : ' . $arrival_time . ' : ' . $arrival_point );
		$sheet->setCellValue( 'D' . $row, $iteneraries_text );
		$sheet->setCellValue( 'E' . $row, $no_dives );
		$sheet->setCellValue( 'F' . $row, $allotment_master );
		$sheet->setCellValue( 'G' . $row, $allotment_single );
		$sheet->setCellValue( 'H' . $row, $allotment_deluxe );
		$sheet->setCellValue( 'I' . $row, $allotment_lower );
				
		$row++;
	 endforeach;
	 
	 $writer = new Csv( $spread );
	 $writer->save('php://output');

	 exit;
	 
	 wp_reset_postdata();
}		
	
	

?>