<?php get_header(); ?>

<?php

if( isset( $_GET[ 'prm' ] ) && !empty( $_GET[ 'prm' ] ) )
{
    $prm = json_decode( base64_decode( $_GET[ 'prm' ] ), true );

    if( isset( $prm[ 'post_id' ] ) )
    {
        $post = get_post( $prm[ 'post_id' ] );

        if( $post->ID )
        {
            $invoice_number = get_post_meta( $post->ID, '_checkout_invoice_no', true);
            $guest_name     = get_post_meta( $post->ID, '_checkout_guest_name', true );

            if( $invoice_number == '' || $guest_name == '' )
            {
                $status = 'invalid';
            }
            else
            {
                $status_payment = get_post_meta( $post->ID, '_checkout_status_payment', true );
                $expired_date   = get_post_meta( $post->ID, '_checkout_expired_date', true );

                if( strtotime( $expired_date ) <= strtotime( date( 'Y-m-d' ) ) )
                {
                    $status = 'expired';
                }
                else
                {
                    if( strtolower( $status_payment ) == 'waiting payment' )
                    {
                        $status = 'valid';
                    }
                    else
                    {
                        $status = 'invalid';
                    }
                }
            }
        }
        else
        {
            $status = 'invalid';
        }
    }
    else
    {
        $status = 'invalid';
    }
}
else
{
    $status = 'invalid';
}

if( $status == 'valid' )
{
    ?>
    <section class="wrap-bank-trf-page">
        <div class="logo-mermaid">
            <a href="<?php echo site_url(); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/mermaid-logo.png" />
            </a>
        </div>
        <div class="container-page-bank-trf clearfix">            
            <div class="title-page">
                <h1 class="heading-default">Accept a Payment</h1>
                <p>Please complete form below to make a payment</p>
            </div>
            <div class="box-bank-trf-details">
                <?php

                $currency = $prm[ 'cc_currency' ];
                $amount   = $prm[ 'amount_' . $currency ];
                $amount   = round( $amount ) * 100;

                $server_name   = stripe_get_option( $prm[ 'form_type' ], 'server' );
                $secret_key    = stripe_get_option( $prm[ 'form_type' ], 'secret_key_' . $server_name );
                $published_key = stripe_get_option( $prm[ 'form_type' ], 'publishable_key_' . $server_name );

                $stripe = new \Stripe\StripeClient([
                    'api_key' => $secret_key
                ]);

                try
                {
                    $payment_intent = $stripe->paymentIntents->create([
                        'payment_method_types' => [ 'card' ],
                        'currency' => $currency,
                        'amount'   => $amount,
                    ]);

                    ?>
                    <form id="payment-form">
                        <div id="link-authentication-element"></div>
                        <br/>
                        <div id="payment-element"></div>
                        <button id="submit" class="button-default" style="border:none;">Pay Now</button>
                        <a class="try-other-method" href="<?php echo get_permalink( $prm[ 'post_id' ] ) ?>">Try Other Method?</a>
                    </form>
                    <?php
                }
                catch( Exception $e )
                {
                    ?>
                    <div class="text-center">
                        <p>There is something wrong on this payment at a moment, please try again later or chose another method by click link below</p>
                        <a class="button-default" style="display:inline-block; padding: 7px 20px; text-decoration: none; margin-top: 10px;" href="<?php echo get_permalink( $prm[ 'post_id' ] ) ?>">Choose Other Method?</a>
                    </div>
                    <?php
                }

                ?> 
            </div>
        </div>
        <script src="https://js.stripe.com/v3/"></script>
        <script>
            const stripe = Stripe('<?php echo $published_key; ?>', {
                locale: 'en'
            });

            const elements = stripe.elements({
                clientSecret: '<?php echo $payment_intent->client_secret; ?>'
            });

            const linkAuthenticationElement = elements.create('linkAuthentication');

            linkAuthenticationElement.mount('#link-authentication-element');

            const payementElement = elements.create('payment');

            payementElement.mount('#payment-element');

            const form = document.getElementById('payment-form');

            let submitted = false;

            form.addEventListener('submit', async (e) => {
                e.preventDefault();

                //-- Disable double submission of the form
                if( submitted )
                { 
                    return; 
                }

                submitted = true;

                form.querySelector('button').disabled = true;

                // Confirm the payment given the clientSecret
                // from the payment intent that was just created on
                // the server.
                const { error: stripeError } = await stripe.confirmPayment({
                    elements,
                    confirmParams: {
                        return_url: '<?php echo site_url( 'execute-stripe-data?prm=' . $_GET[ 'prm' ] ) ?>'
                    }
                });

                if( stripeError )
                {
                    //-- Reenable the form.
                    submitted = false;

                    form.querySelector('button').disabled = false;

                    return;
                }
            });
        </script>
    <?php
}
else
{
    ?>
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo">
                        <a href="<?php echo get_site_url(); ?>"><img data-src-static="<?php echo get_template_directory_uri()."/assets/images/mermaid-logo.png" ?>" /></a>
                    </div>
                    <?php

                    if( $status == 'expired' )
                    {
                        ?>
                        <h1 class="title-payment heading-default"><?= get_option( 'title_page_payment_link_expired' ) ?></h1>
                        <p><?= nl2br( get_option( 'description_page_payment_link_expired' ) ) ?></p>
                        <?php
                    }
                    else
                    {
                        ?>
                        <h1 class="title-payment heading-default"><?= get_option( 'title_page_payment_link_invalid' ) ?></h1>
                        <p><?= nl2br( get_option( 'description_page_payment_link_invalid' ) ) ?></p>
                        <?php
                    }

                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php
}

?>

<?php get_footer(); ?>