<?php
/*
Template Name: Page Mermaid
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php
        // SECTION TOP HOME PAGE
        include_once "layout/hero.php";
        include_once "layout/inquiry-form-popup.php";

        // INISIALISASI VARIBLE PAGE
        $title   = get_post_meta( get_the_ID(), '_boat_title_page', true );
        $title   = empty($title) ? '':   '<h1 class="heading-default">'.$title.'</h1>';
        $content = get_post_meta( get_the_ID(), '_boat_description_page', true );
        $content = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $content);
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = empty($content) ? '': '<div class="desc-about">'.$content.'</div>';
    ?>

    <div class="loader-spinner">
        <img src="<?php echo THEME_URL_ASSETS.'/images/loading.svg'; ?>" />
    </div>

    <!-- GALLERY POPUP -->
    <div class="gallery-popup">
        <div class="container-gallery">
            <h2 class="gallery-title"><?php the_title(); ?> Gallery</h2>
            <span class="close">Close</span>
            <div class="wrap-owl-slide">
                <div class="m-arrow m-prev"></div>
                <div class="owl-carousel owl-theme m-slide list-popup-boat"></div>
                <div class="m-arrow m-next"></div>
            </div>
        </div>
    </div>

    <!-- SECTION WELCOME -->
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        echo $title;
                        echo $content;
                    ?>
                </div>
            </div>
        </div>
    </section>

    <!-- SECTION MERMAID CABINS -->
    <section class="mermaid-cabins">
        <div class="bg-grey"></div>
        <div class="container-mermaid">
            <h2 class="title-section"><?php the_title(); ?> <?php get_label_string('Cabins'); ?></h2>
            <div class="line"></div>

            <div class="row">
                <?php list_mermaid_cabins(); ?>
            </div>
        </div>
    </section>

    <!-- SECTION MERMAID CABINS AMENITIES -->
    <section class="mermaid-cabins-amenities">
        <div class="container-mermaid">
            <h2 class="title-section"><?php the_title(); ?> <?php get_label_string('Cabins Amenities'); ?></h2>
            <div class="line"></div>
            <?php list_amenities_cabins(); ?>
        </div>
    </section>

    <!-- SECTION MERMAI SPECIFICATIONS -->
    <section class="wrap-mermaid-specifications">
        <div class="container-mermaid">
            <h2 class="title-section"><?php the_title(); ?> <?php get_label_string('Specifications'); ?></h2>
            <div class="line"></div>

            <div class="container">
                <div class="wrap-slide-boat-specifications">
                    <div class="inner">
                        <?php
                            $side_view_title = get_post_meta(get_the_ID(), '_boat_side_view_title', true);
                            $side_view_image = wp_get_attachment_image_src( get_post_meta(get_the_ID(), '_boat_side_view_image_id', true), 'full' );

                            $upper_deck_title = get_post_meta(get_the_ID(), '_boat_upper_deck_title', true);
                            $upper_deck_image = wp_get_attachment_image_src( get_post_meta(get_the_ID(), '_boat_upper_deck_image_id', true), 'full' );

                            $main_deck_title = get_post_meta(get_the_ID(), '_boat_main_deck_title', true);
                            $main_deck_image = wp_get_attachment_image_src( get_post_meta(get_the_ID(), '_boat_main_deck_image_id', true), 'full' );

                            $lower_deck_title = get_post_meta(get_the_ID(), '_boat_lower_deck_title', true);
                            $lower_deck_image = wp_get_attachment_image_src( get_post_meta(get_the_ID(), '_boat_lower_deck_image_id', true), 'full' );
                            
                            $specifications = array(
                                array('title' => $side_view_title, 'image' => $side_view_image[0], 'filter' => 'side-view'),
                                array('title' => $upper_deck_title, 'image' => $upper_deck_image[0], 'filter' => 'upper-deck'),
                                array('title' => $main_deck_title, 'image' => $main_deck_image[0], 'filter' => 'main-deck'),
                                array('title' => $lower_deck_title, 'image' => $lower_deck_image[0], 'filter' => 'lower-deck')
                            );

                            foreach($specifications as $spec):
                                echo '
                                <div class="list-boat-specifications" data-title="'.$spec['title'].'" data-filter="'.$spec['filter'].'">
                                    <img src="'.$spec['image'].'" />
                                </div>
                                ';
                            endforeach;
                        ?>
                    </div>
                    <div class="con-button-spec">
                        <button class="prev"></button>
                        <div class="title-boat-spec">
                            <h2></h2>
                            <a href="" class="other-spec">Click to see other decks</a>
                        </div>
                        <button class="next"></button>
                    </div> 
                </div>

                <div class="wrap-desc-boat-specifications">
                    <?php
                    $_boat_specification_side_view = get_post_meta(get_the_ID(), '_boat_specification_side_view', true);
                    $_boat_specification_upper_deck = get_post_meta(get_the_ID(), '_boat_specification_upper_deck', true);
                    $_boat_specification_main_deck = get_post_meta(get_the_ID(), '_boat_specification_main_deck', true);
                    $_boat_specification_lower_deck = get_post_meta(get_the_ID(), '_boat_specification_lower_deck', true);

                    $desc_specications = array(
                        array('class' => 'side-view', 'specifications' => $_boat_specification_side_view),
                        array('class' => 'upper-deck', 'specifications' => $_boat_specification_upper_deck),
                        array('class' => 'main-deck', 'specifications' => $_boat_specification_main_deck),
                        array('class' => 'lower-deck', 'specifications' => $_boat_specification_lower_deck),
                    );

                    foreach($desc_specications as $ds):
                        $specifications = $ds['specifications'];
                    ?>
                        <div class="list-desc-boat-spec <?php echo $ds['class']; ?>">
                            <?php
                                if(!empty($specifications)):
                                    foreach($specifications as $sp):
                                        $title = $sp['title'];
                                        $description = $sp['description'];
                                        echo '<h2>'.$title.'</h2>';
                                        echo '<div class="detail-list clearfix">'.$description.'</div>';
                                    endforeach;
                                endif;
                            ?>
                        </div>
                    <?php
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
    </section>

    <!-- SECTION MERMAID SAFETY FEATURES -->
    <section class="mermaid-safety-features">
        <div class="container-mermaid">
            <h2 class="title-section"><?php the_title(); ?> <?php get_label_string('Safety Features'); ?></h2>
            <div class="line"></div>
            <?php list_safety_features_boat(); ?>
        </div>
    </section>

    <!-- SECTION MERMAID SYSTEM -->
    <section class="mermaid-systems">
        <div class="container-mermaid">
            <h2 class="title-section"><?php the_title(); ?> <?php get_label_string('Systems'); ?></h2>
            <div class="line"></div>
            <?php list_systems_boat(); ?>
        </div>
    </section>

    <?php show_information_pack_schedule(); ?>

    <!-- SECTION MERMAID SCHEDULE DATE -->
    <section class="wrap-schedule-rate-table">
        <div class="container-mermaid">
            <h2 class="title-section"><?php the_title(); ?> <?php get_label_string('Upcoming Departures'); ?></h2>
            <div class="line"></div>
            <?php 
                $args = array(
                    'id'    => get_the_ID(),
                    'title' => get_the_title(),
                    'slug'  => $post->post_name
                );
                table_schedule_and_rate('boat', $args); 
            ?>
        </div>
    </section>

<?php endwhile;  ?>

<?php get_footer(); ?>