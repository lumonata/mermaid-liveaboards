<?php

require __DIR__  . '/vendor/autoload.php';

/*
| -------------------------------------------------------------------------------------
| Add custom endpoint to capture return data
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'add_frontend_endpoint' ) )
{
    function add_frontend_endpoint()
    {
        add_rewrite_endpoint( 'checkout-stripe', EP_PERMALINK );
        add_rewrite_endpoint( 'checkout-airwallex', EP_PERMALINK );
        
        add_rewrite_endpoint( 'execute-paypal-data', EP_PERMALINK );
        add_rewrite_endpoint( 'execute-stripe-data', EP_PERMALINK );
        add_rewrite_endpoint( 'execute-airwallex-data', EP_PERMALINK );
    }

    function add_frontend_endpoint_var( $query_vars )
    {
        $query_vars[] = 'checkout-stripe';
        $query_vars[] = 'checkout-airwallex';
        
        $query_vars[] = 'execute-paypal-data';
        $query_vars[] = 'execute-stripe-data';
        $query_vars[] = 'execute-airwallex-data';

        return $query_vars;
    }

    add_action( 'init', 'add_frontend_endpoint');
    add_filter( 'query_vars', 'add_frontend_endpoint_var' );
}

/*
| -------------------------------------------------------------------------------------
| Get payment URL base on choice and redirect it
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'checkout_payment' ) )
{
    function checkout_payment()
    {
        if( isset( $_POST[ 'payment' ] ) )
        {
            if( $_POST[ 'payment' ] == 'cc' )
            {
                $url = get_credit_card_payment_url();
            }
            elseif( $_POST[ 'payment' ] == 'paypal' )
            {
                $url = get_paypal_payment_url();
            }
            elseif( $_POST[ 'payment' ] == 'bank-trf' )
            {
                $url = get_bank_transfer_payment_url();
            }

            if( empty( $url ) )
            {                
                $url = get_permalink( $_POST[ 'post_id' ] );
            }

            if( wp_redirect( $url ) )
            {
                exit;
            }
        }
    }

    add_action( 'admin_post_checkout_payment', 'checkout_payment' );
    add_action( 'admin_post_nopriv_checkout_payment', 'checkout_payment' );
}

/*
| -------------------------------------------------------------------------------------
| Get bank transfer URL
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'get_bank_transfer_payment_url' ) )
{
    function get_bank_transfer_payment_url()
    {
        $id = 0;

        if( $_POST[ 'post_type' ] == 'checkout' )
        {
            $id = bank_get_option( 'cmc_bank_page_info' );
        }
        elseif( $_POST[ 'post_type' ] == 'checkout-2' )
        {
            $id = bank_get_option( 'mmc_bank_page_info' );
        }

        return get_permalink( $id );
    }
}

/*
| -------------------------------------------------------------------------------------
| Get PayPal API context
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'get_paypal_api_context' ) )
{
    function get_paypal_api_context( $form_type )
    {
        $server        = paypal_get_option( $form_type, 'server' );
        $client_id     = paypal_get_option( $form_type, 'client_id_' . $server );
        $client_secret = paypal_get_option( $form_type, 'client_secret_' . $server );

        if( $client_id != '' && $client_secret != '' )
        {
            try
            {
                $oauth   = new \PayPal\Auth\OAuthTokenCredential( $client_id, $client_secret );
                $context = new \PayPal\Rest\ApiContext( $oauth );

                $context->setConfig( array(
                    'mode' => $server,
                    'log.LogEnabled' => true,
                    'log.FileName' => '../PayPal.log',
                    'log.LogLevel' => 'DEBUG',
                    'cache.enabled' => true
                ));

                return $context;
            }
            catch( Exception $e )
            {
                return null;
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Get PayPal approval URL
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'get_paypal_payment_url' ) )
{
    function get_paypal_payment_url()
    {        
        $post_id   = $_POST[ 'post_id' ];
        $form_type = $_POST[ 'form_type' ];
        $currency  = $_POST[ 'paypal_currency' ];
        $total     = $_POST[ 'amount_' . $currency ];

        //-- Set query params
        $query_prms = base64_encode( json_encode( $_POST ) );

        //-- Set redirect url ( return / cancel )
        $return_url = site_url( 'execute-paypal-data/?prm=' . $query_prms );
        $cancel_url = get_permalink( $post_id );

        //-- Amount must in 2 decimal format
        if( $total > 0  )
        {
           $total = $total + ( ( $total * 1 ) / 100 );
        }

        //-- Currency code must in capital letter
        $currency = strtoupper( $currency );

        try
        {
            $redirect    = new \PayPal\Api\RedirectUrls();
            $transaction = new \PayPal\Api\Transaction();
            $payment     = new \PayPal\Api\Payment();
            $amount      = new \PayPal\Api\Amount();
            $payer       = new \PayPal\Api\Payer();

            //-- SET context
            $context = get_paypal_api_context( $form_type );

            //-- SET payment method
            $payer->setPaymentMethod( 'paypal' );

            //-- SET payment amount
            $amount->setCurrency( $currency );
            $amount->setTotal( $total );

            //-- SET transaction data
            $transaction->setAmount( $amount );

            //-- SET redirect url
            $redirect->setReturnUrl( $return_url );
            $redirect->setCancelUrl( $cancel_url );

            //-- SET payment data
            $payment->setTransactions( array( $transaction ) );
            $payment->setRedirectUrls( $redirect );
            $payment->setIntent( 'sale' );
            $payment->setPayer( $payer );
            $payment->create( $context );

            return $payment->getApprovalLink();
        }
        catch( Exception $e )
        {
            return null;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Get & execute PayPal return data
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'execute_paypal_data' ) )
{
    function execute_paypal_data()
    {
        global $wp_query;

        if( $wp_query->query_vars[ 'name' ] != 'execute-paypal-data' )
        {
            return;
        }

        if( isset( $_GET[ 'paymentId' ] ) && isset( $_GET[ 'prm' ] ) )
        {
            $payment_id = $_GET[ 'paymentId' ];
            $payer_id   = $_GET[ 'PayerID' ];

            try
            {
                //-- GET and decode query param
                $prm = json_decode( base64_decode( $_GET[ 'prm' ] ), true );

                if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
                {                    
                    //-- SET context
                    $context = get_paypal_api_context( $prm[ 'form_type' ] );

                    //-- GET payment data               
                    $payment = \PayPal\Api\Payment::get( $payment_id, $context );

                    try
                    {
                        $execution = new \PayPal\Api\PaymentExecution();

                        //-- GET invoice data
                        $post = get_post( $prm[ 'post_id' ] );

                        //-- GET invoice id
                        $invoice_id = get_post_meta( $post->ID, '_checkout_invoice_no', true );
                        
                        //-- SET payer ID
                        $execution->setPayerId( $payer_id );
                    
                        //-- EXECUTE payment data
                        $payment->execute( $execution, $context );

                        //-- GET payer info
                        $payer = $payment->getPayer()->payer_info;

                        //-- GET transaction info
                        $transaction = $payment->getTransactions();

                        //-- UPDATE payment data
                        update_post_meta( $post->ID, '_checkout_status_payment', 'Waiting Verification');
                        update_post_meta( $post->ID, '_checkout_payment_type', 'Paypal');

                        update_post_meta( $post->ID, '_checkout_payer_email', $payer->email );
                        update_post_meta( $post->ID, '_checkout_payer_id', $payer->payer_id );

                        $sale_id  = '';
                        $currency = '';
                        $amount   = 0;

                        foreach( $transaction as $trans )
                        {
                            $sources  = $trans->getRelatedResources();
                            $currency = $trans->amount->currency;
                            $amount   = $trans->amount->total;

                            foreach( $sources as $source )
                            {
                                $sale_dt = $source->getSale();
                                $sale_id = $sale_dt->getId();

                                update_post_meta( $post->ID, '_checkout_transaction_id', $sale_id );
                            }

                            update_post_meta( $post->ID, '_checkout_amount_pay', $amount );
                            update_post_meta( $post->ID, '_checkout_currency', $currency );
                        }

                        //-- SET GA Ecommerce param
                        set_ga_eccommerce( $post, $amount, $currency );

                        //-- SEND email notification to client
                        send_email_payment_success( 'paypal', array(
                            'payment_id'     => $payment->getId(),
                            'payer_id'       => $payer->payer_id,
                            'payer_email'    => $payer->email,
                            'invoice_id'     => $invoice_id,
                            'currency'       => $currency,
                            'transaction_id' => $sale_id,
                            'amount_pay'     => $amount
                        ));

                        wp_redirect( site_url( 'thank-you-for-payment' ) );
                    }
                    catch( Exception $e )
                    {
                        wp_redirect( site_url( 'payment-failed' ) );
                    }
                }
                else
                {
                    wp_redirect( site_url( 'payment-failed' ) );
                }
            }
            catch( Exception $e )
            {
                wp_redirect( site_url( 'payment-failed' ) );
            }
        }
        else
        {
            wp_redirect( site_url( 'payment-failed' ) );
        }

        exit;
    }

    add_action( 'template_redirect', 'execute_paypal_data' );
}

/*
| -------------------------------------------------------------------------------------
| Get credit card payment URL ( stripe / airwallex )
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'get_credit_card_payment_url' ) )
{
    function get_credit_card_payment_url()
    {
        $form_type = $_POST[ 'form_type' ];
        $currency  = $_POST[ 'cc_currency' ];

        //-- CHECK vendor in use : stripe / airwallex
        $cc_account = payment_get_option( $form_type, $form_type . '_' . $currency . '_account' );

        if( $cc_account == 1 )
        {
            return get_stripe_payment_url();
        }
        else
        {
            return get_airwallex_payment_url();
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Get stripe payment URL
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'get_stripe_payment_url' ) )
{
    function get_stripe_payment_url()
    {
        $param = base64_encode( json_encode( $_POST ) );

        return site_url( 'checkout-stripe/?prm=' . $param );
    }
}

/*
| -------------------------------------------------------------------------------------
| Get stripe checkout form
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'checkout-stripe' ) )
{
    function checkout_stripe()
    {
        global $wp_query;

        if( $wp_query->query_vars[ 'name' ] != 'checkout-stripe' )
        {
            return;
        }

        if( isset( $_GET[ 'prm' ] ) && !empty( $_GET[ 'prm' ] ) )
        {
            $wp_query->is_404 = false;
            $wp_query->is_page = true;
            
            status_header( 200 );

            add_filter( 'body_class', function( $classes ){
                array_push( $classes, 'page-checkout-payment' );
                
                return $classes;
            });

            add_filter( 'template_include', function() {
                return get_template_directory() . '/page-checkout-stripe.php';
            });
        }
    }

    add_action( 'template_redirect', 'checkout_stripe' );
}

/*
| -------------------------------------------------------------------------------------
| Get & execute Stripe return data
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'execute_stripe_data' ) )
{
    function execute_stripe_data()
    {
        global $wp_query;

        if( $wp_query->query_vars[ 'name' ] != 'execute-stripe-data' )
        {
            return;
        }

        if( isset( $_GET[ 'prm' ] ) && isset( $_GET[ 'payment_intent' ] ) )
        {
            status_header( 200 );

            $prm = json_decode( base64_decode( $_GET[ 'prm' ] ), true );

            $server_name   = stripe_get_option( $prm[ 'form_type' ], 'server' );
            $secret_key    = stripe_get_option( $prm[ 'form_type' ], 'secret_key_' . $server_name );
            $published_key = stripe_get_option( $prm[ 'form_type' ], 'publishable_key_' . $server_name );

            $stripe = new \Stripe\StripeClient([
                'api_key' => $secret_key
            ]);

            try
            {
                $payment_intent = $stripe->paymentIntents->retrieve( $_GET[ 'payment_intent' ] );

                if( $payment_intent->status == 'succeeded' )
                {
                    //-- GET invoice data
                    $post = get_post( $prm[ 'post_id' ] );

                    //-- GET invoice id
                    $invoice_id = get_post_meta( $post->ID, '_checkout_invoice_no', true );
                    
                    //-- GET customer data
                    $amount_pay  = $payment_intent->amount / 100;
                    $payer_id    = '';
                    $payer_email = '';

                    if( isset( $payment_intent->charges->data ) )
                    {
                        foreach( $payment_intent->charges->data as $dt )
                        {
                            $payer_id    = $dt->payment_method;
                            $payer_email = $dt->billing_details->email;
                        }
                    }

                    //-- UPDATE payment data
                    update_post_meta( $post->ID, '_checkout_status_payment', 'Waiting Verification');
                    update_post_meta( $post->ID, '_checkout_payment_type', 'Stripe');

                    update_post_meta( $post->ID, '_checkout_transaction_id', $payment_intent->id );
                    update_post_meta( $post->ID, '_checkout_currency', $payment_intent->currency );
                    update_post_meta( $post->ID, '_checkout_amount_pay', $amount_pay );
                            
                    update_post_meta( $post->ID, '_checkout_payer_email', $payer_email );
                    update_post_meta( $post->ID, '_checkout_payer_id', $payer_id );

                    //-- SET GA Ecommerce param
                    set_ga_eccommerce( $post, $amount_pay, $payment_intent->currency );

                    //-- SEND email notification to client
                    send_email_payment_success( 'stripe', array(
                        'currency'       => $payment_intent->currency,
                        'transaction_id' => $payment_intent->id,
                        'payer_email'    => $payer_email,
                        'amount_pay'     => $amount_pay,
                        'invoice_id'     => $invoice_id,
                        'payer_id'       => $payer_id,
                    ));

                    wp_redirect( site_url( 'thank-you-for-payment' ) );
                }
                else
                {
                    wp_redirect( site_url( 'payment-failed' ) );
                }
            }
            catch( Exception $e )
            {
                wp_redirect( site_url( 'payment-failed' ) );
            }
        }
        else
        {
            wp_redirect( site_url( 'payment-failed' ) );
        }

        exit;
    }

    add_action( 'template_redirect', 'execute_stripe_data' );
}

/*
| -------------------------------------------------------------------------------------
| Get airwallex payment URL
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'get_airwallex_payment_url' ) )
{
    function get_airwallex_payment_url()
    {
        $param = base64_encode( json_encode( $_POST ) );

        return site_url( 'checkout-airwallex/?prm=' . $param );
    }
}

/*
| -------------------------------------------------------------------------------------
| Get airwallex checkout form
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'checkout-airwallex' ) )
{
    function checkout_airwallex()
    {
        global $wp_query;

        if( $wp_query->query_vars[ 'name' ] != 'checkout-airwallex' )
        {
            return;
        }

        if( isset( $_GET[ 'prm' ] ) && !empty( $_GET[ 'prm' ] ) )
        {
            $wp_query->is_404 = false;
            $wp_query->is_page = true;
            
            status_header( 200 );

            add_filter( 'body_class', function( $classes ){
                array_push( $classes, 'page-checkout-payment' );
                
                return $classes;
            });

            add_filter( 'template_include', function() {
                return get_template_directory() . '/page-checkout-airwallex.php';
            });
        }
    }

    add_action( 'template_redirect', 'checkout_airwallex' );
}

/*
| -------------------------------------------------------------------------------------
| Generate request id for Airwallex API
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'airwallex_generate_request_id' ) )
{
    function airwallex_generate_request_id()
    {
        if( function_exists('com_create_guid') === true)
        {
            return trim( com_create_guid(), '{}' );
        }

        return sprintf( '%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535) );
    }
}

/*
| -------------------------------------------------------------------------------------
| Airwallex API calls to generate authentication token
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'airwallex_generate_token' ) )
{
    function airwallex_generate_token( $form_type )
    {
        $server    = airwallex_get_option( $form_type, 'server' );
        $api_key   = airwallex_get_option( $form_type, 'api_key_' . $server );
        $client_id = airwallex_get_option( $form_type, 'client_id_' . $server );
        $api_url   = airwallex_get_option( 'airwallex_call_url', $server . '_url' );

        if( !empty( $api_url ) && !empty( $client_id ) && !empty( $api_key ) )
        {
            $url = sprintf( '%s/api/v1/authentication/login', $api_url );
            $res = wp_remote_post( $url, array(
                'headers' => array(
                    'x-client-id' => $client_id,
                    'x-api-key' => $api_key
                ),
            ));

            if( !is_wp_error( $res ) )
            {
                $data = json_decode( wp_remote_retrieve_body( $res ), true );

                if( isset( $data[ 'token' ] ) )
                {
                    return $data[ 'token' ];
                }
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Airwallex API calls to create payment intent
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'airwallex_create_payment_intent' ) )
{
    function airwallex_create_payment_intent( $token, $prm )
    {
        $currency  = $prm[ 'cc_currency' ];
        $amount    = $prm[ 'amount_' . $currency ];

        $form_type = $prm[ 'form_type' ];
        $post_id   = $prm[ 'post_id' ];

        //-- Currency code must in capital letter
        $currency = strtoupper( $currency );

        //-- Round amount
        $amount = round( $amount );

        //-- Get option
        $server  = airwallex_get_option( $form_type, 'server' );
        $api_url = airwallex_get_option( 'airwallex_call_url', $server . '_url' );
        $invoice = get_post_meta( $post_id, '_checkout_invoice_no', true );

        if( empty( $invoice ) )
        {
            $invoice = 'INV-' . airwallex_generate_request_id();
        }

        $url = sprintf( '%s/api/v1/pa/payment_intents/create', $api_url );
        $res = wp_remote_post( $url, array(
            'body' => json_encode( array(
                'return_url'        => site_url( 'execute-airwallex-data/?prm=' . base64_encode( json_encode( $prm ) ) ),
                'request_id'        => airwallex_generate_request_id(),
                'currency'          => $currency,
                'merchant_order_id' => $invoice,
                'amount'            => $amount,
            )),
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ),
        ));

        if( !is_wp_error( $res ) )
        {
            $data = json_decode( wp_remote_retrieve_body( $res ), true );

            if( isset( $data[ 'id' ] ) && isset( $data[ 'client_secret' ] ) )
            {
                return $data;
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Airwallex API calls to create payment intent
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'airwallex_retrieve_payment_intent' ) )
{
    function airwallex_retrieve_payment_intent( $token, $prm, $intent_id )
    {
        $currency  = $prm[ 'cc_currency' ];
        $amount    = $prm[ 'amount_' . $currency ];

        $form_type = $prm[ 'form_type' ];
        $post_id   = $prm[ 'post_id' ];

        //-- Get option
        $server  = airwallex_get_option( $form_type, 'server' );
        $api_url = airwallex_get_option( 'airwallex_call_url', $server . '_url' );
        $invoice = get_post_meta( $post_id, '_checkout_invoice_no', true );

        $url = sprintf( '%s/api/v1/pa/payment_intents/%s', $api_url, $intent_id );
        $res = wp_remote_get( $url, array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ),
        ));

        if( !is_wp_error( $res ) )
        {
            $data = json_decode( wp_remote_retrieve_body( $res ), true );

            return $data;
        }
    }
}


/*
| -------------------------------------------------------------------------------------
| Get & execute Airwallex return data
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'execute_airwallex_data' ) )
{
    function execute_airwallex_data()
    {
        global $wp_query;

        if( $wp_query->query_vars[ 'name' ] != 'execute-airwallex-data' )
        {
            return;
        }

        if( isset( $_GET[ 'prm' ] ) && !empty( $_GET[ 'prm' ] ) )
        {
            status_header( 200 );

            //-- Get parameter
            $prm = json_decode( base64_decode( $_GET[ 'prm' ] ), true );

            //-- Generate Token
            $token  = airwallex_generate_token( $prm[ 'form_type' ] );

            //-- Retrieve Payment Intent
            $intent = airwallex_retrieve_payment_intent( $token, $prm, $_GET[ 'id' ] );

            if( isset( $intent[ 'status' ] ) && strtolower( $intent[ 'status' ] ) == 'succeeded' )
            {
                //-- GET invoice data
                $post = get_post( $prm[ 'post_id' ] );

                //-- GET invoice id
                $invoice_id = get_post_meta( $post->ID, '_checkout_invoice_no', true );
                
                //-- GET customer data
                $payer_id = '';

                if( isset( $intent[ 'latest_payment_attempt' ][ 'payment_method' ][ 'card' ][ 'name' ] ) )
                {
                    $payer_id = $intent[ 'latest_payment_attempt' ][ 'payment_method' ][ 'card' ][ 'name' ];
                }

                //-- UPDATE payment data
                update_post_meta( $post->ID, '_checkout_status_payment', 'Waiting Verification');
                update_post_meta( $post->ID, '_checkout_payment_type', 'Airwallex');

                update_post_meta( $post->ID, '_checkout_transaction_id', $intent[ 'id' ] );
                update_post_meta( $post->ID, '_checkout_currency', $intent[ 'currency' ] );
                update_post_meta( $post->ID, '_checkout_amount_pay', $intent[ 'amount' ] );                        
                update_post_meta( $post->ID, '_checkout_payer_id', $payer_id );                        
                update_post_meta( $post->ID, '_checkout_payer_email', '' );

                //-- SET GA Ecommerce param
                set_ga_eccommerce( $post, $intent[ 'amount' ], $intent[ 'currency' ] );

                //-- SEND email notification to client
                send_email_payment_success( 'airwallex', array(
                    'currency'       => $intent[ 'currency' ],
                    'amount_pay'     => $intent[ 'amount' ],
                    'transaction_id' => $intent[ 'id' ],
                    'invoice_id'     => $invoice_id,
                    'payer_id'       => $payer_id,
                ));

                wp_redirect( site_url( 'thank-you-for-payment' ) );
            }
            else
            {
                wp_redirect( site_url( 'payment-failed' ) );
            }
        }
        else
        {
            wp_redirect( site_url( 'payment-failed' ) );
        }

        exit;
    }

    add_action( 'template_redirect', 'execute_airwallex_data' );
}

/*
| -------------------------------------------------------------------------------------
| Set parameter forn GA Ecommerce
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'set_ga_eccommerce' ) )
{
    function set_ga_eccommerce( $post, $amount, $currency )
    {
        $revenue = convertCurrency( $currency, 'IDR', $amount );

        if( $revenue > 0 )
        {
            //-- GET invoice meta data                            
            $iteneraries  = get_post_meta( $post->ID, '_checkout_iteneraries', true );
            $trip_code    = get_post_meta( $post->ID, '_checkout_trip_code', true );
            $cruise       = get_post_meta( $post->ID, '_checkout_cruise_list' );
            $boat_id      = get_post_meta( $post->ID, '_checkout_boat', true );
            
            $trip_code = json_decode( base64_decode( $trip_code ) );

            if( $trip_code !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $trip_code = $trip_code->post_title;
            }

            $name = array( html_entity_decode( get_the_title( $iteneraries ), ENT_COMPAT, 'UTF-8' ), $trip_code );
            $boat = html_entity_decode( get_the_title( $boat_id ), ENT_COMPAT, 'UTF-8' );

            if( isset( $cruise[ 0 ][ 'cabin_type' ] ) )
            {
                array_push( $name, $cruise[0]['cabin_type'] );
            }

            if( session_status() !== PHP_SESSION_ACTIVE )
            {
                session_start();
            }
                
            $_SESSION['gecommerce'] = array( 
                'addtransitem' => array( 
                    'name'     => implode( ' - ', $name ),
                    'id'       => $post->post_title,
                    'sku'      => $trip_code,
                    'price'    => $amount,
                    'category' => $boat,
                    'quantity' => 1
                ),            
                'addtransparam' => array( 
                    'affiliation' => 'Mermaid Liveaboards',
                    'id'          => $post->post_title,
                    'revenue'     => round( $revenue )
                )
            );
        }
    }
}