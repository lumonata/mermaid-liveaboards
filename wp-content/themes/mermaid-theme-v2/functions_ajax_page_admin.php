<?php

add_action( 'wp_ajax_nopriv_converter_curency', 'converter_curency' );
add_action( 'wp_ajax_converter_curency', 'converter_curency' );

function converter_curency()
{
    $get_curr = $_POST['get_value'];

    $response = wp_remote_request( 'https://xecd-api-ssl.xe.com/v1/convert_from.json', array(
        'headers' => array(
            'Authorization' => 'Basic ' . base64_encode( 'ptlumonata963225277:e9215pt6kmvbql93gpvvo' )
        ),
        'body' => array(
            'from'   => 'idr',
            'to'     => 'eur',
            'amount' => $get_curr
        )
    ) );

    $body = wp_remote_retrieve_body( $response );
    $data = json_decode( $body, true );

    if( isset( $data['to'][0]['mid'] ) )
    {
        $result = number_format( $data['to'][0]['mid'], 2, '.', ' ' );
    }
    else
    {
        $result = 0;
    }

    wp_send_json( $result );

    exit;
}

/*
| -------------------------------------------------------------------------------------
| Function ajax untuk admin page menambahkan itenerary
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_add-row-schedule', 'add_row_schedule' );
function add_row_schedule()
{
    $result['status'] = "faileddd";
    $result['msg']    = "<h3>Something went wrong, please try again later</h3>";

    if( isset( $_POST['action'] ) && $_POST['action'] == "add-row-schedule" ):
        $prefix = "_schedule_data_";

        $post_id = $_POST['post_id'];

        // UPDATE POST META DEPARTURE DATE

        // update_post_meta( $post_id_schedule, $prefix.'arrival_date', '' );
        // update_post_meta( $post_id_schedule, $prefix.'destination', '' );
        // update_post_meta( $post_id_schedule, $prefix.'no_dives', '' );
        // update_post_meta( $post_id_schedule, $prefix.'price_master', '' );
        // update_post_meta( $post_id_schedule, $prefix.'allotment_master', '' );
        // update_post_meta( $post_id_schedule, $prefix.'price_single', '' );
        // update_post_meta( $post_id_schedule, $prefix.'allotment_single', '' );
        // update_post_meta( $post_id_schedule, $prefix.'price_deluxe', '' );
        // update_post_meta( $post_id_schedule, $prefix.'allotment_deluxe', '' );
        // update_post_meta( $post_id_schedule, $prefix.'price_lower', '' );
        // update_post_meta( $post_id_schedule, $prefix.'allotment_lower', '' );

        $iteneraries_list = the_post_list( 'iteneraries' );
        $post_id_schedule = "";

        $html = "";

        for( $i = 1;$i <= 5;$i++ ):

            $select_iteneraries = "";

            $post_schedule_data = array(
                'post_title'   => '',
                'post_content' => '',
                'post_status'  => 'publish',
                'post_type'    => 'schedule_data',
                'post_parent'  => $post_id
            );
            $post_id_schedule = wp_insert_post( $post_schedule_data );

            update_post_meta( $post_id_schedule, $prefix.'departure_date', '' );

            if( !empty( $iteneraries_list ) ):
                $select_iteneraries .= '
                <select name="iteneraries" class="field_schedule" data-key="'.$post_id_schedule.'" data-name="iteneraries" data-field="iteneraries" data-table="post_meta">
                    <option value="">Select Destination</option>
                ';
                foreach( $iteneraries_list as $key => $d ):
                    $select_iteneraries .= '<option value="'.$key.'">'.$d.'</option>';
                endforeach;
                $select_iteneraries .= '</select>';
            endif;


            $html .= '
                <tr class="row_'.$post_id_schedule.'" data-key="'.$post_id_schedule.'">
                    <td>
                        <input type="text" name="trip_code" class="width110 field_schedule" placeholder="Enter Trip Code" data-key="'.$post_id_schedule.'" data-name="trip_code" data-field="post_title" data-table="post" data-action-type="insert" />
                    </td>

                    <td class="depart_point_wrap">
                        <input type="date" class="width120 field_schedule field_date" name="depart_date" id="depart_date_'.$post_id_schedule.'" value="" data-key="'.$post_id_schedule.'" data-name="departure_date" data-field="departure_date" data-table="post_meta" />
                        <select name="depart_point" class="field_schedule field_select" data-key="'.$post_id_schedule.'" data-name="depart_point" data-field="depart_point" data-table="post_meta">
                            <option value="">Select Depart Point</option>
                            <option data-id="100116" value="Benoa Pier Bali">Benoa Pier Bali</option>
                            <option data-id="100031" value="Maumere Flores">Maumere Flores</option>
                            <option data-id="100034" value="Sorong West Papua">Sorong West Papua</option>
                            <option data-id="100040" value="Ambon">Ambon</option>
                            <option data-id="100039" value="Lembeh Sulawesi">Lembeh Sulawesi</option>
                        </select>
                        <select name="depart_time" class="field_schedule field_select" data-key="'.$post_id_schedule.'" data-name="depart_time" data-field="depart_time" data-table="post_meta">
                            <option value="">Select Depart Time</option>
                            <option value="13:00-15:00">13:00-15:00</option>
                            <option value="12:00-14:00">12:00-14:00</option>
                            <option value="07:00-14:00">07:00-14:00</option>
                            <option value="10:00-12:00">10:00-12:00</option>
                            <option value="10:00-16:00">10:00-16:00</option>
                        </select>
                        <input type="hidden" name="depart_point_id" value="">
                    </td>

                    <td class="arrival_point_wrap">
                        <input type="date" class="width120 field_schedule field_date" name="arrival_date" id="arrival_date_'.$post_id_schedule.'" value="" data-key="'.$post_id_schedule.'" data-name="arrival_date" data-field="arrival_date" data-table="post_meta" />
                        <select name="arrival_point" class="field_schedule field_select" data-key="'.$post_id_schedule.'" data-name="arrival_point" data-field="arrival_point" data-table="post_meta">
                            <option value="">Select Arrival Point</option>
                            <option data-id="100116" value="Benoa Pier Bali">Benoa Pier Bali</option>
                            <option data-id="100116" value="Maumere Flores">Maumere Flores</option>
                            <option data-id="100040"  value="Sorong West Papua">Sorong West Papua</option>
                            <option data-id="100040"  value="Ambon">Ambon</option>
                            <option data-id="100040"  value="Lembeh Sulawesi">Lembeh Sulawesi</option>
                        </select>
                        <select name="arrival_time" class="field_schedule ield_select" data-key="'.$post_id_schedule.'" data-name="arrival_time" data-field="arrival_time" data-table="post_meta">
                            <option value="">Select Arrival Point</option>
                            <option value="08:00-08:30">08:00-08:30</option>
                        </select>
                    </td>

                    <td>'.$select_iteneraries.'</td>

                    <td>
                        <input type="text" name="no_dives" class="width60 field_schedule" data-key="'.$post_id_schedule.'" data-name="no_dives" data-field="no_dives" data-table="post_meta" />
                    </td>

                    <td class="price_allotment">
                        <input type="text" name="price_master" class="width70 field_schedule" placeholder="Price (€)" data-key="'.$post_id_schedule.'" data-name="price_master" data-field="price_master" data-table="post_meta" />
                        <br><br>
                        <input type="text" name="allotment_master" class="width70 field_schedule" placeholder="Allotment" data-key="'.$post_id_schedule.'" data-name="allotment_master" data-field="allotment_master" data-table="post_meta" />
                    </td>

                    <td class="price_allotment">
                        <input type="text" name="price_single" class="width70 field_schedule" placeholder="Price (€)" data-key="'.$post_id_schedule.'" data-name="price_single" data-field="price_single" data-table="post_meta" />
                        <br><br>
                        <input type="text" name="allotment_single" class="width70 field_schedule" placeholder="Allotment" data-key="'.$post_id_schedule.'" data-name="allotment_single" data-field="allotment_single" data-table="post_meta" />
                    </td>

                    <td class="price_allotment">
                        <input type="text" name="price_deluxe" class="width70 field_schedule" placeholder="Price (€)" data-key="'.$post_id_schedule.'" data-name="price_deluxe" data-field="price_deluxe" data-table="post_meta" />
                        <br><br>
                        <input type="text" name="allotment_deluxe" class="width70 field_schedule" placeholder="Allotment" data-key="'.$post_id_schedule.'" data-name="allotment_deluxe" data-field="allotment_deluxe" data-table="post_meta" />
                    </td>

                    <td class="price_allotment">
                        <input type="text" name="price_lower" class="width70 field_schedule" placeholder="Price (€)" data-key="'.$post_id_schedule.'" data-name="price_lower" data-field="price_lower" data-table="post_meta" />
                        <br><br>
                        <input type="text" name="allotment_lower" class="width70 field_schedule" placeholder="Allotment" data-key="'.$post_id_schedule.'" data-name="allotment_lower" data-field="allotment_lower" data-table="post_meta" />
                    </td>

                    <td>
                        <button type="button" class="delete_schedule_data" data-key="'.$post_id_schedule.'"><img src="'.THEME_URL_ASSETS.'/images/admin-icons/delete.png" /></button>
                        <span class="icon-validation icon-row-'.$post_id_schedule.'"></span>
                    </td>
                </tr>
            ';
        endfor;

        $result['status'] = "success";
        $result['html']   = $html;
    endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk mengubah data schedule
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_save-form-schedule', 'save_form_schedule' );
function save_form_schedule()
{
    $result['status'] = "failed";
    $result['msg']    = "<h3>Something went wrong, please try again later</h3>";

    if( isset( $_POST['action'] ) && $_POST['action'] == "save-form-schedule" ):
        $action_type = $_POST['action_type'];
        $name        = $_POST['name'];
        $field       = $_POST['field'];
        $table       = $_POST['table'];
        $value       = $_POST['val'];
        $key         = $_POST['key'];
        $boat        = $_POST['boat'];
        $prefix      = "_schedule_data_";

        if( ( $field == "departure_date" && !empty( $value ) ) || ( $field == "arrival_date" && !empty( $value ) ) ):
            $value = date( "Y-m-d", strtotime( $value ) );
        endif;


        if( $table == "post" ):

            $schedule_post = array(
                'ID'         => $key,
                'post_title' => $value,
            );
            wp_update_post( $schedule_post );

            $result['status'] = "success_update";
            $result['msg']    = "Data has been saved";

        elseif( $table == "post_meta" ):


            update_post_meta( $key, $prefix.$field, $value );

            $result['status'] = "success_update";
            $result['msg']    = "Data has been saved";

        endif;

    update_post_meta( $key, $prefix."boat", $boat );

    endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk admin page menambahkan itenerary
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_add_schedule_data', 'add_schedule_data' );
function add_schedule_data()
{
    $result['status'] = "failed";
    $result['msg']    = "<h3>Something went wrong, please try again later</h3>";

    if( isset( $_POST['action'] ) && $_POST['action'] == "add_schedule_data" ):
        $trip_code        = $_POST['trip_code'];
        $departure_date   = $_POST['departure_date'];
        $arrival_date     = $_POST['arrival_date'];
        $iteneraries      = $_POST['iteneraries'];
        $price_master     = $_POST['price_master'];
        $allotment_master = $_POST['allotment_master'];
        $price_single     = $_POST['price_single'];
        $allotment_single = $_POST['allotment_single'];
        $price_deluxe     = $_POST['price_deluxe'];
        $allotment_deluxe = $_POST['allotment_deluxe'];
        $price_lower      = $_POST['price_lower'];
        $allotment_lower  = $_POST['allotment_lower'];
        $price_single     = $_POST['price_single'];
        $post_id          = $_POST['post_id'];

        $my_post = array(
            'post_title'   => wp_strip_all_tags( $trip_code ),
            'post_content' => '',
            'post_status'  => 'publish',
            'post_type'    => 'schedule_data',
            'post_parent'  => $post_id
        );

        $post_id_schedule = wp_insert_post( $my_post );

        // UPDATE POST META DEPARTURE DATE
        $prefix = "_schedule_data_";
        update_post_meta( $post_id_schedule, $prefix.'departure_date', $_POST['price_master'] );
    endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk admin page menghapus row schedule data
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_delete-row-schedule', 'delete_row_schedule' );
function delete_row_schedule()
{
    $result['status'] = "failed";
    $result['msg']    = "<h3>Something went wrong, please try again later</h3>";

    if( isset( $_POST['action'] ) && $_POST['action'] == "delete-row-schedule" ):
        $prefix = "_schedule_data_";

        $key = $_POST['key'];

        wp_delete_post( $key );
        delete_post_meta( $key, $prefix.'boat' );
        delete_post_meta( $key, $prefix.'departure_date' );
        delete_post_meta( $key, $prefix.'arrival_date' );
        delete_post_meta( $key, $prefix.'iteneraries' );
        delete_post_meta( $key, $prefix.'no_dives' );
        delete_post_meta( $key, $prefix.'price_master' );
        delete_post_meta( $key, $prefix.'allotment_master' );
        delete_post_meta( $key, $prefix.'price_single' );
        delete_post_meta( $key, $prefix.'allotment_single' );
        delete_post_meta( $key, $prefix.'price_deluxe' );
        delete_post_meta( $key, $prefix.'allotment_deluxe' );
        delete_post_meta( $key, $prefix.'price_lower' );
        delete_post_meta( $key, $prefix.'allotment_lower' );

        $result['status'] = "success";
        $result['msg']    = "Success! Your data has been deleted";
        $result['key']    = $key;
    endif;

    wp_send_json( $result );
}


/*
| -------------------------------------------------------------------------------------
| Function ajax untuk mendapatkan slug boat berdasarkan id
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_ajax_get-slug-boat', 'get_slug_boat' );
function get_slug_boat()
{
    $result['status'] = "failed";
    $result['msg']    = "";

    if( isset( $_POST['action'] ) && $_POST['action'] == "get-slug-boat" ):
        $boat             = $_POST['boat'];
        $boat_title       = get_the_title( $boat );
        $boat_sanitize    = sanitize_title( $boat_title );
        $result['status'] = "success";
        $result['msg']    = $boat_sanitize;
    endif;

    wp_send_json( $result );
}
