<?php get_header(); ?>

    <?php if ( have_posts() ) : the_post(); ?>

        <?php include_once "layout/hero.php"; ?>

        <?php 
            $post_image     = get_the_post_thumbnail_url();
            $linkshare 		= get_the_permalink();
        ?>

        <section class="wrap-news-detail">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="top-news">
                            <h4>POSTED ON <?php the_date("F d, Y"); ?></h4>
                            <h1 class="heading-default"><?php the_title(); ?></h1>
                            <h3>by <?php echo ucwords(get_the_author()); ?></h3>
                        </div>

                        <div class="body-news">
                            <?php
                                $post_content   = get_the_content();
                                $post_content   = apply_filters('the_content', $post_content);

                                echo $post_content;
                            ?>
                        </div>
                        
                        <div class="bottom-news clearfix">
                            <h4>SHARE THIS ARTICLE</h4>
                            <ul class="share">
                                <li><a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $linkshare; ?>" class="facebook-share"></a></li>
                                <li><a target="_blank" href="http://twitter.com/share?text=Share This on Twitter" class="twitter-share"></a></li>
                                <li><a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php echo $linkshare; ?>&media=<?php echo $post_image; ?>" class="pinterest-share"></a></li>
                                <li><a target="_blank" href="https://api.whatsapp.com/send?text=<?php echo $linkshare; ?>" class="whatsapp-share"></a></li>
                            </ul>
                        </div>
                    
                    </div>
                </div>
            </div>
        </section>


        <section class="wrap-other-news">
            <div class="container-mermaid">
                <h2 class="title-section"><?php get_label_string('Other News'); ?></h2>
                <div class="line"></div>

                <div class="container-other-post-list">
                    <div class="inner clearfix">
                        <?php
                        $args = array(
                            'post_status'   => 'publish',
                            'post_type'     => 'post',
                            'posts_per_page' => 5,
                            'post__not_in'  => array(get_the_ID()),
                        );
                        $query_dest = new WP_Query($args);

                        while ($query_dest->have_posts()): 
                            $query_dest->the_post();
                            
                            $post_title         = get_the_title();
                            $post_image         = get_the_post_thumbnail_url(get_the_ID(), 'large');
                            $post_image_medium  = get_the_post_thumbnail_url(get_the_ID(), 'medium_large');
                            $post_link          = get_permalink();
                        ?>
                            <a href="<?php echo $post_link; ?>">
                                <div class="list-other-post b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $post_image; ?>" data-src-small="<?php echo $post_image_medium; ?>">
                                    <div class="title-other-dest">
                                        <h4><?php get_label_string('Discover'); ?></h4>
                                        <h2><?php echo $post_title; ?></h2>
                                    </div>
                                    <span class="button-arrow" data-label="<?php get_label_string('Learn More'); ?>"></span>
                                </div>
                            </a>
                        <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        </section>

    <?php endif; ?>

<?php get_footer(); ?>