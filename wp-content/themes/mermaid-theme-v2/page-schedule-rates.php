<?php
/*
Template Name: Schedule Rates Page
*/
?>

<?php

?>

<?php get_header(); 
session_start();
?>

<?php if ( have_posts() ) : the_post(); ?>

    <?php 
    include_once "layout/hero.php";
    include_once "layout/inquiry-form-popup.php";
	

    $end_url            = end(cek_url());
    $boat               = get_page_by_path($end_url, '', 'boat');
    $title              = get_the_title();
    $title_html         = empty($title) ? '' : '<h1 class="heading-default desktop">'.$title.'</h1>';
    $title_mobile_html  = "";

    if( strpos( $title, "Mermaid I " ) !== false):
        $title_mobile = explode($title, "Mermaid I");
        $title_mobile_next = ltrim(str_replace("Mermaid I", "", $title));
        $title_mobile_html = '<h1 class="heading-default mobile">Mermaid I <br/>'.$title_mobile_next.'</h1>';
    elseif( strpos( $title, "Mermaid II " ) !== false):
        $title_mobile = explode($title, "Mermaid II");
        $title_mobile_next = ltrim(str_replace("Mermaid II", "", $title));
        $title_mobile_html = '<h1 class="heading-default mobile">Mermaid II <br/>'.$title_mobile_next.'</h1>';
    endif;

    show_information_pack_schedule();
    ?>

    <section class="welcome-about">
        <div class="container">
            <?php 
                echo $title_html;
                echo $title_mobile_html;
            ?>
        </div>
    </section>

    <!-- SECTION SCHDULE ITENERARIES -->
    <section class="wrap-schedule-rate-table">
        <div class="container-mermaid">
            <?php 
                $args = array(
                    'id'    => $boat->ID,
                    'title' => $boat->post_title,
                    'slug'  => $boat->post_name
                );
                table_schedule_and_rate('schedule_rate', $args); 
            ?>
        </div>
    </section>

<?php endif; ?>

<?php get_footer(); ?>