<?php
add_action( 'admin_init', 'add_meta_boxes' );
function add_meta_boxes() 
{
    // add_meta_box( 'price_allotment', 'Price & Allotment', 'price_rates_fields', 'schedule_rates' );
    add_meta_box( 'schedule_rate_table', 'Schedule & Rate', 'schedule_rates_fields', 'schedule_rates' );
    add_meta_box( 'location_dive_spot', 'Location Dive Spot', 'dive_spot_fields', 'dive_spots' );
    add_meta_box( 'location_destination', 'Location Destination', 'destination_fields', 'destination' );
    // add_meta_box( 'itenerary_table', 'Itenerary', 'itenerary_fields', 'iteneraries' );
}


/*
| -------------------------------------------------------------------------------------
| Schedule & Rate Meta Box For Post Type : schedule_rates
| -------------------------------------------------------------------------------------
*/
function schedule_rates_fields() 
{
    wp_enqueue_script('field-date-js', get_template_directory_uri() . '/assets/js/Field_Date.js', array('jquery-ui-datepicker'));
    wp_enqueue_style('jquery-ui-css', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

    global $post;
?>
    <input type="hidden" name="schedule_nonce" value="<?php echo wp_create_nonce( basename( __FILE__ ) ); ?>" />

    <div class="wrap-schedule-rate">

        <div class="container-schedule-rate">

            <table class="table-metabox table-schedule">
                <thead>
                    <tr>
                        <th class="width110">Trip Code</th>
                        <th class="width140">Departure</th>
                        <th class="width140">Arrival</th>
                        <th class="width100">Destination</th>
                        <th class="width60">No Dives</th>
                        <th class="width70">Master</th>
                        <th class="width70">Single</th>
                        <th class="width70">Deluxe</th>
                        <th class="width70">Lower</th>
                        <th class="width70">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $iteneraries_list = the_post_list('iteneraries');

                        $prefix = "_schedule_data_";

                        $args = array(
                            'post_status'   => 'publish',
                            'post_type'     => 'schedule_data',
                            'numberposts'   => -1,
                            'post_parent'   => $post->ID,
                            'order'         => 'ASC',
                            'orderby'       => 'meta_value',
                            'meta_key'      => $prefix.'departure_date'
                        );

                        $posts = get_posts($args);

                        foreach($posts as $d):
                            $post_title         = $d->post_title;
                            $post_id            = $d->ID;
                            $depart_date        = get_post_meta( $post_id, $prefix.'departure_date', true );
                            $arrival_date       = get_post_meta( $post_id, $prefix.'arrival_date', true );
                            $depart_time        = get_post_meta( $post_id, $prefix.'depart_time', true );
                            $arrival_time       = get_post_meta( $post_id, $prefix.'arrival_time', true );
                            $depart_point       = get_post_meta( $post_id, $prefix.'depart_point', true );
                            $arrival_point      = get_post_meta( $post_id, $prefix.'arrival_point', true );
                            $iteneraries        = get_post_meta( $post_id, $prefix.'iteneraries', true );
                            $no_dives           = get_post_meta( $post_id, $prefix.'no_dives', true );
                            $price_master       = get_post_meta( $post_id, $prefix.'price_master', true );
							$price_master_dsc   = get_post_meta( $post_id, $prefix.'price_master_dsc', true );
                            $allotment_master   = get_post_meta( $post_id, $prefix.'allotment_master', true );
                            $price_single       = get_post_meta( $post_id, $prefix.'price_single', true );
							$price_single_dsc   = get_post_meta( $post_id, $prefix.'price_single_dsc', true );
                            $allotment_single   = get_post_meta( $post_id, $prefix.'allotment_single', true );
                            $price_deluxe       = get_post_meta( $post_id, $prefix.'price_deluxe', true );
							$price_deluxe_dsc   = get_post_meta( $post_id, $prefix.'price_deluxe_dsc', true );
                            $allotment_deluxe   = get_post_meta( $post_id, $prefix.'allotment_deluxe', true );
                            $price_lower        = get_post_meta( $post_id, $prefix.'price_lower', true );
							$price_lower_dsc    = get_post_meta( $post_id, $prefix.'price_lower_dsc', true );
                            $allotment_lower    = get_post_meta( $post_id, $prefix.'allotment_lower', true );
                    ?>
                            <tr class="row_<?php echo $post_id; ?>" data-key="<?php echo $post_id; ?>">

                                <td>
                                    <input type="text" name="trip_code" class="width110 field_schedule" placeholder="Enter Trip Code" value="<?php echo $post_title; ?>" data-key="<?php echo $post_id; ?>" data-name="trip_code" data-field="post_title" data-table="post" />
                                </td>

                                <td class="depart_point_wrap">
                                    <input type="text" class="width140 field_schedule field_date" name="depart_date" id="depart_date_<?php echo $post_id; ?>" value="<?php echo empty( $depart_date ) ? '' : date( 'd-m-Y', strtotime( $depart_date ) ); ?>" data-key="<?php echo $post_id; ?>" data-name="departure_date" data-field="departure_date" data-table="post_meta" />
                                    <select name="depart_point" class="field_schedule field_select" data-key="<?php echo $post_id; ?>" data-name="depart_point" data-field="depart_point" data-table="post_meta" autocomplete="off">
                                        <option value="">Select Depart Point</option>
                                        <option data-id="100116" value="Benoa Pier Bali" <?php echo $depart_point == 'Benoa Pier Bali' ? 'selected' : '' ?>>Benoa Pier Bali</option>
                                        <option data-id="100031" value="Maumere Flores" <?php echo $depart_point == 'Maumere Flores' ? 'selected' : '' ?>>Maumere Flores</option>
                                        <option data-id="100034" value="Sorong West Papua" <?php echo $depart_point == 'Sorong West Papua' ? 'selected' : '' ?>>Sorong West Papua</option>
                                        <option data-id="100040" value="Ambon" <?php echo $depart_point == 'Ambon' ? 'selected' : '' ?>>Ambon</option>
                                        <option data-id="100039" value="Lembeh Sulawesi" <?php echo $depart_point == 'Lembeh Sulawesi' ? 'selected' : '' ?>>Lembeh Sulawesi</option>
                                    </select>
                                    <select name="depart_time" class="field_schedule field_select" data-key="<?php echo $post_id; ?>" data-name="depart_time" data-field="depart_time" data-table="post_meta" autocomplete="off">
                                        <option value="">Select Depart Time</option>
                                        <option value="13:00-15:00" <?php echo $depart_time == '13:00-15:00' ? 'selected' : '' ?>>13:00-15:00</option>
                                        <option value="12:00-14:00" <?php echo $depart_time == '12:00-14:00' ? 'selected' : '' ?>>12:00-14:00</option>
                                        <option value="07:00-14:00" <?php echo $depart_time == '07:00-14:00' ? 'selected' : '' ?>>07:00-14:00</option>
                                        <option value="10:00-12:00" <?php echo $depart_time == '10:00-12:00' ? 'selected' : '' ?>>10:00-12:00</option>
                                        <option value="10:00-16:00" <?php echo $depart_time == '10:00-16:00' ? 'selected' : '' ?>>10:00-16:00</option>
                                    </select>
                                </td>

                                <td class="arrival_point_wrap">
                                    <input type="text" class="width140 field_schedule field_date" name="arrival_date" id="arrival_date_<?php echo $post_id; ?>" value="<?php echo empty( $arrival_date ) ? '' : date( 'd-m-Y', strtotime( $arrival_date ) ); ?>" data-key="<?php echo $post_id; ?>" data-name="arrival_date" data-field="arrival_date" data-table="post_meta" />
                                    <select name="arrival_point" class="field_schedule field_select" data-key="<?php echo $post_id; ?>" data-name="arrival_point" data-field="arrival_point" data-table="post_meta" autocomplete="off">
                                        <option value="">Select Arrival Point</option>
                                        <option data-id="100116" value="Benoa Pier Bali" <?php echo $arrival_point == 'Benoa Pier Bali' ? 'selected' : '' ?>>Benoa Pier Bali</option>
                                        <option data-id="100031" value="Maumere Flores" <?php echo $arrival_point == 'Maumere Flores' ? 'selected' : '' ?>>Maumere Flores</option>
                                        <option data-id="100034" value="Sorong West Papua" <?php echo $arrival_point == 'Sorong West Papua' ? 'selected' : '' ?>>Sorong West Papua</option>
                                        <option data-id="100040" value="Ambon" <?php echo $arrival_point == 'Ambon' ? 'selected' : '' ?>>Ambon</option>
                                        <option data-id="100039" value="Lembeh Sulawesi" <?php echo $arrival_point == 'Lembeh Sulawesi' ? 'selected' : '' ?>>Lembeh Sulawesi</option>
                                    </select>
                                    <select name="arrival_time" class="field_schedule field_select" data-key="<?php echo $post_id; ?>" data-name="arrival_time" data-field="arrival_time" data-table="post_meta" autocomplete="off">
                                        <option value="">Select Arrival Time</option>
                                        <option value="08:00-08:30" <?php echo $arrival_time == '08:00-08:30' ? 'selected' : '' ?>>08:00-08:30</option>
                                    </select>
                                </td>

                                <td>
                                    <?php
                                    $select_iteneraries = "";
                                    if(!empty($iteneraries_list)):
                                        $select_iteneraries .= '
                                        <select name="iteneraries" class="field_schedule" data-key="'.$post_id.'" data-name="iteneraries" data-field="iteneraries" data-table="post_meta">
                                            <option value="">Select Destination</option>
                                        ';
                                        
                                        foreach($iteneraries_list as $key => $d):
                                            if($key == $iteneraries):
                                                $select_iteneraries .= '<option value="'.$key.'" selected="selected">'.$d.'</option>';
                                            else:
                                                $select_iteneraries .= '<option value="'.$key.'">'.$d.'</option>';
                                            endif;
                                        endforeach;
                                        $select_iteneraries .= '</select>';
                                    endif;
                                    echo $select_iteneraries;
                                    ?>
                                </td>

                                <td>
                                    <input type="text" name="no_dives" class="width60 field_schedule" value="<?php echo $no_dives; ?>" data-key="<?php echo $post_id; ?>" data-name="no_dives" data-field="no_dives" data-table="post_meta" />
                                </td>

                                 <td class="price_allotment">
									<p style="margin-bottom: 5px">Price</p>
                                    <input type="text" name="price_master" class="width70 field_schedule" placeholder="Price (€)" value="<?php echo $price_master; ?>" data-key="<?php echo $post_id; ?>" data-name="price_master" data-field="price_master" data-table="post_meta" />
									<br>
									<br>
									<p style="margin-bottom: 5px">Allotment</p>
                                    <input type="text" name="allotment_master" class="width70 field_schedule" placeholder="Allotment" value="<?php echo $allotment_master; ?>" data-key="<?php echo $post_id; ?>" data-name="allotment_master" data-field="allotment_master" data-table="post_meta" />
									<br>
									<br>
									<p style="margin-bottom: 5px">Discount(%)</p>
                                    <input type="text" name="price_master_dsc" class="width70 field_schedule" placeholder="Discount" value="<?php echo empty($price_master_dsc) ? '0' : $price_master_dsc; ?>" data-key="<?php echo $post_id; ?>" data-name="price_master_dsc" data-field="price_master_dsc" data-table="post_meta" />
                                </td>
								
                                <td class="price_allotment">
									<p style="margin-bottom: 5px">Price</p>
                                    <input type="text" name="price_single" class="width70 field_schedule" placeholder="Price (€)" value="<?php echo $price_single; ?>" data-key="<?php echo $post_id; ?>" data-name="price_single" data-field="price_single" data-table="post_meta" />
									<br>
									<br>
									<p style="margin-bottom: 5px">Allotment</p>
                                    <input type="text" name="allotment_single" class="width70 field_schedule" placeholder="Allotment" value="<?php echo $allotment_single; ?>" data-key="<?php echo $post_id; ?>" data-name="allotment_single" data-field="allotment_single" data-table="post_meta" />
									<br>
									<br>
									<p style="margin-bottom: 5px">Discount(%)</p>
                                    <input type="text" name="price_single_dsc" class="width70 field_schedule" placeholder="Discount" value="<?php echo empty($price_single_dsc) ? '0' : $price_single_dsc; ?>" data-key="<?php echo $post_id; ?>" data-name="price_single_dsc" data-field="price_single_dsc" data-table="post_meta" />
							   </td>

                                <td class="price_allotment">
									<p style="margin-bottom: 5px">Price</p>
                                    <input type="text" name="price_deluxe" class="width70 field_schedule" placeholder="Price (€)" value="<?php echo $price_deluxe; ?>" data-key="<?php echo $post_id; ?>" data-name="price_deluxe" data-field="price_deluxe" data-table="post_meta" />
									<br>
									<br>
									<p style="margin-bottom: 5px">Allotment</p>
                                    <input type="text" name="allotment_deluxe" class="width70 field_schedule" placeholder="Allotment" value="<?php echo $allotment_deluxe; ?>" data-key="<?php echo $post_id; ?>" data-name="allotment_deluxe" data-field="allotment_deluxe" data-table="post_meta" />
									<br>
									<br>
									<p style="margin-bottom: 5px">Discount(%)</p>
                                    <input type="text" name="price_deluxe_dsc" class="width70 field_schedule" placeholder="Discount" value="<?php echo empty($price_deluxe_dsc) ? '0' : $price_deluxe_dsc ; ?>" data-key="<?php echo $post_id; ?>" data-name="price_deluxe_dsc" data-field="price_deluxe_dsc" data-table="post_meta" />
                                
							   </td>
                                <td class="price_allotment">
									<p style="margin-bottom: 5px">Price</p>
                                    <input type="text" name="price_lower" class="width70 field_schedule" placeholder="Price (€)" value="<?php echo $price_lower; ?>" data-key="<?php echo $post_id; ?>" data-name="price_lower" data-field="price_lower" data-table="post_meta" />
									<br>
									<br>
									<p style="margin-bottom: 5px">Allotment</p>
                                    <input type="text" name="allotment_lower" class="width70 field_schedule" placeholder="Allotment" value="<?php echo $allotment_lower; ?>" data-key="<?php echo $post_id; ?>" data-name="allotment_lower" data-field="allotment_lower" data-table="post_meta" />
									<br>
									<br>
									<p style="margin-bottom: 5px">Discount(%)</p>
                                    <input type="text" name="price_lower_dsc" class="width70 field_schedule" placeholder="Discount" value="<?php echo empty($price_lower_dsc) ? '0' : $price_lower_dsc; ?>" data-key="<?php echo $post_id; ?>" data-name="price_lower_dsc" data-field="price_lower_dsc" data-table="post_meta" />
                                
							   </td>

                                <td>
                                    <button type="button" class="delete_schedule_data" data-key="<?php echo $post_id; ?>"><img src="<?php echo THEME_URL_ASSETS.'/images/admin-icons/delete.png'; ?>" /></button>
                                    <span class="icon-validation icon-row-<?php echo $post_id; ?>"></span>
                                </td>

                            </tr>
                    <?php
                        endforeach;
                    ?>
                </tbody>

            </table>

            <button type="button" class="button button-primary button-add-row">Add Row</button>
            <div class="clear"></div>

            <!-- <span class="icon-validation"></span> -->

        </div>
        <!-- <button type="button" name="add-data-schedule" class="button button-primary">Save Data</button> -->
        <div class="clear"></div>
    </div>
<?php
}

/*
| -------------------------------------------------------------------------------------
| PRICE & RATES Meta Box For Post Type : schedule_rates
| -------------------------------------------------------------------------------------
*/
function price_rates_fields() 
{
    global $post;
    
    // PRICE DATA
    $price_master   = get_post_meta( $post->ID, '_schedule_rates_price_master', true );
    $price_single   = get_post_meta( $post->ID, '_schedule_rates_price_single', true );
    $price_deluxe   = get_post_meta( $post->ID, '_schedule_rates_price_deluxe', true );
    $price_lower    = get_post_meta( $post->ID, '_schedule_rates_price_lower', true );

    // ALLOTMENT DATA
    $allotment_master   = get_post_meta( $post->ID, '_schedule_rates_allotment_master', true );
    $allotment_single   = get_post_meta( $post->ID, '_schedule_rates_allotment_single', true );
    $allotment_deluxe   = get_post_meta( $post->ID, '_schedule_rates_allotment_deluxe', true );
    $allotment_lower    = get_post_meta( $post->ID, '_schedule_rates_allotment_lower', true );
?>
    <input type="hidden" name="price_nonce" value="<?php echo wp_create_nonce( basename( __FILE__ ) ); ?>" />

    <table class="table-metabox">
        <thead>
            <tr>
                <th>Type</th>
                <th>Master</th>
                <th>Single</th>
                <th>Deluxe</th>
                <th>Lower</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>PRICE (€)</td>
                <td><input type="text" name="price_master" value="<?php echo $price_master; ?>" /></td>
                <td><input type="text" name="price_single" value="<?php echo $price_single; ?>" /></td>
                <td><input type="text" name="price_deluxe" value="<?php echo $price_deluxe; ?>" /></td>
                <td><input type="text" name="price_lower" value="<?php echo $price_lower; ?>" /></td>
            </tr>
            <tr>
                <td>ALLOTMENT</td>
                <td><input type="text" name="allotment_master" value="<?php echo $allotment_master; ?>" /></td>
                <td><input type="text" name="allotment_single" value="<?php echo $allotment_single; ?>" /></td>
                <td><input type="text" name="allotment_deluxe" value="<?php echo $allotment_deluxe; ?>" /></td>
                <td><input type="text" name="allotment_lower" value="<?php echo $allotment_lower; ?>" /></td>
            </tr>
        </tbody>
    </table>
<?php
}

add_action( 'save_post', 'save_price_rates_fields' );
function save_price_rates_fields( $post_id ) 
{
    // only run this for schedule_rates
    if ( 'schedule_rates' != get_post_type( $post_id ) ):
        return $post_id;
    endif;

    // verify nonce
    if ( empty( $_POST['price_nonce'] ) || !wp_verify_nonce( $_POST['price_nonce'], basename( __FILE__ ) ) ):
        return $post_id;
    endif;

    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ):
        return $post_id;
    endif;

    // check permissions
    if ( !current_user_can( 'edit_post', $post_id ) ):
        
        return $post_id;
    endif;

    // save
    update_post_meta( $post_id, '_schedule_rates_price_master', $_POST['price_master'] );
    update_post_meta( $post_id, '_schedule_rates_price_single', $_POST['price_single'] );
    update_post_meta( $post_id, '_schedule_rates_price_deluxe', $_POST['price_deluxe'] );
    update_post_meta( $post_id, '_schedule_rates_price_lower', $_POST['price_lower'] );

    update_post_meta( $post_id, '_schedule_rates_allotment_master', $_POST['allotment_master'] );
    update_post_meta( $post_id, '_schedule_rates_allotment_single', $_POST['allotment_single'] );
    update_post_meta( $post_id, '_schedule_rates_allotment_deluxe', $_POST['allotment_deluxe'] );
    update_post_meta( $post_id, '_schedule_rates_allotment_lower', $_POST['allotment_lower'] );
}


/*
| -------------------------------------------------------------------------------------
| Location Dive Spot Meta Box For Post Type : dive_spots
| -------------------------------------------------------------------------------------
*/
function dive_spot_fields() 
{
    global $post;

    // LOCATION DATA
    $latitude   = get_post_meta( $post->ID, '_dive_spot_latitude', true );
    $longitude  = get_post_meta( $post->ID, '_dive_spot_longitude', true );

?>
    <input type="hidden" name="location_nonce" value="<?php echo wp_create_nonce( basename( __FILE__ ) ); ?>" />

    <div class="cmb2-wrap form-table">
        <div id="cmb2-metabox-destinations_metabox" class="cmb2-metabox cmb-field-list">
            <div class="cmb-row cmb-type-text cmb2-id--dive-spot-location table-layout" data-fieldtype="text">
                <div class="cmb-th">
                    <label for="_dive_spot_location">Search Location</label>
                </div>
                <div class="cmb-td">
                    <input type="text" class="regular-text" name="_dive_spot_location" id="_dive_spot_location" placeholder="" value="">
                </div>
            </div>

            <div id="us2" style="width: 100%; height: 500px;"></div>

            <div class="cmb-row cmb-type-text cmb2-id--dive-spot-latitude table-layout" data-fieldtype="text">
                <div class="cmb-th">
                    <label for="_dive_spot_latitude">Latitude</label>
                </div>
                <div class="cmb-td">
                    <input type="text" class="regular-text" name="_dive_spot_latitude" id="_dive_spot_latitude" value="<?php echo $latitude; ?>">
                </div>
            </div>
            <div class="cmb-row cmb-type-text cmb2-id--dive-spot-longitude table-layout" data-fieldtype="text">
                <div class="cmb-th">
                    <label for="_dive_spot_longitude">Longitude</label>
                </div>
                <div class="cmb-td">
                    <input type="text" class="regular-text" name="_dive_spot_longitude" id="_dive_spot_longitude" value="<?php echo $longitude; ?>">
                </div>
            </div>
        </div>
    </div>

    <?php
    $latitude_maps = empty($latitude) ? '-2.4153185' : $latitude;
    $longitude_maps = empty($longitude) ? '108.8502843' : $longitude;
    ?>
    <script>
    $('#us2').locationpicker({
        location: {
            latitude: <?php echo $latitude_maps; ?>, 
            longitude: <?php echo $longitude_maps; ?>,
        },
        zoom: 5,
        scrollwheel: false,
        radius: 100,
        inputBinding: {
            latitudeInput: $('#_dive_spot_latitude'),
            longitudeInput: $('#_dive_spot_longitude'),
            locationNameInput: $('#_dive_spot_location')
        },
        enableAutocomplete: true
    });
    </script>
<?php
}

add_action( 'save_post', 'save_location_data_fields' );
function save_location_data_fields( $post_id ) 
{
    // only run this for schedule_rates
    if ( 'dive_spots' != get_post_type( $post_id ) ):
        return $post_id;
    endif;

    // verify nonce
    if ( empty( $_POST['location_nonce'] ) || !wp_verify_nonce( $_POST['location_nonce'], basename( __FILE__ ) ) ):
        return $post_id;
    endif;

    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ):
        return $post_id;
    endif;

    // check permissions
    if ( !current_user_can( 'edit_post', $post_id ) ):
        
        return $post_id;
    endif;

    // save
    update_post_meta( $post_id, '_dive_spot_latitude', $_POST['_dive_spot_latitude'] );
    update_post_meta( $post_id, '_dive_spot_longitude', $_POST['_dive_spot_longitude'] );
}


/*
| -------------------------------------------------------------------------------------
| Iteneraries Table Meta Box For Post Type : iteneraries
| -------------------------------------------------------------------------------------
*/
function itenerary_fields() 
{
    global $post;
?>
    <input type="hidden" name="iteneraries_nonce" value="<?php echo wp_create_nonce( basename( __FILE__ ) ); ?>" />
<?php
    $destination_list = the_post_list('boat');
    foreach($destination_list as $key => $value):
        $sanitize = sanitize_title($value);
?>      
        <div class="wrap-table-metabox-itenerary" id="wrap-<?php echo $sanitize; ?>-form">
            <h3><?php echo $value; ?></h3>
            <!-- <form action="" method="POST" name="itenerary-form-add"> -->
            <div class="cmb2-wrap form-table <?php echo $sanitize; ?>-form">
                <input type="hidden" name="dest_id" value="<?php echo $key; ?>" />
                <input type="hidden" name="postition_array" value="" />
                <input type="hidden" name="action_itenerary" value="add-itenerary" />
                <div id="cmb2-metabox-destinations_metabox" class="cmb2-metabox cmb-field-list">
                    <div class="cmb-row cmb-type-text cmb2-id--dive-spot-location table-layout" data-fieldtype="text">
                        <div class="cmb-th">
                            <label for="_iteneraries_day">Day</label>
                        </div>
                        <div class="cmb-td">
                            <input type="text" class="regular-text" name="_iteneraries_day" id="_iteneraries_day" placeholder="" value="">
                        </div>
                    </div>
                    <div class="cmb-row cmb-type-text cmb2-id--dive-spot-location table-layout" data-fieldtype="text">
                        <div class="cmb-th">
                            <label for="_iteneraries_day">Sub Day</label>
                        </div>
                        <div class="cmb-td">
                            <input type="text" class="regular-text" name="_iteneraries_sub_day" id="_iteneraries_sub_day" placeholder="" value="">
                        </div>
                    </div>
                    <div class="cmb-row cmb-type-text cmb2-id--dive-spot-latitude table-layout" data-fieldtype="text">
                        <div class="cmb-th">
                            <label for="_iteneraries_detail">Detail Itinerary</label>
                        </div>
                        <div class="cmb-td">
                            <textarea name="_iteneraries_detail" id="_iteneraries_detail" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="cmb-row cmb-type-text cmb2-id--dive-spot-longitude table-layout" data-fieldtype="text">
                        <div class="cmb-th">
                            <label for="_iteneraries_dives">Dives</label>
                        </div>
                        <div class="cmb-td">
                            <input type="text" class="regular-text" name="_iteneraries_dives" id="_iteneraries_dives" value="">
                        </div>
                    </div>
                    <div class="cmb-row cmb-type-text cmb2-id--dive-spot-longitude table-layout" data-fieldtype="text">
                        <div class="cmb-th">
                        </div>
                        <div class="cmb-td">
                            <button type="button" class="button-metabox" data-key="<?php echo $sanitize; ?>" name="save_itenerary">Save Itinerary</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- </form> -->
            <table class="table-metabox" id="<?php echo $key; ?>">
                <thead>
                    <tr>
                        <th width="15%">Day</th>
                        <th width="15%">Sub Day</th>
                        <th>Itinerary With MV <?php echo $value; ?></th>
                        <th width="10%">Dives</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $itenerary_data = get_post_meta($post->ID, '_itenerary_data_'.$key, true);
                        $itenerary_data = !empty($itenerary_data) ? json_decode($itenerary_data, true) : '';

                        if(!empty($itenerary_data) && is_array($itenerary_data)):
                            foreach($itenerary_data as $key => $data):
                                $dives = !empty($data['dives']) ? $data['dives'] : '-';
                    ?>
                            <tr>
                                <td><?php echo $data['day'] ?></td>
                                <td><?php echo $data['sub_day'] ?></td>
                                <td><?php echo $data['detail'] ?></td>
                                <td><?php echo $dives ?></td>
                                <td>
                                    <button type="button" class="btn-metabox btn-edit-metabox" data-boat="<?php echo $sanitize; ?>" data-position_array="<?php echo $key; ?>" data-post_id="<?php echo $post->ID; ?>" data-day="<?php echo $data['day']; ?>" data-sub_day="<?php echo $data['sub_day']; ?>" data-detail="<?php echo $data['detail']; ?>" data-dives="<?php echo $dives; ?>"></button>
                                    <button type="button" class="btn-metabox btn-delete-metabox" data-boat="<?php echo $sanitize; ?>" data-position_array="<?php echo $key; ?>" data-post_id="<?php echo $post->ID; ?>" data-day="<?php echo $data['day']; ?>" data-sub_day="<?php echo $data['sub_day']; ?>" data-detail="<?php echo $data['detail']; ?>" data-dives="<?php echo $dives; ?>"></button>
                                </td>
                            </tr>
                    <?php
                            endforeach;
                        endif;
                    ?>
                </tbody>
            </table>
        </div>
<?php
    endforeach;
}


/*
| -------------------------------------------------------------------------------------
| Location Destination Meta Box For Post Type : destinations
| -------------------------------------------------------------------------------------
*/
function destination_fields()
{
    global $post;

    // LOCATION DATA
    $latitude       = get_post_meta( $post->ID, '_destination_latitude', true );
    $longitude      = get_post_meta( $post->ID, '_destination_longitude', true );
    $zoom_level     = get_post_meta( $post->ID, '_destination_zoom_level', true );
?>
    <input type="hidden" name="location_nonce" value="<?php echo wp_create_nonce( basename( __FILE__ ) ); ?>" />

    <div class="cmb2-wrap form-table">
        <div id="cmb2-metabox-destinations_metabox" class="cmb2-metabox cmb-field-list">
            <div class="cmb-row cmb-type-text cmb2-id--dive-spot-location table-layout" data-fieldtype="text">
                <div class="cmb-th">
                    <label for="_destination_location">Search Location</label>
                </div>
                <div class="cmb-td">
                    <input type="text" class="regular-text" name="_destination_location" id="_destination_location" placeholder="" value="">
                </div>
            </div>

            <div id="us2" style="width: 100%; height: 500px;"></div>

            <div class="cmb-row cmb-type-text cmb2-id--dive-spot-latitude table-layout" data-fieldtype="text">
                <div class="cmb-th">
                    <label for="_destination_latitude">Latitude</label>
                </div>
                <div class="cmb-td">
                    <input type="text" class="regular-text" name="_destination_latitude" id="_destination_latitude" value="<?php echo $latitude; ?>">
                </div>
            </div>
            <div class="cmb-row cmb-type-text cmb2-id--dive-spot-longitude table-layout" data-fieldtype="text">
                <div class="cmb-th">
                    <label for="_destination_longitude">Longitude</label>
                </div>
                <div class="cmb-td">
                    <input type="text" class="regular-text" name="_destination_longitude" id="_destination_longitude" value="<?php echo $longitude; ?>">
                </div>
            </div>
            <div class="cmb-row cmb-type-text cmb2-id--dive-spot-longitude table-layout" data-fieldtype="text">
                <div class="cmb-th">
                    <label for="_destination_longitude">Zoom Level</label>
                </div>
                <div class="cmb-td">
                    <input type="text" class="regular-text" name="_destination_zoom_level" id="_destination_zoom_level" value="<?php echo $zoom_level; ?>">
                </div>
            </div>
        </div>
    </div>
    <?php
    $latitude_maps = empty($latitude) ? '-2.4153185' : $latitude;
    $longitude_maps = empty($longitude) ? '108.8502843' : $longitude;
    ?>
    <script>
    $('#us2').locationpicker({
        location: {
            latitude: <?php echo $latitude_maps; ?>, 
            longitude: <?php echo $longitude_maps; ?>,
        },
        zoom: 5,
        radius: 100,
        inputBinding: {
            latitudeInput: $('#_destination_latitude'),
            longitudeInput: $('#_destination_longitude'),
            locationNameInput: $('#_destination_location')
        },
        enableAutocomplete: true
        });
    </script>
<?php
}

add_action( 'save_post', 'save_location_data_destination_fields' );
function save_location_data_destination_fields( $post_id ) 
{
    // only run this for schedule_rates
    if ( 'destination' != get_post_type( $post_id ) ):
        return $post_id;
    endif;

    // verify nonce
    if ( empty( $_POST['location_nonce'] ) || !wp_verify_nonce( $_POST['location_nonce'], basename( __FILE__ ) ) ):
        return $post_id;
    endif;

    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ):
        return $post_id;
    endif;

    // check permissions
    if ( !current_user_can( 'edit_post', $post_id ) ):
        
        return $post_id;
    endif;

    // save
    update_post_meta( $post_id, '_destination_latitude', $_POST['_destination_latitude'] );
    update_post_meta( $post_id, '_destination_longitude', $_POST['_destination_longitude'] );
    update_post_meta( $post_id, '_destination_zoom_level', $_POST['_destination_zoom_level'] );
}