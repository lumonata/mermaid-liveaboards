<?php

require_once( THEME_DIR . 'includes/cmb/cmb-field-select2/cmb-field-select2.php' );
require_once( THEME_DIR . 'includes/cmb/cmb2-conditionals/cmb2-conditionals.php' );

/*
| -------------------------------------------------------------------------------------
| Post Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'post_metaboxes' ) )
{
    function post_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'post_metaboxes_video_youtube',
            'title'        => __( 'Youtube Video', 'cmb2' ),
            'object_types' => array( 'post' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_post_youtube_video',
            'desc' => 'Enter a youtube URL',
            'name' => 'Youtube URL',
            'type' => 'oembed'
        ) );
    }

    add_action( 'cmb2_admin_init', 'post_metaboxes' );
}


/*
| -------------------------------------------------------------------------------------
| Page Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'page_metaboxes' ) )
{
    function page_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'page_metaboxes_additional_data',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'page' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_page_subtitle',
            'name' => 'Sub Title',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'   => '_page_gallery',
            'name' => 'Gallery',
            'type' => 'file_list'
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'page_liability_release_metaboxes_config_page',
            'title'        => __( 'Shortcode Form Config', 'cmb2' ),
            'object_types' => array( 'page' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true,
            'show_on'      => array( 'key' => 'page-template', 'value' => 'page-enrollment-form.php' )
        ) );

        $cmb->add_field( array(
            'id'   => '_page_shortcode_contact',
            'name' => 'Shortcode Contact',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name'    => 'Enable Button Download File',
            'id'      => '_page_enable_download_file',
            'type'    => 'radio_inline',
            'options' => array(
                '1' => __( 'Yes', 'cmb2' ),
                '0' => __( 'No', 'cmb2' ),
            ),
            'default' => '1',
        ) );

        $cmb->add_field( array(
            'name'    => 'File PDF Guest Information & Liability Release',
            'desc'    => 'Upload a file PDF',
            'id'      => '_page_pdf_file',
            'type'    => 'file',
            'options' => array(
                'url' => false,
            ),
            'text' => array(
                'add_upload_file_text' => 'Add File'
            ),
            'query_args' => array(
                'type' => 'application/pdf',
            ),
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'page_about_metaboxes_config_page',
            'title'        => __( 'About Configuration Page', 'cmb2' ),
            'object_types' => array( 'page' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true,
            'show_on'      => array( 'key' => 'page-template', 'value' => 'page-about.php' )
        ) );

        $cmb->add_field( array(
            'id'   => '_page_ceo_name',
            'name' => 'CEO Name',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'   => '_page_director_name',
            'name' => 'Director Name',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'   => '_page_image_section',
            'name' => 'Section Image',
            'type' => 'file',
            'desc' => 'Recommended Size 1366px * 800px',
        ) );

        $cmb->add_field( array(
            'id'   => '_page_title_dive_areas',
            'name' => 'Title Dive Areas',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'      => '_page_description_dive_areas',
            'name'    => 'Description Dive Areas',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 5,
                'media_buttons' => false
            )
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'page_guest_metaboxes_config_page',
            'title'        => __( 'Guest Information Configuration Page', 'cmb2' ),
            'object_types' => array( 'page' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true,
            'show_on'      => array( 'key' => 'page-template', 'value' => 'page-guest-information-pack.php' )
        ) );

        $cmb->add_field( array(
            'id'   => '_page_mermaid_i_title',
            'name' => 'Mermaid I Title',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name'    => 'MV Mermaid I File',
            'id'      => '_page_file_mermaid_i',
            'type'    => 'file',
            'options' => array(
                'url' => false,
            ),
            'text' => array(
                'add_upload_file_text' => 'Add File'
            ),
            'query_args' => array(
                'type' => 'application/pdf',
            ),
            'preview_size' => 'large',
        ) );

        $cmb->add_field( array(
            'id'   => '_page_mermaid_ii_title',
            'name' => 'Mermaid II Title',
            'type' => 'text'
        ) );


        $cmb->add_field( array(
            'name'    => 'MV Mermaid II File',
            'id'      => '_page_file_mermaid_ii',
            'type'    => 'file',
            'options' => array(
                'url' => false,
            ),
            'text' => array(
                'add_upload_file_text' => 'Add File'
            ),
            'query_args' => array(
                'type' => 'application/pdf',
            ),
            'preview_size' => 'large',
        ) );


        $cmb = new_cmb2_box( array(
            'id'           => 'bank_trf_metaboxes_config_page',
            'title'        => __( 'Check Out Form Type', 'cmb2' ),
            'object_types' => array( 'page' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true,
            'show_on'      => array( 'key' => 'page-template', 'value' => 'page-bank-trf.php' )
        ) );

        $cmb->add_field( array(
            'name'       => 'Check Out Form Type',
            'id'         => '_bank_trf_checkout_form_type',
            'type'       => 'radio_inline',
            'show_names' => false,
            'options'    => array(
                '1' => __( 'CMC Marine', 'cmb2' ),
                '2' => __( 'MMC Marine', 'cmb2' ),
            ),
            'default' => '1',
        ) );
    }

    add_action( 'cmb2_admin_init', 'page_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Boats Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'boats_metaboxes' ) )
{
    function boats_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'boat_metaboxes_additional_data',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_subtitle',
            'name' => 'Sub Title',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_gallery',
            'name' => 'Gallery',
            'type' => 'file_list'
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'boat_metaboxes_config_page',
            'title'        => __( 'Configuration Page', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_title_page',
            'name' => 'Title Page',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_description_page',
            'name' => 'Description Page',
            'type' => 'wysiwyg',
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'boat_image',
            'title'        => __( 'Boat Image', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_image',
            'type' => 'file',
            'desc' => 'Recommended Size 1366px * 800px',
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'boat_image_search',
            'title'        => __( 'Search Image', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_image_search',
            'type' => 'file',
            'desc' => 'Recommended Size 1890px * 705px',
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'boat_id_field',
            'title'        => __( 'Boat ID', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
        ) );

        $cmb->add_field( array(
            'id'         => '_boat_id_',
            'type'       => 'text',
            'desc'       => 'Numeric , max length: 8',
            'name'       => 'BOAT',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        //-- SIDE VIEW GROUP
        $cmb = new_cmb2_box( array(
            'id'           => 'boat_metaboxes_side_view_config',
            'title'        => __( 'Side View Specifications', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_side_view_title',
            'name' => 'Title',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_side_view_image',
            'name' => 'Image',
            'type' => 'file',
        ) );

        $group_field_side_view = $cmb->add_field( array(
            'id'      => '_boat_specification_side_view',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Entry {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_side_view, array(
            'name' => 'Entry Title',
            'id'   => 'title',
            'type' => 'text',
        ) );

        $cmb->add_group_field( $group_field_side_view, array(
            'name'        => 'Description',
            'description' => 'Write a short description for this entry',
            'id'          => 'description',
            'type'        => 'wysiwyg',
            'options'     => array(
                'textarea_rows' => 5,
                'media_buttons' => false
            )
        ) );

        //-- UPPER DECK GROUP
        $cmb = new_cmb2_box( array(
            'id'           => 'boat_metaboxes_upper_deck_config',
            'title'        => __( 'Upper Deck Specifications', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_upper_deck_title',
            'name' => 'Title',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_upper_deck_image',
            'name' => 'Image',
            'type' => 'file',
        ) );

        $group_field_upper_deck = $cmb->add_field( array(
            'id'      => '_boat_specification_upper_deck',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Entry {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_upper_deck, array(
            'name' => 'Entry Title',
            'id'   => 'title',
            'type' => 'text',
        ) );

        $cmb->add_group_field( $group_field_upper_deck, array(
            'name'        => 'Description',
            'description' => 'Write a short description for this entry',
            'id'          => 'description',
            'type'        => 'wysiwyg',
            'options'     => array(
                'textarea_rows' => 5,
                'media_buttons' => false
            )
        ) );

        //-- MAIN DECK GROUP
        $cmb = new_cmb2_box( array(
            'id'           => 'boat_metaboxes_main_deck_config',
            'title'        => __( 'Main Deck Specifications', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_main_deck_title',
            'name' => 'Title',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_main_deck_image',
            'name' => 'Image',
            'type' => 'file',
        ) );

        $group_field_main_deck = $cmb->add_field( array(
            'id'      => '_boat_specification_main_deck',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Entry {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_main_deck, array(
            'name' => 'Entry Title',
            'id'   => 'title',
            'type' => 'text',
        ) );

        $cmb->add_group_field( $group_field_main_deck, array(
            'name'        => 'Description',
            'description' => 'Write a short description for this entry',
            'id'          => 'description',
            'type'        => 'wysiwyg',
            'options'     => array(
                'textarea_rows' => 5,
                'media_buttons' => false
            )
        ) );

        //-- LOWER DECK GROUP
        $cmb = new_cmb2_box( array(
            'id'           => 'boat_metaboxes_lower_deck_config',
            'title'        => __( 'Lower Deck Specifications', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_lower_deck_title',
            'name' => 'Title',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_lower_deck_image',
            'name' => 'Image',
            'type' => 'file',
        ) );

        $group_field_lower_deck = $cmb->add_field( array(
            'id'      => '_boat_specification_lower_deck',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Entry {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_lower_deck, array(
            'name' => 'Entry Title',
            'id'   => 'title',
            'type' => 'text',
        ) );

        $cmb->add_group_field( $group_field_lower_deck, array(
            'name'        => 'Description',
            'description' => 'Write a short description for this entry',
            'id'          => 'description',
            'type'        => 'wysiwyg',
            'options'     => array(
                'textarea_rows' => 5,
                'media_buttons' => false
            )
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'boat_metaboxes_safety_config',
            'title'        => __( 'Configuration Section Safety Features', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_title_safety',
            'name' => 'Title Safety',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'      => '_boat_description_safety',
            'name'    => 'Description Safety',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 3,
                'media_buttons' => false,
            )
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'boat_metaboxes_sistem_config',
            'title'        => __( 'Configuration Section System', 'cmb2' ),
            'object_types' => array( 'boat' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_boat_title_system',
            'name' => 'Title System',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'      => '_boat_description_system',
            'name'    => 'Description System',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 3,
                'media_buttons' => false,
            )
        ) );
    }

    add_action( 'cmb2_admin_init', 'boats_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Destinations Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'destination_metaboxes' ) )
{
    function destination_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'destinations_metabox',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'destination' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'      => '_destination_icon_marker',
            'name'    => 'Icon Marker',
            'type'    => 'file',
            'options' => array(
                'url' => false
            ),
            'text' => array(
                'add_upload_file_text' => 'Add File'
            ),
            'query_args' => array(
                'type' => array(
                    'image/jpeg',
                    'image/png',
                ),
            ),
            'preview_size' => 'medium'
        ) );

        $cmb->add_field( array(
            'id'   => '_destination_image',
            'name' => 'Gallery',
            'type' => 'file_list'
        ) );

        $cmb->add_field( array(
            'id'   => '_destination_slideshow_homepage',
            'name' => 'Slideshow for Homepage',
            'type' => 'file_list'
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'          => '_destination_group_field',
            'type'        => 'group',
            'description' => __( 'Generates reusable form entries', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'Entry {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Entry Title',
            'id'   => 'title',
            'type' => 'text'
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'        => 'Description',
            'description' => 'Write a short description for this entry',
            'id'          => 'description',
            'type'        => 'wysiwyg',
            'options'     => array(
                'textarea_rows' => 5,
                'media_buttons' => false
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'    => 'View',
            'id'      => 'view',
            'type'    => 'radio_inline',
            'options' => array(
                'full' => __( 'Full', 'cmb2' ),
                'half' => __( 'Half', 'cmb2' ),
            ),
            'default' => 'full',
        ) );
    }

    add_action( 'cmb2_admin_init', 'destination_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Dive Spots Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'dive_spots_metaboxes' ) )
{
    function dive_spots_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'dive_spot_metaboxes',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'dive_spots' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'               => '_dive_spot_destination',
            'name'             => 'Destination',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_post_list( 'destination' ),
            'attributes'       => array(
                'autocomplete' => 'off'
            ),
        ) );
    }

    add_action( 'cmb2_admin_init', 'dive_spots_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Gallery Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'gallery_metaboxes' ) )
{
    function gallery_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'gallery_photographer_info',
            'title'        => __( 'Photographer Info', 'cmb2' ),
            'object_types' => array( 'gallery' ),
            'context'      => 'normal',
            'desc'         => 'Please empty this field if the gallery doesn`t have photographer',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_gallery_photographer_name',
            'name' => 'Photographer Name',
            'type' => 'text',
        ) );

        $cmb->add_field( array(
            'id'      => '_gallery_photographer_bio',
            'name'    => 'Photographer Bio',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 5,
                'media_buttons' => false
            )
        ) );

        $cmb->add_field( array(
            'id'      => '_gallery_photographer_foto',
            'name'    => 'Photographer Foto',
            'type'    => 'file',
            'options' => array(
                'url' => false
            ),
            'text' => array(
                'add_upload_file_text' => 'Add Photographer Foto'
            ),
            'query_args' => array(
                'type' => array(
                    'image/jpeg',
                    'image/png',
                ),
            ),
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'gallery_image',
            'title'        => __( 'Gallery Images', 'cmb2' ),
            'object_types' => array( 'gallery' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true,
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'          => '_gallery_foto_images',
            'type'        => 'group',
            'description' => __( 'Generates reusable form entries', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'Gallery Images {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Album Name',
            'id'   => 'album_name',
            'type' => 'text',
            'desc' => 'Empty this field if doesn`t have album name'
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Album Images',
            'id'   => 'album_images',
            'type' => 'file_list',
        ) );
    }

    add_action( 'cmb2_admin_init', 'gallery_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Reviews Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'reviews_metaboxes' ) )
{
    function reviews_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'reviews_metaboxes',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'review' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_review_country',
            'name' => 'Country',
            'type' => 'text',
        ) );

        $cmb->add_field( array(
            'id'          => '_review_date',
            'name'        => 'Date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
        ) );

        $cmb->add_field( array(
            'id'      => '_review_rate',
            'name'    => 'Rate',
            'type'    => 'radio_inline',
            'options' => array(
                '1' => __( '1', 'cmb2' ),
                '2' => __( '2', 'cmb2' ),
                '3' => __( '3', 'cmb2' ),
                '4' => __( '4', 'cmb2' ),
                '5' => __( '5', 'cmb2' ),
            ),
        ) );
    }

    add_action( 'cmb2_admin_init', 'reviews_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Schedule Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'schedule_metaboxes' ) )
{
    function schedule_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'schedule_metaboxes_additional_data',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'schedule_rates' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'               => '_schedule_rates_boat',
            'name'             => 'Boat',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => the_post_list( 'boat' ),
            'attributes'       => array(
                'autocomplete' => 'off'
            ),
        ) );

        $cmb->add_field( array(
            'id'         => '_schedule_rates_year',
            'name'       => 'Year',
            'type'       => 'select',
            'options'    => the_year_list(),
            'attributes' => array(
                'autocomplete' => 'off'
            ),
        ) );
    }

    add_action( 'cmb2_admin_init', 'schedule_metaboxes' );
}


/*
| -------------------------------------------------------------------------------------
| Cabins Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'cabins_metaboxes' ) )
{
    function cabins_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'cabins_metaboxes_additional_data',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'cabins' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_cabins_gallery',
            'name' => 'Gallery',
            'type' => 'file_list'
        ) );

        $cmb->add_field( array(
            'id'               => '_cabins_boat',
            'name'             => 'Boat',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => the_post_list( 'boat' ),
            'attributes'       => array(
                'autocomplete' => 'off'
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_cabins_type',
            'name'             => 'Type',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => array(
                'master' => 'Master',
                'single' => 'Single',
                'deluxe' => 'Deluxe',
                'lower'  => 'Lower',
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            ),
        ) );

        $cmb->add_field( array(
            'id'   => '_cabins_id',
            'name' => 'Cabin ID',
            'type' => 'text',
            'desc' => 'Numeric , max length: 8'
        ) );
    }

    add_action( 'cmb2_admin_init', 'cabins_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Amenities Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'amenities_metaboxes' ) )
{
    function amenities_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'amenities_metaboxes_additional_data',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'amenities' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'      => '_amenities_view',
            'name'    => 'View',
            'type'    => 'select',
            'options' => array(
                'list' => __( 'List', 'cmb2' ),
                'grid' => __( 'Grid', 'cmb2' ),
            ),
        ) );

        $cmb->add_field( array(
            'id'   => '_amenities_gallery',
            'name' => 'Gallery',
            'type' => 'file_list'
        ) );
    }

    add_action( 'cmb2_admin_init', 'amenities_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Iteneraries Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'iteneraries_metaboxes' ) )
{
    function iteneraries_metaboxes()
    {
        $post_id_edit = isset( $_GET['post'] ) ? $_GET['post'] : '';

        $cmb = new_cmb2_box( array(
            'id'           => 'iteneraries_metaboxes_additional_data',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'iteneraries' ),
            'context'      => 'normal',
            'priority'     => 'default',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'      => '_iteneraries_show_list',
            'name'    => 'Show This in List Iteneraries Page',
            'type'    => 'radio_inline',
            'default' => '1',
            'options' => array(
                '1' => __( 'Yes', 'cmb2' ),
                '0' => __( 'No', 'cmb2' ),
            )
        ) );

        $cmb->add_field( array(
            'id'   => '_iteneraries_subtitle',
            'name' => 'Sub Title',
            'type' => 'text',
        ) );

        $cmb->add_field( array(
            'id'      => '_iteneraries_reverse',
            'name'    => 'Reverse Itinerary',
            'type'    => 'pw_select',
            'options' => the_post_list( 'iteneraries', $post_id_edit ),
            'default' => ''
        ) );

        $cmb->add_field( array(
            'id'      => '_iteneraries_destination',
            'name'    => 'Destination',
            'type'    => 'multicheck_inline',
            'options' => the_post_list( 'destination' )
        ) );

        $cmb->add_field( array(
            'id'      => '_iteneraries_route_image',
            'name'    => 'Image Route',
            'type'    => 'file',
            'options' => array(
                'url' => false,
            ),
            'text' => array(
                'add_upload_file_text' => 'Add File'
            ),
            'query_args' => array(
                'type' => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                ),
            ),
            'preview_size' => 'large',
        ) );

        $cmb->add_field( array(
            'id'   => '_iteneraries_departure_port',
            'name' => 'Departure Port',
            'type' => 'text',
        ) );

        $cmb->add_field( array(
            'id'   => '_iteneraries_arrival_port',
            'name' => 'Arrival Port',
            'type' => 'text',
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'          => '_iteneraries_group_field',
            'type'        => 'group',
            'description' => __( 'Generates reusable form entries', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'Additional Field {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Entry Title',
            'id'   => 'title',
            'type' => 'text',
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'        => 'Description',
            'description' => 'Write a short description for this entry',
            'id'          => 'description',
            'type'        => 'wysiwyg',
            'options'     => array(
                'textarea_rows' => 5,
                'media_buttons' => false
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'    => 'View',
            'id'      => 'view',
            'type'    => 'radio_inline',
            'default' => 'full',
            'options' => array(
                'full' => __( 'Full', 'cmb2' ),
                'half' => __( 'Half', 'cmb2' ),
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Class',
            'id'   => 'class',
            'type' => 'text',
        ) );

        //-- PARK FEE
        $cmb = new_cmb2_box( array(
            'id'           => 'iteneraries_metaboxes_park_fee_data',
            'title'        => __( 'Park Fee', 'cmb2' ),
            'object_types' => array( 'iteneraries' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_iteneraries_title_park_fee',
            'name' => 'Title',
            'type' => 'text',
        ) );

        $cmb->add_field( array(
            'id'      => '_iteneraries_show_park_fee',
            'name'    => 'Show Park Fee',
            'type'    => 'radio_inline',
            'default' => 'on',
            'options' => array(
                'on'  => __( 'On', 'cmb2' ),
                'off' => __( 'Off', 'cmb2' ),
            )
        ) );

        $group_field_id_park = $cmb->add_field( array(
            'id'          => '_iteneraries_park_fee_list',
            'type'        => 'group',
            'description' => '',
            'options'     => array(
                'group_title'   => __( 'Entry Park Fee Row {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            )
        ) );

        $cmb->add_group_field( $group_field_id_park, array(
            'name' => 'Entry Title',
            'id'   => 'title_park_fee',
            'type' => 'text',
        ) );

        $cmb->add_group_field( $group_field_id_park, array(
            'name' => 'Entry Value',
            'id'   => 'value_park_fee',
            'type' => 'text',
        ) );

        //-- Flight Recommendation
        $cmb = new_cmb2_box( array(
            'id'           => 'iteneraries_metaboxes_flight_recommendation_data',
            'title'        => __( 'Flight Recommendation', 'cmb2' ),
            'object_types' => array( 'iteneraries' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_iteneraries_title_flight_recommendation',
            'name' => 'Title',
            'type' => 'text',
        ) );

        $cmb->add_field( array(
            'name' => 'Brief',
            'id'   => '_iteneraries_brief_flight_recommendation',
            'type' => 'textarea_small'
        ) );

        $cmb->add_field( array(
            'id'      => '_iteneraries_flight_recommendation_list',
            'name'    => 'Flight',
            'desc'    => 'To add new flight please click <a target="_blank" href="' . admin_url() . '/post-new.php?post_type=flight">here</a>',
            'type'    => 'multicheck_inline',
            'options' => the_post_list( 'flight' )
        ) );

        //-- ITENERARY DETAIL
        $boat_list = the_post_list( 'boat' );

        foreach( $boat_list as $key => $value )
        {
            $cmb = new_cmb2_box( array(
                'id'           => 'iteneraries_metaboxes_data_' . $key,
                'title'        => __( 'ITINERARY DETAIL ' . strtoupper( $value ), 'cmb2' ),
                'object_types' => array( 'iteneraries' ),
                'context'      => 'normal',
                'priority'     => 'low',
                'show_names'   => true
            ) );

            $cmb->add_field( array(
                'id'   => '_iteneraries_day_trip_label_' . $key,
                'name' => 'Number of Day Trip',
                'desc' => 'Ex: 5 days / 4 nights',
                'type' => 'text'
            ) );

            $group_field_id = $cmb->add_field( array(
                'id'          => '_iteneraries_group_field_' . $key,
                'type'        => 'group',
                'description' => __( 'Add itinerary data with MV ' . $value, 'cmb2' ),
                'options'     => array(
                    'group_title'   => __( 'Entry Itinerary Detail ' . $value . ' Row {#}', 'cmb2' ),
                    'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                    'remove_button' => __( 'Remove Entry', 'cmb2' ),
                    'sortable'      => true,
                ),
            ) );

            $cmb->add_group_field( $group_field_id, array(
                'name' => 'Entry Day',
                'id'   => 'day',
                'type' => 'text',
            ) );

            $cmb->add_group_field( $group_field_id, array(
                'name' => 'Entry Sub Day',
                'id'   => 'sub_day',
                'type' => 'text',
            ) );

            $cmb->add_group_field( $group_field_id, array(
                'name'    => 'Itinerary with MV '. $value,
                'id'      => 'description',
                'type'    => 'wysiwyg',
                'options' => array(
                    'textarea_rows' => 5,
                    'media_buttons' => false
                )
            ) );

            $cmb->add_group_field( $group_field_id, array(
                'name' => 'Entry Dives',
                'id'   => 'dives',
                'desc' => 'Please enter only number without text, symbol and space. Empty this field if dont have data',
                'type' => 'text_small',
            ) );
        }

        $cmb = new_cmb2_box( array(
            'id'           => 'iteneraries_pdf',
            'title'        => __( 'Itineraries PDF', 'cmb2' ),
            'object_types' => array( 'iteneraries' ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
        ) );

        $cmb->add_field( array(
            'id'         => '_iteneraries_pdf',
            'type'       => 'file',
            'query_args' => array(
                'type' => 'application/pdf',
            ),
            'text' => array(
                'add_upload_file_text' => 'Add File'
            ),
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'iteneraries_route_id',
            'title'        => __( 'ROUTEID', 'cmb2' ),
            'object_types' => array( 'iteneraries' ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
        ) );

        $cmb->add_field( array(
            'id'   => '_iteneraries_route_id',
            'type' => 'text',
            'desc' => 'Numeric , max length: 8'
        ) );

    }

    add_action( 'cmb2_admin_init', 'iteneraries_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Weather Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'weather_metaboxes' ) )
{
    function weather_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'weather_metaboxes_temperature_data',
            'title'        => __( 'Weather Temperature', 'cmb2' ),
            'object_types' => array( 'weather' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        //-- GET YEAR DATA
        $year      = date( 'Y' );
        $year_next = $year + 5;

        for( $i = $year; $i <= $year_next; $i++ )
        {
            $year_data[ $i ] = $i;
        }

        $cmb->add_field( array(
            'id'      => '_weather_year',
            'name'    => 'Year',
            'type'    => 'radio_inline',
            'options' => $year_data,
            'default' => $year,
        ) );

        $cmb->add_field( array(
            'id'   => '_weather_high_temperature',
            'name' => 'High Temperature',
            'desc' => 'Please enter only number without text, symbol and space. Empty this field if dont have data',
            'type' => 'text',
        ) );

        $cmb->add_field( array(
            'id'   => '_weather_low_temperature',
            'name' => 'Low Temperature',
            'desc' => 'Please enter only number without text, symbol and space. Empty this field if dont have data',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'id'      => '_weather_icon',
            'name'    => 'Icon',
            'type'    => 'file',
            'options' => array(
                'url' => false
            ),
        ) );
    }

    add_action( 'cmb2_admin_init', 'weather_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Currency Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'currency_metaboxes' ) )
{
    function currency_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'currency_metaboxes_code_data',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'currency' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_currency_code',
            'name' => 'Currency Code',
            'type' => 'text_small',
        ) );

        $cmb->add_field( array(
            'id'   => '_currency_symbol',
            'name' => 'Currency Symbol',
            'type' => 'text_small',
        ) );

        $cmb->add_field( array(
            'id'      => '_currency_featured',
            'name'    => 'Featured',
            'type'    => 'radio_inline',
            'default' => 0,
            'options' => array(
                '1' => __( 'Yes', 'cmb2' ),
                '0' => __( 'No', 'cmb2' ),
            )
        ) );
    }

    add_action( 'cmb2_admin_init', 'currency_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Checkout Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'checkout_metaboxes' ) )
{
    function checkout_metaboxes()
    {
        //-- SECTION ADDITIONAL DATA
        $cmb = new_cmb2_box( array(
           'id'           => 'checkout_metaboxes_additional_data_header',
           'title'        => __( 'Invoice', 'cmb2' ),
           'object_types' => array( 'checkout' ),
           'context'      => 'normal',
           'priority'     => 'high',
           'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name' => 'Invoice No.',
            'id'   => '_checkout_invoice_no',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Guest Name',
            'id'   => '_checkout_guest_name',
            'type' => 'text'
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'checkout_metaboxes_additional_data',
            'title'        => __( 'Details', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_boat',
            'name'             => 'Boat',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => the_post_list( 'boat' ),
            'attributes'       => array(
                'data-placeholder' => 'Choose boat',
                'style'            => 'width:200px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_trip_year',
            'name'             => 'Year',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => the_year_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose year',
                'style'            => 'width:200px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_trip_code',
            'name'             => 'Trip Code',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => the_schedule_data_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose trip code',
                'style'            => 'width:200px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'        => 'Departure Date',
            'id'          => 'departure_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'name'        => 'Arrival Date',
            'id'          => 'arrival_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_iteneraries',
            'name'             => 'Itineraries',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => the_post_list( 'iteneraries' ),
            'attributes'       => array(
                'data-placeholder' => 'Choose iteneraries',
                'style'            => 'width:100%;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_booking_type',
            'name'             => 'Booking Type',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => array(
                '1' => __( 'Agent', 'cmb2' ),
                '2' => __( 'Direct', 'cmb2' ),
                '3' => __( 'Agent Charter', 'cmb2' ),
                '4' => __( 'Direct Charter', 'cmb2' )
            ),
            'attributes' => array(
                'data-placeholder' => 'Choose booking type',
                'style'            => 'width:200px;',
                'autocomplete'     => 'off'
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_agent_name',
            'name'             => 'Agent name',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_agent_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose agent name',
                'style'            => 'width:100%;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_agent_address',
            'name'       => 'Agent address',
            'type'       => 'textarea_small',
            'attributes' => array(
                'autocomplete' => 'off',
            )
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_country',
            'name'             => 'Country',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_country_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose country',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'             => 'Departure Point',
            'id'               => '_checkout_departure_point',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_departure_point_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose departure point',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'             => 'Departure Time',
            'id'               => '_checkout_departure_time',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_departure_time_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose departure point',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'             => 'Arrival Point',
            'id'               => '_checkout_arrival_point',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_arrival_point_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose departure point',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'             => 'Arrival Time',
            'id'               => '_checkout_arrival_time',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_arrival_time_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose departure point',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        //-- SECTION CRUISE LIST
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout_metaboxes_cruise_list',
            'title'        => __( 'Cruise List', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'      => '_checkout_cruise_list',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'             => 'Cruise Length',
            'id'               => 'cruise_details',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => array(
                '8 Days/7 Nights'   => '8 Days/7 Nights',
                '9 Days/8 Nights'   => '9 Days/8 Nights',
                '10 Days/9 Nights'  => '10 Days/9 Nights',
                '11 Days/10 Nights' => '11 Days/10 Nights',
                '12 Days/11 Nights' => '12 Days/11 Nights',
                '14 Days/13 Nights' => '14 Days/13 Nights',
                '16 Days/15 Nights' => '16 Days/15 Nights'
            ),
            'attributes' => array(
                'data-placeholder' => 'Choose cruise length',
                'style'            => 'width:250px;',
                'autocomplete'     => 'off'
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'             => 'Cabin Type',
            'id'               => 'cabin_type',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => array(
                'Master'          => 'Master',
                'Single'          => 'Single',
                'Deluxe'          => 'Deluxe',
                'Deluxe Twin'     => 'Deluxe Twin',
                'Lower Deck'      => 'Lower Deck',
                'Lower Deck Twin' => 'Lower Deck Twin'
            ),
            'attributes' => array(
                'data-placeholder' => 'Choose cabin type',
                'style'            => 'width:150px;',
                'autocomplete'     => 'off'
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'id'      => 'cabin_type_options',
            'name'    => 'Cabin Type Options',
            'desc'    => 'Select your cabin type option',
            'type'    => 'radio_inline',
            'default' => '',
            'options' => array(
                '0' => __( 'Double', 'cmb2' ),
                '1' => __( 'Twin Share ', 'cmb2' ),
                '2' => __( 'Single Surcharge ', 'cmb2' )
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'No. Guest',
            'id'         => 'no_guest',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Gross (€)',
            'id'         => 'gross',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'    => 'Type Disc.',
            'id'      => 'disc_type',
            'type'    => 'select',
            'options' => array(
                'Repeat Guest Disc.'   => 'Repeat Guest Disc. (%)',
                'Early Bird Discount.' => 'Early Bird Discount. (%)',
                'Back to Back Disc.'   => 'Back to Back Disc. (%)',
                'Kitas Holder  Disc.'  => 'Kitas Holder  Disc. (%)',
                'Special Disc.'        => 'Special Disc. (%)',
                'No Disc.'             => 'No Discount',
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Value Disc. (%)',
            'id'         => 'repeat_guest_disc',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'    => 'Other Type Disc.',
            'id'      => 'other_disc_type',
            'type'    => 'select',
            'options' => array(
                'Repeat Guest Disc.'   => 'Repeat Guest Disc. (%)',
                'Early Bird Discount.' => 'Early Bird Discount. (%)',
                'Back to Back Disc.'   => 'Back to Back Disc. (%)',
                'Kitas Holder  Disc.'  => 'Kitas Holder  Disc. (%)',
                'Special Disc.'        => 'Special Disc. (%)',
                'No Disc.'             => 'No Discount',
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Other Value Disc. (%)',
            'id'         => 'other_disc_value',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Commission. (%)',
            'id'         => 'commission_value',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (€)',
            'id'         => 'nett',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Total Nett (€)',
            'id'         => 'total_nett',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        //-- SECTION TRAVEL SERVICE
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout_metaboxes_travel_services',
            'title'        => __( 'Travel Service List', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name'        => 'Payment Due Date',
            'id'          => '_checkout_travel_service_payment_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'      => '_checkout_travel_service',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'        => 'Date',
            'id'          => 'date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Service/Route',
            'id'         => 'service_route',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Hotel/Flights',
            'id'         => 'hotel_flight',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Number of Units',
            'id'         => 'guests',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (IDR)',
            'id'         => 'nett_idr',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete'  => 'off',
                'data-rate-eur' => 0
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (€)',
            'id'         => 'nett_euro',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Total Nett (€)',
            'id'         => 'total_nett',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        //-- SECTION FUEL Surcharge
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout_metaboxes_surcharge',
            'title'        => __( 'Surcharge List', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name'        => 'Payment Due Date',
            'id'          => '_checkout_surcharge_payment_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'name'    => 'Show this',
            'id'      => '_checkout_surcharge_show_option',
            'type'    => 'select',
            'default' => 1,
            'options' => array(
                1 => __( 'As Invoice', 'cmb2' ),
                2 => __( 'As Information Only', 'cmb2' )
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'      => '_checkout_surcharge',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'        => 'Date',
            'id'          => 'date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Service',
            'id'         => 'service',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Number of Units',
            'id'         => 'units',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (IDR)',
            'id'         => 'nett_idr',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete'  => 'off',
                'data-rate-eur' => 0
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (€)',
            'id'         => 'nett_euro',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Total Nett (€)',
            'id'         => 'total_nett',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        //-- SECTION PAYMENT TERMS
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout_metaboxes_payment_terms',
            'title'        => __( 'Cruise Payment Terms', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'      => '_checkout_payment_terms',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Label',
            'id'         => 'label',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Percentage (%)',
            'id'         => 'percentage',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'        => 'Due Date',
            'id'          => 'due_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'             => 'Status',
            'id'               => 'status',
            'type'             => 'select',
            'show_option_none' => true,
            'default'          => '',
            'options'          => array(
                'Awaiting Payment' => __( 'Awaiting Payment', 'cmb2' ),
                'Pay On Board'     => __( 'Pay On Board', 'cmb2' ),
                'Paid'             => __( 'Paid', 'cmb2' ),
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Total Cruise (€)',
            'id'         => 'total_deposit',
            'type'       => 'text_small',
            'attributes' => array(
                'autocomplete' => 'off',
                'readonly'     => 'on'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Grand Total (€)',
            'id'         => 'total_nett',
            'type'       => 'text_small',
            'attributes' => array(
                'autocomplete' => 'off',
                'readonly'     => 'on'
            )
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'expired_date_form',
            'title'        => __( 'Expired Date of Payment Link', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
            'attributes'   => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'          => '_checkout_expired_date',
            'name'        => 'Expired Date of Payment Link',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'data-datepicker' => json_encode( array(
                    'minDate' => 0
                ) ),
            )
        ) );

        //-- SECTION OTHER DETAILS
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout_metaboxes_other_detail',
            'title'        => __( 'Other Detail', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'      => '_checkout_bank_options',
            'name'    => 'Bank Options',
            'desc'    => 'Select your bank option here for transfer detail',
            'type'    => 'radio_inline',
            'default' => '0',
            'options' => array(
                '0' => __( 'Used default bank', 'cmb2' ),
                '1' => __( 'Used another bank', 'cmb2' )
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_bank_transfer_detail',
            'name'             => 'Bank Name',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_bank_transfer_list( null, 1 ),
            'attributes'       => array(
                'autocomplete'     => 'off',
                'data-placeholder' => 'Choose bank',
                'style'            => 'width:100%;'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_bank_name',
            'name'       => 'Bank Name',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_bank_address',
            'name'       => 'Bank Address',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_receiver',
            'name'       => 'Receiver',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_account_number',
            'name'       => 'Account Number',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_swift_code',
            'name'       => 'Swift Code',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'name' => 'Remarks',
            'id'   => '_checkout_remarks',
            'type' => 'textarea_small'
        ) );

        //-- SECTION PERSONAL DETAILS
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout_metaboxes_personal_details',
            'title'        => __( 'Personal Details', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name' => 'Full Name',
            'id'   => '_checkout_person_name',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Email Address',
            'id'   => '_checkout_person_email',
            'type' => 'text_email'
        ) );

        $cmb->add_field( array(
            'name' => 'Phone Number',
            'id'   => '_checkout_person_phone',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name'             => 'Gender',
            'id'               => '_checkout_person_gender',
            'type'             => 'select',
            'show_option_none' => false,
            'default'          => 'Male',
            'options'          => array(
                'Male'   => __( 'Male', 'cmb2' ),
                'Female' => __( 'Female', 'cmb2' ),
                'Other'  => __( 'Other', 'cmb2' )
            ),
        ) );

        //-- SECTION PAYMENT DETAILS
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout_metaboxes_payment_details',
            'title'        => __( 'Payment Details', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name' => 'Payment Method',
            'id'   => '_checkout_payment_type',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Transaction ID',
            'id'   => '_checkout_transaction_id',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Payer ID',
            'id'   => '_checkout_payer_id',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Payer Email',
            'id'   => '_checkout_payer_email',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Payment Currency',
            'id'   => '_checkout_currency',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Payment Amount',
            'id'   => '_checkout_amount_pay',
            'type' => 'text'
        ) );

        //-- SECTION REMINDER DATE
        $cmb = new_cmb2_box( array(
            'id'           => 'reminder_date_form',
            'title'        => __( 'Reminder Date for Next Payment', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
        ) );

        $cmb->add_field( array(
            'id'          => '_checkout_reminder_date',
            'name'        => 'Reminder Date for Next Payment',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'data-datepicker' => json_encode( array(
                    'minDate' => 0
                ) ),
            )
        ) );

        //-- SECTION STATUS PAYMENT
        $cmb = new_cmb2_box( array(
            'id'           => 'status_payment_form',
            'title'        => __( 'Amount Due', 'cmb2' ),
            'object_types' => array( 'checkout' ),
            'context'      => 'side',
            'priority'     => 'default',
            'show_names'   => true,
        ) );

        $cmb->add_field( array(
            'name'             => 'Status Payment',
            'id'               => '_checkout_status_payment',
            'type'             => 'select',
            'show_option_none' => false,
            'default'          => 'Check Availability',
            'options'          => array(
                'Check Availability'   => __( 'Check Availability', 'cmb2' ),
                'Waiting Payment'      => __( 'Waiting Payment', 'cmb2' ),
                'Waiting Verification' => __( 'Waiting Verification', 'cmb2' ),
                'Paid'                 => __( 'Paid', 'cmb2' ),
            ),
        ) );
    }

    add_action( 'cmb2_admin_init', 'checkout_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Checkout Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'checkout2_metaboxes' ) )
{
    function checkout2_metaboxes()
    {
        //-- SECTION ADDITIONAL DATA
        $cmb = new_cmb2_box( array(
           'id'           => 'checkout2_metaboxes_additional_data_header',
           'title'        => __( 'Invoice', 'cmb2' ),
           'object_types' => array( 'checkout-2' ),
           'context'      => 'normal',
           'priority'     => 'high',
           'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name' => 'Invoice No.',
            'id'   => '_checkout_invoice_no',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Guest Name',
            'id'   => '_checkout_guest_name',
            'type' => 'text'
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'checkout2_metaboxes_additional_data',
            'title'        => __( 'Details', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_boat',
            'name'             => 'Boat',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => the_post_list( 'boat' ),
            'attributes'       => array(
                'data-placeholder' => 'Choose boat',
                'style'            => 'width:200px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_trip_year',
            'name'             => 'Year',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => the_year_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose year',
                'style'            => 'width:200px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_trip_code',
            'name'             => 'Trip Code',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => the_schedule_data_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose trip code',
                'style'            => 'width:200px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'        => 'Departure Date',
            'id'          => 'departure_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'name'        => 'Arrival Date',
            'id'          => 'arrival_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_iteneraries',
            'name'             => 'Itineraries',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => the_post_list( 'iteneraries' ),
            'attributes'       => array(
                'data-placeholder' => 'Choose iteneraries',
                'style'            => 'width:100%;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_booking_type',
            'name'             => 'Booking Type',
            'type'             => 'pw_select',
            'show_option_none' => false,
            'options'          => array(
                '1' => __( 'Agent', 'cmb2' ),
                '2' => __( 'Direct', 'cmb2' ),
                '3' => __( 'Agent Charter', 'cmb2' ),
                '4' => __( 'Direct Charter', 'cmb2' )
            ),
            'attributes' => array(
                'data-placeholder' => 'Choose booking type',
                'style'            => 'width:200px;',
                'autocomplete'     => 'off'
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_agent_name',
            'name'             => 'Agent name',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_agent_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose agent name',
                'style'            => 'width:100%;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_agent_address',
            'name'       => 'Agent address',
            'type'       => 'textarea_small',
            'attributes' => array(
                'autocomplete' => 'off',
            )
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_country',
            'name'             => 'Country',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_country_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose country',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'             => 'Departure Point',
            'id'               => '_checkout_departure_point',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_departure_point_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose departure point',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'             => 'Departure Time',
            'id'               => '_checkout_departure_time',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_departure_time_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose departure point',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'             => 'Arrival Point',
            'id'               => '_checkout_arrival_point',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_arrival_point_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose departure point',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        $cmb->add_field( array(
            'name'             => 'Arrival Time',
            'id'               => '_checkout_arrival_time',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_arrival_time_list(),
            'attributes'       => array(
                'data-placeholder' => 'Choose departure point',
                'style'            => 'width:400px;',
                'autocomplete'     => 'off',
            ),
        ) );

        //-- SECTION CRUISE LIST
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout2_metaboxes_cruise_list',
            'title'        => __( 'Cruise List', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'      => '_checkout_cruise_list',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'             => 'Cruise Length',
            'id'               => 'cruise_details',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => array(
                '8 Days/7 Nights'   => '8 Days/7 Nights',
                '9 Days/8 Nights'   => '9 Days/8 Nights',
                '10 Days/9 Nights'  => '10 Days/9 Nights',
                '11 Days/10 Nights' => '11 Days/10 Nights',
                '12 Days/11 Nights' => '12 Days/11 Nights',
                '14 Days/13 Nights' => '14 Days/13 Nights',
                '16 Days/15 Nights' => '16 Days/15 Nights'
            ),
            'attributes' => array(
                'data-placeholder' => 'Choose cruise length',
                'style'            => 'width:250px;',
                'autocomplete'     => 'off'
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'             => 'Cabin Type',
            'id'               => 'cabin_type',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => array(
                'Master'          => 'Master',
                'Single'          => 'Single',
                'Deluxe'          => 'Deluxe',
                'Deluxe Twin'     => 'Deluxe Twin',
                'Lower Deck'      => 'Lower Deck',
                'Lower Deck Twin' => 'Lower Deck Twin'
            ),
            'attributes' => array(
                'data-placeholder' => 'Choose cabin type',
                'style'            => 'width:150px;',
                'autocomplete'     => 'off'
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'id'      => 'cabin_type_options',
            'name'    => 'Cabin Type Options',
            'desc'    => 'Select your cabin type option',
            'type'    => 'radio_inline',
            'default' => '',
            'options' => array(
                '0' => __( 'Double', 'cmb2' ),
                '1' => __( 'Twin Share ', 'cmb2' ),
                '2' => __( 'Single Surcharge ', 'cmb2' )
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'No. Guest',
            'id'         => 'no_guest',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Gross (€)',
            'id'         => 'gross',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'    => 'Type Disc.',
            'id'      => 'disc_type',
            'type'    => 'select',
            'options' => array(
                'Repeat Guest Disc.'   => 'Repeat Guest Disc. (%)',
                'Early Bird Discount.' => 'Early Bird Discount. (%)',
                'Back to Back Disc.'   => 'Back to Back Disc. (%)',
                'Kitas Holder  Disc.'  => 'Kitas Holder  Disc. (%)',
                'Special Disc.'        => 'Special Disc. (%)',
                'No Disc.'             => 'No Discount',
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Value Disc. (%)',
            'id'         => 'repeat_guest_disc',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'    => 'Other Type Disc.',
            'id'      => 'other_disc_type',
            'type'    => 'select',
            'options' => array(
                'Repeat Guest Disc.'   => 'Repeat Guest Disc. (%)',
                'Early Bird Discount.' => 'Early Bird Discount. (%)',
                'Back to Back Disc.'   => 'Back to Back Disc. (%)',
                'Kitas Holder  Disc.'  => 'Kitas Holder  Disc. (%)',
                'Special Disc.'        => 'Special Disc. (%)',
                'No Disc.'             => 'No Discount',
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Other Value Disc. (%)',
            'id'         => 'other_disc_value',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Commission. (%)',
            'id'         => 'commission_value',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (€)',
            'id'         => 'nett',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Total Nett (€)',
            'id'         => 'total_nett',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        //-- SECTION TRAVEL SERVICE
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout2_metaboxes_travel_services',
            'title'        => __( 'Travel Service List', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name'        => 'Payment Due Date',
            'id'          => '_checkout_travel_service_payment_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'      => '_checkout_travel_service',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'        => 'Date',
            'id'          => 'date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Service/Route',
            'id'         => 'service_route',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Hotel/Flights',
            'id'         => 'hotel_flight',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Number of Units',
            'id'         => 'guests',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (IDR)',
            'id'         => 'nett_idr',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete'  => 'off',
                'data-rate-eur' => 0
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (€)',
            'id'         => 'nett_euro',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Total Nett (€)',
            'id'         => 'total_nett',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        //-- SECTION FUEL Surcharge
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout2_metaboxes_surcharge',
            'title'        => __( 'Surcharge List', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name'        => 'Payment Due Date',
            'id'          => '_checkout_surcharge_payment_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'name'    => 'Show this',
            'id'      => '_checkout_surcharge_show_option',
            'type'    => 'select',
            'default' => 1,
            'options' => array(
                1 => __( 'As Invoice', 'cmb2' ),
                2 => __( 'As Information Only', 'cmb2' )
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'      => '_checkout_surcharge',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'        => 'Date',
            'id'          => 'date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Service',
            'id'         => 'service',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Number of Units',
            'id'         => 'units',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (IDR)',
            'id'         => 'nett_idr',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete'  => 'off',
                'data-rate-eur' => 0
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Nett (€)',
            'id'         => 'nett_euro',
            'type'       => 'text_small',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Total Nett (€)',
            'id'         => 'total_nett',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        //-- SECTION PAYMENT TERMS
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout2_metaboxes_payment_terms',
            'title'        => __( 'Cruise Payment Terms', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $group_field_id = $cmb->add_field( array(
            'id'      => '_checkout_payment_terms',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ),
                'add_button'    => __( 'Add Another Entry', 'cmb2' ),
                'remove_button' => __( 'Remove Entry', 'cmb2' ),
                'sortable'      => true,
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Label',
            'id'         => 'label',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Percentage (%)',
            'id'         => 'percentage',
            'type'       => 'text',
            'desc'       => 'Please enter only number without text, symbol and space.',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'        => 'Due Date',
            'id'          => 'due_date',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'             => 'Status',
            'id'               => 'status',
            'type'             => 'select',
            'show_option_none' => true,
            'default'          => '',
            'options'          => array(
                'Awaiting Payment' => __( 'Awaiting Payment', 'cmb2' ),
                'Pay On Board'     => __( 'Pay On Board', 'cmb2' ),
                'Paid'             => __( 'Paid', 'cmb2' ),
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Total Cruise (€)',
            'id'         => 'total_deposit',
            'type'       => 'text_small',
            'attributes' => array(
                'autocomplete' => 'off',
                'readonly'     => 'on'
            )
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name'       => 'Grand Total (€)',
            'id'         => 'total_nett',
            'type'       => 'text_small',
            'attributes' => array(
                'autocomplete' => 'off',
                'readonly'     => 'on'
            )
        ) );

        $cmb = new_cmb2_box( array(
            'id'           => 'expired2_date_form',
            'title'        => __( 'Expired Date of Payment Link', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
            'attributes'   => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'          => '_checkout_expired_date',
            'name'        => 'Expired Date of Payment Link',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'data-datepicker' => json_encode( array(
                    'minDate' => 0
                ) ),
            )
        ) );

        //-- SECTION OTHER DETAILS
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout2_metaboxes_other_detail',
            'title'        => __( 'Other Detail', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'      => '_checkout_bank_options',
            'name'    => 'Bank Options',
            'desc'    => 'Select your bank option here for transfer detail',
            'type'    => 'radio_inline',
            'default' => '0',
            'options' => array(
                '0' => __( 'Used default bank', 'cmb2' ),
                '1' => __( 'Used another bank', 'cmb2' )
            ),
            'attributes' => array(
                'autocomplete' => 'off'
            ),
        ) );

        $cmb->add_field( array(
            'id'               => '_checkout_bank_transfer_detail',
            'name'             => 'Bank Name',
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_bank_transfer_list( null, 2 ),
            'attributes'       => array(
                'autocomplete'     => 'off',
                'data-placeholder' => 'Choose bank',
                'style'            => 'width:100%;'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_bank_name',
            'name'       => 'Bank Name',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_bank_address',
            'name'       => 'Bank Address',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_receiver',
            'name'       => 'Receiver',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_account_number',
            'name'       => 'Account Number',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'id'         => '_checkout_swift_code',
            'name'       => 'Swift Code',
            'type'       => 'text',
            'attributes' => array(
                'autocomplete' => 'off'
            )
        ) );

        $cmb->add_field( array(
            'name' => 'Remarks',
            'id'   => '_checkout_remarks',
            'type' => 'textarea_small'
        ) );

        //-- SECTION PERSONAL DETAILS
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout2_metaboxes_personal_details',
            'title'        => __( 'Personal Details', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name' => 'Full Name',
            'id'   => '_checkout_person_name',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Email Address',
            'id'   => '_checkout_person_email',
            'type' => 'text_email'
        ) );

        $cmb->add_field( array(
            'name' => 'Phone Number',
            'id'   => '_checkout_person_phone',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name'             => 'Gender',
            'id'               => '_checkout_person_gender',
            'type'             => 'select',
            'show_option_none' => false,
            'default'          => 'Male',
            'options'          => array(
                'Male'   => __( 'Male', 'cmb2' ),
                'Female' => __( 'Female', 'cmb2' ),
                'Other'  => __( 'Other', 'cmb2' )
            ),
        ) );

        //-- SECTION PAYMENT DETAILS
        $cmb = new_cmb2_box( array(
            'id'           => 'checkout2_metaboxes_payment_details',
            'title'        => __( 'Payment Details', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'normal',
            'priority'     => 'low',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name' => 'Payment Method',
            'id'   => '_checkout_payment_type',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Transaction ID',
            'id'   => '_checkout_transaction_id',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Payer ID',
            'id'   => '_checkout_payer_id',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Payer Email',
            'id'   => '_checkout_payer_email',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Payment Currency',
            'id'   => '_checkout_currency',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => 'Payment Amount',
            'id'   => '_checkout_amount_pay',
            'type' => 'text'
        ) );

        //-- SECTION REMINDER DATE
        $cmb = new_cmb2_box( array(
            'id'           => 'reminder2_date_form',
            'title'        => __( 'Reminder Date for Next Payment', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
        ) );

        $cmb->add_field( array(
            'id'          => '_checkout_reminder_date',
            'name'        => 'Reminder Date for Next Payment',
            'type'        => 'text_date',
            'date_format' => 'd M Y',
            'attributes'  => array(
                'data-datepicker' => json_encode( array(
                    'minDate' => 0
                ) ),
            )
        ) );

        //-- SECTION STATUS PAYMENT
        $cmb = new_cmb2_box( array(
            'id'           => 'status2_payment_form',
            'title'        => __( 'Amount Due', 'cmb2' ),
            'object_types' => array( 'checkout-2' ),
            'context'      => 'side',
            'priority'     => 'default',
            'show_names'   => true,
        ) );

        $cmb->add_field( array(
            'name'             => 'Status Payment',
            'id'               => '_checkout_status_payment',
            'type'             => 'select',
            'show_option_none' => false,
            'default'          => 'Check Availability',
            'options'          => array(
                'Check Availability'   => __( 'Check Availability', 'cmb2' ),
                'Waiting Payment'      => __( 'Waiting Payment', 'cmb2' ),
                'Waiting Verification' => __( 'Waiting Verification', 'cmb2' ),
                'Paid'                 => __( 'Paid', 'cmb2' ),
            ),
        ) );
    }

    add_action( 'cmb2_admin_init', 'checkout2_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| User Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'user_metaboxes' ) )
{
    function user_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'id'           => 'user_metaboxes_additional',
            'title'        => __( 'Additional Data', 'cmb2' ),
            'object_types' => array( 'user' ),
            'context'      => 'normal',
            'priority'     => 'high',
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'id'   => '_user_address',
            'name' => 'Address',
            'type' => 'textarea',
        ) );
    }

    add_action( 'cmb2_admin_init', 'user_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Setting Meta Box
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'bank_option_metaboxes' ) )
{
    function bank_option_metaboxes()
    {
        $cmb = new_cmb2_box( array(
            'position'     => 49,
            'id'           => 'bank_options',
            'option_key'   => 'bank-options',
            'icon_url'     => 'dashicons-bank',
            'object_types' => array( 'options-page' ),
            'title'        => esc_html__( 'Bank Options', 'cmb' )
        ) );

        $bid = $cmb->add_field( array(
            'name'        => 'Bank List',
            'id'          => 'bank_list',
            'type'        => 'group',
            'description' => __( 'This section will help you to add bank data and the list will show as an option for transfer', 'cmb' ),
            'show_names'  => true,
            'options'     => array(
                'group_title'   => __( 'Bank {#}', 'cmb' ),
                'add_button'    => __( 'Add Bank', 'cmb2' ),
                'remove_button' => __( 'Remove Bank', 'cmb2' ),
                'sortable'      => true,
            )
        ) );

        $cmb->add_group_field( $bid, array(
            'id'   => 'bank_name',
            'name' => 'Bank Name',
            'type' => 'text'
        ) );

        $cmb->add_group_field( $bid, array(
            'id'   => 'bank_address',
            'name' => 'Bank Address',
            'type' => 'text'
        ) );

        $cmb->add_group_field( $bid, array(
            'id'   => 'receiver',
            'name' => 'Receiver',
            'type' => 'text'
        ) );

        $cmb->add_group_field( $bid, array(
            'id'   => 'account_number',
            'name' => 'Account Number',
            'type' => 'text'
        ) );

        $cmb->add_group_field( $bid, array(
            'id'   => 'swift_code',
            'name' => 'Swift Code',
            'type' => 'text'
        ) );

        $cmb->add_group_field( $bid, array(
            'id'           => 'bank_logo',
            'name'         => 'Bank Logo',
            'desc'         => 'Recommended for image size 150px * 150px',
            'type'         => 'file',
            'preview_size' => 'thumbnail',
            'options'      => array( 'url' => false ),
            'text'         => array( 'add_upload_file_text' => 'Add Image' ),
            'query_args'   => array(
                'type' => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                )
            ),
        ) );

        $cmb->add_group_field( $bid, array(
            'id'      => 'bank_category',
            'name'    => 'Category',
            'type'    => 'radio_inline',
            'options' => array(
                '1' => __( 'CMC Bank', 'cmb2' ),
                '2' => __( 'MMC Bank', 'cmb2' )
            ),
        ) );

        $cmb->add_field( array(
            'name'             => 'CMC Bank Information Page',
            'id'               => 'cmc_bank_page_info',
            'description'      => __( 'Choose redirect page when client select bank transfer option in checkout form', 'cmb' ),
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_post_list( 'page' ),
            'attributes'       => array(
                'autocomplete' => 'off',
                'style'        => 'width:100%'
            )
        ) );

        $cmb->add_field( array(
            'name'             => 'MMC Bank Information Page',
            'id'               => 'mmc_bank_page_info',
            'type'             => 'group',
            'description'      => __( 'Choose redirect page when client select bank transfer option in checkout form', 'cmb' ),
            'type'             => 'pw_select',
            'show_option_none' => true,
            'options'          => the_post_list( 'page' ),
            'attributes'       => array(
                'autocomplete' => 'off',
                'style'        => 'width:100%'
            )
        ) );
    }

    add_action( 'cmb2_admin_init', 'bank_option_metaboxes' );
}

/*
| -------------------------------------------------------------------------------------
| Checkout Payment Setting
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'payment_setting' ) )
{
    function payment_setting()
    {
        //-- GENERAL tab
        $general = new_cmb2_box( array(
            'position'     => 50,
            'id'           => 'payment_setting',
            'title'        => 'Payments',
            'menu_title'   => 'Payments',
            'icon_url'     => 'dashicons-money-alt',
            'object_types' => array( 'options-page' ),
            'option_key'   => 'payment-setting',
            'tab_group'    => 'payment-setting',
            'tab_title'    => 'General',
            'display_cb'   => 'payment_display_with_tabs'
        ) );

        $pid = $general->add_field( array(
            'name'        => 'CMC Marine',
            'id'          => 'cmc_marine',
            'type'        => 'group',
            'description' => __( 'This section will help you to manage payment setting in general', 'cmb' ),
            'show_names'  => true,
            'repeatable'  => false,
            'options'     => array(
                'group_title' => __( 'Please complete the data in this section with correct data', 'cmb' )
            )
        ) );

        $general->add_group_field( $pid, array(
            'name'        => 'Show Credit Card Method?',
            'id'          => 'cmc_marine_show_cc_method',
            'type'        => 'radio_inline',
            'description' => __( '<em>Show credit card as one of the payment methods on the checkout page</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Yes', 'cmb2' ),
                '2' => __( 'No', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $pid, array(
            'name'        => 'Show Paypal Method?',
            'id'          => 'cmc_marine_show_paypal_method',
            'type'        => 'radio_inline',
            'description' => __( '<em>Show paypal as one of the payment methods on the checkout page</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Yes', 'cmb2' ),
                '2' => __( 'No', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $pid, array(
            'name'        => 'Show Bank Transfer Method?',
            'id'          => 'cmc_marine_show_bank_transfer_method',
            'type'        => 'radio_inline',
            'description' => __( '<em>Show bank transfer as one of the payment methods on the checkout page</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Yes', 'cmb2' ),
                '2' => __( 'No', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $pid, array(
            'name'        => 'Credit Card Account - EUR',
            'id'          => 'cmc_marine_eur_account',
            'type'        => 'radio_inline',
            'description' => __( '<em>Select payment account that will used in checkout form, if client select currency in EUR</b></em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Stripe', 'cmb2' ),
                '2' => __( 'Airwallex', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $pid, array(
            'name'        => 'Credit Card Account - USD',
            'id'          => 'cmc_marine_usd_account',
            'type'        => 'radio_inline',
            'description' => __( '<em>Select payment account that will used in checkout form, if client select currency in USD</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Stripe', 'cmb2' ),
                '2' => __( 'Airwallex', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $pid, array(
            'name'    => 'Paypal Currency',
            'id'      => 'cmc_marine_paypal_currency',
            'type'    => 'radio_inline',
            'default' => 'eur',
            'options' => array(
                'eur' => __( 'EUR', 'cmb2' ),
                'usd' => __( 'USD', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $pid, array(
            'name'    => 'Bank Transfer Currency',
            'id'      => 'cmc_marine_bank_transfer_currency',
            'type'    => 'radio_inline',
            'default' => 'eur',
            'options' => array(
                'eur' => __( 'EUR', 'cmb2' ),
                'usd' => __( 'USD', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $pid, array(
            'name'        => 'Add additional charge?',
            'id'          => 'cmc_marine_use_additional_charge',
            'type'        => 'radio_inline',
            'description' => __( '<em>Show paypal as one of the payment methods on the checkout page</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Yes', 'cmb2' ),
                '2' => __( 'No', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $pid, array(
            'name'       => 'Additional charge value (%)',
            'id'         => 'cmc_marine_additional_charge_value',
            'type'       => 'text',
            'default'    => 1,
            'attributes' => array(
                'style' => 'width:150px;'
            )
        ) );

        $mid = $general->add_field( array(
            'name'        => 'MMC Marine',
            'id'          => 'mmc_marine',
            'type'        => 'group',
            'description' => __( 'This section will help you to manage payment setting in general', 'cmb' ),
            'show_names'  => true,
            'repeatable'  => false,
            'options'     => array(
                'group_title' => __( 'Please complete the data in this section with correct data', 'cmb' )
            )
        ) );

        $general->add_group_field( $mid, array(
            'name'        => 'Show Credit Card Method?',
            'id'          => 'mmc_marine_show_cc_method',
            'type'        => 'radio_inline',
            'description' => __( '<em>Show credit card as one of the payment methods on the checkout page</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Yes', 'cmb2' ),
                '2' => __( 'No', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $mid, array(
            'name'        => 'Show Paypal Method?',
            'id'          => 'mmc_marine_show_paypal_method',
            'type'        => 'radio_inline',
            'description' => __( '<em>Show paypal as one of the payment methods on the checkout page</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Yes', 'cmb2' ),
                '2' => __( 'No', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $mid, array(
            'name'        => 'Show Bank Transfer Method?',
            'id'          => 'mmc_marine_show_bank_transfer_method',
            'type'        => 'radio_inline',
            'description' => __( '<em>Show bank transfer as one of the payment methods on the checkout page</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Yes', 'cmb2' ),
                '2' => __( 'No', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $mid, array(
            'name'        => 'Credit Card Account - EUR',
            'id'          => 'mmc_marine_eur_account',
            'type'        => 'radio_inline',
            'description' => __( '<em>Select payment account that will used in checkout form, if client select currency in EUR</b></em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Stripe', 'cmb2' ),
                '2' => __( 'Airwallex', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $mid, array(
            'name'        => 'Credit Card Account - USD',
            'id'          => 'mmc_marine_usd_account',
            'type'        => 'radio_inline',
            'description' => __( '<em>Select payment account that will used in checkout form, if client select currency in USD</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Stripe', 'cmb2' ),
                '2' => __( 'Airwallex', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $mid, array(
            'name'    => 'Paypal Currency',
            'id'      => 'mmc_marine_paypal_currency',
            'type'    => 'radio_inline',
            'default' => 'eur',
            'options' => array(
                'eur' => __( 'EUR', 'cmb2' ),
                'usd' => __( 'USD', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $mid, array(
            'name'    => 'Bank Transfer Currency',
            'id'      => 'mmc_marine_bank_transfer_currency',
            'type'    => 'radio_inline',
            'default' => 'eur',
            'options' => array(
                'eur' => __( 'EUR', 'cmb2' ),
                'usd' => __( 'USD', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $mid, array(
            'name'        => 'Add additional charge?',
            'id'          => 'mmc_marine_use_additional_charge',
            'type'        => 'radio_inline',
            'description' => __( '<em>Show paypal as one of the payment methods on the checkout page</em>', 'cmb' ),
            'default'     => 1,
            'options'     => array(
                '1' => __( 'Yes', 'cmb2' ),
                '2' => __( 'No', 'cmb2' )
            ),
        ) );

        $general->add_group_field( $mid, array(
            'name'       => 'Additional charge value (%)',
            'id'         => 'mmc_marine_additional_charge_value',
            'type'       => 'text',
            'default'    => 1,
            'attributes' => array(
                'style' => 'width:150px;'
            )
        ) );

        //-- PAYAL tab
        $paypal = new_cmb2_box( array(
            'id'           => 'paypal_setting',
            'menu_title'   => 'Paypal',
            'title'        => 'Payments',
            'object_types' => array( 'options-page' ),
            'option_key'   => 'paypal-setting',
            'parent_slug'  => 'payment-setting',
            'tab_group'    => 'payment-setting',
            'tab_title'    => 'Paypal',
            'display_cb'   => 'payment_display_with_tabs'
        ) );

        $pid = $paypal->add_field( array(
            'name'        => 'CMC Marine',
            'id'          => 'cmc_marine',
            'type'        => 'group',
            'description' => __( 'This section will help you to manage your paypal account credential data', 'cmb' ),
            'show_names'  => true,
            'repeatable'  => false,
            'options'     => array(
                'group_title' => __( 'Paypal Account', 'cmb' )
            )
        ) );

        $paypal->add_group_field( $pid, array(
            'id'      => 'server',
            'name'    => 'Account Server',
            'type'    => 'radio_inline',
            'options' => array(
                'sandbox' => __( 'Sandbox', 'cmb2' ),
                'live'    => __( 'Live', 'cmb2' ),
            ),
        ) );

        $paypal->add_group_field( $pid, array(
            'id'   => 'client_id_sandbox',
            'name' => 'Client ID Sandbox',
            'type' => 'text'
        ) );

        $paypal->add_group_field( $pid, array(
            'id'   => 'client_secret_sandbox',
            'name' => 'Client Secret Sandbox',
            'type' => 'text'
        ) );

        $paypal->add_group_field( $pid, array(
            'id'   => 'client_id_live',
            'name' => 'Client ID Live',
            'type' => 'text'
        ) );

        $paypal->add_group_field( $pid, array(
            'id'   => 'client_secret_live',
            'name' => 'Client Secret Live',
            'type' => 'text'
        ) );

        $mid = $paypal->add_field( array(
            'name'        => 'MMC Marine',
            'id'          => 'mmc_marine',
            'type'        => 'group',
            'description' => __( 'This section will help you to manage your paypal account credential data', 'cmb' ),
            'show_names'  => true,
            'repeatable'  => false,
            'options'     => array(
                'group_title' => __( 'Paypal Account', 'cmb' )
            )
        ) );

        $paypal->add_group_field( $mid, array(
            'id'      => 'server',
            'name'    => 'Account Server',
            'type'    => 'radio_inline',
            'options' => array(
                'sandbox' => __( 'Sandbox', 'cmb2' ),
                'live'    => __( 'Live', 'cmb2' ),
            ),
        ) );

        $paypal->add_group_field( $mid, array(
            'id'   => 'client_id_sandbox',
            'name' => 'Client ID Sandbox',
            'type' => 'text'
        ) );

        $paypal->add_group_field( $mid, array(
            'id'   => 'client_secret_sandbox',
            'name' => 'Client Secret Sandbox',
            'type' => 'text'
        ) );

        $paypal->add_group_field( $mid, array(
            'id'   => 'client_id_live',
            'name' => 'Client ID Live',
            'type' => 'text'
        ) );

        $paypal->add_group_field( $mid, array(
            'id'   => 'client_secret_live',
            'name' => 'Client Secret Live',
            'type' => 'text'
        ) );

        $stripe = new_cmb2_box( array(
            'id'           => 'stripe_setting',
            'menu_title'   => 'Stripe',
            'title'        => 'Payments',
            'object_types' => array( 'options-page' ),
            'option_key'   => 'stripe-setting',
            'parent_slug'  => 'payment-setting',
            'tab_group'    => 'payment-setting',
            'tab_title'    => 'Stripe',
            'display_cb'   => 'payment_display_with_tabs'
        ) );

        $pid = $stripe->add_field( array(
            'name'        => 'CMC Marine',
            'id'          => 'cmc_marine',
            'type'        => 'group',
            'description' => __( 'This section will help you to manage your stripe account credential data', 'cmb' ),
            'show_names'  => true,
            'repeatable'  => false,
            'options'     => array(
                'group_title' => __( 'Stripe Account', 'cmb' )
            )
        ) );

        $stripe->add_group_field( $pid, array(
            'id'      => 'server',
            'name'    => 'Account Server',
            'type'    => 'radio_inline',
            'options' => array(
                'staging'    => __( 'Staging', 'cmb2' ),
                'production' => __( 'Production', 'cmb2' ),
            ),
        ) );

        $stripe->add_group_field( $pid, array(
            'id'   => 'secret_key_staging',
            'name' => 'Secret Key Staging',
            'type' => 'text'
        ) );

        $stripe->add_group_field( $pid, array(
            'id'   => 'publishable_key_staging',
            'name' => 'Publishable Key Staging',
            'type' => 'text'
        ) );

        $stripe->add_group_field( $pid, array(
            'id'   => 'secret_key_production',
            'name' => 'Secret Key Production',
            'type' => 'text'
        ) );

        $stripe->add_group_field( $pid, array(
            'id'   => 'publishable_key_production',
            'name' => 'Publishable Key Production',
            'type' => 'text'
        ) );

        $mid = $stripe->add_field( array(
            'name'        => 'MMC Marine',
            'id'          => 'mmc_marine',
            'type'        => 'group',
            'description' => __( 'This section will help you to manage your stripe account credential data', 'cmb' ),
            'show_names'  => true,
            'repeatable'  => false,
            'options'     => array(
                'group_title' => __( 'Stripe Account', 'cmb' )
            )
        ) );

        $stripe->add_group_field( $mid, array(
            'id'      => 'server',
            'name'    => 'Account Server',
            'type'    => 'radio_inline',
            'options' => array(
                'staging'    => __( 'Staging', 'cmb2' ),
                'production' => __( 'Production', 'cmb2' ),
            ),
        ) );

        $stripe->add_group_field( $mid, array(
            'id'   => 'secret_key_staging',
            'name' => 'Secret Key Staging',
            'type' => 'text'
        ) );

        $stripe->add_group_field( $mid, array(
            'id'   => 'publishable_key_staging',
            'name' => 'Publishable Key Staging',
            'type' => 'text'
        ) );

        $stripe->add_group_field( $mid, array(
            'id'   => 'secret_key_production',
            'name' => 'Secret Key Production',
            'type' => 'text'
        ) );

        $stripe->add_group_field( $mid, array(
            'id'   => 'publishable_key_production',
            'name' => 'Publishable Key Production',
            'type' => 'text'
        ) );

        $airwallex = new_cmb2_box( array(
            'id'           => 'airwallex_setting',
            'title'        => 'Payments',
            'menu_title'   => 'Airwallex',
            'object_types' => array( 'options-page' ),
            'option_key'   => 'airwallex-setting',
            'parent_slug'  => 'payment-setting',
            'tab_group'    => 'payment-setting',
            'tab_title'    => 'Airwallex',
            'display_cb'   => 'payment_display_with_tabs'
        ) );

        $aid = $airwallex->add_field( array(
            'name'        => 'API URL',
            'id'          => 'airwallex_call_url',
            'type'        => 'group',
            'description' => __( 'Fill field below with correct data if want to make API calls', 'cmb' ),
            'show_names'  => true,
            'repeatable'  => false,
            'options'     => array(
                'group_title' => __( 'URL List', 'cmb' )
            )
        ) );

        $airwallex->add_group_field( $aid, array(
            'id'   => 'staging_url',
            'name' => 'Staging Server',
            'type' => 'text'
        ) );

        $airwallex->add_group_field( $aid, array(
            'id'   => 'production_url',
            'name' => 'Production Server',
            'type' => 'text'
        ) );

        $pid = $airwallex->add_field( array(
            'name'        => 'CMC Marine',
            'id'          => 'cmc_marine',
            'type'        => 'group',
            'description' => __( 'This section will help you to manage your airwallex account credential data', 'cmb' ),
            'show_names'  => true,
            'repeatable'  => false,
            'options'     => array(
                'group_title' => __( 'Airwallex Account', 'cmb' )
            )
        ) );

        $airwallex->add_group_field( $pid, array(
            'id'      => 'server',
            'name'    => 'Account Server',
            'type'    => 'radio_inline',
            'options' => array(
                'staging'    => __( 'Staging', 'cmb2' ),
                'production' => __( 'Production', 'cmb2' ),
            ),
        ) );

        $airwallex->add_group_field( $pid, array(
            'id'   => 'client_id_staging',
            'name' => 'Client ID Staging',
            'type' => 'text'
        ) );

        $airwallex->add_group_field( $pid, array(
            'id'   => 'api_key_staging',
            'name' => 'API Key Staging',
            'type' => 'text'
        ) );

        $airwallex->add_group_field( $pid, array(
            'id'   => 'client_id_production',
            'name' => 'Client ID Production',
            'type' => 'text'
        ) );

        $airwallex->add_group_field( $pid, array(
            'id'   => 'api_key_production',
            'name' => 'API Key Production',
            'type' => 'text'
        ) );

        $mid = $airwallex->add_field( array(
            'name'        => 'MMC Marine',
            'id'          => 'mmc_marine',
            'type'        => 'group',
            'description' => __( 'This section will help you to manage your airwallex account credential data', 'cmb' ),
            'show_names'  => true,
            'repeatable'  => false,
            'options'     => array(
                'group_title' => __( 'Airwallex Account', 'cmb' )
            )
        ) );

        $airwallex->add_group_field( $mid, array(
            'id'      => 'server',
            'name'    => 'Account Server',
            'type'    => 'radio_inline',
            'options' => array(
                'staging'    => __( 'Staging', 'cmb2' ),
                'production' => __( 'Production', 'cmb2' ),
            ),
        ) );

        $airwallex->add_group_field( $mid, array(
            'id'   => 'client_id_staging',
            'name' => 'Client ID Staging',
            'type' => 'text'
        ) );

        $airwallex->add_group_field( $mid, array(
            'id'   => 'api_key_staging',
            'name' => 'API Key Staging',
            'type' => 'text'
        ) );

        $airwallex->add_group_field( $mid, array(
            'id'   => 'client_id_production',
            'name' => 'Client ID Production',
            'type' => 'text'
        ) );

        $airwallex->add_group_field( $mid, array(
            'id'   => 'api_key_production',
            'name' => 'API Key Production',
            'type' => 'text'
        ) );
    }

    add_action( 'cmb2_admin_init', 'payment_setting' );

    function payment_display_with_tabs( $cmb_options )
    {
        $tabs = payment_page_tabs( $cmb_options );

        ?>
        <div class="wrap cmb2-options-page option-<?php echo $cmb_options->option_key; ?>">
            <?php if ( get_admin_page_title() ) : ?>
                <h2><?php echo wp_kses_post( get_admin_page_title() ); ?></h2>
            <?php endif; ?>
            <h2 class="nav-tab-wrapper">
                <?php foreach ( $tabs as $option_key => $tab_title ) : ?>
                    <a class="nav-tab<?php if ( isset( $_GET['page'] ) && $option_key === $_GET['page'] ) : ?> nav-tab-active<?php endif; ?>" href="<?php menu_page_url( $option_key ); ?>"><?php echo wp_kses_post( $tab_title ); ?></a>
                <?php endforeach; ?>
            </h2>
            <form class="cmb-form" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="POST" id="<?php echo $cmb_options->cmb->cmb_id; ?>" enctype="multipart/form-data" encoding="multipart/form-data">
                <input type="hidden" name="action" value="<?php echo esc_attr( $cmb_options->option_key ); ?>">
                <?php $cmb_options->options_page_metabox(); ?>
                <?php submit_button( esc_attr( $cmb_options->cmb->prop( 'save_button' ) ), 'primary', 'submit-cmb' ); ?>
            </form>
        </div>
        <?php
    }

    function payment_page_tabs( $cmb_options )
    {
        $tab_group = $cmb_options->cmb->prop( 'tab_group' );
        $tabs      = array();

        foreach ( CMB2_Boxes::get_all() as $cmb_id => $cmb )
        {
            if ( $tab_group === $cmb->prop( 'tab_group' ) )
            {
                $tabs[ $cmb->options_page_keys()[0] ] = $cmb->prop( 'tab_title' )
                    ? $cmb->prop( 'tab_title' )
                    : $cmb->prop( 'title' );
            }
        }

        return $tabs;
    }
}

if( !function_exists( 'payment_get_option' ) )
{
    function payment_get_option( $group, $key = '', $default = false )
    {
        $options = cmb2_get_option( 'payment-setting', $group, $default );

        if( empty( $key ) )
        {
            return $options;
        }

        if( !empty( $options ) && is_array( $options ) )
        {
            foreach( $options as $o )
            {
                if( isset( $o[ $key ] ) )
                {
                    return $o[ $key ];

                    break;
                }
            }
        }
    }
}

if( !function_exists( 'stripe_get_option' ) )
{
    function stripe_get_option( $group, $key = '', $default = false )
    {
        $options = cmb2_get_option( 'stripe-setting', $group, $default );

        if( empty( $key ) )
        {
            return $options;
        }

        if( !empty( $options ) && is_array( $options ) )
        {
            foreach( $options as $o )
            {
                if( isset( $o[ $key ] ) )
                {
                    return $o[ $key ];

                    break;
                }
            }
        }
    }
}

if( !function_exists( 'airwallex_get_option' ) )
{
    function airwallex_get_option( $group, $key = '', $default = false )
    {
        $options = cmb2_get_option( 'airwallex-setting', $group, $default );

        if( empty( $key ) )
        {
            return $options;
        }

        if( !empty( $options ) && is_array( $options ) )
        {
            foreach( $options as $o )
            {
                if( isset( $o[ $key ] ) )
                {
                    return $o[ $key ];

                    break;
                }
            }
        }
    }
}

if( !function_exists( 'paypal_get_option' ) )
{
    function paypal_get_option( $group, $key = '', $default = false )
    {
        $options = cmb2_get_option( 'paypal-setting', $group, $default );

        if( empty( $key ) )
        {
            return $options;
        }

        if( !empty( $options ) && is_array( $options ) )
        {
            foreach( $options as $o )
            {
                if( isset( $o[ $key ] ) )
                {
                    return $o[ $key ];

                    break;
                }
            }
        }
    }
}

if( !function_exists( 'bank_get_option' ) )
{
    function bank_get_option( $key = '', $default = false )
    {
        return cmb2_get_option( 'bank-options', $key, $default );
    }
}

if( !function_exists( 'cmb2_after_form_do_js_validation' ) )
{
    function cmb2_after_form_do_js_validation( $post_id, $cmb )
    {
        static $added = false;

        //-- Only add this to the page once (not for every metabox)
        if( $added )
        {
            return;
        }

        $added = true;

        ?>
        <script type="text/javascript">
            jQuery(document).ready( function( $ ){
                $form       = $( document.getElementById( 'post' ));
                $htmlbody   = $( 'html, body' );
                $toValidate = $( '[data-validation]' );

                if( ! $toValidate.length )
                {
                    return;
                }

                function checkValidation( evt )
                {
                    var $first_error_row = null;
                    var $row = null;
                    var labels = [];

                    function add_required( $row )
                    {
                        $row.css({ 'background-color': 'rgb(255, 170, 170)' });

                        $first_error_row = $first_error_row ? $first_error_row : $row;

                        labels.push( $row.find( '.cmb-th label' ).text());
                    }

                    function remove_required( $row )
                    {
                        $row.css({ background: '' });
                    }

                    $toValidate.each( function(){
                        var $this = $(this);
                        var val   = $this.val();

                        $row = $this.parents( '.cmb-row' );

                        if( $this.is( '[type="button"]' ) || $this.is( '.cmb2-upload-file-id' ) )
                        {
                            return true;
                        }

                        if( 'required' === $this.data( 'validation' ) )
                        {
                            if( $row.is( '.cmb-type-file-list' ) )
                            {
                                var has_LIs = $row.find( 'ul.cmb-attach-list li' ).length > 0;

                                if ( !has_LIs )
                                {
                                    add_required( $row );
                                }
                                else
                                {
                                    remove_required( $row );
                                }
                            }
                            else
                            {
                                if( !val )
                                {
                                    add_required( $row );
                                }
                                else
                                {
                                    remove_required( $row );
                                }
                            }
                        }
                    });

                    if( $first_error_row )
                    {
                        evt.preventDefault();

                        alert( '<?php _e( 'The following fields are required and highlighted below:', 'cmb2' ); ?> ' + labels.join( ', ' ));
                        
                        $htmlbody.animate({
                            scrollTop: ( $first_error_row.offset().top - 200 )
                        }, 1000);
                    }
                }

                $form.on( 'submit', checkValidation );
            });
        </script>
        <?php
    }

    add_action( 'cmb2_after_form', 'cmb2_after_form_do_js_validation', 10, 2 );
}

if( !function_exists( 'pw_cmb2_field_select2_asset_path' ) )
{
    function pw_cmb2_field_select2_asset_path()
    {
        return get_stylesheet_directory_uri() . '/includes/cmb/cmb-field-select2';
    }

    add_filter( 'pw_cmb2_field_select2_asset_path', 'pw_cmb2_field_select2_asset_path' );
}

if( !function_exists( 'cmb2_conditionals_enqueue_script_src' ) )
{
    function cmb2_conditionals_enqueue_script_src()
    {
        return get_stylesheet_directory_uri() . '/includes/cmb/cmb2-conditionals/cmb2-conditionals.js';
    }

    add_action( 'cmb2_admin_init', 'cmb2_conditionals_init' );
    add_filter( 'cmb2_conditionals_enqueue_script_src', 'cmb2_conditionals_enqueue_script_src' );
}

if( !function_exists( 'lm_cmb2_trip_code_meta_value' ) )
{
    function lm_cmb2_trip_code_meta_value( $cmb2_field_no_override_val, $this_object_id, $a, $instance )
    {
        if( $a[ 'field_id' ] == '_checkout_trip_code' )
        {
            $code = get_metadata( $a['type'], $a['id'], $a['field_id'], ( $a['single'] || $a['repeat'] ) );

            if( empty( $code ) )
            {
                return $cmb2_field_no_override_val;
            }
            else
            {
                $ncode = json_decode( base64_decode( $code ) );

                if( $ncode === null && json_last_error() !== JSON_ERROR_NONE )
                {
                    $sdata = get_posts( array( 'post_type' => 'schedule_data', 's' => $code ) );

                    if( !empty( $sdata ) )
                    {
                        $meta = array(
                            '_schedule_rates_boat',
                            '_schedule_rates_year',
                            '_schedule_data_iteneraries',
                            '_schedule_data_price_lower',
                            '_schedule_data_price_master',
                            '_schedule_data_price_single',
                            '_schedule_data_price_deluxe',
                            '_schedule_data_arrival_date',
                            '_schedule_data_departure_date',
                            '_schedule_data_arrival_point',
                            '_schedule_data_depart_point',
                            '_schedule_data_arrival_time',
                            '_schedule_data_depart_time',
                        );

                        foreach( $sdata as $dt )
                        {
                            setup_postdata( $dt );

                            if( $dt->post_title != '' )
                            {
                                $stack = array();

                                foreach( get_post_meta( $dt->post_parent ) as $key => $mt )
                                {
                                    if( in_array( $key, $meta ) )
                                    {
                                        $stack[ $key ] = $mt[0];
                                    }
                                }

                                $cstack = array();

                                foreach( get_post_meta( $dt->ID ) as $key => $mt )
                                {
                                    if( in_array( $key, $meta ) )
                                    {
                                        $cstack[ $key ] = $mt[0];
                                    }
                                }

                                $dt->post_meta = array_merge( $cstack, $stack );

                                return base64_encode( json_encode( $dt ) );
                            }
                        }
                    }
                }
                else
                {
                    return $code;
                }
            }
        }
        elseif( $a[ 'field_id' ] == '_checkout_trip_year' )
        {
            $year = get_metadata( $a['type'], $a['id'], $a['field_id'], ( $a['single'] || $a['repeat'] ) );

            if( empty( $year ) )
            {
                $code = get_metadata( $a['type'], $a['id'], '_checkout_trip_code', ( $a['single'] || $a['repeat'] ) );

                if( empty( $code ) )
                {
                    return $cmb2_field_no_override_val;
                }
                else
                {
                    $ncode = json_decode( base64_decode( $code ) );

                    if( $ncode === null && json_last_error() !== JSON_ERROR_NONE )
                    {
                        $sdata = get_posts( array( 'post_type' => 'schedule_data', 's' => $code ) );
                    }
                    else
                    {
                        $sdata = get_posts( array( 'post_type' => 'schedule_data', 's' => $ncode->post_title ) );
                    }

                    if( !empty( $sdata ) )
                    {
                        foreach( $sdata as $dt )
                        {
                            setup_postdata( $dt );

                            $year = get_metadata( $a['type'], $dt->post_parent, '_schedule_rates_year', true );

                            return $year;
                        }
                    }
                    else
                    {
                        return $cmb2_field_no_override_val;
                    }
                }
            }
            else
            {
                return $year;
            }
        }
        elseif( $a[ 'field_id' ] == '_checkout_cruise_list' )
        {
            $cruise_list = get_metadata( $a['type'], $a['id'], $a['field_id'], ( $a['single'] || $a['repeat'] ) );

            if( !empty( $cruise_list ) )
            {
                foreach( $cruise_list as $i => $cruise )
                {
                    foreach( $cruise as $f => $c )
                    {
                        if( $f == 'cabin_type' )
                        {
                            $src = explode( ' ', strtolower( $c ) );

                            if( in_array( 'master', $src ) )
                            {
                                $cruise_list[ $i ][ $f ] = 'Master';
                            }
                            elseif( in_array( 'single', $src ) )
                            {
                                $cruise_list[ $i ][ $f ] = 'Single';
                            }
                            elseif( in_array( 'lower', $src ) || in_array( 'budget', $src ) )
                            {
                                $cruise_list[ $i ][ $f ] = 'Lower Deck';
                            }
                            elseif( in_array( 'deluxe', $src ) )
                            {
                                $cruise_list[ $i ][ $f ] = 'Deluxe';
                            }
                            else
                            {
                                $cruise_list[ $i ][ $f ] = '';
                            }
                        }
                    }
                }
            }

            return $cruise_list;
        }
        else
        {
            return $cmb2_field_no_override_val;
        }
    }

    add_filter( 'cmb2_override_meta_value', 'lm_cmb2_trip_code_meta_value', 10, 4 );
}
