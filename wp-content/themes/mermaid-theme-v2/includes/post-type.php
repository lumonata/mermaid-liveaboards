<?php
/*
| -------------------------------------------------------------------------------------
| Function untuk mendapatkan data destination
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'get_data_post_type' ) )
{
    function get_data_post_type( $post_type, $post_per_page = -1 )
    {
        $query = new WP_Query( array(
            'posts_per_page' => $post_per_page,
            'post_type'      => $post_type,
            'post_status'    => 'publish',
            'orderby'        => 'title',
            'order'          => 'ASC',
        ));

        return $query;
    }
}

/*
| -------------------------------------------------------------------------------------
| Boat Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'boat', array(
    'labels' => array(
        'name'               => _x( 'Boats', 'post type general name' ),
        'singular_name'      => _x( 'Boat', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'boat' ),
        'add_new_item'       => __( 'Add New Boat' ),
        'edit_item'          => __( 'Edit Boat' ),
        'new_item'           => __( 'New Boat' ),
        'view_item'          => __( 'View Boat' ),
        'search_items'       => __( 'Search Boat' ),
        'not_found'          => __( 'No Boat found' ),
        'not_found_in_trash' => __( 'No Boat found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'hierarchical'       => false,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'menu_position'      => 35,
    'capability_type'    => 'post',
    'menu_icon'          => 'data:image/svg+xml;base64,' . base64_encode( '<svg xmlns="http://www.w3.org/2000/svg" height="1" viewBox="0 0 576 512"><path fill="rgba(240,246,252,.6)" d="M192 32c0-17.7 14.3-32 32-32H352c17.7 0 32 14.3 32 32V64h48c26.5 0 48 21.5 48 48V240l44.4 14.8c23.1 7.7 29.5 37.5 11.5 53.9l-101 92.6c-16.2 9.4-34.7 15.1-50.9 15.1c-19.6 0-40.8-7.7-59.2-20.3c-22.1-15.5-51.6-15.5-73.7 0c-17.1 11.8-38 20.3-59.2 20.3c-16.2 0-34.7-5.7-50.9-15.1l-101-92.6c-18-16.5-11.6-46.2 11.5-53.9L96 240V112c0-26.5 21.5-48 48-48h48V32zM160 218.7l107.8-35.9c13.1-4.4 27.3-4.4 40.5 0L416 218.7V128H160v90.7zM306.5 421.9C329 437.4 356.5 448 384 448c26.9 0 55.4-10.8 77.4-26.1l0 0c11.9-8.5 28.1-7.8 39.2 1.7c14.4 11.9 32.5 21 50.6 25.2c17.2 4 27.9 21.2 23.9 38.4s-21.2 27.9-38.4 23.9c-24.5-5.7-44.9-16.5-58.2-25C449.5 501.7 417 512 384 512c-31.9 0-60.6-9.9-80.4-18.9c-5.8-2.7-11.1-5.3-15.6-7.7c-4.5 2.4-9.7 5.1-15.6 7.7c-19.8 9-48.5 18.9-80.4 18.9c-33 0-65.5-10.3-94.5-25.8c-13.4 8.4-33.7 19.3-58.2 25c-17.2 4-34.4-6.7-38.4-23.9s6.7-34.4 23.9-38.4c18.1-4.2 36.2-13.3 50.6-25.2c11.1-9.4 27.3-10.1 39.2-1.7l0 0C136.7 437.2 165.1 448 192 448c27.5 0 55-10.6 77.5-26.1c11.1-7.9 25.9-7.9 37 0z"/></svg>' ),
    'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
));

register_taxonomy( 'boat_safety_features', 'boat', array(
    'labels' => array(
        'name'               => _x( 'Safety Features', 'post type general name' ),
        'singular_name'      => _x( 'Safety Feature', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'boat' ),
        'add_new_item'       => __( 'Add New Safety Feature' ),
        'edit_item'          => __( 'Edit Safety Feature' ),
        'new_item'           => __( 'New Safety Feature' ),
        'view_item'          => __( 'View Safety Feature' ),
        'search_items'       => __( 'Search Safety Feature' ),
        'not_found'          => __( 'No Safety Feature found' ),
        'not_found_in_trash' => __( 'No Safety Feature in trash' ),
        'parent_item_colon'  => ''
    ),
    'hierarchical'          => true,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'show_in_nav_menus'     => false,
    'update_count_callback' => '_update_post_term_count',
    'rewrite'               => array( 'slug' => 'boat-feature', 'with_front' => true ),
));

register_taxonomy( 'boat_systems', 'boat', array(
    'labels' => array(
        'name'               => _x( 'Systems', 'post type general name' ),
        'singular_name'      => _x( 'Systems', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'boat' ),
        'add_new_item'       => __( 'Add New System' ),
        'edit_item'          => __( 'Edit System' ),
        'new_item'           => __( 'New System' ),
        'view_item'          => __( 'View System' ),
        'search_items'       => __( 'Search System' ),
        'not_found'          => __( 'No System found' ),
        'not_found_in_trash' => __( 'No System in trash' ),
        'parent_item_colon'  => ''
    ),
    'hierarchical'          => true,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'update_count_callback' => '_update_post_term_count',
    'rewrite'               => array( 'slug' => 'boat-system', 'with_front' => true ),
));

/*
| -------------------------------------------------------------------------------------
| Cabins Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'cabins', array(
    'labels' => array(
        'name'               => _x( 'Cabins', 'post type general name' ),
        'singular_name'      => _x( 'Cabin', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'cabins' ),
        'add_new_item'       => __( 'Add New Cabins' ),
        'edit_item'          => __( 'Edit Cabins' ),
        'new_item'           => __( 'New Cabins' ),
        'view_item'          => __( 'View Cabins' ),
        'search_items'       => __( 'Search Cabins' ),
        'not_found'          => __( 'No Cabins found' ),
        'not_found_in_trash' => __( 'No Cabins found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 36,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-tide',
    'supports'           => array( 'title', 'editor', 'thumbnail' )
));

/*
| -------------------------------------------------------------------------------------
| Amenities Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'amenities', array(
    'labels' => array(
        'name'               => _x( 'Amenities', 'post type general name' ),
        'singular_name'      => _x( 'Amenities', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'amenities' ),
        'add_new_item'       => __( 'Add New Amenities' ),
        'edit_item'          => __( 'Edit Amenities' ),
        'new_item'           => __( 'New Amenities' ),
        'view_item'          => __( 'View Amenities' ),
        'search_items'       => __( 'Search Amenities' ),
        'not_found'          => __( 'No Amenities found' ),
        'not_found_in_trash' => __( 'No Amenities found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 37,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-image-filter',
    'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
));

register_taxonomy( 'amenities_categories', 'amenities', array(
    'labels' => array(
        'name'               => _x( 'Categories', 'post type general name' ),
        'singular_name'      => _x( 'Category', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'amenities' ),
        'add_new_item'       => __( 'Add New Category' ),
        'edit_item'          => __( 'Edit Category' ),
        'new_item'           => __( 'New Category' ),
        'view_item'          => __( 'View Category' ),
        'search_items'       => __( 'Search Category' ),
        'not_found'          => __( 'No category found' ),
        'not_found_in_trash' => __( 'No category in trash' ),
        'parent_item_colon'  => ''
    ),
    'hierarchical'          => true,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'update_count_callback' => '_update_post_term_count',
    'rewrite'               => array( 'slug' => 'amenities', 'with_front' => true ),
));

register_taxonomy( 'amenities_boat', 'amenities', array(
    'labels' => array(
        'name'               => _x( 'Amenities Boats', 'post type general name' ),
        'singular_name'      => _x( 'Amenities Boat', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'amenities' ),
        'add_new_item'       => __( 'Add New Boat' ),
        'edit_item'          => __( 'Edit Boat' ),
        'new_item'           => __( 'New Boat' ),
        'view_item'          => __( 'View Boat' ),
        'search_items'       => __( 'Search Boat' ),
        'not_found'          => __( 'No Boat found' ),
        'not_found_in_trash' => __( 'No Boat in trash' ),
        'parent_item_colon'  => ''
    ),
    'hierarchical'          => true,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'update_count_callback' => '_update_post_term_count',
    'rewrite'               => array( 'slug' => 'boat-amenities', 'with_front' => true ),
));

/*
| -------------------------------------------------------------------------------------
| Destinations Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'destination', array(
    'labels' => array(
        'name'               => _x( 'Destinations', 'post type general name' ),
        'singular_name'      => _x( 'Destination', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'destination' ),
        'add_new_item'       => __( 'Add New Destination' ),
        'edit_item'          => __( 'Edit Destination' ),
        'new_item'           => __( 'New Destination' ),
        'view_item'          => __( 'View Destination' ),
        'search_items'       => __( 'Search Destination' ),
        'not_found'          => __( 'No destination found' ),
        'not_found_in_trash' => __( 'No destination found in trash' ),
        'parent_item_colon'  => ''
    ),
    'menu_position'      => null,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 38,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-location',
    'rewrite'            => array( 'slug' => 'destinations' ),
    'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'author' )
));

register_taxonomy( 'destination_categories', 'destination', array(
    'labels' => array(
        'name'               => _x( 'Categories', 'post type general name' ),
        'singular_name'      => _x( 'Category', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'destinations' ),
        'add_new_item'       => __( 'Add New Category' ),
        'edit_item'          => __( 'Edit Category' ),
        'new_item'           => __( 'New Category' ),
        'view_item'          => __( 'View Category' ),
        'search_items'       => __( 'Search Category' ),
        'not_found'          => __( 'No category found' ),
        'not_found_in_trash' => __( 'No category in trash' ),
        'parent_item_colon'  => ''
    ),
    'hierarchical'          => true,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'update_count_callback' => '_update_post_term_count',
    'rewrite'               => array( 'slug' => 'destinations', 'with_front' => true ),
));

/*
| -------------------------------------------------------------------------------------
| Dive Spot Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'dive_spots', array(
    'labels' => array(
        'name'               => _x( 'Dive Spots', 'post type general name' ),
        'singular_name'      => _x( 'Dive Spot', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'dive_spots' ),
        'add_new_item'       => __( 'Add New Dive Spots' ),
        'edit_item'          => __( 'Edit Dive Spots' ),
        'new_item'           => __( 'New Dive Spots' ),
        'view_item'          => __( 'View Dive Spots' ),
        'search_items'       => __( 'Search Dive Spots' ),
        'not_found'          => __( 'No Dive Spots found' ),
        'not_found_in_trash' => __( 'No Dive Spots found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 39,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-palmtree',
    'supports'           => array( 'title', 'editor', 'thumbnail' )
));

/*
| -------------------------------------------------------------------------------------
| Iteneraries Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'iteneraries', array(
    'labels' => array(
        'name'               => _x( 'Itineraries', 'post type general name' ),
        'singular_name'      => _x( 'Itenerary', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'itineraries' ),
        'add_new_item'       => __( 'Add New Itineraries' ),
        'edit_item'          => __( 'Edit Itineraries' ),
        'new_item'           => __( 'New Itineraries' ),
        'view_item'          => __( 'View Itineraries' ),
        'search_items'       => __( 'Search Itineraries' ),
        'not_found'          => __( 'No Itineraries found' ),
        'not_found_in_trash' => __( 'No Itineraries found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 40,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-location-alt',
    'rewrite'            => array( 'slug' => 'itineraries' ),
    'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail' )
));

/*
| -------------------------------------------------------------------------------------
| Facilities Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'facility', array(
    'labels' => array(
        'name'               => _x( 'Facilities', 'post type general name' ),
        'singular_name'      => _x( 'Facility', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'facility' ),
        'add_new_item'       => __( 'Add New Facility' ),
        'edit_item'          => __( 'Edit Facility' ),
        'new_item'           => __( 'New Facility' ),
        'view_item'          => __( 'View Facility' ),
        'search_items'       => __( 'Search Facility' ),
        'not_found'          => __( 'No facility found' ),
        'not_found_in_trash' => __( 'No facility found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 41,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-album',
    'supports'           => array( 'title', 'editor', 'thumbnail', 'author' )
));

/*
| -------------------------------------------------------------------------------------
| Schedule Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'schedule_rates', array(
    'labels' => array(
        'name'               => _x( 'Schedule & Rates', 'post type general name' ),
        'singular_name'      => _x( 'Schedule & Rate', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'schedule_rates' ),
        'add_new_item'       => __( 'Add New Schedule & Rate' ),
        'edit_item'          => __( 'Edit Schedule & Rate' ),
        'new_item'           => __( 'New Schedule & Rate' ),
        'view_item'          => __( 'View Schedule & Rate' ),
        'search_items'       => __( 'Search Schedule & Rate' ),
        'not_found'          => __( 'No schedule & rate found' ),
        'not_found_in_trash' => __( 'No schedule & rate found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 42,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-calendar-alt',
    'supports'           => array( 'title', 'editor' )
));

/*
| -------------------------------------------------------------------------------------
| Gallery Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'gallery', array(
    'labels' => array(
        'name'               => _x( 'Galleries', 'post type general name' ),
        'singular_name'      => _x( 'Gallery', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'gallery' ),
        'add_new_item'       => __( 'Add New Gallery' ),
        'edit_item'          => __( 'Edit Gallery' ),
        'new_item'           => __( 'New Gallery' ),
        'view_item'          => __( 'View Gallery' ),
        'search_items'       => __( 'Search Gallery' ),
        'not_found'          => __( 'No gallery found' ),
        'not_found_in_trash' => __( 'No gallery found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 43,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-format-gallery',
    'supports'           => array( 'title', 'thumbnail' )
));

/*
| -------------------------------------------------------------------------------------
| Flight Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'flight', array(
    'labels' => array(
        'name'               => _x( 'Flight', 'post type general name' ),
        'singular_name'      => _x( 'Flight', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'flight' ),
        'add_new_item'       => __( 'Add New Flight' ),
        'edit_item'          => __( 'Edit Flight' ),
        'new_item'           => __( 'New Flight' ),
        'view_item'          => __( 'View Flight' ),
        'search_items'       => __( 'Search Flight' ),
        'not_found'          => __( 'No Flight found' ),
        'not_found_in_trash' => __( 'No Flight found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 43,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-airplane',
    'supports'           => array( 'title', 'editor', 'thumbnail' )
));

/*
| -------------------------------------------------------------------------------------
| FAQ Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'faq', array(
    'labels' => array(
        'name'               => _x( 'FAQ', 'post type general name' ),
        'singular_name'      => _x( 'FAQ', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'faq' ),
        'add_new_item'       => __( 'Add New FAQ' ),
        'edit_item'          => __( 'Edit FAQ' ),
        'new_item'           => __( 'New FAQ' ),
        'view_item'          => __( 'View FAQ' ),
        'search_items'       => __( 'Search FAQ' ),
        'not_found'          => __( 'No FAQ found' ),
        'not_found_in_trash' => __( 'No FAQ found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 44,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-format-chat',
    'supports'           => array( 'title', 'editor' )
));

/*
| -------------------------------------------------------------------------------------
| Review Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'review', array(
    'labels' => array(
        'name'               => _x( 'Reviews', 'post type general name' ),
        'singular_name'      => _x( 'Review', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'review' ),
        'add_new_item'       => __( 'Add New Review' ),
        'edit_item'          => __( 'Edit Review' ),
        'new_item'           => __( 'New Review' ),
        'view_item'          => __( 'View Review' ),
        'search_items'       => __( 'Search Review' ),
        'not_found'          => __( 'No review found' ),
        'not_found_in_trash' => __( 'No review found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 45,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-format-status',
    'supports'           => array( 'title', 'editor', 'thumbnail' )
));

/*
| -------------------------------------------------------------------------------------
| Currency Post Type
| -------------------------------------------------------------------------------------
*/
register_post_type( 'currency', array(
    'labels' => array(
        'name'               => _x( 'Currency', 'post type general name' ),
        'singular_name'      => _x( 'Currency', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'currency' ),
        'add_new_item'       => __( 'Add New Currency' ),
        'edit_item'          => __( 'Edit Currency' ),
        'new_item'           => __( 'New Currency' ),
        'view_item'          => __( 'View Currency' ),
        'search_items'       => __( 'Search Currency' ),
        'not_found'          => __( 'No Currency found' ),
        'not_found_in_trash' => __( 'No Currency found in trash' ),
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 46,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-money-alt',
    'supports'           => array( 'title' )
));

/*
| -------------------------------------------------------------------------------------
| Form Checkout
| -------------------------------------------------------------------------------------
*/
// register_post_type( 'checkout', array(
//     'labels' => array(
//         'menu_name'          => __( 'CMC Marine' ),
//         'name'               => __( 'CMC Marine Form' ),
//         'singular_name'      => __( 'CMC Marine Form' ),
//         'add_new'            => __( 'Add New', 'checkout' ),
//         'add_new_item'       => __( 'CMC Marine - Add New Form' ),
//         'edit_item'          => __( 'CMC Marine - Edit Form' ),
//         'new_item'           => __( 'CMC Marine - New Form' ),
//         'view_item'          => __( 'View Form' ),
//         'search_items'       => __( 'Search Form' ),
//         'not_found'          => __( 'No form was found' ),
//         'not_found_in_trash' => __( 'No form was found in trash' ),
//         'parent_item_colon'  => ''
//     ),
//     'show_ui'            => true,
//     'query_var'          => true,
//     'rewrite'            => true,
//     'show_in_nav_menus'  => true,
//     'has_archive'        => true,
//     'public'             => true,
//     'publicly_queryable' => true,
//     'hierarchical'       => false,
//     'show_in_menu'       => 'edit.php?post_type=checkout-2',
//     'menu_position'      => 47,
//     'capability_type'    => 'post',
//     'supports'           => array( 'title' )
// ));

register_post_type( 'checkout-2', array(
    'labels' => array(
        'menu_name'          => __( 'Checkout Form' ),
        'name'               => __( 'MMC Marine Form' ),
        'singular_name'      => __( 'MMC Marine Form' ),
        'all_items'          => __( 'MMC Marine' ),
        'add_new'            => __( 'Add New', 'checkout-2' ),
        'add_new_item'       => __( 'MMC Marine - Add New Form' ),
        'edit_item'          => __( 'MMC Marine - Edit Form' ),
        'new_item'           => __( 'MMC Marine - New Form' ),
        'view_item'          => __( 'View Form' ),
        'search_items'       => __( 'Search Form' ),
        'not_found'          => __( 'No form was found' ),
        'not_found_in_trash' => __( 'No form was found in trash' ),
        'parent_item_colon'  => ''
    ),
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => true,
    'show_in_nav_menus'  => true,
    'has_archive'        => true,
    'public'             => true,
    'publicly_queryable' => true,
    'hierarchical'       => false,
    'show_in_menu'       => true,
    'menu_position'      => 48,
    'capability_type'    => 'post',
    'menu_icon'          => 'dashicons-forms',
    'supports'           => array( 'title' )
));