<?php
/*
Template Name: Agent Login Page
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>

<?php
$background_full = get_the_post_thumbnail_url(get_the_ID(), 'full');
$background_medium = get_the_post_thumbnail_url(get_the_ID(), 'medium_large');

?>

<section class="wrap-agent-login b-lazy" data-src="<?php echo $background_full; ?>" data-src-small="<?php echo $background_medium; ?>">
    <div class="line-gradient"></div>

    <div class="container-agent-login">
        <div class="logo">
            <a href="<?php echo get_site_url(); ?>"><img data-src-static="<?php echo get_template_directory_uri().'/assets/images/mermaid-logo.png'; ?>" /></a>
        </div>

        <?php if(is_page('agent-login')): ?>
        <div class="form-login">
            
            <?php
                if(isset($_GET['login']) && ($_GET['login'] == "must-agent" || $_GET['login'] == "failed")):
                    if($_GET['login'] == "must-agent"):
                        $msg = "<strong>Invalid!</strong> please login as agent.";
                    else:
                        $msg = "<strong>Invalid!</strong> please check your account.";
                    endif;

                    echo '
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        '.$msg.'
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    ';
                endif;

            ?>
            <form name="loginform" id="loginform" action="<?php echo get_site_url().'/wp-login.php'; ?>" method="POST">
                <input type="text" name="log" id="user_login" class="input-login" placeholder="Username" />
                <input type="password" name="pwd" id="user_pass" class="input-login" placeholder="Password" />
                <input type="hidden" name="page-login" id="page-login" class="input-login" value="agent-login" placeholder="Password" />
                <input type="hidden" name="redirect_to" value="<?php echo get_site_url().'/schedule-rates/mermaid-i/'; ?>">
                <input type="submit" name="wp-submit" id="wp-submit" class="button-default" value="LOGIN" />
                <a href="<?php echo get_site_url().'/forgot-password/'; ?>">Forgot Password?</a>
				<!--
                <a href="" data-toggle="modal" data-target="#register_modal">Register</a>
				-->
            </form>
        </div>
        <?php else: ?>
        <div class="form-login">
            <form id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
                <input type="text" name="user_login" id="user_login" class="input-login" placeholder="Email" />
                <input type="submit" name="wp-submit" id="wp-submit" class="button-default" value="RESET PASSWORD" />
                <input type="hidden" name="redirect_to" value="<?php echo get_site_url().'/forgot-password/'; ?>">
                <a href="<?php echo get_site_url().'/agent-login/'; ?>">Login?</a>
            </form>
        </div>
		
        <?php endif; ?>
    </div>

    <?php get_copyright(); ?>

</section>

<!--
<div id="register_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
	<!-- Modal content-->
<!--
		<div class="modal-content rounded-0">
			<div class="modal-header" style="background-color: #f4f8fb;height: 100px;">
				
				<h3 class="modal-title" style="color: #b28501;height: 100px;line-height: 79px;font-family: Cinzel,serif;font-weight: 400;font-size: 25px;">Registration Form</h3>
			</div>
			<div class="modal-body">
				<form method="post" name="registration_form" id="registration_form">
					 <div class="form-group">
						<label for="username">Username</label>
						<input type="text" class="form-control border-right-0 border-left-0 border-top-0" id="username" name="username_agent" placeholder="Username">
					 </div>
					 <div class="form-group">
						<label for="email">Email address</label>
						<input type="email" class="form-control border-right-0 border-left-0 border-top-0" id="email" name="email_agent" aria-describedby="emailHelp" placeholder="Enter email">
					 </div>
					 <div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control border-right-0 border-left-0 border-top-0" id="password" name="password_agent" placeholder="Password">
					 </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn button-default mt-0 btn-register" >Register</button>
			</div>
			</form>
		</div>
	</div>
</div>
-->

<script type="text/javascript">
jQuery(document).ready(function($) {	
	var register_agent;
	$("#registration_form").submit(function(event){
		var $form = $(this);
		var $inputs = $form.find('input, button');
		var serializedData = $form.serialize();
		
		register_agent = $.ajax({
			url: "<?php echo get_template_directory_uri(); ?>/ajax/ajax_register_agent.php",
			type: "post",
			beforeSend: function(){ 
				$(".btn-register").html('<i class="fa fa-spinner fa-pulse"></i> Register');
				$(".btn-register").attr('disabled','disabled');},
			data: serializedData
		});
		register_agent.done(function(msg){
			if(msg=='false'){
				$(".btn-register").removeAttr('disabled');
				$(".btn-register").html('Register');
				new PNotify({
				  title: 'Failed!',
				  text: 'Please try again later',
				  type: 'error',
				  styling: 'brighttheme',
				  addclass: 'dark'
				});
			}
			else if(msg=='true'){
				new PNotify({
				  title: 'Success!',
				  text: 'You have successfully registered.',
				  type: 'success',
				  styling: 'brighttheme',
				  addclass: 'dark'
				})
				setTimeout(function() {
					window.location='<?php echo site_url(); ?>/agent-login/';
				}, 2000);
			}
			else if(msg=='ada'){
				$(".btn-register").removeAttr('disabled');
				$(".btn-register").html('Register');
				new PNotify({
				  title: 'Sorry!',
				  text: 'The username or email already registered, please try another one.',
				  type: 'warning',
				  styling: 'brighttheme',
				  addclass: 'dark',
				  hide: false,
				  sticker: true
				 
				})
			}
			else {
				$(".btn-register").removeAttr('disabled');
				$(".btn-register").html('Register');
				new PNotify({
				  title: 'Error!',
				  text: msg,
				  type: 'error',
				  styling: 'brighttheme',
				  addclass: 'dark'
				});
			}			
		});
		register_agent.always(function(){
			$inputs.prop("disabled", false);
		});
		
		event.preventDefault();
	})
})
</script>
<?php endif; ?>

<?php get_footer(); ?>