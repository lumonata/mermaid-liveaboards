<?php get_header(); ?>

<?php session_start(); ?>

<?php if ( have_posts() ) : the_post(); ?>

<?php if( !isset( $_GET['popup'] ) ): ?>

    <?php include_once "layout/hero.php"; ?>
    <?php include_once "layout/inquiry-form-popup.php"; ?>

<?php endif; ?>

<?php

    //-- INISIALISASI VARIBLE PAGE
    $_SESSION["id_itinenary"]      = get_the_ID();
    $_SESSION["title_itinenary"]   = get_the_title();
    $_SESSION["content_itinenary"] = get_the_content();

    $iteneraries_id     = get_the_ID();
    $title              = get_the_title();
    $title_text         = empty( $title ) ? '' : '<h1 class="heading-default">'.$title.'</h1>';
    $sub_title          = get_post_meta( get_the_ID(), '_iteneraries_subtitle', true );
    $sub_title          = empty( $sub_title ) ? '' : '<h4>'.$sub_title.'</h4>';
    $content            = get_the_content();
    $content            = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $content );
    $content            = apply_filters( 'the_content', $content );
    $content            = str_replace( ']]>', ']]&gt;', $content );
    $content            = empty( $content ) ? '' : '<div class="desc-about">'.$content.'</div>';
    $route_image        = get_post_meta( get_the_ID(), '_iteneraries_route_image_id', true );
    $route_image_large  = wp_get_attachment_image_src( $route_image, 'large' );
    $route_image_medium = wp_get_attachment_image_src( $route_image, 'medium_large' );

    ?>

    <!-- SECTION WELCOME -->
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?= $title_text; ?>
                    <?= $sub_title; ?>
                    <?= $content; ?>
                </div>
            </div>
        </div>

        <?php if( !empty( $route_image ) && !file_exists( $route_image ) ): ?>
        <div class="container-mermaid route-image">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $route_image_large[0]; ?>" data-src-small="<?php echo $route_image_medium[0]; ?>" alt="<?php echo get_the_title(); ?>" />
        </div>
        <?php endif; ?>            
    </section>

    <!-- SECTION ITENERARIES DETAIL TABLE -->
    <section class="wrap-iteneraries-table">
        <div class="container-mermaid">
            <?php if( isset( $_GET['popup'] ) ): ?>
            <?php $get_itenerary_data_table = get_itenerary_data_table( get_the_ID(), $_GET['boat_id'], $title ); ?>
            <div class="container-itenerary-table show">
                <?= $get_itenerary_data_table['html'] ?>
            </div>
            <?php else: ?>
            <h2 class="title-section"><?= $title . " with"; ?></h2>
            <div class="line"></div>
            <div class="row">
                <?php

                $itenerary_data       = array();
                $mermaid_choose_array = array();
                $boat_list            = array();

                $query = new WP_Query( array(
                    'post_status'    => 'publish',
                    'orderby'        => 'title',
                    'post_type'      => 'boat',
                    'order'          => 'ASC',
                    'posts_per_page' => 2,
                ) );

                $i = 0;

                while ( $query->have_posts() )
                {
                    $query->the_post();

                    $post_id    = get_the_ID();
                    $post_title = get_the_title();

                    $sanitize_title = sanitize_title( $post_title );
                    $post_image     = wp_get_attachment_image_src( get_post_meta( $post_id, '_boat_image_id', true ), 'large' );
                    $post_image     = $post_image[0];

                    $_SESSION["post_id"]        = $post_id;
                    $_SESSION["post_title"]     = $post_title ;
                    $_SESSION["sanitize_title"] = $sanitize_title;

                    $boat_list[] = array(
                        'id'       => $post_id,
                        'title'    => $post_title,
                        'sanitize' => $sanitize_title
                    );

                    if( empty( $post_image ) )
                    {
                        $post_image = get_the_post_thumbnail_url( $post_id, 'medium-large' );
                    }

                    $post_image = empty( $post_image ) ? THEME_URL_ASSETS . '/images/img-default.jpg' : $post_image;

                    //-- ITENERARIES DATA TABLE
                    $get_itenerary_data_table = get_itenerary_data_table( $iteneraries_id, $post_id, $title );

                    $total_dives_boat = $get_itenerary_data_table['total_dives'];

                    $_SESSION["dives_boat"] = $total_dives_boat ;

                    $html_mermaid_choose = '';

                    if( !empty( $get_itenerary_data_table['html'] ) )
                    {
                        $class = $i == 0 ? 'active' : '';

                        $itenerary_data[] = array(
                            'html'     => $get_itenerary_data_table['html'],
                            'sanitize' => $sanitize_title,
                            'int_id'   => $iteneraries_id,
                            'title'    => $post_title,
                            'post_id'  => $post_id,
                            'class'    => $class,
                        );

                        //-- GET SCHEDULE AND RATES DATA
                        $args_schedule = array(
                            'post_status'    => 'publish',
                            'post_type'      => 'schedule_data',
                            'order'          => 'ASC',
                            'posts_per_page' => 1,
                            'meta_query'     => array(
                                array(
                                    'key'     => '_schedule_data_iteneraries',
                                    'value'   => $iteneraries_id,
                                    'compare' => '=',
                                ),
                                array(
                                    'key'     => '_schedule_data_boat',
                                    'value'   => $post_id,
                                    'compare' => '=',
                                ),
                            ),
                        );

                        $schedule_data = get_posts( $args_schedule );

                        $total_dives_boat = $total_dives_boat > 1 ? $total_dives_boat . ' Dives' : $total_dives_boat . ' Dives';

                        if( count( $schedule_data ) > 0 )
                        {
                            foreach( $schedule_data as $d )
                            {
                                //-- GET DATE
                                $depart_date           = get_post_meta( $d->ID, $prefix.'departure_date', true );
                                $return_date           = get_post_meta( $d->ID, $prefix.'arrival_date', true );
                                $strtotime_depart_date = strtotime( $depart_date );
                                $strtotime_return_date = strtotime( $return_date );
                                $start                 = new DateTime( $depart_date );
                                $end                   = new DateTime( $return_date );
                                $result_date           = $end->diff( $start );
                                $night                 = $result_date->d;
                                $night                 = $night > 1 ? $night . ' nights' : $night . ' night';
                                $day                   = $night + 1;
                                $day                   = $day > 1 ? $day . ' days' : $day . ' day';
                            }
                        }

                        $duration = $get_itenerary_data_table['total_days'];

                        if( $duration != '' )
                        {
                            $total_days = sprintf( '<span class="duration">%s</span>', $duration );
                        }
                        else
                        {
                            $total_days = '';
                        }

                        $html_mermaid_choose .= '
                        <div class="list-boat-iteneraries clearfix">
                            <img src="'.$post_image.'" alt="'.$post_title.'" />
                            <div class="desc-boat-iteneraries">
                                <h2>MV '.$post_title.'</h2>
                                '.$sub_title.'
                                '.$total_days.'
                                <span class="dives">'.$total_dives_boat.'</span>
                            </div>
                        </div>';

                        $mermaid_choose_array[] = array(
                            'html'     => $html_mermaid_choose,
                            'class'    => $class,
                            'sanitize' => $sanitize_title
                        );

                        $i++;
                    }
                }

                wp_reset_postdata();

                if( count( $mermaid_choose_array ) == 2 )
                {
                    foreach( $mermaid_choose_array as $md )
                    {
                        ?>
                        <div class="col-lg-6 col-md-12 wrap-list-boat-iteneraries <?= $md['class'] . ' ' . $md['sanitize'] ?>-itenerary-table" data-filter="<?= $md['sanitize'] ?>-itenerary-table">
                            <?= $md['html'] ?>
                        </div>
                        <?php
                    }
                }
                else
                {
                    foreach( $mermaid_choose_array as $md )
                    {
                        ?>
                        <div class="col-lg-6 col-md-12 offset-lg-3 wrap-list-boat-iteneraries active <?= $md['class'] . ' ' . $md['sanitize'] ?>-itenerary-table" data-filter="<?= $md['sanitize'] ?>-itenerary-table">
                            <?= $md['html'] ?>
                        </div>
                        <?php
                    }
                }

                ?>
            </div>
            <!-- ITENERARIES TABLE -->
             
            <?php

            if( isset( $itenerary_data ) && !empty( $itenerary_data ) )
            {
                $reverse = get_post_meta( $iteneraries_id, '_iteneraries_reverse', true );

                foreach( $itenerary_data as $key => $value )
                {
                    $_SESSION['boat_name'] = $boat_list[$mermaid_other_key]['title'];

                    $mermaid_other_key = $key == 0 ? $key + 1 : $key - 1;

                    $link_one = '';
                    $link_two = '';

                    if( count( $mermaid_choose_array ) == 2 )
                    {
                        $link_one = '
                        <a href="" class="button-itenerary-table" data-filter="' . $boat_list[$mermaid_other_key]['sanitize'] . '-itenerary-table">
                            See '. get_label_string( 'Itenerary', true ) .' With MV ' . $boat_list[$mermaid_other_key]['title'] . '
                        </a>';
                    }

                    if( !empty( $reverse ) )
                    {
                        $link_two = '
                        <a href="' . get_the_permalink( $reverse ) . '" class="button-reverse">
                            See Reverse '.get_label_string( 'Itenerary', true ).' With MV ' . $boat_list[$key]['title'] . ' 
                        </a>';
                    }

                    ?>
                    <div class="container-itenerary-table <?= $value['class'] ?>" id="<?= $value['sanitize'] ?>-itenerary-table">
                        <?= $value['html'] ?>
                        <?= $link_one ?>
                        <?= $link_two ?>
                        <a href="<?= THEME_URL ?>/itinenary-pdf.php?p=<?= $value['post_id'] ?>&id=<?= $value['int_id'] ?>" class="button-reverse" target="_blank"> Download PDF</a>
                    </div>
                    <?php
                }
            } ?>

            <?php endif; ?>			
        </div>
    </section>

    <!-- SECTION GALLERY ITENERARY -->
    <section class="gallery-itenerary">
        <div class="bg-grey"></div>
        <div class="box-title-gallery">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <h2><?php echo $title.' Gallery'; ?></h2>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <h4 class="full-screen"><?php echo get_label_string( 'Full Screen', true ); ?></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-mermaid slide-image-page-detail">
            <div class="container-gallery">
                <h2 class="gallery-title"><?php echo $title.' Gallery'; ?></h2>
                <span class="close">Close</span>
                
                <div class="wrap-owl-slide">
                    <div class="m-arrow m-prev"></div>
                    <div class="owl-carousel owl-theme m-slide">
                        <?php //-- GET IMAGE DATA?>
                        <?php $destination_iteneraries = get_post_meta( $iteneraries_id, '_iteneraries_destination', true ); ?>
                        <?php $gallery_iteneraries     = array(); ?>
                        <?php if( !empty( $destination_iteneraries ) ): ?>
                            <?php foreach( $destination_iteneraries as $dest ): ?>
                                <?php $gallery = get_post_meta( $dest, '_destination_image', true ); ?>
                                <?php if( !empty( $gallery ) ): ?>
                                    <?php $ig = 1; ?>
                                    <?php foreach ( $gallery as $img_id => $d ): ?>
                                        <?php $image   = wp_get_attachment_image_src( $img_id, 'large' ); ?>
                                        <?php $caption = wp_get_attachment_caption( $img_id ); ?>
                                        <?php $caption = !empty( $caption ) ? '<h3>'.$caption.'</h3>' : ''; ?>

                                        <?php $gallery_iteneraries[] = array( 'image' => $image[0], 'caption' => $caption ); ?>
                                        <?php $ig++; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <?php if( !empty( $gallery_iteneraries ) ): ?>
                            <?php foreach ( $gallery_iteneraries as $d ): ?>
                                <?php $image   = $d['image']; ?>
                                <?php $caption = $d['caption']; ?>
                                <div class="item">
                                    <img src="<?= $image ?>" />
                                    <?= $caption ?>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="m-arrow m-next"></div>
                </div>
            </div>
        </div>
    </section>

    <!-- SECTION GROUP FIELD -->
    <section class="wrap-custom-group-field">
        <div class="container">
            <?php $departs_port = get_post_meta( get_the_ID(), '_iteneraries_departure_port', true ); ?>
            <?php $arrival_port = get_post_meta( get_the_ID(), '_iteneraries_arrival_port', true ); ?>
            <?php $departs_port = empty( $departs_port ) ? '-' : $departs_port; ?>
            <?php $arrival_port = empty( $arrival_port ) ? '-' : $arrival_port; ?>

            <div class="row custom-field-group">
                <div class="col-lg-6 col-md-6 container-list-custom-field grid-list">
                    <div class="description-group-field">
                        <h2 class="title-cf"><?= get_label_string( 'Departure Port', true ) ?></h2>
                        <?= $departs_port ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 container-list-custom-field grid-list">
                    <div class="description-group-field">
                        <h2 class="title-cf"><?= get_label_string( 'Arrival Port', true ) ?></h2>
                        <?= $arrival_port ?>
                    </div>
                </div>
            </div>

            <?php get_group_field( get_the_ID(), 'iteneraries' ); ?>

            <?php //-- Park Fee List?>
            <?php $park_fee_title  = get_post_meta( $iteneraries_id, '_iteneraries_title_park_fee', true ); ?>
            <?php $park_fee_title  = empty( $park_fee_title ) ? 'Park Fee' : $park_fee_title; ?>
            <?php $park_fee_status = get_post_meta( $iteneraries_id, '_iteneraries_show_park_fee', true ); ?>
            <?php $park_fee_list   = get_post_meta( $iteneraries_id, '_iteneraries_park_fee_list', true ); ?>

            <?php if( $park_fee_status == "on" && !empty( $park_fee_list ) ): ?>
            <div class="row custom-field-group park-fee">
                <div class="col-md-12 container-list-custom-field grid-list">
                    <h2 class="title-cf"><?php echo $park_fee_title; ?></h2>
                    <div class="description-group-field">
                        <ul>
                            <?php $pi = 1; ?>
                            <?php foreach( $park_fee_list as $d ): ?>
                                <?php $title = $d['title_park_fee']; ?>
                                <?php $value = $d['value_park_fee']; ?>

                                <?php if( $pi % 2 == 1 && $pi > 1 ): ?>
                                </ul><ul>
                                <?php endif; ?>
                                <li>
                                    <h3><?= $title ?></h3>
                                    <p><?= $value ?></p>
                                </li>
                                <?php $pi++; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php //-- FLIGHT RECOMMENDATION LIST?>
            <?php $flight_recommendation_title = get_post_meta( $iteneraries_id, '_iteneraries_title_flight_recommendation', true ); ?>
            <?php $flight_recommendation_brief = get_post_meta( $iteneraries_id, '_iteneraries_brief_flight_recommendation', true ); ?>
            <?php $flight_recommendation_list  = get_post_meta( $iteneraries_id, '_iteneraries_flight_recommendation_list', true ); ?>

            <?php if( !empty( $flight_recommendation_list ) ): ?>
            <div class="row custom-field-group park-fee">
                <div class="col-md-12 container-list-custom-field grid-list">
                    <h2 class="title-cf"><?= $flight_recommendation_title; ?></h2>
                    <p><?= $flight_recommendation_brief; ?></p>
                    <div class="description-group-field flight-list">
                        <ul>
                            <?php $i = 1; ?>
                            <?php foreach( $flight_recommendation_list as $d ): ?>
                                <?php $image_flight   = get_the_post_thumbnail_url( $d, 'post-thumbnail' ); ?>
                                <?php $content_post   = get_post( $d ); ?>
                                <?php $title_flight   = $content_post->post_title; ?>
                                <?php $content_flight = $content_post->post_content; ?>
                                <?php $content_flight = apply_filters( 'the_content', $content_flight ); ?>
                                <?php $content_flight = str_replace( ']]>', ']]&gt;', $content_flight ); ?>

                                <?php if( $i % 2 == 1 && $i > 1 ): ?>
                                </ul><ul>
                                <?php endif; ?>
                                <li>
                                    <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?= $image_flight ?>" data-src-small="<?= $image_flight ?>" alt="<?= $title_flight ?>" />
                                    <?= $content_flight ?>
                                </li>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </section>

    <?php if( !isset( $_GET['popup'] ) ): ?>
        <?php $pdf = get_post_meta( $iteneraries_id, '_iteneraries_pdf', true ); ?>
        <?php if( !empty( $pdf ) ): ?>
        <section class="wrap-pdf">
            <div class="container">
                <div class="row wrap-download-pdf">
                    <div class="col-md-6 text-download">
                        <h3><?php echo "Download Your ".get_the_title()." Itineraries"; ?></h3>
                    </div>
                    <div class="col-md-5 offset-md-1 button-download clearfix">
                        <a href="<?php echo $pdf; ?>" target="_blank"><?php get_label_string( 'Download PDF' ); ?></a>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php show_information_pack_schedule(); ?>

        <!-- SECTION SCHDULE ITENERARIES -->
        <section class="wrap-schedule-rate-table">
            <div class="container-mermaid">
                <h2 class="title-section"><?php the_title(); ?> <?php get_label_string( 'Upcoming Departures' ); ?></h2>
                <div class="line"></div>
            <?php table_schedule_and_rate( 'iteneraries', array( 'id' => get_the_ID() ) ); ?>
            </div>
        </section>

        <!-- SECTION OTHER ITENERARIES LIST -->
        <section class="itenerary-list">
            <div class="container-mermaid">
                <h2 class="title-section"><?php get_label_string( 'Other Itineraries' ); ?></h2>
                <div class="line"></div>
                <?php show_list_iteneraries( 'iteneraries', get_the_ID() ); ?>
            </div>
        </section>
        <?php endif; ?>
    <?php endif; ?>

<?php get_footer(); ?>