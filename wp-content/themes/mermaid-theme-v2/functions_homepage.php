<?php
/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan section about homepage
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'section_about_homepage' ) )
{
    function section_about_homepage()
    {
        $subtitle = get_post_meta( get_the_ID(), '_page_subtitle', true );
        $subtitle = empty( $subtitle ) ? '' : sprintf( '<h2>%s</h2>', $subtitle );

        $content = get_the_content();
        $content = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $content );
        $content = apply_filters( 'the_content', $content );
        $content = str_replace( ']]>', ']]&gt;', $content );
        $content = empty( $content ) ? '' : sprintf( '<div class="desc">%s</div>', $content );

        ?>
        <section class="welcome-about">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?= the_title( '<h1 class="heading-default">', '</h1>' ) ?>
                        <?= $subtitle ?>
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan section discover indonesia
| -------------------------------------------------------------------------------------
*/
if( ! function_exists( 'section_discover_indonesia' ) ):
    function section_discover_indonesia()
    {
        $page = get_post( 82 );
        if( !empty( $page ) ):
            $page_title   = $page->post_title;
            $page_content = wp_strip_all_tags( $page->post_content );
            $page_content = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $page_content );
            $page_content = apply_filters( 'the_content', $page_content );
            $page_content = str_replace( ']]>', ']]&gt;', $page_content );
            $page_gallery = get_post_meta( $page->ID, '_page_gallery', true );
            ?>
        <div class="col-md-12 discover-indonesia">
            <div class="container-mermaid">
                <h2 class="title-section"><?php echo $page_title; ?></h2>
                <div class="line"></div>

                <div class="row">
                    <?php latest_news(); ?>
                    <div class="col-md-9 col-sm-12 slide-discover">

                        <div class="container-slide-image">
                            <div class="inner">
                                <?php
                                                if( !empty( $page_gallery ) ):
                                                    foreach ( $page_gallery as $img_id => $d ):
                                                        $image        = wp_get_attachment_image_src( $img_id, 'large' );
                                                        $image_medium = wp_get_attachment_image_src( $img_id, 'medium_large' );
                                                        $caption      = wp_get_attachment_caption( $img_id );
                                                        $caption      = !empty( $caption ) ? '<h3>'.$caption.'</h3>' : '';

                                                        echo '
                                            <div class="slide-list">
                                                <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src-small="'.$image_medium[0].'" data-src="'.$image[0].'" alt="'.$caption.'" />
                                                '.$caption.'
                                            </div>
                                            ';
                                                    endforeach;
                                                endif;
            ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 align-center">
                        <?php echo $page_content; ?>
                    </div>
                </div>
            </div>
        </div>
<?php
        endif;
    }
endif;


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan section select mermaid
| -------------------------------------------------------------------------------------
*/
if( ! function_exists( 'section_select_mermaid' ) ):
    function section_select_mermaid()
    {
        global $boat_data;
        ?>
        <div class="col-lg-12 select-mermaid">
            <div class="container-mermaid">

                <h2 class="title-section"><?php get_label_string( 'Select Your Mermaid' ); ?></h2>
                <div class="line"></div>

                <?php
                        $i = 1;
        while ( $boat_data->have_posts() ):
            $boat_data->the_post();

            $post_id           = get_the_ID();
            $post_title        = get_the_title();
            $post_link         = get_permalink();
            $post_content      = wp_strip_all_tags( get_the_content() );
            $post_content      = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $post_content );
            $post_content      = apply_filters( 'the_content', $post_content );
            $post_content      = str_replace( ']]>', ']]&gt;', $post_content );
            $post_content      = strlen( $post_content ) > 150 ? rtrim( substr( $post_content, 0, 170 ) ).'...' : $post_content;
            $post_image        = wp_get_attachment_image_src( get_post_meta( $post_id, '_boat_image_id', true ), 'large' );
            $post_image_medium = wp_get_attachment_image_src( get_post_meta( $post_id, '_boat_image_id', true ), 'medium_large' );
            $post_image        = $post_image[0];
            $post_image_medium = $post_image_medium[0];

            if( empty( $post_image ) ):
                $post_image        = get_the_post_thumbnail_url( $post_id, 'large' );
                $post_image_medium = get_the_post_thumbnail_url( $post_id, 'medium_large' );
            endif;

            $post_image        = empty( $post_image ) ? THEME_URL_ASSETS.'/images/img-default.jpg' : $post_image;
            $post_image_medium = empty( $post_image_medium ) ? THEME_URL_ASSETS.'/images/img-default.jpg' : $post_image_medium;

            $image_html = '
                        <div class="col-lg-6 col-md-12 con-image-mermaid list-desc-blue">
                            <a href="'.$post_link.'" class="con-image-link">
                                <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="'.$post_image.'" data-src-small="'.$post_image_medium.'" alt="" />
                            </a>
                        </div>
                    ';
            $desc_html = "
                        <div class=\"col-lg-6 col-md-12 list-desc-blue\">
                            <div class=\"wrap-desc-mermaid\">
                                <h3><a href=\"$post_link\">$post_title</a></h3>
                                <div class=\"desc\">$post_content</div>
                                <a href=\"$post_link\" class=\"btn button-default\">".get_label_string( 'Read More', true )."</a>
                            </div>
                        </div>
                    ";

            $list_mermaid_html = '<div class="row container-gradient container-gradient-'.$i.'">';

            // if($i % 2 != 0):
            $list_mermaid_html .= $image_html.$desc_html;
            // else:
            // $list_mermaid_html .= $desc_html.$image_html;
            // endif;

            $list_mermaid_html .= '</div>';

            echo $list_mermaid_html;

            $i++;

        endwhile;
        wp_reset_postdata();
        ?>

            </div>
        </div>
<?php
    }
endif;


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan section facility
| -------------------------------------------------------------------------------------
*/
if( ! function_exists( 'section_facilities' ) ):
    function section_facilities()
    {
        ?>

        <div class="col-md-12 mermaid-facility">
            <div class="container-mermaid">

                <h2 class="title-section"><?php get_label_string( 'Mermaid Facilities' ); ?></h2>
                <div class="line"></div>

                <div class="container">
                    <div class="row">
                        
                    <?php
                                $args = array(
                                    'post_status'    => 'publish',
                                    'post_type'      => 'facility',
                                    'posts_per_page' => -1
                                );
        $query = new WP_Query( $args );

        $total_facility   = count( $query->posts );
        $count_per_column = floor( $total_facility / 3 );
        $i                = 1;
        $html             = '
                        <div class="col-lg-12 container-list-facility container-list-slide-star">
                            <ul class="list-default clearfix">
                        ';

        while ( $query->have_posts() ):
            $query->the_post();
            $post_title = get_the_title();

            if( $i % $count_per_column == 0 ):
                $html .= '
                                            <li>'.$post_title.'</li>
                                        </ul>
                                    ';

                if( $i != $total_facility ):
                    $html .= '
                                            <ul class="list-default">
                                        ';
                endif;

            else:
                $html .= '<li>'.$post_title.'</li>';
            endif;

            $i++;
        endwhile;
        wp_reset_postdata();

        $html .= '
                            </ul>
                        </div>';

        echo $html;
        ?>
                    </div>
                </div>
            </div>
        </div>

<?php
    }
endif;


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan section destinations
| -------------------------------------------------------------------------------------
*/
if( ! function_exists( 'section_destinations_iteneraries' ) )
{
    function section_destinations_iteneraries( $post_type )
    {
        if( $post_type == "destination" )
        {
            $post_data = $query = new WP_Query( array(
                'post_type'      => $post_type,
                'post_status'    => 'publish',
                'orderby'        => 'date',
                'order'          => 'DESC',
                'posts_per_page' => -1,
            ) );

            $title_page = get_label_string( 'Discover Our Destinations', true );
        }
        else
        {
            $post_data = new WP_Query( array(
                'post_status'    => 'publish',
                'post_type'      => 'iteneraries',
                'posts_per_page' => -1,
                'meta_query'     => array(
                    array(
                        'key'   => '_iteneraries_show_list',
                        'value' => 1
                    )
                )
            ) );

            $title_page = get_label_string( 'Discover Our Iteneraries', true );
        }

        // GET LIST DESTINATIONS
        $html_list_destination = '';
        $html_list_col         = '';

        $i = 1;

        while ( $post_data->have_posts() ):
            $post_data->the_post();

            $post_title   = get_the_title();
            $post_link    = get_permalink();
            $post_content = wp_strip_all_tags( get_the_content() );
            $post_content = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $post_content );
            $post_content = apply_filters( 'the_content', $post_content );
            $post_content = str_replace( ']]>', ']]&gt;', $post_content );
            $post_image   = get_the_post_thumbnail_url( get_the_ID(), 'large' );
            $post_image   = empty( $post_image ) ? THEME_URL_ASSETS.'/images/img-default.jpg' : $post_image;

            $post_image_medium = get_the_post_thumbnail_url( get_the_ID(), 'medium_large' );
            $post_image_medium = empty( $post_image_medium ) ? THEME_URL_ASSETS.'/images/img-default.jpg' : $post_image_medium;

            // GALLERY
            $gallery       = get_post_meta( get_the_ID(), '_destination_slideshow_homepage', true );
            $post_content  = strlen( $post_content ) > 150 ? rtrim( substr( $post_content, 0, 170 ) ).'...' : $post_content;
            $html_list_img = '<div class="col-lg-6 col-md-12 con-image-mermaid list-desc-blue">';
            if( !empty( $gallery ) ):
                $html_list_img .= '<div class="inner">';
                $z = 1;
                foreach ( $gallery as $img_id => $d ):

                    if( $z <= 3 ):
                        $image        = wp_get_attachment_image_src( $img_id, 'large' );
                        $image_medium = wp_get_attachment_image_src( $img_id, 'medium_large' );
                        $caption      = wp_get_attachment_caption( $img_id );
                        $caption      = !empty( $caption ) ? '<h5>'.$caption.'</h5>' : '';

                        $html_list_img .= '
                                    <div class="slide-list">
                                        <a href="'.$post_link.'" class="slide-link">
                                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="b-lazy" data-src="'.$image[0].'"  data-src-small="'.$image_medium[0].'"/>
                                            '.$caption.'
                                        </a>
                                    </div>
                                ';
                    endif;

                    $z++;
                endforeach;

                $html_list_img .= '</div>';
            else:
                $html_list_img .= '
                        <a class="slide-link">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="b-lazy" data-src="'.$post_image.'" data-src-small="'.$post_image_medium.'" alt="" />
                        </a>';
            endif;
            $html_list_img .= '</div>';

            $html_list_desc = '
                        <div class="col-lg-6 col-md-12 list-desc-blue">
                            <div class="wrap-desc-mermaid">
                                <h4>Discover</h4>
                                <a href="'.$post_link.'"><h3>'.$post_title.'</h3></a>
                                <div class="description">'.$post_content.'</div>
                                <div class="wrap-button clearfix">
                                    <a href="'.$post_link.'" class="button-arrow">'.get_label_string( 'Read More', true ).'</a>
                                    <div class="button-schedule button-default">'.get_label_string( 'Schedules And Rates', true ).'</div>
                                    <div class="wrap-btn-sch-mermaid" data-on="0">
                                        <a href="'.get_site_url().'/schedule-rates/mermaid-i/" class="btn-schedule-mermaid">Mermaid I</a>
                                        <a href="'.get_site_url().'/schedule-rates/mermaid-ii/" class="btn-schedule-mermaid">Mermaid II</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ';

            $html_list_dest_mobile = '
                        <div class="row container-gradient mobile">
                            '.$html_list_img.'
                            '.$html_list_desc.'
                        </div>
                    ';

            if( $i < 3 ):
                $html_list_destination .= '
                            <div class="row container-gradient container-gradient-'.$i.' desktop">
                                '.$html_list_img.'
                                '.$html_list_desc.'
                            </div>
                        ';
            else:
                $post_content = strlen( $post_content ) > 300 ? rtrim( substr( $post_content, 0, 320 ) ).'...' : $post_content;
                $html_list_col .= '
                        <div class="list-dest">
                            <a href="'.$post_link.'">
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="b-lazy" data-src="'.$post_image.'" data-src-small="'.$post_image_medium.'">
                            </a>
                            <div class="container-desc">
                                <h5>DISCOVER</h5>
                                <a href="'.$post_link.'"><h3>'.$post_title.'</h3></a>
                                <div class="description">'.$post_content.'</div>
                                <a href="'.$post_link.'" class="button-arrow">'.get_label_string( 'Read More', true ).'</a>
                                <a href="'.get_site_url().'/schedule-rates/mermaid-i/" class="btn button-default">'.get_label_string( 'Schedules And Rates', true ).'</a>
                            </div>
                        </div>
                        ';
            endif;

            $i++;
        endwhile;
        wp_reset_postdata();

        ?>
        <div class="col-md-12 discover-destination">
            <div class="container-mermaid">
                <h2 class="title-section"><?php echo $title_page; ?></h2>
                <div class="line"></div>
                <?= $html_list_destination; ?>
                <?php if( !empty( $html_list_col ) ): ?>
                <div class="container-discover-list desktop">
                    <div class="inner clearfix">
                        <?= $html_list_col ?>
                    </div>
                </div>
                <?php endif; ?>
                <div class="wrap-list-dest-mobile"><?= $html_list_dest_mobile ?></div>
            </div>
        </div>
        <?php
    }
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan section diving in indonesia
| -------------------------------------------------------------------------------------
*/
if( ! function_exists( 'section_diving_indonesia' ) ):
    function section_diving_indonesia()
    {
        global $destination_data;
        $page = get_post( 103 );
        ?>
  
  
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7887.767264261131!2d115.2733812!3d-8.7025997!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x572f50c5cd77cf65!2sMermaid%20Liveaboards!5e0!3m2!1sen!2sth!4v1657780490460!5m2!1sen!2sth" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>



          <?php
                            $location = array();
        while ( $destination_data->have_posts() ):
            $destination_data->the_post();
            $post_id             = get_the_ID();
            $post_title          = get_the_title();
            $post_link           = get_permalink();
            $post_content        = wp_strip_all_tags( get_the_content() );
            $post_content        = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $post_content );
            $post_content        = apply_filters( 'the_content', $post_content );
            $post_content        = str_replace( ']]>', ']]&gt;', $post_content );
            $post_content        = strlen( $post_content ) > 70 ? rtrim( substr( $post_content, 0, 80 ) ).'...' : $post_content;
            $post_featured_image = get_the_post_thumbnail_url( $post_id, 'medium' );
            $post_lattitude      = get_post_meta( $post_id, '_destination_latitude', true );
            $post_longitude      = get_post_meta( $post_id, '_destination_longitude', true );
            $post_marker         = wp_get_attachment_image_url( get_post_meta( get_the_ID(), '_destination_icon_marker_id', 1 ), 'medium' );

            $location[] = array(
                'title'          => $post_title,
                'content'        => $post_content,
                'link'           => $post_link,
                'image'          => $post_featured_image,
                'latitude'       => $post_lattitude,
                'longitude'      => $post_longitude,
                'marker'         => $post_marker,
                'marker_active'  => THEME_URL_ASSETS . '/images/icon-marker-active.png',
                'marker_default' => THEME_URL_ASSETS . '/images/icon-marker-default.png'
            );

        endwhile;
        wp_reset_postdata();
        ?>

                <textarea name="json_location" id="" cols="30" rows="10" style="display: none;">
                    <?php echo json_encode( $location ); ?>
                </textarea>

            </div>
        </div>
<?php
    }
endif;


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan list news & events
| -------------------------------------------------------------------------------------
*/
if( ! function_exists( 'section_news_events' ) ):
    function section_news_events()
    {
        ?>

















        <div class="col-md-12 news-event">
            <div class="container-mermaid">
                <h2 class="title-section"><?php get_label_string( 'News And Events' ); ?></h2>
                <div class="line"></div>
            
                <div class="list-blue-default">
                    <?php
                            $args = array(
                                'post_status'    => 'publish',
                                'post_type'      => 'post',
                                'posts_per_page' => 5,
                                'category_name'  => 'news-events'
                            );
        $query = new WP_Query( $args );

        $total_news = $query->found_posts;

        $i = 1;
        while ( $query->have_posts() ) : $query->the_post();
            $post_title = get_the_title();
            $post_link  = get_permalink();
            $post_date  = date( "d M Y", strtotime( get_the_date() ) );

            // $post_content   = wp_strip_all_tags(get_the_content());
            // $post_content   = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
            // $post_content   = apply_filters('the_content', $post_content);
            // $post_content   = str_replace(']]>', ']]&gt;', $post_content);
            // $post_content   = strlen($post_content) > 100 ? rtrim(substr($post_content, 0, 120)).'...' : $post_content;

            // $post_content   = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', get_the_content() );
            // $post_content   = preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $post_content);
            // $post_content   = wp_trim_words( $post_content, 55, "" );
            $post_content = get_the_excerpt();

            $post_image    = get_the_post_thumbnail_url( get_the_ID(), 'medium' );
            $post_image    = empty( $post_image ) ? THEME_URL_ASSETS.'/images/img-default.jpg' : $post_image;
            $youtube_video = get_post_meta( get_the_ID(), '_post_youtube_video', 1 );

            $youtube_video_iframe = !empty( $youtube_video ) ? get_iframe_youtube( $youtube_video ) : '';
            $youtube_video_code   = !empty( $youtube_video ) ? get_youtube_video_ID( $youtube_video ) : '';

            if( $i <= 4 ):
                ?>
                        <div class="con-list-news">
                            <?php if( !empty( $youtube_video ) ): ?>
                                <div class="container-youtube-video">
                                    <a href="<?php echo $youtube_video; ?>" class="fancy-group">
                                        <div class="thumb-img">
                                            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $post_image ?>" data-src-small="<?php echo $post_image ?>" alt="">
                                            <div class="icon-play" data-play="0" data-youtube-code="<?php echo $youtube_video_code; ?>"></div>
                                        </div>
                                    </a>
                                    <div class="video-background" data-play="0" data-youtube-code="<?php echo $youtube_video_code; ?>">
                                        <div class="video-foreground">
                                            <?php //echo $youtube_video_iframe;?>
                                        </div>
                                    </div>
                                </div>
                            <?php else: ?>
                                <a href="<?php echo $post_link; ?>"><img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $post_image ?>" data-src-small="<?php echo $post_image ?>" alt=""></a>
                            <?php endif; ?>
                            <div class="container-desc">
                                <!--h5><?php echo $post_date; ?></h5-->
                                <a href="<?php echo $post_link; ?>"><h3><?php echo $post_title; ?></h3></a>
                                <div class="desc"><?php echo $post_content; ?></div>
                                <!--h6>by <?php //echo ucwords(get_the_author());?></h6-->
                            </div>
                        </div>
                    <?php
                    endif;

            $i++;
        endwhile;
        wp_reset_postdata();
        ?>
                </div>
                
                <?php
        if( $total_news > 4 ):
            echo '<a href="'.get_page_link( 373 ).'" class="button-default">'.get_label_string( 'View More News', true ).'</a>';
        endif;
        ?>
                

            </div>
        </div>
<?php
    }
endif;


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan list news & events
| -------------------------------------------------------------------------------------
*/
if( ! function_exists( 'section_reviews' ) ):
    function section_reviews()
    {
        ?>
        <div class="col-md-12 recent-reviews">
            <div class="container-mermaid">
                <h2 class="title-section"><?php get_label_string( 'Recent Reviews' ); ?></h2>
                <div class="line"></div>
                
                <div class="recent-reviews-list">

                    <div class="inner clearfix">
                        <?php
                                    $args = array(
                                        'post_status' => 'publish',
                                        'post_type'   => 'review',
                                    );
        $query = new WP_Query( $args );

        while ( $query->have_posts() ):
            $query->the_post();
            $post_title   = get_the_title();
            $post_date    = get_post_meta( get_the_ID(), '_review_date', true );
            $post_date    = empty( $post_date ) ? '' : '<h5>'.date( "d M Y", strtotime( $post_date ) ).'</h5>';
            $post_country = get_post_meta( get_the_ID(), '_review_country', true );
            $post_country = empty( $post_country ) ? '' : '<h5 class="country">'.$post_country.'</h5>';
            $post_content = wp_strip_all_tags( get_the_content() );
            $post_content = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $post_content );
            $post_content = apply_filters( 'the_content', $post_content );
            $post_content = str_replace( ']]>', ']]&gt;', $post_content );
            $post_content = strlen( $post_content ) > 180 ? rtrim( substr( $post_content, 0, 200 ) ).'...' : $post_content;
            $post_rate    = get_post_meta( get_the_ID(), '_review_rate', true );
            ?>
                    
                                <div class="item-list">
                                    <h3><?php echo $post_title; ?></h3>
                                    <?php
                    echo $post_country;
            echo $post_date;
            ?>
                                    <div class="star-review">
                                        <?php
                if( !empty( $post_rate ) ):
                    for( $i = 1; $i <= $post_rate; $i++ ):
                        echo '<span></span>';
                    endfor;
                endif;
            ?>
                                    </div>
                                    <div class="desc"><?php echo $post_content; ?></div>
                                </div>
                                <?php
        endwhile;
        wp_reset_postdata();
        ?>
                    </div>

                </div>
                
                <div class="button-reviews-button">
                    <button class="prev"></button>
                    <a href="<?php echo get_site_url(); ?>/reviews/" class="button-default"><?php get_label_string( 'Read More Reviews' ); ?></a>
                    <button class="next"></button>
                </div>
            </div>
        </div>
<?php
    }
endif;


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan latest news
| -------------------------------------------------------------------------------------
*/
if( ! function_exists( 'latest_news' ) ):
    function latest_news()
    {
        $args = array(
            'post_status'    => 'publish',
            'post_type'      => 'post',
            'order'          => 'DESC',
            'posts_per_page' => 1,
            'orderby'        => 'post_date',
        );
        $query = new WP_Query( $args );

        while ( $query->have_posts() ):
            $query->the_post();

            $post_title = get_the_title();
            $post_link  = get_permalink();
            $post_date  = date( "d M Y", strtotime( get_the_date() ) );
            // $post_content   = wp_strip_all_tags(get_the_content());
            // $post_content   = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
            // $post_content   = apply_filters('the_content', $post_content);
            // $post_content   = str_replace(']]>', ']]&gt;', $post_content);
            // $post_content   = strlen($post_content) > 100 ? rtrim(substr($post_content, 0, 80)).'...' : $post_content;

            $post_content = mb_strimwidth( strip_tags( get_the_content() ), 0, 100, '...' );
            $post_image   = get_the_post_thumbnail_url( get_the_ID(), 'medium' );

            $youtube_video        = get_post_meta( get_the_ID(), '_post_youtube_video', 1 );
            $youtube_video_iframe = !empty( $youtube_video ) ? get_iframe_youtube( $youtube_video ) : '';
            $youtube_video_code   = !empty( $youtube_video ) ? get_youtube_video_ID( $youtube_video ) : '';
            ?>

            <div class="col-md-3 latest-news">
                
                <?php if( !empty( $youtube_video ) ): ?>
                    <div class="container-youtube-video">
                        <a href="<?php echo $youtube_video; ?>" class="fancy-group">
                            <div class="thumb-img">
                                <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $post_image ?>" data-src-small="<?php echo $post_image ?>" alt="">
                                <div class="icon-play" data-play="0" data-youtube-code="<?php echo $youtube_video_code; ?>"></div>
                            </div>
                        </a>
                        <div class="video-background" data-play="0" data-youtube-code="<?php echo $youtube_video_code; ?>">
                            <div class="video-foreground"></div>
                        </div>
                    </div>
                <?php else: ?>
                    <a href="<?php echo $post_link; ?>"><img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $post_image ?>" data-src-small="<?php echo $post_image ?>" alt=""></a>
                <?php endif; ?>
                <div class="container-desc">
                    <h4><?php get_label_string( 'Latest News' ); ?></h4>
                    <a href="<?php echo $post_link; ?>"><h3><?php echo $post_title; ?></h3></a>
                    <div class="description"><?php echo $post_content; ?></div>
                </div>
                <a href="<?php echo $post_link; ?>" class="btn btn-readmore"><?php get_label_string( 'Read More News' ); ?></a>
            </div>
            
            <?php
        endwhile;
        wp_reset_postdata();
    }
endif;