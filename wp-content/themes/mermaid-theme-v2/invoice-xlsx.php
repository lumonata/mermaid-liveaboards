<?php

require_once( '../../../wp-load.php' ); 
require_once 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$query  = new WP_Query( array( 
    'p'              => $_GET['id'],
    'post_type'      => 'checkout',
    'post_status'    => 'publish', 
    'posts_per_page' => 1 
));

if( $query->have_posts() )
{
    while( $query->have_posts() )
    {
    	$query->the_post();

        $trip_code = get_post_meta( $post->ID, '_checkout_trip_code', true );

        //-- Trip Data            
        $trip = json_decode( base64_decode( $trip_code ) );

        if( $trip === null && json_last_error() !== JSON_ERROR_NONE )
        {
            $sdata = get_posts( array( 'post_type' => 'schedule_data', 's' => $trip_code ) );

            if( !empty( $sdata ) )
            {
                foreach( $sdata as $trip )
                {
                    setup_postdata( $trip );

                    if( $trip->post_title != '' )
                    {
                        $trip->post_meta = get_post_meta( $trip->ID );
                    }
                }
            }
        }
        else
        {
            $trip_code = $trip->post_title;
        }

        if( empty( $trip ) === false )
        {
            $ddate = get_post_meta( $post->ID, 'departure_date', true );
            $adate = get_post_meta( $post->ID, 'arrival_date', true );

            if( $ddate == '' || $adate == '' )
            {
                $ddate = $trip->post_meta->_schedule_data_departure_date[0];
                $adate = $trip->post_meta->_schedule_data_arrival_date[0];
            }
            
            $dep = strtotime( $ddate );
            $arv = strtotime( $adate );

            if( date( 'Y', $dep ) == date( 'Y', $arv ) )
            {
                $depart  = date( 'M d', $dep );
            }
            else
            {
                $depart  = date( 'M d Y', $dep );
            }

            $arrival = date( 'M d Y', $arv );

            $dpoint = get_post_meta( $post->ID, '_checkout_departure_point', true );
            $dtime  = get_post_meta( $post->ID, '_checkout_departure_time', true );
            $apoint = get_post_meta( $post->ID, '_checkout_arrival_point', true );
            $atime  = get_post_meta( $post->ID, '_checkout_arrival_time', true );

            $btype     = get_post_meta( $post->ID, '_checkout_booking_type', true );
            $itenerary = get_post_meta( $post->ID, '_checkout_iteneraries', true );
            $remarks   = get_post_meta( $post->ID, '_checkout_remarks', true );
            $boat      = get_post_meta( $post->ID, '_checkout_boat', true );
            $banks     = get_post_meta( $post->ID, '_checkout_bank_options', true );

            header( 'Content-Disposition: attachment;filename="invoice-' . sanitize_title( $post->post_title ) . '.xlsx' );
            header( 'Content-Type: application/xlsx' );
            header( 'Cache-Control: max-age=0' );
            
            $spread = new Spreadsheet();
            $sheet  = $spread->getActiveSheet();

            //-- SETTING global style
            $spread->getDefaultStyle()->applyFromArray( array( 'font'  => array( 'size' => 11, 'name' => 'Arial' ) ) );
            
            //-- SETTING page
            $sheet->getPageMargins()->setTop( 0.2 );
            $sheet->getPageMargins()->setLeft( 0.2 );
            $sheet->getPageMargins()->setRight( 0.2 );
            $sheet->getPageMargins()->setBottom( 0.2 );

            $sheet->getPageSetup()->setFitToWidth( 1 );
            $sheet->getPageSetup()->setFitToHeight( 0 );

            $sheet->getPageSetup()->setHorizontalCentered( true );

            //-- SET style
            $sheet->getColumnDimension( 'A' )->setWidth( 15 );
            $sheet->getColumnDimension( 'B' )->setWidth( 11.57 );
            $sheet->getColumnDimension( 'C' )->setWidth( 15.57 );
            $sheet->getColumnDimension( 'D' )->setWidth( 9.86 );
            $sheet->getColumnDimension( 'E' )->setWidth( 12 );
            $sheet->getColumnDimension( 'F' )->setWidth( 12 );
            $sheet->getColumnDimension( 'G' )->setWidth( 14 );
            $sheet->getColumnDimension( 'H' )->setWidth( 14 );
            $sheet->getColumnDimension( 'I' )->setWidth( 17 );
            $sheet->getColumnDimension( 'J' )->setWidth( 17 );

            //-- ADD logo
            $sheet->mergeCells( 'A1:I1' );
            $sheet->getRowDimension( 1 )->setRowHeight( 72 );

            $drawing = new Drawing();
            $drawing->setName( 'Mermaid Liveaboards Logo' );
            $drawing->setDescription( 'Mermaid Liveaboards Logo' );
            $drawing->setPath( 'assets/images/logo.png');
            $drawing->setHeight( 85 );
            $drawing->setOffsetX( -50 );
            $drawing->setCoordinates( 'E1' );
            $drawing->setWorksheet( $sheet );

            //-- SET value
            $sheet->mergeCells( 'A2:I2' );
            $sheet->setCellValue( 'A2', 'Confirmation/ Invoice' );

            //-- SET style
            $sheet->getStyle( 'A2:I2' )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FF001B48' );
            $sheet->getStyle( 'A2' )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER );
            $sheet->getStyle ('A2' )->getFont()->getColor()->setARGB( 'FFFFFFFF' );
            $sheet->getStyle( 'A2' )->getFont()->setSize( 16 )->setBold( true );

            //-- SET value
            $sheet->mergeCells( 'A3:C3' );
            $sheet->mergeCells( 'D3:I3' );
            $sheet->setCellValue( 'A3', 'Invoice No.' );
            $sheet->setCellValue( 'D3', 'Issue Date' );

            //-- SET style
            $sheet->getStyle( 'A3:I3' )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A3:I3' )->getFont()->setBold( true );

            //-- SET value
            $sheet->mergeCells( 'A4:C4' );
            $sheet->mergeCells( 'D4:I4' );
            $sheet->setCellValue( 'A4', get_the_title() );
            $sheet->setCellValue( 'D4', get_the_date() );

            //-- SET style
            $sheet->getStyle( 'A4:I4' )->getFont()->setSize( 12 )->setBold( true );

            //-- SET value
            $sheet->mergeCells( 'A5:C5' );
            $sheet->mergeCells( 'D5:I5' );
            $sheet->setCellValue( 'A5', 'Guest/Agent' );
            $sheet->setCellValue( 'D5', 'Guest Name/s' );

            //-- SET style
            $sheet->getStyle( 'A5:I5' )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A5:I5' )->getFont()->setBold( true );

            if( $btype == '1' || $btype == '3' )
            {
                $agent_id = get_post_meta( $post->ID, '_checkout_agent_name', true );

                if( !empty( $agent_id ) )
                {                    
                    $agent_name = the_agent_list( $agent_id );
                    $agent_addr = get_post_meta( $post->ID, '_checkout_agent_address', true );

                    if( empty( $agent_addr ) === false )
                    {
                        $sheet->mergeCells( 'A6:C6' );
                        $sheet->mergeCells( 'A7:C7' );
                        $sheet->mergeCells( 'A8:C8' );
                        $sheet->mergeCells( 'A9:C9' );
                        $sheet->setCellValue( 'A6', htmlspecialchars_decode( $agent_name ) );
                        $sheet->setCellValue( 'A7', htmlspecialchars_decode( $agent_addr ) );
                    }
                    else
                    {
                        $sheet->mergeCells( 'A6:C6' );
                        $sheet->mergeCells( 'A7:C7' );
                        $sheet->mergeCells( 'A8:C8' );
                        $sheet->setCellValue( 'A6', htmlspecialchars_decode( $agent_name ) );
                    }
                }
            }
            else if( $btype == '2' || $btype == '4' )
            {
                $country_code = get_post_meta( $post->ID, '_checkout_country', true );

                if( empty( $country_code ) === false )
                {
                    $sheet->mergeCells( 'A6:C6' );
                    $sheet->mergeCells( 'A7:C7' );
                    $sheet->mergeCells( 'A8:C8' );
                    $sheet->mergeCells( 'A9:C9' );
                    $sheet->setCellValue( 'A9', the_country_list( $country_code  ) );
                }
                else
                {
                    $sheet->mergeCells( 'A6:C6' );
                    $sheet->mergeCells( 'A7:C7' );
                    $sheet->mergeCells( 'A8:C8' );
                }

                if( $btype == '2' )
                {
                    $sheet->setCellValue( 'A6', 'Direct' );
                }
                else
                {
                    $sheet->setCellValue( 'A6', 'Direct Charter' );
                }
            }

            $sheet->mergeCells( 'D6:I6' );
            $sheet->mergeCells( 'D7:I7' );
            $sheet->mergeCells( 'D8:I8' );
            $sheet->mergeCells( 'D9:I9' );
            $sheet->setCellValue( 'D6', get_post_meta( $post->ID, '_checkout_person_name', true ) );

            $row = 10;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':B' . $row );
            $sheet->mergeCells( 'D' . $row . ':F' . $row );
            $sheet->mergeCells( 'G' . $row . ':I' . $row );

            $sheet->setCellValue( 'A' . $row, 'Vessel' );
            $sheet->setCellValue( 'C' . $row, 'Trip Code' );
            $sheet->setCellValue( 'D' . $row, 'Trip Date' );
            $sheet->setCellValue( 'G' . $row, 'Destination' );

            //-- SET style
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':B' . $row );
            $sheet->mergeCells( 'D' . $row . ':F' . $row );
            $sheet->mergeCells( 'G' . $row . ':I' . $row );

            $sheet->setCellValue( 'A' . $row, get_the_title( $boat ) );
            $sheet->setCellValue( 'C' . $row, $trip_code );
            $sheet->setCellValue( 'D' . $row, sprintf( '%s - %s', $depart, $arrival ) );
            $sheet->setCellValue( 'G' . $row, html_entity_decode( get_the_title( $itenerary ), ENT_QUOTES, 'UTF-8' ) );

            //-- SET style
            $sheet->getRowDimension( $row )->setRowHeight( 50 );
            $sheet->getStyle( 'A' . $row . ':G' . $row )->getAlignment()->setWrapText( true );
            $sheet->getStyle( 'A' . $row . ':G' . $row )->getAlignment()->setVertical( \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP );
            $row++;

            $cruise = get_metadata( 'post', $post->ID, '_checkout_cruise_list' );
            $ctotal = 0;

            if( empty( $cruise ) === false )
            {
                //-- SET value
                // $sheet->mergeCells( 'A' . $row . ':I' . $row );
                // $sheet->setCellValue( 'A' . $row, 'Cruise List' );

                //-- SET style
                // $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
                // $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );

                // $row++;
                
                foreach( $cruise as $list )
                {
                    foreach( $list as $c )
                    {
                        if( isset( $c[ 'repeat_guest_disc' ] ) && !empty( $c[ 'repeat_guest_disc' ] ) )
                        {
                            $discount_guest =  $c[ 'disc_type' ];
                            $discount_guest_val =  floatval( $c[ 'repeat_guest_disc' ] );
                            $discount_guest_text = $c[ 'disc_type' ] . ' (%) ' . number_format( $c[ 'repeat_guest_disc' ], 0 );
                        }
                        
                         if( $btype == '1' || $btype == '2' )
                         {
                            if( isset( $c[ 'other_disc_value' ] ) && !empty( $c[ 'other_disc_value' ] ) )
                            {
                                $discount_other =  $c[ 'other_disc_type' ];
                                $discount_other_val =  floatval( $c[ 'other_disc_value' ] );
                                $discount_other_text = $c[ 'other_disc_type' ] . ' (%) ' . number_format( $c[ 'other_disc_value' ], 0 );
                            }
                         }
                    }
                }
                
                //-- SET value
                if( $btype == '1' || $btype == '3' )
                {
                    $sheet->mergeCells( 'A' . $row . ':B' . $row );
                }
                else
                {
                    $sheet->mergeCells( 'A' . $row . ':B' . $row );
                    // $sheet->mergeCells( 'G' . $row . ':H' . $row );
                }

                $sheet->setCellValue( 'A' . $row, 'Cruise Length' );
                $sheet->setCellValue( 'C' . $row, 'Cabin Type' );
                $sheet->setCellValue( 'D' . $row, 'No. Guest' );
                $sheet->setCellValue( 'E' . $row, 'Gross (€)' );
                // $sheet->setCellValue( 'F' . $row, $discount_guest ); 
                // $sheet->setCellValue( 'G' . $row, $discount_other );
                if( !empty( $discount_guest ) && $discount_guest_val > 0 && !empty( $discount_other ) && $discount_other_val > 0 ) :
                    $sheet->setCellValue( 'F' . $row, $discount_guest );
                    $sheet->setCellValue( 'G' . $row, $discount_other );
                elseif( !empty( $discount_other ) && $discount_other_val > 0) :
                    $sheet->setCellValue( 'F' . $row, $discount_other );
                elseif( !empty( $discount_guest ) && $discount_guest_val > 0) :
                    $sheet->setCellValue( 'F' . $row, $discount_guest ); 
                endif;
                
                if( $btype == '1' || $btype == '3' )
                {
                    if( !empty( $discount_guest ) && $discount_guest_val > 0 && !empty( $discount_other ) && $discount_other_val > 0 ) :
                      $sheet->setCellValue( 'H' . $row, 'Comm. (%)' );
                      $sheet->setCellValue( 'I' . $row, 'Nett (€)' );
                    elseif( !empty( $discount_other ) && $discount_other_val > 0) :
                      $sheet->setCellValue( 'G' . $row, 'Comm. (%)' );
                      $sheet->setCellValue( 'H' . $row, 'Nett (€)' );
                    elseif( !empty( $discount_guest ) && $discount_guest_val > 0 ) :
                      $sheet->setCellValue( 'G' . $row, 'Comm. (%)' );
                      $sheet->setCellValue( 'H' . $row, 'Nett (€)' );
                    else :
                        $sheet->setCellValue( 'G' . $row, 'Comm. (%)' );
                        $sheet->setCellValue( 'H' . $row, 'Nett (€)' );
                    endif;
                }
                else
                {
                    if( !empty( $discount_guest ) && $discount_guest_val > 0 && !empty( $discount_other ) && $discount_other_val > 0 ) :
                      $sheet->setCellValue( 'H' . $row, 'Nett (€)' );
                    elseif( !empty( $discount_other ) && $discount_other_val > 0) :
                        $sheet->mergeCells( 'G' . $row . ':H' . $row );
                      $sheet->setCellValue( 'G' . $row, 'Nett (€)' );
                    elseif( !empty( $discount_guest ) && $discount_guest_val > 0 ) :
                        $sheet->mergeCells( 'G' . $row . ':H' . $row );
                        $sheet->setCellValue( 'G' . $row, 'Nett (€)' );
                    else:
                        $sheet->mergeCells( 'G' . $row . ':H' . $row );
                        $sheet->setCellValue( 'G' . $row, 'Nett (€)' );
                    endif;
                    
                }

                 if( !empty( $discount_guest ) && $discount_guest_val > 0 && !empty( $discount_other ) && $discount_other_val > 0 ) :
                      $sheet->setCellValue( 'I' . $row, 'Total Nett (€ )' );
                 elseif( !empty( $discount_other ) && $discount_other_val > 0) :
                      $sheet->setCellValue( 'I' . $row, 'Total Nett (€ )' );
                 elseif( !empty( $discount_guest ) && $discount_guest_val > 0 ) :
                      $sheet->setCellValue( 'I' . $row, 'Total Nett (€ )' );
                 else:
                    $sheet->setCellValue( 'I' . $row, 'Total Nett (€ )' );
                 endif;
                

                //-- SET style
                $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
                $sheet->getStyle( 'D' . $row . ':I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
                $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
                $row++;

                foreach( $cruise as $list )
                {
                    foreach( $list as $c )
                    {
                        $discone    = isset( $c[ 'repeat_guest_disc' ] ) ? floatval( $c[ 'repeat_guest_disc' ] ) : 0;
                        $total_nett = isset( $c[ 'total_nett' ] ) ? floatval( $c[ 'total_nett' ] ) : 0;
                        $gross      = isset( $c[ 'gross' ] ) ? floatval( $c[ 'gross' ] ) : 0;
                        $nett       = isset( $c[ 'nett' ] ) ? floatval( $c[ 'nett' ] ) : 0;
                        $disctwo    = 0;
                        $commission = 0;

                        $discount_guest         = $c[ 'disc_type' ];
                        $discount_guest_val     = floatval( $c[ 'repeat_guest_disc' ] );
                        $discount_guest_text    = $c[ 'disc_type' ] . ' (%) ' . number_format( $c[ 'repeat_guest_disc' ], 0 );
                        
                        if( $btype == '1' || $btype == '2' )
                        {
                            $disctwo = isset( $c[ 'other_disc_value' ] ) ? floatval( $c[ 'other_disc_value' ] ) : 0;
                            
                            if( isset( $c[ 'other_disc_value' ] ) && !empty( $c[ 'other_disc_value' ] ) )
                            {
                                $discount_other =  $c[ 'other_disc_type' ];
                                $discount_other_val =  floatval( $c[ 'other_disc_value' ] );
                                $discount_other_text = $c[ 'other_disc_type' ] . ' (%) ' . number_format( $c[ 'other_disc_value' ], 0 );
                            }
                        }

                        if( $btype == '1' || $btype == '3' )
                        {
                            $commission = isset( $c[ 'commission_value' ] ) ? floatval( $c[ 'commission_value' ] ) : 0;
                        }

                        if( $discone > 0 && $disctwo > 0 )
                        {
                            $discount = number_format( $discone, 0 ) . ' + ' . number_format( $disctwo, 0 );
                        }
                        else
                        {
                            $discount = number_format( $discone, 0 );
                        }

                        $ctotal += floatval( $c[ 'total_nett' ] );

                        //-- SET value
                        if( $btype == '1' || $btype == '3' )
                        {
                            $sheet->mergeCells( 'A' . $row . ':B' . $row );
                        }
                        else
                        {
                            $sheet->mergeCells( 'A' . $row . ':B' . $row );
                            // $sheet->mergeCells( 'G' . $row . ':H' . $row );
                        }

                        $sheet->setCellValue( 'A' . $row, $c[ 'cruise_details' ] );
                        $sheet->setCellValue( 'C' . $row, $c[ 'cabin_type' ] );
                        $sheet->setCellValue( 'D' . $row, $c[ 'no_guest' ] );
                        $sheet->setCellValue( 'E' . $row, $gross );
                        
                        if( !empty( $discount_guest ) && $discount_guest_val > 0 && !empty( $discount_other ) && $discount_other_val > 0 ) :
                            $sheet->setCellValue( 'F' . $row, $discount_guest_val );
                            $sheet->setCellValue( 'G' . $row, $discount_other_val );
                        elseif( !empty( $discount_other ) && $discount_other_val > 0) :
                            $sheet->setCellValue( 'F' . $row, $discount_other_val );
                        elseif( !empty( $discount_guest ) && $discount_guest_val > 0) :
                            $sheet->setCellValue( 'F' . $row, $discount_guest_val ); 
                        endif;
                
                        if( $btype == '1' || $btype == '3' )
                        {
                            if( !empty( $discount_guest ) && $discount_guest_val > 0 && !empty( $discount_other ) && $discount_other_val > 0 ) :
                              $sheet->setCellValue( 'H' . $row, $commission );
                              $sheet->setCellValue( 'I' . $row, $nett );
                            elseif( !empty( $discount_other ) && $discount_other_val > 0) :
                              $sheet->setCellValue( 'G' . $row, $commission );
                              $sheet->setCellValue( 'H' . $row, $nett );
                            elseif( !empty( $discount_guest ) && $discount_guest_val > 0 ) :
                              $sheet->setCellValue( 'G' . $row, $commission );
                              $sheet->setCellValue( 'H' . $row, $nett );
                            else:
                                 $sheet->setCellValue( 'G' . $row, $commission );
                                $sheet->setCellValue( 'H' . $row, $nett );
                            endif;
                        }
                        else
                        {
                            if( !empty( $discount_guest ) && $discount_guest_val > 0 && !empty( $discount_other ) && $discount_other_val > 0 ) :
                              $sheet->setCellValue( 'H' . $row, $nett );
                            elseif( !empty( $discount_other ) && $discount_other_val > 0) :
                                $sheet->mergeCells( 'G' . $row . ':H' . $row );
                              $sheet->setCellValue( 'G' . $row, $nett );
                            elseif( !empty( $discount_guest ) && $discount_guest_val > 0 ) :
                                $sheet->mergeCells( 'G' . $row . ':H' . $row );
                                $sheet->setCellValue( 'G' . $row, $nett );
                            else:
                                 $sheet->mergeCells( 'G' . $row . ':H' . $row );
                                $sheet->setCellValue( 'G' . $row, $nett );
                            endif;
                            
                        }
                        
                         if( !empty( $discount_guest ) && $discount_guest_val > 0 && !empty( $discount_other ) && $discount_other_val > 0 ) :
                              $sheet->setCellValue( 'I' . $row, $total_nett );
                         elseif( !empty( $discount_other ) && $discount_other_val > 0) :
                              $sheet->setCellValue( 'I' . $row, $total_nett );
                         elseif( !empty( $discount_guest ) && $discount_guest_val > 0 ) :
                              $sheet->setCellValue( 'I' . $row, $total_nett );
                         else:
                              $sheet->setCellValue( 'I' . $row, $total_nett );
                         endif;
                         
                        //-- SET style
                        $sheet->getStyle( 'A' . $row . ':I' . $row )->getAlignment()->setWrapText( true );
                        $sheet->getStyle( 'A' . $row . ':I' . $row )->getAlignment()->setVertical( \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP );
                        $sheet->getStyle( 'D' . $row . ':I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
                        $sheet->getStyle( 'E' . $row . ':I' . $row )->getNumberFormat()->setFormatCode('#,##');
                        $row++;
                    }
                }
            }

            if( $btype == '1' || $btype == '3' )
            {
                $sheet->mergeCells( 'A' . $row . ':B' . $row );
                $row++;

                $sheet->mergeCells( 'A' . $row . ':B' . $row );
                $row++;
            }
            else
            {
                $sheet->mergeCells( 'A' . $row . ':B' . $row );
                $sheet->mergeCells( 'G' . $row . ':H' . $row );
                $row++;

                $sheet->mergeCells( 'A' . $row . ':B' . $row );
                $sheet->mergeCells( 'G' . $row . ':H' . $row );
                $row++;
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':H' . $row );

            $sheet->setCellValue( 'A' . $row, 'Total' );
            $sheet->setCellValue( 'I' . $row, $ctotal );

            //-- SET style
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'I' . $row )->getNumberFormat()->setFormatCode('#,##');
            $row++;

            $travel = get_metadata( 'post', $post->ID, '_checkout_travel_service' );
            $ttotal  = 0;

            if( empty( $travel ) === false )
            {
                //-- SET value
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, 'Travel Services' );

                //-- SET style
                $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
                $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                $row++;

                //-- SET value
                $sheet->mergeCells( 'B' . $row . ':E' . $row );

                $sheet->setCellValue( 'A' . $row, 'Date' );
                $sheet->setCellValue( 'B' . $row, 'Service' );
                $sheet->setCellValue( 'F' . $row, 'Units' );
                $sheet->setCellValue( 'G' . $row, 'Nett (IDR)' );
                $sheet->setCellValue( 'H' . $row, 'Nett (€)' );
                $sheet->setCellValue( 'I' . $row, 'Total Nett (€ )' );

                //-- SET style
                $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
                $sheet->getStyle( 'D' . $row . ':I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
                $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
                $row++;

                $idr_ex = 0;
                $trv_no = 3;

                if( array_sum( array_column( $travel[0], 'total_nett' ) ) > 0 )
                {
                    foreach( $travel as $list )
                    {
                        foreach( $list as $t )
                        {
                            $date    = empty( $t[ 'date' ] ) ? '-' : date( 'F d Y', strtotime( $t[ 'date' ] ) );
                            $netteur = empty( $t[ 'nett_euro' ] ) ? '-' : number_format( $t[ 'nett_euro' ], 0 );
                            $nettidr = empty( $t[ 'nett_idr' ] ) ? '0' : number_format( $t[ 'nett_idr' ], 0 );
                            $tnett   = empty( $t[ 'total_nett' ] ) ? '0' : number_format( $t[ 'total_nett' ], 0 );
                            $guests  = empty( $t[ 'guests' ] ) ? '0' : $t[ 'guests' ];
                            $idr_ex  = round( $t[ 'nett_idr' ] / $t[ 'nett_euro' ] );

                            if( empty( $t[ 'service_route' ] ) && empty( $t[ 'hotel_flight' ] ) )
                            {
                                $sroute = '-';
                            }
                            elseif( empty( $t[ 'service_route' ] ) && !empty( $t[ 'hotel_flight' ] ) )
                            {
                                $sroute = $t[ 'hotel_flight' ];
                            }
                            elseif( !empty( $t[ 'service_route' ] ) && empty( $t[ 'hotel_flight' ] ) )
                            {
                                $sroute = $t[ 'service_route' ];
                            }
                            else
                            {
                                $sroute = $t[ 'service_route' ] . '/' . $t[ 'hotel_flight' ];
                            }

                            $ttotal += floatval( $t[ 'total_nett' ] );

                            //-- SET value
                            $sheet->mergeCells( 'B' . $row . ':E' . $row );

                            $sheet->setCellValue( 'A' . $row, $date );
                            $sheet->setCellValue( 'B' . $row, $sroute );
                            $sheet->setCellValue( 'F' . $row, $guests );
                            $sheet->setCellValue( 'G' . $row, $nettidr );
                            $sheet->setCellValue( 'H' . $row, $netteur );
                            $sheet->setCellValue( 'I' . $row, $tnett );

                            //-- SET style
                            //$sheet->getRowDimension( $row )->setRowHeight( 50 );
                            $sheet->getStyle( 'A' . $row )->getAlignment()->setWrapText( true );
                            $sheet->getStyle( 'A' . $row . ':I' . $row )->getAlignment()->setVertical( \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP );
                            $sheet->getStyle( 'D' . $row . ':I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );

                            $trv_no--;
                            $row++;
                        }
                    }
                }
                
                if( $trv_no > 0 )
                {
                    for( $i = 0; $i < $trv_no ; $i++ )
                    { 
                        //-- SET value
                        $sheet->mergeCells( 'B' . $row . ':C' . $row );
                        $sheet->mergeCells( 'D' . $row . ':E' . $row );

                        $row++;
                    }
                }

                //-- SET value
                $sheet->mergeCells( 'A' . $row . ':E' . $row );
                $sheet->mergeCells( 'G' . $row . ':H' . $row );

                $sheet->setCellValue( 'A' . $row, 'IDR xe' );
                $sheet->setCellValue( 'G' . $row, 'Total' );
                $sheet->setCellValue( 'F' . $row, number_format( $idr_ex, 0 ) );
                $sheet->setCellValue( 'I' . $row, number_format( $ttotal, 0 ) );

                //-- SET style
                $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
                $sheet->getStyle( 'A' . $row . ':I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
                $sheet->getStyle( 'F' . $row )->getNumberFormat()->setFormatCode('#,##');
                $sheet->getStyle( 'I' . $row )->getNumberFormat()->setFormatCode('#,##');
                $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
                $row++;
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':E' . $row );
            $sheet->mergeCells( 'G' . $row . ':H' . $row );

            $sheet->setCellValue( 'A' . $row, 'Remarks' );
            $sheet->setCellValue( 'F' . $row, '% Due' );
            $sheet->setCellValue( 'G' . $row, 'Due Date' );
            $sheet->setCellValue( 'I' . $row, 'Grand Total' );

            //-- SET style
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );

            $sheet->getStyle( 'F' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'G' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER );
            $sheet->getStyle( 'I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $row++;

            $payment  = get_metadata( 'post', $post->ID, '_checkout_payment_terms' );
            $deposit  = array_sum( array_column( $payment[0], 'total_nett' ) );
            $dpercent = array_sum( array_column( $payment[0], 'percentage' ) );
            $gtotal   = $ctotal + $ttotal;

            if( empty( $payment ) === false && $deposit > 0 )
            {
                foreach( $payment as $list )
                {
                    foreach( $list as $p )
                    {
                        //-- SET value
                        $sheet->mergeCells( 'D' . $row . ':E' . $row );
                        $sheet->mergeCells( 'G' . $row . ':H' . $row );

                        if( reset( $list ) == $p )
                        {
                            $sheet->mergeCells( 'A' . $row . ':C' . ( $row + count( $list ) + 2 ) );
                            $sheet->setCellValue( 'A' . $row, $remarks );

                            $sheet->getStyle( 'A' . $row )->getAlignment()->setWrapText( true );
                            $sheet->getStyle( 'A' . $row )->getAlignment()->setVertical( \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP );
                        }

                        $sheet->setCellValue( 'D' . $row, $p[ 'label' ] );
                        $sheet->setCellValue( 'G' . $row, $p[ 'due_date' ] );
                        $sheet->setCellValue( 'I' . $row, $p[ 'total_nett' ] );
                        $sheet->setCellValue( 'F' . $row, $p[ 'percentage' ] . '%' );

                        //-- SET style
                        $sheet->getStyle( 'D' . $row )->getFont()->setBold( true );
                        $sheet->getStyle( 'F' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
                        $sheet->getStyle( 'G' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER );
                        $sheet->getStyle( 'I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
                        $sheet->getStyle( 'I' . $row )->getNumberFormat()->setFormatCode('#,##');

                        $row++;
                    }
                }
            }
            else
            {
                $sheet->mergeCells( 'A' . $row . ':C' . ( $row + 2 ) );
                $sheet->setCellValue( 'A' . $row, $remarks );

                $sheet->getStyle( 'A' . $row )->getAlignment()->setWrapText( true );
                $sheet->getStyle( 'A' . $row )->getAlignment()->setVertical( \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP );
                $sheet->getStyle( 'I' . $row )->getNumberFormat()->setFormatCode('#,##');
            }

            //-- SET value
            $sheet->mergeCells( 'D' . $row . ':E' . $row );
            $sheet->mergeCells( 'G' . $row . ':H' . $row );

            $balance  = $ctotal - $deposit;
            $bpercent = 100 - $dpercent;

            $sheet->setCellValue( 'D' . $row, 'Cruise Balance' );
            $sheet->setCellValue( 'G' . $row, '' );
            $sheet->setCellValue( 'I' . $row, $balance );
            $sheet->setCellValue( 'F' . $row, $bpercent . '%' );

            //-- SET style
            $sheet->getStyle( 'D' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'F' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'G' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER );
            $sheet->getStyle( 'I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'I' . $row )->getNumberFormat()->setFormatCode('#,##');
            $row++;

            //-- SET value
            $sheet->mergeCells( 'D' . $row . ':E' . $row );
            $sheet->mergeCells( 'G' . $row . ':H' . $row );
            
            $sheet->setCellValue( 'D' . $row, 'Total Cruise' );
            $sheet->setCellValue( 'F' . $row, '100%' );
            $sheet->setCellValue( 'I' . $row, $ctotal );            

            //-- SET style
            $sheet->getStyle( 'D' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'F' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'G' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER );
            $sheet->getStyle( 'I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'I' . $row )->getNumberFormat()->setFormatCode('#,##');
            $row++;

            //-- SET value
            $sheet->mergeCells( 'D' . $row . ':E' . $row );
            $sheet->mergeCells( 'G' . $row . ':H' . $row );

            $sheet->setCellValue( 'D' . $row, 'Travel Services' );
            $sheet->setCellValue( 'G' . $row, get_post_meta( $post->ID, '_checkout_travel_service_payment_date', true ) );
            $sheet->setCellValue( 'F' . $row, '100%' );
            $sheet->setCellValue( 'I' . $row, $ttotal );

            //-- SET style
            $sheet->getStyle( 'D' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'F' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'G' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER );
            $sheet->getStyle( 'I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'I' . $row )->getNumberFormat()->setFormatCode('#,##');
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':H' . $row );

            $sheet->setCellValue( 'A' . $row, 'Grand Total' );
            $sheet->setCellValue( 'I' . $row, $gtotal );

            //-- SET style
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'I' . $row )->getNumberFormat()->setFormatCode('#,##');
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );

            $sheet->setCellValue( 'A' . $row, 'Departure' );
            $sheet->setCellValue( 'D' . $row, 'Arrival' );

            //-- SET style
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );

            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Point : ' )->getFont()->setBold( true );
            $btext->createText( $dpoint );
            $sheet->setCellValue( 'A' . $row, $btext );

            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Point : ' )->getFont()->setBold( true );
            $btext->createText( $apoint );
            $sheet->setCellValue( 'D' . $row, $btext );
            $row++;

            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );

            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Time : ' )->getFont()->setBold( true );
            $btext->createText( $dtime );
            $sheet->setCellValue( 'A' . $row, $btext );

            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Time : ' )->getFont()->setBold( true );
            $btext->createText( $atime );
            $sheet->setCellValue( 'D' . $row, $btext );
            $row++;

            if( $banks == '0' )
            {
                $btransfer = get_post_meta( $post->ID, '_checkout_bank_transfer_detail', true );

                if( $btransfer != '' )
                {
                    $bank_list = bank_get_option( 'bank_list' );

                    if( isset( $bank_list[ $btransfer ] ) )
                    {
                        //-- SET value
                        $sheet->mergeCells( 'A' . $row . ':I' . $row );
                        $sheet->setCellValue( 'A' . $row, 'Bank Transfer Details' );

                        //-- SET style
                        $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
                        $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
                        $row++;

                        //-- SET value
                        $sheet->mergeCells( 'B' . $row . ':I' . $row );
                        $sheet->setCellValue( 'A' . $row, 'Receiver' );
                        $sheet->setCellValue( 'B' . $row, $bank_list[ $btransfer ][ 'receiver' ] );
                        $sheet->getStyle( 'B' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT );
                        $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                        $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                        $row++;

                        //-- SET value
                        $sheet->mergeCells( 'B' . $row . ':I' . $row );
                        $sheet->setCellValue( 'A' . $row, 'Account No' );
                        $sheet->setCellValue( 'B' . $row, $bank_list[ $btransfer ][ 'account_number' ] );
                        $sheet->getStyle( 'B' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT );
                        $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                        $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                        $row++;

                        //-- SET value
                        $sheet->mergeCells( 'B' . $row . ':I' . $row );
                        $sheet->setCellValue( 'A' . $row, 'Swift Code' );
                        $sheet->setCellValue( 'B' . $row, $bank_list[ $btransfer ][ 'swift_code' ] );
                        $sheet->getStyle( 'B' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT );
                        $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                        $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                        $row++;

                        //-- SET value
                        $sheet->mergeCells( 'B' . $row . ':I' . $row );
                        $sheet->setCellValue( 'A' . $row, 'Bank Name' );
                        $sheet->setCellValue( 'B' . $row, $bank_list[ $btransfer ][ 'bank_name' ] );
                        $sheet->getStyle( 'B' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT );
                        $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                        $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                        $row++;

                        //-- SET value
                        $sheet->mergeCells( 'B' . $row . ':I' . $row );
                        $sheet->setCellValue( 'A' . $row, 'Bank Address' );
                        $sheet->setCellValue( 'B' . $row, $bank_list[ $btransfer ][ 'bank_address' ] );
                        $sheet->getStyle( 'B' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT );
                        $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                        $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                        $row++;
                    }
                }
            }
            else
            {
                $receiver       = get_post_meta( $post->ID, '_checkout_receiver', true );
                $bank_name      = get_post_meta( $post->ID, '_checkout_bank_name', true );
                $swift_code     = get_post_meta( $post->ID, '_checkout_swift_code', true );
                $bank_address   = get_post_meta( $post->ID, '_checkout_bank_address', true );
                $account_number = get_post_meta( $post->ID, '_checkout_account_number', true );

                //-- SET value
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, 'Bank Transfer Details' );

                //-- SET style
                $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
                $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
                $row++;

                //-- SET value
                $sheet->mergeCells( 'B' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, 'Receiver' );
                $sheet->setCellValue( 'B' . $row, $receiver );
                $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                $row++;

                //-- SET value
                $sheet->mergeCells( 'B' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, 'Account No' );
                $sheet->setCellValue( 'B' . $row, $account_number );
                $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                $row++;

                //-- SET value
                $sheet->mergeCells( 'B' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, 'Swift Code' );
                $sheet->setCellValue( 'B' . $row, $swift_code );
                $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                $row++;

                //-- SET value
                $sheet->mergeCells( 'B' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, 'Bank Name' );
                $sheet->setCellValue( 'B' . $row, $bank_name );
                $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                $row++;

                //-- SET value
                $sheet->mergeCells( 'B' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, 'Bank Address' );
                $sheet->setCellValue( 'B' . $row, $bank_address );
                $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                $sheet->getStyle( 'B' . $row )->getFont()->setBold( true );
                $row++;
            }

            //-- SET border
            for( $i = 2; $i < $row; $i++ )
            {
                foreach( array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I' ) as $cell )
                {
                    $sheet->getStyle( $cell . $i )->getBorders()->getBottom()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
                    $sheet->getStyle( $cell . $i )->getBorders()->getRight()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
                    $sheet->getStyle( $cell . $i )->getBorders()->getLeft()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
                    $sheet->getStyle( $cell . $i )->getBorders()->getTop()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
                }
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( "Registered Company name and address for bank transfers: please include guest name/inv no. on payments\n" )->getFont()->setName( 'Arial' )->setSize( 10 )->setItalic( true );
            $btext->createText( "CMC Marine Ltd: Room 3603, Level 36, Tower 1, Enterprise Square Five, 38 Wang Chui Rd Kowloon Bay, Kowloon, Hong Kong\n" );
            $btext->createTextRun( "Indonesia Operations + Bali Guest Lounge address : ")->getFont()->setName( 'Arial' )->setSize( 10 )->setItalic( true );
            $btext->createText( "Mermaid Liveaboards - PT Indonesia Liveaboards\n" );
            $btext->createText( "75 Jl Danau Tamblingan Sanur Bali Indonesia ph: +62 0361 285 054\n" );
            $btext->createText( "Important : Please include invoice No./Company or GuestName on all Bank Transfer documents" );
            $sheet->setCellValue( 'A' . $row, $btext );

            //-- SET style
            $sheet->getRowDimension( $row )->setRowHeight( 12.75 * 6 );
            $sheet->getStyle( 'A' . $row )->getAlignment()->setWrapText( true );
            $sheet->getStyle( 'A' . $row )->getAlignment()->setVertical( \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP );
            $sheet->getStyle( 'A' . $row )->getBorders()->getLeft()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'A' . $row )->getBorders()->getTop()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'I' . $row )->getBorders()->getRight()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'I' . $row )->getBorders()->getTop()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':D' . $row );
            $sheet->mergeCells( 'F' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'www.mermaid-liveaboards.com' );
            $sheet->setCellValue( 'F' . $row, 'info@mermaid-liveaboards.com' );

            //-- SET style
            $sheet->getStyle( 'A' . $row )->getFont()->getColor()->setARGB( 'FF001B48' );
            $sheet->getStyle( 'F' . $row )->getFont()->getColor()->setARGB( 'FF001B48' );
            $sheet->getStyle( 'A' . $row )->getFont()->setSize( 10 )->setUnderline( true );
            $sheet->getStyle( 'F' . $row )->getFont()->setSize( 10 )->setUnderline( true );
            $sheet->getStyle( 'A' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT );
            $sheet->getStyle( 'F' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT );            
            $sheet->getStyle( 'A' . $row )->getBorders()->getLeft()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'I' . $row )->getBorders()->getRight()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $row++;

            $sheet->getStyle( 'A' . $row )->getBorders()->getLeft()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'I' . $row )->getBorders()->getRight()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );

            foreach( array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I' ) as $cell )
            {
                $sheet->getStyle( $cell . $row )->getBorders()->getBottom()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            }

            $sheet->setBreak( 'A' . $row, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::BREAK_ROW);
            $row++;
            $row++;

            $start = $row;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( "Mermaid Liveaboards\n" )->getFont()->setName( 'Arial' )->setSize( 12 )->setBold( true )->getColor()->setARGB( 'FFFFFFFF' );
            $btext->createTextRun( "General Booking, Cancellation, Payment and other policies - effective date October 1 2021" )->getFont()->setName( 'Arial' )->setSize( 11 )->setBold( true )->getColor()->setARGB( 'FFFFFFFF' );
            $sheet->setCellValue( 'A' . $row, $btext );

            //-- SET style
            $sheet->getRowDimension( $row )->setRowHeight( 30.75 );
            $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FF001B48' );
            $sheet->getStyle( 'A' . $row )->getAlignment()->setWrapText( true )->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER );
            $row++;

            //-- SET value
            $texts = array(
                'Acceptance of the above booking means that you/your company agree to the terms and conditions as outlined below in their entirety',
                'and to be read in conjunction with any published addendum applicable for the booking period :-' 
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
                $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
                $row++;
            }
            
            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Cruise Payments: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'Payments are preferred by telegraphic transfer in Euros, sender pays all fees, to the account on your invoice only');
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            //-- SET value
            $texts = array(
                'Acceptance of the above booking means that you/your company agree to the terms and conditions as outlined below in their entirety',
                'and to be read in conjunction with any published addendum applicable for the booking period.',
                'Short payments will be invoiced. Please send copy of your transfer advice by scan email as soon as you process payment.',                
                'Payments via credit card via Stripe orPaypal are also accepted and are fee free for direct bookings only.',
                'Agent payment via Stripe or PayPal will be subject to a 5% admin fee on top of the invoiced amount.',
                'Sorry we do not accept personal cheques or travellers cheques',
                'When you accept this booking , you accept that it represents you/your company not that of the individual guest and therefore you or',
                'your company remain responsible for any/all payments no later than on or by the due date. Failure to make payments by the due',
                'date may result in the guest\'s space being released and any monies paid being forfeited. Group/charter payments are required to be',
                'paid as invoiced and will not be accepted by individual participants of that cruise.'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'On Board Payments: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'On board sales are accepted in Euros, US dollars , Indonesian rupiah and by credit card (Visa and Mastercard).' );
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Cruise price includes: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'transfer to/from the vessel on departure/arrival days as per each cruise destination allowances, all meals and' );            
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            //-- SET value
            $texts = array(
                'snacks, fruits, coffee, tea, hot chocolate, soft drinks and drinking water, tanks, weights, weight belts and diving with experienced and certified',
                'Divemasters & Instructors'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Cruise price does not include: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'Any applicable fuel surcharge, transfers to/from anywhere outside the cruise free transfer zone as per our' );            
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'information packs, equipment rental, any on board courses, Nitrox and national park/port fees. Snorkellers will be charged the same rate as divers' );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'National Park/Port Fees: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'are payable on board only. National Park and Port fees are subject to increase and/or amendment by' );
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'government policy and are out of Mermaid Liveaboards\' control.' );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Dive Insurance: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'Dive insurance is mandatory for all guests in Indonesia by government regulation. If guests board without proof of insurance' );
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'they are required to purchase on board.' );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Travel and Cancellation Insurances: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'Guests are strongly advised to have valid travel and cancellation insurances.' );
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Guest Information: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'Hotel/airport transfer information is required a minimum of 1 week prior to the cruise.' );
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            //-- SET value
            $texts = array(
                'Guest Information forms must be submitted to Mermaid Liveaboards by email no later than 1 month prior to the cruise',
                'When checking in, all divers are required to complete a liability release waiver and to show proof of diving certification and dive',
                'insurance such as PADI, Dan or equivalent. Please have these available in your hand luggage.'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Payment and cancellations - the fine print :' );

            //-- SET style
            $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FF001B48' );
            $sheet->getStyle( 'A' . $row )->getFont()->getColor()->setARGB( 'FFFFFFFF' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'FIT Bookings :' );
            $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Deposit' );
            $sheet->setCellValue( 'D' . $row, '30% within 10 days of booking' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'D' . $row )->getFont()->setItalic( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Final Payment' );
            $sheet->setCellValue( 'D' . $row, '60 days prior to departure' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'D' . $row )->getFont()->setItalic( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Last Minute Bookings ie less than 60 days' );
            $sheet->setCellValue( 'D' . $row, '100% payment within 3 days of booking or prior to' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'D' . $row )->getFont()->setItalic( true );
            $row++;            

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );
            $sheet->setCellValue( 'D' . $row, 'boarding if booked less than 48 hours prior to departure' );
            $sheet->getStyle( 'D' . $row )->getFont()->setItalic( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Cancellations:' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'A' . $row )->getFont()->setUnderline( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'More than 90 days prior departure:' );
            $sheet->setCellValue( 'D' . $row, 'Refund of deposit less €100.00 per person administration charge' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'D' . $row )->getFont()->setItalic( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, '90 to 60 days prior departure:' );
            $sheet->setCellValue( 'D' . $row, 'Cruise non-transferable and deposit lost' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'D' . $row )->getFont()->setItalic( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':C' . $row );
            $sheet->mergeCells( 'D' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Less than 60 days prior departure:' );
            $sheet->setCellValue( 'D' . $row, 'Non-cancellable, non-refundable and non-transferable' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'D' . $row )->getFont()->setItalic( true );
            $row++;
            $row++;

            //-- SET value
            $texts = array(
                'Transfers are only accepted if requested more than 90 days prior to the cruise date. Transferred cruises are only transferable within',
                '18 months of the original cruise date and will be adjusted to the rates applicable for the new cruise date. Any discounts valid for the original',
                'booking do not transfer to the new date unless the same offer is valid for the same destination at time of transfer request.'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Important Note: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'Refunds will be made by bank transfer only - beneficiary to pay any/all bank fees' );
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Full Boat Charter or Group Bookings : ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'identified as 8 pax or more Mermaid I; 9 pax or more Mermaid II' );
            $sheet->setCellValue( 'A' . $row, $btext );

            $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $row++;

            //-- SET value
            $texts = array(
                '10% non-refundable and non-transferable deposit is required within 10 business days of booking. The next 25% of the total cost',
                'to be paid no less than 120 days before the cruise date. The balance 65% is payable a minimum of 90 days prior to the cruise date.',    
                'If any payments are not made on or by the due date the cruise will be released, any monies paid will be forfeited and the space',
                'made available for resale.'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Cancellations:' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $sheet->getStyle( 'A' . $row )->getFont()->setUnderline( true );
            $row++;

            //-- SET value
            $texts = array(                
                'More than 120 days before the departure date, all payments will be refunded, less the 10% initial deposit.',
                'Less than 120 days but more than 90 days before the departure date all payments will be refunded, less 35% of the total cost.',
                'If a cancellation is made for part of the charter/ group then the refund will be pro-rated and any free of charge concessions will be',
                'forfeited. Any discount will reduce to FIT rate if cancellation results in less than minimum numbers for a group booking.',
                'Less than 90 days before the departure date, no refunds will be made and date transfers are not accepted for any reason.'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            $sheet->getStyle( 'A' . $start . ':I' . $row )->getBorders()->getBottom()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'A' . $start . ':I' . $row )->getBorders()->getRight()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'A' . $start . ':I' . $row )->getBorders()->getLeft()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'A' . $start . ':I' . $row )->getBorders()->getTop()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );

            //-- SET break
            $sheet->setBreak( 'A' . $row, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::BREAK_ROW);
            $row++;

            $start = $row;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Other Services : Terms and Conditions' );

            //-- SET style
            $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FF001B48' );
            $sheet->getStyle( 'A' . $row )->getFont()->getColor()->setARGB( 'FFFFFFFF' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Hotels, Tours and Transfers:-' );
            $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $row++;

            //-- SET value
            $texts = array(   
                'Full payment is required a minimum of 60 days prior to your check in/tour day. Bookings made less than 60 days prior to the activity',
                'payment is due within 3 days of invoicing. Failure to comply with prepayment terms may result in the cancellation of any reservations',
                'made on your behalf. Hotels require complete rooming lists a minimum of 1 month prior for group bookings'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Airline Tickets:-' );
            $sheet->getStyle( 'A' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FFF3F3F3' );
            $sheet->getStyle( 'A' . $row )->getFont()->setBold( true );
            $row++;

            //-- SET value
            $texts = array(   
                'Tickets must be confirmed within 48 hours of request to ensure the same rate and are subject to availability at time of confirmation',
                'Tickets confirmed more than 48 hours after our quote are subject to fare and class change on rebooking with the airline',
                'Ticket rates include a nominal service fee of €30.00/per person/per ticket',
                'Airport taxes are not included and are required to be paid prior to boarding at the airports',
                'Tickets are reroutable during the ticket\'s validity dates subject to acceptance by the booked airline',
                'Airlines will apply an administration fee for any changes to date, class, route and the guest will be required to pay such charges as',
                'required by the airline plus €20.00/per person/per ticket service fee.'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Airline Tickets and other Services Cancellation Policy: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'Refunds will be paid as per each service\'s policy and for flights as per each airline\'s' );
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;

            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'policy as shown on the tickets less the initial any ticketing fees charged' );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $sheet->setCellValue( 'A' . $row, 'Important Notes:' );

            //-- SET style
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFill()->setFillType( \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID )->getStartColor()->setARGB( 'FF001B48' );
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->getColor()->setARGB( 'FFFFFFFF' );
            $sheet->getStyle( 'A' . $row . ':I' . $row )->getFont()->setBold( true );
            $row++;
            
            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Minimum Numbers: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'Mermaid Liveaboards  reserves the right to cancel any trip if there are less than the equivalent of 4 deluxe cabin guests.' );
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;
            
            //-- SET value
            $texts = array(
                'Mermaid Liveaboards will do their best to transfer the guest to a vessel the company deems to be of similar or better standard.',
                'Alternatively we may ask the guest to transfer to the date before/after the cruise. In the event none of these options are possible, we will offer',
                'a full refund (please allow maximum 2 weeks processing time)',
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }
            
            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Policies: ' )->getFont()->setName( 'Arial' )->setSize( 10 )->setBold( true );
            $btext->createText( 'Mermaid Liveaboards and PT Indonesia Liveaboards and its booking company CMC Marine Ltd reserve the rights' );
            $sheet->setCellValue( 'A' . $row, $btext );
            $row++;
            
            //-- SET value
            $texts = array(
                'to alter published rates without prior notice. Invoiced bookings will remain at the invoiced rates for that cruise date/destination.',
                'If the invoiced booking is transferred to another cruise/destination, the applicable rates for that cruise will apply.',
                'The company reserves the right to introduce any other fees as deemed necessary eg fuel surcharges.',
                'Mermaid Liveaboards/PT Indonesia Liveaboards and their booking company CMC Marine Ltd are  held accountable solely by the laws',
                'of Indonesia. Any contractual laws from countries other than Indonesia are outside of our concern.',
                'Any booking and/or on board cruise disputes will be discussed/reviewed only with the guest/s booking for each individual invoice.',
                'Mermaid Liveaboards does not accept any legal liability for loss or damage to any luggage/dive equipment while being transferred',
                'to or from the vessel nor while on board. Mermaid Liveaboards will offer any assistance possible to help with any insurance claims',
                'for lost or damaged property.'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            $row++;
            
            //-- SET value
            $texts = array(
                'Lastly, Mermaid Liveaboards, PT Indonesia Liveaboards and its booking agency CMC Marine Limited , do not offer refunds in the',
                'event of personal injury, airplane delay, breakdowns, weather, sickness, strikes, war, criminal acts, pandemics, quarantine, acts of god,',
                'or vessel relocation due to guest evacuation for any reason or any other event whatsoever beyond the company\'s actual control.'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }
            
            //-- SET value
            $texts = array(
                'Regardless of which cruise you choose ... Be prepared for some of the world\'s best diving plus a maximum of comfort, service and',
                'safety on M/V Mermaid I & II - Welcome on board !'
            );

            foreach( $texts as $txt )
            {
                $sheet->mergeCells( 'A' . $row . ':I' . $row );
                $sheet->setCellValue( 'A' . $row, $txt );
                $row++;
            }

            $row++;

            //-- SET value
            $sheet->mergeCells( 'A' . $row . ':I' . $row );
            $btext = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
            $btext->createTextRun( 'Mermaid Liveaboards ' )->getFont()->setSize( 16 )->setBold( true )->setColor( new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE ) );
            $btext->createTextRun( 'Hotline + 62 81 337 778 077 Indonesia' )->getFont()->setSize( 16 )->setBold( true )->setColor( new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED ) );
            $sheet->setCellValue( 'A' . $row, $btext );
            $sheet->getStyle( 'A' . $row )->getAlignment()->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER );
            $row++;
            $row++;

            //-- SET value
            $sheet->mergeCells( 'B' . $row . ':D' . $row );
            $sheet->mergeCells( 'F' . $row . ':I' . $row );
            $sheet->setCellValue( 'B' . $row, 'Bali contact:' );
            $sheet->setCellValue( 'F' . $row, 'Registered Company address:' );
            $sheet->getStyle( 'B' . $row . ':F' . $row )->getFont()->setBold( true );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'B' . $row . ':D' . $row );
            $sheet->mergeCells( 'F' . $row . ':I' . $row );
            $sheet->setCellValue( 'B' . $row, 'Mermaid Liveaboards – Bali Guest Lounge' );
            $sheet->setCellValue( 'F' . $row, 'CMC Marine Ltd' );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'B' . $row . ':D' . $row );
            $sheet->mergeCells( 'F' . $row . ':I' . $row );
            $sheet->setCellValue( 'B' . $row, 'PT Indonesia Liveaboards' );
            $sheet->setCellValue( 'F' . $row, 'Room 3603, Level 36 Tower 1, Enterprise Square Five' );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'B' . $row . ':D' . $row );
            $sheet->mergeCells( 'F' . $row . ':I' . $row );
            $sheet->setCellValue( 'B' . $row, '75 Jl Danau Tamblingan Sanur Bali Indonesia' );
            $sheet->setCellValue( 'F' . $row, '38 Wang Chiu Road' );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'B' . $row . ':D' . $row );
            $sheet->mergeCells( 'F' . $row . ':I' . $row );
            $sheet->setCellValue( 'B' . $row, 'English: ph +62 0361 285054' );
            $sheet->setCellValue( 'F' . $row, 'Kowloon Bay, Kowloon, Hong Kong' );
            $row++;

            //-- SET value
            $sheet->mergeCells( 'B' . $row . ':D' . $row );
            $sheet->mergeCells( 'F' . $row . ':I' . $row );
            $sheet->setCellValue( 'B' . $row, 'Bahasa:  ph +62 811386940' );
            $sheet->setCellValue( 'F' . $row, 'Contact: info@cmc-marine.com' );
            $row++;
            $row++;

            //-- SET value
            $sheet->mergeCells( 'B' . $row . ':E' . $row );
            $sheet->mergeCells( 'F' . $row . ':I' . $row );
            $sheet->setCellValue( 'B' . $row, 'info@mermaid-liveaboards.com' );
            $sheet->setCellValue( 'F' . $row, 'www.mermaid-liveaboards.com' );
            $sheet->getStyle( 'B' . $row . ':F' . $row )->getFont()->setSize( 14 )->setBold( true )->setColor( new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE ) );
            $row++;

            $sheet->getStyle( 'A' . $start . ':I' . $row )->getBorders()->getBottom()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'A' . $start . ':I' . $row )->getBorders()->getRight()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'A' . $start . ':I' . $row )->getBorders()->getLeft()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );
            $sheet->getStyle( 'A' . $start . ':I' . $row )->getBorders()->getTop()->setBorderStyle( \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN );

            //-- SET break
            $sheet->setBreak( 'A' . $row, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::BREAK_ROW );
            $row++;

            $writer = new Xlsx( $spread );
            $writer->save('php://output');

            exit;
        }
    }

    wp_reset_postdata();
}