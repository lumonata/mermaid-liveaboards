<?php

require_once 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class batch_download_csv_file
{
    private $message;
    private $headers;
    private $harbour;
    private $cabins;

    public function __construct()
    {
        $this->message = '';
        $this->cabins  = array();
        $this->harbour = array(
            '100116' => 'Benoa Pier Bali',
            '100031' => 'Maumere Flores',
            '100034' => 'Sorong West Papua',
            '100040' => 'Ambon',
            '100039' => 'Lembeh Sulawesi'
        );

        $this->headers = array(
            'IMPORTAUTHENTICATIONCODE',
            'BOATID',
            'DEPARTUREDATE',
            'DEPARTURETIME',
            'DEPARTUREHARBOURID',
            'ARRIVALDATE',
            'ARRIVALTIME',
            'ARRIVALHARBOURID',
            'ROUTEID',
            'FLEETINTERNALCOMMENTS',
            'REMARKSFORAGENTS',
            'DEPARTURESTATUSCODE',
            'ONBOARDNIGHTS',
            'ONBOARDDAYS',
            'DIVEDAYS',
            'AVERAGENUMBEROFDIVES',
            'EXPERIENCEREQUIREDDIVES',
            'TRIPAVAILABILITYONREQUEST',
            'SPECIALOFFER',
            'CURRENCYCODE',
            'CABIN01ID',
            'CABIN01PAXAVAILABLE',
            'CABIN01LASTPLACEMALE',
            'CABIN01LASTPLACEFEMALE',
            'CABIN01PRICE',
            'CABIN01PRICEFULL',
            'CABIN02ID',
            'CABIN02PAXAVAILABLE',
            'CABIN02LASTPLACEMALE',
            'CABIN02LASTPLACEFEMALE',
            'CABIN02PRICE',
            'CABIN02PRICEFULL',
            'CABIN03ID',
            'CABIN03PAXAVAILABLE',
            'CABIN03LASTPLACEMALE',
            'CABIN03LASTPLACEFEMALE',
            'CABIN03PRICE',
            'CABIN03PRICEFULL',
            'CABIN04ID',
            'CABIN04PAXAVAILABLE',
            'CABIN04LASTPLACEMALE',
            'CABIN04LASTPLACEFEMALE',
            'CABIN04PRICE',
            'CABIN04PRICEFULL',
            'ADDITIONALCOST01NAME',
            'ADDITIONALCOST01INCLUDEDINPRICE',
            'ADDITIONALCOST01PRICE',
            'ADDITIONALCOST02NAME',
            'ADDITIONALCOST02INCLUDEDINPRICE',
            'ADDITIONALCOST02PRICE',
            'ADDITIONALCOST03NAME',
            'ADDITIONALCOST03INCLUDEDINPRICE',
            'ADDITIONALCOST03PRICE',
            'ADDITIONALCOST04NAME',
            'ADDITIONALCOST04INCLUDEDINPRICE',
            'ADDITIONALCOST04PRICE',
            'ADDITIONALCOST05NAME',
            'ADDITIONALCOST05INCLUDEDINPRICE',
            'ADDITIONALCOST05PRICE',
        );
    }

    public function render()
    {
        try
        {
            //-- GET post id
            if( isset( $_GET[ 'post_id' ] ) )
            {
                $post_id = $_GET[ 'post_id' ];
            }
            elseif( isset( $_GET[ 'id' ] ) )
            {
                $post_id = array( $_GET[ 'id' ] );
            }
            else
            {
                throw new Exception( 'Download Failed! The data you selected is not valid.' );
            }

            //-- LOAD spreadsheet class
            $spread = new Spreadsheet();
            $sheet  = $spread->getActiveSheet();

            //-- ADD headers to the sheet
            foreach ( $this->headers as $i => $header )
            {
                $column = $this->get_column_name( $i ) . '1';

                $sheet->setCellValue( $column, $header );
            }

            //-- GET cabins
            $this->get_cabins();

            //-- SET next row
            $row = 2;

            foreach( $post_id as $id )
            {
                $post = get_post( $id );

                if( empty( $post ) )
                {
                    throw new Exception( 'Download Failed! The data you selected is not valid.' );
                }
                else
                {
                    $boat = get_post_meta( $id, '_schedule_rates_boat', true );
                    $boid = get_post_meta( $boat, '_boat_id_', true );

                    if( empty( $boid ) )
                    {
                        throw new Exception( 'Download Failed! Boat in the data you selected has not been set' );
                    }
                    else
                    {
                        $sdata = get_posts( array(
                            'meta_key'    => '_schedule_data_departure_date',
                            'post_type'   => 'schedule_data',
                            'orderby'     => 'meta_value',
                            'post_status' => 'publish',
                            'order'       => 'ASC',
                            'post_parent' => $id,
                            'numberposts' => -1,
                        ) );

                        if( empty( $sdata ) )
                        {
                            throw new Exception( 'Download Failed! The data you selected is not have schedule and rate' );
                        }
                        else
                        {
                            foreach( $sdata as $d )
                            {
                                $depart_point  = get_post_meta( $d->ID, '_schedule_data_depart_point', true );
                                $arrival_point = get_post_meta( $d->ID, '_schedule_data_arrival_point', true );

                                $depart_harbour  = array_search( $depart_point, $this->harbour );
                                $arrival_harbour = array_search( $arrival_point, $this->harbour );

                                if( empty( $depart_harbour ) || empty( $arrival_point ) )
                                {
                                    throw new Exception( 'Download Failed! Departure/Arrival point in schedule is not have been set' );
                                }
                                else
                                {
                                    $itine_id = get_post_meta( $d->ID, '_schedule_data_iteneraries', true );
                                    $route_id = get_post_meta( $itine_id, '_iteneraries_route_id', true );

                                    if( $route_id != '' )
                                    {
                                        $cabins = $this->get_cabin_data( $d->ID, $boat );

                                        $master_price_full = $cabins[ 'master' ][ 'price' ];
                                        $master_discount   = $cabins[ 'master' ][ 'discount' ];

                                        if( $master_discount > 0 )
                                        {
                                            $master_price_disc = $master_price_full - ( $master_price_full * ( $master_discount / 100 ) );
                                        }
                                        else
                                        {
                                            $master_price_disc = $master_price_full;
                                        }

                                        $deluxe_price_full = $cabins[ 'deluxe' ][ 'price' ];
                                        $deluxe_discount   = $cabins[ 'deluxe' ][ 'discount' ];

                                        if( $deluxe_discount > 0 )
                                        {
                                            $deluxe_price_disc = $deluxe_price_full - ( $deluxe_price_full * ( $deluxe_discount / 100 ) );
                                        }
                                        else
                                        {
                                            $deluxe_price_disc = $deluxe_price_full;
                                        }

                                        $single_price_full = $cabins[ 'single' ][ 'price' ];
                                        $single_discount   = $cabins[ 'single' ][ 'discount' ];

                                        if( $single_discount > 0 )
                                        {
                                            $single_price_disc = $single_price_full - ( ( $single_price_full * $single_discount ) / 100 );
                                        }
                                        else
                                        {
                                            $single_price_disc = $single_price_full;
                                        }

                                        $lower_price_full = $cabins[ 'lower' ][ 'price' ];
                                        $lower_discount   = $cabins[ 'lower' ][ 'discount' ];

                                        if( $lower_discount > 0 )
                                        {
                                            $lower_price_disc = $lower_price_full - ( ( $lower_price_full * $lower_discount ) / 100 );
                                        }
                                        else
                                        {
                                            $lower_price_disc = $lower_price_full;
                                        }

                                        $depart_date  = get_post_meta( $d->ID, '_schedule_data_departure_date', true );
                                        $arrival_date = get_post_meta( $d->ID, '_schedule_data_arrival_date', true );
                                        $depart_time  = get_post_meta( $d->ID, '_schedule_data_depart_time', true );
                                        $arrival_time = get_post_meta( $d->ID, '_schedule_data_arrival_time', true );
                                        $no_dives     = get_post_meta( $d->ID, '_schedule_data_no_dives', true );

                                        $ddate = new DateTime( $depart_date );
                                        $adate = new DateTime( $arrival_date );

                                        $nights = $adate->diff( $ddate )->format( '%a' );
                                        $nights = $nights == 0 ? 1 : $nights;

                                        if( $nights == 1 )
                                        {
                                            $days = 1;
                                        }
                                        else
                                        {
                                            $days = $nights + 1;
                                        }

                                        if( $depart_time != '' )
                                        {
                                            $depart_time = substr( $depart_time, 0, 5 );
                                        }

                                        if( $arrival_time != '' )
                                        {
                                            $arrival_time = substr( $arrival_time, 0, 5 );
                                        }

                                        $sheet->setCellValue( 'A' . $row, '803fe3c1-8616-4d74-8842-8cf80ff26466' );
                                        $sheet->setCellValue( 'B' . $row, $boid );
                                        $sheet->setCellValue( 'C' . $row, $depart_date );
                                        $sheet->setCellValue( 'D' . $row, $depart_time );
                                        $sheet->setCellValue( 'E' . $row, $depart_harbour );
                                        $sheet->setCellValue( 'F' . $row, $arrival_date );
                                        $sheet->setCellValue( 'G' . $row, $arrival_time );
                                        $sheet->setCellValue( 'H' . $row, $arrival_harbour );
                                        $sheet->setCellValue( 'I' . $row, $route_id );
                                        $sheet->setCellValue( 'J' . $row, '' );
                                        $sheet->setCellValue( 'K' . $row, '' );
                                        $sheet->setCellValue( 'L' . $row, 'confirmed' );
                                        $sheet->setCellValue( 'M' . $row, $nights );
                                        $sheet->setCellValue( 'N' . $row, $days );
                                        $sheet->setCellValue( 'O' . $row, '' );
                                        $sheet->setCellValue( 'P' . $row, $no_dives );
                                        $sheet->setCellValue( 'Q' . $row, '' );
                                        $sheet->setCellValue( 'R' . $row, '' );
                                        $sheet->setCellValue( 'S' . $row, '' );
                                        $sheet->setCellValue( 'T' . $row, 'EUR' );

                                        $sheet->setCellValue( 'U' . $row, $cabins[ 'master' ][ 'id' ] );
                                        $sheet->setCellValue( 'V' . $row, $cabins[ 'master' ][ 'allotment' ] );
                                        $sheet->setCellValue( 'W' . $row, '' );
                                        $sheet->setCellValue( 'X' . $row, '' );
                                        $sheet->setCellValue( 'Y' . $row, $master_price_disc );
                                        $sheet->setCellValue( 'Z' . $row, $master_price_full );

                                        $sheet->setCellValue( 'AA' . $row, $cabins[ 'deluxe' ][ 'id' ] );
                                        $sheet->setCellValue( 'AB' . $row, $cabins[ 'deluxe' ][ 'allotment' ] );
                                        $sheet->setCellValue( 'AC' . $row, '' );
                                        $sheet->setCellValue( 'AD' . $row, '' );
                                        $sheet->setCellValue( 'AE' . $row, $deluxe_price_disc );
                                        $sheet->setCellValue( 'AF' . $row, $deluxe_price_full );

                                        $sheet->setCellValue( 'AG' . $row, $cabins[ 'single' ][ 'id' ] );
                                        $sheet->setCellValue( 'AH' . $row, $cabins[ 'single' ][ 'allotment' ] );
                                        $sheet->setCellValue( 'AI' . $row, '' );
                                        $sheet->setCellValue( 'AJ' . $row, '' );
                                        $sheet->setCellValue( 'AK' . $row, $single_price_disc );
                                        $sheet->setCellValue( 'AL' . $row, $single_price_full );

                                        $sheet->setCellValue( 'AM' . $row, $cabins[ 'lower' ][ 'id' ] );
                                        $sheet->setCellValue( 'AN' . $row, $cabins[ 'lower' ][ 'allotment' ] );
                                        $sheet->setCellValue( 'AO' . $row, '' );
                                        $sheet->setCellValue( 'AP' . $row, '' );
                                        $sheet->setCellValue( 'AQ' . $row, $lower_price_disc );
                                        $sheet->setCellValue( 'AR' . $row, $lower_price_full );

                                        $row++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $filename = sanitize_title( 'boats dsv file' ) . '.dsv';

            header( 'Content-Disposition: attachment;filename="schedule-and-rates-' . $filename . '"' );
            header( 'Cache-Control: max-age=0' );
            header( 'Content-Type: text/csv' );
            header( 'Pragma: public' );
            header( 'Expires: 0' );

            $writer = new Csv( $spread );

            $writer->setDelimiter( '|' );
            $writer->setEnclosure( '' );
            $writer->setLineEnding( "\r\n" );
            $writer->setSheetIndex( 0 );
            $writer->save( 'php://output' );

            wp_reset_postdata();

            exit;
        }
        catch( Exception $e )
        {
            $this->message = $e->getMessage();

            add_action( 'admin_notices', function() {
                wp_admin_notice( $this->message, array( 'type' => 'error', 'dismissible' => true ) );
            } );
        }
    }

    public function get_column_name( $index )
    {
        $letters = '';

        while( $index >= 0 )
        {
            $letters = chr( $index % 26 + 65 ) . $letters;
            $index   = (int)( $index / 26 ) - 1;
        }

        return $letters;
    }

    public function get_cabins()
    {
        $cabins = the_post_list( 'cabins' );

        foreach( $cabins as $cid => $cabin )
        {
            $boid = get_post_meta( $cid, '_cabins_boat', true );
            $type = get_post_meta( $cid, '_cabins_type', true );
            $cbid = get_post_meta( $cid, '_cabins_id', true );

            if( !empty( $boid ) && !empty( $type ) && !empty( $cbid ) )
            {
                $this->cabins[ $boid ][ $type ] = $cbid;
            }
        }
    }

    public function get_cabin_data( $id, $boat )
    {
        $data = array(
            'master' => array(
                'allotment' => '',
                'price'     => '',
                'discount'  => '',
                'id'        => ''
            ),
            'deluxe' => array(
                'allotment' => '',
                'price'     => '',
                'discount'  => '',
                'id'        => ''
            ),
            'single' => array(
                'allotment' => '',
                'price'     => '',
                'discount'  => '',
                'id'        => ''
            ),
            'lower' => array(
                'allotment' => '',
                'price'     => '',
                'discount'  => '',
                'id'        => ''
            ),
        );

        foreach( array( 'master', 'deluxe', 'single', 'lower' ) as $type )
        {
            if( isset( $this->cabins[ $boat ][ $type ] ) )
            {
                $data[ $type ] = array(
                    'allotment' => get_post_meta( $id, '_schedule_data_allotment_' . $type, true ),
                    'price'     => get_post_meta( $id, '_schedule_data_price_' . $type, true ),
                    'discount'  => get_post_meta( $id, '_schedule_data_price_' . $type . '_dsc', true ),
                    'id'        => $this->cabins[ $boat ][ $type ]
                );
            }
        }

        return $data;
    }
}
