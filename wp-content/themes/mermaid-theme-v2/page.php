<?php get_header(); ?>

    <?php if ( have_posts() ) : the_post(); ?>

        <?php
            include_once "layout/hero.php";
            $page_id        = get_the_ID();
            $title          = get_the_title();
            $title          = empty($title) ? '' : '<h1 class="heading-default">'.$title.'</h1>';
            $content        = get_the_content();
            // $content        = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $content);
            $content        = apply_filters('the_content', $content);
            // $content        = str_replace(']]>', ']]&gt;', $content);
            // $content        = empty($content) ? '' : $content;
        ?>

        <!-- SECTION WELCOME -->
        <?php if(!is_page('thank-you') && !is_page('thank-you-for-payment')): ?>
        <section class="welcome-about">
            <div class="container">
                <?php echo $title; ?>
            </div>
        </section>
        <?php endif; ?>

        <section class="wrap-static-content">
            <div class="container">
                <div class="container-content">
                    <?php echo $content; ?>
                </div>
            </div>
        </section>

        <?php if( is_page('thank-you-for-payment') ): 

            if( session_status() !== PHP_SESSION_ACTIVE )
            {
                session_start();

                if( !empty( $_SESSION['gecommerce'] ) )
                {
                    extract( $_SESSION['gecommerce'] );

                    ?>
                    <!-- Booking Confirmation / Thank You Page -->
                    <script>
                        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                        ga('create', 'UA-16600952-14', 'auto', {'allowLinker': true}); //Replace with your Google Analytics ID
                        ga('send', 'pageview', '/thank-you-for-payment/'); //send page view to Google Analytics on the last page path
                        ga('require', 'ecommerce');

                        ga('ecommerce:addTransaction', {
                            'id': '<?php echo $addtransparam['id']; ?>', // Invoice ID
                            'affiliation': '<?php echo $addtransparam['affiliation']; ?>', // Put the Property Name here
                            'revenue': '<?php echo $addtransparam['revenue']; ?>', // Input the Grand Total of booking in IDR - before tax
                            'shipping': '' , // Shipping cost - leave blank
                            'tax': '' 
                        }); // Add the total tax for the booking if any

                        ga('ecommerce:addItem', {
                            'id': '<?php echo $addtransitem['id'];  ?>', // Invoice ID
                            'sku': '<?php echo $addtransitem['sku']; ?>', // Trip Code
                            'name': '<?php echo $addtransitem['name']; ?>', // Itinerary - Trip Code - Cabin 
                            'category': '<?php echo $addtransitem['category']; ?>', // Tipe Mermaid
                            'price': '<?php echo $addtransitem['price']; ?>', // Price per night in EURO
                            'quantity': '<?php echo $addtransitem['quantity']; ?>'//number of room night
                        }); // Number of nights

                        ga('ecommerce:send');
                    </script>;
                    <?php

                    session_destroy();
                }
            }
        ?>

        <?php endif; ?>

    <?php endif; ?>

<?php get_footer(); ?>