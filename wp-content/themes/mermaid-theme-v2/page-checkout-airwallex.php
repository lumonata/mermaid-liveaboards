
<?php get_header(); ?>

<?php

if( isset( $_GET[ 'prm' ] ) && !empty( $_GET[ 'prm' ] ) )
{
    $prm = json_decode( base64_decode( $_GET[ 'prm' ] ), true );

    if( isset( $prm[ 'post_id' ] ) )
    {
        $post = get_post( $prm[ 'post_id' ] );

        if( $post->ID )
        {
            $invoice_number = get_post_meta( $post->ID, '_checkout_invoice_no', true);
            $guest_name     = get_post_meta( $post->ID, '_checkout_guest_name', true );

            if( $invoice_number == '' || $guest_name == '' )
            {
                $status = 'invalid';
            }
            else
            {
                $status_payment = get_post_meta( $post->ID, '_checkout_status_payment', true );
                $expired_date   = get_post_meta( $post->ID, '_checkout_expired_date', true );

                if( strtotime( $expired_date ) <= strtotime( date( 'Y-m-d' ) ) )
                {
                    $status = 'expired';
                }
                else
                {
                    if( strtolower( $status_payment ) == 'waiting payment' )
                    {
                        $status = 'valid';
                    }
                    else
                    {
                        $status = 'invalid';
                    }
                }
            }
        }
        else
        {
            $status = 'invalid';
        }
    }
    else
    {
        $status = 'invalid';
    }
}
else
{
    $status = 'invalid';
}

if( $status == 'valid' )
{
    ?>
    <section class="wrap-bank-trf-page">
        <div class="logo-mermaid">
            <a href="<?php echo site_url(); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/mermaid-logo.png" />
            </a>
        </div>
        <div class="container-page-bank-trf clearfix">            
            <div class="title-page">
                <h1 class="heading-default">Accept a Payment</h1>
                <p>Please complete form below to make a payment</p>
            </div>
            <div class="box-bank-trf-details">
                <?php

                try
                {
                    //-- Get parameter
                    $prm = json_decode( base64_decode( $_GET[ 'prm' ] ), true );

                    //-- Generate Token
                    $token  = airwallex_generate_token( $prm[ 'form_type' ] );

                    //-- Create Payment Intent
                    $intent = airwallex_create_payment_intent( $token, $prm );

                    //-- Get airwallex server
                    $server = airwallex_get_option( $prm[ 'form_type' ], 'server' );

                    if( $server == 'production' )
                    {
                        $server = 'prod';
                    }
                    else
                    {
                        $server = 'demo';
                    }

                    if( empty( $intent ) === false )
                    {
                        ?>
                        <div id="full-featured-card"></div>
                        <script src="https://checkout.airwallex.com/assets/elements.bundle.min.js"></script>
                        <script>
                            try
                            {
                                Airwallex.init({
                                    env: '<?php echo $server ?>',
                                    origin: window.location.origin
                                });

                                const element = Airwallex.createElement( 'fullFeaturedCard', {
                                    intent: {
                                        id: '<?php echo $intent[ 'id' ] ?>',
                                        client_secret: '<?php echo $intent[ 'client_secret' ] ?>',
                                    },
                                });

                                element.mount('full-featured-card');
                            }
                            catch( error )
                            {
                                console.error('There was an error', error);
                            }

                            window.addEventListener('onReady', (event) => {
                                console.log('Element is ready', event.detail);
                            });

                            window.addEventListener('onSuccess', (event) => {                            
                                window.location.replace( '<?php echo site_url( 'execute-airwallex-data/?prm=' . $_GET[ 'prm' ] . '&id=' ) ?>' + event.detail.intent.id );
                            });

                            window.addEventListener('onError', (event) => {
                                const { error } = event.detail;

                                console.error('There was an error', event.detail.error);
                            });
                        </script>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="text-center">
                            <p>There is something wrong on this payment at a moment, please try again later or chose another method by click link below</p>
                            <a class="button-default" style="display:inline-block; padding: 7px 20px; text-decoration: none; margin-top: 10px;" href="<?php echo get_permalink( $prm[ 'post_id' ] ) ?>">Choose Other Method?</a>
                        </div>
                        <?php
                    }
                }
                catch( Exception $e )
                {
                    ?>
                    <div class="text-center">
                        <p>There is something wrong on this payment at a moment, please try again later or chose another method by click link below</p>
                        <a class="button-default" style="display:inline-block; padding: 7px 20px; text-decoration: none; margin-top: 10px;" href="<?php echo get_permalink( $prm[ 'post_id' ] ) ?>">Choose Other Method?</a>
                    </div>
                    <?php
                }

                ?>
            </div>
        </div>
    </section>
    <?php
}
else
{
    ?>
    <section class="welcome-about welcome-mermaid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo">
                        <a href="<?php echo get_site_url(); ?>"><img data-src-static="<?php echo get_template_directory_uri()."/assets/images/mermaid-logo.png" ?>" /></a>
                    </div>
                    <?php

                    if( $status == 'expired' )
                    {
                        ?>
                        <h1 class="title-payment heading-default"><?= get_option( 'title_page_payment_link_expired' ) ?></h1>
                        <p><?= nl2br( get_option( 'description_page_payment_link_expired' ) ) ?></p>
                        <?php
                    }
                    else
                    {
                        ?>
                        <h1 class="title-payment heading-default"><?= get_option( 'title_page_payment_link_invalid' ) ?></h1>
                        <p><?= nl2br( get_option( 'description_page_payment_link_invalid' ) ) ?></p>
                        <?php
                    }

                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php
}

?>

<?php get_footer(); ?>