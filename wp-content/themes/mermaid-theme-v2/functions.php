<?php

define( 'THEME_DIR', get_template_directory() . '/' );
define( 'THEME_URL', get_template_directory_uri() );
define( 'THEME_URL_ASSETS', THEME_URL . '/assets' );
define( 'THEME_NAME', 'mermaid' );

require_once( THEME_DIR . 'includes/post-type.php' );
require_once( THEME_DIR . 'includes/cmb.php' );
require_once( THEME_DIR . 'includes/metabox.php' );
require_once( THEME_DIR . 'includes/mailchimp.php' );
require_once( THEME_DIR . 'includes/flash.php' );
require_once( THEME_DIR . 'functions_homepage.php' );
require_once( THEME_DIR . 'functions_page_detail.php' );
require_once( THEME_DIR . 'functions_ajax_page.php' );
require_once( THEME_DIR . 'functions_ajax_page_admin.php' );
require_once( THEME_DIR . 'functions_payment.php' );
require_once( THEME_DIR . 'functions_admin.php' );

$label_string_data = get_option( 'label_string' );
$version           = "?v=2.4.4";

if ( !is_single() ):
    $destination_data = get_data_post_type( 'destination' );
    $boat_data        = get_data_post_type( 'boat' );
    $iteneraries_data = get_iteneraries_data();
endif;

// GET DATA SEARCH
if ( isset( $_GET['s'] ) ):
    $boat_data_search = get_page_by_path( $_GET['s'], '', 'boat' );
    $iten_data_search = get_iteneraries_search_data( $_GET['dest'] );
    $dest_data_search = get_page_by_path( $_GET['dest'], '', 'destination' );
endif;

function get_iteneraries_search_data( $dest )
{
    $part = explode( ',', $dest );
    $post = array();

    if ( count( $part ) > 1 )
    {
        foreach ( $part as $ds )
        {
            $dta = get_post( $ds );

            $post[] = get_page_by_path( $dta->post_name, '', 'iteneraries' );
        }
    }
    else
    {
        $dta = get_post( $dest );

        $post[] = get_page_by_path( $dta->post_name, '', 'iteneraries' );
    }

    return $post;
}

function get_iteneraries_search_title()
{
    global $iten_data_search;

    if ( !empty( $iten_data_search ) )
    {
        $data = reset( $iten_data_search );

        if ( count( $iten_data_search ) > 1 )
        {
            return sprintf( '%s or Reverse', get_the_title( $data ) );
        }
        else
        {
            return get_the_title( $data );
        }
    }
}

function wpassist_remove_block_library_css()
{
    wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_enqueue_scripts', 'wpassist_remove_block_library_css' );

function get_iteneraries_data()
{
    $data = get_data_post_type( 'iteneraries' );
    $arry = array();

    while ( $data->have_posts() ):
        $dt = $data->the_post();

        $post_id    = get_the_ID();
        $post_title = get_the_title();
        $reserved   = get_post_meta( $post_id, '_iteneraries_reverse' );

        if ( $post_title !== 'In Port' )
        {
            if ( isset( $reserved[0] ) && empty( $reserved[0] ) === false )
            {
                $key = array_search( $reserved[0], array_column( $arry, 'post_id' ) );

                if ( isset( $arry[$key] ) && $reserved[0] == $arry[$key]['post_id'] )
                {
                    $arry[$key]['post_id']    = $arry[$key]['post_id'] . ',' . $post_id;
                    $arry[$key]['post_title'] = $arry[$key]['post_title'] . ' or Reverse';
                }
                else
                {
                    $arry[] = array(
                        'post_id'      => $post_id,
                        'post_reverse' => $reserved[0],
                        'post_title'   => get_the_title()
                    );
                }
            }
            else
            {
                $arry[] = array(
                    'post_id'      => $post_id,
                    'post_reverse' => '',
                    'post_title'   => get_the_title()
                );
            }
        }
    endwhile;

    wp_reset_postdata();

    return $arry;
}

/*
| -------------------------------------------------------------------------------------
| Setup Mermaid Source Assets
| -------------------------------------------------------------------------------------
*/
function mermaid_assets()
{
    global $version;
    // FONT
    // wp_enqueue_style( 'montserrat-font-css', 'https://fonts.googleapis.com/css?family=Cinzel|Work+Sans:400,500' );

    if ( is_singular( 'gallery' ) ):
        wp_enqueue_script( 'lazy', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js', array( 'jquery' ), null, true );
        wp_enqueue_script( 'isotope', 'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js', array( 'jquery' ), null, true );
    endif;

    // JQUERY
    // wp_enqueue_script( 'selectize-standalone', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js', array( 'jquery' ), null, true );
    // wp_enqueue_script( 'fancybox', THEME_URL_ASSETS . '/js/jquery.fancybox.min.js', array( 'jquery' ), null, true );
    // wp_enqueue_script( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array( 'jquery' ), null, true );
    // wp_enqueue_script( 'blazy', 'https://cdnjs.cloudflare.com/ajax/libs/blazy/1.8.2/blazy.min.js', array( 'jquery' ), null, true );
    // wp_enqueue_script( 'intltel', THEME_URL_ASSETS . '/js/intlTelInput.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'intltel', 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/14.0.7/js/intlTelInput.min.js', array( 'jquery' ), null, true );

    if ( is_singular( 'destination' ) ):
        wp_enqueue_style( 'scrollbar-css', THEME_URL_ASSETS . '/css/scrollbar/jquery.scrollbar.min.css', array(), null );
        wp_enqueue_script( 'scrollbar-js', THEME_URL_ASSETS . '/js/scrollbar/jquery.scrollbar.min.js', array( 'jquery' ), null, true );
        wp_enqueue_script( 'dive_spot', THEME_URL_ASSETS . '/js/dive_spot.min.js' . $version, array( 'jquery' ), null, true );
        wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDFlVTwZqnOJhc3LsnE9pKHdgajhbB9S2U&callback=initialize', array( 'jquery' ), null, true );
    endif;

    if ( is_singular( 'destination' ) || is_singular( 'iteneraries' ) || is_singular( 'boat' ) || is_page( 'mermaid-i' ) || is_page( 'mermaid-ii' ) ):
        // wp_enqueue_style( 'owl-css', THEME_URL_ASSETS . '/owlcarousel/assets/owl.carousel.min.css', array(), null );
        // wp_enqueue_style( 'owl-theme-css', THEME_URL_ASSETS . '/owlcarousel/assets/owl.theme.default.min.css', array(), null );
        // wp_enqueue_script( 'owl-js', THEME_URL_ASSETS . '/owlcarousel/owl.carousel.min.js', array( 'jquery' ), null, true );
        wp_enqueue_style( 'owl-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css', array(), null );
        wp_enqueue_style( 'owl-theme-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css', array(), null );
        wp_enqueue_script( 'owl-js', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', array( 'jquery' ), null, true );
    endif;

    if ( get_page_template_slug( get_the_ID() ) == 'page-enrollment-form.php' ):
        wp_enqueue_style( 'datepicker-css', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array(), null );
        wp_enqueue_script( 'datepicker-js', '//code.jquery.com/ui/1.12.1/jquery-ui.js', array( 'jquery' ), null, true );
    endif;

    if ( is_page( 'agent-login' ) ) :
        wp_enqueue_style( 'font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
        wp_enqueue_style( 'pnotify', THEME_URL_ASSETS . '/css/pnotify.custom.min.css' . '', array(), null );
        wp_enqueue_script( 'pnotify-js', THEME_URL_ASSETS . '/js/pnotify.custom.min.js' . $version, array( 'jquery' ), null, true );
    endif;

    wp_enqueue_script( 'script', THEME_URL_ASSETS . '/js/script.js' . $version, array( 'jquery' ), null, true );
    // wp_enqueue_script( 'script', THEME_URL_ASSETS . '/js/script.min.js'.$version, array( 'jquery' ), null, true );

    if ( is_home() || is_front_page() || is_post_type_archive( 'destination' ) ):
        wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDFlVTwZqnOJhc3LsnE9pKHdgajhbB9S2U&callback=initialize', array( 'jquery' ), null, true );
    endif;

    if ( is_page( 'news-events' ) || is_tag() ):
        wp_enqueue_script( 'infinity-scroll-js', 'https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js', array( 'jquery' ), null, true );
    endif;


    // CSS
    // wp_enqueue_style( 'fancybox', THEME_URL_ASSETS . '/css/jquery.fancybox.min.css', array(), null );
    // wp_enqueue_style( 'intltel', THEME_URL_ASSETS . '/css/intlTelInput.min.css', array(), null );
    // wp_enqueue_style( 'selectize', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css', array(), null );
    // wp_enqueue_style( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css', array(), null );
    // wp_enqueue_style( 'slick-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css', array(), null );

    // wp_enqueue_style( 'style', THEME_URL_ASSETS . '/css/style.css'.$version, array(), null );
    wp_enqueue_style( 'style', THEME_URL_ASSETS . '/css/style.min.css' . $version, array(), null );
    wp_enqueue_style( 'style', THEME_URL_ASSETS . '/css/custom.css' . '', array(), null );

    // FOR AJAX URL
    wp_localize_script( 'jquery', 'url_for_ajax', array( 'ajaxUrl' => admin_url() . 'admin-ajax.php' ) );
}
add_action( 'wp_enqueue_scripts', 'mermaid_assets' );

/*
| -------------------------------------------------------------------------------------
| Removes <link rel="prefetch" for WP assets not used in the theme
| -------------------------------------------------------------------------------------
*/
function remove_dns_prefetch( $hints, $relation_type )
{
    if ( is_home() || is_front_page() || is_post_type_archive( 'destination' ) )
    {
        if ( 'dns-prefetch' === $relation_type )
        {
            return array_diff( wp_dependencies_unique_hosts(), $hints );
        }
    }

    return $hints;
}
add_filter( 'wp_resource_hints', 'remove_dns_prefetch', 10, 2 );


/*
| -------------------------------------------------------------------------------------
| Mermaid Setup
| -------------------------------------------------------------------------------------
*/
if ( ! function_exists( 'mermaid_setup' ) ):
    add_action( 'after_setup_theme', 'mermaid_setup' );

    function mermaid_setup()
    {
        register_nav_menus( array( 'primary' => esc_html__( 'Header Menu', THEME_NAME ) ) );
        register_nav_menus( array( 'footer_right_menu' => esc_html__( 'Footer Right Menu', THEME_NAME ) ) );
        register_nav_menus( array( 'footer_left_menu' => esc_html__( 'Footer Left Menu', THEME_NAME ) ) );

        add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'title-tag' );

        add_post_type_support( 'page', 'excerpt' );

        add_editor_style();

        add_image_size( 'image-1920-815', 1920, 815, true );

        add_filter( 'widget_text', 'do_shortcode' );
        add_filter( 'body_class', 'set_body_class' );
    }
endif;


/*
| -------------------------------------------------------------------------------------
| Function untuk membuat body class
| -------------------------------------------------------------------------------------
*/
if ( ! function_exists( 'set_body_class' ) ):
    function set_body_class( $classes )
    {
        global $post;

        if ( is_front_page() ):
            $classes[] = 'homepage';
        elseif ( is_page( 'thank-you' ) || is_page( 'thank-you-for-payment' ) ):
            $classes[] = 'pages thankyou-page';
        else:
            $classes[] = 'pages ' . $post->post_name;
        endif;

        return $classes;
    }
endif;

/*
| -------------------------------------------------------------------------------------
| Setup Admin Menus
| -------------------------------------------------------------------------------------
*/
if ( !function_exists( 'setup_theme_admin_menus' ) ):
    add_action( 'admin_menu', 'setup_theme_admin_menus' );

    function setup_theme_admin_menus()
    {
        add_submenu_page(
            'options-general.php',
            'Additional Settings',
            'Additional',
            'manage_options',
            'additional-setting',
            'mermaid_additional_settings'
        );
    }
endif;


/*
| -------------------------------------------------------------------------------------
| Mermaid Additional Settings
| -------------------------------------------------------------------------------------
*/
if ( !function_exists( 'mermaid_additional_settings' ) ):
    function mermaid_additional_settings()
    {
        global $wpdb;

        if ( !current_user_can( 'manage_options' ) ):
            wp_die( 'You do not have sufficient permissions to access this page.' );
        endif;

        if ( isset( $_POST['update_settings'] ) ):

            // ================== Contact Information ==================
            $c_company_name        = esc_attr( $_POST['c_company_name'] );
            $c_address             = esc_attr( $_POST['c_address'] );
            $c_address_maps_url    = esc_attr( $_POST['c_address_maps_url'] );
            $c_phone               = esc_attr( $_POST['c_phone'] );
            $c_fax                 = esc_attr( $_POST['c_fax'] );
            $c_hotline_english     = esc_attr( $_POST['c_hotline_english'] );
            $c_hotline_indonesia   = esc_attr( $_POST['c_hotline_indonesia'] );
            $c_email               = esc_attr( $_POST['c_email'] );
            $email_payment_success = esc_attr( $_POST['email_payment_success'] );
            $c_skype               = esc_attr( $_POST['c_skype'] );
            $contact_information   = array(
                'c_company_name'        => $c_company_name,
                'c_address'             => $c_address,
                'c_address_maps_url'    => $c_address_maps_url,
                'c_phone'               => $c_phone,
                'c_fax'                 => $c_fax,
                'c_hotline_english'     => $c_hotline_english,
                'c_hotline_indonesia'   => $c_hotline_indonesia,
                'c_email'               => $c_email,
                'email_payment_success' => $email_payment_success,
                'c_skype'               => $c_skype,
            );
            $contact_information = json_encode( $contact_information );
            update_option( 'contact_information', $contact_information );


            // ================== Social media ==================
            $facebook_link      = esc_attr( $_POST['facebook_link'] );
            $instagram_link     = esc_attr( $_POST['instagram_link'] );
            $twitter_link       = esc_attr( $_POST['twitter_link'] );
            $youtube_link       = esc_attr( $_POST['youtube_link'] );
            $embed_youtube_link = esc_attr( $_POST['embed_youtube_link'] );
            $social_media       = array(
                'facebook_link'      => $facebook_link,
                'instagram_link'     => $instagram_link,
                'twitter_link'       => $twitter_link,
                'youtube_link'       => $youtube_link,
                'embed_youtube_link' => $embed_youtube_link
            );
            $social_media = json_encode( $social_media );
            update_option( 'social_media', $social_media );


            // ================== Mailchimp Setting ==================
            $mailchimp_api_key             = esc_attr( $_POST['mailchimp_api_key'] );
            $mailchimp_list_id             = esc_attr( $_POST['mailchimp_list_id'] );
            $mailchimp_success_title       = esc_attr( $_POST['mailchimp_success_title'] );
            $mailchimp_success_description = esc_attr( $_POST['mailchimp_success_description'] );
            $mailchimp_setting             = array(
                'mailchimp_api_key'             => $mailchimp_api_key,
                'mailchimp_list_id'             => $mailchimp_list_id,
                'mailchimp_success_title'       => $mailchimp_success_title,
                'mailchimp_success_description' => $mailchimp_success_description
            );
            $mailchimp_setting = json_encode( $mailchimp_setting );
            update_option( 'mailchimp_setting', $mailchimp_setting );


            // ================== Google Tag Manager ==================
            // $google_tag_manager_head = $wpdb->update('mermaid_options', array('option_value' => $_POST['google_tag_manager_head']), array('option_name' => 'google_tag_manager_head'));
            // update_option( 'google_tag_manager_head', json_encode($_POST['google_tag_manager_head']) );
            // update_option( 'google_tag_manager_body', json_encode($_POST['google_tag_manager_body']) );

            // ================== Currency Box ==================
            update_option( 'info_box_currency', esc_attr( $_POST['info_box_currency'] ) );


            // ================== Schedule and Rates ==================
            $title_rates_inport           = esc_attr( $_POST['title_rates_inport'] );
            $des_rates_inport             = esc_attr( $_POST['des_rates_inport'] );
            $des_next_schedule            = esc_attr( $_POST['des_next_schedule'] );
            $title_rates_cabin_first      = esc_attr( $_POST['title_rates_cabin_first'] );
            $des_rates_cabin_first        = esc_attr( $_POST['des_rates_cabin_first'] );
            $schedule_ratest_inport_alert = array(
                'title'                   => $title_rates_inport,
                'description'             => $des_rates_inport,
                'des_next_schedule'       => $des_next_schedule,
                'title_rates_cabin_first' => $title_rates_cabin_first,
                'des_rates_cabin_first'   => $des_rates_cabin_first,
            );
            $schedule_ratest_inport_alert = json_encode( $schedule_ratest_inport_alert );
            update_option( 'schedule_ratest_inport_alert', $schedule_ratest_inport_alert );


            // ================== Check Out Page Setting ==================
            update_option( 'title_page_payment_link_expired', esc_attr( $_POST['title_page_payment_link_expired'] ) );
            update_option( 'description_page_payment_link_expired', esc_attr( $_POST['description_page_payment_link_expired'] ) );
            update_option( 'title_page_payment_link_invalid', esc_attr( $_POST['title_page_payment_link_invalid'] ) );
            update_option( 'description_page_payment_link_invalid', esc_attr( $_POST['description_page_payment_link_invalid'] ) );
            update_option( 'title_page_payment_sent', esc_attr( $_POST['title_page_payment_sent'] ) );
            update_option( 'description_page_payment_sent', esc_attr( $_POST['description_page_payment_sent'] ) );


            // ================== Payment Successfully Email Message ==================
            update_option( 'message_paypal_success', esc_attr( $_POST['message_paypal_success'] ) );
            update_option( 'message_stripe_success', esc_attr( $_POST['message_stripe_success'] ) );
            update_option( 'message_airwallex_success', esc_attr( $_POST['message_airwallex_success'] ) );


            // ================== Message Successfully Popup ==================
            update_option( 'message_contact_form_popup', esc_attr( $_POST['message_contact_form_popup'] ) );
            update_option( 'message_enrollment_form_popup', esc_attr( $_POST['message_enrollment_form_popup'] ) );

            // ================== Message Successfully Popup ==================
            update_option( 'additional_charge_status', esc_attr( $_POST['additional_charge_status'] ) );
            update_option( 'additional_charge_value', esc_attr( $_POST['additional_charge_value'] ) );

        endif;

        ?>
        <div class="wrap">
            <h2>Additional Setting</h2>

            <form method="POST" action="">
                <div style="padding-bottom:20px; margin-bottom:20px; border-bottom:1px dashed #BBBBBB;">
                    <h3>Contact Information</h3>
                    <p>
                        Please provide your contact information to ensure visitors can contact or visiting you</em>
                    </p>

                    <?php
                            $contact_information = get_option( 'contact_information' );
        $c_company_name                          = "";
        $c_address                               = "";
        $c_address_maps_url                      = "";
        $c_phone                                 = "";
        $c_fax                                   = "";
        $c_hotline_english                       = "";
        $c_hotline_indonesia                     = "";
        $c_email                                 = "";
        $email_payment_success                   = "";
        $c_skype                                 = "";

        if ( !empty( $contact_information ) ):
            $contact_information   = json_decode( $contact_information );
            $c_company_name        = $contact_information->c_company_name;
            $c_address             = $contact_information->c_address;
            $c_address_maps_url    = $contact_information->c_address_maps_url;
            $c_phone               = $contact_information->c_phone;
            $c_fax                 = $contact_information->c_fax;
            $c_hotline_english     = $contact_information->c_hotline_english;
            $c_hotline_indonesia   = $contact_information->c_hotline_indonesia;
            $c_email               = $contact_information->c_email;
            $email_payment_success = $contact_information->email_payment_success;
            $c_skype               = $contact_information->c_skype;
        endif;
        ?>

                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Company Name </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="c_company_name" value="<?php echo $c_company_name; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Address </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" name="c_address"><?php echo $c_address; ?></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Address Maps URL </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" name="c_address_maps_url"><?php echo $c_address_maps_url; ?></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Phone </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="c_phone" value="<?php echo $c_phone; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Fax </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="c_fax" value="<?php echo $c_fax; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Hotline (English) </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="c_hotline_english" value="<?php echo $c_hotline_english; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Bahasa Indonesia Hotline </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="c_hotline_indonesia" value="<?php echo $c_hotline_indonesia; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Email </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="c_email" value="<?php echo $c_email; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Email For Receive Payment Successfully</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="email_payment_success" value="<?php echo $email_payment_success; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Skype </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="c_skype" value="<?php echo $c_skype; ?>" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="hidden" name="update_settings" value="Y" />
                        <input type="submit" value="Save settings" class="button-primary" />
                    </p>
                </div>

                <div style="padding-bottom:20px; margin-bottom:20px; border-bottom:1px dashed #BBBBBB;">
                    <h3>Social Media & External Link</h3>
                    <p>
                        Add your social media or external link to make visitor easier connect with you.
                        You must include the http link as well, example <em>http://your-social-media-link.com</em>
                    </p>

                    <?php
        $social_media       = get_option( 'social_media' );
        $facebook_link      = "";
        $instagram_link     = "";
        $twitter_link       = "";
        $youtube_link       = "";
        $embed_youtube_link = "";

        if ( !empty( $social_media ) ):
            $social_media       = json_decode( $social_media );
            $facebook_link      = $social_media->facebook_link;
            $instagram_link     = $social_media->instagram_link;
            $twitter_link       = $social_media->twitter_link;
            $youtube_link       = $social_media->youtube_link;
            $embed_youtube_link = $social_media->embed_youtube_link;
        endif;
        ?>

                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Facebook </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="facebook_link" value="<?php echo $facebook_link; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Instagram </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="instagram_link" value="<?php echo $instagram_link; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Twitter </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="twitter_link" value="<?php echo $twitter_link; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Youtube </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="youtube_link" value="<?php echo $youtube_link; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Embed Video Link</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="embed_youtube_link" value="<?php echo $embed_youtube_link; ?>" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="hidden" name="update_settings" value="Y" />
                        <input type="submit" value="Save settings" class="button-primary" />
                    </p>
                </div>

                <div style="padding-bottom:20px; margin-bottom:20px; border-bottom:1px dashed #BBBBBB;">
                    <h3>Mailchimp Setting</h3>

                    <?php
        $mailchimp_setting             = get_option( 'mailchimp_setting' );
        $mailchimp_api_key             = "";
        $mailchimp_list_id             = "";
        $mailchimp_success_title       = "";
        $mailchimp_success_description = "";

        if ( !empty( $mailchimp_setting ) ):
            $mailchimp_setting             = json_decode( $mailchimp_setting );
            $mailchimp_api_key             = $mailchimp_setting->mailchimp_api_key;
            $mailchimp_list_id             = $mailchimp_setting->mailchimp_list_id;
            $mailchimp_success_title       = $mailchimp_setting->mailchimp_success_title;
            $mailchimp_success_description = $mailchimp_setting->mailchimp_success_description;
        endif;
        ?>

                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Mailchimp API Key</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="mailchimp_api_key" value="<?php echo $mailchimp_api_key; ?>" />
                                </div>
                            </td>
                        </tr>

                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Mailchimp List ID</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="mailchimp_list_id" value="<?php echo $mailchimp_list_id; ?>" />
                                </div>
                            </td>
                        </tr>

                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Mailchimp Success Title</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="mailchimp_success_title" value="<?php echo $mailchimp_success_title; ?>" />
                                </div>
                            </td>
                        </tr>

                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Mailchimp Success Description</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="mailchimp_success_description"><?php echo $mailchimp_success_description; ?></textarea>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="hidden" name="update_settings" value="Y" />
                        <input type="submit" value="Save settings" class="button-primary" />
                    </p>
                </div>

                <div style="padding-bottom:20px; margin-bottom:20px; border-bottom:1px dashed #BBBBBB;">
                    <h3>Currency Box Setting</h3>

                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Info Box Currency</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="info_box_currency" value="<?php echo get_option( 'info_box_currency' ); ?>" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="hidden" name="update_settings" value="Y" />
                        <input type="submit" value="Save settings" class="button-primary" />
                    </p>
                </div>

                <div style="padding-bottom:20px; margin-bottom:20px; border-bottom:1px dashed #BBBBBB;">
                    <h3>Schedule & Rates Setting</h3>

                    <?php
        $schedule_ratest_alert   = get_option( 'schedule_ratest_inport_alert' );
        $title_rates             = "";
        $des_rates               = "";
        $title_rates_cabin_first = "";
        $des_rates_cabin_first   = "";

        if ( !empty( $schedule_ratest_alert ) ):
            $schedule_ratest_alert = json_decode( $schedule_ratest_alert );
            $title_rates           = $schedule_ratest_alert->title;
            $des_rates             = $schedule_ratest_alert->description;
            $des_next_schedule     = $schedule_ratest_alert->des_next_schedule;

            $title_rates_cabin_first = $schedule_ratest_alert->title_rates_cabin_first;
            $des_rates_cabin_first   = $schedule_ratest_alert->des_rates_cabin_first;
        endif;
        ?>

                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Title Alert Schedule & Rates Select Cabin First</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="title_rates_cabin_first" value="<?php echo $title_rates_cabin_first; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Description Alert Schedule & Rates Select Cabin First</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="des_rates_cabin_first"><?php echo $des_rates_cabin_first; ?></textarea>
                                </div>
                            </td>
                        </tr>

                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Title Alert Schedule & Rates In-Port</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="title_rates_inport" value="<?php echo $title_rates; ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Description Alert Schedule & Rates In-Port</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="des_rates_inport"><?php echo $des_rates; ?></textarea>
                                </div>
                            </td>
                        </tr>

                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Text Next Schedule</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="des_next_schedule"><?php echo $des_next_schedule; ?></textarea>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="hidden" name="update_settings" value="Y" />
                        <input type="submit" value="Save settings" class="button-primary" />
                    </p>
                </div>

                <div style="padding-bottom:20px; margin-bottom:20px; border-bottom:1px dashed #BBBBBB;">
                    <h3>Checkout Page Setting</h3>

                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Title Page Payment Link Expired </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="title_page_payment_link_expired" value="<?php echo get_option( 'title_page_payment_link_expired' ); ?>" />
                                </div>
                            </td>
                        </tr>

                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Description Page Payment Link Expired </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="description_page_payment_link_expired"><?php echo get_option( 'description_page_payment_link_expired' ); ?></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Title Page Payment Link Invalid </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="title_page_payment_link_invalid" value="<?php echo get_option( 'title_page_payment_link_invalid' ); ?>" />
                                </div>
                            </td>
                        </tr>

                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Description Page Payment Link Invalid </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="description_page_payment_link_invalid"><?php echo get_option( 'description_page_payment_link_invalid' ); ?></textarea>
                                </div>
                            </td>
                        </tr>

                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Title Page Payment Sent </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input class="regular-text" type="text" name="title_page_payment_sent" value="<?php echo get_option( 'title_page_payment_sent' ); ?>" />
                                </div>
                            </td>
                        </tr>

                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Description Page Payment Sent </label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="description_page_payment_sent"><?php echo get_option( 'description_page_payment_sent' ); ?></textarea>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="hidden" name="update_settings" value="Y" />
                        <input type="submit" value="Save settings" class="button-primary" />
                    </p>
                </div>

                <div style="padding-bottom:20px; margin-bottom:20px; border-bottom:1px dashed #BBBBBB;">
                    <h3>Payment Message Setting Email</h3>

                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Message Paypal Eamil</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="message_paypal_success"><?php echo get_option( 'message_paypal_success' ); ?></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Message Stripe Email</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="message_stripe_success"><?php echo get_option( 'message_stripe_success' ); ?></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Message Airwallex Email</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="message_airwallex_success"><?php echo get_option( 'message_airwallex_success' ); ?></textarea>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="hidden" name="update_settings" value="Y" />
                        <input type="submit" value="Save settings" class="button-primary" />
                    </p>
                </div>

                <div style="padding-bottom:20px; margin-bottom:20px; border-bottom:1px dashed #BBBBBB;">
                    <h3>Form Successfully Message</h3>

                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Message Contact Form Successfully Popup</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="message_contact_form_popup"><?php echo get_option( 'message_contact_form_popup' ); ?></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Message Enrollment Form Successfully Popup</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <textarea class="regular-text" rows="6" name="message_enrollment_form_popup"><?php echo get_option( 'message_enrollment_form_popup' ); ?></textarea>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="hidden" name="update_settings" value="Y" />
                        <input type="submit" value="Save settings" class="button-primary" />
                    </p>
                </div>

                <div style="padding-bottom:20px; margin-bottom:20px; border-bottom:1px dashed #BBBBBB; display: none;">
                    <h3>Additional Charge</h3>
                    <?php

        $astatus = get_option( 'additional_charge_status' );
        $avalue  = get_option( 'additional_charge_value' );

        ?>
                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Activate additonal charge?</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <select name="additional_charge_status">
                                        <option value="0" <?php echo $astatus == 0 ? 'selected' : ''; ?>>No</option>
                                        <option value="1" <?php echo $astatus == 1 ? 'selected' : ''; ?>>Yes</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="padding-top:0;">
                                <label>Charge value (%)</label>
                            </th>
                            <td style="padding-top:0;">
                                <div class="form-field">
                                    <input style="width:100px;" class="regular-text" type="text" name="additional_charge_value" value="<?php echo $avalue == '' ? 0 : $avalue; ?>" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="hidden" name="update_settings" value="Y" />
                        <input type="submit" value="Save settings" class="button-primary" />
                    </p>
                </div>
            </form>
        </div>
    <?php
    }
endif;


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan custom post type sesuai author yang login
| -------------------------------------------------------------------------------------
*/
function posts_for_current_author( $query )
{
    global $user_level;

    $query_vars = $query->query_vars;

    if( isset( $query_vars['post_type'] ) )
    {
        $post_type = $query_vars['post_type'];

        if ( $post_type == "inquiry_form" )
        {
            if ( $query->is_admin && $user_level < 5 )
            {
                global $user_ID;

                $query->set( 'author', $user_ID );

                unset( $user_ID );
            }

            unset( $user_level );
        }
    }

    return $query;
}

add_filter( 'pre_get_posts', 'posts_for_current_author' );


/*
| -------------------------------------------------------------------------------------
| Function untuk membuat tombol add new hidden
| -------------------------------------------------------------------------------------
*/
function disable_new_posts()
{
    global $submenu;
    global $user_level;
    unset( $submenu['edit.php?post_type=trip_report'][10] );

    if ( $user_level < 5 ):
        if ( isset( $_GET['post_type'] ) && ( $_GET['post_type'] == 'trip_report' || $_GET['post_type'] == 'destination' || $_GET['post_type'] == 'schedule_rates' ) ):
            echo '<style type="text/css">
            .page-title-action { display:none; }
            </style>';
        endif;
    endif;
}
add_action( 'admin_menu', 'disable_new_posts' );


/*
| -------------------------------------------------------------------------------------
| Function untuk membuat roles tidak dapat di akses oleh beberapa role
| -------------------------------------------------------------------------------------
*/
function restrict_menus()
{
    $author = wp_get_current_user();

    if ( isset( $author->roles[0] ) ):
        $current_role = $author->roles[0];
    else:
        $current_role = 'no_role';
    endif;

    $screen = get_current_screen();
    $base   = $screen->id;

    if ( 'agent' == $current_role ) :
        $not_allowed_access = array(
            'edit-post',
            'post',
            'edit-review',
            'review',
            'edit-facility',
            'facility',
            'upload',
            'tools',
            'edit-comments'
        );

        if ( in_array( $base, $not_allowed_access ) ):
            wp_die( 'Sorry, You Are Not Allowed to Access This Page' );
        endif;
    endif;
}
add_action( 'current_screen', 'restrict_menus' );


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan contact dan address office
| -------------------------------------------------------------------------------------
*/
if ( ! function_exists( 'mermaid_footer_address_contact' ) )
{
    function mermaid_footer_address_contact()
    {
        // GET OPTION DATA
        $contact_information = get_option( 'contact_information' );
        $c_company_name      = "";
        $c_address           = "";
        $c_address_maps_url  = "";
        $c_phone             = "";
        $c_fax               = "";
        $c_hotline_english   = "";
        $c_hotline_indonesia = "";
        $c_email             = "";
        $c_skype             = "";

        if ( !empty( $contact_information ) ):
            $contact_information = json_decode( $contact_information );
            $c_company_name      = $contact_information->c_company_name;
            $c_address           = $contact_information->c_address;
            $c_address_maps_url  = $contact_information->c_address_maps_url;
            $c_phone             = $contact_information->c_phone;
            $c_fax               = $contact_information->c_fax;
            $c_hotline_english   = $contact_information->c_hotline_english;
            $c_hotline_indonesia = $contact_information->c_hotline_indonesia;
            $c_email             = $contact_information->c_email;
            $c_skype             = $contact_information->c_skype;
        endif;

        //-- GET HTML DATA
        $c_company_name      = empty( $c_company_name ) ? '' : '<h4>' . $c_company_name . '</h4>';
        $c_address           = empty( $c_address ) ? '' : '<h4><a href="' . $c_address_maps_url . '" target="_blank">' . $c_address . '</a></h4>';
        $c_phone             = empty( $c_phone ) ? '' : '<li class="icon-call"><a href="tel:' . $c_phone . '">' . $c_phone . '</a></li>';
        $c_fax               = empty( $c_fax ) ? '' : '<li class="icon-fax"><a href="tel:' . $c_fax . '">' . $c_fax . '</a></li>';
        $c_hotline_english   = empty( $c_hotline_english ) ? '' : '<li class="icon-call"><a href="tel:' . $c_hotline_english . '">' . $c_hotline_english . ' (English)</a></li>';
        $c_hotline_indonesia = empty( $c_hotline_indonesia ) ? '' : '<li class="icon-call"><a href="tel:' . $c_hotline_indonesia . '">' . $c_hotline_indonesia . ' (Bahasa Indonesia)</a></li>';
        $c_email             = empty( $c_email ) ? '' : '<li class="icon-email"><a href="mailto:' . $c_email . '">' . $c_email . '</a></li>';
        $c_skype             = empty( $c_skype ) ? '' : '<li class="icon-skype"><a href="skype:' . $c_skype . '?chat">' . $c_skype . '</li></a>';

        $html = '
        <div class="address-office">
            ' . $c_company_name . '
            ' . $c_address . '
        </div>
        <div class="contact-office">
            <ul>
                ' . $c_phone . '
                ' . $c_fax . '
                ' . $c_hotline_english . '
                ' . $c_hotline_indonesia . '
                ' . $c_email . '
                ' . $c_skype . '
            </ul>
        </div>';

        echo $html;
    }
}


/*
| -------------------------------------------------------------------------------------
| Function untuk mendapatkan lis post type
| -------------------------------------------------------------------------------------
*/
function the_post_list( $type, $post_id_ignore = '' )
{
    $posts = get_posts( array(
        'exclude'     => array( $post_id_ignore ),
        'post_type'   => array( $type ),
        'orderby'     => 'menu_order',
        'post_status' => 'publish',
        'order'       => 'ASC',
        'numberposts' => -1,
    ) );

    $posts_list = array();

    if ( !empty( $posts ) )
    {
        foreach ( $posts as $post )
        {
            setup_postdata( $post );

            if ( $post->post_title != '' )
            {
                $posts_list[$post->ID] = $post->post_title;
            }
        }

        wp_reset_postdata();
    }

    return $posts_list;
}

function the_schedule_data_list( $post_id_ignore = '' )
{
    $posts = get_posts( array(
        'post_type'   => array( 'schedule_rates' ),
        'orderby'     => 'menu_order',
        'post_status' => 'publish',
        'order'       => 'ASC',
        'numberposts' => -1,
    ) );

    $posts_list = array();

    if ( !empty( $posts ) )
    {
        $meta = array(
            '_schedule_rates_boat',
            '_schedule_rates_year',
            '_schedule_data_iteneraries',
            '_schedule_data_price_lower',
            '_schedule_data_price_master',
            '_schedule_data_price_single',
            '_schedule_data_price_deluxe',
            '_schedule_data_arrival_date',
            '_schedule_data_departure_date',
            '_schedule_data_arrival_point',
            '_schedule_data_depart_point',
            '_schedule_data_arrival_time',
            '_schedule_data_depart_time',
        );

        foreach ( $posts as $post )
        {
            setup_postdata( $post );

            $stack = array();

            foreach ( get_post_meta( $post->ID ) as $key => $mt )
            {
                if ( in_array( $key, $meta ) )
                {
                    $stack[$key] = $mt[0];
                }
            }

            $sdata = get_posts( array(
                'meta_key'    => '_schedule_data_departure_date',
                'post_type'   => 'schedule_data',
                'orderby'     => 'meta_value',
                'post_status' => 'publish',
                'order'       => 'ASC',
                'numberposts' => -1,
                'post_parent' => $post->ID,
            ) );

            if ( !empty( $sdata ) )
            {
                foreach ( $sdata as $dt )
                {
                    setup_postdata( $dt );

                    if ( $dt->post_title != '' )
                    {
                        $cstack = array();

                        foreach ( get_post_meta( $dt->ID ) as $key => $mt )
                        {
                            if ( in_array( $key, $meta ) )
                            {
                                $cstack[$key] = $mt[0];
                            }
                        }

                        $dt->post_meta = array_merge( $cstack, $stack );

                        $posts_list[base64_encode( json_encode( $dt ) )] = $dt->post_title;
                    }
                }
            }
        }

        wp_reset_postdata();
    }

    return $posts_list;
}

function the_year_list()
{
    $range = range( '2020', date( 'Y', strtotime( '+5 years' ) ) );
    $years = array_combine( $range, $range );

    return $years;
}

function the_bank_transfer_list( $bank_id = '', $bank_category = '' )
{
    $banks = bank_get_option( 'bank_list' );

    if ( empty( $banks ) === false )
    {
        $bank_list = array();

        foreach ( $banks as $idx => $bank )
        {
            if ( $bank['bank_category'] == $bank_category )
            {
                $bank_list[$idx] = esc_html( $bank['bank_name'] ) . ' ( ' . esc_html( $bank['account_number'] ) . ' )';
            }
        }

        if ( !empty( $bank_id ) && isset( $bank_list[$bank_id] ) )
        {
            return $bank_list[$bank_id];
        }
        else
        {
            return $bank_list;
        }
    }
}

function the_agent_list( $agent_id = '' )
{
    $agents = get_users( array(
        'role'    => 'agent',
        'orderby' => 'user_nicename',
        'order'   => 'ASC'
    ) );

    if ( !empty( $agents ) )
    {
        $agent_list = array();

        foreach ( $agents as $agent )
        {
            $meta = get_user_meta( $agent->ID );

            if ( isset( $meta['_user_address'][0] ) )
            {
                $agent->user_address = $meta['_user_address'][0];
            }
            else
            {
                $agent->user_address = '';
            }

            $agent_list[base64_encode( json_encode( array( 'id' => $agent->ID, 'addr' => $agent->user_address ) ) )] = esc_html( $agent->display_name );
        }

        if ( !empty( $agent_id ) && isset( $agent_list[$agent_id] ) )
        {
            return $agent_list[$agent_id];
        }
        else
        {
            return $agent_list;
        }
    }
}

function the_departure_point_list()
{
    return array(
        'Benoa Pier Bali'   => 'Benoa Pier Bali',
        'Maumere Flores'    => 'Maumere Flores',
        'Sorong West Papua' => 'Sorong West Papua',
        'Ambon'             => 'Ambon',
        'Lembeh Sulawesi'   => 'Lembeh Sulawesi'
    );
}

function the_arrival_point_list()
{
    return array(
        'Benoa Pier Bali'   => 'Benoa Pier Bali',
        'Maumere Flores'    => 'Maumere Flores',
        'Sorong West Papua' => 'Sorong West Papua',
        'Ambon'             => 'Ambon',
        'Lembeh Sulawesi'   => 'Lembeh Sulawesi'
    );
}

function the_departure_time_list()
{
    return array(
        '13:00-15:00' => '13:00-15:00',
        '12:00-14:00' => '12:00-14:00',
        '07:00-14:00' => '07:00-14:00',
        '10:00-12:00' => '10:00-12:00',
        '10:00-16:00' => '10:00-16:00',
    );
}

function the_arrival_time_list()
{
    return array(
        '08:00-08:30' => '08:00-08:30',
    );
}

function the_country_list( $code = '' )
{
    $countries = array(
        'AF' => 'Afghanistan',
        'AX' => 'Aland Islands',
        'AL' => 'Albania',
        'DZ' => 'Algeria',
        'AS' => 'American Samoa',
        'AD' => 'Andorra',
        'AO' => 'Angola',
        'AI' => 'Anguilla',
        'AQ' => 'Antarctica',
        'AG' => 'Antigua and Barbuda',
        'AR' => 'Argentina',
        'AM' => 'Armenia',
        'AW' => 'Aruba',
        'AU' => 'Australia',
        'AT' => 'Austria',
        'AZ' => 'Azerbaijan',
        'BS' => 'Bahamas',
        'BH' => 'Bahrain',
        'BD' => 'Bangladesh',
        'BB' => 'Barbados',
        'BY' => 'Belarus',
        'BE' => 'Belgium',
        'BZ' => 'Belize',
        'BJ' => 'Benin',
        'BM' => 'Bermuda',
        'BT' => 'Bhutan',
        'BO' => 'Bolivia',
        'BQ' => 'Bonaire, Sint Eustatius and Saba',
        'BA' => 'Bosnia and Herzegovina',
        'BW' => 'Botswana',
        'BV' => 'Bouvet Island',
        'BR' => 'Brazil',
        'IO' => 'British Indian Ocean Territory',
        'BN' => 'Brunei Darussalam',
        'BG' => 'Bulgaria',
        'BF' => 'Burkina Faso',
        'BI' => 'Burundi',
        'KH' => 'Cambodia',
        'CM' => 'Cameroon',
        'CA' => 'Canada',
        'CV' => 'Cape Verde',
        'KY' => 'Cayman Islands',
        'CF' => 'Central African Republic',
        'TD' => 'Chad',
        'CL' => 'Chile',
        'CN' => 'China',
        'CX' => 'Christmas Island',
        'CC' => 'Cocos (Keeling) Islands',
        'CO' => 'Colombia',
        'KM' => 'Comoros',
        'CG' => 'Congo',
        'CD' => 'Congo, Democratic Republic of the Congo',
        'CK' => 'Cook Islands',
        'CR' => 'Costa Rica',
        'CI' => 'Cote D\'Ivoire',
        'HR' => 'Croatia',
        'CU' => 'Cuba',
        'CW' => 'Curacao',
        'CY' => 'Cyprus',
        'CZ' => 'Czech Republic',
        'DK' => 'Denmark',
        'DJ' => 'Djibouti',
        'DM' => 'Dominica',
        'DO' => 'Dominican Republic',
        'EC' => 'Ecuador',
        'EG' => 'Egypt',
        'SV' => 'El Salvador',
        'GQ' => 'Equatorial Guinea',
        'ER' => 'Eritrea',
        'EE' => 'Estonia',
        'ET' => 'Ethiopia',
        'FK' => 'Falkland Islands (Malvinas)',
        'FO' => 'Faroe Islands',
        'FJ' => 'Fiji',
        'FI' => 'Finland',
        'FR' => 'France',
        'GF' => 'French Guiana',
        'PF' => 'French Polynesia',
        'TF' => 'French Southern Territories',
        'GA' => 'Gabon',
        'GM' => 'Gambia',
        'GE' => 'Georgia',
        'DE' => 'Germany',
        'GH' => 'Ghana',
        'GI' => 'Gibraltar',
        'GR' => 'Greece',
        'GL' => 'Greenland',
        'GD' => 'Grenada',
        'GP' => 'Guadeloupe',
        'GU' => 'Guam',
        'GT' => 'Guatemala',
        'GG' => 'Guernsey',
        'GN' => 'Guinea',
        'GW' => 'Guinea-Bissau',
        'GY' => 'Guyana',
        'HT' => 'Haiti',
        'HM' => 'Heard Island and Mcdonald Islands',
        'VA' => 'Holy See (Vatican City State)',
        'HN' => 'Honduras',
        'HK' => 'Hong Kong',
        'HU' => 'Hungary',
        'IS' => 'Iceland',
        'IN' => 'India',
        'ID' => 'Indonesia',
        'IR' => 'Iran, Islamic Republic of',
        'IQ' => 'Iraq',
        'IE' => 'Ireland',
        'IM' => 'Isle of Man',
        'IL' => 'Israel',
        'IT' => 'Italy',
        'JM' => 'Jamaica',
        'JP' => 'Japan',
        'JE' => 'Jersey',
        'JO' => 'Jordan',
        'KZ' => 'Kazakhstan',
        'KE' => 'Kenya',
        'KI' => 'Kiribati',
        'KP' => 'Korea, Democratic People\'s Republic of',
        'KR' => 'Korea, Republic of',
        'XK' => 'Kosovo',
        'KW' => 'Kuwait',
        'KG' => 'Kyrgyzstan',
        'LA' => 'Lao People\'s Democratic Republic',
        'LV' => 'Latvia',
        'LB' => 'Lebanon',
        'LS' => 'Lesotho',
        'LR' => 'Liberia',
        'LY' => 'Libyan Arab Jamahiriya',
        'LI' => 'Liechtenstein',
        'LT' => 'Lithuania',
        'LU' => 'Luxembourg',
        'MO' => 'Macao',
        'MK' => 'Macedonia, the Former Yugoslav Republic of',
        'MG' => 'Madagascar',
        'MW' => 'Malawi',
        'MY' => 'Malaysia',
        'MV' => 'Maldives',
        'ML' => 'Mali',
        'MT' => 'Malta',
        'MH' => 'Marshall Islands',
        'MQ' => 'Martinique',
        'MR' => 'Mauritania',
        'MU' => 'Mauritius',
        'YT' => 'Mayotte',
        'MX' => 'Mexico',
        'FM' => 'Micronesia, Federated States of',
        'MD' => 'Moldova, Republic of',
        'MC' => 'Monaco',
        'MN' => 'Mongolia',
        'ME' => 'Montenegro',
        'MS' => 'Montserrat',
        'MA' => 'Morocco',
        'MZ' => 'Mozambique',
        'MM' => 'Myanmar',
        'NA' => 'Namibia',
        'NR' => 'Nauru',
        'NP' => 'Nepal',
        'NL' => 'Netherlands',
        'AN' => 'Netherlands Antilles',
        'NC' => 'New Caledonia',
        'NZ' => 'New Zealand',
        'NI' => 'Nicaragua',
        'NE' => 'Niger',
        'NG' => 'Nigeria',
        'NU' => 'Niue',
        'NF' => 'Norfolk Island',
        'MP' => 'Northern Mariana Islands',
        'NO' => 'Norway',
        'OM' => 'Oman',
        'PK' => 'Pakistan',
        'PW' => 'Palau',
        'PS' => 'Palestinian Territory, Occupied',
        'PA' => 'Panama',
        'PG' => 'Papua New Guinea',
        'PY' => 'Paraguay',
        'PE' => 'Peru',
        'PH' => 'Philippines',
        'PN' => 'Pitcairn',
        'PL' => 'Poland',
        'PT' => 'Portugal',
        'PR' => 'Puerto Rico',
        'QA' => 'Qatar',
        'RE' => 'Reunion',
        'RO' => 'Romania',
        'RU' => 'Russian Federation',
        'RW' => 'Rwanda',
        'BL' => 'Saint Barthelemy',
        'SH' => 'Saint Helena',
        'KN' => 'Saint Kitts and Nevis',
        'LC' => 'Saint Lucia',
        'MF' => 'Saint Martin',
        'PM' => 'Saint Pierre and Miquelon',
        'VC' => 'Saint Vincent and the Grenadines',
        'WS' => 'Samoa',
        'SM' => 'San Marino',
        'ST' => 'Sao Tome and Principe',
        'SA' => 'Saudi Arabia',
        'SN' => 'Senegal',
        'RS' => 'Serbia',
        'CS' => 'Serbia and Montenegro',
        'SC' => 'Seychelles',
        'SL' => 'Sierra Leone',
        'SG' => 'Singapore',
        'SX' => 'Sint Maarten',
        'SK' => 'Slovakia',
        'SI' => 'Slovenia',
        'SB' => 'Solomon Islands',
        'SO' => 'Somalia',
        'ZA' => 'South Africa',
        'GS' => 'South Georgia and the South Sandwich Islands',
        'SS' => 'South Sudan',
        'ES' => 'Spain',
        'LK' => 'Sri Lanka',
        'SD' => 'Sudan',
        'SR' => 'Suriname',
        'SJ' => 'Svalbard and Jan Mayen',
        'SZ' => 'Swaziland',
        'SE' => 'Sweden',
        'CH' => 'Switzerland',
        'SY' => 'Syrian Arab Republic',
        'TW' => 'Taiwan, Province of China',
        'TJ' => 'Tajikistan',
        'TZ' => 'Tanzania, United Republic of',
        'TH' => 'Thailand',
        'TL' => 'Timor-Leste',
        'TG' => 'Togo',
        'TK' => 'Tokelau',
        'TO' => 'Tonga',
        'TT' => 'Trinidad and Tobago',
        'TN' => 'Tunisia',
        'TR' => 'Turkey',
        'TM' => 'Turkmenistan',
        'TC' => 'Turks and Caicos Islands',
        'TV' => 'Tuvalu',
        'UG' => 'Uganda',
        'UA' => 'Ukraine',
        'AE' => 'United Arab Emirates',
        'GB' => 'United Kingdom',
        'US' => 'United States',
        'UM' => 'United States Minor Outlying Islands',
        'UY' => 'Uruguay',
        'UZ' => 'Uzbekistan',
        'VU' => 'Vanuatu',
        'VE' => 'Venezuela',
        'VN' => 'Viet Nam',
        'VG' => 'Virgin Islands, British',
        'VI' => 'Virgin Islands, U.s.',
        'WF' => 'Wallis and Futuna',
        'EH' => 'Western Sahara',
        'YE' => 'Yemen',
        'ZM' => 'Zambia',
        'ZW' => 'Zimbabwe'
    );

    if ( isset( $countries[$code] ) )
    {
        return $countries[$code];
    }
    else
    {
        return $countries;
    }
}

/*
| -------------------------------------------------------------------------------------
| Function untuk mendapatkan data month
| -------------------------------------------------------------------------------------
*/
function get_month_data()
{
    $months = array(
        1  => 'January',
        2  => 'February',
        3  => 'March',
        4  => 'April',
        5  => 'May',
        6  => 'June',
        7  => 'July',
        8  => 'August',
        9  => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December'
    );
    return $months;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan table schedule
| -------------------------------------------------------------------------------------
*/
function table_schedule_and_rate( $page = '', $argument = '' )
{
    $meta_query       = '';
    $boat_name_search = '';
    $year_month       = '';

    $meta_query_boat_schedule      = array();
    $meta_query_iteneraries        = array();
    $meta_query_iteneraries_search = array();
    $meta_query_year               = array();

    $prefix = '_schedule_data_';

    $post_per_page = -1;

    $meta_query_general = array( array(
        'key'     => '_schedule_data_departure_date',
        'value'   => date( 'Y-m-d' ),
        'type'    => 'DATE',
        'compare' => '>='
    ) );

    //-- CHECK USER LOGIN
    $roles_agent = 0;

    if ( is_user_logged_in() )
    {
        $current_user = wp_get_current_user();
        $user_roles   = $current_user->roles;
        $roles        = $user_roles[0];

        if ( $roles == 'agent' )
        {
            $roles_agent = 1;
        }
    }

    if ( $page == 'boat' || $page == 'schedule_rate' )
    {
        $meta_query_boat_schedule = array( array(
            'key'     => '_schedule_data_boat',
            'value'   => $argument['id'],
            'compare' => '=',
        ) );
    }
    elseif ( $page == 'iteneraries' )
    {
        $meta_query_iteneraries = array( array(
            'key'     => '_schedule_data_iteneraries',
            'value'   => $argument['id'],
            'compare' => '='
        ) );

        $show_schedule = 5;
        $post_per_page = 5;
    }
    elseif ( $page == 'search' )
    {
        global $boat_data_search;

        $boat_name_search = $boat_data_search->post_title;
        $boat_id          = $boat_data_search->ID;

        if ( empty( $_GET['s'] ) === false )
        {
            $meta_query_boat_schedule = array( array(
                'key'     => '_schedule_data_boat',
                'value'   => $boat_id,
                'compare' => '='
            ) );
        }

        if ( isset( $_GET['dest'] ) && $_GET['dest'] != 'all' )
        {
            $meta_query_iteneraries_search[] = meta_query_itenerary_search( '_schedule_data_' );
        }

        $meta_query_year = meta_query_year_month_search( '_schedule_data_', $_GET );
    }

    $meta_query = array_merge( $meta_query_general, $meta_query_boat_schedule, $meta_query_iteneraries, $meta_query_iteneraries_search, $meta_query_year );

    if ( $page == "schedule_rate" || $page == "boat" )
    {
        echo '<input type="hidden" name="page_now" value="schedule_rate" />';
    }
    elseif ( $page == "search" )
    {
        if ( !empty( $_GET['s'] ) )
        {
            echo '<input type="hidden" name="page_now" value="schedule_rate" />';
        }
    }
    else
    {
        echo '<input type="hidden" name="page_now" value="" />';
    }

    $html = alert_inport();

    $schedule_data = array();
    $year_data     = array();
    $boat_data     = array();
    $boat_id_array = array();

    $query = get_posts( array(
        'meta_key'    => '_schedule_data_departure_date',
        'numberposts' => $post_per_page,
        'meta_query'  => $meta_query,
        'post_type'   => 'schedule_data',
        'orderby'     => 'meta_value',
        'post_status' => 'publish',
        'order'       => 'ASC',
    ) );

    //-- IF DATA EMPTY
    $empty_search = 0;

    if ( count( $query ) == 0 )
    {
        $meta_query_year = meta_query_year_month_search( $prefix, $_GET, 1 );
        $meta_query      = array_merge( $meta_query_general, $meta_query_boat_schedule, $meta_query_iteneraries, $meta_query_iteneraries_search, $meta_query_year );

        $query = get_posts( array(
            'meta_key'    => '_schedule_data_departure_date',
            'numberposts' => $post_per_page,
            'meta_query'  => $meta_query,
            'post_type'   => 'schedule_data',
            'orderby'     => 'meta_value',
            'post_status' => 'publish',
            'order'       => 'ASC',
        ) );

        $empty_search = 1;
    }

    $i = 1;

    foreach ( $query as $d )
    {
        $post_id    = $d->ID;
        $post_title = $d->post_title;

        //-- GET DATE
        $depart_date = get_post_meta( $post_id, '_schedule_data_departure_date', true );
        $return_date = get_post_meta( $post_id, '_schedule_data_arrival_date', true );
        $year        = date( 'Y', strtotime( $depart_date ) );

        //-- GET MERMAID
        $boat_id    = get_post_meta( $post_id, '_schedule_data_boat', true );
        $boat_title = get_the_title( $boat_id );

        if ( !in_array( $boat_id, $boat_id_array ) )
        {
            if ( strtolower( $boat_title ) == 'mermaid i' )
            {
                $link_sch = get_site_url() . '/schedule-rates/mermaid-i/';
            }
            else
            {
                $link_sch = get_site_url() . '/schedule-rates/mermaid-ii/';
            }

            $boat_data[] = array(
                'boat_id'       => $boat_id,
                'boat_title'    => $boat_title,
                'boat_link_sch' => $link_sch
            );

            $boat_id_array[] = $boat_id;
        }

        //-- YEAR DATA
        if ( !in_array( $year, $year_data ) )
        {
            $year_data[] = $year;
        }

        $schedule_data[] = array(
            'id'          => $post_id,
            'title'       => $post_title,
            'depart_date' => $depart_date,
            'return_date' => $return_date,
            'boat_title'  => $boat_title,
            'boat_id'     => $boat_id
        );

        //-- JIKA SHOW SCHEDULE SESUAI DENGAN YANG DI SET MAKA PROSES LOOPING AKAN LANGSUNG DI STOP
        if ( !empty( $show_schedule ) && $show_schedule == $i )
        {
            break;
        }

        $i++;
    }

    //-- GET CURRENCY DATA
    $currency_data = get_currency_data();

    $select_your_trip_text = get_label_string( 'Select Your Trip', true );

    $month       = isset( $_GET['month'] ) ? $_GET['month'] : 'all';
    $year        = isset( $_GET['year'] ) ? $_GET['year'] : 'all';
    $destination = isset( $_GET['dest'] ) ? $_GET['dest'] : 'all';

    $html .= '<div class="row wrap-filter-schedule">';
    $html .= $currency_data;

    if ( $page == "search" )
    {
        if ( !empty( $_GET['s'] ) )
        {
            $in_month_year = ucwords( $_GET['month'] ) . ' ' . $_GET['year'];

            if ( $month == "all" || $year == "all" )
            {
                $month_search = $month == "all" ? 'All Months' : $_GET['month'];
                $year_search  = $year == "all" ? 'All Years' : $_GET['year'];

                $in_month_year = ucwords( $month_search ) . ' & ' . $year_search;
            }

            $html .= '<div class="col-lg-10 select-trip">';

            if ( isset( $_GET['dest'] ) && $_GET['dest'] == "all" )
            {
                $html .= '<h4>Results for <b>' . $boat_name_search . '</b> to <b>All Destinations</b> in <b>' . $in_month_year . '</b></h4>';
            }
            else
            {
                $html .= '<h4>Results for <b>' . $boat_name_search . '</b> to <b>' . get_iteneraries_search_title() . '</b> in <b>' . $in_month_year . '</b></h4>';
            }

            $html .= '</div>';
            $html .= '<div class="col-lg-2 choose-currency-year clearfix">';
        }
        else
        {
            $html .= '<div class="col-lg-2 offset-10 choose-currency-year clearfix">';
        }
    }
    else
    {
        $html .= '<div class="col-lg-3 col-md-3 col-sm-12 select-trip">';
        $html .= '  <h4>' . $select_your_trip_text . '</h4>';
        $html .= '</div>';
        $html .= '<div class="col-lg-9 col-md-9 col-sm-12 choose-currency-year clearfix">';
    }

    $html .= '<input type="hidden" name="action-choose-price" value="choose-price" />';
    $html .= '<input type="hidden" name="action-currency-year" value="filter-currency-year" />';

    if ( $page == "iteneraries" )
    {
        $html .= '<input type="hidden" name="filter-dest" value="" />';
        $html .= '<input type="hidden" name="filter-iteneraries" value="' . $argument['id'] . '" />';
        $html .= '<input type="hidden" name="filter-month" value="" />';
        $html .= '<input type="hidden" name="filter-year" value="" />';
        $html .= '<input type="hidden" name="year-month" value="" />';
    }
    else
    {
        $html .= '<input type="hidden" name="filter-dest" value="' . $destination . '" />';
        $html .= '<input type="hidden" name="filter-iteneraries" value="' . $destination . '" />';
        $html .= '<input type="hidden" name="filter-month" value="' . $month . '" />';
        $html .= '<input type="hidden" name="filter-year" value="' . $year . '" />';
        $html .= '<input type="hidden" name="year-month" value="' . $year_month . '" />';
    }

    if ( $page != "search" )
    {
        $html .= '<ul class="choose-year choose-filter" data-filter="choose-year">';

        if ( $page == "schedule_rate" )
        {
            $html .= '<li data-filter="all" class="active"><span>All</span></li>';
        }

        if ( !empty( $year_data ) )
        {
            foreach ( $year_data as $d )
            {
                $html .= "<li data-filter=\"$d\"><span>$d</span></li>";
            }
        }

        $html .= '</ul>';
    }

    $html .= '<span class="select-currency" data-currency-symbol="€" data-currency-code="EUR">EUR</span>';

    if ( $page == "iteneraries" )
    {
        $html .= '<select name="mermaid-type" class="default choose-mermaid choose-filter" data-filter="choose-mermaid">';
        $html .= '  <option value="">' . get_label_string( 'Select Your Mermaid', true ) . '</option>';

        if ( !empty( $boat_data ) )
        {
            sort( $boat_data );

            foreach ( $boat_data as $d )
            {
                $html .= '<option value="' . $d['boat_id'] . '">' . $d['boat_title'] . '</option>';
            }
        }

        $html .= '</select>';
    }
    else
    {
        if ( isset( $_GET['s'] ) && empty( $_GET['s'] ) )
        {
            $html .= '<input type="hidden" name="mermaid-type" value="" />';
        }
        else
        {
            $html .= '<input type="hidden" name="mermaid-type" value="' . $boat_id . '" />';
        }
    }

    $html .= '</div>';
    $html .= '</div>';
    $html .= '<div class="row">';
    $html .= '  <div class="col-md-12 wrap-table-default wrap-table-schedule">';

    if ( $empty_search )
    {
        $schedule_ratest_inport_alert = get_option( 'schedule_ratest_inport_alert' );

        $des_next_schedule = '';

        if ( !empty( $schedule_ratest_inport_alert ) )
        {
            $schedule_ratest_inport_alert = json_decode( $schedule_ratest_inport_alert );

            $des_next_schedule = $schedule_ratest_inport_alert->des_next_schedule;
            $des_next_schedule = !empty( $des_next_schedule ) ? $des_next_schedule : '';
        }

        $html .= '<div class="search-result-not-available">' . $des_next_schedule . '</div>';
    }

    $html .= '<p class="note-schedule first">*Prices are per person</p>';
    $html .= '<div class="loading"></div>';
    $html .= '<div class="loading-global"></div>';

    $slug = isset( $argument['slug'] ) ? $argument['slug'] : '';
    $slug = !empty( $slug ) ? $slug : $_GET['s'];

    $date_text         = get_label_string( 'Date', true );
    $trip_code_text    = get_label_string( 'Trip Code', true );
    $itenerary_text    = get_label_string( 'Itenerary', true );
    $master_text       = get_label_string( 'Master', true );
    $single_text       = get_label_string( 'Single', true );
    $deluxe_text       = get_label_string( 'Deluxe', true );
    $budget_text       = get_label_string( 'Budget', true );
    $lower_deck_text   = get_label_string( 'Lower Deck', true );
    $status_text       = get_label_string( 'Status', true );
    $duration_text     = get_label_string( 'Duration', true );
    $return_text       = get_label_string( 'Return', true );
    $trip_details_text = get_label_string( 'Trip Details', true );
    $book_now_text     = get_label_string( 'Book Now', true );
    $get_last_mont     = strtolower( Date( "F", strtotime( $_GET['month'] . " last month" ) ) );
    $get_next_mont     = strtolower( Date( "F", strtotime( $_GET['month'] . " next month" ) ) );
    $last_link         = '/?s=' . $slug . '&dest=' . $_GET['dest'] . '&month=' . $get_last_mont . '&year=' . $_GET['year'];
    $next_link         = '/?s=' . $slug . '&dest=' . $_GET['dest'] . '&month=' . $get_next_mont . '&year=' . $_GET['year'];
    $html_mobile       = '';

    $html .= '<div class="row mb-3">';
    $html .= '    <div class="col-md-6 col-sm-6 col-6 float-left text-left">';
    $html .= '        <a href="' . $last_link . '" class="btn btn-mermaid" style="background-color: #001B48; color: #fff">Previous</a>';
    $html .= '    </div>';
    $html .= '    <div class="col-md-6 col-sm-6 col-6 float-right text-right">';
    $html .= '        <a href="' . $next_link . '" class="btn btn-mermaid" style="background-color: #001B48; color: #fff">Next</a>';
    $html .= '    </div>';
    $html .= '</div>';
    $html .= '<table>';
    $html .= '    <thead>';
    $html .= '        <tr>';
    $html .= '            <th width="5%">' . $date_text . '</th>';
    $html .= '            <th width="12%">' . $trip_code_text . '</th>';
    $html .= '            <th width="28%">' . $itenerary_text . '</th>';

    if ( $slug != "mermaid-ii" )
    {
        $html .= '        <th width="11%">' . $master_text . '</th>';
        $html .= '        <th width="11%">' . $single_text . '</th>';
        $html .= '        <th width="11%">' . $deluxe_text . '</th>';
    }
    else
    {
        $html .= '        <th width="11%">' . $deluxe_text . ' Cabin</th>';
    }

    if ( $slug == "mermaid-ii" )
    {
        $html .= '        <th width="11%">' . $budget_text . ' Cabin</th>';
    }
    else
    {
        $html .= '        <th width="11%">' . $lower_deck_text . '</th>';
    }

    $html .= '            <th width="11%">' . $status_text . '</th>';
    $html .= '         </tr>';
    $html .= '    </thead>';
    $html .= '    <tbody>';

    if ( !empty( $schedule_data ) )
    {
        $i = 0;

        foreach ( $schedule_data as $d )
        {
            $post_id        = $d['id'];
            $post_title     = $d['title'];
            $sanitize_title = sanitize_title( $post_title );

            //-- Date
            $depart_date           = $d['depart_date'];
            $return_date           = $d['return_date'];
            $strtotime_depart_date = strtotime( $depart_date );
            $strtotime_return_date = strtotime( $return_date );
            $start                 = new DateTime( $depart_date );
            $end                   = new DateTime( $return_date );
            $result_date           = $end->diff( $start );
            $night                 = $result_date->d;
            $day                   = $night + 1;

            //-- PRICE DATA
            $price_master     = get_post_meta( $post_id, $prefix . 'price_master', true );
            $price_master_dsc = get_post_meta( $post_id, $prefix . 'price_master_dsc', true );
            $price_single     = get_post_meta( $post_id, $prefix . 'price_single', true );
            $price_single_dsc = get_post_meta( $post_id, $prefix . 'price_single_dsc', true );
            $price_deluxe     = get_post_meta( $post_id, $prefix . 'price_deluxe', true );
            $price_deluxe_dsc = get_post_meta( $post_id, $prefix . 'price_deluxe_dsc', true );
            $price_lower      = get_post_meta( $post_id, $prefix . 'price_lower', true );
            $price_lower_dsc  = get_post_meta( $post_id, $prefix . 'price_lower_dsc', true );

            //-- ALLOTMENT DATA
            if ( $roles_agent )
            {
                $allotment_master = get_post_meta( $post_id, $prefix . 'allotment_master', true );
                $allotment_single = get_post_meta( $post_id, $prefix . 'allotment_single', true );
                $allotment_deluxe = get_post_meta( $post_id, $prefix . 'allotment_deluxe', true );
                $allotment_lower  = get_post_meta( $post_id, $prefix . 'allotment_lower', true );
            }

            //-- TRIP
            $trip_id   = get_post_meta( $post_id, $prefix . 'iteneraries', true );
            $trip      = get_the_title( $trip_id );
            $trip_link = get_the_permalink( $trip_id );

            //-- Departure Port & Arrival port
            $port_in  = get_post_meta( $trip_id, '_iteneraries_departure_port', true );
            $port_in  = !empty( $port_in ) ? $port_in : '-';
            $port_out = get_post_meta( $trip_id, '_iteneraries_arrival_port', true );
            $port_out = !empty( $port_out ) ? $port_out : '-';

            // ================= HTML DESKTOP =================
            $html .= '        <tr id="' . $sanitize_title . '">';
            $html .= '            <td class="date depart_date" data-value="' . date( "d M Y", $strtotime_depart_date ) . '"><span>' . date( "d", $strtotime_depart_date ) . '</span><h5>' . date( "M", $strtotime_depart_date ) . '</h5> <h5>' . date( "Y", $strtotime_depart_date ) . '</h5> </td>';
            $html .= '            <td class="trip_code" data-value="' . $post_title . '">' . $post_title . '</td>';
            $html .= '            <td class="itenerary">';
            $html .= '                <h3 data-value="' . $trip . '">' . $trip . '</h3>';
            $html .= '                <h4 class="duration">' . $duration_text . ': ' . $day . 'D ' . $night . 'N</h4>';
            $html .= '                <h4 class="return" data-value="' . date( "d M Y", $strtotime_return_date ) . '">' . $return_text . ': ' . date( "d M Y", $strtotime_return_date ) . '</h4>';
            $html .= '                <h4 class="mermaid-type" data-value="' . $d['boat_title'] . '">' . $d['boat_title'] . '</h4>';

            if ( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) )
            {
                $html .= '            <a href="' . $trip_link . '" class="disabled-inport" data-boat-id="' . $d['boat_id'] . '" data-trip="' . $trip_id . '">' . $trip_details_text . '</a>';
            }
            else
            {
                $html .= '            <a href="' . $trip_link . '" class="trip-details" data-boat-id="' . $d['boat_id'] . '" data-trip="' . $trip_id . '">' . $trip_details_text . '</a>';
            }

            $html .= '            </td>';

            if ( $slug != "mermaid-ii" )
            {
                if ( $price_master == 0 || empty( $price_master ) )
                {
                    $html .= '<td>-</td>';
                }
                else
                {
                    $html .= '<td class="price_type">';

                    if ( $price_master_dsc != 0 )
                    {
                        $html .= '<p class="discount_price_real"> <strike>€' . $price_master . ' </strike></p>';
                        $price_discount = $price_master - ( $price_master * ( $price_master_dsc / 100 ) );
                        $html .= '<input type="radio" class="radio-desktop radio-master" name="type-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="master" id="master-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_discount . '" data-currency-code="EUR"><label for="master-' . $i . '" class="price-desktop">€ ' . number_format( $price_discount, 0 ) . '</label>';
                    }
                    else
                    {
                        $html .= '<input type="radio" class="radio-desktop radio-master" name="type-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="master" id="master-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_master . '" data-currency-code="EUR"><label for="master-' . $i . '" class="price-desktop">€ ' . number_format( $price_master, 0 ) . '</label>';
                    }

                    $html .= ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_master . '</p>' : '';
                    $html .= !empty( $price_master_dsc ) ? ' <p class="discount_price"><span class="triangle-right"></span><strong>' . $price_master_dsc . '%</strong> Discount</p>' : '';
                    $html .= '</td>';
                }

                if ( $price_single == 0 || empty( $price_single ) )
                {
                    $html .= '<td>-</td>';
                }
                else
                {
                    $html .= '<td class="price_type">';

                    if ( $price_single_dsc != 0 )
                    {
                        $html .= '<p class="discount_price_real"> <strike>€' . $price_single . ' </strike></p>';
                        $price_discount2 = $price_single - ( $price_single * ( $price_single_dsc / 100 ) );
                        $html .= '<input type="radio" class="radio-desktop radio-single" name="type-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="single" id="single-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_discount2 . '" data-currency-code="EUR"><label for="single-' . $i . '" class="price-desktop">€ ' . number_format( $price_discount2, 0 ) . '</label>';
                    }
                    else
                    {
                        $html .= '<input type="radio" class="radio-desktop radio-single" name="type-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="single" id="single-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_single . '" data-currency-code="EUR"><label for="single-' . $i . '" class="price-desktop">€ ' . number_format( $price_single, 0 ) . '</label>';
                    }

                    $html .= ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_single . '</p>' : '';
                    $html .= !empty( $price_single_dsc ) ? ' <p class="discount_price"><span class="triangle-right"></span><strong>' . $price_single_dsc . '%</strong> Discount</p>' : '';
                    $html .= '</td>';
                }
            }

            if ( $price_deluxe == 0 || empty( $price_deluxe ) )
            {
                $html .= '<td>-</td>';
            }
            else
            {
                $html .= '<td class="price_type">';

                if ( $price_deluxe_dsc != 0 )
                {
                    $html .= '<p class="discount_price_real"> <strike>€' . $price_deluxe . ' </strike></p>';
                    $price_discount3 = $price_deluxe - ( $price_deluxe * ( $price_deluxe_dsc / 100 ) );
                    $html .= '<input type="radio" class="radio-desktop radio-deluxe" name="type-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="deluxe" id="deluxe-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_discount3 . '" data-currency-code="EUR"><label for="deluxe-' . $i . '" class="price-desktop">€ ' . number_format( $price_discount3, 0 ) . '</label>';
                }
                else
                {
                    $html .= '<input type="radio" class="radio-desktop radio-deluxe" name="type-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="deluxe" id="deluxe-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_deluxe . '" data-currency-code="EUR"><label for="deluxe-' . $i . '" class="price-desktop">€ ' . number_format( $price_deluxe, 0 ) . '</label>';
                }

                $html .= ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_deluxe . '</p>' : '';
                $html .= !empty( $price_deluxe_dsc ) ? '<p class="discount_price"><span class="triangle-right"></span><strong>' . $price_deluxe_dsc . '%</strong> Discount</p>' : '';

                $html .= '</td>';
            }

            if ( $price_lower == 0 || empty( $price_lower ) )
            {
                $html .= '<td>-</td>';
            }
            else
            {
                $html .= '<td class="price_type">';

                if ( $price_lower_dsc != 0 )
                {
                    $html .= '<p class="discount_price_real"> <strike>€' . $price_lower . ' </strike></p>';
                    $price_discount4 = $price_lower - ( $price_lower * ( $price_lower_dsc / 100 ) );
                    $html .= '<input type="radio" class="radio-desktop radio-lower" name="type-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="lower" id="lower-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_discount4 . '" data-currency-code="EUR"><label for="lower-' . $i . '" class="price-desktop">€ ' . number_format( $price_discount4, 0 ) . '</label>';
                }
                else
                {
                    $html .= '<input type="radio" class="radio-desktop radio-lower" name="type-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="lower" id="lower-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_lower . '" data-currency-code="EUR"><label for="lower-' . $i . '" class="price-desktop">€ ' . number_format( $price_lower, 0 ) . '</label>';
                }

                $html .= ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_lower . '</p>' : '';
                $html .= !empty( $price_lower_dsc ) ? '<p class="discount_price"> <span class="triangle-right"></span><strong>' . $price_lower_dsc . '%</strong> Discount</p>' : '';

                $html .= '</td>';
            }

            $html .= '<td class="status">';

            if ( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) )
            {
                $html .= '<a href="" class="disabled-inport" data-screen="desktop" data-boat="' . sanitize_title( $boat_title ) . '" data-port-in="' . $port_in . '" data-port-out="' . $port_out . '">' . $book_now_text . '</a>';
            }
            else
            {
                $html .= '<a href="" class="disabled" data-screen="desktop" data-boat="' . sanitize_title( $boat_title ) . '" data-port-in="' . $port_in . '" data-port-out="' . $port_out . '">' . $book_now_text . '</a>';
            }

            $html .= '  </td>';
            $html .= '</tr>';

            // ================= HTML MOBILE =================
            $html_mobile .= '
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="list-schedule-data" id="mobile-' . $sanitize_title . '">
                            <div class="loading"></div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <h3>' . $date_text . '</h3>
                                    <h5 class="depart-date">' . date( "d M Y", $strtotime_depart_date ) . '</h5>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <h3>' . $trip_code_text . '</h3>
                                    <h5 class="trip-code">' . $post_title . '</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <h3>' . $itenerary_text . '</h3>
                                    <h5 class="itenerary-title">' . $trip . '</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 itenerary">
                                    <h4 class="duration">' . $day . 'D ' . $night . 'N</h4>
                                    <h4 class="return" data-value="' . date( "d M Y", $strtotime_return_date ) . '">' . date( "d M Y", $strtotime_return_date ) . '</h4>
                                </div>
                                <div class="col-lg-6 col-md-6 itenerary">
                                    <h4 class="mermaid-type" data-value="' . $d['boat_title'] . '">' . $d['boat_title'] . '</h4>
                    ';

            if ( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) ):
                $html_mobile .= '<a href="' . $trip_link . '" class="disabled-inport" data-boat-id="' . $d['boat_id'] . '" data-trip="' . $trip_id . '">' . $trip_details_text . '</a>';
            else:
                $html_mobile .= '<a href="' . $trip_link . '" class="trip-details" data-boat-id="' . $d['boat_id'] . '" data-trip="' . $trip_id . '">' . $trip_details_text . '</a>';
            endif;

            $html_mobile .= '  
                                </div>
                            </div>
                            <div class="row">
                    ';
            if ( $slug != "mermaid-ii" ):
                if ( $price_master == 0 || empty( $price_master ) ):
                    $html_mobile .= '
                                        <div class="col-lg-6 col-md-6 cabins-type">
                                            <label>Master</label>
                                            <p>-</p>
                                        </div>
                                        ';
                else:
                    $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_master . '</p>' : '';

                    if ( $price_master_dsc != 0 ) :
                        $price                = '<p class="discount_price_real_mobile"> <strike>€ ' . $price_master . ' </strike></p>';
                        $price_discount       = $price_master - ( $price_master * ( $price_master_dsc / 100 ) );
                        $price_discount_label = !empty( $price_master_dsc ) ? ' <div class="label_discount"><span class="triangle-right"></span><p class="discount_price"><strong>' . $price_master_dsc . '%</strong> Discount</p></div>' : '';

                        $html_mobile .= '
                                            <div class="col-lg-6 col-md-6 cabins-type">
                                                <label>Master</label>
                                                ' . $price . '
                                                <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="master" id="master-m-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_discount . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-master"><label for="master-' . $i . '" class="price-mobile">€ ' . number_format( $price_discount, 0 ) . '</label>
                                                ' . $roles_agent_html . '
                                                ' . $price_discount_label . '
                                            </div>';
                    else:
                        $html_mobile .= '
                                            <div class="col-lg-6 col-md-6 cabins-type">
                                                <label>Master</label>
                                                <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="master" id="master-m-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_master . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-master"><label for="master-' . $i . '" class="price-mobile">€ ' . number_format( $price_master, 0 ) . '</label>
                                                ' . $roles_agent_html . '
                                            </div>';
                    endif;
                endif;

            if ( $price_single == 0 || empty( $price_single ) ):
                $html_mobile .= '
                                        <div class="col-lg-6 col-md-6 cabins-type">
                                            <label>Single</label>
                                            <p>-</p>
                                        </div>
                                        ';
            else:

                $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_single . '</p>' : '';

                if ( $price_single_dsc != 0 ) :
                    $price                = '<p class="discount_price_real_mobile"> <strike>€ ' . $price_single . ' </strike></p>';
                    $price_discount       = $price_single - ( $price_single * ( $price_single_dsc / 100 ) );
                    $price_discount_label = !empty( $price_single_dsc ) ? ' <div class="label_discount"><span class="triangle-right"></span><p class="discount_price"><strong>' . $price_single_dsc . '%</strong> Discount</p></div>' : '';
                    $html_mobile .= '
                                            <div class="col-lg-6 col-md-6 cabins-type">
                                                <label>Single</label>
                                                ' . $price . '
                                                <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="single" id="single-m-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_discount . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-single"><label for="single-' . $i . '" class="price-mobile">€ ' . number_format( $price_discount, 0 ) . '</label>
                                                ' . $roles_agent_html . '
                                                ' . $price_discount_label . '
                                            </div>';
                else:
                    $html_mobile .= '
                                            <div class="col-lg-6 col-md-6 cabins-type">
                                                <label>Single</label>
                                                <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="single" id="single-m-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_single . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-single"><label for="single-' . $i . '" class="price-mobile">€ ' . number_format( $price_single, 0 ) . '</label>
                                                ' . $roles_agent_html . '
                                            </div>';
                endif;
            endif;
            endif;

            $html_mobile .= '  
                            </div>
                            <div class="row">
                    ';

            if ( $price_deluxe == 0 || empty( $price_deluxe ) ):
                $text_label_price = $slug != "mermaid-ii" ? 'Deluxe' : 'Deluxe Cabin';
                $html_mobile .= '
                                        <div class="col-lg-6 col-md-6 cabins-type">
                                            <label>' . $text_label_price . '</label>
                                            <p>-</p>
                                        </div>
                                    ';
            else:
                $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_deluxe . '</p>' : '';

                $text_label_price = $slug != "mermaid-ii" ? 'Deluxe' : 'Deluxe Cabin';

                if ( $price_deluxe_dsc != 0 ) :
                    $price                = '<p class="discount_price_real_mobile"> <strike>€ ' . $price_deluxe . ' </strike></p>';
                    $price_discount       = $price_deluxe - ( $price_deluxe * ( $price_deluxe_dsc / 100 ) );
                    $price_discount_label = !empty( $price_deluxe_dsc ) ? ' <div class="label_discount"><span class="triangle-right"></span><p class="discount_price"><strong>' . $price_deluxe_dsc . '%</strong> Discount</p></div>' : '';
                    $html_mobile .= '
                                        <div class="col-lg-6 col-md-6 cabins-type">
                                            <label>' . $text_label_price . '</label>
                                            ' . $price . '
                                            <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="deluxe" id="deluxe-m-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_discount . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-deluxe"><label for="deluxe-' . $i . '" class="price-mobile">€ ' . number_format( $price_discount, 0 ) . '</label>
                                            ' . $roles_agent_html . '
                                            ' . $price_discount_label . '
                                        </div>';
                else:
                    $html_mobile .= '
                                        <div class="col-lg-6 col-md-6 cabins-type">
                                            <label>' . $text_label_price . '</label>
                                            <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="deluxe" id="deluxe-m-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_deluxe . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-deluxe"><label for="deluxe-' . $i . '" class="price-mobile">€ ' . number_format( $price_deluxe, 0 ) . '</label>
                                            ' . $roles_agent_html . '
                                        </div>';
                endif;
            endif;

            if ( $price_lower == 0 || empty( $price_lower ) ):
                $text_label_price = $slug != "mermaid-ii" ? 'Lower Deck' : 'Budget Cabin';
                $html_mobile .= '
                                        <div class="col-lg-6 col-md-6 cabins-type">
                                            <label>' . $text_label_price . '</label>
                                            <p>-</p>
                                        </div>
                                    ';
            else:
                $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_lower . '</p>' : '';
                $text_label_price = $slug != "mermaid-ii" ? 'Lower Deck' : 'Budget Cabin';
                if ( $price_lower_dsc != 0 ) :
                    $price                = '<p class="discount_price_real_mobile"> <strike>€ ' . $price_lower . ' </strike></p>';
                    $price_discount       = $price_lower - ( $price_lower * ( $price_lower_dsc / 100 ) );
                    $price_discount_label = !empty( $price_lower_dsc ) ? ' <div class="label_discount"><span class="triangle-right"></span><p class="discount_price"><strong>' . $price_lower_dsc . '%</strong> Discount</p></div>' : '';
                    $html_mobile .= '
                                        <div class="col-lg-6 col-md-6 cabins-type">
                                            <label>' . $text_label_price . '</label>
                                            ' . $price . '
                                            <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="lower" id="lower-m-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_discount . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-lower"><label for="lower-' . $i . '" class="price-mobile">€ ' . number_format( $price_discount, 0 ) . '</label>
                                            ' . $roles_agent_html . '
                                            ' . $price_discount_label . '
                                        </div>';
                else:
                    $html_mobile .= '
                                        <div class="col-lg-6 col-md-6 cabins-type">
                                            <label>' . $text_label_price . '</label>
                                            <input type="radio" name="type-m-mermaid-' . $i . '" data-itenerary="' . $trip_id . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="lower" id="lower-m-' . $i . '" data-boat="' . sanitize_title( $boat_title ) . '" data-price="' . $price_lower . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-lower"><label for="lower-' . $i . '" class="price-mobile">€ ' . number_format( $price_lower, 0 ) . '</label>
                                            ' . $roles_agent_html . '
                                        </div>';
                endif;
            endif;

            $html_mobile .= '  
                            </div>
                            <div class="row">
                    ';

            $html_mobile .= '<div class="col-lg-12 col-md-12 status">';

            if ( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) ):
                $html_mobile .= '<a href="" class="disabled-inport" data-screen="mobile" data-boat="' . sanitize_title( $boat_title ) . '" data-port-in="' . $port_in . '" data-port-out="' . $port_out . '">' . $book_now_text . '</a>';
            else:
                $html_mobile .= '<a href="" class="disabled" data-screen="mobile" data-boat="' . sanitize_title( $boat_title ) . '" data-port-in="' . $port_in . '" data-port-out="' . $port_out . '">' . $book_now_text . '</a>';
            endif;

            $html_mobile .= '</div>';

            $html_mobile .= '
                            </div>
                        </div>
                    </div>
                    ';

            if ( $i % 2 == 1 ):
                $html_mobile .= '
                        </div>
                        <div class="row">
                        ';
            endif;

            $i++;
        }
    }

    $wrap_schedule_mobile = '
                <div class="wrap-schedule-data-mobile">
                    <h3 class="select-trip">' . $select_your_trip_text . '</h3>
                    <div class="container-schedule-data-mobile">
                        <div class="row">
                            ' . $html_mobile . '
                        </div>
                    </div>
                </div>
            ';

    $html .= '
                </tbody>
            </table>
            ' . $wrap_schedule_mobile . '
            <p class="note-schedule">*Prices are per person</p>
        </div>
    </div>
    ';


    // ============= MOBILE SCHEDULE AND RATES =============
    if ( $page == "iteneraries" || $page == "search" ):
        if ( !empty( $boat_data ) ):
            $html .= '<div class="button-more-sch">';
            sort( $boat_data );
            foreach ( $boat_data as $ab ):
                $html .= '<a href="' . $ab['boat_link_sch'] . '" class="btn-more-schedule">More Schedules & Rates for ' . $ab['boat_title'] . '</a>';
            endforeach;
            $html .= '</div>';
        endif;
    elseif ( $page == "boat" ):
        global $boat_data;

        $boat_id = $argument['id'];

        // OTHER BOAT
        $other_boat = array();

        while ( $boat_data->have_posts() ):
            $boat_data->the_post();

            $post_id    = get_the_ID();
            $post_title = get_the_title();
            $post_link  = get_permalink();

            if ( $post_id != $boat_id ):
                $other_boat = array(
                    'title' => $post_title,
                    'link'  => $post_link
                );
                break;
            endif;

        endwhile;
    wp_reset_postdata();

    if ( !empty( $other_boat ) ):
        $html .= '<a href="' . $other_boat['link'] . '" class="btn-more-schedule">See ' . $other_boat['title'] . ' Cruises</a>';
    endif;

    endif;


    // ALERT ERROR SELECT CABIN FIRST
    $schedule_ratest_alert   = get_option( 'schedule_ratest_inport_alert' );
    $title_rates_cabin_first = "";
    $des_rates_cabin_first   = "";

    if ( !empty( $schedule_ratest_alert ) ):
        $schedule_ratest_alert = json_decode( $schedule_ratest_alert );

        $title_rates_cabin_first = $schedule_ratest_alert->title_rates_cabin_first;
        $des_rates_cabin_first   = $schedule_ratest_alert->des_rates_cabin_first;
    endif;

    $title_rates_cabin_first = empty( $title_rates_cabin_first ) ? '' : "<h3>" . $title_rates_cabin_first . "</h3>";
    $des_rates_cabin_first   = empty( $des_rates_cabin_first ) ? '' : "<p>" . $des_rates_cabin_first . "</p>";

    $html .= '<input type="hidden" name="alert_cabin_first" value="' . $title_rates_cabin_first . $des_rates_cabin_first . '" />';

    echo $html;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk set schedule rate for mobile
| -------------------------------------------------------------------------------------
*/
function schedule_rate_mobile( $schedule_data, $prefix, $slug, $roles_agent )
{
    $html = '';
    if ( !empty( $schedule_data ) ):
        $html .= '
        <div class="wrap-schedule-data-mobile">
            <h3 class="select-trip">' . get_label_string( 'Select Your Trip', true ) . '</h3>
            <div class="row">
        ';
        $i = 0;
        foreach ( $schedule_data as $d ):
            $post_id        = $d['id'];
            $post_title     = $d['title'];
            $sanitize_title = sanitize_title( $post_title );

            // Date
            $depart_date           = $d['depart_date'];
            $return_date           = $d['return_date'];
            $strtotime_depart_date = strtotime( $depart_date );
            $strtotime_return_date = strtotime( $return_date );
            $start                 = new DateTime( $depart_date );
            $end                   = new DateTime( $return_date );
            $result_date           = $end->diff( $start );
            $night                 = $result_date->d;
            $day                   = $night + 1;

            // PRICE DATA
            $price_master = get_post_meta( $post_id, $prefix . 'price_master', true );
            $price_single = get_post_meta( $post_id, $prefix . 'price_single', true );
            $price_deluxe = get_post_meta( $post_id, $prefix . 'price_deluxe', true );
            $price_lower  = get_post_meta( $post_id, $prefix . 'price_lower', true );

            // ALLOTMENT DATA
            $allotment_master = get_post_meta( $post_id, $prefix . 'allotment_master', true );
            if ( $roles_agent ):
                $allotment_single = get_post_meta( $post_id, $prefix . 'allotment_single', true );
                $allotment_deluxe = get_post_meta( $post_id, $prefix . 'allotment_deluxe', true );
                $allotment_lower  = get_post_meta( $post_id, $prefix . 'allotment_lower', true );
            endif;

            // TRIP
            $trip_id   = get_post_meta( $post_id, $prefix . 'iteneraries', true );
            $trip      = get_the_title( $trip_id );
            $trip_link = get_the_permalink( $trip_id );

            $html .= '
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="list-schedule-data" id="mobile-' . $sanitize_title . '">
                    <div class="loading"></div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3>Date</h3>
                            <h5>' . date( "d M Y", $strtotime_depart_date ) . '</h5>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3>Trip Code</h3>
                            <h5>' . date( "d M Y", $strtotime_return_date ) . '</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <h3>' . get_label_string( 'Itenerary', true ) . '</h3>
                            <h5>' . $trip . '</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 itenerary">
                            <h4 class="duration">' . $day . 'D ' . $night . 'N</h4>
                            <h4 class="return" data-value="' . date( "d M Y", $strtotime_return_date ) . '">' . date( "d M Y", $strtotime_return_date ) . '</h4>
                        </div>
                        <div class="col-lg-6 col-md-6 itenerary">
                            <h4 class="mermaid-type" data-value="' . $d['boat_title'] . '">' . $d['boat_title'] . '</h4>
            ';

            if ( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) ):
                $html .= '<a href="' . $trip_link . '" class="disabled-inport" data-boat-id="' . $d['boat_id'] . '">' . get_label_string( 'Trip Details', true ) . '</a>';
            else:
                $html .= '<a href="' . $trip_link . '" class="trip-details" data-boat-id="' . $d['boat_id'] . '">' . get_label_string( 'Trip Details', true ) . '</a>';
            endif;

            $html .= '  </div>
                    </div>
                    <div class="row">
            ';

            if ( $slug != "mermaid-ii" ):
                if ( $price_master == 0 || empty( $price_master ) ):
                    $html .= '<div class="col-lg-6 col-md-6">-</div>';
                else:
                    $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_master . '</p>' : '';

                    $html .= '
                                <div class="col-lg-6 col-md-6 cabins-type">
                                    <label>Master</label>
                                    <input type="radio" name="type-m-mermaid-' . $i . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="master" id="master-m-' . $i . '" data-price="' . $price_master . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-master"><label for="master-' . $i . '" class="price-mobile">€ ' . $price_master . '</label>
                                    ' . $roles_agent_html . '
                                </div>';
                endif;

            if ( $price_single == 0 || empty( $price_single ) ):
                $html .= '<div class="col-lg-6 col-md-6">-</div>';
            else:

                $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_single . '</p>' : '';

                $html .= '
                                <div class="col-lg-6 col-md-6 cabins-type">
                                    <label>Single</label>
                                    <input type="radio" name="type-m-mermaid-' . $i . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="single" id="single-m-' . $i . '" data-price="' . $price_single . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-single"><label for="single-' . $i . '" class="price-mobile">€ ' . $price_single . '</label>
                                    ' . $roles_agent_html . '
                                </div>';
            endif;

            endif;

            $html .= '  </div>
                        <div class="row">';

            if ( $price_deluxe == 0 || empty( $price_deluxe ) ):
                $html .= '<div class="col-lg-6 col-md-6">-</div>';
            else:
                $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_deluxe . '</p>' : '';

                $text_label_price = $slug != "mermaid-ii" ? 'Deluxe' : 'Deluxe Cabin';

                $html .= '
                            <div class="col-lg-6 col-md-6 cabins-type">
                                <label>' . $text_label_price . '</label>
                                <input type="radio" name="type-m-mermaid-' . $i . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="deluxe" id="deluxe-m-' . $i . '" data-price="' . $price_deluxe . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-deluxe"><label for="deluxe-' . $i . '" class="price-mobile">€ ' . $price_deluxe . '</label>
                                ' . $roles_agent_html . '
                            </div>';
            endif;

            if ( $price_lower == 0 || empty( $price_lower ) ):
                $html .= '<div class="col-lg-6 col-md-6">-</div>';
            else:
                $roles_agent_html = ( $roles_agent ) ? '<p class="available">Available: ' . $allotment_lower . '</p>' : '';

                $text_label_price = $slug != "mermaid-ii" ? 'Lower Deck' : 'Budget Cabin';
                $html .= '
                            <div class="col-lg-6 col-md-6 cabins-type">
                                <label>' . $text_label_price . '</label>
                                <input type="radio" name="type-m-mermaid-' . $i . '" data-key="' . $post_id . '" data-sanitize="' . $sanitize_title . '" data-type="lower" id="lower-m-' . $i . '" data-price="' . $price_lower . '" data-currency-code="EUR" class="m-price-choose radio-mobile radio-lower"><label for="lower-' . $i . '" class="price-mobile">€ ' . $price_lower . '</label>
                                ' . $roles_agent_html . '
                            </div>';
            endif;

            $html .= '  </div>
                        <div class="row">
            ';

            $html .= '<div class="col-lg-12 col-md-12 status">';

            if ( empty( $price_deluxe ) && empty( $price_lower ) && empty( $price_master ) && empty( $price_single ) ):
                $html .= '<a href="" class="disabled-inport">' . get_label_string( 'Book Now', true ) . '</a>';
            else:
                $html .= '<a href="" class="disabled">' . get_label_string( 'Book Now', true ) . '</a>';
            endif;

            $html .= '</div>';


            $html .= '
                    </div>
                </div>
            </div>
            ';

            if ( $i % 2 == 1 ):
                $html .= '
                </div>
                <div class="row">
                ';
            endif;
            $i++;
        endforeach;
    $html .= '
            </div>
        </div>
        ';
    endif;

    return $html;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk set meta query itenerary search
| -------------------------------------------------------------------------------------
*/
function meta_query_year_month_search( $prefix, $GET, $empty = false )
{
    $meta_query_year = array();

    if ( !empty( $GET['month'] ) && !empty( $GET['year'] ) ):
        $month_search = $GET['month'];
        $year_search  = $GET['year'];

        if ( $month_search == "all" && $year_search == "all" ):
            $meta_query_year = array();
        elseif ( $month_search != "all" && $year_search != "all" ):
            $date  = date_parse( $month_search );
            $month = $date['month'];
            $month = $month < 10 ? '0' . $month : $month;
            $year  = $year_search;

            if ( $empty ):
                $year_month      = $year . "-" . $month . "-" . "01";
                $meta_query_year = array(
                    array(
                        'key'     => $prefix . 'departure_date',
                        'value'   => $year_month,
                        'compare' => '>=',
                    )
                );
            else:
                $year_month      = $year . "-" . $month;
                $meta_query_year = array(
                    array(
                        'key'     => $prefix . 'departure_date',
                        'value'   => $year_month,
                        'compare' => 'LIKE',
                    )
                );
            endif;


    else:
        $date  = date_parse( $month_search );
        $month = $date['month'];
        $month = $month < 10 ? '0' . $month : $month;

        if ( $month_search == "all" ):
            $meta_query_year = array(
                array(
                    'key'     => $prefix . 'departure_date',
                    'value'   => $year_search . '-',
                    'compare' => 'LIKE',
                )
            );
        elseif ( $year_search == "all" ):
            $meta_query_year = array(
                array(
                    'key'     => $prefix . 'departure_date',
                    'value'   => '-' . $month . '-',
                    'compare' => 'LIKE',
                )
            );
        endif;
    endif;
    endif;

    return $meta_query_year;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk set meta query itenerary search
| -------------------------------------------------------------------------------------
*/
function meta_query_itenerary_search( $prefix )
{
    global $iten_data_search;

    if ( isset( $_GET['dest'] ) && empty( $_GET['dest'] ) === false )
    {
        $itenerary_search_id = explode( ',', $_GET['dest'] );
        $meta_query_relation = array( 'relation' => 'or' );
        $meta_query_argument = array();

        if ( empty( $iten_data_search ) === false )
        {
            foreach ( $iten_data_search as $d )
            {
                if ( in_array( $d->ID, $itenerary_search_id ) )
                {
                    $meta_query_argument[] = array(
                        'key'     => $prefix . 'iteneraries',
                        'value'   => $d->ID,
                        'compare' => '='
                    );
                }
            }
        }

        return array_merge( $meta_query_relation, $meta_query_argument );
    }
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan alert inport
| -------------------------------------------------------------------------------------
*/
function alert_inport()
{
    $schedule_ratest_inport_alert = get_option( 'schedule_ratest_inport_alert' );
    $title_rates                  = "";
    $des_rates                    = "";
    $html                         = "";
    if ( !empty( $schedule_ratest_inport_alert ) ):
        $schedule_ratest_inport_alert = json_decode( $schedule_ratest_inport_alert );

        $title_rates = $schedule_ratest_inport_alert->title;
        $title_rates = !empty( $title_rates ) ? "<h3>$title_rates</h3>" : '';
        $des_rates   = $schedule_ratest_inport_alert->description;
        $des_rates   = !empty( $des_rates ) ? "<p>$des_rates</p>" : '';

        $html = '
        <div class="wrap-alert-inport">
            <div class="container-popup-notification flex flex-center">
                <div class="message">
                    ' . $title_rates . '
                    ' . $des_rates . '
                </div>
                <span class="close-popup"></span>
            </div>
        </div>
        ';
    endif;

    return $html;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk mendapatkan currency data
| -------------------------------------------------------------------------------------
*/
function get_currency_data( $add_class = '' )
{
    $prefix = "_currency_";
    $posts  = get_posts(
        array(
            'posts_per_page' => -1,
            'post_type'      => 'currency',
            'order'          => 'ASC',
            'orderby'        => 'meta_value',
            'meta_key'       => $prefix . 'code',
        )
    );

    $currency_featured = array();
    $all_currency      = array();
    $html              = "";

    if ( count( $posts ) > 0 ):
        foreach ( $posts as $d ):
            $post_title      = $d->post_title;
            $post_id         = $d->ID;
            $code_currency   = get_post_meta( $post_id, $prefix . 'code', true );
            $symbol_currency = get_post_meta( $post_id, $prefix . 'symbol', true );
            $featured        = get_post_meta( $post_id, $prefix . 'featured', true );

            if ( $featured && count( $currency_featured ) <= 4 ):
                $currency_featured[] = array(
                    'id'     => $post_id,
                    'title'  => $post_title,
                    'code'   => $code_currency,
                    'symbol' => $symbol_currency
                );
            endif;

            $all_currency[] = array(
                'id'     => $post_id,
                'title'  => $post_title,
                'code'   => $code_currency,
                'symbol' => $symbol_currency
            );

        endforeach;

        $html .= '
            <div class="wrap-list-currency shadow-default">
                <div class="container-list-currency-featured ' . $add_class . '">
                    <div class="row">
        ';
        foreach ( $currency_featured as $d ):
            $html .= '<div class="col-lg-3 col-md-6 list-curr list-currency-featured ' . $d['code'] . '" data-currency-symbol="' . $d['symbol'] . '" data-currency-code="' . $d['code'] . '"><span>' . $d['code'] . '</span> ' . $d['title'] . '</div>';
        endforeach;

        $html .= '
                    </div>
                </div>
                <div class="container-list-currency-all ' . $add_class . '">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 desktop">
        ';
        $html .= set_all_currency_list( $all_currency, 'desktop' );
        $html .= '
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 mobile">
        ';
        $html .= set_all_currency_list( $all_currency, 'mobile' );
        $html .= '
                        </div>
                    </div>
                </div>
                <div class="info-box">
                    <p>' . get_option( 'info_box_currency' ) . '</p>
                </div>
            </div>
        ';
    endif;

    return $html;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk set all cureency lis html
| -------------------------------------------------------------------------------------
*/
function set_all_currency_list( $all_currency, $screen = 'desktop' )
{
    $i       = 1;
    $html    = "<ul>";
    $count   = count( $all_currency );
    $row_mob = round( $count / 2 );

    foreach ( $all_currency as $d ):
        $html .= '
            <li class="list-currency-all list-curr ' . $d['code'] . '" data-currency-symbol="' . $d['symbol'] . '" data-currency-code="' . $d['code'] . '"><span>' . $d['code'] . '</span> ' . $d['title'] . '</li>
        ';

        if ( $screen == "desktop" ):
            if ( $i % 5 == 0 ):
                $html .= '</ul></div><div class="col-lg-3 col-md-6 ' . $screen . '"><ul>';
            endif;
        else:
            if ( $i % $row_mob == 0 ):
                $html .= '</ul></div><div class="col-lg-6 col-md-6 ' . $screen . '"><ul>';
            endif;
        endif;

        $i++;
    endforeach;
    $html .= "</ul>";

    return $html;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk get iteneraries list
| -------------------------------------------------------------------------------------
*/
function get_iteneraries_list( $dest_id )
{
    // GET ITENERARIES DATA
    $args = array(
        'post_status' => 'publish',
        'post_type'   => 'iteneraries',
        'meta_query'  => array(
            array(
                'key' => '_iteneraries_destination',
                // 'value'     => $dest_id,
                // 'compare'   => 'LIKE'
                'value'   => '\;i\:' . $dest_id . '\;|\"' . $dest_id . '\";',
                'compare' => 'REGEXP'
            )
        )
    );
    $query = new WP_Query( $args );

    $data = array();
    while ( $query->have_posts() ):
        $query->the_post();

        $data[] = array(
            'id'    => get_the_ID(),
            'title' => get_the_title()
        );
    endwhile;
    wp_reset_postdata();

    return $data;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk get iteneraries list
| -------------------------------------------------------------------------------------
*/
function get_iteneraries_list_by_id( $iten_id )
{
    // GET ITENERARIES DATA
    $iten_id = explode( ',', $iten_id );

    $query = new WP_Query( array(
        'post__in'    => $iten_id,
        'post_status' => 'publish',
        'post_type'   => 'iteneraries'
    ) );

    $data = array();

    while ( $query->have_posts() )
    {
        $query->the_post();

        $data[] = array(
            'id'    => get_the_ID(),
            'title' => get_the_title()
        );
    }

    wp_reset_postdata();

    return $data;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk get social media
| -------------------------------------------------------------------------------------
*/
function get_social_media()
{
    $social_media   = get_option( 'social_media' );
    $facebook_link  = "";
    $instagram_link = "";
    $twitter_link   = "";
    $youtube_link   = "";

    if ( !empty( $social_media ) ):
        $social_media   = json_decode( $social_media );
        $facebook_link  = $social_media->facebook_link;
        $instagram_link = $social_media->instagram_link;
        $twitter_link   = $social_media->twitter_link;
        $youtube_link   = $social_media->youtube_link;
    endif;

    ?>
    <div class="container-social-media">
        <?php
        if ( !empty( $facebook_link ) ):
            echo '<a href="' . $facebook_link . '" target="_blank" class="facebook"><img src="' . THEME_URL_ASSETS . '/images/icon-facebook.svg" /></a>';
        endif;

    if ( !empty( $instagram_link ) ):
        echo '<a href="' . $instagram_link . '" target="_blank" class="instagram"><img src="' . THEME_URL_ASSETS . '/images/icon-instagram.svg" /></a>';
    endif;

    if ( !empty( $twitter_link ) ):
        echo '<a href="' . $twitter_link . '" target="_blank" class="twitter"><img src="' . THEME_URL_ASSETS . '/images/icon-twitter.svg" /></a>';
    endif;

    if ( !empty( $youtube_link ) ):
        echo '<a href="' . $youtube_link . '" target="_blank" class="youtube"><img src="' . THEME_URL_ASSETS . '/images/icon-youtube.svg" /></a>';
    endif;
    ?>
    </div>
    <?php
}


/*
| -------------------------------------------------------------------------------------
| Function untuk cek url
| -------------------------------------------------------------------------------------
*/
function cek_url()
{
    global $wp;
    $uri = home_url( $wp->request );
    $ex  = explode( '/', $uri );
    return $ex;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN V CODE YOUTUBE LINK
| -----------------------------------------------------------------------------
*/
function get_youtube_video_ID( $url )
{
    $url_string = parse_url( $url, PHP_URL_QUERY );
    parse_str( $url_string, $args );
    return isset( $args['v'] ) ? $args['v'] : false;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT YOUTUBE IFRAME
| -----------------------------------------------------------------------------
*/
function get_iframe_youtube( $url )
{
    $youtube_id = get_youtube_video_ID( $url );

    $iframe = '
        <iframe width="500" height="294" src="https://www.youtube.com/embed/' . $youtube_id . '?&theme=dark&autohide=2&modestbranding=1&fs=0&showinfo=0&iv_load_policy=3" frameborder="0"></iframe>
    ';

    // $iframe = '
    //     <iframe id="ytplayer" type="text/html" width="420" height="345" src="https://www.youtube.com/embed/'.$youtube_id.'?&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
    // ';

    return $iframe;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK ADD SHORTCODE THANK YOU PAGE
| -----------------------------------------------------------------------------
*/
add_shortcode( 'detail-inquiry-text', 'shortcode_thankyou_page' );
function shortcode_thankyou_page()
{
    if ( isset( $_GET['boat'] ) && isset( $_GET['date'] ) && isset( $_GET['iteneraries'] ) && isset( $_GET['cabin'] ) ):
        return ucwords( $_GET['boat'] ) . ' on ' . date( "d M Y", strtotime( $_GET['date'] ) ) . ' for ' . $_GET['iteneraries'] . ' in our ' . $_GET['cabin'];
    endif;

    return '';
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK ADD SHORTCODE BUTTON PAGE HOME IN THANK YOU PAGE
| -----------------------------------------------------------------------------
*/
add_shortcode( 'button-back-home', 'shortcode_button_back_home' );
function shortcode_button_back_home()
{
    return '<a href="' . get_site_url() . '" class="btn-book-now">Back To Home</a>';
}


/*
| -------------------------------------------------------------------------------------
| Function untuk currency converter
| -------------------------------------------------------------------------------------
*/
function convertCurrency( $from_currency = 'EUR', $to_currency, $amount = '1' )
{
    $response = wp_remote_request( 'https://xecd-api-ssl.xe.com/v1/convert_from.json', array(
        'headers' => array(
            'Authorization' => 'Basic ' . base64_encode( 'ptlumonata963225277:e9215pt6kmvbql93gpvvo' )
        ),
        'body' => array(
            'from'   => $from_currency,
            'to'     => $to_currency,
            'amount' => $amount
        )
    ) );

    $body = wp_remote_retrieve_body( $response );
    $data = json_decode( $body, true );

    if ( isset( $data['to'][0]['mid'] ) )
    {
        return $data['to'][0]['mid'];
    }
    else
    {
        return 0;
    }
}

/*
| -------------------------------------------------------------------------------------
| Function untuk get copyright
| -------------------------------------------------------------------------------------
*/
function get_copyright()
{
    echo '<p class="copyright">Copyright &copy;' . date( "Y" ) . ' by Mermaid Liveaboards - All rights reserved.</p>';
}


/*
| -------------------------------------------------------------------------------------
| Function untuk forgot password
| -------------------------------------------------------------------------------------
*/
add_action( 'login_form_lostpassword', array( '', 'do_password_lost' ) );
function do_password_lost()
{
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] ):
        $errors = retrieve_password();
        if ( is_wp_error( $errors ) ):
            // Errors found
            $redirect_url = home_url( 'forgot-password' );
            $redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
        else:
            // Email sent
            $redirect_url = home_url( 'agent-login' );
            $redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
        endif;

    wp_redirect( $redirect_url );
    exit;
    endif;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk custom logout
| -------------------------------------------------------------------------------------
*/
function custom_logout()
{
    wp_destroy_current_session();
    wp_clear_auth_cookie();
    // do_action( 'wp_logout' );
}


/*
| -------------------------------------------------------------------------------------
| Function untuk redirect ke login failed jika username dan password salah
| -------------------------------------------------------------------------------------
*/
add_action( 'wp_login_failed', 'my_front_end_login_fail' );
function my_front_end_login_fail( $username )
{
    $referrer = $_SERVER['HTTP_REFERER'];

    if ( !empty( $referrer ) && !strstr( $referrer, 'wp-login' ) && !strstr( $referrer, 'wp-admin' ) ):
        wp_redirect( site_url() . '/agent-login' . '?login=failed' );
        exit;
    endif;
}


/*
| -------------------------------------------------------------------------------------
| Function hook login redirect
| -------------------------------------------------------------------------------------
*/
function my_login_redirect( $redirect_to, $request, $user )
{

    global $user;
    $page = isset( $_POST['page-login'] ) ? $_POST['page-login'] : '';
    if ( $page == "agent-login" ):
        if ( isset( $user->roles ) && is_array( $user->roles ) ):
            if ( in_array( 'agent', $user->roles ) ):
                $redirect = $_POST['redirect_to'];
            else:
                $redirect = site_url() . '/agent-login/?login=must-agent';
                custom_logout();
            endif;
        return $redirect;
    endif;
    else:
        $redirect = site_url() . '/wp-admin';
        if ( isset( $user->roles ) && is_array( $user->roles ) ):
            if ( in_array( 'agent', $user->roles ) ):
                $redirect = site_url() . '/schedule-rates/mermaid-i/';
            endif;
            return $redirect;
        endif;
    return $redirect;
    endif;
}
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );


/*
| -------------------------------------------------------------------------------------
| Function untuk send email payment success
| -------------------------------------------------------------------------------------
*/
function send_email_payment_success( $type, $args = array() )
{
    $contacts = get_option( 'contact_information' );

    if ( empty( $contacts ) === false )
    {
        $contacts = json_decode( $contacts );
        $email_to = $contacts->email_payment_success;

        if ( empty( $email_to ) === false )
        {
            $headers = array( 'Content-Type: text/html; charset=UTF-8' );
            $subject = sprintf( 'Accepted Payment Via %s', ucwords( $type ) );

            $message = '
            <table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto; width: 700px; font-family: Arial, Helvetica, sans-serif;">
                <tr align="center">
                    <td>
                        <a href="' . site_url() . '" target="_blank">
                            <img src="' . THEME_URL_ASSETS . '/images/logo-mermaid.png" width="150" alt="Mermaid Liveaboard" style="width: 150px;" />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #DDDDDD; opacity: 0.5; padding-top: 30px;"></td>
                </tr>
                <tr align="center">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; width: 500px;">';

            if ( $type == 'paypal' )
            {
                $note = get_option( 'message_paypal_success' );
            }
            elseif ( $type == 'stripe' )
            {
                $note = get_option( 'message_stripe_success' );
            }
            elseif ( $type == 'airwallex' )
            {
                $note = get_option( 'message_airwallex_success' );
            }

            if ( $note != '' )
            {
                $message .= '
                                <tr>
                                    <td style="font-size: 16px; color: #001B48; font-weight: 700; padding:15px;">
                                        ' . nl2br( $note ) . '
                                    </td>
                                </tr>';
            }

            $message .= '
                            <tr>
                                <td style="padding-top: 20px;">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; border: 1px solid #E6E9F7;">
                                        <tr>
                                            <td style="background-color: #F4F8FB; height: 50px; padding: 0 15px;"><h4 style="line-height: 50px; font-size: 16px; font-weight: 700; color: #001A47; opacity: 0.5; margin: 0; padding: 0;">Payment Detail</h4></td>
                                        </tr>';

            if ( isset( $args['invoice_id'] ) )
            {
                $message .= '
                                            <tr>
                                                <td style="padding: 15px 15px 0;">
                                                    <p style="margin: 0 0 10px 0; padding: 0; font-size: 16px; color: #525252; opacity: 0.5;">Invoice ID</p>
                                                    <p style="margin: 0; padding: 0 0 5px 0; font-size: 18px; color: #525252; border-bottom: 1px solid #DDDDDD;">' . $args['invoice_id'] . '</p>
                                                </td>
                                            </tr>';
            }

            if ( isset( $args['transaction_id'] ) )
            {
                $message .= '
                                            <tr>
                                                <td style="padding: 15px 15px 0;">
                                                    <p style="margin: 0 0 10px 0; padding: 0; font-size: 16px; color: #525252; opacity: 0.5;">Transaction ID</p>
                                                    <p style="margin: 0; padding: 0 0 5px 0; font-size: 18px; color: #525252; border-bottom: 1px solid #DDDDDD;">' . $args['transaction_id'] . '</p>
                                                </td>
                                            </tr>';
            }

            if ( isset( $args['payer_email'] ) )
            {
                $message .= '
                                            <tr>
                                                <td style="padding: 15px 15px 0;">
                                                    <p style="margin: 0 0 10px 0; padding: 0; font-size: 16px; color: #525252; opacity: 0.5;">Payer Email</p>
                                                    <p style="margin: 0; padding: 0 0 5px 0; font-size: 18px; color: #525252; border-bottom: 1px solid #DDDDDD;">' . $args['payer_email'] . '</p>
                                                </td>
                                            </tr>';
            }

            if ( isset( $args['payer_id'] ) )
            {
                $message .= '
                                            <tr>
                                                <td style="padding: 15px 15px 0;">
                                                    <p style="margin: 0 0 10px 0; padding: 0; font-size: 16px; color: #525252; opacity: 0.5;">Payer ID</p>
                                                    <p style="margin: 0; padding: 0 0 5px 0; font-size: 18px; color: #525252; border-bottom: 1px solid #DDDDDD;">' . $args['payer_id'] . '</p>
                                                </td>
                                            </tr>';
            }

            if ( isset( $args['payment_id'] ) )
            {
                $message .= '
                                            <tr>
                                                <td style="padding: 15px 15px 0;">
                                                    <p style="margin: 0 0 10px 0; padding: 0; font-size: 16px; color: #525252; opacity: 0.5;">Payment ID</p>
                                                    <p style="margin: 0; padding: 0 0 5px 0; font-size: 18px; color: #525252; border-bottom: 1px solid #DDDDDD;">' . $args['payment_id'] . '</p>
                                                </td>
                                            </tr>';
            }

            $message .= '
                                        <tr>
                                            <td style="padding: 15px 15px;">
                                                <p style="margin: 0 0 10px 0; padding: 0; font-size: 16px; color: #525252; opacity: 0.5;">Amount Pay</p>
                                                <p style="margin: 0; padding: 0 0 5px 0; font-size: 18px; color: #525252;">' . $args['currency'] . ' ' . $args['amount_pay'] . '</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 16px; color: #B28500; line-height: 25px; padding: 15px 15px 0px;">
                                    <i style="margin: 0; padding: 0;">Regards</i>
                                    <p style="font-size: 16px; font-weight: bold; margin: 0; padding: 0;">Mermaid Liveaboards Team</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #DDDDDD; opacity: 0.5; padding-top: 30px;"></td>
                </tr>
                <tr align="center">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 500px; margin: 0 auto; padding: 10px 30px;">
                            <tr>
                                <td width="100px;"><a href="' . site_url( 'contact-us' ) . '" target="_blank" style="text-decoration: underline; font-size: 14px; color: #525252;">Contact Us</a></td>
                                <td width="70px;"><a href="' . site_url( 'frequently-asked-questions' ) . '" target="_blank" style="text-decoration: underline; font-size: 14px; color: #525252;">FAQS</a></td>
                                <td><a href="' . site_url( 'booking-policy' ) . '" target="_blank" style="text-decoration: underline; font-size: 14px; color: #525252;">Booking Policy</a></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding-top: 10px; font-size: 12px; color: #525252;">&copy;Copyright ' . date( 'Y' ) . ' by Mermaid Liveaboards - All right reserved.</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>';

            wp_mail( $email_to, $subject, $message, $headers );
        }
    }
}


/*
| -------------------------------------------------------------------------------------
| Function untuk send email payment success
| -------------------------------------------------------------------------------------
*/
function is_category_parent( $name, $page = '', $category_id = '' )
{
    if( $page == 'single' )
    {
        $parent_data = get_category( $category_id );
    }
    else
    {
        $queried_object = get_queried_object();

        if( isset( $queried_object->category_parent ) )
        {
            $parent_data = get_category( $queried_object->category_parent );
        }
    }

    if ( isset( $parent_data->slug ) )
    {
        if ( $parent_data->slug == $name )
        {
            return true;
        }
    }

    return false;
}



/*
| -------------------------------------------------------------------------------------
| Function untuk check agent login
| -------------------------------------------------------------------------------------
*/
function is_agent_logged_in()
{
    $roles_agent = 0;
    $agent_email = "";
    if ( is_user_logged_in() ):
        $current_user = wp_get_current_user();
        $user_login   = $current_user->user_login;
        $user_roles   = $current_user->roles;
        $roles        = $user_roles[0];
        $roles_agent  = $roles == "agent" ? 1 : 0;
    endif;
    return $roles_agent;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan information pack di atas table schedule
| -------------------------------------------------------------------------------------
*/
function show_information_pack_schedule()
{
    // CHECK AGENT LOGIN
    if ( is_agent_logged_in() ):
        $information_pack = get_page_by_path( 'agent-information-pack' );
        if ( !empty( $information_pack ) ):
            $pack_id      = $information_pack->ID;
            $title_pack   = $information_pack->post_title;
            $title_pack   = empty( $title_pack ) ? '' : '<h1 class="heading-default">' . $title_pack . '</h1>';
            $content_pack = $information_pack->post_content;
            $content_pack = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $content_pack );
            $content_pack = apply_filters( 'the_content', $content_pack );
            $content_pack = str_replace( ']]>', ']]&gt;', $content_pack );
            $content_pack = empty( $content_pack ) ? '' : '<div class="desc-about">' . $content_pack . '</div>';
            ?>
            <section class="welcome-about welcome-mermaid wrap-information-pack">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo $title_pack; ?>
                            <?php echo $content_pack; ?>
                        </div>
                    </div>
                    <div class="row file-guest-information">
                        <?php
                                $title_mermaid_i = get_post_meta( $pack_id, '_page_mermaid_i_title', true );
            $file_mermaid_i                      = get_post_meta( $pack_id, '_page_file_mermaid_i', true );
            $title_mermaid_ii                    = get_post_meta( $pack_id, '_page_mermaid_ii_title', true );
            $file_mermaid_ii                     = get_post_meta( $pack_id, '_page_file_mermaid_ii', true );

            if ( !empty( $file_mermaid_i ) && filter_var( $file_mermaid_i, FILTER_VALIDATE_URL ) !== false ):
                echo '
                            <div class="col-lg-6 col-md-12">
                                <a href="' . $file_mermaid_i . '" target="_blank">
                                    <h2>' . $title_mermaid_i . '<br/> ' . $information_pack->post_title . '</h2>
                                </a>
                            </div>
                            ';
            endif;

            if ( !empty( $file_mermaid_ii ) && filter_var( $file_mermaid_ii, FILTER_VALIDATE_URL ) !== false ):
                echo '
                            <div class="col-lg-6 col-md-12">
                                <a href="' . $file_mermaid_ii . '" target="_blank">
                                    <h2>' . $title_mermaid_ii . '<br/> ' . $information_pack->post_title . '</h2>
                                </a>
                            </div>
                            ';
            endif;
            ?>

                    </div>
                </div>
            </section>
    <?php
        endif;
    endif;
}

add_action( 'wp_footer', 'wpcf7_event' );

function wpcf7_event()
{
    ?>
    <script type="text/javascript">
        document.addEventListener('wpcf7mailsent', function(event) {
            window.dataLayer = window.dataLayer || [];

            if (jQuery('#' + event.detail.id).find('[type=checkbox]:checked').length > 0) {
                var prm = new Array;
                var code = '';
                var sum = 0;

                jQuery('#' + event.detail.id).find('[type=checkbox]:checked').each(function() {
                    var obj = new Object;
                    obj.quantity = jQuery('#' + event.detail.id + ' [name=person_count_' + jQuery(this).data('type') + ']').val();
                    obj.price = jQuery(this).data('price');
                    obj.name = jQuery(this).data('name');
                    obj.id = jQuery(this).data('id');

                    prm.push(obj);

                    sum = sum + (parseFloat(obj.price) * parseFloat(obj.quantity));
                    code = jQuery(this).data('currencyCode');
                });

                var act = new Object;
                act.id = jQuery('#' + event.detail.id).find('[name=trip_code]').data('id');
                act.affiliation = jQuery('#' + event.detail.id).find('[name=itenerary]').val();
                act.revenue = sum;
                act.coupon = '';
                act.shipping = 0;
                act.tax = 0;

                window.dataLayer.push({
                    ecommerce: {
                        currencyCode: code,
                        purchase: {
                            actionField: act,
                            products: prm
                        }
                    }
                });

                console.log(window.dataLayer);
            }

        }, false);
    </script>
<?php
}

//-- Define the wpcf7_submit callback
if ( !function_exists( 'action_wpcf7_submit' ) )
{
    function action_wpcf7_submit( $instance, $result )
    {
        if ( isset( $result['status'] ) && $result['status'] == 'mail_sent' )
        {
            $submission = WPCF7_Submission::get_instance();

            if ( $submission )
            {
                $post = $submission->get_posted_data();
                $trip = get_page_by_title( $post['trip_code'], OBJECT, 'schedule_data' );

                if ( !empty( $trip ) )
                {
                    //-- Add trip meta to stack
                    $trip_meta       = get_post_meta( $trip->ID );
                    $parent_meta     = get_post_meta( $trip->post_parent );
                    $trip->post_meta = array_merge( $trip_meta, $parent_meta );

                    $itenerary     = str_replace( '–', '-', $post['itenerary'] );
                    $cruise_detail = $itenerary . ' ' . $post['trip_start'];
                    $cruise_list   = array();

                    $user = wp_get_current_user();

                    if ( $user->exists() && in_array( 'agent', $user->roles ) )
                    {
                        $book_type     = '1';
                        $agent_address = get_user_meta( $user->data->ID, '_user_address', true );
                        $agent_name    = $user->data->ID;
                    }
                    else
                    {
                        $book_type     = '2';
                        $agent_address = '';
                        $agent_name    = '';
                    }

                    $book_status = 'Check Availability';

                    foreach ( $post['cabin_type'] as $cabin )
                    {
                        if ( $cabin == 'Master' )
                        {
                            $disc     = $trip->post_meta['_schedule_data_price_master_dsc'][0];
                            $gross    = $trip->post_meta['_schedule_data_price_master'][0];
                            $no_guest = $post['person_count_master'];
                        }
                        elseif ( $cabin == 'Single' )
                        {
                            $disc     = $trip->post_meta['_schedule_data_price_single_dsc'][0];
                            $gross    = $trip->post_meta['_schedule_data_price_single'][0];
                            $no_guest = $post['person_count_single'];
                        }
                        elseif ( $cabin == 'Deluxe' )
                        {
                            $disc     = $trip->post_meta['_schedule_data_price_deluxe_dsc'][0];
                            $gross    = $trip->post_meta['_schedule_data_price_deluxe'][0];
                            $no_guest = $post['person_count_deluxe'];
                        }
                        elseif ( $cabin == 'Budget' )
                        {
                            $disc     = $trip->post_meta['_schedule_data_price_lower_dsc'][0];
                            $gross    = $trip->post_meta['_schedule_data_price_lower'][0];
                            $no_guest = $post['person_count_lower'];
                        }

                        $nett  = $gross - ( ( $gross * $disc ) / 100 );
                        $tnett = $nett * $no_guest;

                        $cruise_list[] = array(
                            'cruise_details'    => $cruise_detail,
                            'no_guest'          => $no_guest,
                            'cabin_type'        => $cabin,
                            'gross'             => $gross,
                            'repeat_guest_disc' => $disc,
                            'nett'              => $nett,
                            'total_nett'        => $tnett,
                        );
                    }

                    //-- Insert the post into the database.
                    $post_id = wp_insert_post( array(
                        'post_title'  => sprintf( '%s %s', $trip->post_title, $post['fullname'] ),
                        'post_type'   => 'checkout-2',
                        'post_status' => 'publish',
                        'post_author' => 1,
                        'meta_input'  => array(
                            '_checkout_arrival_point'   => $trip->post_meta['_schedule_data_arrival_point'][0],
                            '_checkout_departure_point' => $trip->post_meta['_schedule_data_depart_point'][0],
                            '_checkout_arrival_time'    => $trip->post_meta['_schedule_data_arrival_time'][0],
                            '_checkout_departure_time'  => $trip->post_meta['_schedule_data_depart_time'][0],
                            '_checkout_iteneraries'     => $trip->post_meta['_schedule_data_iteneraries'][0],
                            '_checkout_boat'            => $trip->post_meta['_schedule_rates_boat'][0],
                            '_checkout_trip_year'       => $trip->post_meta['_schedule_rates_year'][0],
                            '_checkout_trip_code'       => $trip->post_title,
                            '_checkout_remarks'         => $post['additional_data'],
                            '_checkout_country'         => $post['nationality'],
                            '_checkout_person_email'    => $post['your-email'],
                            '_checkout_person_name'     => $post['fullname'],
                            '_checkout_person_phone'    => $post['phone'],
                            '_checkout_person_gender'   => $post['sex'],
                            '_checkout_agent_address'   => $agent_address,
                            '_checkout_agent_name'      => $agent_name,
                            '_checkout_cruise_list'     => $cruise_list,
                            '_checkout_status_payment'  => $book_status,
                            '_checkout_booking_type'    => $book_type
                        )
                    ) );

                    // if( !is_wp_error( $post_id ) )
                    // {
                    //     wp_update_post( array(
                    //         'post_name' => base64_encode( json_encode( array( 'key' => md5( $post[ 'post_id' ] ) ) ) ),
                    //         'ID'        => $post_id
                    //     ));
                    // }
                }
            }
        }
    }

    add_action( 'wpcf7_submit', 'action_wpcf7_submit', 10, 2 );
}

/*
| -------------------------------------------------------------------------------------
| Function untuk mendapatkan data destination
| -------------------------------------------------------------------------------------
*/
if ( !function_exists( 'the_user_list' ) )
{
    function the_user_list( $role )
    {
        $users = get_users( array(
            'role'    => 'Agent',
            'orderby' => 'user_nicename',
            'order'   => 'ASC'
        ) );

        $data = array();

        foreach ( $users as $user )
        {
            $data[$user->ID] = $user->display_name;
        }

        return $data;
    }
}

add_filter( 'wpcf7_autop_or_not', '__return_false' );


/*
| -------------------------------------------------------------------------------------
| Function untuk menghapus add new link di submenu checkout form
| -------------------------------------------------------------------------------------
*/
if ( !function_exists( 'remove_add_new_link_from_submenus' ) )
{
    function remove_add_new_link_from_submenus()
    {
        global $submenu;

        if ( isset( $submenu['edit.php?post_type=checkout'][10] ) )
        {
            unset( $submenu['edit.php?post_type=checkout'][10] );
        }

        if ( isset( $submenu['edit.php?post_type=checkout-2'][10] ) )
        {
            unset( $submenu['edit.php?post_type=checkout-2'][10] );
        }
    }

    add_action( 'admin_menu', 'remove_add_new_link_from_submenus' );
}

if ( !function_exists( 'has_body_class' ) )
{
    function has_body_class( $class )
    {
        $classes = get_body_class();

        if ( in_array( $class, $classes ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

remove_action( 'wpcf7_swv_create_schema', 'wpcf7_swv_add_select_enum_rules', 20, 2 );
remove_action( 'wpcf7_swv_create_schema', 'wpcf7_swv_add_checkbox_enum_rules', 20, 2 );

/*
| -------------------------------------------------------------------------------------
| Function untuk menambahkan action batch download dsv
| -------------------------------------------------------------------------------------
*/
if ( !function_exists( 'register_custom_bulk_action' ) )
{
    function register_custom_bulk_action( $bulk_actions )
    {
        $bulk_actions['download_dsvs'] = __( 'Download DSV', '' );

        return $bulk_actions;
    }

    add_filter( 'bulk_actions-edit-schedule_rates', 'register_custom_bulk_action' );
}

if ( !function_exists( 'handle_custom_bulk_action' ) )
{
    function handle_custom_bulk_action( $redirect_to, $doaction, $post_ids )
    {
        if ( $doaction === 'download_dsvs' )
        {
            $redirect_to = add_query_arg( array( 'download_csv' => '1', 'post_id' => $post_ids ), $redirect_to );
        }

        return $redirect_to;
    }

    add_filter( 'handle_bulk_actions-edit-schedule_rates', 'handle_custom_bulk_action', 10, 3 );
}

if ( !function_exists( 'check_for_csv_download' ) )
{
    function check_for_csv_download()
    {
        if( isset( $_GET[ 'download_csv' ] ) && $_GET[ 'download_csv' ] == '1' )
        {
            if( isset( $_GET[ 'post_id' ] ) && !empty( $_GET[ 'post_id' ] ) )
            {
                require_once 'export-dsv.php';

                $mod = new batch_download_csv_file();

                $mod->render();
            }
        }
    }

    add_action( 'admin_init', 'check_for_csv_download' );
}

/*
| -------------------------------------------------------------------------------------
| Function untuk membatasi jumlah post yang ditampilkan dalam kategori post
| -------------------------------------------------------------------------------------
*/
if ( !function_exists( 'posts_for_current_category' ) )
{
    function posts_for_current_category( $query )
    {
        if ( is_category() )
        {
            if( isset( $query->query['category_name'] ) )
            {
                $limit = 8;

                if( isset( $query->query['paged'] ) )
                {
                    $offset = ( ( $query->query['paged'] - 1 ) * $limit ) + 2;
                }
                else
                {
                    $offset = 2;
                }

                $query->set( 'orderby', 'publish_date' );
                $query->set( 'posts_per_page', $limit );
                $query->set( 'orderby', 'DESC' );
                $query->set( 'offset', $offset );
            }
        }
    }

    add_filter( 'pre_get_posts', 'posts_for_current_category' );
}

/*
| -------------------------------------------------------------------------------------
| Function untuk mengijinkan custom type file to upload
| -------------------------------------------------------------------------------------
*/
if ( !function_exists( 'custom_extension_to_uploaded' ) )
{
    function custom_extension_to_uploaded( $upload_mimes )
    {
        $upload_mimes['dsv'] = 'text/csv';

        return $upload_mimes;
    }

    add_filter( 'upload_mimes', 'custom_extension_to_uploaded' );
}
