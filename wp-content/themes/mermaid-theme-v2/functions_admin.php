<?php

/*
| -------------------------------------------------------------------------------------
| Filter to Hide admin bar in frontend
| -------------------------------------------------------------------------------------
*/
add_filter('show_admin_bar', '__return_false');

if( !function_exists( 'ts_hiding_some_fields' ) )
{
    function ts_hiding_some_fields()
    {
        if( in_array( get_post_type(), array( 'checkout', 'checkout-2' ) ) || in_array( get_admin_page_title(), array( 'Bank Options', 'Stripe Setting' ) ) )
        {
            $version = isset( $version ) ? $version : '';

            wp_enqueue_style( 'checkout-styles', THEME_URL_ASSETS.'/css/admin/checkout_style.css'. $version );
        }
    }

    add_action('admin_head', 'ts_hiding_some_fields');
}


/*
| -------------------------------------------------------------------------------------
| Function untuk mengubah title text pada form admin
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'wpb_change_title_text' ) )
{
    function wpb_change_title_text( $title )
    {
        $screen = get_current_screen();

        if ( 'schedule_rates' == $screen->post_type )
        {
             $title = 'Enter trip code';
         }

        if ( 'checkout' == $screen->post_type || 'checkout-2' == $screen->post_type )
        {
            $title = 'Enter Invoice No.';
        }

        return $title;
    }

    add_filter( 'enter_title_here', 'wpb_change_title_text' );
}


/*
| -------------------------------------------------------------------------------------
| HOOK function ketika publish form checkout
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'update_checkout_hook' ) )
{
    function update_checkout_hook( $postid, $post )
    {
        global $table_prefix;
        global $wpdb;

        $pname = base64_encode( json_encode( array( 'key' => md5( $postid ) ) ) );
        $table = $table_prefix . 'posts';
        $ptype = $post->post_type;

        $wpdb->query( 'UPDATE ' . $table . ' SET post_name = "' . $pname . '" WHERE post_type = "' . $ptype . '" AND ID = ' . $postid );
    }

    // add_action( 'publish_checkout', 'update_checkout_hook', 10, 2 );
    // add_action( 'publish_checkout-2', 'update_checkout_hook', 10, 2 );
}


/*
| -------------------------------------------------------------------------------------
| HOOK function ketika publish post type schedule rates
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'update_schedule_data_hook' ) )
{
    function update_schedule_data_hook( $postid, $post )
    {
        global $wpdb;

        $prefix = "_schedule_data_";

        $boat_new = $_POST['_schedule_rates_boat'];

        // ==================== DELETE POST META WHEN SCHEDULE DATA HAVE POST TITLE EMPTY ====================
        $sql = "SELECT ID FROM mermaid_posts WHERE post_parent=$postid";
        $result = $wpdb->get_results($sql);
        
        foreach($result as $d):
            $id                 = $d->ID;
            $title              = get_the_title($id);
            $departure_date     = get_post_meta($id, $prefix.'departure_date', true);
            $arrival_date       = get_post_meta($id, $prefix.'arrival_date', true);
            $departure_time     = get_post_meta($id, $prefix.'depart_time', true);
            $arrival_time       = get_post_meta($id, $prefix.'arrival_time', true);
            $departure_point    = get_post_meta($id, $prefix.'depart_point', true);
            $arrival_point      = get_post_meta($id, $prefix.'arrival_point', true);
            $iteneraries        = get_post_meta($id, $prefix.'iteneraries', true);
            $no_dives           = get_post_meta($id, $prefix.'no_dives', true);
            $price_master       = get_post_meta($id, $prefix.'price_master', true);
            $allotment_master   = get_post_meta($id, $prefix.'allotment_master', true);
            $price_single       = get_post_meta($id, $prefix.'price_single', true);
            $allotment_single   = get_post_meta($id, $prefix.'allotment_single', true);
            $price_deluxe       = get_post_meta($id, $prefix.'price_deluxe', true);
            $allotment_deluxe   = get_post_meta($id, $prefix.'allotment_deluxe', true);
            $price_lower        = get_post_meta($id, $prefix.'price_lower', true);
            $allotment_lower    = get_post_meta($id, $prefix.'allotment_lower', true);

            if($boat_new == "213"):
                if($title == "" || $departure_date == "" || $arrival_date == "" || $iteneraries == "" || $no_dives == "" || $price_deluxe == "" || $allotment_deluxe == "" || $price_lower == "" || $allotment_lower == ""):
                    delete_post_meta( $id, $prefix.'departure_date');
                    delete_post_meta( $id, $prefix.'arrival_date');
                    delete_post_meta( $id, $prefix.'depart_time');
                    delete_post_meta( $id, $prefix.'arrival_time');
                    delete_post_meta( $id, $prefix.'depart_point');
                    delete_post_meta( $id, $prefix.'arrival_point');
                    delete_post_meta( $id, $prefix.'iteneraries');
                    delete_post_meta( $id, $prefix.'no_dives');
                    delete_post_meta( $id, $prefix.'price_master');
                    delete_post_meta( $id, $prefix.'allotment_master');
                    delete_post_meta( $id, $prefix.'price_single');
                    delete_post_meta( $id, $prefix.'allotment_single');
                    delete_post_meta( $id, $prefix.'price_deluxe');
                    delete_post_meta( $id, $prefix.'allotment_deluxe');
                    delete_post_meta( $id, $prefix.'price_lower');
                    delete_post_meta( $id, $prefix.'allotment_lower');
        
                    $wpdb->query("DELETE FROM mermaid_posts WHERE post_parent=$id");
                endif;
            else:
                if($title == "" || $departure_date == "" || $arrival_date == "" || $iteneraries == "" || $no_dives == "" || $price_master == "" || $allotment_master == "" || $price_single == "" || $allotment_single == "" || $price_deluxe == "" || $allotment_deluxe == "" || $price_lower == "" || $allotment_lower == ""):
                    delete_post_meta( $id, $prefix.'departure_date');
                    delete_post_meta( $id, $prefix.'arrival_date');
                    delete_post_meta( $id, $prefix.'depart_time');
                    delete_post_meta( $id, $prefix.'arrival_time');
                    delete_post_meta( $id, $prefix.'depart_point');
                    delete_post_meta( $id, $prefix.'arrival_point');
                    delete_post_meta( $id, $prefix.'iteneraries');
                    delete_post_meta( $id, $prefix.'no_dives');
                    delete_post_meta( $id, $prefix.'price_master');
                    delete_post_meta( $id, $prefix.'allotment_master');
                    delete_post_meta( $id, $prefix.'price_single');
                    delete_post_meta( $id, $prefix.'allotment_single');
                    delete_post_meta( $id, $prefix.'price_deluxe');
                    delete_post_meta( $id, $prefix.'allotment_deluxe');
                    delete_post_meta( $id, $prefix.'price_lower');
                    delete_post_meta( $id, $prefix.'allotment_lower');
        
                    $wpdb->query("DELETE FROM mermaid_posts WHERE post_parent=$id");
                endif;
            endif;
        endforeach;
        

        // ==================== UPDATE POST DATA BOAT ====================
        $sql = "SELECT ID FROM mermaid_posts WHERE post_parent=$postid AND post_title!=''";
        $result = $wpdb->get_results($sql);
        foreach($result as $d):
            $schedule_data_id = $d->ID;
            update_post_meta( $schedule_data_id, $prefix."boat", $boat_new );
        endforeach;

        $wpdb->query("UPDATE mermaid_posts SET post_status='publish' WHERE post_type='schedule_data' AND post_parent=$postid");
    }

    add_action( 'publish_schedule_rates', 'update_schedule_data_hook', 10, 2 );
}


/*
| -------------------------------------------------------------------------------------
| HOOK function ketika trash post type schedule rates
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'my_wp_trash_post' ) )
{
    function my_wp_trash_post($postid) 
    {
        global $wpdb;

        $post_type = get_post_type( $postid );
        $post_status = get_post_status( $postid );

        if( $post_type == 'schedule_rates' ):
            $wpdb->query("UPDATE mermaid_posts SET post_status='trash' WHERE post_type='schedule_data' AND post_parent=$postid");
        endif;
    }

    add_action('wp_trash_post', 'my_wp_trash_post');
}


/*
| -------------------------------------------------------------------------------------
| HOOK function ketika draft post
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'draft_post_hook' ) )
{
    function draft_post_hook( $postid, $post ) 
    {
        global $wpdb;

        $post_type = $post->post_type;

        if( $post_type == 'schedule_rates' )
        {
            $wpdb->query( "UPDATE mermaid_posts SET post_status='draft' WHERE post_type='schedule_data' AND post_parent=$postid" );
        }
    }

    add_action( 'draft_schedule_rates',  'draft_post_hook', 10, 2 );
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menambahkan link css pada halaman admin
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'admin_assets' ) )
{
    function admin_assets() 
    {
        global $version;

        $screen    = get_current_screen();
        $post_type = empty( $screen) ? '' : $screen->post_type;

        wp_enqueue_style( 'admin-styles', THEME_URL_ASSETS . '/css/admin/style.css' . $version );

        if( $post_type == 'destination' || $post_type == 'dive_spots' )
        {
            wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDFlVTwZqnOJhc3LsnE9pKHdgajhbB9S2U', null, true );
            wp_enqueue_script( 'location-picker', THEME_URL_ASSETS . '/js/admin/locationpicker.jquery.js', null, true );
        }

        wp_enqueue_script( 'sweet-alert', 'https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js', array( 'jquery' ),  null, true );
        wp_enqueue_script( 'script', THEME_URL_ASSETS . '/js/admin/script.js'.$version, array( 'jquery' ),  null, true );
    }

    add_action('admin_enqueue_scripts', 'admin_assets');
}

/*
| -------------------------------------------------------------------------------------
| Function untuk menambahkan class di tag body pada admin page
| -------------------------------------------------------------------------------------
*/
if( !function_exists( 'se_154951_add_admin_body_class' ) )
{
    function se_154951_add_admin_body_class( $classes ) 
    {
        $screen = get_current_screen();
        $nclass = '';
        
        if ( 'schedule_rates' == $screen->post_type )
        {
            $nclass = 'schedule_rates_body';
        }

        return $classes . ' ' . $nclass;
    }

    add_filter( 'admin_body_class', 'se_154951_add_admin_body_class' );
}