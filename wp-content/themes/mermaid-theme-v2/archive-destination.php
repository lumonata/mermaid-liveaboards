<?php get_header(); ?>

    <?php include_once "layout/hero.php"; ?>

    <section class="row homepage-content homepage-content-2">
        
        <!-- DISCOVER OUR DESTINATION -->
        <?php section_destinations_iteneraries('destination'); ?>
        
    </section>

    <section class="row homepage-content homepage-content-3">
        
        <!-- DIVING IN INDONESIA -->
        <?php section_diving_indonesia(); ?>

    </section>

<?php get_footer(); ?>