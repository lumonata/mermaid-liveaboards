        
    <?php if( !is_page( 'agent-login' ) && !is_page( 'forgot-password' ) && !is_404() ): ?>
    </div>
    <!-- END container-fluid -->

    <?php if( !isset( $_GET[ 'popup' ] ) ): ?>
    <footer>
        <div class="line-gradient"></div>
        <div class="container">

            <!-- LOGO & SUBSCRIBE DESCRIPTION -->
            <div class="row">
                <div class="col-md-12">
                    <div class="logo">
                        <a href="<?php echo get_site_url(); ?>"><img data-src-static="<?php echo get_template_directory_uri()."/assets/images/mermaid-logo.png" ?>" /></a>
                    </div>
                    <h2>INTERESTED IN MERMAID LIVEABOARD'S NEWS?</h2>
                    <h4>Subscribe here for latest news and deals</h4>
                </div>
            </div>
            
            <?php echo do_shortcode('[mc4wp_form id="3362"]');?>

            <?php if( !is_singular( 'checkout' ) || !is_singular( 'checkout-2' ) ): ?>
            <!-- MENU & CONTACT FOOTER -->
            <div class="row menu-contact">
                <div class="col-lg-9 col-md-8">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer_left_menu' ) ); ?>

                    <?php mermaid_footer_address_contact(); ?>
                </div>
                <div class="col-lg-3 col-md-4">                    
                    <?php wp_nav_menu( array( 'theme_location' => 'footer_right_menu' ) ); ?>
                </div>
            </div>

            <!-- SOCIAL MEDIA & PAYMENT -->
            <div class="row social-media-payment">
                <div class="col-lg-8 col-md-8">
                    <h4>Follow Us</h4>
                    <?php get_social_media(); ?>
                </div>
                <div class="col-lg-4 col-md-4">
                    <ul class="list-payment">
                        <li class="stripe"><img data-src-static="<?php echo THEME_URL_ASSETS.'/images/stripe-logo.svg' ?>"/></li>
                        <li class="visa"><img data-src-static="<?php echo THEME_URL_ASSETS.'/images/pay-visa.png' ?>"/></li>
                        <li class="master-card"><img data-src-static="<?php echo THEME_URL_ASSETS.'/images/pay-master-card.png' ?>"/></li>
                        <li class="paypal"><img data-src-static="<?php echo THEME_URL_ASSETS.'/images/pay-paypal.png' ?>"/></li>
                    </ul>
                </div>
            </div>
            <?php else: ?>
            <div class="row social-media-payment checkout-payment">
                <div class="col-md-6 offset-md-3">
                    <ul class="list-payment">
                        <li class="stripe"><img data-src-static="<?php echo THEME_URL_ASSETS.'/images/stripe-logo.svg' ?>"/></li>
                        <li class="visa"><img data-src-static="<?php echo THEME_URL_ASSETS.'/images/pay-visa.png' ?>"/></li>
                        <li class="master-card"><img data-src-static="<?php echo THEME_URL_ASSETS.'/images/pay-master-card.png' ?>"/></li>
                        <li class="paypal"><img data-src-static="<?php echo THEME_URL_ASSETS.'/images/pay-paypal.png' ?>"/></li>
                    </ul>
                </div>
            </div>
            <?php endif; ?>

            <?php get_copyright(); ?>
        </div>
    </footer>
    <?php endif; ?>
    <?php endif; ?>

    <input type="hidden" name="theme_url_asset" value="<?php echo THEME_URL_ASSETS; ?>">
    <input type="hidden" name="web_url" value="<?php echo site_url() . '/'; ?>">
    <input type="hidden" name="image_default_src" value="<?php echo THEME_URL_ASSETS.'/images/img-default.jpg'; ?>" />

    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
    <div class="modal fade" id="modal_textarea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content rounded-0">
                <div class="modal-header" style="background-color: #f4f8fb;height: 100px;">
                    <h2 class="modal-title" id="exampleModalLabel" style="color: #b28501;height: 100px;line-height: 79px;font-family: Cinzel,serif;font-weight: 400;font-size: 25px;">Warning!</h2>
                </div>
                <div class="modal-body">Sorry you can't include link.</div>
                <div class="modal-footer">
                    <button type="button" class="btn rounded-0" style="background-color: #b08502;color: #fff;line-height: 28px;height: 40px;border: none;" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <?php wp_footer(); ?>
</body>
</html>