<?php
/*
Template Name: Bank Transfer Detail Page
*/
?>

<?php get_header(); ?>

<?php 

        global $wp_query;
        
if( have_posts() )
{
    the_post();

    $post_id            = get_the_ID();
    $sub_title          = get_post_meta( $post_id, '_bank_trf_sub_title', true );
    $bank_trf_detail    = get_post_meta( $post_id, '_bank_trf_detail_account', true );
    $checkout_form_type = get_post_meta( $post_id, '_bank_trf_checkout_form_type', true );

    ?>
    <section class="wrap-bank-trf-page">
        <div class="logo-mermaid">
            <a href="<?php echo get_site_url(); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/mermaid-logo.png" />
            </a>
        </div>

        <div class="container-page-bank-trf clearfix">            
            <div class="title-page">
                <h1 class="heading-default">Bank Transfer Detail</h1>
                <p><?php echo $sub_title; ?></p>
            </div>
            <?php

			$bank_options  = bank_get_option('all') ;
			
            if( !empty( $bank_options) )
            {
                foreach($bank_options['bank_list'] as $d)
                {
                    if( $d['bank_category'] == $checkout_form_type )
                    {
                        ?>
                        <div class="box-bank-trf-details">
                            <div class="clearfix">
                                <img src="<?= $d['bank_logo'] ?>" class="bank-logo" />
                                <div class="bank-detail">
                                    <h3><?= $d['bank_name'] ?></h3>
                                    <h4><?= $d['bank_address'] ?></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Receiver</h5>
                                    <h4><?= $d['receiver'] ?></h4>
                                    <h5>Swift Code</h5>
                                    <h4><?= $d['swift_code'] ?></h4>
                                </div>
                                <div class="col-md-6">
                                    <h5>Account No.</h5>
                                    <h4><?= $d['account_number'] ?></h4>
                                </div>
                            </div>
                            <div class="row" style="background:none; color:inherit;">
                                <div class="col-md-12">
                                    <p><i>Registered Company name and address for bank transfers:</i> 
    								<p style="margin-bottom:0;"><?php the_excerpt() ?></p>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
            }

            ?>

            <div class="description-page">
                <?php 

                $post_content = get_the_content();
                $post_content = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $post_content );
                $post_content = apply_filters( 'the_content', $post_content );
                $post_content = str_replace( ']]>', ']]&gt;', $post_content );

                echo $post_content; 

                ?>
            </div>
        </div>        
    </section>
    <?php
}

?>

<?php get_footer(); ?>