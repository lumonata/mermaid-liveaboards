<?php
/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan list mermaid cabins
| -------------------------------------------------------------------------------------
*/
function list_mermaid_cabins()
{
    $args = array(
        'post_status'    => 'publish',
        'post_type'      => 'cabins',
        'posts_per_page' => 4,
        'meta_query'     => array(
            array(
                'key'   => '_cabins_boat',
                'value' => get_the_ID()
            )
        )
    );
    $query = new WP_Query( $args );

    while ( $query->have_posts() ):
        $query->the_post();

        $post_id      = get_the_ID();
        $post_title   = get_the_title();
        $post_content = wp_strip_all_tags( get_the_content() );
        // $post_content   = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
        // $post_content   = apply_filters('the_content', $post_content);
        // $post_content   = str_replace(']]>', ']]&gt;', $post_content);
        $post_content = strlen( $post_content ) > 350 ? rtrim( substr( $post_content, 0, 370 ) ) : $post_content;
        $post_gallery = get_post_meta( get_the_ID(), '_cabins_gallery', true );
        ?>
        <div class="col-lg-6 col-md-12 list-cabins">
            <div class="container-slide-image">
                <div class="inner">
                    <?php
                                foreach( $post_gallery as $img_id => $d ):
                                    $image        = wp_get_attachment_image_src( $img_id, 'large' );
                                    $image_mediun = wp_get_attachment_image_src( $img_id, 'medium_large' );
                                    $caption      = wp_get_attachment_caption( $img_id );
                                    $caption      = !empty( $caption ) ? '<h3>'.$caption.'</h3>' : '';
                                    ?>
                            <div class="slide-list">
                                <?php
                                                    echo '<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="'.$image[0].'" data-src-small="'.$image_mediun[0].'" class="b-lazy" />';
                                    echo $caption;
                                    ?>
                                <a href="<?php echo $image[0]; ?>" data-caption="<?php echo $caption; ?>" data-fancybox="images-<?php echo $post_id; ?>" class="fancy-group">
                                    <h4 class="view-gallery"><?php get_label_string( 'View Gallery' ); ?></h4>
                                </a>
                            </div>
                    <?php
                                endforeach;
        ?>
                </div>
            </div>

            <div class="detail-list">
                <h3><?php echo $post_title; ?></h3>
                <div class="desc"><?php echo $post_content; ?></div>
            </div>
        </div>
        
<?php
    endwhile;
    wp_reset_postdata();
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan list amenities
| -------------------------------------------------------------------------------------
*/
function list_amenities_cabins()
{
    global $wp;

    $sef       = basename( $wp->request );
    $list_data = array();
    $grid_data = array();

    $posts = get_posts(
        array(
            'posts_per_page' => -1,
            'post_type'      => 'amenities',
            'tax_query'      => array(
                array(
                    'taxonomy' => 'amenities_boat',
                    'field'    => 'slug',
                    'terms'    => $sef,
                )
            )
        )
    );

    if( count( $posts ) > 0 )
    {
        foreach( $posts as $post ):
            $post_id      = $post->ID;
            $post_title   = $post->post_title;
            $post_content = $post->post_content;
            $post_content = preg_replace( '/(<)([img])(\w+)([^>]*>)/', "", $post_content );
            $post_content = apply_filters( 'the_content', $post_content );
            $post_content = str_replace( ']]>', ']]&gt;', $post_content );
            $post_view    = get_post_meta( $post_id, '_amenities_view', true );

            if( $post_view == "grid" ):
                $grid_data[] = array(
                    'id'      => $post_id,
                    'title'   => $post_title,
                    'content' => $post_content
                );
            else:
                $list_data[] = array(
                    'id'      => $post_id,
                    'title'   => $post_title,
                    'content' => $post_content
                );
            endif;
        endforeach;
    }

    // LIST VIEW
    $total_amenities  = count( $list_data );
    $count_per_column = floor( $total_amenities / 3 );

    $i              = 1;
    $html_list_view = '
    <div class="row">
        <div class="col-lg-12">
            <div class="container container-list-slide-star clearfix">
                <ul class="list-default">
    ';

    foreach( $list_data as $d ):
        $post_title = $d['title'];

        if( $i % $count_per_column == 0 ):
            $html_list_view .= '
                        <li>'.$post_title.'</li>
                    </ul>
                ';

            if( $i != $total_amenities ):
                $html_list_view .= '
                        <ul class="list-default">
                    ';
            endif;

        else:
            $html_list_view .= '<li>'.$post_title.'</li>';
        endif;

        $i++;
    endforeach;

    $html_list_view .= '
                </ul>
            </div>
        </div>
    </div>
    ';

    echo $html_list_view;

    echo '<div class="row grid-view">';
    // GRID VIEW
    foreach( $grid_data as $d ):
        $post_id      = $d['id'];
        $post_title   = $d['title'];
        $post_content = $d['content'];
        $post_gallery = get_post_meta( $d['id'], '_amenities_gallery', true );
        ?>
        <div class="col-lg-6 col-md-12 list-cabins-amenities">
            <div class="container-slide-image">
                <div class="inner">
                    <?php
                            foreach( $post_gallery as $img_id => $d ):
                                $image        = wp_get_attachment_image_src( $img_id, 'large' );
                                $image_medium = wp_get_attachment_image_src( $img_id, 'medium_large' );
                                $caption      = wp_get_attachment_caption( $img_id );
                                $caption      = !empty( $caption ) ? '<h3>'.$caption.'</h3>' : '';
                                ?>
                            <div class="slide-list">
                                <?php
                                                echo '<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="b-lazy" data-src="'.$image[0].'" data-src-small="'.$image_medium[0].'" />';
                                echo $caption;
                                ?>
                                <a href="<?php echo $image[0]; ?>" data-caption="<?php echo $caption; ?>" data-fancybox="images-<?php echo $post_id; ?>" class="fancy-group">
                                    <h4 class="view-gallery"><?php get_label_string( 'View Gallery' ); ?></h4>
                                </a>
                            </div>
                    <?php
                            endforeach;
        ?>
                </div>
            </div>

            <div class="detail-list">
                <h3><?php echo $post_title; ?></h3>
                <div class="description"><?php echo $post_content; ?></div>
            </div>
        </div>
    <?php
    endforeach;
    echo '</div>';
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan list safety features
| -------------------------------------------------------------------------------------
*/
function list_safety_features_boat()
{
    $taxonomy = get_the_terms( get_the_ID(), 'boat_safety_features' );

    if( !empty( $taxonomy ) ):

        $total_taxonomy   = count( $taxonomy );
        $count_per_column = floor( $total_taxonomy / 3 );

        $i    = 1;
        $html = '
        <div class="row">
            <div class="col-lg-12">
                <div class="container container-list-slide-star clearfix">
                    <ul class="list-default">
        ';

        foreach( $taxonomy as $d ):
            $name = $d->name;

            if( $i % $count_per_column == 0 ):
                $html .= '
                                    <li>'.$name.'</li>
                                </ul>
                            ';

                if( $i != $total_taxonomy ):
                    $html .= '
                                    <ul class="list-default">
                                ';
                endif;

            else:
                $html .= '<li>'.$name.'</li>';
            endif;

            $i++;
        endforeach;

    $html .= '
                    </ul>   
                </div>
            </div>
        </div>
        ';

    echo $html;

    endif;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk menampilkan list system
| -------------------------------------------------------------------------------------
*/
function list_systems_boat()
{
    $args = array(
        'parent' => 0
    );
    $taxonomy_parent = wp_get_post_terms( get_the_ID(), 'boat_systems', $args );


    if( !empty( $taxonomy_parent ) ):

        $html = '
        <div class="container">
        ';

        foreach( $taxonomy_parent as $d ):
            $name    = $d->name;
            $term_id = $d->term_id;

            $html .= '<h3>'.$name.'</h3>';

            $args_child = array(
                'parent' => $term_id
            );
            $taxonomy_child = wp_get_post_terms( get_the_ID(), 'boat_systems', $args_child );

            $total_taxonomy   = count( $taxonomy_child );
            $count_per_column = floor( $total_taxonomy / 3 );


            $i          = 1;
            $html_child = '
                <div class="row list-systems">
                    <div class="col-lg-12 container-list-slide-star ">
                        <ul class="list-default">
                ';

            foreach( $taxonomy_child as $d ):
                $name = $d->name;

                if( $count_per_column > 0 && $i % $count_per_column == 0 ):
                    $html_child .= '
                                <li>'.$name.'</li>
                            </ul>
                        ';

                    if( $i != $total_taxonomy ):
                        $html_child .= '
                                <ul class="list-default">
                            ';
                    endif;

                else:
                    $html_child .= '<li>'.$name.'</li>';
                endif;

                $i++;
            endforeach;

            $html_child .= '
                        </ul>
                    </div>
                </div>
                ';

            $html .= $html_child;

        endforeach;

    $html .= '
        </div>
        ';

    echo $html;

    endif;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk mengambil data dive spot
| -------------------------------------------------------------------------------------
*/
function location_dive_spot_list()
{
    $args = array(
        'post_status'    => 'publish',
        'posts_per_page' => -1,
        'post_type'      => 'dive_spots',
        'meta_query'     => array(
            array(
                'key'   => '_dive_spot_destination',
                'value' => get_the_ID()
            )
        )
    );

    $query            = new WP_Query( $args );
    $location         = array();
    $destination_id   = get_the_ID();
    $destination_slug = get_post_field( 'post_name', get_post( $destination_id ) );

    while ( $query->have_posts() ):
        $query->the_post();
        $post_id             = get_the_ID();
        $post_title          = get_the_title();
        $post_content        = get_the_content();
        $post_lattitude      = get_post_meta( $post_id, '_dive_spot_latitude', true );
        $post_longitude      = get_post_meta( $post_id, '_dive_spot_longitude', true );
        $post_featured_image = get_the_post_thumbnail_url( $post_id, 'medium' );

        $location[] = array(
            'title'              => $post_title,
            'content'            => $post_content,
            'latitude'           => $post_lattitude,
            'longitude'          => $post_longitude,
            'icon_marker'        => THEME_URL_ASSETS . '/images/icon-marker-dive.png',
            'icon_marker_active' => THEME_URL_ASSETS . '/images/icon-marker-active.png',
            'image'              => $post_featured_image,
            'link'               => get_site_url().'/?s=&dest='.$destination_slug
        );
    endwhile;
    wp_reset_postdata();

    echo json_encode( $location );
}


/*
| -------------------------------------------------------------------------------------
| Function untuk mendapatkan group field post type
| -------------------------------------------------------------------------------------
*/
function get_group_field( $post_id, $type, $return = false )
{
    // $post_id        = get_the_ID();
    $group_field = get_post_meta( $post_id, '_'.$type.'_group_field', true );

    $count_group_field = count( $group_field );

    $html = '<div class="row custom-field-group">';

    $col_count = 1;
    foreach( $group_field as $d ):
        $class = isset( $d['class'] ) ? $d['class'] : '';
        if( $d['view'] == "full" ):

            $html .= '
            <div class="col-lg-12 '.$class.' container-list-custom-field '.$d['view'].'">
                <div class="description-group-field">
                    <h2 class="title-cf">'.$d['title'].'</h2>
                    '.$d['description'].'
                </div>
            </div>
            ';

        else:
            if( $col_count % 2 == 1 ):

                $html .= '
                </div>
                <div class="row custom-field-group">
                ';

                $col_count++;
            endif;

        $html .= '
            <div class="col-lg-6 col-md-6 '.$class.' container-list-custom-field grid-list">
                <div class="description-group-field">
                    <h2 class="title-cf">'.$d['title'].'</h2>
                    '.$d['description'].'
                </div>
            </div>
            ';
        endif;
    endforeach;

    $html .= '</div>';

    if( $return ):
        return $html;
    else:
        echo $html;
    endif;

}


/*
| -------------------------------------------------------------------------------------
| Function untuk mendapatkan itenerary data table
| -------------------------------------------------------------------------------------
*/
function get_itenerary_data_table( $iteneraries_id, $boat_id, $title = '' )
{
    $result = array(
        'data'        => array(),
        'html'        => '',
        'dives_text'  => '',
        'total_dives' => 0,
        'total_days'  => 0,
    );

    $itenerary = get_post_meta( $iteneraries_id, '_iteneraries_group_field_' . $boat_id, true );

    if( !empty( $itenerary ) )
    {
        $items = '';

        foreach( $itenerary as $dt )
        {
            $dives = empty( $dt['dives'] ) ? 0 : $dt['dives'];

            $result[ 'total_dives' ] += $dives;

            $result[ 'data' ][] = $dt;

            $items .= '
            <div class="row">
                <div class="col-md-2 day">
                    <p class="day-title">' . $dt['day'] . '</p>
                    <p class="day-sub-title">' . $dt['sub_day'] . '</p>
                </div>
                <div class="col-md-8 itenerary-detail">' . nl2br( $dt['description'] ) . '</div>
                <div class="col-md-2 dives">
                    <p class="dives-title">Dives</p>
                    <p class="dives-value">' . ( $dives == 0 ? '-' : $dives ) . '</p>
                </div>
            </div>';
        }

        $result[ 'total_days' ] = get_post_meta( $iteneraries_id, '_iteneraries_day_trip_label_' . $boat_id, true );

        $result[ 'html' ] = '
        <div class="row head">
            <div class="col-md-2 day">Day</div>
            <div class="col-md-8 itenerary-detail">
                '. get_label_string( 'Itenerary', true ) . ' with MV ' . get_the_title( $boat_id ) . ' ' . $title . '
            </div>
            <div class="col-md-2 dives">Dives </div>
        </div>
        <div class="body-itenerary-table">
            ' . $items . '
        </div>
        <div class="foot">
            Total Dives: ' . $result[ 'total_dives' ] . '
        </div>';
    }

    return $result;
}


/*
| -------------------------------------------------------------------------------------
| Function untuk mendapatkan list iteneraries
| -------------------------------------------------------------------------------------
*/
function show_list_iteneraries( $page = '', $post_id = '' )
{
    $meta_query = "";
    $args_add   = "";
    if( $page == "destination" ):
        $meta_query = array(
            array(
                'key'     => '_iteneraries_destination',
                'value'   => $post_id,
                'compare' => 'LIKE'
            )
        );
    else:
        $args_add = array(
            'post__not_in' => array( $post_id )
        );
    endif;
    ?>
    <div class="list-blue-default add-mrg">
        <?php
            $args = array(
                'post_status' => 'publish',
                'post_type'   => 'iteneraries',
                'meta_query'  => $meta_query,
            );

    $args = !empty( $args_add ) ? array_merge( $args, $args_add ) : $args;

    $query = new WP_Query( $args );

    echo '<div class="inner clearfix">';

    while ( $query->have_posts() ) : $query->the_post();
        $post_title   = get_the_title();
        $post_link    = get_permalink();
        $post_exceprt = get_the_excerpt();
        $post_exceprt = strlen( $post_exceprt ) > 100 ? rtrim( substr( $post_exceprt, 0, 120 ) ).'...' : $post_exceprt;
        $post_image   = get_the_post_thumbnail_url( get_the_ID(), 'medium' );
        $post_image   = empty( $post_image ) ? THEME_URL_ASSETS.'/images/img-default.jpg' : $post_image;
        $post_image   = empty( $post_image ) ? THEME_URL_ASSETS.'/images/img-default.jpg' : $post_image;
        ?>

            <div class="list-item-iteneraries">
                <a href="<?php echo $post_link; ?>"><img class="b-lazy" data-src="<?php echo $post_image ?>" data-src-small="<?php echo $post_image ?>" alt=""></a>
                <div class="container-desc">
                    <h5><?php get_label_string( 'Iteneraries' ); ?></h5>
                    <a href="<?php echo $post_link; ?>"><h3><?php echo $post_title; ?></h3></a>
                    <p><?php echo $post_exceprt; ?></p>
                    <a href="<?php echo $post_link; ?>" class="button-arrow"><?php get_label_string( 'Learn More' ); ?></a>
                </div>
            </div>
            
        <?php
    endwhile;
    wp_reset_postdata();

    echo '</div>';
    ?>
    </div>
<?php
}
