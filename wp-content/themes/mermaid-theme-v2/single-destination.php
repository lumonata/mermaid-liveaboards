<?php get_header(); ?>

    <?php if ( have_posts() ) : the_post(); ?>

        <?php include_once "layout/hero.php"; ?>

        <?php
            // INISIALISASI VARIBLE PAGE
            $title      = get_the_title();
            $title_text = empty($title) ? '': '<h1 class="heading-default">'.$title.'</h1>';
            $content    = get_the_content();
            $content    = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $content);
            $content    = apply_filters('the_content', $content);
            $content    = str_replace(']]>', ']]&gt;', $content);
            $content    = empty($content) ? '': '<div class="desc-about">'.$content.'</div>';

            // GET DATA LATITUDE DAN LONGITUDE
            $destination_latitude       = get_post_meta( get_the_ID(), '_destination_latitude', true );
            $destination_longitude      = get_post_meta( get_the_ID(), '_destination_longitude', true );
            $destination_zoom_level     = get_post_meta( get_the_ID(), '_destination_zoom_level', true );

            $gallery   = get_post_meta( get_the_ID(), '_destination_image', true );
        ?>


        <input type="hidden" name="destination_latitude" value="<?php echo $destination_latitude; ?>">
        <input type="hidden" name="destination_longitude" value="<?php echo $destination_longitude; ?>">
        <input type="hidden" name="destination_zoom_level" value="<?php echo $destination_zoom_level; ?>">
        <textarea name="json_dive_spot" id="" cols="30" rows="10" style="display: none;">
            <?php location_dive_spot_list(); ?>
        </textarea>

        <!-- SECTION WELCOME -->
        <section class="welcome-about welcome-mermaid">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php 
                            echo $title_text;
                            echo $content;
                        ?>
                    </div>
                </div>
            </div>

            <?php
            $social_media = get_option( 'social_media' );
            
            if( !empty( $social_media ) )
            {
                $social_media       = json_decode( $social_media );

                if( isset( $social_media->embed_youtube_link ) && !empty( $social_media->embed_youtube_link ) )
                {
                    ?>                    
                    <div class="row dive-spot-wrap">
                        <div class="container-mermaid">
                            <div class="wrap-maps-dive-spot">
                                <div class="iframe-wrapp">
                                    <?= get_iframe_youtube( $social_media->embed_youtube_link ); ?>
                                </div>
                                <div style="display: none;">
                                    <div id="map"></div>
                                    <div class="wrap-location-list">
                                        <div class="title-list"><?php get_label_string('Dive Spots'); ?></div>
                                        
                                        <div class="container-location-list">
                                            <div class="scrollbar-macosx"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }

            ?>
        </section>

        <!-- SECTION GALLERY & CUSTOM FORM -->
        <section class="custom-form-and-gallery clearfix">
            <div class="bg-grey"></div>
            <div class="container">
                <?php get_group_field(get_the_ID(), 'destination');  ?>
            </div>

            <div class="box-title-gallery">
                <div class="container clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <h2><?php echo $title.' Gallery'; ?></h2>
                        </div>
                        <div class="col-md-6">
                            <h4 class="full-screen"><?php echo get_label_string('Full Screen', true); ?></h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-mermaid slide-image-page-detail">

                <div class="container-gallery">
                    <h2 class="gallery-title"><?php echo get_the_title().' Gallery'; ?></h2>
                    <span class="close">Close</span>
                    
                    <div class="wrap-owl-slide">
                        <div class="m-arrow m-prev"></div>
                        <div class="owl-carousel owl-theme m-slide">
                            <?php
                                if(!empty($gallery)):
                                    foreach ( $gallery as $img_id => $d ):
                                        $image      = wp_get_attachment_image_src( $img_id, 'large' );
                                        $caption    = wp_get_attachment_caption( $img_id );
                                        $caption    = !empty($caption) ? '<h3>'.$caption.'</h3>' : '';

                                        // echo '
                                        //     <div class="slide-list">
                                        //         <img class="lazy" src="'.$image[0].'" />
                                        //         '.$caption.'
                                        //         <a href="'.$image[0].'" data-fancybox="images" class="fancy-group" data-caption="'.$caption.'">
                                        //             <h4 class="view-photo">'.get_label_string('View Photo', true).'</h4>
                                        //         </a>
                                        //     </div>
                                        // ';

                                        echo '
                                        <div class="item">
                                            <img src="'.$image[0].'" />
                                            '.$caption.'
                                        </div>
                                        ';
                                    endforeach;
                                endif;
                            ?>
                        </div>
                        <div class="m-arrow m-next"></div>
                    </div>

                    <!-- <div class="inner-nav"> -->
                        <?php
                            // if(!empty($gallery)):
                            //     foreach ( $gallery as $img_id => $d ):
                            //         $image      = wp_get_attachment_image_src( $img_id, 'thumbnail' );
                            //         $caption    = wp_get_attachment_caption( $img_id );
                            //         $caption    = !empty($caption) ? '<h3>'.$caption.'</h3>' : '';

                            //         echo '<div class="slide-list"><img class="lazy" data-src="'.$image[0].'" /></div>';
                            //     endforeach;
                            // endif;
                        ?>
                    <!-- </div> -->
                </div>
            </div>
        </section>

        <!-- SECTION ITENERARIES LIST -->
        <section class="itenerary-list">
            <div class="container-mermaid">
                <h2 class="title-section">
                    <?php
                        echo get_the_title()." ";
                        get_label_string('Iteneraries'); 
                    ?>
                </h2>
                <div class="line"></div>

                <?php show_list_iteneraries('destination', get_the_ID()); ?>
            </div>
        </section>

        <!-- SECTION OTHER DESTINATION -->
        <section class="other-destination">
            <div class="container-mermaid">
                <h2 class="title-section"><?php get_label_string('Other Destination'); ?></h2>
                <div class="line"></div>

                <div class="container-other-post-list">
                    <div class="inner clearfix">
                        <?php
                        $args = array(
                            'post_status'   => 'publish',
                            'post_type'     => 'destination',
                            'post__not_in'  => array(get_the_ID()),
                        );
                        $query_dest = new WP_Query($args);

                        while ($query_dest->have_posts()): 
                            $query_dest->the_post();
                            
                            $post_title     = get_the_title();
                            $post_image     = get_the_post_thumbnail_url(get_the_ID(), 'large');
                            $post_image     = empty($post_image) ? THEME_URL_ASSETS.'/images/img-default.jpg' : $post_image;
                            $post_link      = get_permalink();
                        ?>
                            <a href="<?php echo $post_link; ?>">
                                <div class="list-other-post b-lazy" data-src="<?php echo $post_image; ?>">
                                    <div class="title-other-dest">
                                        <h4><?php get_label_string('Discover'); ?></h4>
                                        <h2><?php echo $post_title; ?></h2>
                                    </div>
                                    <span class="button-arrow" data-label="<?php get_label_string('Learn More'); ?>"></span>
                                </div>
                            </a>
                        <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        </section>

    <?php endif; ?>

<?php get_footer(); ?>