<?php
/*
Template Name: FAQ Page
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php include_once "layout/hero.php"; ?>

    <section class="page-default page-static">
        <div class="container">

            <?php
            $post_content   = get_the_content();
            //$post_content   = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
            $post_content   = apply_filters('the_content', $post_content);
            $post_content   = str_replace(']]>', ']]&gt;', $post_content);
            ?>

            <h1><?php the_title(); ?></h1>
            <div class="description-page"><?php echo $post_content; ?></div>

            <div class="row page-static-content wrap-list-faq">
                <div class="col-md-12">

                    <?php
                    $args = array(
                        'post_status'       => 'publish',
                        'post_type'         => 'faq',
                        'orderby'   => 'menu_order',
                        'order'     => 'ASC',
                        'posts_per_page'     => -1
                    );
                    $query = new WP_Query($args);

                    while ($query->have_posts()) : $query->the_post();
                    
                        $post_title     = get_the_title();
                        $post_date      = date("d M Y", strtotime(get_the_date()));
                        $post_content   = get_the_content();
                        //$post_content   = preg_replace('/(<)([img])(\w+)([^>]*>)/', "", $post_content);
                        $post_content   = apply_filters('the_content', $post_content);
                        $post_content   = str_replace(']]>', ']]&gt;', $post_content);

                        echo '
                        <div class="container-list-faq">
                            <div class="title-faq clearfix">
                                <h2>'.$post_title.'</h2>
                                <img src="' . THEME_URL_ASSETS . '/images/icon-cross.png" />
                            </div>
                            <div class="desc-faq">'.$post_content.'</div>
                        </div>
                        ';
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php endwhile;  ?>

<?php get_footer(); ?>