<?php

$roles_agent = 0;
$agent_email = '';

if( is_user_logged_in() )
{
    $current_user = wp_get_current_user();
    $roles_agent  = $roles == "agent" ? 1 : 0;
    $agent_email  = $current_user->user_email;
    $user_login   = $current_user->user_login;
    $user_roles   = $current_user->roles;
    $roles        = $user_roles[0];
}

?>
<!DOCTYPE html>

<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="application-name" content="&nbsp;"/>

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    
    <?php wp_head(); ?>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NTWNJ6Q');</script>
	<!-- End Google Tag Manager -->
</head>
<body <?php body_class(); ?> >
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NTWNJ6Q"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

    <div class="loading-page"></div>

    <div class="wrap-popup-notification">
        <div class="container-popup-notification flex flex-center">
            <span class="close-popup"></span>
        </div>
    </div>

    <div class="wrap-popup-trip-details">
        <div class="container-trip-details">
            <span class="close-popup"></span>
            <div class="wrap-trip-details"></div>
        </div>
    </div>

    <?php if( $roles_agent == 1 ): ?>
    <div class="page-anchor"></div>
    <div class="navbar-top">
        <div class="row">
            <div class="col-md-1"><a href="<?php echo get_site_url(); ?>"><img class="logo b-lazy" src="<?php echo get_template_directory_uri(); ?>/assets/images/mermaid-logo-compress.png" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/mermaid-logo-compress.png" /></a></div>
            <div class="col-md-11 user-info">
                <?php 
                    echo '<a href="'.wp_logout_url( home_url() ).'" class="button-default button-logout">logout</a>'; 
                    wp_nav_menu( array( 'theme_location' => 'primary' ) );
                ?>
            </div>
        </div>

        <div class="burger-menu">
            <span class="top element"></span>
            <span class="bottom element"></span>
        </div>
    </div>
    <?php endif; ?>

    <?php if( is_singular( 'checkout' ) || is_singular( 'checkout-2' ) || is_page_template( 'page-bank-trf.php' ) || has_body_class( 'page-checkout-payment' ) ): ?>
    <div class="navbar-top">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-4">
                <a href="<?php echo get_site_url(); ?>"><img class="logo" data-src-static="<?php echo get_template_directory_uri(); ?>/assets/images/mermaid-logo.png" /></a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-8 contact-checkout">
                <!-- <span class="currency-navbar">€ EUR</span> -->
                <?php echo '<a href="'.get_permalink( get_page_by_path( 'contact-us' ) ).'" class="button-default">Contact Us</a>'; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
    
    <input type="hidden" name="email_agent" value="<?php echo $agent_email ?>" />

    <?php if( !isset( $_GET[ 'popup' ] ) && !is_page( 'agent-login' ) && !is_page( 'forgot-password' ) && !is_singular( 'checkout' ) && !is_singular( 'checkout-2' ) && !is_404() ): ?>
       
        <?php if( !is_page( 'bank-transfer-detail' ) ): ?>
        <header class="<?php if( $roles_agent ): echo 'add-mrg'; endif; ?>">
            <span class="close-popup"></span>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>

                        <?php if( $roles_agent == 1 ): ?>
                        <a href="<?php echo wp_logout_url( home_url() ) ?>" class="button-default button-logout">logout</a> 
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </header>
        <div class="burger-menu">
            <span class="top element"></span>
            <span class="bottom element"></span>
        </div>
        <?php endif; ?>
        
        <?php if( !$roles_agent ): ?>
        <nav class="fixed-top-menu shadow-default <?php if( is_page( 'bank-transfer-detail' ) ): echo "active"; endif; ?>">
            <div class="row">
                <div class="col-md-1">
                    <a href="<?php echo get_site_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/mermaid-logo-compress-transparent.png"/></a>
                </div>
                <div class="col-md-11">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                </div>
            </div>

            <div class="burger-menu">
                <span class="top element"></span>
                <span class="bottom element"></span>
            </div>
        </nav>
        <?php endif; ?>

    <?php endif; ?>

    <?php if( !is_page( 'agent-login' ) && !is_page( 'forgot-password' ) && !is_404() ): ?>
    <!-- BEGIN container-fluid -->
    <div class="container-fluid <?php if( isset( $_GET[ 'popup' ] ) ): echo "wrap-popup-trip-details"; endif; ?> <?php if( $roles_agent ): echo 'add-mrg'; endif; ?>">
    <?php endif; ?>